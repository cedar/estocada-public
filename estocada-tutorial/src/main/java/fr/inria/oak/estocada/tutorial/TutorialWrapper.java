package fr.inria.oak.estocada.tutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;
import fr.inria.oak.estocada.tutorial.utils.MetadataUtil;
import fr.inria.oak.estocada.tutorial.utils.RewritingUtil;

/**
 * Tutorial Example Wrapper
 * 
 * @author ranaalotaibi
 * 
 */
public class TutorialWrapper {

    /** Found rewritings **/
    private static List<ConjunctiveQuery> rws = new ArrayList<>();
    /** Decoded rewritings **/
    private static List<String> decodedRWs = new ArrayList<>();

    /**
     * Find and decode rewritings for an example provided in config.json
     */
    @SuppressWarnings("unchecked")
    public static void findRewritings() throws EstocadaException {

        //Read from config JSON file 
        final Map<String, Object> tutConfig = MetadataUtil.parseConfigTutorialFile();
        final Language language = Language.valueOf((String) tutConfig.get("lang"));
        final String examplePath = (String) tutConfig.get("exampleFolder");

        //find Rewritings
        try {
            rws = RewritingUtil.computeRewritings(examplePath, language);
            decodedRWs = RewritingUtil.getDeodedRWs(language);
        } catch (Exception e) {
            throw new EstocadaException(e.getMessage());
        }

    }

    /**
     * Get enocded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return RewritingUtil.getEncodedQuery();
    }

    /**
     * Get CQ rewritings
     * 
     * @return rws
     *         CQ rewritings
     */
    public static List<ConjunctiveQuery> getRWs() {
        return rws;
    }

    /**
     * Get decoded rewritings
     * 
     * @return decodedRWs decoded rewritings
     */
    public static List<String> getDecodedRWs() {
        return decodedRWs;
    }

}
