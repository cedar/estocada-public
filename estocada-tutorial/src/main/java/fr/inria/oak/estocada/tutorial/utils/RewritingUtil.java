package fr.inria.oak.estocada.tutorial.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.parser.ConstraintParser;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.commons.relationalschema.parser.RelSchemaParser;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.cg.Rewriter;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriting.decoder.aj.AJTranslator;
import fr.inria.oak.estocada.rewriting.decoder.rk.RKTranslator;
import fr.inria.oak.estocada.rewriting.decoder.sppj.SPPJTranslator;

/**
 * Rewriting Utils
 * 
 * @author ranaalotaibi
 */
public class RewritingUtil {
    private static ConjunctiveQuery encodedQuery = null;
    private static List<String> decodedRW = new ArrayList<>();
    private static List<ConjunctiveQuery> cqs = new ArrayList<>();

    /**
     * Compute rewritings
     * 
     * @param examplePath
     *            example path
     * @param language
     *            query and view language
     * @return found rewritings
     * @throws Exception
     */
    public static List<ConjunctiveQuery> computeRewritings(final String examplePath, final Language language)
            throws Exception {
        //CQ Caae: query and constrants read from files.
        if (language.equals(Language.CONJUNCTIVE_QUERY)) {
            Path ab = Paths.get(examplePath).toAbsolutePath();
            cqs = computeRewritings(ab.toString());
            encodedQuery = getQuery(ab.toString());
            return cqs;

        }

        //Parse query in `query` file and generate the corresponding CQ query.
        final ConjunctiveQuery query = ParsingUtil.getCQQuery(language, examplePath);
        encodedQuery = query;
        //Get encoded views 
        final List<QueryBlockTree> encodedViews = ParsingUtil.getViews(language, examplePath);

        //Find rewritings of a given example configuration
        cqs = getRewriting(query, encodedViews, language);

        if (language.equals(Language.CYPHER)) {//TODO:Fix
            Path ab = Paths.get(examplePath).toAbsolutePath();
            Rewriter rewriter = new Rewriter(ab.toString() + "/", false);
            cqs = rewriter.rewrite();
            encodedQuery = fr.inria.oak.estocada.compiler.Utils.parseQuery(rewriter.encodedQuery());
            decodedRW = rewriter.decode();
        }

        return cqs;
    }

    private static List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector, final Language lang) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        switch (lang) {
            case AQL:
                compilers.add(injector.getInstance(AJNaiveQueryBlockTreeCompiler.class));
                break;
            case KQL:
                compilers.add(injector.getInstance(RKNaiveQueryBlockTreeCompiler.class));
                break;
            case SQLPlusPlus:
                compilers.add(injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class));
                break;
            case CYPHER:
                break;
            case QBT:
                compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
                break;
            default:
                throw new IllegalArgumentException("Language is not supported!");
        }

        return compilers;
    }

    private static List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector,
            final Language lang) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector, lang).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private static List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts,
            final Injector injector, final Language lang) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector, lang).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

    private static List<ConjunctiveQuery> getRewriting(final ConjunctiveQuery query, final List<QueryBlockTree> views,
            final Language lang) throws Exception {
        final Injector injector = ParsingUtil.getInjector(lang);
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector, lang);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
        }
        return reformulations;
    }

    /**
     * Get encoded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return encodedQuery;
    }

    /**
     * Get decoded rw
     * 
     * @return decodedRW
     */
    public static List<String> getDeodedRWs(final Language lang) {
        switch (lang) {
            case AQL:
                final AJTranslator ajTranslator = new AJTranslator();
                for (ConjunctiveQuery cq : cqs) {
                    decodedRW.add(ajTranslator.translate(cq));
                }
                break;
            case KQL:
                final RKTranslator rkTranslator = new RKTranslator();
                for (ConjunctiveQuery cq : cqs) {
                    decodedRW.add(rkTranslator.translate(cq));
                }
                break;
            case SQLPlusPlus:
                for (ConjunctiveQuery cq : cqs) {
                    final SPPJTranslator spjjTranslator = new SPPJTranslator(cq);
                    decodedRW.add(spjjTranslator.translate());
                }
                break;
            case CYPHER:
                break;
            case QBT:
                break;
            default:
                throw new IllegalArgumentException("Language is not supported!");
        }
        return decodedRW;
    }

    //#############################FOR READING FROM FILES ######################//
    private static PACBConjunctiveQueryRewriter getRewriter(final String examplePath) throws Exception {
        return new PACBConjunctiveQueryRewriter(getContext(examplePath));
    }

    private static List<ConjunctiveQuery> computeRewritings(final ConjunctiveQuery query, final String examplePath)
            throws Exception {
        return getRewriter(examplePath).getReformulations(query);
    }

    private static List<ConjunctiveQuery> computeRewritings(final String examplePath) throws Exception {
        return computeRewritings(getQuery(examplePath), examplePath);
    }

    private static List<Constraint> parseConstraints(final String fileName)
            throws IOException, fr.inria.oak.commons.constraints.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(fr);
        final List<Constraint> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static List<RelationalSchema> parseSchemas(final String fileName)
            throws IOException, fr.inria.oak.commons.relationalschema.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final RelSchemaParser parser = new RelSchemaParser(fr);
        final List<RelationalSchema> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static Context getContext(final String examplePath) throws Exception {
        final List<RelationalSchema> schemas = parseSchemas(examplePath + "/schemas");
        return new Context(schemas.get(0), schemas.get(1), parseConstraints(examplePath + "/constraints_chase"),
                parseConstraints(examplePath + "/constraints_bkchase"));
    }

    private static ConjunctiveQuery getQuery(final String examplePath) throws Exception {
        if (examplePath.contains("BP")) {//TODO:FIX-This just for testing!!
            final ConjunctiveQuery cq = Utils.parseQuery(new File(examplePath + "/query"));
            final List<Atom> cqBody = new ArrayList<>();
            for (Atom atom : cq.getBody()) {
                for (int i = 0; i < atom.getTerms().size(); i++) {
                    cqBody.add(new Atom("D", atom.getTerm(i)));
                }
                cqBody.add(atom);
            }

            return new ConjunctiveQuery(cq.getName(), cq.getHead(), cqBody);
        }
        return Utils.parseQuery(new File(examplePath + "/query"));
    }

}
