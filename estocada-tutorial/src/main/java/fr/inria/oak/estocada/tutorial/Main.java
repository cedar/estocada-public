package fr.inria.oak.estocada.tutorial;

import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;

public class Main {

    public static void main(String[] args) {
        try {
            TutorialWrapper.findRewritings();
            System.out.println("\n****** Encoded CQ Query *****");
            System.out.println(TutorialWrapper.getEncodedQuery());
            System.out.println("\n****** CQ Rewritings *****");
            TutorialWrapper.getRWs().forEach(System.out::println);
            System.out.println("\n****** Decoded Rewritings *****");
            TutorialWrapper.getDecodedRWs().forEach(s -> System.out.print(s + "\n\n"));
        } catch (EstocadaException e) {
            System.err.println(e.getMessage());
        }

    }
}
