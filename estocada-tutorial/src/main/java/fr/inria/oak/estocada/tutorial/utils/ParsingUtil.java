package fr.inria.oak.estocada.tutorial.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.compiler.model.aj.AJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveModule;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.rk.RKQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveModule;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.sppj.SPPJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.sppj.Utils;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveModule;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.utils.RunnerUtils;

/**
 * Query and views ParsingUtils
 * 
 * @author ranaalotaibi
 */
public class ParsingUtil {

    /**
     * Get CQ of a query
     * 
     * @param lang
     *            query language
     * @return a string CQ of a query
     * @throws fr.inria.oak.commons.conjunctivequery.parser.ParseException
     * @throws IllegalArgumentException
     */
    public static ConjunctiveQuery getCQQuery(final Language lang, final String examplePath) throws EstocadaException,
            IllegalArgumentException, fr.inria.oak.commons.conjunctivequery.parser.ParseException {
        final Injector injector = getInjector(lang);
        final QueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(lang, injector);
        QueryBlockTree nbt = null;
        ConjunctiveQuery cqQuery = null;
        try {

            final String query = readQuery(examplePath, lang);
            if (query != null && queryBlockTreeBuilder != null && !lang.equals(Language.QBT)) {

                nbt = queryBlockTreeBuilder.buildQueryBlockTree(query);
            }
            final String queryStr = processCQ(nbt, lang, query);
            if (queryStr != null) {
                cqQuery = fr.inria.oak.estocada.compiler.Utils.parseQuery(queryStr);
            }

        } catch (ParseException e) {
            throw new EstocadaException("Query couldn't be parsed: \n" + e.getMessage());
        }

        return cqQuery;

    }

    /**
     * Get CQ constraints of views
     * 
     * @param lang
     *            query language
     * @param examplePath
     *            example path
     * @return list of view constraints
     */
    public static final List<QueryBlockTree> getViews(final Language lang, final String examplePath)
            throws EstocadaException {
        final Injector injector = getInjector(lang);
        final QueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(lang, injector);
        final QueryBlockTreeViewCompiler compiler = getCompiler(lang, injector);
        QueryBlockTree nbt = null;
        final List<QueryBlockTree> views = new ArrayList<>();
        final List<Constraint> fws = new ArrayList<>();
        final List<Constraint> bws = new ArrayList<>();
        final List<RelationalSchema> gs = new ArrayList<>();
        final List<RelationalSchema> ts = new ArrayList<>();

        final File[] files = new File(examplePath).listFiles();

        if (compiler != null) {
            for (final File file : files) {
                if (file.getName().contains("View")) {
                    try {

                        String view = FileUtils.readFileToString(new File(examplePath + file.getName()));
                        view = preProcessView(view, lang);
                        if (lang.equals(Language.QBT)) {//TODO:FIX
                            nbt = queryBlockTreeBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(view));
                        } else {
                            nbt = queryBlockTreeBuilder.buildQueryBlockTree(view);
                        }

                        views.add(nbt);
                        Context context =
                                compiler.compileContext(nbt, new RelationalSchema(new ArrayList<Relation>()), true);
                        fws.addAll(context.getForwardConstraints());
                        bws.addAll(context.getBackwardConstraints());
                        if (gs.isEmpty()) {
                            gs.add(context.getGlobalSchema());
                        }
                        if (ts.isEmpty()) {
                            ts.add(context.getTargetSchema());
                        }
                        if (queryBlockTreeBuilder instanceof QBTQueryBlockTreeBuilder) {
                            QBTQueryBlockTreeBuilder.reIntilize();
                        }

                    } catch (ParseException | IOException e) {
                        throw new EstocadaException("View couldn't be parsed: \n" + e.getMessage());
                    }

                }

            }

            try {
                fr.inria.oak.estocada.compiler.Utils
                        .writeConstraints(Paths.get(examplePath + "constraints_chase").toString(), fws);
                fr.inria.oak.estocada.compiler.Utils
                        .writeConstraints(Paths.get(examplePath + "constraints_bkchase").toString(), bws);
                fr.inria.oak.estocada.compiler.Utils.writeSchemas(Paths.get(examplePath + "schemas").toString(),
                        gs.get(0), ts.get(0));
            } catch (IOException e) {
                throw new EstocadaException(e.getMessage());
            }

        }
        return views;

    }

    /**
     * Get language injector
     * 
     * @param lang
     *            query language
     * @return language injector
     */
    public static Injector getInjector(final Language lang) throws EstocadaException {
        Injector injector = null;
        switch (lang) {
            case AQL:
                injector = Guice.createInjector(new AJNaiveModule());
                return injector;
            case KQL:
                injector = Guice.createInjector(new RKNaiveModule());
                return injector;
            case SQLPlusPlus:
                injector = Guice.createInjector(new SPPJNaiveModule());
                return injector;
            case CYPHER: //TODO:fix
                return null;
            case QBT:
                injector = Guice.createInjector(new QBTNaiveModule());
                return injector;
            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    /**
     * Get language builder
     * 
     * @param lang
     *            query language
     * @param injector
     *            query language injector
     * @return language builder
     */
    private static QueryBlockTreeBuilder getBuilder(final Language lang, final Injector injector)
            throws EstocadaException {
        QueryBlockTreeBuilder builder = null;
        switch (lang) {
            case AQL:
                builder = injector.getInstance(AJQueryBlockTreeBuilder.class);
                return builder;
            case KQL:
                builder = injector.getInstance(RKQueryBlockTreeBuilder.class);
                return builder;
            case SQLPlusPlus:
                builder = injector.getInstance(SPPJQueryBlockTreeBuilder.class);
                return builder;
            case CYPHER://TODO:fix
                return null;
            case QBT:
                builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
                return builder;
            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    /**
     * Get language builder
     * 
     * @param lang
     *            query language
     * @param injector
     *            query language injector
     * @return language builder
     */
    private static QueryBlockTreeViewCompiler getCompiler(final Language lang, final Injector injector)
            throws EstocadaException {
        switch (lang) {
            case AQL:
                final AJNaiveQueryBlockTreeCompiler compilerAJ =
                        injector.getInstance(AJNaiveQueryBlockTreeCompiler.class);
                return compilerAJ;
            case KQL:
                final RKNaiveQueryBlockTreeCompiler compilerRK =
                        injector.getInstance(RKNaiveQueryBlockTreeCompiler.class);
                return compilerRK;
            case SQLPlusPlus:
                final SPPJNaiveQueryBlockTreeCompiler compilerSPPJ =
                        injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class);
                return compilerSPPJ;
            case CYPHER: //TODO:fix
                return null;
            case QBT:
                final QBTNaiveQueryBlockTreeCompiler compilerQBT =
                        injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
                return compilerQBT;

            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    private static String processCQ(final QueryBlockTree nbt, final Language language, final String query) {

        final StringBuilder querystrBuilder = new StringBuilder();
        final List<Atom> atoms = new ArrayList<>();
        final List<String> returnVars = new ArrayList<>();
        final List<Atom> extraEqAtoms = new ArrayList<>();
        final List<Term> newReturnVars = new ArrayList<>();
        switch (language) {
            case AQL:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    final Variable variable = new Variable(term.toString());
                    final Variable newVariable = new Variable(term.toString() + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.aj.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                break;
            case KQL:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    String var = term.getReferredVariables().iterator().next().toString(); //TODO:Fix
                    final Variable variable = new Variable(var);
                    final Variable newVariable = new Variable(var + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.rk.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                break;
            case SQLPlusPlus:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    final Variable variable = new Variable(term.toString());
                    final Variable newVariable = new Variable(term.toString() + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.sppj.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                final List<Atom> conditioAtoms = nbt.getRoot().getPattern().encoding(Utils.conditionEncoding);
                atoms.addAll(conditioAtoms);
                break;

            case CYPHER: //TODO:fix
                return null;
            case QBT:
                return RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(query));
            default:
                throw new EstocadaException("Langugae is not supported!");

        }

        final String queryName = nbt.getRoot().getQueryName();
        //Build CQ String
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");

        //Head
        if (!newReturnVars.isEmpty()) {
            newReturnVars.forEach(r -> querystrBuilder.append(r.toString()).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        } else {
            returnVars.forEach(r -> querystrBuilder.append(r).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        }
        querystrBuilder.append(">:-");

        //Body
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }

        //ExtraEQ
        atoms.addAll(extraEqAtoms);

        if (!atoms.isEmpty()) {
            atoms.forEach(r -> querystrBuilder.append(r.toString()).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        }

        querystrBuilder.append(";");
        return querystrBuilder.toString();

    }

    private static String preProcessView(String view, Language lang) {

        view = view.replace("jsonb_array_elements", "JSONARRAYELEMENTS");
        if (view.contains("solr")) {
            view = view.replace("solr_json_build_object", "SOLRJSONBUILDOBJECT");
        } else {
            view = view.replace("json_build_object", "JSONBUILDOBJECT");
        }

        return view;
    }

    private static String readQuery(final String examplePath, final Language lang) {
        String query = null;
        try {
            query = FileUtils.readFileToString(new File(examplePath + "/Query"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return query;
    }

}
