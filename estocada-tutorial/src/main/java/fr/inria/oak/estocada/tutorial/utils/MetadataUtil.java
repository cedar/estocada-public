package fr.inria.oak.estocada.tutorial.utils;

import java.io.File;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;

public class MetadataUtil {

    /**
     * Metadata path
     */
    private final static String META_DATA_PATH = "src/main/resources/metadata.json";

    /**
     * Parse config tutorial file
     * 
     * @return parsed info
     */
    public static Map<String, Object> parseConfigTutorialFile() throws EstocadaException {
        try {
            final ObjectMapper mapper = new ObjectMapper();

            @SuppressWarnings("unchecked")
            final Map<String, Object> map = mapper.readValue(new File("config.json"), Map.class);
            return map;

        } catch (Exception ex) {
            throw new EstocadaException("Config file coudln't be parsed: \n" + ex.getMessage());
        }
    }

}
