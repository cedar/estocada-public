<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/ESTOCADA.png"  width=150  >
    <br>
</p>

# Overview
This tutorial is aimed at getting you up and running with `ESTOCADA` internal representation.
If you haven't heard about conjunctive queries and rewriting under integrity constraints, you may want to read [ESTOCADA paper](https://dl.acm.org/doi/pdf/10.1145/3299869.3319895). **More materials to be added (in progress)**. 

We will focus on ESTOCADA rewriting capabilities, which include (1) rewriting in a single model setting: query and view are of the same data model, (2) rewriting in a cross-model setting: e.g., a query is expressed in a single model and views are expressed across-models, (3) example of a rewriting with different treatments of Null and (4) example of reasoning about limited binding patterns using our intermediate abstraction

# Tutorial Setup 
(1) [Build](https://gitlab.inria.fr/cedar/estocada-public/-/tree/master#code-requirements-and-build) `ESTOCADA`. You need to provide a `config.json` file that includes the following information (as an example): 

```bash
{
	
	"exampleFolder":"examples/AJExample/",
	"lang": "AQL"
}
```

`config.json` **info**:
* `"exampleFolder":"examples/AJExample/"` (Example folder path, where we add a query and views).
* `"lang": "AQL"` (Used query language. In this tutorial, we will use different languages for each data model. `AQL`- JSON query language, `KQL`-declarative language we built on top of key-value store (Redis), `PJSQL`- PostgreSQL JSON query language, etc.)

# Rewriting: Same model Query and View
## JSON Data Model - AQL Language 
* Add your AQL query and view in `examples/AJExample/`, and make sure to specify the used language (`AQL`) and the example path in the `config.json` file. We provide the following example in `examples/AJExample/`: 

```bash
#AQL Query 
Q = for d in dataset D, 
        p in d.site.person,
          ssn in p.ssn,
          a in p.address,
            zip in a.zip
return zip
```
```bash
#AQL View 
V = 
for d in dataset D, p in d.site.person, ssn in p.ssn
return {"id": ssn, 
        "addresses": for a in p.address
                     return {"SDAddress": 
                                {"zip": for z in a.zip return z, 
                                "name": for st in a.street return st}
                            }
        }
```

* Run the following command to get (1) query `Q` encoded using JSON virtual encoding relations, (2) view `V` encoded as a set of constraints capturing the level of nesting in `V`, and (3) the relational reformulation of `Q` using `V`. 

```bash 
#If you are using JAVA17, you may need to run: export JAVA_HOME=$(/usr/libexec/java_home -v 1.8) if you are using MacOS
#Make sure to run this command under `estocada-tutorial` directory
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main 
```

The output:
``` 
****** Encoded CQ Query *****
Q<b_40> :- root_aj_68(b_0),child_aj(b_1,b_2,"ssn","o"),child_aj(b_0,c_1,"site","o"),child_aj(c_1,b_1,"person","o"),child_aj(b_3,b_4,"zip","o"),child_aj(b_1,b_3,"address","o"),eq_aj(b_4,b_40);

****** CQ Rewritings *****
RW0<b_40> :- child_aj_V(SK_4,SK_6,SK_7,"a"),eq_aj_V(SK_6,b_40),child_aj_V(SK_3,SK_4,"zip","o"),child_aj_V(SK_2,SK_3,"SDAddress","o"),child_aj_V(SK_1,SK_2,"addresses","o"),V(SK_1);

****** Decoded Rewritings *****
 FOR SK_1 IN  DATASET V,
 	 SK_2 IN SK_1.addresses, 
 	 	SK_3 IN SK_2.SDAddress, 
 	 		SK_4 IN SK_3.zip  
 RETURN SK_4
```
In `constraints_chase` and `constraints_bkchase` files, you will see the set of generated view `V` related constraints. 

## JSON Data Model - SQL++ Language
* In this simple example, the query `Q` and view `V` are expressed in SQL++. To run the example, you need to update the `config.json` file as follows:
```bash
{
	
	"exampleFolder":"examples/SPPJExample/",
	"lang": "SQLPlusPlus"
}
```

```bash
#SQL++ Query 
Q = FROM X AS D,  D.Addresses AS AD
    WHERE AD.state="CA"
    SELECT AD
```
```bash
#SQL++ View 
V = FROM X AS D
    SELECT {"CAaddresses": FROM D.Addresses AS AD WHERE AD.state="CA" SELECT AD}
```
Notice that Notice that finding the rewriting of ``Q`` using ``V`` requires reasoning about the nested blocks and the construction of a new JSON object in the SELECT clause of ``V``. The view V cannot be directly matched against Q.

Running the example,
```bash 
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main
```

The output:
```
****** Encoded CQ Query *****
Q<b_10> :- root_sppj_88(b_0),child_sppj(b_0,b_1,"Addresses","o"),child_sppj(b_1,c_2,"state","o"),val_sppj(c_2,"CA"),root_sppj_88(b_0),child_sppj(b_0,b_1,"Addresses","o"),eq_sppj(b_1,b_10);

****** CQ Rewritings *****
RW0<b_10> :- child_sppj_V(SK_0,SK_1,"CAaddresses","o"),child_sppj_V(SK_1,SK_2,SK_3,"a"),V(SK_0),eq_sppj_V(SK_2,b_10);

****** Decoded Rewritings *****
 SELECT SK_2 FROM V AS SK_0,SK_0.CAaddresses AS SK_2
```

## Key-Value Data Model - KQL Language
* In this example, the query `Q` and view `V` are expressed in KQL (Key-value query language). To run the example, you need to update the `config.json` file as follows:
```bash
{
	
	"exampleFolder":"examples/RKExample/",
	"lang": "KQL"
}
```

```bash
#KQL Query 
Q = SELECT mathGrade
	FROM AlexInfo IN MAIN ['Alex'], grades IN AlexInfo ['grades'], mathGrade IN grades ['math']
```
```bash
#KQL View 
V = SELECT "AlexGrades"->{"Math":mathGrade} 
	FROM AlexInfo IN MAIN ['Alex'], grades IN AlexInfo ['grades'], mathGrade IN grades ['math']
```

* Notice that in this example, the body of the view `V` and query `Q` matches, but in order to rewrite `Q` using `V`, this requires more reasoning about the newly constructed map in the `SELECT` clause in `V`, which the view generated constraints capture. Run the following command to inspect the view `V` generated relational constraints and get the equivalent rewriting.

```bash 
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main
```

The output:
``` 
****** Encoded CQ Query *****
Q<f_50> :- collection_rk(g_0,g_1,"grades","im"),collection_rk(g_1,f_3,"0","s"),mainmap_rk_1702218425(f_0),collection_rk(f_0,g_0,"Alex","m"),collection_rk(f_3,g_2,"math","im"),collection_rk(g_2,f_5,"0","s"),eq_rk(f_5,f_50);

****** CQ Rewritings *****
RW0<f_50> :- eq_rk(f_5,f_50),collection_rk_V(f_0,SK_0,"AlexGrades","m"),collection_rk_V(SK_0,f_5,"Math","im"),mainmap_rk_1702218425(f_0);

****** Decoded Rewritings *****
SELECT f_5 FROM  SK_0 IN MAIN["AlexGrades"],f_5 IN SK_0['Math']
```
The generated constriants appear in `constraints_chase` and `constraints_bkchase` files. 

## Graph Data Model - Cypher Language
* We currently support a fragment of Cypher graph-based language. This fragment includes
(i) pattern-matching (MATCH), (ii) WHERE clause, containing simple predicates, and (ii) construction of a new node and a relationship between nodes. We do not
support the encoding of ``Variable-length pattern matching`` in the form: (n1)-[``*``]→(n2). This kind
of matching can not be expressed directly in a conjunctive query form. 

To run the graph example, we need to update the ``config.json`` file as follows:

```bash
{
	
	"exampleFolder":"examples/CGExample/",
	"lang": "CYPHER"
}
```
```bash
#Cypher Query 
# modified from interactive-short-2 in idbc benchmark
MATCH (:Person {personId: "4398046512167"})<-[:HAS_CREATOR]-(m:Message)-[:REPLY_OF]->(p:Post)
MATCH (p)-[:HAS_CREATOR]->(c)
RETURN
  m.commentId as messageId,
  m.creationDate AS messageCreationDate,
  p.postId AS originalPostId,
  c.personId AS originalPostAuthorId,
  c.firstName as originalPostAuthorFirstName,
  c.lastName as originalPostAuthorLastName

```
```bash
#Cypher View 
MATCH (po:Post)<-[:REPLY_OF]-(m:Message)-[:HAS_CREATOR]->(p:Person {personId: "4398046512167"})
CREATE (po)<-[:REPLY_OF_v3]-(:Message_v3 {commentId:m.commentId, creationDate:m.creationDate, content:m.content, length:m.length})-[:HAS_CREATOR_v3]->(p)
```
Now, running the command below: 
```bash 
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main
```
The output:
```
****** Encoded CQ Query *****
q<v0,v1,v2,v3,v4,v5> :- Label_I(id0,"Person"),Property_I(id0,"personId",dtId0,vId0),Label_I(m,"Message"),Label_I(id1,"HAS_CREATOR"),Connection_I(m,id0,id1),Label_I(p,"Post"),Label_I(id2,"REPLY_OF"),Connection_I(m,p,id2),Label_I(id3,"HAS_CREATOR"),Connection_I(p,c,id3),Property_I(m,"commentId",dtId1,vId1),Property_I(m,"creationDate",dtId2,vId2),Property_I(p,"postId",dtId3,vId3),Property_I(c,"personId",dtId4,vId4),Property_I(c,"firstName",dtId5,vId5),Property_I(c,"lastName",dtId6,vId6),Kind_I(c,"N"),Value_I(vId0,"4398046512167"),Value_I(vId1,v0),Value_I(vId2,v1),Value_I(vId3,v2),Value_I(vId4,v3),Value_I(vId5,v4),Value_I(vId6,v5);

****** CQ Rewritings *****
RW0<v0,v1,v2,v3,v4,v5> :- Connection_I(m,p,id2),Value_I(vId1,v0),
Label_I(id1,"HAS_CREATOR"),Label_I(id2,"REPLY_OF"),
Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),
Property_I(m,"creationDate",dtId2,vId2),Value_I(vId6,v5),
Value_I(vId0,"4398046512167"),Property_I(c,"personId",dtId4,vId4),
Label_I(m,"Message"),Value_I(vId5,v4),Connection_I(m,id0,id1),
Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),
Label_I(id0,"Person"),Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(m,"commentId",dtId1,vId1),Property_I(c,"lastName",dtId6,vId6),Value_I(vId2,v1);

RW1<v0,v1,v2,v3,v4,v5> :- Connection_I(m,p,id2),Value_I(vId1,v0),Label_I(id1,"HAS_CREATOR"),
Label_I(id2,"REPLY_OF"),Label_I(SK_53,"Message_v3"),Connection_I(SK_53,id0,SK_63),
Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),Property_I(m,"creationDate",dtId2,vId2),Value_I(vId6,v5),Property_I(c,"personId",dtId4,vId4),
Label_I(m,"Message"),Value_I(vId5,v4),Connection_I(m,id0,id1),Connection_I(SK_53,p,SK_62),
Label_I(SK_63,"HAS_CREATOR_v3"),Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),Label_I(id0,"Person"),
Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(m,"commentId",dtId1,vId1),
Property_I(c,"lastName",dtId6,vId6),Value_I(vId2,v1);

RW2<v0,v1,v2,v3,v4,v5> :- Value_I(vId1,v0),Label_I(SK_53,"Message_v3"),Connection_I(SK_53,id0,SK_63),Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),Value_I(vId6,v5),Property_I(c,"personId",dtId4,vId4),Property_I(SK_53,"creationDate",dtId2,vId2),
Value_I(vId5,v4),Property_I(SK_53,"commentId",dtId1,vId1),Connection_I(SK_53,p,SK_62),
Label_I(SK_63,"HAS_CREATOR_v3"),Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),Label_I(id0,"Person"),
Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(c,"lastName",dtId6,vId6),
Value_I(vId2,v1);

****** Decoded Rewritings *****
MATCH (SK_53:Message_v3)-[SK_63:HAS_CREATOR_v3]->(id0:Person)<-[id1:HAS_CREATOR]-(m:Message)-[id2:REPLY_OF]->(p:Post)<-[SK_62]-(SK_53:Message_v3)
MATCH (p:Post)-[id3:HAS_CREATOR]->(c)
RETURN m.commentId, m.creationDate, c.firstName, c.lastName, p.postId, c.personId


MATCH (c)<-[id3:HAS_CREATOR]-(p:Post)<-[SK_62]-(SK_53:Message_v3)-[SK_63:HAS_CREATOR_v3]->(id0:Person)
RETURN c.firstName, c.lastName, SK_53.creationDate, p.postId, SK_53.commentId, c.personId
```

As we have seen in the prevoius examples, the generated constriants appear in `constraints_chase` and `constraints_bkchase` files. 


# Rewriting: Single Model Query and Cross-Model Views
In this example, we show a rewriting of a single-model query using cross-models views.
Update the ``config.json`` file as follows: 
```bash
{
	
	 "exampleFolder":"examples/SMQ-CMV/",
	 "lang": "QBT"
}
```

```bash
#QBT^XM Query - Single model using PJQL query language
Q = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB, A->'HADM_ID' AS HADM_ID
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteevents') AS NOTEVEENTS,
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'icustays') AS ICUSTAYS, 
	jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS 
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   PRESCRIPTIONS->'drug_type'= "MAIN" AND
	   PRESCRIPTIONS->'drug'= "Potassium Chloride" AND 
	   D->'Fluid'="Ascites" AND
	   P->'GENDER' ="F" AND
	   NOTEVEENTS->'TEXT'='specis'} 

RETURN	SUBJECT_ID, DOB, HADM_ID
```
```bash
#Views 
#View VPJPJ (PJ->PJ)
VPJPJ = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID,  A->'HADM_ID' AS HADM_ID, 
       ICUSTAYS->'prescriptions' AS PRESCRIPTIONS
FROM MIMIC AS P, MIMIC_D AS D, 
	 jsonb_array_elements(P->'ADMISSIONS') AS A, 
	 jsonb_array_elements(A->'labevents') AS LABEVENTS,
	 jsonb_array_elements(A->'icustays') AS ICUSTAYS 
		
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   D->'Fluid'="Ascites" }
	   
RETURN	PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID,'HADM_ID', HADM_ID,'PRESCRIPTIONS', PRESCRIPTIONS) }      	
      	

#View VPJText (PJ-SJ)
VPJText= FOR  PJ:{ 
SELECT P->'SUBJECT_ID' AS SUBJECT_ID,   A->'HADM_ID' AS HADM_ID,  
	   NOTEVEENTS->'TEXT' AS TEXT 
FROM MIMIC AS P, 
	 jsonb_array_elements(P->'ADMISSIONS') AS A, 
	 jsonb_array_elements(A->'noteevents') AS NOTEVEENTS}
	 
RETURN	 SJ:{ solr_json_build_object ("subject_id":SUBJECT_ID, "hadm_id":HADM_ID, "TEXT":TEXT )}

#View VPJPR (PJ-PR)
VPJPR= FOR  PJ:{ 
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB
FROM MIMIC AS P
WHERE P->'GENDER' ="F" } 
RETURN	 PR:{SUBJECT_ID,DOB}

```

The output: 
```
****** Encoded CQ Query *****
Q<b_7,b_8,b_9> :- root_pj_73362187(b_0),root_pj_1781587984(b_1),
child_pj(b_0,b_2,"ADMISSIONS","o"),child_pj(b_2,b_3,"noteevents","o"),
child_pj(b_2,b_4,"labevents","o"),child_pj(b_2,b_5,"icustays","o"),
child_pj(b_5,b_6,"prescriptions","o"),child_pj(b_0,b_7,"SUBJECT_ID","o"),
child_pj(b_0,b_8,"DOB","o"),child_pj(b_2,b_9,"HADM_ID","o"),child_pj(b_1,c_11,"ITEMID","o"),
child_pj(b_4,c_11,"itemid","o"),child_pj(b_6,c_12,"drug_type","o"),child_pj(b_6,c_13,"drug","o"),child_pj(b_1,c_14,"Fluid","o"),child_pj(b_0,c_15,"GENDER","o"),
child_pj(b_3,c_16,"TEXT","o"),val_pj(c_12,"MAIN"),val_pj(c_13,"Potassium Chloride"),
val_pj(c_14,"Ascites"),val_pj(c_15,"F"),val_pj(c_16,"specis");
****** CQ Rewritings *****
RW0<b_7,b_8,b_9> :- val_pj(c_12,"MAIN"),VPJPR(b_7,b_8),child_pj_VPJPJ(SK_1,b_6,"PRESCRIPTIONS","o"),VPJPJ(SK_1),child_sj_VPJText(SK_0,c_16,"TEXT","o"),
child_pj(b_6,c_13,"drug","o"),val_pj(c_16,"specis"),child_sj_VPJText(SK_0,b_9,"hadm_id","o"),
child_pj(b_6,c_12,"drug_type","o"),child_sj_VPJText(SK_0,b_7,"subject_id","o"),
child_pj_VPJPJ(SK_1,b_9,"HADM_ID","o"),child_pj_VPJPJ(SK_1,b_7,"SUBJECT_ID","o"),
val_pj(c_13,"Potassium Chloride"),VPJText(SK_0);
```

# Equality and Multiple Treatment of Null
An important issue raised by the polystore context is the fact that the notion of null values
is treated differently across the spectrum of data models/stores, which in turn has implications
on such fundamental primitives as equality and hence equi-joins, etc. Even in the single-model
relational scenario, the classical theory of conjunctive queries, constraints, and view-based
rewritings defines this real-life problem away by focusing on idealized relations without null
values.


To mitigate this issue, our current idea is to model each equality flavor by its own relational
predicate, capturing as much as possible of their intended semantics using constraints. We use
constraints to capture relationships between various equality flavors, as we will show in this example. 

Update the ``config.json`` file as follows: 
```bash
{
	
	 "exampleFolder":"examples/NullSemanticsExample/",
	 "lang": "CONJUNCTIVE_QUERY"
}
```

So, the fact that whenever two values are equal according to flavor eq2 (e.g., Store S2) they are also equal according to flavor eq1 (e.g., Store S2), but only if they are both non-null could be captured as:

```
eq2(x1,x2),notnull(x1),notnull(x2)->eq1(x1,x2);
eq1(x1,x2)->eq2(x1,x2),notnull(x1),notnull(x2);
```

Consider the query in the ``query`` file:
```
Q<y,z>:-S(x2,y),R(x1,z),eq1(x1,x2);
```

which joins R and S relations (in the store S2) on x1 and x2 using the equality flavor EqS2, where
join variables cannot match null values (the standard SQL semantics).


Now, suppose we have the following two views (in the ``constraints_chase`` file), which are materialized in the store S2, where join variables can match null values (i.e., null = null is evaluated to true):

```
R(x,y)->V1(x,y);
S(x,z)->V2(x,z);
```
Running the exampele, 
```bash 
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main
```
we get the following rewriting: 
```
RW1<y,z> :- eq2(x1,x2),notnull(x1),notnull(x2),V1(x1,z),V2(x2,y);
```

which joins V1 and V2 in (in the store S2) using the equality flavor eq2 and filtering out the null
values of x1 and x2.

The rewriting R is equivalent to Q under null-aware semantics. 

Suppose that we do not appropriately capture different null-semantics across stores, in other words, we treat equality similarly across stores.  For example, the constraint eq1(x, y)->eq2(x, y)indicates
that the semantic of equality in S1 is the same as the one in S2.

Update the equality constraint in the ``constraints_chase``file as follow: 
```
#eq1(x1,x2)->eq2(x1,x2),notnull(x1),notnull(x2);
#eq2(x1,x2),notnull(x1),notnull(x2)->eq1(x1,x2);

eq1(x1,x2)->eq2(x1,x2);
```
And in the ``constraints_bkchase`` file: 
```
#eq1(x1,x2)->eq2(x1,x2),notnull(x1),notnull(x2);
#eq2(x1,x2),notnull(x1),notnull(x2)->eq1(x1,x2);
eq2(x1,x2)->eq1(x1,x2);
```

Run the example again, you will notice that the found rewriting is not equivalent to Q under null-aware semantic.
```
RW0<y,z> :- V2(x2,y),V1(x1,z),eq2(x1,x2);
```

The reason is that join variables can match null values when using the equality flavor eq2, which is different from the equality flavor used in the original query. 


# Binding Pattern
To run the binding pattern example, update the ``config.json`` file as follows: 

```bash
{
	
	 "exampleFolder":"examples/BPExample/",
	 "lang": "CONJUNCTIVE_QUERY"
}
```

This example shows a query Q(y,z) : −R(x, y,z) that can be rendered executable only after exploiting an integrity constraint and a binding pattern. Let us consider a store S that imposes a binding access constraint on a relation R(x, y,z), which requires that y and z can be extracted (projected) only when x is given. Given this constraint, Q is not executable as such. Moreover, consider that we have a constraint that states an inclusion, which guarantees that all values of R’s first column (x) are among those in the relation S’s first column. 

```
R(x, y,z)->S(x)
```

In order to make the query executable, we first introduce a view for each access pattern
on a given relation:
```
R(x,y,z)->R_ioo(x,y,z);
S(x)->S_o(x);
```
These constraints appear in the ``constraints_chase``file. 
The above constraints state that x is required as input to project y and z from the relation
R, and x is always accessible (can be extracted) from the relation S. Then, we need to introduce extractability constraints for access pattern modeling:

```
R_ioo(x,y,z)->R(x,y,z);
S_o(x) -> D(x);
R_ioo(x,y,z), D(x) -> D(y),D(z);
```

These constraints appear in the ``constraints_bkchase``file. 

The access pattern modeling above reduces the problem from rewriting under binding
patterns to rewriting only under constraints. This means that if we have an algorithm for
rewriting under constraints, we can “trick” it into rewriting under binding patterns even though it
is unaware of them and does not treat them as first-class citizens. Now by running the example, 

```bash 
$ mvn exec:java -Dexec.mainClass=fr.inria.oak.estocada.tutorial.Main
```
The found rewriting (the executable version of Q) is:

```
RW0<y,z> :- S_o(x),R_ioo(x,y,z);
```

which extracts all x values from the relation S, and uses these values as a look-up key to extract y
and z from the relation R.


