package fr.inria.oak.commons.conjunctivequery;


/**
 * Constants are used to represent constant objects in the CQs.
 *
 * @author Stamatis Zampetakis
 * @author Damian Bursztyn
 */
public abstract class Constant implements Term,java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.oak.commons.conjunctivequery.Term#isConstant()
	 */
	public boolean isConstant() {
		return true;
	}

	@Override
	/*
	 * (non-Javadoc)
	 *
	 * @see fr.inria.oak.commons.conjunctivequery.Term#isVariable()
	 */
	public boolean isVariable() {
		return false;
	}

	public abstract Object getValue();
}
