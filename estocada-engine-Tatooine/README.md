Overview
======
Mixed-instance querying: a lightweight integration architecture for data journalism

<p align="center">
<img src="https://gitlab.inria.fr/cedar/tatooine-demo/raw/master/webroot/img/diagram.png" align="center">
</p>

---

Setup
======
In order to run **Tatooine** you will need to download and/or install software needed to connect and query several heterogenous data sources. <br />
Install only the software needed to run the classes that you need to work with. <br />
Follow this guide to get you started.

---

#### PostgreSQL ####

[Here](https://wiki.postgresql.org/wiki/Detailed_installation_guides) you can find a list of detailed PostgreSQL installation guides for different operating systems. <br />
For Mac OS X, I recommend installing PostgreSQL via [Homebrew](https://brew.sh/) by running two commands:

    brew update
    brew install postgresql
Follow the instructions at the end of the install to initialize the DB, add startup items, and start PostgreSQL. <br />
If you have any problems with the installation, please refer to the [troubleshooting section](https://gitlab.inria.fr/cedar/keyword-search/blob/master/README.md#troubleshooting) for PostgreSQL that we included as part of the documentation of Keyword-search.

---

#### Apache Jena ####

Apache Jena is a framework for building semantic web applications that provides a programmatic environment for RDF and SPARQL. 
Our [PhyJenaOperator](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyJENAOperator.java) and [JenaDatabaseHolder](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/storage/database/JENADatabaseHolder.java) classes rely on it to read and query RDF data sources in [Turtle format](https://www.w3.org/TeamSubmission/turtle/) (TTL). Have a look at the integration tests [here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyJQScanIntegrationTest.java) and [here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyJQEvalIntegrationTest.java) to get a feeling of how our RDF-based operator works. 
The language used for querying is [SPARQL](https://www.w3.org/TR/rdf-sparql-query/). <br /><br />
You can download Apache Jena from this [website](https://jena.apache.org/download/index.cgi). Simply download the latest distribution (either format: zip or tar.gz), and extract it. Move it to a path in your computer that makes sense to you.

---

#### Apache Solr ####

Apache Solr runs as a standalone server with a REST-like API that allows for full-text indexing and searching.
Our [PhySolrOperator](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/operators/physical/PhySOLROperator.java), [SolrSourceLayer](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/storage/database/SOLRSourceLayer.java), and [SolrDatabaseLoader](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/loader/SolrDatabaseLoader.java) classes use it to load and perform full-text searches on [JSON](http://www.json.org/) documents.
Have a look at the unit tests ([here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhySOLRScanTest.java) and [here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhySOLREvalTest.java)) and integration tests ([here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhySOLRScanIntegrationTest.java) and [here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhySOLREvalIntegrationTest.java)) if you want to get a overview of what this operator does and how it does it. Here's a quick [summary](https://wiki.apache.org/solr/SolrQuerySyntax) of Solr's query syntax.<br /><br />
You can download Apache Solr from this [website](http://lucene.apache.org/solr/mirrors-solr-latest-redir.html). Here's a short [tutorial](http://lucene.apache.org/solr/quickstart.html) that is very simple to follow and covers setting up Solr and performing basic document loading and searching. In order for Tatooine to interact with an active Solr instance, you will need to specify several connection parameters (URL, port, and optional authentication credentials). Take a look at the tests mentioned above for working examples of this.
You probably should download [Apache Solr 6.5.0](http://archive.apache.org/dist/lucene/solr/6.5.0/) instead of the latest version, because Apache Solr 7.6.0 doesn't contain the required configuration files.
---

#### Redis ####

Redis is an in-memory data structure store that persists on disk. 
It is based on key-value data model; you can read more about it [here](https://redis.io/topics/data-types-intro). 
Our [PhyRedisEval](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyRedisEval.java) and [RedisSourceLayer](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/storage/database/RedisSourceLayer.java) classes rely on it. 
Look at [this](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyRedisEvalTest.java) unit test to get a feeling of how our Redis operator works.<br /><br />
You can download Redis from this [website](https://redis.io/download). 
Here's a simple [tutorial](https://redis.io/topics/quickstart) that will help you get Redis up and running. The [PhyRedisEvalTest](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/operators/physical/PhyRedisEvalTest.java) test class should run without error as long as you have an active Redis instance running in the background.

---

Configuration
======
**Tatooine** relies on two configuration files.<br /><br /> 
:white_check_mark: One of them is `tatooine.conf`. It is used to load certain parameters and initial settings, and it is located [here](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/resources/tatooine.conf).
Once you will have downloaded both Apache Jena and Apache Solr somewhere in your computer (see next section), make sure to add the properties `tdbloaderPath`, `solrHome`, and `solrConfigSets`, and set their values to the paths in your computer where you stored this software.<br />
*_Example:_*

    # Path to tdbloader tool, please change this path to the corresponding one on your computer
    tdbloaderPath = /Users/cedar/bin/apache-jena-3.2.0/bin/tdbloader
    # Path to SOLR HOME
    solrHome = /Users/cedar/bin/solr-6.5.0
    # Path to Solr config files on your system, please change this path to the corresponding one on your computer
    solrConfigSets = /Users/cedar/bin/solr-6.5.0/server/solr/configsets/data_driven_schema_configs/conf

<br />:white_check_mark: The other one is `catalog.json`. 
It contains catalog entries. 
Each catalog entry is a JSON document containing the connection information needed for Tatooine to load and query a specific collection in a data source, as well as a description of the metadata that is stored in said collection.
*You should not edit this file directly*. 
Instead, add, update, or delete catalog entries by running unit tests in the [CatalogTest](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/test/java/fr/inria/cedar/commons/tatooine/loader/CatalogTest.java) class. 
This class makes use of sub-classes of [DatabaseLoader](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/loader/DatabaseLoader.java) to load data into several different tools, like Apache Jena or Apache Solr.<br />
*_Example:_*
```java
/** Adds connection information and metadata of a Solr collection called 'SocialMediaSolr' to the catalog */
    public void addSocialMediaSolr() throws Exception {
        String collectionName = "SocialMediaSolr";
        StorageReference gsr = new GSR(collectionName);
		gsr.setProperty("serverUrl", SolrDatabaseLoader.DEFAULT_SOLR_URL);
		gsr.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);
		// JSON file that we want to load in Solr
        String jsonDataFile = path + "/json/socialmedia.json";
        DatabaseLoader databaseLoader = new SolrDatabaseLoader(SOLR_CONFIG_SETS, gsr);
		String dataCollectionName = databaseLoader.getCatalogEntryName();
		// Load the collection if it's not in Solr yet
		if (!databaseLoader.isDataLoaded(dataCollectionName)) {
			// Load the data into SOLR and verify the loading process is successful
			databaseLoader.load(jsonDataFile);
			databaseLoader.registerNewCatalogEntry();
			assertTrue(databaseLoader.isDataLoaded(dataCollectionName));
		}
        // Verify that catalog has a new entry with the correct name and number of columns
		Catalog catalog = Catalog.getInstance();
		assertEquals(10, catalog.getViewSchema(collectionName).getNRSMD().colNo);
	}
```

In this example, 
[SolrDatabaseLoader](https://gitlab.inria.fr/cedar/tatooine/blob/master/src/main/java/fr/inria/cedar/commons/tatooine/loader/SolrDatabaseLoader.java) 
will create a new Solr collection called `SocialMediaSolr`, load a JSON document (`/json/socialmedia.json`) to it, 
figure out the metadata of said document (number and type of fields)
and create a new catalog entry that will contain both the connection information needed to query 
this new Solr collection (in this specific case, the values stored in `SolrDatabaseLoader.DEFAULT_SOLR_URL` and `SolrDatabaseLoader.DEFAULT_SOLR_PORT`) 
and a break-down of the metadata of this collection.

---

Wiki
======
The cedarpedia page for **Tatooine** is right [here] (https://wiki.inria.fr/cedar/Tatooine).