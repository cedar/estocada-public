package ucsd.edu.redis.RedisKQL.datatype;

/**
 * A DataType Interface
 * 
 * @author ranaalotaibi
 *
 */
public interface DataType {

	/**
	 * @return the DataType itself
	 */
	public DataType returnDataType();

	/**
	 * @return a JAVA object of any of DataType instance
	 */
	public Object returneObject();
}
