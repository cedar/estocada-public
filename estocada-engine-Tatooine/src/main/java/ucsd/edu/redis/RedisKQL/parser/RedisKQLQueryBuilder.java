package ucsd.edu.redis.RedisKQL.parser;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.ArrayConstructorContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.FromContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.KeysMapLookUpContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.MainMapLookUpContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.MapConstructorContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.RedisKQLQueryContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.SelectContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.SelectFromWhereQueryContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.SelectItemContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.SelectStatementContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.SetConsturctorContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.VarBindingContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.VarMapLookUpContext;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser.VariableContext;
import ucsd.edu.redis.RedisKQL.query.From;
import ucsd.edu.redis.RedisKQL.query.ListConstrutor;
import ucsd.edu.redis.RedisKQL.query.LookUpExpression;
import ucsd.edu.redis.RedisKQL.query.MapConstructor;
import ucsd.edu.redis.RedisKQL.query.RedisKQL;
import ucsd.edu.redis.RedisKQL.query.SelectClause;
import ucsd.edu.redis.RedisKQL.query.SelectItem;
import ucsd.edu.redis.RedisKQL.query.SelectWhereFrom;
import ucsd.edu.redis.RedisKQL.query.SetConstructor;
import ucsd.edu.redis.RedisKQL.query.VarBinding;
import ucsd.edu.redis.RedisKQL.query.Variable;

/**
 * Redis KQL Query Builder
 * 
 * @author ranaalotaibi
 *
 */
public class RedisKQLQueryBuilder extends RedisKQLBaseListener {

	private ParseTreeProperty<Object> RedisKQLQueryObjects = new ParseTreeProperty<Object>();
	private boolean parametrized ;

	/**
	 * @param obj
	 *            The sub-parse tree node
	 * @return The corresponding query object (From, Where clauses .etc)
	 */
	private Object retrieveObject(ParseTree subtree) {

		return RedisKQLQueryObjects.get(subtree);
	}

	/**
	 * @param subtree
	 *            The sub-parse tree node
	 * @param obj
	 *            The query object that's corresponding to that sub-tree
	 */
	private void setObject(ParseTree subtree, Object obj) {
		RedisKQLQueryObjects.put(subtree, obj);
	}

	/**
	 * @param tree
	 *            The RedisKQL parse tree
	 * @return The corresponding RedsiKQL query object/instance
	 */
	public RedisKQL getRedisKQL(ParseTree tree) {
		return (RedisKQL) retrieveObject(tree);
	}

	/*
	 * ========================================================================
	 * RedisKQLQuery
	 * ========================================================================
	 */
	@Override
	public void exitRedisKQLQuery(@NotNull RedisKQLQueryContext ctx) {
		setObject(ctx, retrieveObject(ctx.getChild(0)));
	}

	/*
	 * ========================================================================
	 * Select From Where Clause
	 * ========================================================================
	 */
	@Override
	public void exitSelectFromWhereQuery(@NotNull SelectFromWhereQueryContext ctx) {

		From from = (From) retrieveObject(ctx.from());
		SelectClause select = (SelectClause) retrieveObject(ctx.select());
		SelectWhereFrom selectFromWhere = new SelectWhereFrom(from, select, parametrized);
		setObject(ctx, selectFromWhere);
	}

	/*
	 * ========================================================================
	 * Select Clause
	 * ========================================================================
	 */
	@Override
	public void exitSelect(@NotNull SelectContext ctx) {
		setObject(ctx, retrieveObject(ctx.getChild(0)));
	}

	/*
	 * ========================================================================
	 * Select Statement
	 * ========================================================================
	 */
	@Override
	public void exitSelectStatement(@NotNull SelectStatementContext ctx) {
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		for (SelectItemContext varBindContext : ctx.selectItem()) {
			selectItems.add((SelectItem) retrieveObject(varBindContext));
		}
		SelectClause selectStateClause = new SelectClause(selectItems);
		setObject(ctx, selectStateClause);
	}

	/*
	 * ========================================================================
	 * Select Item
	 * ========================================================================
	 */
	@Override
	public void exitSelectItem(@NotNull SelectItemContext ctx) {

		setObject(ctx, (SelectItem) retrieveObject(ctx.getChild(0)));
	}

	/*
	 * ========================================================================
	 * Map Constructor
	 * ========================================================================
	 */
	@Override
	public void exitMapConstructor(@NotNull MapConstructorContext ctx) {

		Variable key = (Variable) retrieveObject(ctx.variable(0));
		Variable vlaue = (Variable) retrieveObject(ctx.variable(1));
		setObject(ctx, new MapConstructor(key.toString(), vlaue.toString()));
	}

	/*
	 * ========================================================================
	 * List Constructor
	 * ========================================================================
	 */
	@Override
	public void exitArrayConstructor(@NotNull ArrayConstructorContext ctx) {
		Variable var = (Variable) retrieveObject(ctx.variable());
		setObject(ctx, new ListConstrutor(var));
	}

	/*
	 * ========================================================================
	 * Set Constructor
	 * ========================================================================
	 */
	@Override
	public void exitSetConsturctor(@NotNull SetConsturctorContext ctx) {
		Variable var = (Variable) retrieveObject(ctx.variable());
		setObject(ctx, new SetConstructor(var));
	}

	/*
	 * ========================================================================
	 * From Clause
	 * ========================================================================
	 */
	@Override
	public void exitFrom(@NotNull FromContext ctx) {
		List<VarBinding> listofVarBinding = new ArrayList<VarBinding>();
		for (VarBindingContext varBindContext : ctx.varBinding()) {
			listofVarBinding.add((VarBinding) retrieveObject(varBindContext));
		}
		setObject(ctx, new From(listofVarBinding));
	}

	/*
	 * ========================================================================
	 * Binding Valriables in From Clause
	 * ========================================================================
	 */
	@Override
	public void exitVarBinding(@NotNull VarBindingContext ctx) {
		Map<Variable, LookUpExpression> varBinding = new LinkedHashMap<Variable, LookUpExpression>();
		varBinding.put((Variable) retrieveObject(ctx.variable()),
				(LookUpExpression) retrieveObject(ctx.lookUpExpression()));
		setObject(ctx, new VarBinding(varBinding));
	}

	/*
	 * ========================================================================
	 * Variable
	 * ========================================================================
	 */
	@Override
	public void exitVariable(@NotNull VariableContext ctx) {
		if (ctx.NAME() != null) {
			setObject(ctx, new Variable(ctx.NAME().getText()));
		}
	}

	/*
	 * ========================================================================
	 * Main Map Look Up - Store Look Up
	 * ========================================================================
	 */
	@Override
	public void exitMainMapLookUp(@NotNull MainMapLookUpContext ctx) {
		String mapName = ctx.MAP().getText().toString();
		List<Variable> mapVarsList = new ArrayList<Variable>();
		if (ctx.key().size() == 1) {
			if ((ctx.key(0).variable() != null)) {
				mapVarsList.add(new Variable(ctx.key(0).variable().NAME().getText()));
			} else {
				if (ctx.key(0).stringKey().STRING().toString().equals("?"))
					parametrized = true;
				mapVarsList.add((new Variable(ctx.key(0).stringKey().STRING().toString())));
			}

		} else {
			mapVarsList.add(new Variable(ctx.key(0).stringKey().STRING().toString()));
			mapVarsList.add(new Variable(ctx.key(1).stringKey().STRING().toString()));
		}
		LookUpExpression mapExpr = new LookUpExpression(mapName, null, mapVarsList);
		setObject(ctx, mapExpr);
	}

	/*
	 * ========================================================================
	 * Map look up with a predefined Variable in a query
	 * ========================================================================
	 */
	@Override
	public void exitVarMapLookUp(@NotNull VarMapLookUpContext ctx) {
		String mapName = ctx.NAME().getText().toString();
		List<Variable> mapVarList = new ArrayList<Variable>();
		/*
		 * for (VariableContext variableContext : ctx.variable()) {
		 * mapVarList.add((Variable) retrieveObject(variableContext)); }
		 */
		if (ctx.key().size() == 1) {
			if ((ctx.key(0).variable() != null)) {

				mapVarList.add(new Variable(ctx.key(0).variable().NAME().getText()));
			} else {
				mapVarList.add((new Variable(ctx.key(0).stringKey().STRING().toString())));
			}

		} else {
			mapVarList.add(new Variable(ctx.key(0).stringKey().STRING().toString()));
			mapVarList.add(new Variable(ctx.key(1).stringKey().STRING().toString()));
		}
		LookUpExpression mapExpr = new LookUpExpression(mapName, null, mapVarList);
		setObject(ctx, mapExpr);
	}

	/*
	 * ========================================================================
	 * Key look up
	 * ========================================================================
	 */
	@Override
	public void exitKeysMapLookUp(@NotNull KeysMapLookUpContext ctx) {
		String keysName = ctx.KEYS().getText().toString();
		List<Variable> keysVarList = new ArrayList<Variable>();
		String mapName;
		if (ctx.mapName().MAP() != null) {
			mapName = "MAP";
		} else
			mapName = null;
		VariableContext variableContext = ctx.mapName().variable();
		keysVarList.add((Variable) retrieveObject(variableContext));
		LookUpExpression mapExpr = new LookUpExpression(keysName, mapName, keysVarList);
		setObject(ctx, mapExpr);
	}

}
