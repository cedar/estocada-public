/**
 * 
 */
package ucsd.edu.redis.RedisKQL.datatype;

import java.math.BigDecimal;

/**
 * 
 * A Number Type
 * 
 * @author ranaalotaibi
 *
 */
public class NumberType extends AbstractDataType {
	private BigDecimal Nnumber;

	public NumberType(BigDecimal number) {
		Nnumber = number;
	}

	@Override
	public DataType returnDataType() {
		return this;
	}

	@Override
	public Object returneObject() {
		return Nnumber;
	}
}
