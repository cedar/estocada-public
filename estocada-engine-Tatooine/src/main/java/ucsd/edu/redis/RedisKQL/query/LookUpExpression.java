package ucsd.edu.redis.RedisKQL.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.exceptions.JedisDataException;
import ucsd.edu.redis.RedisKQL.datatype.DataType;
import ucsd.edu.redis.RedisKQL.datatype.ListType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;
import ucsd.edu.redis.RedisKQL.runtime.RedisConnection;

/**
 * A LookUpExpression Clause
 * 
 * @author ranaalotaibi
 *
 */
public class LookUpExpression {

	private String lookUpName;
	private String mapName;
	private List<Variable> lookUpVars;

	/**
	 * @param mlookUpName
	 *            the name of the lookup (Main Map , Keys
	 * @param mlookUpVars
	 *            the list of variables for lookup
	 */
	public LookUpExpression(String lookUpName, String mapName, List<Variable> lookUpVars) {

		this.lookUpName = lookUpName;
		this.lookUpVars = lookUpVars;
		this.mapName = mapName;

	}

	/**
	 * Apply Redis JAVA APIs for lookups within a given environment
	 * 
	 * @param val
	 *            the given Map to be evaluated in a LookUpExpression
	 * @return the result after applying LookUpExpression evaluation
	 */
	public DataType executeXRedisLookUpExpression(DataType val, List<char[]> kqlParams, int kqlParamPosition) {
		/**
		 * For Retrieving all keys from inner map or main map
		 */
		if (lookUpName.equals("KEYS")) {
			if (this.mapName == null) {
				SetType setVal = new SetType();
				MapType mapVal = (MapType) val;
				SetType internalVal = ((SetType) mapVal.getFromMap(lookUpVars.get(0).toString()));
				MapType internalMap = (MapType) internalVal.getFirstElement(internalVal);
				setVal.addValueAll(RedisConnection.jedis()
						.hkeys(internalMap.getKeys().toString().replaceAll("\\[", "").replaceAll("\\]", "")));
				return setVal;
			} else {
				SetType setVal = new SetType();
				setVal.addValueAll(RedisConnection.jedis().keys("*"));
				return setVal;
			}

		}
		if (lookUpName.equals("MAP")) {

			if (val == null) {
				return stringMainMapLookUP(kqlParams, kqlParamPosition);
			} else {
				if (((MapType) val).returnMap().get(lookUpVars.get(0).toString()) == null) {
					return stringMainMapLookUP(kqlParams, kqlParamPosition);
				} else {
					return variableMainMapLookUP(val, kqlParams, kqlParamPosition);
				}
			}
		} else {
			return internalMapLookUP(val, kqlParams, kqlParamPosition);
		}
	}

	/**
	 * @return The result of the LookUp starting from the Main Map could be
	 *         Singleton Set of String, Map of Key Value Pairs where we have a
	 *         Key String and a Value as a String, Set of Strings
	 */
	public DataType stringMainMapLookUP(List<char[]> kqlParams, int kqlParamPosition) {
		// Case #1 : Return Internal Map From Main Map
		MapType mapVal = new MapType();
		SetType setVal = new SetType();
		String lookupParam = null;
		try {
			if (lookUpVars.size() == 1) {
				if (lookUpVars.get(0).toString().equals("?")) {
					lookupParam = new String(kqlParams.get(kqlParamPosition));
				} else
					lookupParam = lookUpVars.get(0).toString().replaceAll("'", "");
				Map<String, String> valResult = RedisConnection.jedis().hgetAll(lookupParam);
				for (Map.Entry<String, String> e : valResult.entrySet()) {
					mapVal.putToMap(e.getKey(), e.getValue().toString());
				}
				MapType mapResult = new MapType();
				mapResult.putToMap(lookupParam, mapVal);
				SetType setofMaps = new SetType();
				setofMaps.addToSet(mapResult);
				return setofMaps;
			} else {
				SetType setofString = new SetType();
				setofString.addToSet(
						RedisConnection.jedis().hget(lookupParam, lookUpVars.get(1).toString().replaceAll("'", "")));
				return setofString;
			}
			// Case #2 : Return a Set of Set of Strings From Main Map

		} catch (JedisDataException mainMapExecption) {

			try {

				Set<String> valResult = RedisConnection.jedis().smembers(lookupParam);
				Iterator<String> setIterator = valResult.iterator();
				while (setIterator.hasNext()) {
					setVal.addToSet((setIterator.next()));
				}
				SetType setofSet = new SetType();
				setofSet.addToSet(setVal);
				return setofSet;
				// Case #3 : Return a Singleton Set of String from a Main Map
			} catch (JedisDataException SetException) {

				SetType setofStrings = new SetType();
				setofStrings.addToSet(RedisConnection.jedis().get(lookupParam));
				return setofStrings;
			}
		}
	}

	/**
	 * @return The result of the LookUp starting from the Main Map could be
	 *         Singleton Set of String, Map of Key Value Pairs where we have a
	 *         Key String and a Value as a String, Set of Strings
	 */
	public DataType variableMainMapLookUP(DataType val, List<char[]> kalParams, int kqlParamPosition) {
		// Case #1 : Return Internal Map From Main Map/LookUp only based on Set
		// of strings
		SetType setofMaps = new SetType();
		MapType mapVal = (MapType) val;
		MapType mapTemp = new MapType();
		SetType keyArray = new SetType();
		SetType setofSet = new SetType();
		String lookupParam = null;

		if (lookUpVars.get(0).toString().equals("?")) {
			lookupParam = new String(kalParams.get(kqlParamPosition));
		} else
			lookupParam = lookUpVars.get(0).toString().replaceAll("'", "");

		try {

			if (mapVal.getFromMap(lookupParam) instanceof SetType) {
				SetType setValues = new SetType();
				setValues = (SetType) (mapVal.getFromMap(lookupParam));
				if (setValues.returneObject().iterator().next() instanceof String) {
					keyArray = setValues;
				}
				if (setValues.returneObject().iterator().next() instanceof SetType) {
					for (Object listElement : setValues.returneObject()) {
						Iterator<?> listIterator = ((SetType) listElement).getSet().iterator();
						while (listIterator.hasNext()) {
							keyArray.addToSet(listIterator.next());
						}
					}
				}
				if (setValues.returneObject().iterator().next() instanceof MapType) {
					// Exception
				}
				if (setValues.returneObject().iterator().next() instanceof ListType) {
					// Exception
				}
			}
			for (Object key : keyArray.returneObject()) {
				Map<String, String> valResult = RedisConnection.jedis().hgetAll(key.toString());
				for (Map.Entry<String, String> e : valResult.entrySet()) {
					mapTemp.putToMap(e.getKey(), e.getValue().toString());
				}
				MapType mapResult = new MapType();
				mapResult.putToMap(key.toString(), mapTemp);
				setofMaps.addToSet(mapResult);
			}
			return setofMaps;

		} catch (JedisDataException mainMapException) {
			try {
				for (Object key : keyArray.returneObject()) {
					Set<String> valResult = RedisConnection.jedis().smembers(key.toString().replaceAll("'", ""));
					SetType setType = new SetType();
					setType.addValueAll(valResult);
					setofSet.addToSet(setType);
				}
				return setofSet;
			} catch (JedisDataException setException) {
				SetType setofStrings = new SetType();
				for (Object key : keyArray.returneObject()) {
					setofStrings.addToSet(RedisConnection.jedis().get(key.toString().replaceAll("'", "")));
				}
				return setofStrings;
			}

		}

	}

	/**
	 * @return The result of the LookUp starting from the Internal Map could be
	 *         A singleton set of a String or a Set of strings if we are
	 *         iterating over a Set of keys in an Internal
	 * 
	 */
	public DataType internalMapLookUP(DataType val, List<char[]> kqlParams, int kqlParamPosition) {
		// Case List
		SetType setofStrings = new SetType();
		SetType mapVal = (SetType) ((MapType) val).getFromMap(lookUpName);
		SetType keyArray = new SetType();
		String lookupParam = null;
		Map<String, String> innerMapInfo = new HashMap<String, String>();
		if (lookUpVars.get(0).toString().equals("?")) {
			lookupParam = new String(kqlParams.get(kqlParamPosition));
		} else
			lookupParam = lookUpVars.get(0).toString().replaceAll("'", "");

		for (Object temp : mapVal.returneObject()) {
			if ((((MapType) val).getFromMap(lookupParam)) != null) {
				SetType setofValues = new SetType();
				setofValues = (SetType) (((MapType) val).getFromMap(lookupParam));
				if (setofValues.returneObject().iterator().next() instanceof String) {
					keyArray = setofValues;
				} else {
					for (Object listElement : setofValues.returneObject()) {
						Iterator<?> listIterator = ((SetType) listElement).getSet().iterator();
						while (listIterator.hasNext()) {
							keyArray.addToSet(listIterator.next());
						}
					}
				}
				for (Object key : keyArray.returneObject()) {
					setofStrings.addToSet(RedisConnection.jedis().hget(
							(((MapType) temp).getKeys()).toString().replaceAll("\\[", "").replaceAll("\\]", ""),
							key.toString()));
				}
			} else {
				innerMapInfo.put(((MapType) temp).getKeys().toString().replaceAll("\\[", "").replaceAll("\\]", ""),
						RedisConnection.jedis().hget(
								((MapType) temp).getKeys().toString().replaceAll("\\[", "").replaceAll("\\]", ""),
								lookupParam));
			}
			if (!innerMapInfo.isEmpty()) {
				setofStrings.addToSet(innerMapInfo);
			}
		}

		return setofStrings;
	}

	@Override
	public String toString() {
		StringBuilder mapExprString = new StringBuilder();
		mapExprString.append(lookUpName);
		mapExprString.append("[");
		boolean begin = true;
		for (Variable var : lookUpVars) {
			if (!begin)
				mapExprString.append("[");
			begin = false;
			mapExprString.append(var);
			mapExprString.append("]");
		}
		return mapExprString.toString();
	}
}