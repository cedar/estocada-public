package ucsd.edu.redis.RedisKQL.query;

import ucsd.edu.redis.RedisKQL.datatype.MapType;

/**
 * A SelectItem
 * 
 * @author ranaalotaibi
 *
 */
public interface SelectItem {

	/**
	 * @param map
	 *            The Map that holds variable binding from the FROM clause,
	 *            where SELECT statement needs to be evaluated on top of it
	 * 
	 * @return The DataType as a result of the evaluation
	 */
	public Object evaluateSelectItem(MapType map);

}
