package ucsd.edu.redis.RedisKQL.query;

import java.util.List;

import ucsd.edu.redis.RedisKQL.datatype.DataType;

/**
 * A RedisKQL query interface
 * 
 */
public interface RedisKQL {
	/**
	 * Evaluate RedisKQL Query
	 * 
	 * @param map
	 *            A Map that RedisKQL Query will be evaluated in top of it
	 * @return The Result which could be a singleton Set, Set of Sets, Set of
	 *         Strings,Set of Maps
	 */
	public Object evalaueRedisKQLQuery(DataType map);

	/**
	 * Sets KQL parameters for a map look up in case of bind access
	 * 
	 * @param 
	 * 		paramter for bind access
	 */
	public void setKQLParams(final char[] param);
	
	/**
	 * Gets KQL parameters for bindAccess
	 * @return
	 */
	public List<char[]> getKQLParams();
	
	/**
	 * Re-Initialize KQLParams
	 */
	public void reInitializeKQLParams();
	
	/**
	 *  Check parameters in the query
	 * @return
	 */
	public boolean hasParam();
}
