// Generated from ucsd/edu/redis/RedisKQL/parser/RedisKQL.g4 by ANTLR 4.3
package ucsd.edu.redis.RedisKQL.parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RedisKQLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__5=1, T__4=2, T__3=3, T__2=4, T__1=5, T__0=6, FROM=7, SELECT=8, WHERE=9, 
		OFFEST=10, LIMIT=11, IN=12, MAP=13, KEYS=14, LIST=15, SET=16, NAME=17, 
		STRING=18, INTEGER=19, WHITESPACE=20;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'"
	};
	public static final String[] ruleNames = {
		"T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "FROM", "SELECT", "WHERE", 
		"OFFEST", "LIMIT", "IN", "MAP", "KEYS", "LIST", "SET", "NAME", "STRING", 
		"ESCAPE", "UNICODE", "HEX", "INTEGER", "WHITESPACE", "A", "B", "C", "D", 
		"E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", 
		"S", "T", "U", "V", "W", "X", "Y", "Z"
	};


	public RedisKQLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "RedisKQL.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\26\u0113\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\3\2\3\3\3"+
		"\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17"+
		"\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\7\22"+
		"\u00a8\n\22\f\22\16\22\u00ab\13\22\3\23\3\23\3\23\7\23\u00b0\n\23\f\23"+
		"\16\23\u00b3\13\23\3\23\3\23\3\23\3\23\7\23\u00b9\n\23\f\23\16\23\u00bc"+
		"\13\23\3\23\3\23\5\23\u00c0\n\23\3\24\3\24\3\24\5\24\u00c5\n\24\3\25\3"+
		"\25\3\25\3\25\3\25\3\25\3\26\3\26\3\27\3\27\3\27\7\27\u00d2\n\27\f\27"+
		"\16\27\u00d5\13\27\5\27\u00d7\n\27\3\30\6\30\u00da\n\30\r\30\16\30\u00db"+
		"\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36"+
		"\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3"+
		")\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62"+
		"\2\2\63\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17"+
		"\35\20\37\21!\22#\23%\24\'\2)\2+\2-\25/\26\61\2\63\2\65\2\67\29\2;\2="+
		"\2?\2A\2C\2E\2G\2I\2K\2M\2O\2Q\2S\2U\2W\2Y\2[\2]\2_\2a\2c\2\3\2%\5\2C"+
		"\\aac|\6\2\62;C\\aac|\4\2$$^^\4\2))^^\13\2$$))\61\61^^ddhhppttvv\5\2\62"+
		";CHch\3\2\63;\3\2\62;\5\2\13\f\17\17\"\"\4\2CCcc\4\2DDdd\4\2EEee\4\2F"+
		"Fff\4\2GGgg\4\2HHhh\4\2IIii\4\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4"+
		"\2OOoo\4\2PPpp\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2WWw"+
		"w\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4\2\\\\||\u0100\2\3\3\2\2\2\2\5\3\2"+
		"\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2-\3\2"+
		"\2\2\2/\3\2\2\2\3e\3\2\2\2\5g\3\2\2\2\7i\3\2\2\2\tk\3\2\2\2\13m\3\2\2"+
		"\2\ro\3\2\2\2\17q\3\2\2\2\21v\3\2\2\2\23}\3\2\2\2\25\u0083\3\2\2\2\27"+
		"\u008a\3\2\2\2\31\u0090\3\2\2\2\33\u0093\3\2\2\2\35\u0097\3\2\2\2\37\u009c"+
		"\3\2\2\2!\u00a1\3\2\2\2#\u00a5\3\2\2\2%\u00bf\3\2\2\2\'\u00c1\3\2\2\2"+
		")\u00c6\3\2\2\2+\u00cc\3\2\2\2-\u00d6\3\2\2\2/\u00d9\3\2\2\2\61\u00df"+
		"\3\2\2\2\63\u00e1\3\2\2\2\65\u00e3\3\2\2\2\67\u00e5\3\2\2\29\u00e7\3\2"+
		"\2\2;\u00e9\3\2\2\2=\u00eb\3\2\2\2?\u00ed\3\2\2\2A\u00ef\3\2\2\2C\u00f1"+
		"\3\2\2\2E\u00f3\3\2\2\2G\u00f5\3\2\2\2I\u00f7\3\2\2\2K\u00f9\3\2\2\2M"+
		"\u00fb\3\2\2\2O\u00fd\3\2\2\2Q\u00ff\3\2\2\2S\u0101\3\2\2\2U\u0103\3\2"+
		"\2\2W\u0105\3\2\2\2Y\u0107\3\2\2\2[\u0109\3\2\2\2]\u010b\3\2\2\2_\u010d"+
		"\3\2\2\2a\u010f\3\2\2\2c\u0111\3\2\2\2ef\7*\2\2f\4\3\2\2\2gh\7+\2\2h\6"+
		"\3\2\2\2ij\7<\2\2j\b\3\2\2\2kl\7]\2\2l\n\3\2\2\2mn\7.\2\2n\f\3\2\2\2o"+
		"p\7_\2\2p\16\3\2\2\2qr\5;\36\2rs\5S*\2st\5M\'\2tu\5I%\2u\20\3\2\2\2vw"+
		"\5U+\2wx\59\35\2xy\5G$\2yz\59\35\2z{\5\65\33\2{|\5W,\2|\22\3\2\2\2}~\5"+
		"]/\2~\177\5? \2\177\u0080\59\35\2\u0080\u0081\5S*\2\u0081\u0082\59\35"+
		"\2\u0082\24\3\2\2\2\u0083\u0084\5M\'\2\u0084\u0085\5;\36\2\u0085\u0086"+
		"\5;\36\2\u0086\u0087\59\35\2\u0087\u0088\5U+\2\u0088\u0089\5W,\2\u0089"+
		"\26\3\2\2\2\u008a\u008b\5G$\2\u008b\u008c\5A!\2\u008c\u008d\5I%\2\u008d"+
		"\u008e\5A!\2\u008e\u008f\5W,\2\u008f\30\3\2\2\2\u0090\u0091\5A!\2\u0091"+
		"\u0092\5K&\2\u0092\32\3\2\2\2\u0093\u0094\5I%\2\u0094\u0095\5\61\31\2"+
		"\u0095\u0096\5O(\2\u0096\34\3\2\2\2\u0097\u0098\5E#\2\u0098\u0099\59\35"+
		"\2\u0099\u009a\5a\61\2\u009a\u009b\5U+\2\u009b\36\3\2\2\2\u009c\u009d"+
		"\5G$\2\u009d\u009e\5A!\2\u009e\u009f\5U+\2\u009f\u00a0\5W,\2\u00a0 \3"+
		"\2\2\2\u00a1\u00a2\5U+\2\u00a2\u00a3\59\35\2\u00a3\u00a4\5W,\2\u00a4\""+
		"\3\2\2\2\u00a5\u00a9\t\2\2\2\u00a6\u00a8\t\3\2\2\u00a7\u00a6\3\2\2\2\u00a8"+
		"\u00ab\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa$\3\2\2\2"+
		"\u00ab\u00a9\3\2\2\2\u00ac\u00b1\7$\2\2\u00ad\u00b0\5\'\24\2\u00ae\u00b0"+
		"\n\4\2\2\u00af\u00ad\3\2\2\2\u00af\u00ae\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1"+
		"\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b2\u00b4\3\2\2\2\u00b3\u00b1\3\2"+
		"\2\2\u00b4\u00c0\7$\2\2\u00b5\u00ba\7)\2\2\u00b6\u00b9\5\'\24\2\u00b7"+
		"\u00b9\n\5\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b7\3\2\2\2\u00b9\u00bc\3\2"+
		"\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bd\3\2\2\2\u00bc"+
		"\u00ba\3\2\2\2\u00bd\u00c0\7)\2\2\u00be\u00c0\7A\2\2\u00bf\u00ac\3\2\2"+
		"\2\u00bf\u00b5\3\2\2\2\u00bf\u00be\3\2\2\2\u00c0&\3\2\2\2\u00c1\u00c4"+
		"\7^\2\2\u00c2\u00c5\t\6\2\2\u00c3\u00c5\5)\25\2\u00c4\u00c2\3\2\2\2\u00c4"+
		"\u00c3\3\2\2\2\u00c5(\3\2\2\2\u00c6\u00c7\7w\2\2\u00c7\u00c8\5+\26\2\u00c8"+
		"\u00c9\5+\26\2\u00c9\u00ca\5+\26\2\u00ca\u00cb\5+\26\2\u00cb*\3\2\2\2"+
		"\u00cc\u00cd\t\7\2\2\u00cd,\3\2\2\2\u00ce\u00d7\7\62\2\2\u00cf\u00d3\t"+
		"\b\2\2\u00d0\u00d2\t\t\2\2\u00d1\u00d0\3\2\2\2\u00d2\u00d5\3\2\2\2\u00d3"+
		"\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d7\3\2\2\2\u00d5\u00d3\3\2"+
		"\2\2\u00d6\u00ce\3\2\2\2\u00d6\u00cf\3\2\2\2\u00d7.\3\2\2\2\u00d8\u00da"+
		"\t\n\2\2\u00d9\u00d8\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00d9\3\2\2\2\u00db"+
		"\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de\b\30\2\2\u00de\60\3\2\2"+
		"\2\u00df\u00e0\t\13\2\2\u00e0\62\3\2\2\2\u00e1\u00e2\t\f\2\2\u00e2\64"+
		"\3\2\2\2\u00e3\u00e4\t\r\2\2\u00e4\66\3\2\2\2\u00e5\u00e6\t\16\2\2\u00e6"+
		"8\3\2\2\2\u00e7\u00e8\t\17\2\2\u00e8:\3\2\2\2\u00e9\u00ea\t\20\2\2\u00ea"+
		"<\3\2\2\2\u00eb\u00ec\t\21\2\2\u00ec>\3\2\2\2\u00ed\u00ee\t\22\2\2\u00ee"+
		"@\3\2\2\2\u00ef\u00f0\t\23\2\2\u00f0B\3\2\2\2\u00f1\u00f2\t\24\2\2\u00f2"+
		"D\3\2\2\2\u00f3\u00f4\t\25\2\2\u00f4F\3\2\2\2\u00f5\u00f6\t\26\2\2\u00f6"+
		"H\3\2\2\2\u00f7\u00f8\t\27\2\2\u00f8J\3\2\2\2\u00f9\u00fa\t\30\2\2\u00fa"+
		"L\3\2\2\2\u00fb\u00fc\t\31\2\2\u00fcN\3\2\2\2\u00fd\u00fe\t\32\2\2\u00fe"+
		"P\3\2\2\2\u00ff\u0100\t\33\2\2\u0100R\3\2\2\2\u0101\u0102\t\34\2\2\u0102"+
		"T\3\2\2\2\u0103\u0104\t\35\2\2\u0104V\3\2\2\2\u0105\u0106\t\36\2\2\u0106"+
		"X\3\2\2\2\u0107\u0108\t\37\2\2\u0108Z\3\2\2\2\u0109\u010a\t \2\2\u010a"+
		"\\\3\2\2\2\u010b\u010c\t!\2\2\u010c^\3\2\2\2\u010d\u010e\t\"\2\2\u010e"+
		"`\3\2\2\2\u010f\u0110\t#\2\2\u0110b\3\2\2\2\u0111\u0112\t$\2\2\u0112d"+
		"\3\2\2\2\r\2\u00a9\u00af\u00b1\u00b8\u00ba\u00bf\u00c4\u00d3\u00d6\u00db"+
		"\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}