package ucsd.edu.redis.RedisKQL.query;

import java.util.ArrayList;
import java.util.List;

import ucsd.edu.redis.RedisKQL.datatype.DataType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;

/**
 * 
 * A Sfw (Select From Where ) Query
 * 
 * @author ranaalotaibi
 *
 */
public class SelectWhereFrom extends AbstractRedisKQLQuery {
	private From fromClause;
	private SelectClause selectClause;
	private List<char[]> kalParams;
	private boolean paramterized;

	/**
	 * Create a SELECT FROM WHERE (SFW) query.
	 * 
	 * @param from
	 *            the FROM clause.
	 * @param whereClause
	 *            //TODO
	 * @param selectClause
	 *            the SELECT clause.
	 */
	public SelectWhereFrom(From fromClause, SelectClause selectClause, boolean paramterized ) {
		this.fromClause = fromClause;
		this.selectClause = selectClause;
		this.paramterized = paramterized;
		kalParams = new ArrayList<char[]>();
	}

	@Override
	public String toString() {
		StringBuilder sfwClauseString = new StringBuilder();
		sfwClauseString.append(selectClause.toString());
		sfwClauseString.append("\n");
		sfwClauseString.append(fromClause.toString());
		sfwClauseString.append("\n");
		return sfwClauseString.toString();
	}

	/**
	 * Apply a SELECT FROM WHERE (SFW) query within a given Map
	 * 
	 * @param map
	 *            the given DataType to be evaluated in SEELCT FROM WHERE clause
	 * @return the result after applying SELECT FROM WHERE Query
	 */
	@Override
	public Object evalaueRedisKQLQuery(DataType map) {
		DataType fromClauseResult = fromClause.evaluateFromClause(map, kalParams);
		Object result = selectClause.evalauteSelectClause((MapType) fromClauseResult);
		selectClause.selectClauseReset();
		return result;
	}

	@Override
	public void setKQLParams(char[] param) {
		kalParams.add(param);
	}

	@Override
	public List<char[]> getKQLParams() {
		return kalParams;
	}

	@Override
	public void reInitializeKQLParams() {
		kalParams = new ArrayList<char[]>();
	}
	@Override
	public boolean hasParam() {
		return paramterized;
	}
}
