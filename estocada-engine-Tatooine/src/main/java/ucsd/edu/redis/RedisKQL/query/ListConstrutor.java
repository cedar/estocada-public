/**
 * 
 */
package ucsd.edu.redis.RedisKQL.query;

import com.google.gson.Gson;

import ucsd.edu.redis.RedisKQL.datatype.DataType;
import ucsd.edu.redis.RedisKQL.datatype.ListType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;

/************
 * 
 * This Class is Not Used Now
 * 
 ************/
/**
 * An Array Constructor
 * 
 * @author ranaalotaibi
 *
 */
public class ListConstrutor extends AbstractSelectItem {

	private Variable variableName;

	/**
	 * Construct a List from variable values in a query
	 * 
	 * @param vName
	 *            The variable name.
	 */
	public ListConstrutor(Variable variableName) {
		this.variableName = variableName;
	}

	/**
	 * Construct the List representation that is specified in the SELECT clause
	 * 
	 * @return constructed List Type.
	 */
	@Override
	public DataType evaluateSelectItem(MapType map) {

		ListType returnedList = new ListType();
		return returnedList;
	}

	/**
	 * Return JSONArray String Representation
	 * 
	 * @param listVal
	 *            the listVal to be converted to JSONArray
	 */
	public String JSONArray(ListType listVal) {
		return new Gson().toJson(listVal.returnArray());
	}

	/*
	 * ========================================================================
	 * String Representation of List Constructor
	 * ========================================================================
	 */
	@Override
	public String toString() {
		return "list ( " + variableName.returnVariableName() + " )";
	}

}
