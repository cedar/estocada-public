/**
 * 
 */
package ucsd.edu.redis.RedisKQL.datatype;

/**
 * A String Type
 * 
 * @author Rana Alotaibi
 * 
 */

public class StringType extends AbstractDataType {
	private String sString;

	public StringType(String string) {
		sString = string;
	}

	public String getString() {
		return sString;
	}

	@Override
	public String toString() {
		return "\"" + sString + "\"";
	}

	@Override
	public DataType returnDataType() {
		return this;
	}

	@Override
	public Object returneObject() {
		return sString;
	}
}
