package ucsd.edu.redis.RedisKQL.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;

/**
 * A SELECT Clause
 * 
 * @author ranaalotaibi
 *
 */
public class SelectClause {
	private List<SelectItem> selectItems;
	private SetType setVal;

	/**
	 * Create a Select Clause
	 * 
	 * @param selectItem
	 *            selectItem could be variable or complex types constructor
	 */
	public SelectClause(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
		this.setVal = new SetType();
	}

	/**
	 * @param map
	 *            The Map that holds the variable binding from the FROM Clause
	 * @return The result Value
	 */
	public Object evalauteSelectClause(MapType map) {

		SetType innerSet = new SetType();
		Map<String, Set<String>> innerMaps = new HashMap<String, Set<String>>();
		for (SelectItem selectItem : selectItems) {
			if (selectItem instanceof Variable) {
				Iterator<Object> setIterator = ((SetType) selectItem.evaluateSelectItem(map)).getSet().iterator();
				Object setValue = setIterator.next();
				if (setValue instanceof String) {
					innerSet.addToSet(setValue);
				}
				if (setValue instanceof Map) {
					Set<?> setEntry = ((Map<?, ?>) setValue).entrySet();
					for (Object entry : setEntry) {
						String key = ((Map.Entry<?, ?>) entry).getKey().toString();
						String val = ((Map.Entry<?, ?>) entry).getValue().toString();
						Set<String> innerSetVal = new LinkedHashSet<String>();
						if (!innerMaps.containsKey(key)) {
							innerSetVal.add(val);
							innerMaps.put(key, innerSetVal);
						} else {
							innerSetVal = innerMaps.get(key);
							innerSetVal.add(val);
							innerMaps.put(key, innerSetVal);
						}
					}
				} else {
					setVal = (((SetType) selectItem.evaluateSelectItem(map)));
					return setVal;
				}
			}

		}
		if (!innerMaps.isEmpty()) {
			for (Set<String> setValue : innerMaps.values()) {
				SetType set = new SetType();
				set.addValueAll(setValue);
				setVal.addToSet(set);
			}
			return setVal;
		} else
			setVal.addToSet(innerSet);
		return setVal;
	}

	/**
	 * Re-set The outer level SetType
	 */
	public void selectClauseReset() {
		this.setVal = null;
	}

	@Override
	public String toString() {
		StringBuilder statementClauseString = new StringBuilder();
		statementClauseString.append("SELECT ");
		statementClauseString.append(selectItems.toString());
		return statementClauseString.toString();
	}
}
