package ucsd.edu.redis.RedisKQL.query;

import java.util.List;

import ucsd.edu.redis.RedisKQL.datatype.DataType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;

/**
 * A From Clause Class
 * 
 * @author ranaalotaibi
 *
 */
public class From {
	private List<VarBinding> varBinding;

	/**
	 * Create a FROM clause.
	 * 
	 * @param fVarBinding
	 *            The list of variable bindings in the From Clause
	 */
	public From(List<VarBinding> varBinding) {
		this.varBinding = varBinding;
	}

	/**
	 * Apply a FROM clause evaluation within a given lookup
	 * 
	 * @param map
	 *            The given DataType which is a Map to be evaluated in FROM
	 * @param kqlParams
	 *            Parameters for setting in case of bind access
	 * @return the result after applying FROM clause
	 */
	public DataType evaluateFromClause(DataType map, List<char[]> kqlParams) {
		DataType valResult = null;
		int kqlParamPos = 0;
		for (VarBinding variableBinding : varBinding) {
			if (valResult != null) {
				valResult = variableBinding.executeXRedisVarBindingClause((MapType) valResult, kqlParams, kqlParamPos);
				kqlParamPos++;
			} else {
				valResult = variableBinding.executeXRedisVarBindingClause((MapType) map, kqlParams, kqlParamPos);
				kqlParamPos++;
			}
		}
		return valResult;
	}

	/*
	 * ========================================================================
	 * String Representation of From Clause
	 * ========================================================================
	 */
	@Override
	public String toString() {

		StringBuilder fromClauseString = new StringBuilder();
		fromClauseString.append("FROM ");
		boolean begin = true;
		for (VarBinding var : varBinding) {
			if (!begin)
				fromClauseString.append(",");
			begin = false;
			fromClauseString.append(var.toString());
		}
		return fromClauseString.toString();
	}
}
