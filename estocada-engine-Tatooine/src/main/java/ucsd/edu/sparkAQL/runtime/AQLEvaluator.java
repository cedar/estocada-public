package ucsd.edu.sparkAQL.runtime;

import org.apache.spark.sql.Dataset;

import ucsd.edu.sparkAQL.query.Expression;

/**
 * An evaluator for XSparkSQL Query
 * 
 * @author ranaalotaibi
 * 
 */
public class AQLEvaluator {
    /**
     * Evaluates an XSparkSQL query
     * 
     * @param query
     *            the query.
     * @return the DataFrame as a result of evaluating the query
     */
    public Dataset evaluateAQLQuery(final Expression query, final String jsonDataSetPath) {
        assert (query != null);
        return query.evaluateExpression(null, jsonDataSetPath);

    }
}
