package ucsd.edu.sparkAQL.runtime;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SQLContext;

/**
 * A Connection to SparkSQL Engine
 * 
 * @author ranaalotaibi
 * 
 */
public class SparkSQLConnection {
	public static JavaSparkContext	sc			= new JavaSparkContext("local", "spark");
	public static SQLContext		sqlContext	= new SQLContext(sc);

	public static void Conn() {
		sc.setLogLevel("ERROR");
	}

}
