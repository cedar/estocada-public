/**
 * 
 */
package ucsd.edu.sparkAQL.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.apache.log4j.Logger;

import ucsd.edu.sparkAQL.parser.AQLParser.AqlQueryContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ArrayExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.AttributeNameContext;
import ucsd.edu.sparkAQL.parser.AQLParser.EqualExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ForClauseContext;
import ucsd.edu.sparkAQL.parser.AQLParser.FwrQueryContext;
import ucsd.edu.sparkAQL.parser.AQLParser.OtherruleContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ParenthesizedExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.RecordConstructorContext;
import ucsd.edu.sparkAQL.parser.AQLParser.RecordExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ReturnClauseContext;
import ucsd.edu.sparkAQL.parser.AQLParser.ReturnStatementContext;
import ucsd.edu.sparkAQL.parser.AQLParser.SourceAccessExpressionContext;
import ucsd.edu.sparkAQL.parser.AQLParser.VarBindingContext;
import ucsd.edu.sparkAQL.parser.AQLParser.VariableRefContext;
import ucsd.edu.sparkAQL.parser.AQLParser.WhereClauseContext;
import ucsd.edu.sparkAQL.query.Expression;
import ucsd.edu.sparkAQL.query.ForClause;
import ucsd.edu.sparkAQL.query.ForWhereReturnClause;
import ucsd.edu.sparkAQL.query.OperatorFinder;
import ucsd.edu.sparkAQL.query.ParenthesizedExpression;
import ucsd.edu.sparkAQL.query.PrimaryExpression;
import ucsd.edu.sparkAQL.query.RecordConstructor;
import ucsd.edu.sparkAQL.query.ReturnClause;
import ucsd.edu.sparkAQL.query.ReturnStatemnet;
import ucsd.edu.sparkAQL.query.SourceAccessExpression;
import ucsd.edu.sparkAQL.query.StringType;
import ucsd.edu.sparkAQL.query.VarBinding;
import ucsd.edu.sparkAQL.query.VariableRef;
import ucsd.edu.sparkAQL.query.WhereClause;

public class AQLBuilder extends AQLBaseListener {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(AQLBuilder.class);

    private static final Pattern UNICODE_LITERAL_PATTERN = Pattern.compile("\\\\u([0-9a-fA-F]{4})");

    private ParseTreeProperty<Object> XSparkSQLQueryObjects = new ParseTreeProperty<Object>();

    private boolean paramterized = false;

    public Expression getQuery(ParseTree parse_tree) {
        assert (parse_tree != null);
        return (Expression) retrieveObject(parse_tree);
    }

    private Object retrieveObject(ParseTree obj) {

        return XSparkSQLQueryObjects.get(obj);
    }

    private void setObject(ParseTree subtree, Object obj) {
        XSparkSQLQueryObjects.put(subtree, obj);
    }

    @Override
    public void exitAqlQuery(@NotNull AqlQueryContext ctx) {
        setObject(ctx, retrieveObject(ctx.getChild(0)));
    }

    @Override
    public void exitExpression(@NotNull ExpressionContext ctx) {
        ctx.toString();
        setObject(ctx, retrieveObject(ctx.getChild(0)));
    }

    @Override
    public void exitSourceAccessExpression(@NotNull SourceAccessExpressionContext ctx) {
        String sourceName = "";
        sourceName = ctx.getText().replaceAll("\'", "");
        setObject(ctx, new SourceAccessExpression(sourceName));
    }

    @Override
    public void exitFwrQuery(@NotNull FwrQueryContext ctx) {
        ForClause forClause = (ForClause) retrieveObject(ctx.forClause());
        WhereClause whereClause = null;
        if (ctx.whereClause() != null) {
            whereClause = (WhereClause) retrieveObject(ctx.whereClause());
        }
        ReturnClause returnClause = (ReturnClause) retrieveObject(ctx.returnClause());
        ForWhereReturnClause fwrQuery = new ForWhereReturnClause(forClause, whereClause, returnClause, paramterized);
        setObject(ctx, fwrQuery);
    }

    @Override
    public void exitForClause(@NotNull ForClauseContext ctx) {
        List<VarBinding> varBindings = new ArrayList<VarBinding>();
        for (VarBindingContext varBinding : ctx.varBinding()) {

            varBindings.add((VarBinding) retrieveObject(varBinding));
        }
        setObject(ctx, new ForClause(varBindings));
    }

    @Override
    public void exitVarBinding(@NotNull VarBindingContext ctx) {
        Map<VariableRef, PrimaryExpression> varBinding = new LinkedHashMap<VariableRef, PrimaryExpression>();
        VariableRef variableName = (VariableRef) retrieveObject(ctx.variableRef());
        PrimaryExpression priamryExpression = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        varBinding.put(variableName, priamryExpression);
        setObject(ctx, new VarBinding(varBinding));
    }

    @Override
    public void exitWhereClause(@NotNull WhereClauseContext ctx) {
        PrimaryExpression priamryExpression = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        setObject(ctx, new WhereClause(priamryExpression));
    }

    @Override
    public void exitReturnClause(@NotNull ReturnClauseContext ctx) {
        setObject(ctx, retrieveObject(ctx.getChild(0)));
    }

    @Override
    public void exitReturnStatement(@NotNull ReturnStatementContext ctx) {
        PrimaryExpression pariamryExpression = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        ReturnStatemnet returnStatement = new ReturnStatemnet(pariamryExpression);
        setObject(ctx, returnStatement);
    }

    @Override
    public void exitRecordExpression(@NotNull RecordExpressionContext ctx) {
        String operator = "RecordPathExpression";

        PrimaryExpression pariamryExpression = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        StringType name;
        if (ctx.NAME() != null) {
            name = new StringType(ctx.NAME().getText());
        } else {
            name = new StringType(getStringLiteral(ctx.STRING().getText()));
        }
        OperatorFinder operatorFinder = new OperatorFinder(operator, Arrays.<Object> asList(pariamryExpression, name));
        setObject(ctx, operatorFinder);
    }

    @Override
    public void exitArrayExpression(@NotNull ArrayExpressionContext ctx) {
        String operator = "ArrayPathExpression";
        PrimaryExpression pariamryExpression = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        StringType index = new StringType(null);
        if (ctx.INDEX() != null) {
            index = new StringType(ctx.INDEX().getText());
        }
        OperatorFinder operatorFinder = new OperatorFinder(operator, Arrays.<Object> asList(pariamryExpression, index));
        setObject(ctx, operatorFinder);
    }

    @Override
    public void exitEqualExpression(@NotNull EqualExpressionContext ctx) {
        String operator = "Equality";
        if (ctx.STRING().getText().contains("?")) {
            paramterized = true;
        }
        PrimaryExpression pariamryExpressionLeft = (PrimaryExpression) retrieveObject(ctx.primaryExpression());
        OperatorFinder operatorFinder = new OperatorFinder(operator,
                Arrays.<Object> asList(pariamryExpressionLeft, new String(ctx.STRING().getText())));
        setObject(ctx, operatorFinder);
    }

    @Override
    public void exitOtherrule(@NotNull OtherruleContext ctx) {
        setObject(ctx, retrieveObject(ctx.getChild(0)));

    }

    @Override
    public void exitParenthesizedExpression(@NotNull ParenthesizedExpressionContext ctx) {
        ForWhereReturnClause expression = (ForWhereReturnClause) retrieveObject(ctx.expression());
        ParenthesizedExpression parenthesizedExpression = new ParenthesizedExpression(expression);
        setObject(ctx, parenthesizedExpression);
    }

    @Override
    public void exitRecordConstructor(@NotNull RecordConstructorContext ctx) {
        RecordConstructor recordConstructor = new RecordConstructor();
        for (AttributeNameContext attributeName : ctx.attributeName()) {
            String attName;
            if (attributeName.NAME() != null) {
                attName = attributeName.NAME().getText();
            } else {
                attName = getStringLiteral(attributeName.STRING().getText());
            }
            PrimaryExpression primaryExpression = (PrimaryExpression) retrieveObject(attributeName.primaryExpression());
            recordConstructor.setPrimaryExpression(attName, primaryExpression);
        }
        setObject(ctx, recordConstructor);
    }

    @Override
    public void exitVariableRef(@NotNull VariableRefContext ctx) {
        setObject(ctx, new VariableRef(ctx.getText()));
    }

    public static String getStringLiteral(String str) {
        // Remove quotes
        String s = str.substring(1, str.length() - 1);

        // Escape characters
        s = s.replace("\\'", "'");
        s = s.replace("\\\"", "\"");
        s = s.replace("\\\\", "\\");
        s = s.replace("\\/", "/");
        s = s.replace("\\b", "\b");
        s = s.replace("\\f", "\f");
        s = s.replace("\\n", "\n");
        s = s.replace("\\r", "\r");
        s = s.replace("\\t", "\t");

        // Unicode
        Matcher matcher = UNICODE_LITERAL_PATTERN.matcher(s);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            int codepoint = Integer.valueOf(matcher.group(1), 16);
            String symbol = Character.toString((char) codepoint);
            matcher.appendReplacement(sb, symbol);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

}
