package ucsd.edu.sparkAQL.query;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * A for where return Query
 * 
 * @author ranaalotaibi
 * 
 */
public class VarBinding {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(VarBinding.class);

    private Map<VariableRef, PrimaryExpression> varBinding = new LinkedHashMap<VariableRef, PrimaryExpression>();

    public VarBinding(Map<VariableRef, PrimaryExpression> varBinding) {

        this.varBinding = varBinding;

    }

    /**
     * @param varBindingVal
     * @return an new updated varBindingVal environment
     */
    public Map<String, Dataset<Row>> evaluateVarBiding(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {

        // RecordPathExpression.preNaviagteVar = "";
        Map<String, Dataset<Row>> varBindingMap = new LinkedHashMap<String, Dataset<Row>>();
        if (varBindingVal != null) {

            varBindingMap.putAll(varBindingVal);
            for (Map.Entry<VariableRef, PrimaryExpression> e : varBinding.entrySet()) {
                Dataset<Row> df = e.getValue().evaluateExpression(varBindingMap, jsonPath);
                varBindingMap.put(e.getKey().toString(), df);
            }

        } else {
            for (Map.Entry<VariableRef, PrimaryExpression> e : varBinding.entrySet()) {
                Dataset<Row> df = e.getValue().evaluateExpression(varBindingVal, jsonPath);
                varBindingMap.put(e.getKey().toString(), df);
            }

        }

        return varBindingMap;
    }

    /**
     * @param varBindingVal
     * @return
     */
    public Map<String, Dataset> varBindingArrayIteration(Map<String, Dataset<Row>> varBindingVal,
            final String jsonPath) {

        for (Map.Entry<VariableRef, PrimaryExpression> e : varBinding.entrySet()) {
            Dataset<Row> df = e.getValue().evaluateExpression(varBindingVal, jsonPath);
            if (df.schema().toString().contains("Array")) {
                // System.out.print("Array\n********************");
                // Get the root DataFrame
                String rootDF = e.getValue().toString().charAt(1) + "";
                varBindingVal.get(rootDF).registerTempTable(rootDF);

                // DataFrame rootDF = e.getValue().toString();
            }

        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Map.Entry<VariableRef, PrimaryExpression> e : varBinding.entrySet()) {
            if (!first)
                sb.append(", ");
            first = false;
            sb.append(e.getKey());
            sb.append(" IN ");
            sb.append(e.getValue());
        }
        return sb.toString();
    }
}
