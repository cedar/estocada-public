package ucsd.edu.sparkAQL.query;

import org.apache.log4j.Logger;

public abstract class AbstractOperatorExpression implements Operator {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(AbstractOperatorExpression.class);
}
