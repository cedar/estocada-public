/**
 * 
 */
package ucsd.edu.sparkAQL.query;

import org.apache.log4j.Logger;

/**
 * Abstract Class that implements XSparkSQL Queries
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractAQLQuery implements Expression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(AbstractAQLQuery.class);

}
