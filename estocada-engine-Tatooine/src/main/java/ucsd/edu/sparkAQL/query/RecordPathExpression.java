package ucsd.edu.sparkAQL.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

public class RecordPathExpression extends AbstractOperatorExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(RecordPathExpression.class);

    public static final OperatorName OPERATORNAME =
            new OperatorName("RecordPathExpression", Arrays.<Object> asList(new Object(), new Object()));
    public static String preNaviagteVar = "";
    public static String currNaviagteVar = "";
    public static String preNaviagteVarTemp = "";

    @Override
    public OperatorName getOperator() {
        return OPERATORNAME;
    }

    @Override
    public Dataset<Row> evaluateOperator(Map<String, Dataset<Row>> varBinding, List<Object> operatorParameters) {
        Dataset<Row> df = null;
        Dataset<Row> lhs = (Dataset<Row>) operatorParameters.get(0);
        lhs.registerTempTable("tempDF");
        if (preNaviagteVar.equals("") && currNaviagteVar.equals("")) {
            df = SparkSQLConnection.sqlContext.sql("SELECT " + operatorParameters.get(1) + " FROM tempDF");
        } else {
            if (!currNaviagteVar.equals("")) {
                df = SparkSQLConnection.sqlContext.sql("SELECT " + currNaviagteVar + " FROM tempDF");

            } else

                df = SparkSQLConnection.sqlContext
                        .sql("SELECT " + preNaviagteVar + "." + operatorParameters.get(1) + " FROM tempDF");
        }
        preNaviagteVarTemp = preNaviagteVar;
        preNaviagteVar = (String) operatorParameters.get(1);
        currNaviagteVar = "";
        return df;
    }
}
