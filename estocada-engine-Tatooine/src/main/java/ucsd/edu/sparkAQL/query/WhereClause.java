
package ucsd.edu.sparkAQL.query;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

public class WhereClause {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(WhereClause.class);

    private PrimaryExpression wherePrimaryExpression;

    public WhereClause(PrimaryExpression wherePrimaryExpression) {
        this.wherePrimaryExpression = wherePrimaryExpression;
    }

    static char[] aqlParam = new char[1];

    // TODO: To Fix
    public Map<String, Dataset<Row>> evaluateWhereClause(Map<String, Dataset<Row>> varBindingVal,
            final String jsonPath) {
        Map<String, Dataset<Row>> varlResult = new LinkedHashMap<String, Dataset<Row>>();
        String param = wherePrimaryExpression.toString();
        if (param.contains("?"))
            param = param.replaceAll("\\?", new String(WhereClause.aqlParam));
        Dataset df = varBindingVal.get(wherePrimaryExpression.toString().substring(0, 2));
        df.registerTempTable("tempDF");
        Dataset dfTemp = SparkSQLConnection.sqlContext.sql("SELECT * FROM tempDF where tempDF" + param.substring(2));
        varBindingVal.put(wherePrimaryExpression.toString().substring(0, 2), dfTemp);
        varlResult.putAll(varBindingVal);
        return varlResult;
    }

    public void setAQLParams(char[] param) {
        WhereClause.aqlParam = param;
    }

    @Override
    public String toString() {
        return "WHERE " + wherePrimaryExpression;
    }
}
