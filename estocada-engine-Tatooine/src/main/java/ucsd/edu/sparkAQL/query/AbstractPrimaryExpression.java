/**
 * 
 */
package ucsd.edu.sparkAQL.query;

import org.apache.log4j.Logger;

public abstract class AbstractPrimaryExpression extends AbstractAQLQuery implements PrimaryExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(AbstractPrimaryExpression.class);

}
