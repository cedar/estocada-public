
package ucsd.edu.sparkAQL.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class OperatorFinder extends AbstractPrimaryExpression {
    @SuppressWarnings("unused")

    private static final Logger log = Logger.getLogger(OperatorFinder.class);

    private String opertaorName;

    private List<Object> operatorArguments;

    public OperatorFinder(String operatorName, List<Object> operatorArguments) {

        this.opertaorName = operatorName;
        this.operatorArguments = operatorArguments;
    }

    @Override
    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        List<Object> operatorOperands = new ArrayList<Object>();
        Dataset<Row> lhs = ((PrimaryExpression) operatorArguments.get(0)).evaluateExpression(varBindingVal, jsonPath);
        if ((PrimaryExpression) operatorArguments.get(0) instanceof ParenthesizedExpression) {
            RecordPathExpression.preNaviagteVar = ParenthesizedExpression.pathExpressionTag;
            RecordPathExpression.currNaviagteVar = operatorArguments.get(1).toString().replaceAll("\"", "");
        }
        operatorOperands.add(0, lhs);
        String rhs = operatorArguments.get(1).toString().replaceAll("\"", "");
        operatorOperands.add(1, rhs);
        Operator operator = OperatorMapping.get().find(opertaorName);
        Dataset<Row> df = operator.evaluateOperator(varBindingVal, operatorOperands);
        return df;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (opertaorName.equals("RecordPathExpression")) {
            boolean first = true;
            for (Object argument : operatorArguments) {

                if (!first)
                    sb.append(".");
                first = false;
                sb.append(argument.toString().replaceAll("\"", ""));
            }
        } else {
            if (opertaorName.equals("Equality")) {
                boolean first = true;
                for (Object argument : operatorArguments) {

                    if (!first)
                        sb.append("=");
                    first = false;
                    sb.append(argument.toString().replaceAll("\"", ""));
                }
            } else {
                boolean first = true;
                for (Object argument : operatorArguments) {

                    if (!first)
                        sb.append("[");
                    first = false;
                    sb.append(argument.toString().replaceAll("\"", ""));
                }
                sb.append("]");
            }

        }
        return sb.toString();

    }

    @Override
    public void setAQLParams(char[] param) {
        // TODO Auto-generated method stub

    }

    @Override
    public char[] getAQLParams() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reInitializeAQLParams() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasParam() {
        // TODO Auto-generated method stub
        return false;
    }
}
