package ucsd.edu.sparkAQL.query;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import ucsd.edu.sparkAQL.parser.AQLBuilder;
import ucsd.edu.sparkAQL.parser.AQLLexer;
import ucsd.edu.sparkAQL.parser.AQLParser;
import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

/**
 * XSparkSQL query Parser and Compiler.
 * 
 * @author ranaalotaibi
 * 
 */
public class AQLQueryParser {
	/**
	 * Parses and returns XSparkSQL Query as an instance of XSparkSQL Expression
	 * class
	 * 
	 * @param queryAsString
	 *            the query string.
	 * @return the corresponding XSparkSQL expression instance.
	 */
	public Expression parse(String queryAsString) {
		RecordPathExpression.preNaviagteVar = "";
		RecordPathExpression.currNaviagteVar = "";
		RecordConstructor.counterPrev = 0;
		WhereClause.aqlParam = null;
		SparkSQLConnection.Conn();
		Expression query = null;
		try {
			ANTLRInputStream input = new ANTLRInputStream(queryAsString);
			AQLLexer lexer = new AQLLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			AQLParser parser = new AQLParser(tokens);
			parser.removeErrorListeners();
			ParseTree tree = parser.aqlQuery();
			ParseTreeWalker walker = new ParseTreeWalker();
			AQLBuilder builder = new AQLBuilder();
			walker.walk(builder, tree);
			query = (Expression) builder.getQuery(tree);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error: " + e.getMessage());
		}
		return query;
	}

}
