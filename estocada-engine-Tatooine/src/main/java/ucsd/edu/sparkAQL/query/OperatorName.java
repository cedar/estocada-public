
package ucsd.edu.sparkAQL.query;

import java.util.List;

import org.apache.log4j.Logger;

public class OperatorName {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(OperatorName.class);

    private String operatorName;

    private List<Object> operatorArguments;

    public OperatorName(String operatorName, List<Object> operatorArguments) {

        this.operatorName = operatorName;
        this.operatorArguments = operatorArguments;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public List<Object> getParameters() {
        return operatorArguments;
    }
}
