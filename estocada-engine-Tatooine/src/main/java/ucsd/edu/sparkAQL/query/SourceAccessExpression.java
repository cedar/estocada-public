package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

/**
 * A SourceAccessExpression Implementation
 * 
 * @author ranaalotaibi
 *
 */
public class SourceAccessExpression extends AbstractPrimaryExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(SourceAccessExpression.class);

    private String sourceName;

    public SourceAccessExpression(String sourceName) {

        this.sourceName = sourceName;
    }

    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPathFile) {
        System.out.println(sourceName);
        return SparkSQLConnection.sqlContext.table(sourceName);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(sourceName);
        return sb.toString();
    }

    @Override
    public void setAQLParams(char[] param) {
        // TODO Auto-generated method stub

    }

    @Override
    public char[] getAQLParams() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reInitializeAQLParams() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasParam() {
        // TODO Auto-generated method stub
        return false;
    }

}
