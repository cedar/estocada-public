
package ucsd.edu.sparkAQL.query;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

public class ArrayPathExpression extends AbstractOperatorExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(RecordPathExpression.class);

    public static final OperatorName OPERATORNAME =
            new OperatorName("ArrayPathExpression", Arrays.<Object> asList(new Object(), new Object()));

    @Override
    public OperatorName getOperator() {
        return OPERATORNAME;
    }

    @Override
    public Dataset<Row> evaluateOperator(Map<String, Dataset<Row>> varBinding, List<Object> operatorParameters) {
        Dataset<Row> df = null;
        Dataset<Row> lhs = (Dataset) operatorParameters.get(0);
        lhs.registerTempTable("tempDF");
        df = SparkSQLConnection.sqlContext.sql("SELECT " + RecordPathExpression.preNaviagteVar + "["
                + operatorParameters.get(1) + "] AS " + RecordPathExpression.preNaviagteVar + "  FROM tempDF");
        return df;
    }
}
