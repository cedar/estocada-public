
package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class ReturnStatemnet extends AbstractReturnClause {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(ReturnStatemnet.class);

    private PrimaryExpression returnPrimaryExpression;

    public ReturnStatemnet(PrimaryExpression returnPrimaryExpression) {
        this.returnPrimaryExpression = returnPrimaryExpression;

    }

    @Override
    public Dataset<Row> evaluateReturnClause(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        Dataset<Row> df = returnPrimaryExpression.evaluateExpression(varBindingVal, jsonPath);
        return df;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RETURN ");
        sb.append(returnPrimaryExpression);
        return sb.toString();
    }
}
