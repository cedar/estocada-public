package ucsd.edu.sparkAQL.query;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

/**
 * A Record constructor.
 * 
 * @author ranaalotaibi
 * 
 */
public class RecordConstructor extends AbstractPrimaryExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(RecordConstructor.class);

    private Map<String, PrimaryExpression> recordConstructorPrimaryExp = new LinkedHashMap<String, PrimaryExpression>();
    static int counterPrev = 0;

    /**
     * Constructs a Record Constructor.
     */
    public RecordConstructor() {

    }

    /**
     * Sets an expr query.
     * 
     * @param attribute_name
     *            the attribute name.
     * @param expr_query
     *            the expr query.
     */
    public void setPrimaryExpression(String attrName, PrimaryExpression primaryExpression) {
        recordConstructorPrimaryExp.put(attrName, primaryExpression);
    }

    @Override
    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        int tableNum = 0;
        String[] names = null;
        Map<Integer, String> coulumnNames = new HashMap<Integer, String>();
        Dataset<Row> mainDF = null;
        int counter = counterPrev;
        int tableNumber = 0;
        int counterCol = counterPrev;
        for (Map.Entry<String, PrimaryExpression> e : recordConstructorPrimaryExp.entrySet()) {
            Dataset<Row> df = e.getValue().evaluateExpression(varBindingVal, jsonPath);
            df.registerTempTable("temp" + counter);
            names = df.columns();
            if (names.length == 1) {
                Dataset<Row> temp = SparkSQLConnection.sqlContext
                        .sql("SELECT " + names[0] + " as " + e.getKey().toString() + " FROM temp" + counter);
                Dataset<Row> tempId = temp.withColumn("id", org.apache.spark.sql.functions.monotonicallyIncreasingId());
                tempId.registerTempTable("temp" + counter);
                coulumnNames.put(counter, e.getKey().toString());
                tableNumber++;
                counter++;
                counterPrev++;
            } else {
                return df;
            }

        }
        tableNum = counter - 1;
        if (tableNumber == 1) {
            return SparkSQLConnection.sqlContext
                    .sql("SELECT " + coulumnNames.get((counter - 1)) + " FROM temp" + (counter - 1));
        }
        boolean first = true;
        StringBuilder sbtable = new StringBuilder();
        StringBuilder ids = new StringBuilder();
        StringBuilder colNames = new StringBuilder();
        while (counterCol <= tableNum) {
            if (!first) {
                sbtable.append(",");
                ids.append("=");
                colNames.append(",");
            }
            sbtable.append("temp" + counterCol);
            ids.append("temp" + counterCol + ".id");
            colNames.append(coulumnNames.get(counterCol));
            counterCol++;
            if (!first && !(counterCol > tableNum)) {
                ids.append(" and " + "temp" + (counterCol - 1) + ".id");
            }
            first = false;

        }
        mainDF = SparkSQLConnection.sqlContext
                .sql("SELECT " + colNames.toString() + " FROM " + sbtable.toString() + " WHERE " + ids.toString());

        return mainDF;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        boolean first = true;
        for (Map.Entry<String, PrimaryExpression> e : recordConstructorPrimaryExp.entrySet()) {
            if (!first)
                sb.append(", ");
            first = false;
            sb.append(e.getKey());
            sb.append(": ");
            sb.append(e.getValue());
        }
        sb.append(" }");
        return sb.toString();
    }

    @Override
    public void setAQLParams(char[] param) {
        // TODO Auto-generated method stub

    }

    @Override
    public char[] getAQLParams() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reInitializeAQLParams() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasParam() {
        // TODO Auto-generated method stub
        return false;
    }
}
