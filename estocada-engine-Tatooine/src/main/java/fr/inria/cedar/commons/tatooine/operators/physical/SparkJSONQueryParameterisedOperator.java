package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Row;

import com.google.common.base.Strings;

import scala.collection.Iterator;
import scala.collection.mutable.WrappedArray;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.DBSourceLayer;
import fr.inria.cedar.commons.tatooine.storage.database.JQSourceLayer;

/**
 * Physical operator that queries JSON data sources using Apache Spark SQL.
 * @author Oscar Mendoza
 */
public abstract class SparkJSONQueryParameterisedOperator extends QueryParameterisedOperator {

	/** Universal version identifier for the PhyJQOperator class */
	private static final long serialVersionUID = -7790918421016654141L;
	
	private static final Logger log = Logger.getLogger(SparkJSONQueryParameterisedOperator.class);
	
	/* Query parameters */
	protected GSR ref;
	protected NTuple params;
	protected String queryStr = "";
	protected String paramStr = "";
	
	// Interface that handles DB calls
	private DBSourceLayer source;	

	// In-memory queue of NTuples:
	// It is filled when empty and the user calls "hasNext()"
	// It is polled when the user calls "next()"
	private Queue<NTuple> tuples = null;
	
	// MD mismatch error msg
	private static final String mismatchNRSMDErrorMsg = "The value '%s' does not match the expected input NRSMD %s";
	
	protected static final String PLACEHOLDER = "%s";
	
	// String needed to escape queries with whitespaces 
	protected static final String REPLACEMENT_BLANK = "\\\\\\\\ ";

	public SparkJSONQueryParameterisedOperator(String query, GSR ref) {
		queryStr = query;
		source = new JQSourceLayer(ref);
		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		source.connect();
		paramStr = "";
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}
	
	/** Checks the query has been parameterised if needed */
	private String resolveQuery(String query) throws TatooineExecutionException {
		// Check for placeholders in parameterised queries
		if (queryStr.contains(PLACEHOLDER) && Strings.isNullOrEmpty(paramStr)) {
			String message = "Method hasNext() called with query that contains placeholders and hasn't been parameterised!";
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		return Strings.isNullOrEmpty(paramStr) ? queryStr : paramStr;
	}
	
	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// Checking for timeout
		if (timeout) {
			return false;
		}
		
		if (tuples == null) {
			// It's time to query Spark to fetch more results			
			String[] parameters = new String[] { resolveQuery(queryStr) };
			Object batch = source.getBatchRecords(parameters);
			if (batch == null) {
				return false;
			} else {
				// Transforming the Spark response into a list of NTuple objects
				tuples = transformResponse((Row[]) batch);
				return CollectionUtils.isEmpty(tuples) ? false : true;
			}
		} else if (tuples.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		numberOfTuples++;
		NTuple next = tuples.poll();
		return next;
	}
	
	
	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		source.close();
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
	}
	
	/** Transforms Spark data frames into NTuple objects */
	private Queue<NTuple> transformResponse(Row[] collection) throws TatooineExecutionException {
		Queue<NTuple> records = new ArrayDeque<NTuple>();
		for (Row row : collection) {
			NTuple tuple = new NTuple(nrsmd);
			for (int index = 0; index < nrsmd.types.length; index++) {
				String name = nrsmd.colNames[index];
				Object value =  row.get(row.fieldIndex(name));
				if (value == null) {
					String msg = String.format("The Spark response row has no [%s] field: %s", name, row);
					log.error(msg);
					throw new TatooineExecutionException(msg);
				}
				JsonType type = mapSparkTypeToJSONType(getSimpleClassName(value.getClass().getName()));
				tuple = addFieldToNTuple(tuple, nrsmd.types[index], type, nrsmd, index, name, value);
			}
			records.add(tuple);
		}
		

		return records;
	}
	
	/** Gradually fills in an NTuple object from a Spark response row */
	@SuppressWarnings("unchecked")
	private NTuple addFieldToNTuple(NTuple tuple, TupleMetadataType typeNRSMD, JsonType typeJSON, NRSMD nrsmd, int index, String name, Object obj)
			throws TatooineExecutionException {
		
		switch (typeNRSMD) {
		case STRING_TYPE:
			if (!typeJSON.equals(JsonType.STR)) {
				String msg = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(msg);
				throw new TatooineExecutionException(msg);
			}
			tuple.addString(String.valueOf(obj));
			break;
		case INTEGER_TYPE:
			if (!typeJSON.equals(JsonType.INT)) {
				String msg = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(msg);
				throw new TatooineExecutionException(msg);
			}
			tuple.addInteger(Integer.parseInt(String.valueOf(obj)));
			break;
		case TUPLE_TYPE:
			// Nestle tuple whose children are all of the same type
			if (typeJSON.equals(JsonType.SEQ)) {
				tuple.addNestedField(transformNestedTuple((WrappedArray<Object>) obj, nrsmd.getNestedChild(index), name));
			}
			// Nested tuple whose children are of different types
			else if (typeJSON.equals(JsonType.TUP)) {
				String msg = "Nested tuples with children of different types are not currently supported";
				log.error(msg);
				throw new TatooineExecutionException(msg);
			}
			else {
				String msg = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(msg);
				throw new TatooineExecutionException(msg);
			}
			break;
		default:
			String msg = String.format("PhyJQOperator only handles string, integer, and nested types");
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}

		return tuple;
	}
	
	/** Transforms a WrappedArray object into a List of NTuple objects */
	private List<NTuple> transformNestedTuple(WrappedArray<Object> array, NRSMD childNRSMD, String name) throws TatooineExecutionException {
		Iterator<Object> it = array.iterator();
		List<NTuple> children = new ArrayList<NTuple>();
		String childName = childNRSMD.colNames[0];
		TupleMetadataType childType = childNRSMD.types[0];
		
		int counter = 1;
		switch (childType) {
		case STRING_TYPE: 
			while (it.hasNext()) {
				Object cur = it.next();
				JsonType type = mapSparkTypeToJSONType(getSimpleClassName(cur.getClass().getName()));
				if (!type.equals(JsonType.STR)) {
					String msg = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
					log.error(msg);
					throw new TatooineExecutionException(msg);
				}
				NTuple child = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { childName + counter }, new NRSMD[0]));
				child.addString(String.valueOf(cur));
				children.add(child);
				counter++;
			}
			break;
		case INTEGER_TYPE:
			while (it.hasNext()) {
				Object cur = it.next();
				JsonType type = mapSparkTypeToJSONType(getSimpleClassName(cur.getClass().getName()));
				if (!type.equals(JsonType.INT)) {
					String msg = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
					log.error(msg);
					throw new TatooineExecutionException(msg);
				}
				NTuple child = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { childName + counter }, new NRSMD[0]));
				child.addInteger(Integer.parseInt(String.valueOf(cur)));
				children.add(child);
				counter++;
			}
			break;
		default:
			String msg = String.format("The PhyJQOperator currently supports only simply-nested tuples");
			log.error(msg);
			throw new UnsupportedOperationException(msg);
		}
		
		return children;
	}
	
	/** Enumerated type representing all supported JSON types */
	private enum JsonType {
		INT, STR, SEQ, TUP;
	}

	/** Maps Apache Spark types to supported JSON types */
	private JsonType mapSparkTypeToJSONType(String className) {
		switch (className) {
			case "WrappedArray":
				// Nested tuple (children are all of the same type)
				return JsonType.SEQ;
			case "GenericRowWithSchema":
				// Nested tuple (children can be of different types)
				return JsonType.TUP;
			case "Integer":
			case "Double":
			case "Float":
			case "Long":
			case "Short":
				return JsonType.INT;
			default:
				return JsonType.STR;
		}
	}
	
	/** Finds simple name of Spark classes */
	private String getSimpleClassName(String className) {
		className = className.substring(className.lastIndexOf('.') + 1);
		int index = className.indexOf('$');
		className = className.substring(0, index == -1 ? className.length() : index);
		return className;
	}
	
	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}
	
	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}
	
	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		this.params = params;
		paramStr = super.updateParameters(params, queryStr, PLACEHOLDER, REPLACEMENT_BLANK);
	}
	
	/* Getters and setters */
	public String getQuery() {
		return queryStr;
	}
	
	public void setQuery(String query) {
		queryStr = query;
	}
}
