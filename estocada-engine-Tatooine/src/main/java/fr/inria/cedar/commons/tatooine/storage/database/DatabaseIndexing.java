package fr.inria.cedar.commons.tatooine.storage.database;

import java.util.ArrayList;
import java.util.Iterator;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * Class that holds useful utility functions for the generation of index keys through XAMS with (R)equired fields.
 * @author Stamatis ZAMPETAKIS
 */
public class DatabaseIndexing {

	/**
	 * Out of a nested tuple and its corresponding xam node, fills in a ArrayList with index keys: first, extract the
	 * nested tuple with the necessary values, then, unnest it.
	 */
	public static ArrayList<NTuple> makeIndexKeys(NTuple nt, TreePatternNode pn) throws TatooineExecutionException {
		// extract nested index keys, by copying
		NTuple t2 = makeNestedKey(nt, pn, new traversalRecord());
		// logger.debug("##### Out of: ");
		// nt.display();
		// logger.debug("##### Extracted nested key:");
		// t2.display();
		ArrayList<NTuple> v = new ArrayList<NTuple>();
		t2.unnest(v);
		// logger.debug("##### Obtained " + v.size() + " unnested keys");
		return v;
	}

	/**
	 * Out of a nested tuple and its corresponding xam node, extracts another nested tuple that contains only the fields
	 * marked R in the xam.
	 */
	@SuppressWarnings("incomplete-switch")
	private static NTuple makeNestedKey(NTuple nt, TreePatternNode pn, traversalRecord tr)
			throws TatooineExecutionException {
		NTuple res = null;
		NRSMD localKeyRSMD = NRSMD.makeRequiredNRSMD(pn);

		// logger.debug("\nMaking nested key out of ");
		// nt.display();
		// logger.debug("... for node " + pn.tag + "\n");
		// logger.debug("Target tuple RSMD is: ");
		// localKeyRSMD.display();

		if (localKeyRSMD != null) {
			int iStringNT = tr.stringFrom;
			int iStringRes = 0;
			int iIDNT = tr.IDFrom;
			int iIDRes = 0;
			int iNestedNT = tr.nestedFrom;
			int iNestedRes = 0;

			res = new NTuple(localKeyRSMD);

			// logger.debug("Allocated tuple: ");
			// res.display();

			// first, take care of this node's contribution to this tuple

			// ID
			if (pn.requiresID()) {
				ElementID idAux = nt.idFields[iIDNT];
				res.idFields[iIDRes] = idAux;
				iIDRes++;
				iIDNT++;

				tr.nIDs++;
			} else {
				// advance iIDNT to skip this ID
				// logger.debug(pn.tag + " does not require ID");
				if (pn.storesID()) {
					iIDNT++;

					tr.nIDs++;
				}
			}

			// Tag
			if (pn.requiresTag()) {
				res.stringFields[iStringRes] = nt.stringFields[iStringNT];
				iStringRes++;
				iStringNT++;

				tr.nStrings++;
			} else {
				// advance iStringNT to skip this string
				if (pn.storesTag()) {
					iStringNT++;

					tr.nStrings++;
				}
			}

			// Value
			if (pn.requiresVal()) {
				// logger.debug(pn.tag + " requires value, adding it");
				res.stringFields[iStringRes] = nt.stringFields[iStringNT];
				iStringRes++;
				iStringNT++;

				tr.nStrings++;
			} else {
				// logger.debug(pn.tag + " does not require value");
				// advance iStringNT to skip this string
				if (pn.storesValue()) {
					// logger.debug(
					// pn.tag + " uses a value, advancing index");
					iStringNT++;

					tr.nStrings++;
				}
			}

			// Content
			if (pn.storesContent()) {
				iStringNT++;

				tr.nStrings++;
			}

			// next, take care of the unnested children of this node's
			// contribution to
			// this tuple, if any
			if (pn.getEdges() != null) {
				for (PatternEdge pe : pn.getEdges()) {
					TreePatternNode n2 = pe.n2;
					// if (n2.requiresSomething()){
					if (pe.isNested()) {
						if (n2.requiresSomething()) {
							// logger.debug(
							// "Considering nested child "
							// + n2.tag
							// + " requiring something");
							// gather the tuples for the nested child.
							ArrayList<NTuple> vThisChild = new ArrayList<NTuple>();
							Iterator<NTuple> itChild = nt.nestedFields[iNestedNT].iterator();
							while (itChild.hasNext()) {
								NTuple tChild = (NTuple) itChild.next();
								NTuple keyChild = makeNestedKey(tChild, n2, new traversalRecord());
								vThisChild.add(keyChild);
							}
							res.nestedFields[iNestedRes] = vThisChild;
							iNestedRes++;
						}
						// in any case, whether n2 requires something or not,
						// we crossed it
						iNestedNT++;
						tr.nNested++;
					} else {
						// logger.debug(
						// "Computing key from child "
						// + n2.tag
						// + " starting at "
						// + iStringNT
						// + " "
						// + iIDNT
						// + " "
						// + iNestedNT);

						traversalRecord tr2 = new traversalRecord();
						tr2.stringFrom = iStringNT;
						tr2.IDFrom = iIDNT;
						tr2.nestedFrom = iNestedNT;
						// this will mark in tr2 how many string/id/nested
						// fields have been traversed.

						NTuple tFromThisChild = makeNestedKey(nt, n2, tr2);

						if (tFromThisChild != null) {
							int iCopyStringFromChild = 0;
							int iCopyIDFromChild = 0;
							int iCopyNestedFromChild = 0;
							TupleMetadataType[] childTypes = tFromThisChild.nrsmd.getColumnsMetadata();
							for (int j2 = 0; j2 < childTypes.length; j2++) {
								switch (childTypes[j2]) {
								case STRING_TYPE:
									res.stringFields[iStringRes] = tFromThisChild.stringFields[iCopyStringFromChild];
									iStringRes++;
									iCopyStringFromChild++;
									break;
								case UPDATE_ID:
								case UNIQUE_ID:
								case ORDERED_ID:
								case STRUCTURAL_ID:
									res.idFields[iIDRes] = tFromThisChild.idFields[iCopyIDFromChild];
									iIDRes++;
									iCopyIDFromChild++;
									break;
								case TUPLE_TYPE:
									res.nestedFields[iNestedRes] = tFromThisChild.nestedFields[iCopyNestedFromChild];
									iCopyNestedFromChild++;
									iNestedRes++;
									break;
								}
							}
						}
						// in any case
						iStringNT += tr2.nStrings;
						iIDNT += tr2.nIDs;
						iNestedNT += tr2.nNested;
					}
				}
			}
		}
		// logger.debug("##### NESTED KEY TO RETURN: ");
		// res.display();
		return res;
	}

}

/**
 * utility class for recording how many Strings, IDs, and Nested fields at top level of a tuple have been traversed.
 * 
 * @author Ioana MANOLESCU
 */
class traversalRecord {

	// start looking from this point
	int stringFrom;
	int IDFrom;
	int nestedFrom;

	// record here how many Strings, IDs, nested fields (at top level) have
	// been traversed.
	int nStrings;
	int nIDs;
	int nNested;

	traversalRecord() {
		nStrings = 0;
		nIDs = 0;
		nNested = 0;
	}

}
