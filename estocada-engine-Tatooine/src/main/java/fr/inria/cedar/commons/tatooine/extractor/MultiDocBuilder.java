package fr.inria.cedar.commons.tatooine.extractor;

import java.io.BufferedOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.CompactDynamicDeweyElementID;
import fr.inria.cedar.commons.tatooine.IDs.ElementIDFactory;
import fr.inria.cedar.commons.tatooine.IDs.IDScheme;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.MultiDocLoader;
import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * This class collects the tuple building functionality from the extractor, in
 * this case, NTuple building.
 *
 * The class is called by the loader.multidoc.MultiDocLoader.
 *
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 */
public class MultiDocBuilder implements TupleBuilder {
	private static final Logger log = Logger.getLogger(MultiDocBuilder.class);
	/* Constant */
	private final boolean STORE_DERIVATION_COUNT = Boolean.parseBoolean(Parameters.getProperty("updatesAlgorithm.storeDerivationCount"));

	private HashMap<TreePatternNode, NRSMD> strictNRSMDs;

	public HashMap<TreePatternNode, ExtractorMatchStack> stacksByNodes;
	public HashMap<TreePatternNode, IDScheme> schemesByNodes;

	boolean thisIsFirstAttribute;

	NRSMD currentTupleNRSMD;
	NRSMD nestedKeyNRSMD;
	NRSMD flatKeyNRSMD;

	/**
	 * for the case when the data is unordered and we need to make up integer tuple keys
	 */
	NRSMD integerKeyRSMD;
	NTuple integerKeyTuple;

	private MultiDocLoader myLoader;

	public MultiDocBuilder() {
		thisIsFirstAttribute = true;
		this.currentTupleNRSMD = null;
		this.nestedKeyNRSMD = null;
		this.flatKeyNRSMD = null;

		int colNo = 1;
		TupleMetadataType[] types = new TupleMetadataType[1];
		types[0] = TupleMetadataType.UNIQUE_ID;
		try{
			integerKeyRSMD = new NRSMD(colNo, types);
		}
		catch(Exception e){
			log.error("Exception: ", e);
		}
		strictNRSMDs = new HashMap<TreePatternNode, NRSMD>();
	}

	/* Public methods implemented from TuplesBuilder interface */

	@Override
	public final void produceTuples(ExtractorMatch em, TreePatternNode pn, BufferedOutputStream bos, ArrayList<NTuple> v, TreePattern p, int i){
		this.produceTuples(em, pn, i);
	}

	@Override
	public final void produceTuples(ExtractorMatch em, TreePatternNode pn, BufferedOutputStream bos, ArrayList<NTuple> v, TreePattern p) {
		this.produceTuples(em, pn);
	}

	@Override
	public final void setStacksByNodes(HashMap<TreePatternNode, ExtractorMatchStack> hm) {
		this.stacksByNodes = hm;
	}

	@Override
	public final void setSchemesByNodes(HashMap<TreePatternNode, IDScheme> hm) {
		this.schemesByNodes = hm;
	}

	public void setLoader(MultiDocLoader l){
		this.myLoader = l;
	}

	/*
	 * This method knows it is working for a multi-xam loader,
	 * therefore it has to put tuples at the i-th position in the extractedMultiData of that xam.
	 *
	 * @param em
	 * @param pn
	 * @param i
	 */
	private final void produceTuples(ExtractorMatch em, TreePatternNode pn, int i) {
		thisIsFirstAttribute = true;
		try {
			ArrayList<NTuple> v2 = build(em, pn);
			// log.debug("Obtained " + v2.size() + " tuples");
			Iterator<NTuple> it = v2.iterator();
			while (it.hasNext()) {
				NTuple tuple = it.next();
				// log.info("PRODUCED TUPLE! ");
				//tuple.display();
				// erase null tuples introduced for some dummy purposes
				tuple.clean();
				myLoader.extractedMultiData.get(i).add(tuple);
			}
		} catch (Exception e) {
			log.error("Cannot produce tuples. Either: the thread I was runing into was killed or I ran out of memory."
					+ e);
		}
	}

	/*
	 * This does two things:
	 * - producing tuples
	 * - ***** transmitting the tuples to the loader *****. This is something the simple NestedBuilder does not (it just stores).
	 *
	 * No storage.
	 *
	 * @param em
	 * @param pn
	 */
	private final void produceTuples(ExtractorMatch em, TreePatternNode pn) {
		// log.info("PRODUCING TUPLES...");
		thisIsFirstAttribute = true;

		try {
			ArrayList<NTuple> v2 = build(em, pn);
			// log.debug("Obtained " + v2.size() + " tuples");
			Iterator<NTuple> it = v2.iterator();
			while (it.hasNext()) {
				NTuple tuple = it.next();

				// log.info("PRODUCED TUPLE: ");
				//tuple.display();

				// erase null tuples introduced for some dummy purposes
				tuple.clean();

				if (myLoader.extractedData == null) {
					myLoader.extractedData = new ArrayList<NTuple>();
				}
				myLoader.extractedData.add(tuple);
				// we would like to add to myLoader.extractedMultiData[i], but for this we would need to know which is i
				// (for which number we work).
			}
		} catch (Exception e) {
			log.error("Cannot produce tuples. Exception: ", e);
		}
	}

	/*
	 * Returns a vector of NTuples corresponding to the matches of em for this
	 * xam node.
	 *
	 * @param em
	 * @param pn
	 * @return
	 */
	private final ArrayList<NTuple> build(ExtractorMatch em, TreePatternNode pn) throws TatooineException, TatooineExecutionException {
		LinkedHashMap<NTuple, Integer> v = recBuild(em, pn);

		if(!STORE_DERIVATION_COUNT) {
			return new ArrayList<NTuple>(v.keySet());
		} else {
			NRSMD nrsmdDerivationCount = new NRSMD(1, new TupleMetadataType[]{TupleMetadataType.INTEGER_TYPE});
			NTuple tupleDerivationCount = new NTuple(nrsmdDerivationCount);

			ArrayList<NTuple> res = new ArrayList<NTuple>();
			for (NTuple nt: v.keySet()) {
				tupleDerivationCount.setFirstIntegerField(v.get(nt));

				res.add(NTuple.append(NRSMD.appendNRSMD(nrsmdDerivationCount, nt.nrsmd), tupleDerivationCount, nt));
			}



			return res;
		}
	}

	/**
	 * Returns an array with two elements: the first element is a list of NTuples
	 * corresponding to the matches of em for this xam node, and the second element
	 * is a list of integers that represent the derivation count for each tuple
	 * ("number of reasons why the tuple belongs to the view").
	 *
	 * @param em
	 * @param pn
	 * @return
	 */
	//return an Object[]: first the ArrayList of NTuple and then the ArrayList of Integer
	@SuppressWarnings("unchecked")
	private final LinkedHashMap<NTuple, Integer> recBuild(ExtractorMatch em, TreePatternNode pn) throws TatooineException, TatooineExecutionException {
		// log.debug("\nBuilding from ");
		//em.displayTree();

		// log.info("Called build on a match for " + em.tag);

		LinkedHashMap<NTuple, Integer> res = new LinkedHashMap<NTuple, Integer>();

		NRSMD nrsmdRoot = this.strictNRSMDs.get(pn);
		if (nrsmdRoot == null){
			nrsmdRoot = NRSMD.getStrictNRSMD(pn);
			this.strictNRSMDs.put(pn, nrsmdRoot);
		}

		NTuple tupleRoot = new NTuple(nrsmdRoot);

		// log.debug("Adding atomic attributes from " + em.no + " for " +
		// pn.tag);
		// log.debug("Adding to: ");
		//tupleRoot.display();

		addAtomicAttributes(tupleRoot, em, pn);

		if (pn.getEdges() == null) {
			res.put(tupleRoot, 1);
			// log.debug("End of build 1");
			return res;
		}
		if (pn.getEdges().size() == 0) {
			res.put(tupleRoot, 1);
			// log.debug("End of build 2");
			return res;
		}
		// there are children

		int iChild = -1;

		ArrayList<LinkedHashMap<NTuple, Integer>>[] vChildren = new ArrayList[pn.getEdges().size()];

		boolean[] contributing = new boolean[pn.getEdges().size()];
		int childrenContributing = 0;

		boolean[] nesteds = new boolean[pn.getEdges().size()];
		for (int i = 0; i < pn.getEdges().size(); i++) {
			vChildren[i] = new ArrayList<LinkedHashMap<NTuple, Integer>>();
			nesteds[i] = false;
			contributing[i] = false;
		}

		for(PatternEdge pe: pn.getEdges()) {
			iChild++;

			if (pe.isNested()) {
				nesteds[iChild] = true;
			}
			TreePatternNode child = pe.n2;
			// log.debug("\nFrom " + pn.tag + " going to child " + child.tag);

			ExtractorMatchStack sChild = (this.stacksByNodes.get(child));

			ArrayList<ExtractorMatch> childMatches = em.childrenByStack.get(sChild);

			if (childMatches != null) {
				// log.debug("In " + pn.tag + " there are " +
				// childMatches.size() + " matches for " + child.tag);
				Iterator<ExtractorMatch> thisChildMatches = childMatches.iterator();
				while (thisChildMatches.hasNext()) {
					ExtractorMatch emChild = thisChildMatches.next();
					if (emChild.erased) {
						continue;
					}

					// vector of tuples and numbers resulting from this match for this
					// child:
					LinkedHashMap<NTuple, Integer> vAux = recBuild(emChild, child);
					// log.debug(vAux.size() + " tuples from this match of " +
					// child.tag);

					// collect this vector in vChildren[i]
					// feb 2010:
					vChildren[iChild].add(vAux);

					// log.debug("In " + pn.tag + " added for " + iChild +
					//		"-th child node a vector of " + vAux.size());
				}
			} else {
				// log.debug("In " + pn.tag + " there are no matches for " +
				// child.tag);

				// no matches for this child
				LinkedHashMap<NTuple, Integer> vChild = new LinkedHashMap<NTuple, Integer>();

				// in any case, collect the contribution of this child
				vChildren[iChild].add(vChild);
				// log.debug("In " + pn.tag + " for child " + child.tag +
				//		" got one vector");
			}
			if (vChildren[iChild].size() > 0 && child.deepStoresSomething()) {
				contributing[iChild] = true;
				// log.debug("Child number " + iChild + " contributes");
				childrenContributing++;
			}
		}
		// log.debug("\nOut of children enumeration loop for " + pn.tag);
		if (childrenContributing > 0) {
			// log.debug("Going for children");
			return cartProd(tupleRoot, vChildren, nesteds, contributing, vChildren.length);
		} else {
			// no child brings anything -- e.g. they are all semijoin children.
			// Yet there are edges.
			// in this case at least add the root tuple.
			int count = 1;
			for (ArrayList<LinkedHashMap<NTuple, Integer>> element : vChildren) {
				int sum = 0;
				for(LinkedHashMap<NTuple, Integer> map: element) {
					sum += map.values().iterator().next();
				}
				count *= sum;
			}

			res.put(tupleRoot, count);
			return res;
		}
		// log.debug("End of build 3");
		// log.info("res.size: " + res.size());
	}

	// **************************************************************************
	// *** From here downwards, there are just utilities for building tuples. ***
	// **************************************************************************

	/**
	 * OrderUtility for building tuples.
	 *
	 * @param rootTuple
	 * @param vChildren
	 * @param vChildrenCount
	 * @param nesteds
	 * @param contributing
	 * @param n
	 * @return
	 * @throws TatooineExecutionException
	 */
	private final LinkedHashMap<NTuple, Integer> cartProd(NTuple rootTuple, ArrayList<LinkedHashMap<NTuple, Integer>>[] vChildren, boolean[] nesteds,
			boolean[] contributing, int n) throws TatooineExecutionException {
		// log.info("Called cart prod with n="+n);

		LinkedHashMap<NTuple, Integer> v = new LinkedHashMap<NTuple, Integer>();
		if (n == 0) {
			// log.debug("0 length !");
			v.put(rootTuple, 1);
			// log.debug("Returning vector with only:");
			//rootTuple.display();
			return v;
		} else {
			LinkedHashMap<NTuple, Integer> allButLast = cartProd(rootTuple, vChildren, nesteds, contributing, n - 1);

			if (!contributing[n - 1]) {
				// log.debug("Child at " + (n-1) + " not contributing");
				int sum = 0;
				for (LinkedHashMap<NTuple, Integer> v2: vChildren[n-1]) {
					sum += v2.values().iterator().next();
				}

				LinkedHashMap<NTuple, Integer> newAllButLast = new LinkedHashMap<NTuple, Integer>();
				for (NTuple tuple: allButLast.keySet()) {
					newAllButLast.put(tuple, allButLast.get(tuple) * sum);
				}

				return newAllButLast;
			}

			if (nesteds[n - 1]) {
				// log.debug("In cartProd(" + n + "), nested attribute at " +
				// (n-1));
				// this child is nested
				// in any case, vChildren[0] contains many ArrayLists
				// all vectors must become one
				NRSMD newNRSMD = null;
				boolean NRSMDOK = false;

				for (NTuple nt: allButLast.keySet()) {
					int firstFactor = allButLast.get(nt);
					// log.debug("Previous tuple: ");
					//nt.display();
					// log.debug("\n");

					ArrayList<NTuple> nestedField = new ArrayList<NTuple>();
					int sum = 0;
					for (LinkedHashMap<NTuple, Integer> v2: vChildren[n-1]) {
						for (NTuple nt2: v2.keySet()) {
							sum += v2.get(nt2);
							// log.debug("Last child brought: ");
							//nt2.display();
							if (!NRSMDOK) {
								newNRSMD = NRSMD.addNestedField(nt.nrsmd, nt2.nrsmd);
							}
							nestedField.add(nt2);
						}
					}
					// continue, fix NRSMD nt.addNestedField(nestedField);

					// log.debug("Resulting enriched tuple: ");
					NTuple newTuple = NTuple.nestField(newNRSMD, nt, nestedField);
					//newTuple.display();
					v.put(newTuple, firstFactor * sum);
				}
			} else {
				// log.debug("In cartProd(" + n + "), unnested attribute at " +
				// (n-1));
				NRSMD cartProdRSMD = null;
				boolean NRSMDOK = false;

				for (NTuple tPrevious: allButLast.keySet()) {
					int firstFactor = allButLast.get(tPrevious);
					// log.debug("At top level, " + vChildren[n-1].size() +
					// " vectors");
					for (LinkedHashMap<NTuple, Integer> v2: vChildren[n-1]) {
						// log.debug("Internally, " + v2.size() + " vectors");
						for (NTuple nt2: v2.keySet()) {
							int secondFactor = v2.get(nt2);
							// what we want to do here is to compute the concatenated tuple if
							// possible. If one of the sides is the null tuple, this is a stand-in
							// for the empty list. In this case, just return the other tuple.
							NTuple appendedTuple;
							int derivationCount;
							if (tPrevious.isNull) {
								appendedTuple = nt2;
								derivationCount = secondFactor;
							} else if (nt2.isNull) {
								appendedTuple = tPrevious;
								derivationCount = firstFactor;
							} else {
								if (!NRSMDOK) {
									cartProdRSMD = NRSMD.appendNRSMD(tPrevious.nrsmd, nt2.nrsmd);
									NRSMDOK = true;
								}

								derivationCount = firstFactor * secondFactor;

								appendedTuple =	NTuple.append(cartProdRSMD,	tPrevious, nt2);
								appendedTuple.setFirstIntegerField(derivationCount);
							}
							v.put(appendedTuple, derivationCount);
						}
					}
				}
			}
		}
		return v;
	}

	/*
	 * OrderUtility for building tuples.
	 *
	 * This method builds a vector with all atomic attributes corresponding to
	 * a match em for a xam node pn. This can mean one or more of the
	 * following: ID, Tag, Value, and Content. These attributes are by
	 * definition siblings at the same tuple level. They are not nested.
	 *
	 * @param em
	 *            The match which contains the data.
	 * @param pn
	 *            The xam node which gives the information that we should
	 *            extract.
	 * @return
	 */
	private final void addAtomicAttributes(NTuple tuple, ExtractorMatch em, TreePatternNode pn) {
		if (pn.storesID()) {
			if (em != null) {
				tuple.addID(em.id);
				// log.debug("Added |" + em.id.toString() + "|");
			} else {
				if (pn.isOrderIDType()) {
					tuple.addID(OrderedIntegerElementID.theNull);
				} else if (pn.isUpdateIDType()) {
					tuple.addID(CompactDynamicDeweyElementID.theNull);
				} else if (pn.isStructIDType()) {
					tuple.addID(ElementIDFactory.getElementID(-1, -1));
				}
				// log.debug("Added null");
			}
		} else {
			// log.debug(pn.tag + " DOES NOT NEED ID");
		}

		// log.debug("Node " + pn.toString() + " " + pn.tag + " needs tag: " +
		// pn.needsTag + " and selects: " +
		//	pn.selectOnTag);
		if (pn.storesTag()) {
			if (em != null) {
				tuple.addString(em.tag);
			} else {
				tuple.addString(TupleMetadataType.NULL.toString());
			}
		} else {
			// log.debug(pn.tag + " DOES NOT NEED TAG");
		}

		if (pn.storesValue()) {
			if (em != null) {
				String emv = em.getVal();
				if (emv != null) {
					tuple.addString(emv);
				} else {
					tuple.addString(TupleMetadataType.NULL.toString());
				}
			} else {
				tuple.addString(TupleMetadataType.NULL.toString());
			}
		}
		if (pn.storesContent()) {
			if (em != null) {
				String emc = em.getContent();
				if (emc != null) {
					tuple.addString(emc);
				} else {
					tuple.addString(TupleMetadataType.NULL.toString());
				}
			}
		}
	}
}


