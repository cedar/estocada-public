package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical SQL query operator.
 * 
 * @author Romain Primet
 */
public class LogSQLEval extends LogLeafOperator {

    private final String query;

    public LogSQLEval(String query, NRSMD meta) {
        this.query = query;
        this.setOwnName("LogSQLEval");
        this.setVisible(true);
        this.setNRSMD(meta);
    }

    public LogSQLEval(String query, NRSMD meta, StorageReference sref) {
        this.query = query;
        this.ref = sref;
        this.setOwnName("LogSQLEval");
        this.setVisible(true);
        this.setNRSMD(meta);
    }

    @Override
    public int getJoinDepth() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public String getName() {
        return "LogSQLEval";
    }

    @Override
    public String toString() {
        return "[LogSqlEval query: " + query + "]";
    }

    @Override
    public void accept(LogOperatorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public LogOperator deepCopy() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedCardinality() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedIOCost() {
        throw new UnsupportedOperationException("Unsupported");
    }

    public String getQueryString() {
        return query;
    }

    public StorageReference getStorageReference() {
        return ref;
    }
}
