package fr.inria.cedar.commons.tatooine.predicates;

import java.util.ArrayList;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Abstract class that any predicate should extend.
 * 
 * @author Ioana MANOLESCU
 */
public abstract class Predicate implements Cloneable {
	
	public abstract boolean isTrue(NTuple t) throws TatooineExecutionException;
	
	public abstract String toString();
	
	// For getting string representation of predicates separated by commas as in Plan file grammar
	public abstract String getName();
	
	public abstract Object clone();
	
	/**
	 * Adds an integer offset to the columns to which a predicate refers
	 * @author Konstantinos KARANASOS
	 */
	public static void addOffsetToPredicate(Predicate pred, int offset) {
		if (pred instanceof SimplePredicate) {
			((SimplePredicate) pred).column += offset;
			if (((SimplePredicate) pred).onJoin)
				((SimplePredicate) pred).otherColumn += offset;
		}
		else if (pred instanceof ConjunctivePredicate)
			for (int i = 0; i < ((ConjunctivePredicate) pred).preds.length; i++)
				addOffsetToPredicate(((ConjunctivePredicate) pred).preds[i], offset);
	}
	
	/**
	 * Gets a Predicate (either Simple or Conjunctive) and a SimplePredicates list, extracts the 
	 * SimplePredicates from the Predicate and adds them to the list
	 * @author Konstantinos KARANASOS
	 */
	public static void addPredToSimplePredList(Predicate pred, ArrayList<SimplePredicate> simplePredsList) {
		if (pred instanceof SimplePredicate)
			simplePredsList.add((SimplePredicate) pred);
		else if (pred instanceof ConjunctivePredicate)
			for (int i = 0; i < ((ConjunctivePredicate) pred).preds.length; i++)
				addPredToSimplePredList(((ConjunctivePredicate) pred).preds[i], simplePredsList);
	}
}
