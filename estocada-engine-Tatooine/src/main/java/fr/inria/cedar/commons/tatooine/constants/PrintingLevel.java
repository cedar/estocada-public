package fr.inria.cedar.commons.tatooine.constants;

/**
 * Enumerated type representing the possible printing level for tree patterns.
 * 
 * @author Jesus CAMACHO RODRIGUEZ
 * @created 16/12/2010
 */
public enum PrintingLevel {
	SIMPLIFY, STRUCTURE_ONLY, EXTENDED;
}
