package fr.inria.cedar.commons.tatooine.loader;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.util.Pair;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Created by DucCao on 17/10/16.
 * Base class of DatabaseLoader which can query its metadata about data type of each field/column
 */
public abstract class FieldMapDatabaseLoader extends DatabaseLoader {
    /**
     * @return mapping between name of column/field with its data type
     */
    public abstract Map<String, Pair<TupleMetadataType, TupleMetadataType>> getFieldMap();

    /**
     * @param dataCollectionName
     * @return ViewSchema of the given data collection
     * @throws SolrServerException
     * @throws IOException
     * @throws TatooineExecutionException
     */
    protected ViewSchema findMetadata(String dataCollectionName)
            throws SolrServerException, IOException, TatooineExecutionException {
        Map<String, Pair<TupleMetadataType, TupleMetadataType>> fieldMap = getFieldMap();

        // build ViewSchema
        Set<String> keySet = new TreeSet<>(fieldMap.keySet());
        int numOfColumns = keySet.size();
        TupleMetadataType[] types = new TupleMetadataType[numOfColumns];
        String[] colNames = new String[numOfColumns];
        List<NRSMD> nestedNrsmds = new LinkedList<>();
        int index = 0;
        for (String key : keySet) {
            colNames[index] = key;

            Pair<TupleMetadataType, TupleMetadataType> pair = fieldMap.get(key);
            TupleMetadataType tupleMetadataType = pair.getKey();
            types[index] = tupleMetadataType;
            if (tupleMetadataType.equals(TupleMetadataType.TUPLE_TYPE)) {
                NRSMD child =
                        new NRSMD(1, new TupleMetadataType[] { pair.getValue() }, new String[] { key }, new NRSMD[0]);
                nestedNrsmds.add(child);
            }

            index++;
        }

        NRSMD nrsmd = new NRSMD(numOfColumns, types, colNames,
                nestedNrsmds.size() == 0 ? new NRSMD[0] : nestedNrsmds.toArray(new NRSMD[] {}));

        Map<String, Integer> colNameToIndexMap = new HashMap<>();
        for (int i = 0; i < colNames.length; ++i) {
            colNameToIndexMap.put(colNames[i], i);
        }

        return new ViewSchema(nrsmd, colNameToIndexMap);
    }
}
