package fr.inria.cedar.commons.tatooine.xam.xparser;

import java.io.FileReader;
import java.io.StringReader;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * Class with all the utilities that we can use for parsing {@link TreePattern}s
 * and {@link JoinedPattern}s from files or string objects and vice versa.
 *
 * @author Andrei ARION
 * @author Konstantinos KARANASOS
 * @author Jesus CAMACHO RODRIGUEZ
 */
public class XAMParserUtility {
	private static final Logger log = Logger.getLogger(XAMParserUtility.class);
	
	/** Get a string that has the exact content of the file from which the given TreePattern would have come from */
	public static String getParsableStringFromTreePattern(TreePattern p) {
		String nodes = "" ;
		String edges = "" ;
		String file = "" ;

		LinkedList<TreePatternNode> nodesList = p.getNodes();

		try {
			for (int j = 0; j < p.getNodesNo(); j++) {
				TreePatternNode dummy = nodesList.get(j);

				if (dummy.isAttribute()) {
					nodes += "A: ";
				} else {
					nodes += "E: ";
				}
				nodes += dummy.getNodeCode();

				if (dummy.storesID()) {
					nodes += " ID ";
					if(dummy.isIdentityIDType()) {
						nodes += "i";
					} else if(dummy.isOrderIDType()) {
						nodes += "o";
					} else if(dummy.isStructIDType()) {
						nodes += "s";
					} else if(dummy.isUpdateIDType()) {
						nodes += "u";
					}
				}

				if (dummy.requiresID()) {
					nodes += " R ";
				}
				if (dummy.selectsTag()) {
					if(dummy.getNamespace().compareTo("") != 0) {
						nodes += " [Tag=\"{" + dummy.getNamespace() + "}" + dummy.getTag() + "\"]";
					} else {
						nodes += " [Tag=\"" + dummy.getTag() + "\"]";
					}
				}
				if (dummy.storesTag()) {
					nodes += " Tag";
				}

				if (dummy.requiresTag()) {
					nodes += " R ";
				}

				if (dummy.selectsValue()) {
					nodes += " [Val=\"" + dummy.getValue() + "\"]";
				}
				if (dummy.storesValue()) {
					nodes += " Val";
				}

				if (dummy.requiresVal()) {
					nodes += " R ";
				}
				if (dummy.storesContent()) {
					nodes += " Cont";
				}

				nodes += "\n";

				for (int k = 0; k < dummy.getEdges().size(); k++) {
					PatternEdge edge = dummy.getEdges().get(k);
					edges += edge.n1.getNodeCode() + "," + edge.n2.getNodeCode();
					edges += (edge.isParent()) ? " / " : " // ";
					edges += edge.isNested() ? "n" : "";
					edges += "j";
					edges += "\n";

				}
			}
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		file = (p.isOrdered() ? "o " : " ")
				+ (p.getRoot().getEdges().get(0).isParent() ? "/\n" : "\n") + nodes + ";\n" + edges;
		return file;
	}

	/** Tries to parse a file containing a tree pattern */
	public static TreePattern getTreePatternFromFile(String tPatFileName) throws Exception {
		// Parse the query: For now the translation from XQuery is not done, so the query is already a XAM file
		XamParserVJ parser = new XamParserVJ(new FileReader(tPatFileName));

		// Parsing the XAMs of the file
		ASTStart st = parser.Start(tPatFileName);
		XamParserVJVisitor v = new XamParserVJVisitorImpl();

		TreePattern jPat = null;
		try {
			jPat = (TreePattern) st.jjtAccept(v, null);
		}
		catch(Exception e) {
			log.error("Exception: ", e);
		}
		
		// Give names to the JoinedPattern and the included Patterns
		String jPatName = tPatFileName.substring( tPatFileName.lastIndexOf("/") + 1 );
		jPat.setName(jPatName);

		return jPat;
	}

	/** Trying to parse a tree pattern back without going through a disk-resident file */
	public static TreePattern getTreePatternFromString(String tPatString, String tPatFileName)
			throws Exception {
		// parse the query - for now the translation from XQuery is not done,
		// so the query is already a XAM file
		XamParserVJ parser = new XamParserVJ(new StringReader(tPatString));

		//parsing the xams of the file
		ASTStart st = parser.Start(tPatFileName);
		XamParserVJVisitor v = new XamParserVJVisitorImpl();

		TreePattern jPat = null;
		try{
			jPat = (TreePattern) st.jjtAccept(v, null);
		}
		catch(Exception e){
			log.error("Exception: ", e);
		}

		//give names to the JoinedPattern and the included Patterns
		String jPatName = tPatFileName.substring( tPatFileName.lastIndexOf("/") + 1 );
		jPat.setName(jPatName);

		return jPat;
	}
}
