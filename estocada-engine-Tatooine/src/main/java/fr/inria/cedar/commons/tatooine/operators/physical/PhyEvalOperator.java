package fr.inria.cedar.commons.tatooine.operators.physical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Physical operator that accesses data from a given data source and returns results that may NOT preserve the metadata associated with it.
 * This means that the resulting metadata is determined by each query used as input. 
 * @author Oscar Mendoza
 */
public interface PhyEvalOperator {

	/** Infers the metadata of the results of a query over a data source with a given input metadata */
	public abstract NRSMD inferMetadata(NRSMD inputMD, String queryParams) throws TatooineExecutionException;
	public static final String MSG_USE_SCAN = "Eval operators are meant for queries that do NOT preserve the metadata of the data source they're querying; for queries that DO preserve it, use Scan operators";
	
}
