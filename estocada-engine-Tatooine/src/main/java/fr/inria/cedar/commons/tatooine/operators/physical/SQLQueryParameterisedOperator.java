package fr.inria.cedar.commons.tatooine.operators.physical;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;

/**
 * Physical SQL (through JDBC) query operator.
 * 
 * @author Romain Primet
 * @author Oscar Mendoza
 */
public abstract class SQLQueryParameterisedOperator extends QueryParameterisedOperator {

    /** Universal version identifier for the PhySQLOperator class */
    private static final long serialVersionUID = 8654358043466128435L;

    private static final Logger log = Logger.getLogger(SQLQueryParameterisedOperator.class);

    // Metadata
    protected ViewSchema schema;

    // Connection information
    protected StorageReference ref;

    // Query and parameters
    protected String queryStr;
    protected Connection connection;
    protected PreparedStatement stmt;
    protected ResultSet rs;
    protected NTuple current = null;
    protected NTuple params;

    /* Constructor */
    public SQLQueryParameterisedOperator(String query, StorageReference ref, NRSMD nrsmd) {
        super();
        queryStr = query;
        this.ref = ref;
    }

    /* Constructor */
    public SQLQueryParameterisedOperator(String query, StorageReference ref, ViewSchema schema) {
        super();
        queryStr = query;
        this.ref = ref;
    }

    @Override
    public void open() throws TatooineExecutionException {
        System.out.println("*******" + queryStr);
        try {
            connection = DriverManager.getConnection(ref.getPropertyValue("url"));
            //needed for setFetchSize
            connection.setAutoCommit(false);
            stmt = connection.prepareStatement(queryStr);
            stmt.setFetchSize(1000000);
        } catch (Exception ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        try {
            if (rs == null) {
                rs = stmt.executeQuery();
            }
            if (current != null) {
                return true;
            } else {
                rs.next();
                current = toNTuple();
                if (isNullOrEmpty(current)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (SQLException ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    private boolean isNullOrEmpty(NTuple tuple) {
        if (tuple == null) {
            return true;
        } else if ((tuple.iID == 0) && (tuple.iInteger == 0) && (tuple.iString == 0) && (tuple.iUri == 0)
                && (tuple.iNested == 0)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        if (rs == null) {
            String msg = "Error while evaluating query: uninitialized result set";
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
        NTuple tuple = current;
        current = null;
        return tuple;
    }

    @Override
    public void close() throws TatooineExecutionException {
        try {
            if (rs != null) {
                rs.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        String msg = "Relational queries do not support nesting";
        log.error(msg);
        throw new TatooineExecutionException(msg);
    }

    @Override
    public void initialize() throws TatooineExecutionException {
        try {
            rs.beforeFirst();
        } catch (SQLException ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Adds the code for the graphical representation to the StringBuffer.
     *
     * @author Maxime Buron
     */
    @Override
    public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        String display = this.queryStr.trim().replaceAll(" (select|SELECT)", "\n$1").replaceAll("(union|UNION)", "\n$1")
                .replaceAll("(from|FROM)", "\n$1").replaceAll("(where|WHERE)", "\n$1");

        if (display.length() > 3000) {
            display = display.substring(0, 3000);
            display += "...";
        }

        sb.append(firstAvailableNo + " [label=\"SQLQuery\n" + display + "\", color = " + getColoring() + "] ; \n");

        if (parentNo != -1) {
            sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
        }

        return firstAvailableNo + 1;
    }

    @Override
    public void setParameters(NTuple params) throws IllegalArgumentException {
        this.params = params;
        try {
            checkParameters();
        } catch (TatooineExecutionException e) {
            log.error("Invalid parameter count");
            e.printStackTrace();
        }
        try {
            fillParameters();
        } catch (TatooineExecutionException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        }
    }

    private void checkParameters() throws TatooineExecutionException {
        try {
            int queryParamCount = stmt.getParameterMetaData().getParameterCount();
            if (queryParamCount != params.nrsmd.colNo) {
                String msg = String.format("Invalid parameter count: expected %s, got %s", queryParamCount,
                        params.nrsmd.colNo);
                throw new TatooineExecutionException(msg);
            }
        } catch (SQLException ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    /** Replaces placeholders in parameterised queries with actual parameters */
    private void fillParameters() throws TatooineExecutionException {
        try {
            final ParameterMetaData pmd = stmt.getParameterMetaData();
            // Note that parameters in prepared statements are 1-indexed :(
            for (int i = 1; i <= pmd.getParameterCount(); i++) {
                switch (pmd.getParameterType(i)) {
                    case Types.INTEGER: {
                        stmt.setInt(i, params.getIntegerField(i - 1));
                        break;
                    }
                    case Types.VARCHAR: {
                        stmt.setString(i, new String(params.getStringField(i - 1)));
                        break;
                    }
                    default:
                        String msg = String.format("Unsupported SQL typecode: %s", pmd.getParameterType(i));
                        throw new TatooineExecutionException(msg);
                }
            }
        } catch (SQLException ex) {
            throw new TatooineExecutionException(ex);
        }
    }

    /**
     * Converts the current row of the current result set into a NTuple.
     * This is a draft version that only supports string and integer fields.
     */
    protected NTuple toNTuple() throws TatooineExecutionException {
        NTuple tuple = new NTuple(nrsmd);

        try {
            // Note that parameters in prepared statements are 1-indexed :(
            int colIdx = 1;
            for (TupleMetadataType type : nrsmd.getColumnsMetadata()) {
                switch (type) {
                    case INTEGER_TYPE: {
                        tuple.addInteger(rs.getInt(colIdx));
                        break;
                    }
                    case STRING_TYPE: {
                        // String message = String.format("%s %s %s", colIdx, rs.getString(colIdx), queryStr);
                        // if (rs.getString(colIdx) == null)
                        //     System.out.println(message);
                        // first try to handle null values 
                        String value = (rs.getString(colIdx) != null) ? rs.getString(colIdx) : "__NULL";
                        tuple.addString(value);
                        break;
                    }
                    default: {
                        String message = String.format("Unsupported NTuple typecode: %s", type);
                        log.error(message);
                        throw new TatooineExecutionException(message);
                    }
                }
                colIdx++;
            }
            return tuple;
        } catch (SQLException ex) {
            // No results found
            rs = null;
            return null;
        }
    }

    public boolean hasSameDatabaseAs(SQLQueryParameterisedOperator op) {
        return this.ref.getPropertyValue("url").equals(op.getRef().getPropertyValue("url"));
    }

    /* Getters and setters */
    public String getQuery() {
        return queryStr;
    }

    public void setQuery(String query) {
        queryStr = query;
    }

    public StorageReference getRef() {
        return this.ref;
    }

}
