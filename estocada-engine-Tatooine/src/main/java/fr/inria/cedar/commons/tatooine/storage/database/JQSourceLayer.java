package fr.inria.cedar.commons.tatooine.storage.database;

import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class JQSourceLayer implements DBSourceLayer {

    private static final Logger log = Logger.getLogger(JQSourceLayer.class);

    /* Spark client implementation */
    // A dataframe is a distributed collection of data organised into named columns
    // It is said to be conceptually equivalent to a table in a relational database
    // Note: Research seems to indicate that Spark SQL's API does NOT support pagination
    private String tableStr = "";
    private String frameStr = "";
    private Dataset<Row> frame = null;
    private static JavaSparkContext context = null;

    private GSR view;

    /* Constructor */
    public JQSourceLayer(GSR gsr) {
        view = gsr;

        /* Resolving table and frame */
        String table = view.getPropertyValue("table");
        if (!Strings.isNullOrEmpty(table)) {
            tableStr = table;
        }
        String frame = view.getPropertyValue("frame");
        if (!Strings.isNullOrEmpty(frame)) {
            frameStr = frame;
        }
    }

    @Override
    public void connect() throws TatooineExecutionException {
        try {
            context = new JavaSparkContext("local", "spark");
            SQLContext sqlContext = new SQLContext(context);
            frame = sqlContext.read().json(frameStr);
            frame.registerTempTable(tableStr);
        } catch (Exception e) {
            String msg = String.format("Exception reading the input JSON file %s", frameStr);
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
    }

    @Override
    /**
     * Fetches the next "N" results from a Spark frame that correspond to an input query
     * "N" refers to the value of the variable "resultsPerQuery" which is provided by the user
     */
    public Object getBatchRecords(Object[] parameters) throws TatooineExecutionException {
        Row[] rows = null;

        // Check parameters
        int len = parameters.length;
        if (parameters.length != 1) {
            String msg = String.format("Method getBatchRecords called with %d parameters instead of 1", len);
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
        if (!(parameters[0] instanceof String)) {
            String msg = "Method getBatchRecords called with non-String parameters!";
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
        String queryStr = String.valueOf(parameters[0]);

        try {
            log.info(String.format("Querying Spark data frame \"%s\" with query \"%s\"", tableStr, parameters[0]));
            Dataset response = frame.sqlContext().sql(queryStr);
            rows = (Row[]) response.collect();
        } catch (Exception e) {
            e.printStackTrace();
            String msg = String.format("Error retrieving results from Spark's '%s' data frame with query '%s': %s",
                    tableStr, parameters[0], e.getMessage());
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }

        return rows;
    }

    public void close() {
        context.close();
    }

    @Override
    public void reset() throws TatooineExecutionException {
    }
}
