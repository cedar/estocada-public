package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import twitter4j.HashtagEntity;
import twitter4j.Status;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.OrderedLongElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;

public class TwitterHelper {
	
	// NRSMD
	public static NRSMD hashtagNRSMD;
	public static NRSMD twitterNRSMD = computeTwitterNRSMD();
	
	private static final Logger log = Logger.getLogger(TwitterHelper.class);

	/**
	 * A full description of tweet metadata can be found here @link {https://dev.twitter.com/overview/api/tweets}
	 * These are the names of the fields we are interested in with their corresponding TupleMetadataType:
	 * Tweet ID (field called "id"): URI
	 * User ID (field called "user.id"): URI
	 * User name (field called "user.name"): String
	 * Text (field called "text"): String
	 * Timestamp (field called "created_at"): String
	 * Hashtags (field called "entities.hashtags"): Nested, each containing a hashtag as a String
	 * Reply tweet ID (field called "in_reply_to_status_id"): URI, -1 if tweet is not a reply
	 * Reply user ID (field called "in_reply_to_user_id"): URI, -1 if tweet is not a reply
	 * Original tweet ID (field called "retweeted_status.id"): URI, -1 if tweet is not a retweet
	 * Retweet count (field called "retweet_count"): Integer
	 * Favourite count (field called "favorite_count"): Integer
	 */
	private static NRSMD computeTwitterNRSMD() {
		try {
			TupleMetadataType[] hashtagMetaTypes = {TupleMetadataType.STRING_TYPE};
			List<String> hashtagColNames = new ArrayList<String>();
			hashtagColNames.add("hashtag");
			hashtagNRSMD = new NRSMD(1, hashtagMetaTypes, hashtagColNames);

			String[] columnNames = {
				"id", "user_id", "user_name", "text", "created_at", "hashtags",
				"in_reply_to_status_id", "in_reply_to_user_id", 
				"retweeted_status_id", "retweet_count", "favourite_count",
				"user_description" 
			};
			
			TupleMetadataType[] columnTypes = {
					TupleMetadataType.UNIQUE_ID,
					TupleMetadataType.UNIQUE_ID,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.TUPLE_TYPE,
					TupleMetadataType.UNIQUE_ID,
					TupleMetadataType.UNIQUE_ID,
					TupleMetadataType.UNIQUE_ID,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.STRING_TYPE,
			};
			
			NRSMD[] nestedTypes = {hashtagNRSMD};
			NRSMD twitterNRSMD = new NRSMD(columnNames.length, columnTypes, columnNames, nestedTypes);
			return twitterNRSMD;
		} catch (Exception e) {
			String message = "Error creating Twitter NRSMD";
			log.error(message);
			return null;
		}
	}

	/** Transforms a tweet in the Twitter4J format into an NTuple */
	public static NTuple getNTupleFromStatus(Status status) {
		NTuple output = new NTuple(twitterNRSMD);

		// Tweet ID
		// Unique identifier for the tweet
		long id = status.getId();
		output.addID(new OrderedLongElementID(id));

		// User ID
		// Identifier of the user who posted the tweet
		long user_id = status.getUser().getId();
		output.addID(new OrderedLongElementID(user_id));

		// User name
		// Name of the user who posted the tweet
		String user_name = status.getUser().getName();
		output.addString(user_name);

		// Text
		// The actual text of the tweet
		String text = status.getText();
		Status original = status.getRetweetedStatus();
		if (original != null) {
			// Retweets start with the pattern "RT @ScreenName:"
			String marker = status.getText().split(":")[0];
			text = marker + ": " + original.getText();
		}		
		output.addString(text);

		// Timestamp
		// UTC time when the tweet was created
		String created_at = status.getCreatedAt().toString();
		output.addString(created_at);

		// Hashtags
		// An array of hashtags extracted from the tweet text
		ArrayList<NTuple> hastags = new ArrayList<NTuple>();
		for (HashtagEntity hashtag: status.getHashtagEntities()) {
			NTuple hashtagTuple = new NTuple(hashtagNRSMD);
			hashtagTuple.addString(hashtag.getText());
			hastags.add(hashtagTuple);
		}
		output.addNestedField(hastags);

		// Reply tweet ID
		// If the tweet is a reply, this field will contain the ID of the original tweet
		// Twitter API returns "-1" if the tweet is not a reply
		long in_reply_to_status_id = status.getInReplyToStatusId();
		output.addID(new OrderedLongElementID(in_reply_to_status_id));

		// Reply user ID
		// If the tweet is a reply, this field will contain the ID of the original tweet's author
		// Twitter API returns "-1" if the tweet is not a reply
		long in_reply_to_user_id = status.getInReplyToUserId();

		output.addID(new OrderedLongElementID(in_reply_to_user_id));

		// Original tweet ID
		// If the tweet is a retweet, this field will contain the ID of the original tweet
		// To comply with the previous two fields, we set this to -1 if not a retweet
		long retweeted_status_id = -1;
		if (original != null) {
			retweeted_status_id = original.getId();
		}
		output.addID(new OrderedLongElementID(retweeted_status_id));

		// Retweet count
		// Number of times the tweet has been retweeted
		int retweet_count = status.getRetweetCount();
		output.addInteger(retweet_count);

		// Favourite count
		// Indicates how many times the tweet has been "liked" by Twitter users
		int favorite_count = status.getFavoriteCount();
		output.addInteger(favorite_count);
		
		// User description
		String user_description = status.getUser().getDescription();
		output.addString((user_description == null) ? "" : user_description);
		return output;
	}
}
