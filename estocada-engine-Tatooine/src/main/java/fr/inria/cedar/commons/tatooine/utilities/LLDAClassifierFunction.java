package fr.inria.cedar.commons.tatooine.utilities;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by DucCao on 20/07/16.
 */
public class LLDAClassifierFunction implements NTupleFunction {
	private static final Logger log = Logger.getLogger(LLDAClassifierFunction.class);
	
    public static final int NUM_OF_MAX_TOPICS_PER_DOCUMENT          = 3;

    private static final String LLDA_FOLDER                         = "llda";
    private static final String LLDA_TOPIC_WORD_WEIGHTS_FILE        = LLDA_FOLDER + "/model/topic-word-weights-49.txt";
    private static final String LLDA_TOPIC_KEYS_FILE                = LLDA_FOLDER + "/model/zirn-49.keys";
    private static final String LLDA_TRAINING_DATA_FILE             = LLDA_FOLDER + "/model/mallet_train_data-49.seq";
    private static final String LLDA_INFERENCER_FILE                = LLDA_FOLDER + "/model/zirn-49.best_inferencer";
    private static final String LLDA_INPUT_FILE                     = LLDA_FOLDER + "/llda_test_input.txt";
    private static final String LLDA_DOC_TOPIC_FILE                 = LLDA_FOLDER + "/doc_topics.txt";
    private static final String MINISTRY_IDS_FILE                   = LLDA_FOLDER + "/ministry_ids.csv";

    private static final int NUM_ITERATIONS                         = 100;
    private int numOfTopTopics;

    private static FastInferTopics fastInferTopics = new FastInferTopics();
    private static Map<String, List<WordColorGenerator.TopicWeight>> stemToTopTopicWeights;
    private static final Map<String, String> mapTopicToRdfId = new HashMap<>();
    private static final Map<String, String> mapRdfIdToTopic = new HashMap<>();
    
    public static boolean isPreloaded = false;
    public static NRSMD outNRSMD;

    public LLDAClassifierFunction(int numOfTopTopics) {
    	this.numOfTopTopics = numOfTopTopics;
	}

	// NOTE: This function MUST be called before running the model
    public synchronized static void preloadModel() {
        System.setProperty("java.util.logging.config.file", "tatooine.conf");

    	if (isPreloaded) return;
    	isPreloaded = true;
        try {
            long startTime = System.currentTimeMillis();
            fastInferTopics.preload(new File(LLDA_TRAINING_DATA_FILE),
                    LLDA_INFERENCER_FILE
            );
            stemToTopTopicWeights =
                    WordColorGenerator.stemToTopTopicWeights(
                            LLDA_TOPIC_WORD_WEIGHTS_FILE,
                            LLDA_TOPIC_KEYS_FILE
                    );
            Stream<String> stream = Files.lines(Paths.get(MINISTRY_IDS_FILE));
            stream.forEach(line -> {
                String[] items = line.split(",");
                String topicId = items[0];
                String rdfId = items[1];
                mapTopicToRdfId.put(topicId, rdfId);
                mapRdfIdToTopic.put(rdfId, topicId);
            });
            stream.close();
            log.info("Preload model time = " + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            String msg = String.format("Error preloading the LLDA model: %s", e.getMessage());
            throw new RuntimeException(msg);
        }
        
        // Creating the output NRSMD
        TupleMetadataType[] outNRSMDMeta = {TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE};
		String[] outNRSMDNames = {"topicId","probability","html"};
        NRSMD[] outNRSMDNested = {};
        try {
			outNRSMD = new NRSMD(3, outNRSMDMeta, outNRSMDNames, outNRSMDNested);
		} catch (TatooineExecutionException e) {} 
    }


    public static String getTopicRdfId(String topicId) {
        return mapTopicToRdfId.get(topicId);
    }

    /**
     * @param input an NTuple which has 2 fields:
     *              + the first field is the preprocessed text
     *              + the second field is a string in JSON array format that maps between every word in the
     *              text document with its stem
     * @return list of NTuple objects, each of them has 3 fields:
     * + the RDF ID of the ministry which is topic of input text document
     * + the probability of this predicted topic
     * + the HTML code which annotates each word in the given text document with the color of that word's topic
     */
    @Override
    public List<NTuple> call(NTuple input) {
        List<NTuple> outputTuples = new LinkedList<>();

        try {
            String inputText = new String(input.getStringField(0));
            FileUtils.write(
                    new File(LLDA_INPUT_FILE),
                    "1 || " + inputText,
                    "UTF-8"
            );

            fastInferTopics.predict(LLDA_INPUT_FILE, NUM_ITERATIONS, LLDA_DOC_TOPIC_FILE);

            String wordToStem = new String(input.getStringField(1));
            WordColorGenerator wordColorGenerator = new WordColorGenerator();
            
            Map<String, String> topicKeys = topicKeys(LLDA_TOPIC_KEYS_FILE);
            Stream<String> stream = Files.lines(Paths.get(LLDA_DOC_TOPIC_FILE));
            stream.filter(line -> !line.startsWith("#")).forEach(line -> {
                String[] items = line.split("\t");
                items = Arrays.copyOfRange(items, 2, items.length);

                Map<String, Float> map = new HashMap<>();
                for (int topicIndex = 0; topicIndex < items.length; ++topicIndex) {
                    String topicId = topicKeys.get("" + topicIndex);
                    Float prob = Float.parseFloat(items[topicIndex]);
                    map.put(mapTopicToRdfId.get(topicId), prob);
                }
                
                List<Map.Entry<String, Float>> topTopicProbs = map.entrySet().stream()
                    .sorted(Map.Entry.<String, Float>comparingByValue()
                    .reversed())
                    .limit(numOfTopTopics)
                    .collect(Collectors.toList());

                final List<String> topTopics = topTopicProbs.stream()
                    .map(entry -> mapRdfIdToTopic.get(entry.getKey()))
                    .collect(Collectors.toList());

                try {
                    final String html = wordColorGenerator.generateHtml(topTopics, wordToStem, stemToTopTopicWeights);

                    topTopicProbs.forEach(entry -> {
                    	NTuple tuple = new NTuple(outNRSMD);
                    	
                    	tuple.setStringField(0, entry.getKey());
                    	tuple.setStringField(1, entry.getValue() + "");
                    	tuple.setStringField(2, html);
                    	
                        outputTuples.add(tuple);
                    });
                } catch (IOException e) {
                    String msg = String.format("Error generating the HTML code for the LLDA model: %s", e.getMessage());
                    stream.close();
                    throw new RuntimeException(msg);
                }
            });
            stream.close();
        } catch (Exception e) {
        	String msg = String.format("Error running the LLDA prediction: %s", e.getMessage());
            throw new RuntimeException(msg);
        }

        return outputTuples;
    }

    /**
     *
     * @param input an NTuple which has 1 nested field, that field contains a list of NTuple
     *              these NTuples has 1 string field which contains the preprocessed text of news article
     * @return the average probabilities for each topic
     * @throws TatooineException
     */
    public Map<String, Double> avgProbabilities(NTuple input) throws Exception {
        List<NTuple> nestedField = input.getNestedField(0);

        File file = new File(LLDA_INPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        int documentId = 1;
        for (NTuple article : nestedField) {
            String preprocessedText = new String(article.getStringField(0));

            FileUtils.write(
                    file,
                    documentId + " || " + preprocessedText + "\n",
                    "UTF-8",
                    true
            );

            documentId++;
        }

        fastInferTopics.predict(LLDA_INPUT_FILE, NUM_ITERATIONS, LLDA_DOC_TOPIC_FILE);

        Map<String, String> topicKeys = topicKeys(LLDA_TOPIC_KEYS_FILE);
        Stream<String> stream = Files.lines(Paths.get(LLDA_DOC_TOPIC_FILE));
        Map<String, ArrayList<Double>> map = new HashMap<>();
        stream.filter(line -> !line.startsWith("#")).forEach(line -> {
            String[] items = line.split("\t");
            items = Arrays.copyOfRange(items, 2, items.length);

            for (int topicIndex = 0; topicIndex < items.length; ++topicIndex) {
                String topicId = topicKeys.get("" + topicIndex);
                Double prob = Double.parseDouble(items[topicIndex]);
                String rdfTopicId = mapTopicToRdfId.get(topicId);

                if (!map.containsKey(rdfTopicId)) {
                    map.put(rdfTopicId, new ArrayList<Double>());
                }
                map.get(rdfTopicId).add(prob);
            }
        });
        stream.close();

        Map<String, Double> avgProbabilities = new HashMap<>();
        map.forEach((rdfTopicId, listProbabilities) -> {
            OptionalDouble average = listProbabilities.stream().mapToDouble(a -> a).average();
            avgProbabilities.put(rdfTopicId, average.getAsDouble());
        });

        return avgProbabilities;
    }

    @Override
    public NRSMD getOutputNRSMD() {
        return  outNRSMD;
    }

    @Override
    public String getName() {
        return LLDAClassifierFunction.class.getSimpleName();
    }

    /**
     * @param topicKeysFile path to the .keys file
     * @return mapping between topic id defined by the model and by us
     * @throws IOException
     */
    public static Map<String, String> topicKeys(String topicKeysFile) throws IOException {
        Map<String, String> topicKeys = new HashMap<>();

        Stream<String> stream = Files.lines(Paths.get(topicKeysFile));
        stream.forEach(line -> {
            String[] items = line.split("\t");
            topicKeys.put(items[0], items[1]);
        });
        stream.close();

        return topicKeys;
    }
}
