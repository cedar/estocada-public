package fr.inria.cedar.commons.tatooine.optimization;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * Interface that provides the basis for the implementation of equivalence rules used in query optimization
 * @author Oscar Mendoza
 */
public interface Transformation {

	/** Implements equivalence rules: transforms a given node to a different one */
	public NIterator apply(NIterator node) throws TatooineExecutionException;
	
	/** Decides if a particular transformation can be applied to a specific node */
	public boolean isApplicable(NIterator node);
	
}
