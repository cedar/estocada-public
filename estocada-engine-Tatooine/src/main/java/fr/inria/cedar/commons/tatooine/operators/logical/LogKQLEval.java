package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical Redis operator.
 * 
 * @author ranaalotaibi
 * @author Romain Primet
 */
public class LogKQLEval extends LogLeafOperator {

	private String				query;
	private StorageReference	sref;

	public LogKQLEval(String query, NRSMD meta) {
		this.query = query;
		this.setNRSMD(meta);
	}

	public LogKQLEval(String query, NRSMD meta, StorageReference sref) {
		this.query = query;
		this.sref = sref;
		this.setNRSMD(meta);
	}

	@Override
	public int getJoinDepth() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public String getName() {
		return "LogKQLEval";
	}

	@Override
	public String toString() {
		return "[LogKQLEval query: " + query + "]";
	}

	public String getQueryString() {
		return query.replaceAll("\"", "'");
	}

	public StorageReference getStorageReference() {
		return sref;
	}

	public void UpdateQuery(String parameter, String bindAccessVar) {
		query = query.replaceAll(bindAccessVar, parameter);
	}

	@Override
	public void accept(LogOperatorVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public LogOperator deepCopy() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public double estimatedCardinality() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public double estimatedIOCost() {
		throw new UnsupportedOperationException("Unsupported");
	}
}
