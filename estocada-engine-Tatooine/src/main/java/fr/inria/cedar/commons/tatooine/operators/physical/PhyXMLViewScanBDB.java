package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.sleepycat.je.DatabaseEntry;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.constants.PatternType;
import fr.inria.cedar.commons.tatooine.constants.ViewType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.OrderUtility;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.BDBDatabaseHolder;
import fr.inria.cedar.commons.tatooine.xam.Pattern;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;
import fr.inria.cedar.ontosql.db.BerkeleyDBException;

/**
 * Physical operator that represents a scan over a set of tuples stored in a database.
 * @author Ioana MANOLESCU
 * @created 28/07/2005
 */
public class PhyXMLViewScanBDB extends NIterator {
	private static final Logger log = Logger.getLogger(PhyXMLViewScanBDB.class);
	
	/** Universal version identifier for the PhyXMLViewScanBDB class */
	private static final long serialVersionUID = -3562335385360971761L;
	String databaseName;
	String XAMString;
	Pattern qp;

	transient BDBDatabaseHolder dbHolder;
	int cursorId = -1;
	NTuple tuple = null;

	String lastDocID = "-1";
	long lastDocTime = -1;

	GSR ref;

	String viewName;

	// Whether the scan is a never-closing scan (reads data as new data arrive in views).
	// If a tuple is not present in the database, the scan will block until one arrives.
	// The blocking happens in the hasNext() method.
	private boolean blocking = false;

	public PhyXMLViewScanBDB(GSR ref, boolean blocking) throws TatooineException, TatooineExecutionException {
		this(ref);
		this.blocking = blocking;
	}

	/**
	 * Public constructor for the class.
	 *
	 * @param ref
	 * @param peer
	 * @throws TatooineException
	 * @throws TatooineExecutionException
	 */
	public PhyXMLViewScanBDB(GSR ref) throws TatooineException, TatooineExecutionException {
		this.ref = ref;
		tuple = null;
		XAMString = ref.getPropertyValue("XAMString");

		try {
			qp = XAMParserUtility.getTreePatternFromString(XAMString, ref.getPropertyValue("ENVIRONMENT_PARA"));
			databaseName = qp.getName();

		} catch (Exception pe) {
			log.error("Exception: ", pe);
			throw new TatooineExecutionException(pe.toString());
		}

		this.nrsmd = NRSMD.getNRSMD(((TreePattern) qp).getRoot(), true,
				new HashMap<Integer, HashMap<String, ArrayList<Integer>>>());

		if (((TreePattern) qp).isOrdered()) {
			// Parameters.logger.debug("Ordered XAM");
			ArrayList<Integer> orderFlatAttributesIndices = OrderUtility.makeOrderAttributeIndices((TreePattern) qp);

			int[] orderGroupSizes = new int[orderFlatAttributesIndices.size()];
			for (int i = 0; i < orderGroupSizes.length; i++) {
				orderGroupSizes[i] = 1;
			}

			this.setOrderMaker(new OrderMarker(orderGroupSizes, 0));
			int iGroup = 0;
			Iterator<Integer> itColumns = orderFlatAttributesIndices.iterator();
			while (itColumns.hasNext()) {
				int thisColumn = itColumns.next().intValue();
				this.getOrderMaker().addDirect(iGroup, thisColumn);
				iGroup++;
			}

		} else {
			// Parameters.logger.debug("Non-ordered XAM");
			setOrderMaker(new OrderMarker());
		}

		viewName = ref.getPropertyValue("ENVIRONMENT_PARA");
		viewName = viewName.substring(viewName.lastIndexOf("/") + 1);

		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		// Parameters.logger.info("Scan: " + this.getName() + " open!");
		informPlysPlanMonitorOperStatus(0, getOperatorID());

		try {
			dbHolder = BDBDatabaseHolder.getInstance();
		} catch (BerkeleyDBException e) {
			throw new TatooineExecutionException(e);
		}
		try {
			cursorId = dbHolder.getCursor(ref, blocking);
		} catch (Exception e) {
			throw new TatooineExecutionException(e);
		}
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// Parameters.logger.info("Scan: " + this.getName() + " hasNext:");

		if (timeout) {
			return false;
		}

		// If the operator is persistent, we will wait until timeout or a
		// a tuple arrives in the database
		if (blocking) {
			while (!timeout && (tuple = dbHolder.getNextRecord(cursorId)) == null) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else {
			tuple = dbHolder.getNextRecord(cursorId);
		}

		if (tuple != null && !timeout) {

			// Parameters.logger.info("I have read a tuple (" + this.databaseName + "). " + tuple.toString());

			if (blocking && "".equals(tuple.getDocID())) {
				return false;
			}

			return true;
		}

		return false;
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		NTuple returnTuple = this.tuple;
		this.tuple = null;
		this.numberOfTuples++;
		// Parameters.logger.info("Returned so far " + this.numberOfTuples);
		return returnTuple;
	}

	public DatabaseEntry getKeyForLastTuple() {
		return dbHolder.getKeyForLastTuple(cursorId);
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());

		dbHolder.closeCursor(cursorId);

		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		List<NTuple> v = tuple.nestedFields[i];
		return new PhyArrayIterator(v, tuple.nrsmd.getNestedChild(i));
	}

	@Override
	public String getName() {

		return "PhyXMLViewScanBDB" + "(" + this.databaseName + ")";
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return "\n" + spaceForIndent + "PhyXMLViewScanBDB(" + this.databaseName + ")";
	}

	@Override
	public NIterator copy() {
		try {
			return new PhyXMLViewScanBDB(this.ref);
		} catch (Exception e) {
			log.error("Exception: ", e);
			return null;
		}
	}

	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 *
	 * @author Aditya SOMANI
	 */
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		if (qp.getName().startsWith(ViewType.COMPULSORY.toString())) {
			sb.append(firstAvailableNo + " [color=\"" + PatternType.COMPULSORY.getForeground()
					+ "\" style=\"filled\" fillcolor=\"" + PatternType.COMPULSORY.getBackground()
					+ "\" label=\"ViewScan(" + this.qp.getName() + ")\"] ; \n");
		} else if (qp.getName().startsWith(ViewType.COLLABORATIVE.toString())) {
			sb.append(firstAvailableNo + " [color=\"" + PatternType.COLLABORATIVE.getForeground()
					+ "\" style=\"filled\" fillcolor=\"" + PatternType.COLLABORATIVE.getBackground()
					+ "\" label=\"ViewScan(" + this.qp.getName() + ")\"] ; \n");
		} else if (qp.getName().startsWith(ViewType.SELFISH.toString())) {
			sb.append(firstAvailableNo + " [color=\"" + PatternType.SELFISH.getForeground()
					+ "\" style=\"filled\" fillcolor=\"" + PatternType.SELFISH.getBackground() + "\" label=\"ViewScan("
					+ this.qp.getName() + ")\"] ; \n");
		} else {
			sb.append(firstAvailableNo + " [color=\"" + PatternType.CONTENT.getForeground()
					+ "\" style=\"filled\" fillcolor=\"" + PatternType.CONTENT.getBackground() + "\" label=\"ViewScan("
					+ this.qp.getName() + ")\"] ; \n");
		}

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return firstAvailableNo + 1;
	}

	public GSR getGSR() {
		try {
			return ref;
		} catch (ClassCastException e) {
			log.error("Cannot cast storage reference to GSR", e);
			return null;
		}
	}

}
