package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Physical SQL (through JDBC) query operator.
 * @author Romain Primet
 */
public class PhySQLEval extends SQLQueryParameterisedOperator implements PhyEvalOperator {

	/** Universal version identifier for the PhySQLEval class */
	private static final long serialVersionUID = 4140886496116239642L;
	
	private static final Logger log = Logger.getLogger(PhySQLEval.class);
	
	/* Constructor */
	public PhySQLEval(String query, StorageReference ref, NRSMD nrsmd) throws TatooineExecutionException {
		super(query, ref, nrsmd);
		this.nrsmd = inferMetadata(nrsmd, findOutputColumns(query));
	}
	
	/* Constructor */
	public PhySQLEval(String query, StorageReference ref, ViewSchema schema) throws TatooineExecutionException {
		this(query, ref, schema.getNRSMD());
	}

	@Override
	public String getName() {
		return "PhySQLEval( "+this.queryStr+" )";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}
	
	/** Attempts to determine the output columns of a given SQL query */
	private String findOutputColumns(String query) throws TatooineExecutionException {
		// We assume that the Eval SQL queries we will support are of the form: 
		// SELECT [DISTINCT] col1, ..., colN FROM [...]		
		Pattern p = Pattern.compile("(?:SELECT|select)(?:\\s+(?:DISTINCT|distinct))?\\s+(.*?)\\s+(?:FROM|from)");
		Matcher m = p.matcher(query);
		if (!m.find() || m.group(1) == null || m.group(1).isEmpty()) {
			String message = String.format("Failed to determine resulting metadata from input query: %s", query);
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		String match = m.group(1).trim();
		if (match.equals("*")) {
			log.error(MSG_USE_SCAN);
			throw new TatooineExecutionException(MSG_USE_SCAN);
		}
		return match;
	}

	@Override
	public NRSMD inferMetadata(NRSMD inputMD, String fieldsParam) throws TatooineExecutionException {
		List<String> paramFields = new ArrayList<String>(); 
		for (String field : Arrays.asList(fieldsParam.split(","))) {
            String[] splitField = field.split("\\s+(as|AS)\\s+");
            String fieldId = splitField[0];
            
			paramFields.add(fieldId.trim());
		}
		List<String> foundFields = new LinkedList<String>(paramFields);
		
		int counterCol = 0;
		int[] keepColumns = new int[paramFields.size()];			
        for (String field : paramFields) {
            int index = Arrays.asList(inputMD.colNames).indexOf(field);
            if (index != -1 ) {
                keepColumns[counterCol] = index;
                counterCol++;
                foundFields.remove(field);
            }
        }
		
    // All param fields in the query should also be present in the NRSMD
    if (!foundFields.isEmpty()) {
        String msg = String.format("The field(s) %s in the SELECT statement of the input query are not part of the input NRSMD [%s]", foundFields, String.join(" ", inputMD.colNames));
        log.error(msg);
        throw new TatooineExecutionException(msg);
    }
		
    NRSMD resultMD = NRSMD.makeProjectRSMD(inputMD, keepColumns);
    return resultMD;
}

@Override
public NIterator copy() throws TatooineExecutionException {
    return new PhySQLEval(this.queryStr, this.ref, this.nrsmd);
}

public void accept(Visitor visitor) throws TatooineExecutionException {
    visitor.visit(this);
}
}
