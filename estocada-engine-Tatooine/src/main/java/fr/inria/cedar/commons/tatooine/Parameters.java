package fr.inria.cedar.commons.tatooine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineException;

/**
 * This file loads and saves Tatooine parameters. When we want to know the
 * value of a property in Tatooine, we should access it through this class.
 *
 * @author Alin TILEA
 * @author Asterios KATSIFODIMOS
 */
public class Parameters {
	private static final Logger log = Logger.getLogger(Parameters.class);
	
	// Configuration file that contains all Tatooine parameters
	private static final String CONFIG_FILE = "tatooine.conf";

	private static boolean initialized = false;
	private static boolean defaultCF =  false;
	private static PropertiesConfiguration propertiesConfiguration;
	
	// This is used to hold all the parameters and their respective values
	private static Map<String, String> parameters = new HashMap<String, String>(); 

	/** Initializes all Tatooine parameters using the default path 
	 * @throws IOException */
	public static void init() throws TatooineException, IOException {
		defaultCF =  true;
		init(CONFIG_FILE);
	}

	/** Initializes all Tatooine parameters given a path of a configuration file 
	 * @throws IOException */
	public static void init(String configFile) throws TatooineException, IOException {
		//assert (!initialized) : "Do not re-initialize parameters. Use destroy first";
		File f;
		if(defaultCF){
			InputStream stream = Parameters.class.getClassLoader().getResourceAsStream(configFile);
			f = new File("tatooinetmp.conf");
			OutputStream outputStream = new FileOutputStream(f);
			IOUtils.copy(stream, outputStream);
		}
		else{
			f = new File(configFile);
		}
		//System.out.println(Parameters.class.getClassLoader().getResourceAsStream("tatooine.conf").toString());
		// Getting path of current class being executed
		//File f = new File(Parameters.class.getProtectionDomain().getCodeSource().getLocation().getPath()+CONFIG_FILE);
		if (!f.exists() || !f.isFile()) {
			String msg = String.format("The file %s was not found", f.getAbsolutePath());
			log.error(msg);
			log.error("Will shutdown now.");
			System.exit(0);
		}

		try {
			propertiesConfiguration = new PropertiesConfiguration(f);
		} catch (ConfigurationException e) {
			String msg = String.format("ConfigurationException while reading %s: %s", f, e.getMessage());
			log.error(msg);
			log.error("Will shutdown now.");
			System.exit(0);
		}

		Iterator<String> keysIterator = propertiesConfiguration.getKeys();
		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			parameters.put(key.toUpperCase(), propertiesConfiguration.getProperty(key).toString());
		}

		initialized = true;
		if(defaultCF){
			f.delete();
		}

        defaultCF = false;
	}

	/** Returns the value of a Tatooine property */
	public static String getProperty(String propName) {		
		String propValue = parameters.get(propName.toUpperCase());
		if (propValue != null) {
			// Some configurations like SOLR_HOME contain "#ignoreline"
			// They shouldn't be pushed to GIT
			// We should remove "#ignoreline" to retrieve the correct value
			propValue = propValue.replace("#ignoreline", "").trim();
		}
		if (propValue == null) {
			String msg = String.format("The property '%s' is not defined in tatooine.conf!", propName);
			log.error(msg);
		}
		return propValue;
	}

	public static void setProperty(String propName, String propValue) {
		if (propName == null || propValue == null) {
			log.error("Setting a property name or a property value to null is not supported!");
		} else {
			String oldPropValue = parameters.get(propName.toUpperCase());
			if (oldPropValue != null) {
				String msg = String.format("The property '%s' is already set to '%s' and will be overwritten by '%s'", 
						propName, oldPropValue, propValue);
				log.warn(msg);
			}
			parameters.put(propName.toUpperCase(), propValue);
		}
	}
}
