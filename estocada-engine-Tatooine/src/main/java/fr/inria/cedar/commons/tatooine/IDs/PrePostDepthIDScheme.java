package fr.inria.cedar.commons.tatooine.IDs;

import java.util.Stack;

/**
 * Pre-post-depth element ID scheme.
 *
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public class PrePostDepthIDScheme implements IDScheme {

	int currentPre;
	int currentPost;
	int currentDepth;

	static boolean initialized;
	
	Stack<PrePostDepthElementID> s;
	
	PrePostDepthElementID currentID;
	
	private static String nullIDStringImage = "null null null";

	public PrePostDepthIDScheme(){
		currentPre = 0;
		currentPost = 0;
		currentDepth = 0;
		currentID = null;
		s = new Stack<PrePostDepthElementID>();
	}
		
	/** True if the scheme is order-preserving */
	public boolean isOrderPreserving() {
		return true;
	}

	/** True if the scheme allows to infer parent-child relationships */
	public boolean isParentAncestorPreserving() {
		return true;
	}

	/** True if the scheme allows parent navigation */
	public boolean allowsParentNavigation() {
		return false;
	}

	/** True if the scheme allows updates */
	public boolean allowsUpdates() {
		return false;
	}

	/** To be called when a document starts, for possible initialization */
	public void beginDocument() {
		currentPre = 0;
		currentPost = 0;
		currentDepth = 0;
	}

	/** To be called when an element or attribute starts */
	public void beginNode() {
		currentID = (PrePostDepthElementID)ElementIDFactory.getElementID(currentPre, currentDepth);
		currentPre ++;
		currentDepth ++;
		s.push(currentID);
	}
	
	/** This is used when the IDScheme is required to store the tag associated with each ID */
	public void beginNode(String tag) {
		// TODO Auto-generated method stub
	}

	/** To be called when an element or attribute ends */
	public void endNode() {
		currentID = (PrePostDepthElementID)(s.pop());
		currentID.setPost(currentPost);
		currentPost ++;
		currentDepth --;
	}

	/** To be called at the end of the document */
	public void endDocument() {
		// Nothing...
	}

	/** Returns the ID of the last element for which endElement has been called */
	public ElementID getLastID() {
		return currentID;
	}
	
	/** Returns a string snippet containing the JDBC type(s) associated to the IDs produced by this scheme */
	public String getSignature(String suffix){
		return ("ID" + suffix + "Pre int, ID" + suffix + "Post int, ID" + suffix + "depth int");
	}
	
	/** Returns a string that will be used to enter null ID values in an RDB */
	public  String nullIDStringImage(){
		return nullIDStringImage;
	}

	/**
	 * Returns a string snippet containing the name of the ID attribute produced by this scheme.
	 * This is used to declare an index via JDBC.
	 */
	public String getIndexSignature(String suffix) {
		return ("ID" + suffix + "Pre, ID" + suffix + "Post, ID" + suffix + "depth");
	}
}

