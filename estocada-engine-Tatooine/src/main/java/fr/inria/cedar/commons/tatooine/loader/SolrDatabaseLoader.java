package fr.inria.cedar.commons.tatooine.loader;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient.RemoteSolrException;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.request.LukeRequest;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.common.cloud.SolrZkClient;
import org.apache.solr.common.params.CoreAdminParams.CoreAdminAction;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.Pair;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.zookeeper.KeeperException;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

/**
 * Created by DucCao on 05/10/16.
 * Load data file into Solr
 */
public class SolrDatabaseLoader extends FieldMapDatabaseLoader {
    private static final Logger log = Logger.getLogger(SolrDatabaseLoader.class);

    /** Enum type that represents all possible SOLR schema types (as of version 6.0.0) */
    private static enum SOLRSchemaFieldType {
        INT,
        TINT,
        FLOAT,
        TFLOAT,
        LONG,
        TLONG,
        DOUBLE,
        TDOUBLE,
        CURRENCY,
        INTS,
        TINTS,
        FLOATS,
        TFLOATS,
        LONGS,
        TLONGS,
        DOUBLES,
        TDOUBLES,
        STRING,
        BOOLEAN,
        DATE,
        TDATE,
        BINARY,
        TEXT_WS,
        MANAGED_EN,
        TEXT_GENERAL,
        TEXT_GENERAL_REV,
        TEXT_EN,
        TEXT_EN_SPLITTING,
        TEXT_EN_SPLITTING_TIGHT,
        ALPHAONLYSORT,
        PHONETIC,
        LOWERCASE,
        DESCENDENT_PATH,
        ANCESTOR_PATH,
        POINT,
        LOCATION,
        STRINGS,
        BOOLEANS,
        DATES,
        TDATES
    }

    public static final String DEFAULT_SOLR_URL = "http://localhost";
    public static final String DEFAULT_SOLR_PORT = "8983";

    private static final String ZOOKEEPER_URL = "localhost:9983";
    private static final int ZK_TIMEOUT = 30000;
    private static final int ZK_CONNECTION_TIMEOUT = 45000;
    private static final String CONFIGS_ZKNODE = "/configs";

    private String solrUrl;
    private String configDir;

    // We assume Solr was started in cloud mode
    // When running the method "isDataLoaded()" we will update this variable if needed
    private static boolean solrCloudMode = true;

    public SolrDatabaseLoader(String configDir, StorageReference ref) {
        this.configDir = configDir;
        setStorageReference(ref);
        solrUrl = String.format("%s:%s/solr/", ref.getPropertyValue("serverUrl"), ref.getPropertyValue("serverPort"));
    }

    @Override
    public boolean load(String filePath) {
        String indexName = getCatalogEntryName();
        log.info(String.format("Loading index: %s", indexName));
        try {
            createIndexIfNeeded(indexName, configDir);
            try {
                String solrHome = Parameters.getProperty("solrHome");
                Process process =
                        Runtime.getRuntime().exec(String.format("%s/bin/post -c %s %s", solrHome, indexName, filePath));
                process.waitFor();
            } catch (IOException | InterruptedException e) {
                String msg = String.format("Error loading Solr index '%s': %s", indexName, e.getMessage());
                log.error(msg);
                e.printStackTrace();
            }
            return true;
        } catch (Exception e) {
            log.error(String.format("Error creating Solr index '%s': %s", indexName, e.getMessage()));
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void deleteAllData(String indexName) {
        log.info(String.format("Deleting all data from index: %s", indexName));
        SolrClient solrClient = new HttpSolrClient(solrUrl + indexName);
        try {
            solrClient.deleteByQuery("*:*");
            solrClient.commit();
            solrClient.close();
        } catch (SolrServerException | IOException e) {
            log.error(String.format("Error deleting all data from index '%s': %s", indexName, e.getMessage()));
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCollection(String indexName) {
        log.info(String.format("Deleting index: %s", indexName));

        if (isDataLoaded(indexName)) {
            SolrClient solrClient = new HttpSolrClient(solrUrl);
            if (solrCloudMode) {
                // Deleting Solr collection (in solrcloud mode)
                final CollectionAdminRequest.Delete deleteCollectionRequest = new CollectionAdminRequest.Delete();
                //CollectionAdminRequest.deleteCollection(indexName);
                deleteCollectionRequest.setCollectionName(indexName);

                final CollectionAdminResponse response;
                try {
                    response = deleteCollectionRequest.process(solrClient);
                    solrClient.close();
                    if (!response.isSuccess()) {
                        log.error(response.getErrorMessages());
                        String msg = String.format("Failed to delete Solr collection '%s': %s", indexName,
                                response.getErrorMessages().toString());
                        throw new IllegalStateException(msg);
                    }
                } catch (SolrServerException | IOException e) {
                    log.error(String.format("Error deleting Solr collection '%s': %s", indexName, e.getMessage()));
                    e.printStackTrace();
                }
            } else {
                // Deleting Solr core (in standalone mode)
                CoreAdminRequest.Unload deleteCoreRequest = new CoreAdminRequest.Unload(true);
                deleteCoreRequest.setCoreName(indexName);
                try {
                    deleteCoreRequest.process(solrClient);
                } catch (SolrServerException | IOException e) {
                    log.error(String.format("Error deleting Solr core '%s': %s", indexName, e.getMessage()));
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isDataLoaded(String indexName) {
        SolrClient solrClient = new HttpSolrClient(solrUrl);
        CollectionAdminRequest.List listColl = new CollectionAdminRequest.List();
        try {
            List<String> collectionNames = (List<String>) listColl.process(solrClient).getResponse().get("collections");
            solrClient.close();
            return collectionNames.contains(indexName);
        } catch (RemoteSolrException rse) {
            // A RemoteSolrException is raised when Solr is running in standalone mode
            CoreAdminRequest request = new CoreAdminRequest();
            request.setAction(CoreAdminAction.STATUS);
            try {
                CoreAdminResponse response = request.process(solrClient);
                solrCloudMode = false;
                solrClient.close();
                return (response.getCoreStatus(indexName) != null);
            } catch (SolrServerException | IOException e) {
                String msg = String.format("Error pinging Solr core '%s': %s", indexName, e.getMessage());
                log.error(msg);
                e.printStackTrace();
            }
        } catch (SolrServerException | IOException e) {
            String msg = String.format("Error pinging Solr collection '%s': %s", indexName, e.getMessage());
            log.error(msg);
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void registerNewCatalogEntry() {
        String indexName = getCatalogEntryName();
        Catalog catalog = Catalog.getInstance();
        try {
            ViewSchema schema = findMetadata(indexName);
            storageReference.setProperty("coreName", indexName);
            catalog.add(indexName, storageReference, new ViewSizeBundle(5, 5), schema);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, Pair<TupleMetadataType, TupleMetadataType>> getFieldMap() {
        HashMap<String, Pair<TupleMetadataType, TupleMetadataType>> fieldMap = new HashMap<>();

        // A LukeRequest returns a summary of the schame field types of a Solr index
        SolrClient solr = new HttpSolrClient(solrUrl);
        NamedList<Object> queryResult = null;
        try {
            queryResult = solr.request(new LukeRequest(), getCatalogEntryName());
            solr.close();
        } catch (SolrServerException | IOException e) {
            String msg = String.format("Error performing LukeRequest: %s", e.getMessage());
            log.error(msg);
            e.printStackTrace();
        }

        @SuppressWarnings("unchecked")
        SimpleOrderedMap<NamedList<String>> elems = (SimpleOrderedMap<NamedList<String>>) queryResult.get("fields");
        Iterator<Map.Entry<String, NamedList<String>>> it = elems.iterator();

        while (it.hasNext()) {
            Map.Entry<String, NamedList<String>> entry = it.next();
            SOLRSchemaFieldType typeSOLR = null;
            String typeJSON = entry.getValue().get("type").toUpperCase();

            if (isReservedField(entry.getKey())) {
                continue;
            }

            try {
                typeSOLR = SOLRSchemaFieldType.valueOf(typeJSON);
            } catch (IllegalArgumentException e) {
                log.error(String.format("Unidentified SOLR schema type: %s", typeJSON));
                e.printStackTrace();
            }

            // Maps a SOLR schema field type to an NTuple MD type
            Pair<TupleMetadataType, TupleMetadataType> pair = transformSOLRToNTupleMetadataType(typeSOLR);
            fieldMap.put(entry.getKey(), pair);
        }

        return fieldMap;
    }

    /** Maps a SOLR schema field type to an NTuple metadata type */
    private Pair<TupleMetadataType, TupleMetadataType> transformSOLRToNTupleMetadataType(SOLRSchemaFieldType type) {
        switch (type) {
            case INT:
            case TINT:
                // TODO: Support float MD for TupleMetadataType
            case FLOAT:
            case TFLOAT:
            case LONG:
            case TLONG:
            case DOUBLE:
            case TDOUBLE:
            case CURRENCY:
                // Simple tuple of type integer
                return new Pair<TupleMetadataType, TupleMetadataType>(TupleMetadataType.INTEGER_TYPE, null);
            case INTS:
            case TINTS:
            case FLOATS:
            case TFLOATS:
            case LONGS:
            case TLONGS:
            case DOUBLES:
            case TDOUBLES:
                // Nested tuples with integer child tuples
                return new Pair<TupleMetadataType, TupleMetadataType>(TupleMetadataType.TUPLE_TYPE,
                        TupleMetadataType.INTEGER_TYPE);
            case STRINGS:
            case BOOLEANS:
            case DATES:
            case TDATES:
                // Nested tuples with string child tuples
                return new Pair<TupleMetadataType, TupleMetadataType>(TupleMetadataType.TUPLE_TYPE,
                        TupleMetadataType.STRING_TYPE);
            default:
                // Simple tuple of type string
                return new Pair<TupleMetadataType, TupleMetadataType>(TupleMetadataType.STRING_TYPE, null);
        }
    }

    /** Checks if a SOLR field is reserved or not */
    private boolean isReservedField(String fieldName) {
        // Names with both leading and trailing underscores are reserved
        return fieldName.startsWith("_") | fieldName.endsWith("_");
    }

    private void uploadConfigToZk(String configDir, String configName)
            throws InterruptedException, IOException, KeeperException {
        SolrZkClient zkClient = null;
        try {
            zkClient = new SolrZkClient(ZOOKEEPER_URL, ZK_TIMEOUT, ZK_CONNECTION_TIMEOUT, null);

            File[] files = new File(configDir).listFiles();
            for (File file : files) {
                uploadConfigFileToZk(zkClient, configName, file.getName(), new File(configDir, file.getName()));
            }
        } finally {
            if (zkClient != null) {
                zkClient.close();
            }
        }
    }

    private void uploadConfigFileToZk(SolrZkClient zkClient, String configName, String nameInZk, File file)
            throws InterruptedException, IOException, KeeperException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File child : files) {
                zkClient.makePath(CONFIGS_ZKNODE + "/" + configName + "/" + nameInZk + "/" + child.getName(), child,
                        false, true);
            }
        } else {
            zkClient.makePath(CONFIGS_ZKNODE + "/" + configName + "/" + nameInZk, file, false, true);
        }
    }

    /** Creates Solr index depending on its execution mode */
    private void createIndexIfNeeded(String indexName, String configDir)
            throws InterruptedException, IOException, KeeperException, SolrServerException {
        log.info(String.format("Creating index: %s", indexName));

        if (!isDataLoaded(indexName)) {
            SolrClient solrClient = new HttpSolrClient(solrUrl);
            if (solrCloudMode) {
                // Creating Solr collection (in solrcloud mode)
                String configName = indexName;
                uploadConfigToZk(configDir, configName);

                final CollectionAdminRequest.Create createCollectionRequest = new CollectionAdminRequest.Create();
                //CollectionAdminRequest.Create.createCollection(indexName, indexName, 1, 1);

                CollectionAdminResponse response = createCollectionRequest.process(solrClient);
                solrClient.close();

                if (!response.isSuccess()) {
                    String msg =
                            String.format("Failed to create collection: %s", response.getErrorMessages().toString());
                    log.error(msg);
                    throw new IllegalStateException(msg);
                }
            } else {
                // Creating Solr core (in standalone mode)	
                String solrHome = Parameters.getProperty("solrHome");
                Process process =
                        Runtime.getRuntime().exec(String.format("%s/bin/solr create_core -c %s", solrHome, indexName));
                process.waitFor();
            }
        }
    }
}
