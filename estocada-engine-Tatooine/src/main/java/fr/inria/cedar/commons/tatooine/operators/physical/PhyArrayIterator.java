package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * @author Ioana MANOLESCU
 * @created 28/07/2005
 */
public class PhyArrayIterator extends NIterator {
	private static final Logger log = Logger.getLogger(PhyArrayIterator.class);

	/** Universal version identifier for the PhyArrayIterator class */
	private static final long serialVersionUID = -8416301341963231262L;

	Iterator<NTuple> i;

	NTuple t;
	List<NTuple> v;
	String xamName;

	public PhyArrayIterator(List<NTuple> v2, NRSMD newNRSMD) {
		this(v2, newNRSMD, "");
		increaseOpNo();
	}

	public PhyArrayIterator(List<NTuple> list, NRSMD newNRSMD, String xamName) {
		super();
		this.v = list;
		this.nrsmd = newNRSMD;
		assert (this.nrsmd != null) : "Null NRSMD";
		this.setOrderMaker(new OrderMarker());
		this.xamName = xamName;
		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());

		i = v.iterator();

		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		return i.hasNext();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		this.t = i.next();
		this.numberOfTuples++;
		return t;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		if (t.nestedFields == null) {
			t.display();
			throw new TatooineExecutionException("No nested fields ");
		}
		if (i >= t.nestedFields.length) {
			t.display();
			throw new TatooineExecutionException("Nested field index " + i + " larger than "
					+ (t.nestedFields.length - 1));
		}
		List<NTuple> aux = t.nestedFields[i];
		return new PhyArrayIterator(aux, t.nrsmd.getNestedChild(i), "");

	}

	@Override
	public String getName() {
		return "PhyArrayIter(" + this.xamName + ")";
	}

	@Override
	public String getName(int depth) {
		return getTabs(depth) + "PhyArrayIter(\"" + this.xamName + "\")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyArrayIterator(this.v, this.nrsmd);
	}

	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 * 
	 * @author Aditya SOMANI
	 */
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"" + "(" + this.getOperatorID().physOpNo + ")" + xamName + "\"] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return (firstAvailableNo + 1);
	}

	/**
	 * Returns the number of NetReceive Operations under this class.
	 * 
	 * @author Aditya SOMANI
	 */
	public int receiveCount() {
		return 0;
	}

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }

    public boolean isEmpty() {
        return this.v.isEmpty();
    }
}
