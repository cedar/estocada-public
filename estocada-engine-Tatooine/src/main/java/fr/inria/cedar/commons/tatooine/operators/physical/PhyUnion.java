package fr.inria.cedar.commons.tatooine.operators.physical;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

public class PhyUnion extends NIterator {

    private static final long serialVersionUID = 8095919137037803408L;
    final protected NIterator[] children;
    protected int childIndex = 0;
    
    public PhyUnion(NIterator[] children) {
        super(children);
        this.children = children;
        this.nrsmd = this.children[0].nrsmd;
    }

    public PhyUnion(NIterator left, NIterator right) {
        super(left, right);
        this.children = new NIterator[] {left, right};
        this.nrsmd = left.nrsmd;
    }

    @Override
    public void close() throws TatooineExecutionException {
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {

        int l = this.children.length;
        NIterator[] childrenCopy = new NIterator[l];
        
        for(int i = 0; i < l; i++) {
            childrenCopy[i] = this.children[i].copy();
        }
        
        return new PhyUnion(childrenCopy);
    }

    @Override
    public String getName() {
        String childrenName = "";

        for (NIterator child : this.children) {
            childrenName += child.getName();
        }
        
        String name = String.format("PhyUnion (%s)", childrenName);
        return name;
    }

    @Override
    public String getName(int depth) {
        String childrenName = "";

        for (NIterator child : this.children) {
            childrenName += child.getName(depth);
        }
        
        String name = String.format("PhyUnion (%s)", childrenName);
        return name;
    }

    @Override
    public NIterator getNestedIterator(int arg0) throws TatooineExecutionException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {

        if(this.children[this.childIndex].hasNext()) {
            return true;
        } else {
            this.children[this.childIndex].close();
            
            if (this.childIndex < this.children.length - 1) {
                this.childIndex++;
                this.children[this.childIndex].open();
                return hasNext();
            } else {
                return false;
            }
        }
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        return this.children[this.childIndex].next();
    }

    @Override
    public void open() throws TatooineExecutionException {
        if(children.length > 0) {
            this.children[0].open();
        }
    }

    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        sb.append(firstAvailableNo + " [label=\"Union\", color = " + getColoring() + "] ; \n");

        if (parentNo != -1) {
            sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
        }

        int childNumber = firstAvailableNo;

        for(NIterator child : this.children) {
            childNumber = child.recursiveDotString(sb, firstAvailableNo, (childNumber + 1));
        }

        return childNumber;
    }

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
