package fr.inria.cedar.commons.tatooine.operators.physical.util;

import java.util.HashSet;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * distinct operator build using hash set
 *
 * @author Maxime Buron
 */
public class PhyHashDistinct extends NIterator {

    protected HashSet<NTuple> set;
    protected NTuple current;
    final protected NIterator child;

    public PhyHashDistinct(NIterator child) {
        super(child);
        this.child = child;
    }

    @Override
    public void open() throws TatooineExecutionException {
        child.open();
        set = new HashSet<>();
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        if (child.hasNext()){
            NTuple candidate = child.next();

            while(set.contains(candidate) && child.hasNext()) {
                candidate = child.next();
            }

            if (set.contains(candidate)) {
                return false;
            } else {
                current = candidate;
                set.add(current);
                return true;
            }
        }

        return false;
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        return current;
    }

    @Override
    public void close() throws TatooineExecutionException {
        set.clear();
        child.close();
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        return null;
    }

    @Override
    public String getName() {
        return "PhyHashDistinct(" + child.getName() + ")";
    }

    @Override
    public String getName(int depth) {
        String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
        return "\n" + spaceForIndent + "PhyHashDistinct(" + child.getName(1 + depth) + "\n"  + ")";
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        return new PhyHashDistinct(child.copy());
    }

    @Override
    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        sb.append(firstAvailableNo + " [label=\"HashDistinct\", color = " + getColoring() + "] ; \n");

        if (parentNo != -1) {
            sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
        }

        return child.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
    }

    public NIterator getChild() {
        return child;
    }

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
