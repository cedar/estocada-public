package fr.inria.cedar.commons.tatooine.operators.physical;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.constants.AggregateFunction;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class Aggregator {
	
	private static final Logger log = Logger.getLogger(PhyGroupBy.class);
	
	private AggregateFunction function;
	private String column;
	
	public Aggregator(AggregateFunction function, String column) throws TatooineExecutionException {
		if (Strings.isNullOrEmpty(column) && function != AggregateFunction.COUNT) {
			String message = "All aggregation functions other than COUNT expect a value column on which the aggregation should be performed";
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		this.function = function;
		this.column = column;
	}
	
	/* Getters and setters */
	
	public AggregateFunction getFunction() {
		return function;
	}
	public void setFunction(AggregateFunction function) {
		this.function = function;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	
}
