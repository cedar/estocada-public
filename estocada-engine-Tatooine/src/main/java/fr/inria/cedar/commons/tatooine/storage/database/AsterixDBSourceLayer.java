package fr.inria.cedar.commons.tatooine.storage.database;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * AsterixDB source layer that defines the connection and querying of Apache AsterixBD used by PhyAsterixDBJSONEval.
 * 
 * @author ranaalotaibi
 *
 */
public class AsterixDBSourceLayer implements DBSourceLayer {

    private static final Logger LOGGER = Logger.getLogger(AsterixDBSourceLayer.class);

    private static final String QUERY_SERVICE_PATH = "/query/service";
    private static final String QUERY_CANCEL_PATH = "/admin/requests/running/";
    private static final String CLIENT_CONTEXT_ID = "CANCEL_PARAM";

    private CloseableHttpClient closeableHttpClient = null;
    private HttpPost apiHttpPost = null;
    private StringBuilder uri;

    public AsterixDBSourceLayer(final GSR gsr) {
        uri = new StringBuilder();
        uri.append("http://");
        final String host = gsr.getPropertyValue("host");
        if (!Strings.isNullOrEmpty(host)) {
            uri.append(host);
        }
        uri.append(":");
        final String port = gsr.getPropertyValue("port");
        if (!Strings.isNullOrEmpty(port)) {
            uri.append(port);
        }

    }

    /**
     * Connect to AsterixDB.
     */
    @Override
    public void connect() throws TatooineExecutionException {
        try {
            closeableHttpClient = HttpClients.createDefault();
            apiHttpPost = new HttpPost(uri.toString() + QUERY_SERVICE_PATH);
        } catch (Exception exception) {
            final String message = String.format("Cannot create default HttpClients: " + exception.getMessage());
            LOGGER.error(message);
            throw new TatooineExecutionException(message);
        }
    }

    /**
     * Fetches the next "N" results from a AsterixDB dataset that correspond to an input query
     * "N" refers to the value of the variable "resultsPerQuery" which is provided by the user.
     */
    @Override
    public Object getBatchRecords(Object[] parameters) throws TatooineExecutionException {
        final ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = null;
        final int length = parameters.length;
        if (parameters.length != 1) {
            final String message =
                    String.format("Method getBatchRecords called with %d parameters instead of 1", length);
            LOGGER.error(message);
            throw new TatooineExecutionException(message);
        }
        if (!(parameters[0] instanceof String)) {
            final String message = "Method getBatchRecords called with non-String parameters!";
            LOGGER.error(message);
            throw new TatooineExecutionException(message);
        }
        final String queryStr = String.valueOf(parameters[0]);

        try {
            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("statement", queryStr));
            params.add(new BasicNameValuePair("client_context_id", CLIENT_CONTEXT_ID));
            LOGGER.info(String.format("Querying AsterixDB with query \"%s\"", queryStr));
            apiHttpPost.setEntity(new UrlEncodedFormEntity(params));
            InputStream inputStream = closeableHttpClient.execute(apiHttpPost).getEntity().getContent();
            StringWriter writerInputstream = new StringWriter();
            IOUtils.copy(inputStream, writerInputstream, "UTF-8");
            final String result = writerInputstream.toString();
            jsonNode = objectMapper.readTree(result).get("results");
        } catch (IOException exception) {
            final String message = String.format("Error retrieving results from AsterixDB with query '%s': %s",
                    parameters[0], exception.getMessage());
            close();
            LOGGER.error(message);
            throw new TatooineExecutionException(message);
        }

        return jsonNode;
    }

    /**
     * Close AsterixDB connection.
     */
    public void close() {
        final CloseableHttpClient httpclient = HttpClients.createDefault();
        final HttpDelete endPoint =
                new HttpDelete(uri.toString() + QUERY_CANCEL_PATH + "?client_context_id=" + CLIENT_CONTEXT_ID);
        try {
            final InputStream inputStream = httpclient.execute(endPoint).getEntity().getContent();
            LOGGER.info("AsterixDB connection canceled.");
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException iOException) {
            LOGGER.error("Could not cancel job for id " + CLIENT_CONTEXT_ID, iOException);
        }

    }

    @Override
    public void reset() throws TatooineExecutionException {
        throw new UnsupportedOperationException("Unsupported");
    }

}
