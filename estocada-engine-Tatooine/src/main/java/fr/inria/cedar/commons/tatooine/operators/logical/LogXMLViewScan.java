package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.PatternType;
import fr.inria.cedar.commons.tatooine.constants.ViewType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

/**
 * This logical operator implements the scan of tuples from a database that was created when a view arrived to the system.
 * @author Ioana MANOLESCU
 */
public class LogXMLViewScan extends LogLeafOperator {
	private static final Logger log = Logger.getLogger(LogXMLViewScan.class);

	public LogXMLViewScan(StorageReference ref) throws TatooineException, TatooineExecutionException {
		this.ref = ref;
		String XAMString = this.ref.getPropertyValue("XAMString");
		// Parameters.logger.info("Got string: " + XAMString);
		String xScanName = null;

		try {
			pat = XAMParserUtility.getTreePatternFromString(XAMString, this.ref.getPropertyValue("ENVIRONMENT_PARA"));
			// x.init();
		} catch (Exception pe) {
			log.error("Exception: ", pe);
			throw new TatooineExecutionException(pe.toString());
		}
		this.setNRSMD(NRSMD.getNRSMD(((TreePattern) pat).getRoot(), true,
				new HashMap<Integer, HashMap<String, ArrayList<Integer>>>()));
		xScanName = pat.getName();

		this.setVisible(true);

		this.setOwnName("LogXMLViewScan(" + xScanName + ",|" + this.pat.toXAMString().replace('\n', ' ') + "|)");
	}

	public LogXMLViewScan(TreePattern qp) throws TatooineException, TatooineExecutionException {
		this.pat = qp;
		this.setVisible(true);
		this.setOwnName("LogXMLViewScan(" + qp.getName() + ")");
		this.setNRSMD(NRSMD.getNRSMD(qp.getRoot(), true, new HashMap<Integer, HashMap<String, ArrayList<Integer>>>()));
	}

	@Override
	public String getName() {

		if (pat != null) {
			String viewName = pat.getName().lastIndexOf('/') != -1 ? pat.getName().substring(
					pat.getName().lastIndexOf("/") + 1) : pat.getName();
			this.setOwnName("XAMScan(" + viewName + ",|" + this.pat.toXAMString().replace('\n', ' ') + "|)");
		}

		return this.getOwnName();

	}

	public StorageReference getStorageReference() {
		return this.ref;
	}

	/**
	 * Added a clone method in order to have 2 readers from the same XAM. This method first clones the Xam and create a
	 * new XAMScan operator that uses the same storage reference and the cloned Xam. BDB/Postgress should deal with
	 * concurrency in multithreaded environments. Thread safe?!
	 *
	 * @return a new XAMScan operators that reads data from the same BDB/POSTGRESS DB
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		try {
			TreePattern cloneXam = ((TreePattern) this.pat).deepCopy(); // viene de this.x.clone() es lo mismo??

			LogXMLViewScan newXS = new LogXMLViewScan(cloneXam);
			newXS.ref = ref;
			return newXS;

		} catch (Exception e) {
			log.error("Exception: ", e);
			return null;
		}
	}

	/**
	 * making a deep copy of the current operator
	 * 
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogXMLViewScan deepCopy() {
		LogXMLViewScan copy = null;

		try {
			if (this.ref == null) {
				log.error("Copying this XAMScan failed");
			} else {
				copy = new LogXMLViewScan(this.ref);
				copy.setEquivalentPattern(this.getEquivalentPattern());
			}
		} catch (Exception e) {
			log.error("Exception: ", e);
		}

		return copy;
	}

	@Override
	public int getJoinDepth() {
		return 0;
	}

	@Override
	public String toString() {
		return this.getOwnName();
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		int selfNumber = -1;
		if (isVisible()) {
			selfNumber = firstAvailableNo;

			if (pat.getName().startsWith(ViewType.COMPULSORY.toString())) {
				sb.append(selfNumber + " [color=\"" + PatternType.COMPULSORY.getForeground()
						+ "\" style=\"filled\" fillcolor=\"" + PatternType.COMPULSORY.getBackground() + "\" label=\""
						+ this.getOwnName() + "\"] ; \n");
			} else if (pat.getName().startsWith(ViewType.COLLABORATIVE.toString())) {
				sb.append(selfNumber + " [color=\"" + PatternType.COLLABORATIVE.getForeground()
						+ "\" style=\"filled\" fillcolor=\"" + PatternType.COLLABORATIVE.getBackground()
						+ "\" label=\"" + this.getOwnName() + "\"] ; \n");
			} else if (pat.getName().startsWith(ViewType.SELFISH.toString())) {
				sb.append(selfNumber + " [color=\"" + PatternType.SELFISH.getForeground()
						+ "\" style=\"filled\" fillcolor=\"" + PatternType.SELFISH.getBackground() + "\" label=\""
						+ this.getOwnName() + "\"] ; \n");
			} else {
				sb.append(selfNumber + " [color=\"" + PatternType.CONTENT.getForeground()
						+ "\" style=\"filled\" fillcolor=\"" + PatternType.CONTENT.getBackground() + "\" label=\""
						+ this.getOwnName() + "\"] ; \n");
			}

			if (parentNo != -1) {
				sb.append(parentNo + " -> " + selfNumber + "\n");
			}
			return (firstAvailableNo + 1);
		} else {
			return firstAvailableNo;
		}
	}

	@Override
	public double inputCardinality() {
		return Catalog.getInstance().lookUpViewSize(this.ref.getPropertyValue("ENVIRONMENT_PARA")).cardinality;
	}

	@Override
	public double estimatedIOCost() {
		return Catalog.getInstance().lookUpViewSize(this.ref.getPropertyValue("ENVIRONMENT_PARA")).bytes;
	}

	@Override
	public double estimatedCardinality() {
		return this.inputCardinality();
	}

}
