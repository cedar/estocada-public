package fr.inria.cedar.commons.tatooine.xam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.constants.PrintingLevel;
import fr.inria.cedar.commons.tatooine.constants.ValueJoinAttribute;

/**
 * Class that represents each node of a {@link TreePattern}.
 *
 * @author Ioana MANOLESCU
 * @author Konstantinos KARANASOS
 * @author Jesus CAMACHO RODRIGUEZ
 *
 * @created 13/06/2005
 */
public class TreePatternNode implements Serializable, Comparable<TreePatternNode> {
	
	private static final Logger log = Logger.getLogger(TreePatternNode.class);
	
	/** Universal version identifier for the TreePatternNode class */
	private static final long serialVersionUID = -1510356036670827009L;

	/** Global counter of nodes: it is used to number uniquely the nodes of several patterns */
	public static AtomicInteger globalNodeCounter = new AtomicInteger(1);

	/* Node related properties */
	/** If true, this node represents an attribute; if false, it represents an element */
	private boolean isAttribute;

	/** If the xam is built from a XAM, this is the hash code of the XAM node from which it originated */
	private int nodeCode; // Now XAMNodeHashCode has to point to this one also

	/* ID related properties */
	/** When {@link #storesID} is set to true, requiresID is always set to true */
	private boolean storesID;

	/**
	 * If true, the ID of this element must be known in order to access the data stored in the XAM */
	private boolean requiresID;

	/** 
	 * If true, "ID i" was specified for this node in the XAM file,
	 * so the identity ID for this node should be stored. 
	 */
	private boolean identityID;

	/** 
	 * If true, "ID o" was specified for this node in the XAM file, 
	 * so the order preserving ID for this node should be stored. 
	 */
	private boolean orderID;

	/**
	 * If true, "ID s" was specified for this node in the XAM file, 
	 * so the structural ID for this node should be stored.
	 */
	private boolean structID;

	/**
	 * If true, "ID u" was specified for this node in the XAM file, 
	 * so the update ID for this node should be stored.
	 */
	private boolean updateID;

	/* Tag related properties */
	/** If true, the tag of this element is stored */
	private boolean storesTag;

	/** If true, the tag of this element must be known in order to access the data stored in the XAM */
	private boolean requiresTag;

	/** If true, there is a selection on tag */
	private boolean selectOnTag;

	/** The namespace of this node */
	private String namespace;

	/** The tag of this node ([Tag="a"]), "" for the XAM root, "*" for any tag */
	private String tag;

	/* Value related properties */
	/** If true, the value of this element is stored */
	private boolean storesValue;

	/** If true, the value of this element must be known in order to access the data stored in the XAM */
	private boolean requiresVal;

	/** If true, there is a selection on value */
	private boolean selectOnValue;

	/** If selectOnValue is true, this is what the value should be ([Val="a"]) */
	private String value;

	/* Content related properties */
	/** If true, the content of this element is stored */
	private boolean storesContent;

	/* Edges to other nodes */
	/** Edge connecting this node to its parent */
	private PatternEdge parentEdge;

	/** Edges connecting this node to its children */
	private ArrayList<PatternEdge> edges;

	/**
	 * Top returning node:
	 * -1: not computed yet
	 * 0: false
	 * 1: true
	 */
	private int isTopReturningNode;

	private int nestingDepth;

	/** True if this node has been obtained by navigating inside a CONT attribute */
	private boolean virtual;


	/** Creates a new node with no children. All properties are set to false by default. */
	public TreePatternNode(String namespace, String tag, int nodeCode) {
		this.namespace = namespace;
		this.tag = tag;
		this.storesTag = false;
		if (tag == null || tag.compareTo("*") == 0) {
			this.namespace = "";
			this.tag = "*";
		} else {
			this.selectOnTag = true;
		}
		this.edges = new ArrayList<PatternEdge>();
		storesContent = false;
		storesValue = false;
		storesID = false;
		requiresID = false;
		requiresTag = false;
		requiresVal = false;
		isTopReturningNode = -1;
		this.nodeCode = nodeCode;
	}

	/** Compares TreePatternNodes based on their namespaces and tags */
	@Override
	public int compareTo(TreePatternNode pn) {
		int result = nodeExtendedString().compareTo(pn.nodeExtendedString());
		if(result == 0) {
			return treeToString(PrintingLevel.EXTENDED).compareTo(pn.treeToString(PrintingLevel.EXTENDED));
		}
		return result;
	}

	public void setVirtual(boolean isVirtual){
		this.virtual = isVirtual;
	}

	/** Checks whether the current node is a return node */
	public boolean isReturnNode() {
		return (this.storesID || this.storesValue || this.storesContent);
	}

	/** Checks if the current node is a leaf (has no children edges) */
	public boolean isLeaf() {
		return this.edges.isEmpty();
	}


	/** Counts the number of nodes in this subtree */
	public int getNumberOfNodes() {
		if (this.edges == null) {
			return 1;
		}
		if (this.edges.size() == 0) {
			return 1;
		}
		int aux = 1;
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			aux += it.next().n2.getNumberOfNodes();
		}
		return aux;
	}

	/** Counts the nodes in this subtree that return ID, CONT, and VAL */
	public int getNumberOfReturningNodes() {
		int aux = 0;
		if(this.storesContent || this.storesID || this.storesValue){
			aux++;
		}
		if (this.edges == null){
			return aux;
		}
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			aux += it.next().n2.getNumberOfReturningNodes();
		}
		return aux;
	}

	/** Counts the nodes in this subtree that return CONT and VAL */
	public int getNumberOfValContReturningNodes() {
		int aux = 0;
		if(this.storesContent || this.storesValue){
			aux++;
		}
		if (this.edges == null){
			return aux;
		}
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			aux += it.next().n2.getNumberOfValContReturningNodes();
		}
		return aux;
	}

	/**
	 * Removes the current node from the pattern.
	 * TODO: Konstantinos to check if it's needed to take care of the matches as well (I think so).
	 */
	public void removeNodeFromPattern() {
		// Remove this node from the list of children of its parent.
		TreePatternNode parentNode = this.getParentEdge().getN1();

		Iterator<PatternEdge> edgesIter = parentNode.getEdges().iterator();
		while (edgesIter.hasNext()) {
			PatternEdge childEdge = edgesIter.next();
			if (childEdge.getN2().getNodeCode() == this.nodeCode) {
				edgesIter.remove();
				return;
			}
		}
	}

	/**
	 * Removes from the list of children the one that has the given {@link #nodeCode}.
	 * @param childCode the nodeCode of the child that will be removed.
	 */
	public void removeChild(int childCode) {
		if (edges == null) {
			return;
		}
		Iterator<PatternEdge> edgeIter = edges.iterator();
		while (edgeIter.hasNext()) {
			PatternEdge edge = edgeIter.next();
			if (edge.getN2().getNodeCode() == childCode) {
				edgeIter.remove();
				break;
			}
		}
	}

	/**
	 * Removes from the list of children those whose code does not appear in the given set.
	 * @param childCodesToKeep if a child's code belongs to this list, it will not be removed.
	 */
	public void removeChildrenNotInList(HashSet<Integer> childCodesToKeep) {
		if (edges == null) {
			return;
		}
		Iterator<PatternEdge> edgeIter = edges.iterator();
		while (edgeIter.hasNext()) {
			PatternEdge edge = edgeIter.next();
			if (!childCodesToKeep.contains(edge.getN2().getNodeCode())) {
				edgeIter.remove();
			}
		}
	}

	/** Returns the node of the subtree whose {@link #nodeCode} is equal to a given value */
	public TreePatternNode locate(Integer nx2) {
		// Parameters.logger.info("Locate " + nx2 + " on " + this.tag + " " +
		// this.XAMNodeHashCode);
		if (this.nodeCode == nx2.intValue()) {
			return this;
		} else {
			Iterator<PatternEdge> it = this.edges.iterator();
			while (it.hasNext()) {
				TreePatternNode n2 = it.next().n2.locate(nx2);
				if (n2 != null) {
					return n2;
				}
			}
		}
		return null;
	}

	public TreePatternNode pruneIfUseless(TreePatternNode pn) {
		// Parameters.logger.info("Trying to prune " + pn.tag);
		if (pn.edges == null || pn.edges.size() == 0) {
			if (pn.storesContent) {
				return pn;
			}
			if (pn.storesID) {
				return pn;
			}
			// Parameters.logger.info("Pruning: node " + pn.tag + " needs tag: " +
			// pn.needsTag +
			// " selects on tag: " + pn.selectOnTag);
			if (pn.storesTag) {
				return pn;
			}
			if (pn.storesValue) {
				return pn;
			}

			return null;
		}

		Iterator<PatternEdge> it = pn.edges.iterator();
		TreePatternNode newParent = new TreePatternNode(pn.namespace, pn.tag, pn.nodeCode);
		newParent.isAttribute = pn.isAttribute;
		newParent.storesContent = pn.storesContent;
		newParent.storesID = pn.storesID;
		newParent.storesTag = pn.storesTag;
		// Parameters.logger.info("At simplification, new node created of tag " +
		// pn.tag + " needs tag: " +
		// newParent.needsTag + " " + newParent.toString());
		newParent.storesValue = pn.storesValue;
		newParent.selectOnTag = pn.selectOnTag;
		if (newParent.selectOnTag) {
			newParent.namespace = pn.namespace;
			newParent.tag = pn.tag;
		}
		newParent.selectOnValue = pn.selectOnValue;
		if (newParent.selectOnValue) {
			newParent.value = pn.value;
		}
		newParent.setIDType(pn.identityID, pn.orderID, pn.structID, pn.updateID);

		ArrayList<PatternEdge> newEdges = new ArrayList<PatternEdge>();

		while (it.hasNext()) {
			PatternEdge e = it.next();
			TreePatternNode n2 = e.n2;
			TreePatternNode n3 = pruneIfUseless(n2);
			if (n3 != null) {
				PatternEdge eNew = new PatternEdge(pn, n3, e.isParent(), e.isNested());
				newEdges.add(eNew);
				n3.parentEdge = eNew;
			} else { // n2 was useless
				// Parameters.logger.info("Useless node " + n2.tag + " under
				// a join");
				// is a join; transform it and all below it into
				// semijoin
				PatternEdge peNew = new PatternEdge(pn, n2, e.isParent(), e.isNested());
				peNew.setNested(e.isNested());
				newEdges.add(peNew);
				n2.parentEdge = peNew;
			}
		}
		Iterator<PatternEdge> it2 = newEdges.iterator();
		while (it2.hasNext()) {
			PatternEdge edg = it2.next();
			newParent.addEdge(edg.n2, edg.isParent(), edg.isNested());
		}
		return newParent;
	}




	/**
	 * FIXME: Asterios thinks that this function should not judge from the nodeID as it can mislead the code. 
	 * For example, what if a (legal) pattern like this is given: (-1)/site(-1)/catgraph(-1).
	 *
	 *  Why not locating nodes by their actual memory address (node1==node2)?
	 *  Is this preserved for a special reason? It has caused a bug in the estimations code
	 *  (where patterns were created with -1 code on all nodes).
	 * */
	public int pruneAllButPathTo(TreePatternNode n1) {
		// Parameters.logger.info("pruneAllButPathTo " + n1.tag + " in " +
		// this.tag);
		if (this.nodeCode == n1.nodeCode) {
			this.edges = (new ArrayList<PatternEdge>());
			// Parameters.logger.info("I am the target, erasing children");
			return 1;
		}
		ArrayList<PatternEdge> newEdges = (new ArrayList<PatternEdge>());
		Iterator<PatternEdge> it = this.edges.iterator();
		while (it.hasNext()) {
			PatternEdge thisEdge = it.next();
			TreePatternNode child = thisEdge.n2;
			// Parameters.logger.info("Moving to child " + child.tag);
			int k = child.pruneAllButPathTo(n1);
			// Parameters.logger.info("Child " + child.tag + " returned " + k);
			if (k == 1) {
				newEdges.add(thisEdge);
				this.edges = newEdges;
				// Parameters.logger.info("Copying child " + child.tag + ",
				// returning 1");
				return 1;
			}
		}
		// Parameters.logger.info("Erasing all children, returning");
		this.edges = newEdges;
		return 2;
	}

	/** Replaces the child n2 with the child newN in this node. Used by the new rewriting algorithm. */
	public void replaceChild(TreePatternNode n2, TreePatternNode newN) {
		Iterator<PatternEdge> ite = this.edges.iterator();
		while (ite.hasNext()){
			PatternEdge pe = ite.next();
			TreePatternNode peChild = pe.n2;
			if (peChild.nodeCode == n2.nodeCode){
				pe.n2 = newN;
				newN.parentEdge = pe;
			}
		}
	}

	/** A hack for hand-made rewritings (rewriteSpecific...) */
	public TreePatternNode findFirstDescendant(String thisTag) {
		Stack<TreePatternNode> s = new Stack<TreePatternNode>();
		s.push(this);
		while (!s.empty()){
			TreePatternNode node = s.pop();
			if (node.tag.compareTo(thisTag) == 0){
				return node;
			}
			Iterator<PatternEdge> ie = node.edges.iterator();
			while (ie.hasNext()){
				s.push(ie.next().n2);
			}
		}
		return null;
	}

	/** Gets the closest ancestor with an ID */
	public TreePatternNode getAncestorWithID() {
		if (parentEdge != null){
			if (parentEdge.n1.storesID){
				return parentEdge.n1;
			}
			else{
				return parentEdge.n1.getAncestorWithID();
			}
		}
		return null;
	}

	/** Returns the ID-return or ID-ancestor of the current node; null if such a node does not exist */
	public TreePatternNode findParentPathIdAncestor() {
		PatternEdge parEdge = this.parentEdge;
		while (parEdge != null) {
			// If the the parEdge is not a /-edge, stop immediately.
			if (!parEdge.isParent()) {
				return null;
			}

			TreePatternNode curParent = parEdge.getN1();
			// If current parent node is ID-return, return it, otherwise continue the upward traversal.
			if (curParent.storesID()) {
				return curParent;
			}
			else {
				parEdge = curParent.getParentEdge();
			}
		}
		return null;
	}

	/**
	 * Method that expands this pattern node with the attributes introduced in the parameters.
	 * @param nodesToUpdate a map with node codes as keys (unique identifiers) and a list of
	 * attributes that we would like those nodes to have
	 */
	protected void expandPattern(Map<Integer,ArrayList<ValueJoinAttribute>> nodesToUpdate) {
		// If we need something from this node for the value join evaluation
		if(nodesToUpdate.containsKey(nodeCode)) {
			ArrayList<ValueJoinAttribute> list = nodesToUpdate.get(nodeCode);
			for(ValueJoinAttribute attr:list) {
				switch (attr) {
				case ID:
					this.storesID = true;
					this.setIDType(false, false, true, false);
					break;
				case VAL:
					this.storesValue = true;
					break;
				case CONT:
					this.storesContent = true;
					break;
				case DOCID:
					break;
				}
			}
		}

		// We call recursively the method
		if (this.edges != null) {
			Iterator<PatternEdge> it = edges.iterator();
			while (it.hasNext()) {
				it.next().n2.expandPattern(nodesToUpdate);
			}
		}
	}

	/**
	 * Recursive method that removes the storage requirements of the subtree starting 
	 * from this node, skipping the nodes with the tag specified in the parameter.
	 * @param tagOfTuplesToKeep tag of the nodes whose storage requirements
	 * we want to preserve
	 */
	protected void parseUnrequiredData(String tagOfTuplesToKeep) {
		if(!this.tag.equals(tagOfTuplesToKeep)) {
			this.removeRequirementsToStoreData();
		}
		for(PatternEdge edge : this.edges) {
			edge.n2.parseUnrequiredData(tagOfTuplesToKeep);
		}
	}

	/**
	 * Removes the storage requirements from this node. It sets to false {@link #storesID}, 
	 * {@link #storesTag}, {@link #storesValue} and {@link #storesContent}.
	 */
	private void removeRequirementsToStoreData() {
		this.storesID = false;
		this.storesValue = false;
		this.storesContent = false;
		this.storesTag = false;
	}

	/**
	 * Removes the storage requirements from the subtree rooted at this node.
	 * It sets to false {@link #storesID}, {@link #storesTag}, {@link #storesValue}
	 * and {@link #storesContent} for all nodes of the subtree.
	 */
	public void removeRecRequirementsToStoreData() {
		this.storesID = false;
		this.storesValue = false;
		this.storesContent = false;
		this.storesTag = false;

		for (PatternEdge pe: this.getEdges()) {
			pe.n2.removeRecRequirementsToStoreData();
		}
	}

	/* Copy methods */
	/**
	 * Recursive method that returns the copy of the subtree starting from this node. 
	 * This method will preserve the {@link #nodeCode} of the nodes of the subtree.
	 * @return the root of the subtree copied
	 */
	public TreePatternNode deepCopy() {
		TreePatternNode aux = new TreePatternNode(this.namespace, this.tag, this.nodeCode);

		aux.setStoresContent(this.storesContent);
		aux.setStoresID(this.storesID);
		aux.setStoresTag(this.storesTag);
		aux.setStoresValue(this.storesValue);

		aux.setRequiresID(this.requiresID);
		aux.setRequiresTag(this.requiresTag);
		aux.setRequiresVal(this.requiresVal);

		aux.setNamespace(this.namespace);
		aux.setSelectOnTag(this.selectOnTag, this.namespace, this.tag);
		aux.setSelectOnValue(this.selectOnValue, this.value);

		aux.identityID = this.identityID;
		aux.orderID = this.orderID;
		aux.updateID = this.updateID;
		aux.structID = this.structID;

		aux.isAttribute = this.isAttribute;

		aux.virtual = this.virtual;

		aux.edges = (new ArrayList<PatternEdge>());
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			PatternEdge pe = it.next();
			TreePatternNode childCopy = pe.n2.deepCopy();
			aux.addEdge(childCopy, pe.isParent(), pe.isNested());
		}
		return aux;
	}

	/**
	 * Recursive method that returns the copy of the subtree starting from this
	 * node, but replacing the old node introduced as the first parameters for
	 * a new node introduced as the second parameter. This method will preserve
	 * the {@link #nodeCode} of the nodes of the subtree.
	 *
	 * @param oldNode the old node that we want to replace
	 * @param newNode the new node that will replace the old node
	 * @return the root of the subtree copied
	 */
	public TreePatternNode deepCopyWithReplace(TreePatternNode oldNode, TreePatternNode newNode) {
		if (this == oldNode){
			return newNode;
		}

		TreePatternNode aux = new TreePatternNode(this.namespace, this.tag, this.nodeCode);

		aux.storesContent = this.storesContent;
		aux.storesID = this.storesID;
		aux.storesTag = this.storesTag;
		aux.storesValue = this.storesValue;

		aux.setRequiresID(this.requiresID);
		aux.setRequiresTag(this.requiresTag);
		aux.setRequiresVal(this.requiresVal);

		aux.selectOnTag = this.selectOnTag;

		aux.selectOnValue = this.selectOnValue;
		if (aux.selectOnValue){
			aux.value = this.value;
		}

		aux.identityID = this.identityID;
		aux.orderID = this.orderID;
		aux.updateID = this.updateID;
		aux.structID = this.structID;

		aux.isAttribute = this.isAttribute;

		aux.edges = new ArrayList<PatternEdge>();

		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()){
			PatternEdge pe = it.next();
			TreePatternNode childCopy = null;
			if (pe.n2 == oldNode){
				childCopy = newNode;
			} else{
				childCopy = pe.n2.deepCopyWithReplace(oldNode, newNode);
			}
			aux.addEdge(childCopy, pe.isParent(), pe.isNested());
		}

		return aux;
	}

	/** Copies a node. This method will preserve the {@link #nodeCode} of the node. */
	public TreePatternNode nodeCopy() {
		TreePatternNode aux = new TreePatternNode(this.namespace, this.tag, this.nodeCode);

		aux.storesContent = this.storesContent;
		aux.storesID = this.storesID;
		aux.storesTag = this.storesTag;
		aux.storesValue = this.storesValue;

		aux.setRequiresID(this.requiresID);
		aux.setRequiresTag(this.requiresTag);
		aux.setRequiresVal(this.requiresVal);

		aux.selectOnTag = this.selectOnTag;
		if (aux.selectOnTag) {
			aux.namespace = this.namespace;
			aux.tag = this.tag;
		}

		aux.selectOnValue = this.selectOnValue;
		if (aux.selectOnValue) {
			aux.value = this.value;
		}

		aux.identityID = this.identityID;
		aux.orderID = this.orderID;
		aux.updateID = this.updateID;
		aux.structID = this.structID;

		aux.isAttribute = this.isAttribute;

		aux.virtual = this.virtual;
		return aux;
	}

	/**
	 * Method that copies the value of {@link #storesID}, {@link #storesValue} and {@link #storesContent}
	 * if they are set to true, from the node introduced in the parameters to this node.
	 * @param node the node whose storage parameters we will copy
	 */
	public void copyAttributesFrom(TreePatternNode node) {
		if (!this.storesValue && node.storesValue){
			this.storesValue = true;
		}
		if (!this.storesContent && node.storesContent){
			//Parameters.logger.info("Node " + this.tag + "(" + this.nodeCode + ") copied Cont from node " + node.tag + "(" + node.nodeCode + ")");
			this.storesContent = true;
		}
		if (!this.storesID && node.storesID){
			this.storesID = true;
		}
	}

	/**
	 * Copies all the children of the node introduced in the parameters as the children of this node.
	 * @param node the node whose children we want to copy
	 */
	public void copyChildrenFrom(TreePatternNode node) {
		Iterator<PatternEdge> it = node.edges.iterator();
		while (it.hasNext()) {
			PatternEdge e = it.next();
			TreePatternNode child = e.n2;
			addEdge(child.deepCopy(), e.isParent(), e.isNested());
		}
	}

	/**
	 *
	 * @param edge
	 */
	public void copyVirtualChild(PatternEdge edge){
		TreePatternNode childCopy = edge.n2.deepCopy();
		// marking childCopy and its subtree as virtual:
		Stack<TreePatternNode> st = new Stack<TreePatternNode>();
		st.push(childCopy);
		while (!st.empty()) {
			TreePatternNode pn = st.pop();
			// Parameters.logger.info("Set virtual node: " + pn.tag);
			pn.virtual = true;
			pn.nodeCode = TreePatternNode.globalNodeCounter.getAndIncrement();
			// TODO: Konstantinos to change this (our virtual nodes will be ID-return).
			// virtual nodes obtained by navigation cannot store ID
			pn.storesID = false;
			Iterator<PatternEdge> pnChildren = pn.edges.iterator();
			while (pnChildren.hasNext()) {
				PatternEdge pnEdge = pnChildren.next();
				st.push(pnEdge.n2);
			}
		}
		addEdge(childCopy, edge.isParent(), edge.isNested());
	}

	/* Auxiliary functions to getters and setters */

	/** Returns true if there is some required field underneath this node */
	public  boolean requiresSomething() {
		if (this.requiresID || this.requiresTag || this.requiresVal) {
			return true;
		}
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			if (it.next().n2.requiresSomething()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Recursive method that returns true if any node in the subtree starting 
	 * from this node stores the ID, the tag, the value or the content.
	 * @return if the subtree starting from this node stores something
	 */
	public boolean deepStoresSomething() {
		if (this.storesID) {
			return true;
		}
		if (this.storesTag) {
			return true;
		}
		if (this.storesValue) {
			return true;
		}
		if (this.storesContent) {
			return true;
		}
		if (this.edges != null) {
			Iterator<PatternEdge> it = edges.iterator();
			while (it.hasNext()) {
				TreePatternNode child = it.next().n2;
				if (child.deepStoresSomething()) {
					return true;
				}
			}
		}
		return false;
	}

	/** Recursive method that returns true if this node stores an ID, tag, value or content */
	public boolean nodeStoresSomething() {
		if(this.storesID || this.storesTag ||
				this.storesValue || this.storesContent) {
			return true;
		}
		return false;
	}

	/**
	 * Returns what properties (ID, VAL, CONT) are stored for a node.
	 * @return a boolean array of size 3 where the 0th position represents ID, 1st VAL and 2nd CONT.
	 * These positions are set to <code>true</code> if they are stored, otherwise <code>false</code>.
	 */
	public boolean[] getStoredProperties() {
		boolean[] storedProperties = new boolean[3];
		if(this.storesID) {
			storedProperties[0] = true;
		} else {
			storedProperties[0] = false;
		}
		if(this.storesValue) {
			storedProperties[1] = true;
		} else {
			storedProperties[1] = false;
		}
		if(this.storesContent) {
			storedProperties[2] = true;
		} else {
			storedProperties[2] = false;
		}
		return storedProperties;
	}

	/**
	 * Returns the columns required for this node.
	 * @param the boolean array returned from getStoredProperties
	 * @see fr.inria.cedar.commons.tatooine.xam.TreePatternNode#getStoredProperties()
	 */
	public int[] getStoredColumns(boolean[] storedProperties) {
		ArrayList<Integer> columns = new ArrayList<Integer>();

		for(int i = 0; i<3; i++) {
			if(storedProperties[i] == true) {
				columns.add(i);
			}
		}

		int[] columnsArray = new int[columns.size()];
		for(int j = 0; j<columns.size(); j++) {
			columnsArray[j] = columns.get(j);
		}
		return columnsArray;
	}


	/** Gets the root of the tree pattern to which the given node belongs */
	public TreePatternNode getRoot(TreePatternNode n1) {
		if (n1.parentEdge == null) {
			return n1;
		} else {
			return getRoot(n1.parentEdge.n1);
		}
	}

	/** Returns the descendants of this node in BFS order */
	public ArrayList<TreePatternNode> getBFSOrderedDescendants(){
		ArrayList<TreePatternNode> nodes = new ArrayList<TreePatternNode>();

		//BFS uses Queue data structure
		Queue<TreePatternNode> q=new LinkedList<TreePatternNode>();
		q.add(this);
		nodes.add(this);

		while(!q.isEmpty()){
			TreePatternNode n=q.remove();

			for(TreePatternNode child: n.getChildrenList()){
				nodes.add(child);
				q.add(child);
			}
		}

		return nodes;
	}

	public LinkedList<TreePatternNode> getNodes() {
		if (this.edges == null) {
			return null;
		}
		if (this.edges.size() == 0) {
			return null;
		}
		LinkedList<TreePatternNode> nodes = new LinkedList<TreePatternNode>();
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			nodes = it.next().n2.getNodesRec(nodes);
		}
		return nodes;
	}

	private LinkedList<TreePatternNode> getNodesRec(LinkedList<TreePatternNode> nodes) {
		nodes.add(this);
		if (this.edges != null) {
			Iterator<PatternEdge> it = edges.iterator();
			while (it.hasNext()) {
				nodes = it.next().n2.getNodesRec(nodes);
			}
		}
		return nodes;
	}

	/** Returns a list with all the nodes of the subtree rooted at the current node */
	public ArrayList<TreePatternNode> getNodes(boolean onlyReturnNodes) {
		ArrayList<TreePatternNode> returnNodesList = new ArrayList<TreePatternNode>();

		Stack<TreePatternNode> stack = new Stack<TreePatternNode>();
		stack.push(this);
		if (!onlyReturnNodes || this.isReturnNode()) {
			returnNodesList.add(this);
		}

		while (!stack.empty()){
			TreePatternNode node = stack.pop();

			for (PatternEdge edge: node.getEdges()) {
				stack.push(edge.n2);

				if (!onlyReturnNodes || edge.n2.isReturnNode()) {
					returnNodesList.add(edge.n2);
				}
			}
		}

		return returnNodesList;
	}

	/**
	 * Traverses the current sub-pattern and populate the map that is given as input.
	 * This map contains as key the node code of each sub-pattern node and as value
	 * the corresponding sub-pattern node.
	 * @param codeToNodeMap the map
	 */
	public void createCodeToNodeMap(HashMap<Integer,TreePatternNode> codeToNodeMap) {
		codeToNodeMap.put(this.nodeCode, this);
		if (this.edges != null) {
			for (PatternEdge edge : this.edges) {
				edge.getN2().createCodeToNodeMap(codeToNodeMap);
			}
		}
	}

	/**
	 * Adds an edge connecting this node to a child node. It also marks the
	 * child node as being existential, if this is the case.
	 * @param the child node.
	 * @param whether or not the edge is a parent-child edge (otherwise, it is ancestor-descendant).
	 * @param nested
	 */
	public void addEdge(TreePatternNode child, boolean parent, boolean nested) {
		PatternEdge e = new PatternEdge(this, child, parent, nested);

		this.edges.add(e);
		child.parentEdge = e;
	}

	/** Adds an edge to our TreePatternNode */
	public void addEdge(PatternEdge pe) {
		if(this.edges == null) {
			this.edges = new ArrayList<PatternEdge>();
		}
		this.edges.add(pe);
	}

	/** Removes an edge from our TreePatternNode */
	public void removeEdge(PatternEdge pe) {
		edges.remove(pe);
	}

	/** Clears all edges from our TreePatternNode */
	public void cleanEdges() {
		this.edges = new ArrayList<PatternEdge>();
	}

	/** Returns a list of the children on the specific XAM node */
	public ArrayList<TreePatternNode> getChildrenList() {
		ArrayList<TreePatternNode> children = new ArrayList<TreePatternNode>();
		for(PatternEdge pe: edges) {
			TreePatternNode n2 = pe.n2;
			children.add(n2);
		}
		return children;
	}

	/* Getters/setters */
	/* Node related getters/setters */
	/** If true, this node represents an attribute; otherwise, an element */
	public boolean isAttribute() {
		return this.isAttribute;
	}

	/** Defines whether a node represents an attribute (true) or an element (false) */
	public void setAttribute(boolean isAttribute) {
		this.isAttribute = isAttribute;
	}

	/** If the xam is built from a XAM, this is the hash code of the XAM node from which it originated */
	public int getNodeCode() {
		return nodeCode;
	}

	/** Sets the hash code of the XAM node */
	public void setNodeCode(int nodeCode) {
		this.nodeCode = nodeCode;
	}

	/* ID related getters/setters */
	/**
	 * If true, the ID of this element is required. 
	 * When {@link #storesID} is set to true, requiresID is always set to true
	 * @return the storesID
	 */
	public boolean storesID() {
		return this.storesID;
	}
	
	/** Sets the variable {@link #storesID} to the given value */
	public void setStoresID(boolean storesID) {
		this.storesID = storesID;
	}

	/** If true, the ID of this element is must be known in order to access the data stored in the XAM */
	public boolean requiresID(){
		return this.requiresID;
	}

	/** Sets the variable {@link #requiresID} to the given value */
	public void setRequiresID(boolean requiresID){
		this.requiresID = requiresID;
	}

	/**
	 * If true, "ID i" was specified for this node in the XAM file, 
	 * so the identity ID for this node should be stored.
	 */
	public boolean isIdentityIDType() {
		return this.identityID;
	}

	/**
	 * If true, "ID o" was specified for this node in the XAM file, 
	 * so the order preserving ID for this node should be stored.
	 */
	public boolean isOrderIDType() {
		return this.orderID;
	}

	/**
	 * If true, "ID s" was specified for this node in the XAM file, 
	 * so the structural ID for this node should be stored.
	 */
	public boolean isStructIDType() {
		return this.structID;
	}

	/**
	 * If true, "ID u" was specified for this node in the XAM file, 
	 * so the update ID for this node should be stored.
	 */
	public boolean isUpdateIDType() {
		return this.updateID;
	}

	/** This method sets which type of ID will be stored for this node */
	public void setIDType(boolean identityID, boolean orderID, boolean structID, boolean updateID) {
		this.identityID = identityID;
		this.orderID = orderID;
		this.structID = structID;
		this.updateID = updateID;
	}

	/* Tag related getters/setters */
	/** If true, the tag of this element is stored */
	public boolean storesTag() {
		return this.storesTag;
	}

	/** If it is set to true, the tag of this element is stored */
	public void setStoresTag(boolean storesTag) {
		this.storesTag = storesTag;
	}

	/** If true, the tag of this element must be known in order to access the data stored in the XAM */
	public boolean requiresTag(){
		return this.requiresTag;
	}

	/** This method sets the variable {@link #requiresTag} to the given value */
	public void setRequiresTag(boolean requiresTag){
		this.requiresTag = requiresTag;
	}

	/** If true, there is a selection on tag */
	public boolean selectsTag() {
		return this.selectOnTag;
	}

	/** If it is set to true, there is a selection on the namespace and tag specified */
	public void setSelectOnTag(boolean selectOnTag, String namespace, String tag) {
		this.selectOnTag = selectOnTag;
		if (selectOnTag) {
			this.namespace = namespace;
			this.tag = tag;
		} else {
			this.namespace = "";
			this.tag = "*";
		}
	}

	/** Returns the namespace stored for this node */
	public String getNamespace() {
		return namespace;
	}

	/** Sets the {@link #namespace} for this node */
	public void setNamespace(String namespace) {
		this.namespace = namespace;

	}

	/** Returns the tag stored for this node */
	public String getTag() {
		return tag;
	}

	/** Sets the {@link #tag} for this node */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/* Value related getters/setters */
	/** If true, the value of this element is stored */
	public boolean storesValue() {
		return this.storesValue;
	}

	/** If set to true, the value of this element is stored */
	public void setStoresValue(boolean storesValue) {
		this.storesValue = storesValue;
	}

	/** If true, the value of this element must be known in order to access the data stored in the XAM */
	public boolean requiresVal(){
		return this.requiresVal;
	}

	/** This method sets the variable {@link #requiresVal} to the given value */
	public void setRequiresVal(boolean requiresVal){
		this.requiresVal = requiresVal;
	}

	/** If true, there is a selection on value */
	public boolean selectsValue() {
		return this.selectOnValue;
	}

	/** Returns the {@link #value} for this node */
	public String getValue() {
		return this.value;
	}

	/** If it is set to true, there is a selection on the value specified */
	public void setSelectOnValue(boolean selectOnValue, String value) {
		this.selectOnValue = selectOnValue;
		if (selectOnValue) {
			this.value = value;
		}
	}

	/* Content related getters/setters */
	/** If true, the content of this element is stored */
	public boolean storesContent() {
		return this.storesContent;
	}

	/** If set to true, the content of this element is stored */
	public void setStoresContent(boolean storesContent) {
		this.storesContent = storesContent;
	}

	/* Edges related getters/setters */
	/** Returns the edge that connects this node to its parent/ancestor */
	public PatternEdge getParentEdge() {
		return this.parentEdge;
	}

	/** Returns the list of edges that connect this node to its children/descendants */
	public ArrayList<PatternEdge> getEdges() {
		return this.edges;
	}

	/** Sets the list of edges that connect this node to its children/descendants */
	public void setEdges(ArrayList<PatternEdge> edges) {
		this.edges = edges;
	}

	/* Other properties getters/setters */

	/**
	 * Returns true if this node is the top returning node of the tree pattern.
	 * Assumes that the node returns at least an ID, so we don't check for that.
	 * Also assumes that the node selects on the tag and does not return it.
	 * @return isTopReturningNode
	 */
	public boolean isTopReturningNode() {
		if (this.isTopReturningNode == 0){
			return false;
		}
		if (this.isTopReturningNode == 1){
			return true;
		}
		// otherwise, it is -1, and we need to look
		if (this.parentEdge == null){
			return true;
		}
		TreePatternNode n2 = this.parentEdge.n1;
		Stack<TreePatternNode> sn = new Stack<TreePatternNode>();
		sn.push(n2);
		while (!sn.empty()){
			TreePatternNode n3 = sn.pop();
			if (n3 == null){
				this.isTopReturningNode = 1;
				return true;
			}
			if (n3.storesID || n3.storesValue || n3.storesContent){
				this.isTopReturningNode = 0;
				return false;
			}
			if (n3.parentEdge != null){
				sn.push(n3.parentEdge.n1);
			}
		}
		this.isTopReturningNode = 1;
		return true;
	}

	/** The nested depth of this node */
	public int getNestingDepth() {
		return this.nestingDepth;
	}

	/** Builds the nested depth from this node */
	public void setNestingDepth() {
		Stack<TreePatternNode> st = new Stack<TreePatternNode>();
		st.push(this);
		int currentDepth = 0;
		Stack<Integer> st2 = new Stack<Integer>();
		st2.push(new Integer(currentDepth));

		while (!st.empty()) {
			TreePatternNode x = st.pop();
			Integer y = st2.pop();
			x.nestingDepth = y.intValue();

			Iterator<PatternEdge> it = x.edges.iterator();
			while (it.hasNext()) {
				PatternEdge e = it.next();
				st.push(e.n2);
				if (e.isNested()) {
					st2.push(new Integer(x.nestingDepth + 1));
				} else {
					st2.push(y);
				}
			}
		}
	}

	/** Returns true if this node is virtual */
	public boolean isVirtual() {
		return virtual;
	}

	/* Displaying and printing methods */
	/**
	 * Recursive method that transforms the subtree starting from this node into its equivalent in a XAM file.
	 * @param nodeBuffer buffer that will be filled with the definition of each node
	 * @param edgeBuffer buffer that will be filled with the definition of the edges
	 * @param parentNo the node code of the last parent
	 * @param availableNo the node code of the actual node
	 * @return the next available node code
	 */
	protected int toXAMString(StringBuffer nodeBuffer, StringBuffer edgeBuffer, int parentNo, int availableNo) {
		nodeBuffer.append("\n");
		int myCode = availableNo;
		int nextAvailable = availableNo + 1;
		// Parameters.logger.info("toXAMString on " + this.tag + " tag value: " +
		// this.tagValue
		// + " needs Tag: " + this.needsTag + " selects on tag: " +
		// this.selectOnTag);
		if (!this.isAttribute) {
			nodeBuffer.append("E: ");
		} else {
			nodeBuffer.append("A: ");
		}
		nodeBuffer.append(availableNo);
		// nodeBuffer.append(nodeCode);	//Konstantinos
		nodeBuffer.append(" ");
		if (this.storesID) {
			nodeBuffer.append(" ID ");
			if (this.updateID) {
				nodeBuffer.append("u ");
			}
			if (this.structID
					|| (!this.orderID && !this.updateID && !this.structID)) {
				nodeBuffer.append("s ");
			}
			if (this.orderID || this.identityID) {
				nodeBuffer.append("i ");
			}
		}
		if (this.requiresID) {
			nodeBuffer.append(" R ");
		}
		if (this.selectOnTag) {
			nodeBuffer.append("[Tag=\"");
			if (!this.namespace.equals("")) {
				nodeBuffer.append("{" + this.namespace + "}");
			}
			nodeBuffer.append(this.tag + "\"] ");
		}
		if (this.storesTag) {
			nodeBuffer.append("Tag ");
		}
		if (this.requiresTag){
			nodeBuffer.append(" R ");
		}

		if (this.selectOnValue) {
			nodeBuffer.append("[Val=\"" + this.value + "\"] ");
		}
		if (this.storesValue) {
			nodeBuffer.append("Val ");
		}
		if (this.requiresVal) {
			nodeBuffer.append(" R ");
		}
		if (this.storesContent) {
			nodeBuffer.append("Cont ");
		}
		Iterator<PatternEdge> itE = this.edges.iterator();
		while (itE.hasNext()) {
			PatternEdge pe = itE.next();
			TreePatternNode child = pe.n2;
			edgeBuffer.append(myCode + "," + nextAvailable + " ");
			// edgeBuffer.append(nodeCode + "," + pe.n2.nodeCode + " ");	// Konstantinos
			if (pe.isParent()) {
				edgeBuffer.append("/");
			} else {
				edgeBuffer.append("//");
			}
			if (pe.isNested()) {
				edgeBuffer.append("n");
			}
			edgeBuffer.append("j");
			edgeBuffer.append("\n");
			nextAvailable = child.toXAMString(nodeBuffer, edgeBuffer, myCode, nextAvailable);
		}
		return nextAvailable;
	}

	/**
	 * This method informally display the tree starting from this node.
	 * Optional edges are marked with "?". Edges that lead to children
	 * are nested within parenthesis in the parent.
	 * We can specify several level of details while transforming the tree into a String.
	 * The levels of detail are defined in the {@link PrintingLevel} enumeration.
	 *
	 * @param level of detail
	 * @return the tree representation
	 */
	public String treeToString(PrintingLevel level) {
		StringBuffer sb = new StringBuffer();
		this.recTreeToString(sb, level);
		return new String(sb);
	}

	private void recTreeToString(StringBuffer sb, PrintingLevel level) {
		switch (level) {
		case STRUCTURE_ONLY:
			sb.append(nodeStringStructureOnly());
			break;
		case SIMPLIFY:
			sb.append(nodeString());
			break;
		case EXTENDED:
			sb.append(nodeExtendedString());
			break;
		default:
			log.error("Printing level not defined properly: I don't know how to print patterns!");
		}
		if (edges.size() > 1) {
			sb.append("(");
		}
		Iterator<PatternEdge> it = edges.iterator();
		while (it.hasNext()) {
			PatternEdge e = it.next();
			e.n2.recTreeToString(sb, level);
			sb.append(" ");
		}
		if (edges.size() > 1) {
			sb.append(")");
		}

	}

	/** Prints the node simplify representation */
	@Override
	public String toString() {
		return nodeString();
	}


	/** Node simplify representation */
	private String nodeString() {
		StringBuffer sb = new StringBuffer();

		if(parentEdge!=null) {
			if (parentEdge.isParent()) {
				sb.append("/");
			} else {
				sb.append("//");
			}
		}

		if (virtual) {
			sb.append("~");
		}
		sb.append(this.isAttribute ? "@" : "");
		if (!this.namespace.equals("")) {
			sb.append(this.namespace + ":");
		}
		sb.append(this.tag);// + ":" + this.nodeCode);
		if (this.storesID){
			sb.append("*");
		}
		if (this.storesValue){
			sb.append("V");
		}
		if (this.storesContent){
			sb.append("C");
		}
		if (this.selectOnValue) {
			sb.append(" {=" + this.value + "} ");
		}
		return new String(sb);
	}

	/** Node simplify representation */
	private String nodeStringStructureOnly() {
		StringBuffer sb = new StringBuffer();
		if(parentEdge!=null) {
			if (parentEdge.isParent()) {
				sb.append("/");
			} else {
				sb.append("//");
			}
		}

		sb.append(this.isAttribute ? "@" : "");
		if (!this.namespace.equals("")) {
			sb.append(this.namespace + ":");
		}
		sb.append(this.tag);
		return new String(sb);
	}

	/**
	 * Extended node representation used for comparing uniquely two nodes 
	 * in the method {@link #compareTo(TreePatternNode)}
	 * @return the extended full node representation
	 */
	private String nodeExtendedString() {
		StringBuffer sb = new StringBuffer();

		if(parentEdge!=null) {
			if (parentEdge.isParent()) {
				sb.append("/");
			} else {
				sb.append("//");
			}
		}

		sb.append(this.virtual ? "~" : "");

		sb.append(this.isAttribute ? "@" : "");
		if (this.selectOnTag) {
			if (!this.namespace.equals("")) {
				sb.append(this.namespace + ":");
			}
			sb.append(this.tag);
			sb.append("(" + this.getNodeCode() + ")");
		}
		if (this.storesTag) {
			sb.append("_S");
		}
		if (this.requiresTag) {
			sb.append("R");
		}
		if (this.storesID) {
			sb.append("*");
		}
		if (this.requiresID) {
			sb.append("R");
		}
		if (this.selectOnValue) {
			sb.append("[V=" + this.value + "]");
		}
		if (this.storesValue) {
			sb.append("#V");
		}
		if (this.requiresVal) {
			sb.append("R");
		}
		if (this.storesContent) {
			sb.append("#C");
		}
		return new String(sb);
	}

	/**
	 * Adds the code for the graphical representation of this XAM node to the StringBuffer
	 * and then calls the recursiveDraw of its children.
	 * @author Spyros ZOUPANOS
	 */
	public void drawTree(StringBuffer sb, String backgroundColor, String foregroundColor) {
		sb.append(-this.hashCode() + " [ fontname=\"Lucida Grande\" fontcolor=\"white\" ] ; \n");

		for(int i =0; i<edges.size(); i++) {
			edges.get(i).n2.recursiveDrawTree(sb, -this.hashCode(), backgroundColor, foregroundColor);
		}
	}

	private void recursiveDrawTree(StringBuffer sb, int parentNodeCode, String backgroundColor, String foregroundColor) {
		sb.append(nodeCode + " [");

		if (storesContent) {
			sb.append(" shape=\"trapezium\" color=\"" + foregroundColor + "\" style=\"filled\" fillcolor=\"" + backgroundColor + "\" ");
		} else if (storesValue) {
			sb.append(" color=\"" + foregroundColor + "\" style=\"filled\" fillcolor=\"" + backgroundColor + "\" ");
		} else {
			sb.append(" color=\"" + foregroundColor + "\" ");
		}

		if (this.selectOnTag && this.tag.length() > 0) {
			sb.append(" label=\"");
			if (this.isAttribute) {
				sb.append("@");
			}
			if (this.namespace.equals("")) {
				sb.append( this.tag );
			} else if(this.namespace.length() > 15) {
				sb.append( this.namespace.substring(0, 6) + "..." + this.namespace.substring(this.namespace.length()-6, this.namespace.length()) + ":" + this.tag );
			} else {
				sb.append( this.namespace + ":" + this.tag );
			}
			if (this.storesID) {
				sb.append(" &bull;");
			}
			if (this.selectOnValue) {
				sb.append("\\n[\\\"" + this.value + "\\\"]");
			}
			sb.append("\" ");
		}

		sb.append("] ; \n");

		// if we want to design a parent-child edge, we add it one time
		sb.append(parentNodeCode + " -- " + nodeCode + " ; \n");
		if(!parentEdge.isParent()) {
			// if we want to design an ancestor-descendant edge, we add it one more time
			sb.append(parentNodeCode + " -- " + nodeCode + " ; \n");
		}

		for(int i =0; i<edges.size(); i++) {
			edges.get(i).n2.recursiveDrawTree(sb, nodeCode, backgroundColor, foregroundColor);
		}
	}

	/** Returns the name of the columns that this node stores */
	protected ArrayList<String> getColumnsName() {
		ArrayList<String> list = new ArrayList<String>();

		if(this.storesID) {
			list.add(nodeCode + ".ID");
		}
		if(this.storesTag) {
			list.add(nodeCode + ".Tag");
		}
		if(this.storesValue) {
			list.add(nodeCode + ".Val");
		}
		if(this.storesContent) {
			list.add(nodeCode + ".Cont");
		}

		return list;
	}

	/**
	 * Boolean showing whether the ID of the current node is used in a rewriting.
	 * Used in the view selection algorithm.
	 */
	private boolean requiredForRewriting = false;

	public boolean usedInRewriting(){
		return this.requiredForRewriting;
	}

	public void setUsedInRewriting(boolean used){
		this.requiredForRewriting = used;
	}


}
