package fr.inria.cedar.commons.tatooine.constants;

public enum AggregateFunction {
	MIN, MAX, AVG, SUM, COUNT
}
