package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Pre-post-depth element ID.
 *
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public class PrePostDepthElementID implements ElementID, Serializable {
	private static final Logger log = Logger.getLogger(PrePostDepthElementID.class);
	
	/** Universal version identifier for the PrePostDepthElementID class */
	private static final long serialVersionUID = -7955485825469353904L;

	/* Constants */
	private static final String DELIMITER = Parameters.getProperty("delimiter");

	public int pre;
	public int post;
	public int depth;

	public static PrePostDepthElementID theNull = new PrePostDepthElementID(-1, -1);

	public PrePostDepthElementID(int pre, int depth) {
		this.pre = pre;
		this.depth = depth;
	}
	
	@Override
	public int getType() {
		// TODO
		return 0;
	}

	@Override
	public String toString() {
		if (this == theNull) {
			return TupleMetadataType.NULL.toString();
		}
		return new String(pre + DELIMITER + post + DELIMITER + depth);
	}

	/** Returns true if ID1 is a parent of ID2 */
	@Override
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException {
		try {
			PrePostDepthElementID other = (PrePostDepthElementID) id2;
			if (other.pre > this.pre) {
				if (other.post < this.post) {
					if (other.depth == this.depth + 1) {
						return true;
					}
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Exception: ", e);
			throw new TatooineExecutionException(e.toString());
		}
	}

	/** Returns true if ID1 is an ancestor of ID2 */
	@Override
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException {
		try {
			PrePostDepthElementID other = (PrePostDepthElementID) id2;
			if (other.pre > this.pre) {
				if (other.post < this.post) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Exception: ", e);
			throw new TatooineExecutionException(e.toString());
		}
	}

	/** Returns the parent ID if it can be computed */
	@Override
	public ElementID getParent() throws TatooineExecutionException {
		throw new TatooineExecutionException("PrePostDepthElementID cannot infer parent");
	}

	/** Returns the null element for this kind of ID */
	@Override
	public ElementID getNull() {
		return theNull;
	}

	/** Returns true if this element ID is null */
	@Override
	public boolean isNull() {
		return (this == getNull());
	}

	/** Returns true if ID1 starts strictly after ID2 */
	@Override
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException {
		PrePostDepthElementID other = null;
		try {
			other = (PrePostDepthElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply startAfter on different ID schemes");
		}
		return (this.pre > other.pre);
	}

	/** Returns true if ID1 ends strictly after ID2 */
	@Override
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException {
		PrePostDepthElementID other = null;
		try {
			other = (PrePostDepthElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply startAfter on different ID schemes");
		}
		return (this.post > other.post);
	}

	public void setPost(int post2) {
		this.post = post2;
	}
	
	@Override
	public boolean equals(Object o) {
		try {
			PrePostDepthElementID id2 = (PrePostDepthElementID) o;
			return (this.pre == id2.pre && this.post == id2.post && this.depth == id2.depth);
		} catch (ClassCastException cce) {
			log.error("ClassCastException: ", cce);
			return false;
		}
	}

	@Override
	public int hashCode() {
		return ((new Integer(this.pre)).hashCode());
	}

}
