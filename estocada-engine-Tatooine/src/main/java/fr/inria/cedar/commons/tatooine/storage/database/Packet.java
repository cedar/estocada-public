package fr.inria.cedar.commons.tatooine.storage.database;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;

/**
 * This class is used as a container in order to send compressed
 * NTuples from the Send operator to the Receive operator.
 *
 * @author Spyros ZOUPANOS
 */
public class Packet implements Serializable {
	private static final Logger log = Logger.getLogger(Packet.class);
	private static final long serialVersionUID = 393683054582676856L;

	HashMap<Long, String> partIdToUriMap;
	public List<NTuple> ntContainer;

	public Packet()
	{
		partIdToUriMap = new HashMap<Long, String>();
		ntContainer = new ArrayList<NTuple>();
	}


	public Packet(HashMap<Long, String> partIdToUriMap, List<NTuple> ntContainer) {
		this.partIdToUriMap = partIdToUriMap;
		this.ntContainer = ntContainer;
	}

	public Packet(String singleUri, List<NTuple> ntContainer) {
		this.partIdToUriMap = new HashMap<Long, String>();
		this.partIdToUriMap.put(0L, singleUri);
		this.ntContainer = ntContainer;
	}

	public void clear()
	{
		if(partIdToUriMap != null) {
			partIdToUriMap.clear();
		}
		if(ntContainer != null) {
			ntContainer.clear();
		}
	}

	public char[] getSingleUri()
	{
		assert partIdToUriMap.size() == 1;
		return partIdToUriMap.values().iterator().next().toCharArray();
	}

	public List<NTuple> getTuples()
	{
		return ntContainer;
	}

	/**
	 * This unpickles a packet from a byte array. The format is assumed to be the same
	 * as the one used by {@link #toByteArray()}.
	 *
	 * @param buf The byte array to read the packet from
	 * @throws IOException if the byte array has the wrong format
	 */
	public Packet(byte[] buf) throws IOException
	{
		this();
		DataInputStream in = new DataInputStream(new ByteArrayInputStream(buf));
		// unpickle partIdToUriMap
		int uriCount = in.readInt();
		for ( int i=0; i<uriCount; i++ )
		{
			long key = in.readLong();
			String val = in.readUTF();
			partIdToUriMap.put(key, val);
		}
		// unpickle the tuples
		ntContainer = new ArrayList<NTuple>(NTuple.fromDataInput(in));
	}

	/**
	 * <p>This pickles a packet and returns it as a byte array. The output format is:</p>
	 *
	 * <ul>
	 * <li>Int <i>docIDCount</i></li>
	 * <li><i>docIDCount</i> times:<ul>
	 * <li>Int <i>partID</i></li>
	 * <li>UTF <i>docID</i></li>
	 * </ul></li>
	 * <li>The NTuples, as output by {@link }</li>
	 * </ul>
	 *
	 * @return a byte array containing the packet in pickled form
	 */
	public byte[] toByteArray() {
		try {
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(byteOut);
			// pickle partIdToUriMap
			if ( partIdToUriMap != null ) {
				out.writeInt(partIdToUriMap.size());
				for ( Entry<Long, String> i: partIdToUriMap.entrySet() ) {
					out.writeLong(i.getKey());
					out.writeUTF(i.getValue());
					out.flush();
				}
			} else {
				out.writeInt(0);
				out.flush();
			}
			// pickle the tuples
			NTuple.toDataOutput(ntContainer, out);
			return byteOut.toByteArray();
		} catch (IOException e) {
			log.error(e);
			return null;
		}
	}

	public void setTuples(List<NTuple> ntContainer) {
		this.ntContainer = ntContainer;
	}
}

