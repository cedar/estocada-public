package fr.inria.cedar.commons.tatooine.loader;

import java.util.ArrayList;

import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

public class OrderUtility {

	private static ArrayList<String> res;
	private static ArrayList<Integer> orderCols;
	private static int iID;
	private static int iColumn; // cursor in the array of column names
	
	public synchronized static ArrayList<Integer> makeOrderAttributeIndices(TreePattern p){
		makeOrderAttribute(p);
		return orderCols;
	}
	
	/**
	 * Gathers the JDBC names of all IDs from this XAM. Parents come before children,
	 * children come in order.
	 * @param x
	 * @return
	 */
	public synchronized static ArrayList<String> makeOrderAttributeNames(TreePattern p){
		makeOrderAttribute(p);
		return res;
	}
	
	private static void makeOrderAttribute(TreePattern p) {
		res = new ArrayList<String>();
		orderCols = new ArrayList<Integer>();
		// adding the doc URI at the beginning
		res.add("docURI");
		orderCols.add(0);
		iID = 1;
		iColumn = 1;

		TreePatternNode xn = p.getRoot();
		recMakeOrderAttribute(xn, false);
	}
	
	private static void recMakeOrderAttribute(TreePatternNode xn, boolean IMNested){

		//Parameters.logger.debug("\nXAM Node " + xn.getTagName());
		if (xn.storesID()){
			//Parameters.logger.debug("ID Specified!");
			if (!IMNested){
				//Parameters.logger.debug("Not nested, added " + iColumn + " to resColumn");
				orderCols.add(new Integer(iColumn));
			}
			// we have an ID field, so we increment by 1
			iColumn ++;
			if (!IMNested){
				if(xn.isIdentityIDType() || xn.isOrderIDType() || xn.isStructIDType() || xn.isUpdateIDType())
					res.add(new String("ID" + iID));
			}
		}
		if (xn.storesTag()){
			if (!IMNested){
				//Parameters.logger.debug("Added tag!");
				res.add(new String("Tag" + iID));
				orderCols.add(new Integer(iColumn));
				iColumn ++;
			}
		}
		if (xn.storesValue()){
			if (!IMNested){
				//Parameters.logger.debug("Added value!");
				res.add(new String("Val" + iID));
				orderCols.add(new Integer(iColumn));
				iColumn ++;
			}
		}
		if (xn.storesContent()){
			if (!IMNested){
				//Parameters.logger.debug("Added cont!");
				res.add(new String("Cont" + iID));
				orderCols.add(new Integer(iColumn));
				iColumn ++;
			}
		}
		iID ++;
		for (int i = 0; i < xn.getEdges().size(); i ++){
			PatternEdge xe = xn.getEdges().get(i);
			TreePatternNode childI = xn.getEdges().get(i).n2;
			recMakeOrderAttribute(childI, xe.isNested()||IMNested);
		}
	}
	
}
