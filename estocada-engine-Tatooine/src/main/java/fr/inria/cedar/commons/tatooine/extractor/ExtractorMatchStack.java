package fr.inria.cedar.commons.tatooine.extractor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

/**
 * This class encapsulates the functions of stack in the particular case when we will
 * put on the stack ExtractorMatches.
 * The stack knows "it is related to" other stacks, i.e. the stack corresponding to the
 * xam node one level up, and the stacks corresponding to the children (in the xam)
 * of the node corresponding to this stack.
 *
 * Its proximity to the DataExtractor enables this one to search in the stack from the bottom up,
 * i.e. from the oldest to the most recent element on the stack.
 * Another non-stack style operation retrieves the entry from the stack corresponding to a given
 * number.
 *
 * @author Ioana MANOLESCU
 *
 * @created 13/06/2005
 */
public class ExtractorMatchStack {
	private static final Logger log = Logger.getLogger(ExtractorMatchStack.class);
	
	/**
	 * If it is an attribute. If false, it is an element.
	 */
	boolean isAttribute;

	/**
	 * The tags of nodes that are matched here, or "*" if any tag matches.
	 */
	String tag;

	/*
	 * ArrayList of StackEntries.
	 */
	private TreeMap<Integer, ExtractorMatch> entries;
	//private ArrayList<ExtractorMatch> aEntries;
	// using Entries instead of aEntries.
	// this supposes that the last entry to be added always has the largest no.
	// This seems to make sense because entries are added upon beginElement.

	/**
	 * Last open entry in this stack. May be the top of the stack in fact ? In
	 * this case, it would suffice to use the last occupied position in the
	 * array. Needs more thinking.
	 */
	ExtractorMatch dnop;

	/**
	 * The parent stack: this one is created for the query xam node that is
	 * a parent of the node, for which this stack is created.
	 */
	ExtractorMatchStack parentStack;

	/**
	 * Stacks created for xam nodes, that are children of the node for
	 * which this stack has been created.
	 */
	ArrayList<Object> childrenStacks;

	public ExtractorMatchStack(String tag, boolean isAttribute, ExtractorMatchStack parentStack) {
		this.tag = tag;
		this.isAttribute = isAttribute;
		entries = new TreeMap<Integer, ExtractorMatch>();
		dnop = null;
		this.parentStack = parentStack;
		childrenStacks = new ArrayList<Object>();
	}

	public void push(ExtractorMatch s) {
		entries.put(s.no, s);
		s.theStack = this;
	}

	public void pop() {
		ExtractorMatch victim = entries.get(entries.lastKey());
		entries.remove(victim.no);
	}

	public ExtractorMatch top() {
		if (entries.size() > 0){
			return entries.get(entries.lastKey());
		}
		else{
			return null;
		}
	}

	public ExtractorMatch findEntry(int k) {
		return entries.get(k);
	}

	public void removeEntry(ExtractorMatch se) {
		entries.remove(se.no);
	}

	public int getEntriesSize() {
		if (entries != null){
			return entries.size();
		}
		return 0;
	}

	public void display()
	{
		log.debug(this.toString());
	}

	@Override
	public String toString()
	{
		StringBuffer retStr = new StringBuffer();

		int tagLength = this.tag.length();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < tagLength + 6; i++) {
			sb.append("=");
		}
		retStr.append("\n" + "== " + tag + " == \n" + sb);

		Iterator<ExtractorMatch> it = entries.values().iterator();
		while (it.hasNext()) {
			StringBuffer line = new StringBuffer("| ");
			ExtractorMatch se = it.next();
			line.append(se.no);
			log.debug("se.no: " + se.no);
			if (se.ownParent != null) {
				line.append(" <" + se.ownParent.no);
			}
			line.append(" [");
			Iterator<ArrayList<ExtractorMatch>> it2 = se.childrenByStack.values().iterator();
			while (it2.hasNext()) {
				Iterator<ExtractorMatch> it3 = it2.next().iterator();
				while (it3.hasNext()) {
					ExtractorMatch sChild = it3.next();
					line.append((sChild.erased ? "***" : "") + sChild.no + " ");
				}
			}
			line.append("] |");
			retStr.append(line);
		}

		retStr.append(sb);

		return retStr.toString();
	}

}

