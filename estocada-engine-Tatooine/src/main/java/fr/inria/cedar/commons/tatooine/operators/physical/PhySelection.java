package fr.inria.cedar.commons.tatooine.operators.physical;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/**
 * Physical operator that represents a simple selection over a set of tuples coming from its child.
 * @author Ioana MANOLESCU
 */
public class PhySelection extends NIterator {

	/** Universal version identifier for the PhySelection class */
	private static final long serialVersionUID = -8883794766257452076L;

	NIterator child;
	Predicate pred;
	NTuple t;

	public PhySelection(NIterator child, Predicate pred) {
		super(child);
		this.child = child;
		this.pred = pred;
		this.nrsmd = child.nrsmd;

		this.setOrderMaker(child.getOrderMaker());

		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		child.open();
		this.t = null;
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (timeout) {
			return false;
		}

		while (true) {

			if (timeout) {
				return false;
			}

			if (child.hasNext()) {
				t = child.next();
				if (pred.isTrue(t)) {
					return true;
				}
			} else {
				return false;
			}
		}
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		this.numberOfTuples++;
		return t;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		child.close();
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return new PhyArrayIterator(t.getNestedField(i), t.nrsmd.getNestedChild(i));
	}

	@Override
	public String getName() {
		String predToString;

		if (pred == null) {
			predToString = "null pred";
		} else {
			predToString = pred.getName();
		}

		String tabs = getTabs(PRINTING_INDENTATION_TABS);

		return "PhySel" + child.getName(1) + "," + "\n" + tabs + "[" + predToString + "]" + "\n" + ")";
	}

	@Override
	public String getName(int depth) {
		String predToString;

		if (pred == null) {
			predToString = "null pred";
		} else {
			predToString = pred.getName();
		}

		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);

		return "\n" + spaceForIndent + "PhySel(" + child.getName(depth + 1) + "," + "\n" + tabs + "[" + predToString
				+ "]" + "\n" + spaceForIndent + ")";
	}
	
	@Override
	public String getDetailedName(){
		return "Select(" + (pred != null ? pred.getName() : "") + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhySelection(this.child.copy(), this.pred);
	}

	// TODO finish this
	void estimateCost() {
		// Niterator child, Predicate pre
		// There are many type of predicate -> must consider all case
		// The reason why we should estimate the statistic in the logical
		// Logicaly. :-)
		SimplePredicate sp = (SimplePredicate) pred;
		if (sp.onString) {
			// TODO
		}

		if (sp.onJoin) {
			// TODO
		}
	}

	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 *
	 * @author Aditya SOMANI
	 */
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"Filter\\n" + this.pred + ", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return child.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
	}

	public NIterator getChild() {
		return child;
	}

	public Predicate getPredicate() {
		return pred;
	}

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
