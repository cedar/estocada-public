package fr.inria.cedar.commons.tatooine.loader;

import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;
import org.apache.log4j.Logger;
import org.apache.solr.common.util.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by DucCao on 17/10/16.
 * Load a table into Postgres
 */
public class PostgresDatabaseLoader extends FieldMapDatabaseLoader {
	private static final Logger log = Logger.getLogger(PostgresDatabaseLoader.class);

	public static final String KEY_HOST                         = "host";
	public static final String KEY_PORT                         = "port";
	public static final String KEY_DATABASE                     = "database";
	public static final String KEY_USERNAME                     = "user";
	public static final String KEY_PASSWORD                     = "pwd";

	private boolean isDatabaseCreated = false;

	public PostgresDatabaseLoader(StorageReference storageReference, boolean shouldDiscardDatabase) {
		setStorageReference(storageReference);
		initializeDb(shouldDiscardDatabase);
	}

	public PostgresDatabaseLoader(StorageReference storageReference) {
		this(storageReference, false);
	}

	public boolean isDatabaseCreated() {
		return isDatabaseCreated;
	}

	@Override
	public boolean load(String filePath) {
		try {
			Connection connection = getDbConnection();
			importSQL(connection, new FileInputStream(filePath));

			connection.close();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void deleteAllData(String dataCollectionName) {
		executeSql("delete * from " + dataCollectionName);
	}

	@Override
	public void deleteCollection(String dataCollectionName) {
		executeSql("drop table if exists " + dataCollectionName);
	}

	@Override
	public boolean isDataLoaded(String dataCollectionName) {
		try {
			Connection connection = getDbConnection();
			ResultSet resultSet =
					connection.getMetaData().getTables(null, null, dataCollectionName, new String[]{"TABLE"});

			if (resultSet.next()) {
				return true;
			}

			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void registerNewCatalogEntry() {
		String collectionName = getCatalogEntryName();

		Catalog catalog = Catalog.getInstance();
		try {
			ViewSchema schema = findMetadata(collectionName);
			catalog.add(collectionName, storageReference, new ViewSizeBundle(5, 5), schema);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Check if the given database exists
	 * @param databaseName
	 * @return
	 */
	public boolean doesDatabaseExist(String databaseName) {
		try {
			Connection connection = DriverManager.getConnection(String.format("jdbc:postgresql://%s:%s/%s",
					storageReference.getPropertyValue(KEY_HOST),
					storageReference.getPropertyValue(KEY_PORT),
					databaseName
			));
			connection.close();

			return true;
		} catch (SQLException e) {
			log.warn("Database connection failed: " + e.getMessage());
		}

		return false;
	}

	/**
	 * Create database, user and grant permissions
	 */
	private void initializeDb(boolean shouldDiscardDatabase) {
		this.isDatabaseCreated = false;

		if (shouldDiscardDatabase || !doesDatabaseExist(storageReference.getPropertyValue(KEY_DATABASE))) {
			try {
				Connection connection = getDefaultDbConnection();
				String[] commands = new String[] {
						String.format("drop database if exists %s", storageReference.getPropertyValue(KEY_DATABASE)),
						String.format("create database %s", storageReference.getPropertyValue(KEY_DATABASE)),
						String.format("drop user if exists %s", storageReference.getPropertyValue(KEY_USERNAME)),
						String.format("create user %s with password '%s'",
								storageReference.getPropertyValue(KEY_USERNAME), storageReference.getPropertyValue(KEY_PASSWORD)),
						String.format("grant all privileges on database %s to %s",
								storageReference.getPropertyValue(KEY_DATABASE), storageReference.getPropertyValue(KEY_USERNAME))
				};

				for (String command : commands) {
					Statement statement = connection.createStatement();
					statement.execute(command);
					statement.close();
				}

				connection.close();

				this.isDatabaseCreated = true;
			} catch (SQLException e) {
				log.warn("Initialize database failed: " + e.getMessage());
			}
		}
	}

	private void executeSql(String sql) {
		try {
			Connection connection = getDbConnection();
			Statement statement = connection.createStatement();
			statement.execute(sql);

			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Pair<TupleMetadataType, TupleMetadataType>> getFieldMap() {
		HashMap<String, Pair<TupleMetadataType, TupleMetadataType>> fieldMap = new HashMap<>();
		try {
			Connection connection = getDbConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from " + getCatalogEntryName());
			ResultSetMetaData resultSetMetaData = rs.getMetaData();

			int numberOfColumns = resultSetMetaData.getColumnCount();

			for (int i = 1; i <= numberOfColumns; i++) {
				fieldMap.put(
						resultSetMetaData.getColumnName(i),
						transformPostgreToNTupleMetadataType(resultSetMetaData.getColumnClassName(i))
				);
			}

			statement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return fieldMap;
	}
	/**
	 * Transform the class name of a given column into NTuple data type
	 * @param columnClassName
	 * @return
	 */
	private Pair<TupleMetadataType, TupleMetadataType> transformPostgreToNTupleMetadataType(String columnClassName) {
		if (Integer.class.getName().equals(columnClassName) ||
				Float.class.getName().equals(columnClassName) ||
				Double.class.getName().equals(columnClassName)) {
			// Simple tuple of type integer
			// TODO support float data type for TupleMetadataType
			// TODO check if Postgres supports tuple with integer/string child types
			return new Pair<>(TupleMetadataType.INTEGER_TYPE, null);
		} else {
			return new Pair<>(TupleMetadataType.STRING_TYPE, null);
		}
	}

	/**
	 * The connection to Postgres without specific database name
	 * @return
	 * @throws SQLException
	 */
	private Connection getDefaultDbConnection() throws SQLException {
		return DriverManager.getConnection(String.format("jdbc:postgresql://%s:%s/",
				storageReference.getPropertyValue(KEY_HOST),
				storageReference.getPropertyValue(KEY_PORT)
		));
	}

	/**
	 * The connection to Postgres database in order to execute SQL queries or query metadata
	 * @return
	 * @throws SQLException
	 */
	private Connection getDbConnection() throws SQLException {
		return DriverManager.getConnection(
			String.format("jdbc:postgresql://%s:%s/%s",
					storageReference.getPropertyValue(KEY_HOST),
					storageReference.getPropertyValue(KEY_PORT),
					storageReference.getPropertyValue(KEY_DATABASE)
			),
			storageReference.getPropertyValue(KEY_USERNAME),
			storageReference.getPropertyValue(KEY_PASSWORD)
		);
	}

	/**
	 * Execute SQL scripts from an InputStream
	 * Reference: http://stackoverflow.com/a/1498029
	 * @param connection
	 * @param inputStream
	 * @throws SQLException
	 */
	private void importSQL(Connection connection, InputStream inputStream) throws SQLException {
		Scanner s = new Scanner(inputStream);
		s.useDelimiter("(;(\r)?\n)|(--\n)");
		Statement st = null;
		try {
			st = connection.createStatement();
			while (s.hasNext()) {
				String line = s.next();
				if (line.startsWith("/*!") && line.endsWith("*/")) {
					int i = line.indexOf(' ');
					line = line.substring(i + 1, line.length() - " */".length());
				}

				if (line.trim().length() > 0) {
					st.execute(line);
				}
			}
		} finally {
			if (st != null) st.close();
            s.close();
		}
	}
}
