package fr.inria.cedar.commons.tatooine.operators.physical.join;

import java.util.ArrayList;
import java.util.Iterator;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.NTupleStack;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyArrayIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.util.PhysicalOperatorUtility;

/**
 * Stack-based structural join that returns results ordered by the descendant ID.
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 */
public class StackTreeDesc extends StructuralJoin {

	/** Universal version identifier for the StackTreeDesc class */
	private static final long serialVersionUID = 7493275595848109147L;

	private ArrayList<NTuple> result;
	private int iResult;
	private NTuple currentTuple;

	public StackTreeDesc(NIterator left, NIterator right, int leftIdx, int rightIdx, PredicateType idPredCode)
			throws TatooineExecutionException {

		super(left, right, leftIdx, rightIdx, idPredCode);

		this.nrsmd = NRSMD.appendNRSMD(left.nrsmd, right.nrsmd);

		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.setOrderMaker(OrderMarker.shift(right.getOrderMaker(), left.nrsmd.colNo));
		} else {
			this.setOrderMaker(left.getOrderMaker());
		}

		increaseOpNo();
	}

	public StackTreeDesc(NIterator left, NIterator right, int leftIdx, int rightIdx, int leftDocIdx, int rightDocIdx,
			PredicateType idPredCode) throws TatooineExecutionException {

		super(left, right, leftIdx, rightIdx, leftDocIdx, rightDocIdx, idPredCode);

		this.nrsmd = NRSMD.appendNRSMD(left.nrsmd, right.nrsmd);

		int leftSize = left.nrsmd.colNo;
		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.setOrderMaker(OrderMarker.shift(right.getOrderMaker(), left.nrsmd.colNo));
			this.setOrderMaker(this.getOrderMaker().addToGroup((rightDocIdx + leftSize), leftDocIdx));
		} else {
			this.setOrderMaker(left.getOrderMaker());
			this.setOrderMaker(this.getOrderMaker().addToGroup(leftDocIdx, (rightDocIdx + leftSize)));
		}

		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		ancestorChild.open();
		descendantChild.open();
		initialize();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public void initialize() throws TatooineExecutionException {
		noMoreAncestors = false;
		noMoreDescendants = false;
		this.joinStack = new NTupleStack();
		result = new ArrayList<NTuple>();
		iResult = -1;

		noMoreChildrenTuples = false;
		currentTupleAncestor = null;
		currentTupleDescendant = null;

		noMoreAncestors = (!ancestorChild.hasNext());
		if (!noMoreAncestors) {
			currentTupleAncestor = ancestorChild.next();
		}

		noMoreDescendants = (!descendantChild.hasNext());
		if (!noMoreDescendants) {
			currentTupleDescendant = descendantChild.next();
			// //Parameters.logger.debug(this.getName() + "\nTRight is " + tRight.toString());
			// Parameters.logger.debug(NodeInformation.localPID + ": STD operator with no " + this.getOperatorID() +
			// ", TDesc is " + currentTupleDescendant.toString());
		} else {
			// Parameters.logger.debug(this.getName() + "\nRIGHT OVER");
			// Parameters.logger.debug(NodeInformation.localPID + ": STD operator with no " + this.getOperatorID() +
			// ", DESC OVER");
		}
		noMoreChildrenTuples = noMoreDescendants || noMoreAncestors;
		if (!noMoreChildrenTuples) {
			alignDoc();
		}
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {

		if (timeout) {
			return false;
		}

		// Parameters.logger.debug("\nEfficientSTD.next");
		if (result.size() > 0) {
			iResult++;
			if (iResult < result.size()) {
				currentTuple = result.get(iResult);
				return true;
			} else {
				result = new ArrayList<NTuple>();
				iResult = -1;
			}
		}
		// other cases: iResult is too large, or result is empty

		if (noMoreChildrenTuples) {
			return false;
		}

		while ((result.size() == 0) && (!noMoreChildrenTuples)) {

			if (timeout) {
				return false;
			}

			// Parameters.logger.debug("\n\nOuter loop searching for results");

			boolean mustBreak = false;
			while (!mustBreak && (!noMoreChildrenTuples || !joinStack.empty())) {

				if (timeout) {
					return false;
				}

				boolean mustAlign = false;

				boolean choiceMade = false;

				// Parameters.logger.debug("\n\n Inner loop ");

				boolean branch1 = true;
				boolean branch2 = true;
				boolean branch3 = true;

				char[] ancDoc = null;
				char[] descDoc = null;
				ElementID ancID = null;
				if (currentTupleAncestor != null) {
					ancID = currentTupleAncestor.getIDField(ancestorColumn);
					ancDoc = currentTupleAncestor.getUriField(ancestorDocColumn);
					// Parameters.logger.info("STD " + this.operatorID + " anc doc: " + new String(ancDoc));
				}
				ElementID descID = null;
				if (currentTupleDescendant != null) {
					descID = currentTupleDescendant.getIDField(descendantColumn);
					descDoc = currentTupleDescendant.getUriField(descendantDocColumn);
					// Parameters.logger.info("STD " + this.operatorID + " desc doc: " + new String(descDoc));
				}
				NTuple tStack = null;
				ElementID stackID = null;

				if (joinStack.empty() || (ancID == null) || (descID == null)) {
					branch1 = false;
					// Parameters.logger.debug("Branch 1 invalidated 1");
				}
				if (noMoreAncestors) {
					branch1 = false;
					branch2 = false;
					// Parameters.logger.debug("Branches 1 and 2 invalidated 2");
				}
				if ((ancID == null) || (descID == null)) {
					branch2 = false;
					// Parameters.logger.debug("Branch 2 invalidated 3");
				}
				if (currentTupleDescendant == null) {
					branch3 = false;
					// Parameters.logger.debug("Branch 3 invalidated 4");
				}

				if (!branch1 && !branch2 && !branch3) {
					noMoreChildrenTuples = true;
					// Parameters.logger.debug("Going out !");
					break;
				}

				if (branch1 && (!choiceMade)) {
					tStack = joinStack.top();
					stackID = tStack.getIDField(ancestorColumn);
					if (stackID.startsAfter(descID) || descID.endsAfter(stackID)) {
						// Parameters.logger.debug("Branch 1, popped: " +
						// tStack.toString());
						// Parameters.logger.debug(" Popped: " + tStack.toString());
						joinStack.pop();
						choiceMade = true;
					}
				}

				if (branch2 && (!choiceMade)) {
					if ((descID.startsAfter(ancID) || descID.equals(ancID))
							&& PhysicalOperatorUtility.compareCharArrays(ancDoc, descDoc) == 0) {
						// Parameters.logger.debug("Branch 2, pushed: " + tLeft.toString());
						joinStack.push(currentTupleAncestor);
						noMoreAncestors = (!ancestorChild.hasNext());
						if (!noMoreAncestors) {
							currentTupleAncestor = ancestorChild.next();
							// Parameters.logger.debug("Got from left: " + tLeft);
						} else {
							currentTupleAncestor = null;
						}
						choiceMade = true;
					}
				}

				if (branch3 && (!choiceMade)) {
					// Parameters.logger.debug("Branch 3");
					Iterator<NTuple> it = joinStack.iterator();
					char[] stackDoc = null;
					while (it.hasNext()) {

						if (timeout) {
							return false;
						}

						tStack = it.next();
						stackDoc = tStack.getUriField(ancestorDocColumn);
						stackID = tStack.getIDField(ancestorColumn);
						descID = currentTupleDescendant.getIDField(descendantColumn);
						// Parameters.logger.debug("Trying to produce from stack ID " +
						// stackID + " and right ID " + rightID);

						if (stackID.startsAfter(descID)) {
							// Parameters.logger.debug(stackID + " starts after or at the same time as " + descID);
							continue;
						}
						if (descID.endsAfter(stackID)) {
							// Parameters.logger.debug(descID + " ends after or at the same time as "+ stackID);
							continue;
						}
						if (this.predicate == PredicateType.PREDICATE_PARENT
								|| this.predicate == PredicateType.PREDICATE_CHILD) {
							// check level condition if this is required
							if (!stackID.isParentOf(descID)) {
								// Parameters.logger.debug("Child condition not met by " + stackID + " and " + rightID);
								continue;
							}
						}
						if (PhysicalOperatorUtility.compareCharArrays(stackDoc, descDoc) != 0) {
							continue;
						}
						NTuple tRes;
						if (this.predicate == PredicateType.PREDICATE_ANCESTOR
								|| this.predicate == PredicateType.PREDICATE_PARENT) {
							tRes = NTuple.append(this.nrsmd, tStack, currentTupleDescendant);
						} else {
							tRes = NTuple.append(this.nrsmd, currentTupleDescendant, tStack);
						}
						result.add(tRes);
						// Parameters.logger.debug(" Produced tuple " + tRes.toString());
					}
					iResult = 0;

					noMoreDescendants = (!descendantChild.hasNext());
					if (!noMoreDescendants) {
						currentTupleDescendant = descendantChild.next();
						// Parameters.logger.debug("Got from right: " + tRight.toString());
						if (ancestorDocColumn >= 0) {
							char[] newTDescDocID = currentTupleDescendant.getUriField(descendantDocColumn);
							// Parameters.logger.info("New right docID: " + new String(newTRightDocID));
							int k2 = PhysicalOperatorUtility.compareCharArrays(currentDocID, newTDescDocID);
							if (k2 < 0) {// this new right tuple has a bigger doc ID
								mustAlign = true;
							} else {
								assert (k2 <= 0) : "Wrong docID order. OperatorID: " + this.getOperatorID();
							}
							// end of June 2008
						}

					} else {
						currentTupleDescendant = null;
					}

					choiceMade = true;
					mustBreak = true;
				}
				if (mustAlign) {
					alignDoc();
				}
				// this was done anyway, it is redundant if it comes after alignment,
				// but it doesn't matter
				noMoreChildrenTuples = noMoreAncestors && noMoreDescendants;

			}
		}

		if (result.size() == 0) {
			return false;
		} else {
			currentTuple = result.get(iResult);
			return true;
		}
	}

	@Override
	public final void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());

		ancestorChild.close();
		descendantChild.close();

		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		// Parameters.logger.debug("NEXT " + this.getName());
		// currentTuple.display();
		this.numberOfTuples++;
		return currentTuple;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return new PhyArrayIterator(currentTuple.getNestedField(i), currentTuple.nrsmd.getNestedChild(i));
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		if (this.leftDocIdx < 0) {
			return new StackTreeDesc(this.leftChild.copy(), this.rightChild.copy(), this.leftIdx, this.rightIdx,
					this.predicate);
		} else {
			return new StackTreeDesc(this.leftChild.copy(), this.rightChild.copy(), this.leftIdx, this.rightIdx,
					this.leftDocIdx, this.rightDocIdx, this.predicate);
		}
	}

	@Override
	public String getName() {
		String predToString;
		if (this.predicate == null) {
			predToString = "null pred";
		} else {
			predToString = Integer.toString(this.leftIdx) + this.predicate.toString() + Integer.toString(this.rightIdx);
		}

		String tabs = getTabs(PRINTING_INDENTATION_TABS);

		return "StackTreeDesc(" + leftChild.getName(1) + "," + rightChild.getName(1) + "," + "\n" + tabs + predToString
				+ "\n" + ")";
	}

	@Override
	public String getName(int depth) {
		String predToString;
		if (this.predicate == null) {
			predToString = "null pred";
		} else {
			predToString = Integer.toString(this.leftIdx) + this.predicate.toString() + Integer.toString(this.rightIdx);
		}

		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);

		return "\n" + spaceForIndent + "StackTreeDesc(" + leftChild.getName(1 + depth) + ","
				+ rightChild.getName(1 + depth) + "," + "\n" + tabs + predToString + "\n" + spaceForIndent + ")";
	}
}