package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.AsterixDBSourceLayer;
import fr.inria.cedar.commons.tatooine.storage.database.DBSourceLayer;

/**
 * Physical operator that queries JSON data source using Apache AsterixDB.
 * 
 * @author ranaalotaibi
 */
public abstract class AsterixDBJSONQueryParameterizedOperator extends QueryParameterisedOperator {

    /** Universal version identifier for AsterixDBQueryParameterisedOperator class **/
    private static final long serialVersionUID = 736091201987566961L;
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(AsterixDBJSONQueryParameterizedOperator.class);
    /** Placeholder for bind access **/
    private static final String PLACEHOLDER = "%";

    protected static final String REPLACEMENT_BLANK = "\\\\\\\\ ";

    /** Query parameters **/
    protected GSR gsr;
    protected NTuple params;
    protected String queryStr = "";
    protected String paramStr = "";

    /** DBSourceLayer that handles AsterixBD calls **/
    private DBSourceLayer source;

    /** In-memory queue of NTuples **/
    private Queue<NTuple> tuples = null;

    /** Constructor **/
    public AsterixDBJSONQueryParameterizedOperator(final String query, final GSR gsr) {
        queryStr = query;
        source = new AsterixDBSourceLayer(gsr);
        increaseOpNo();
    }

    /**
     * Resolve the query.
     * 
     * @param query
     *            the query.
     * @return resolved query.
     * @throws TatooineExecutionException
     */
    private String resolveQuery(final String query) throws TatooineExecutionException {
        return Strings.isNullOrEmpty(paramStr) ? queryStr : paramStr;
    }

    @Override
    public void open() throws TatooineExecutionException {
        informPlysPlanMonitorOperStatus(0, getOperatorID());
        source.connect();
        paramStr = "";
        informPlysPlanMonitorOperStatus(1, getOperatorID());
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        if (timeout) {
            return false;
        }
        if (tuples == null) {
            final String[] parameters = new String[] { resolveQuery(queryStr) };
            final Object batch = source.getBatchRecords(parameters);
            if (batch == null) {
                return false;
            } else {
                tuples = toNTuple((JsonNode) batch);
                return CollectionUtils.isEmpty(tuples) ? false : true;
            }
        } else if (tuples.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        numberOfTuples++;
        NTuple next = tuples.poll();
        return next;
    }

    @Override
    public void close() throws TatooineExecutionException {
        informPlysPlanMonitorOperStatus(2, getOperatorID());
        source.close();
        informPlysPlanMonitorOperStatus(3, getOperatorID());
        informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
    }

    /**
     * Transform AsterixDB response into NTuples.
     * 
     * @param jsonNode
     *            The root node of the JSON collection.
     * @return The constructed NTuples.
     * @throws TatooineExecutionException
     */
    private Queue<NTuple> toNTuple(final JsonNode jsonNode) throws TatooineExecutionException {
        final Queue<NTuple> records = new ArrayDeque<NTuple>();
        final Iterator<JsonNode> iterator = jsonNode.elements();
        while (iterator.hasNext()) {
            final NTuple nTuple = new NTuple(nrsmd);
            final JsonNode tuple = iterator.next();
            for (int index = 0; index < nrsmd.types.length; index++) {
                final String name = nrsmd.colNames[index];
                final JsonNode value = tuple.get(name);
                if (value == null) {
                    final String message = String.format("The AsterixDB response tuple has no [%s] field", name);
                    LOGGER.error(message);
                    throw new TatooineExecutionException(message);
                }
                final JsonType jsonType = getJSONElementType(value);
                if (jsonType != null) {
                    addFieldToNTuple(nTuple, jsonType, value, index);
                } else {
                    final String message = String.format("The field [%s] data type is not supported", name);
                    LOGGER.error(message);
                    throw new TatooineExecutionException(message);
                }
            }
            records.add(nTuple);

        }

        return records;
    }

    /**
     * Add a field to the NTuple.
     * 
     * @param nTuple
     *            the NTuple.
     * @param jsonType
     *            the JSON type of the field.
     * @param value
     *            the value of the field.
     * @param colIndex
     *            the index of the field in the NRSMD
     * @throws TatooineExecutionException
     */
    private void addFieldToNTuple(final NTuple nTuple, final JsonType jsonType, final JsonNode value,
            final int colIndex) throws TatooineExecutionException {
        switch (jsonType) {
            case STRING:
                nTuple.addString(value.asText());
                break;
            case INTEGER:
                nTuple.addInteger(value.asInt());
                break;
            case ARRAY:
                nTuple.addNestedField(transformNestedTuple(value, nrsmd.getNestedChild(colIndex)));
                break;
            default:
                final String message = String.format("PhyAsterixDBEval only handles string, integer, and nested types");
                LOGGER.error(message);
                throw new TatooineExecutionException(message);
        }

    }

    /**
     * Transform JSON array element to List<NTuple>
     * 
     * @param value
     *            the nested JSON element
     * @param childNRSMD
     *            the NRSMD of the nested value
     * @return The list of NTuple
     * @throws TatooineExecutionException
     */
    private List<NTuple> transformNestedTuple(final JsonNode value, final NRSMD childNRSMD)
            throws TatooineExecutionException {
        final List<NTuple> children = new ArrayList<NTuple>();
        final Iterator<JsonNode> iterator = value.elements();
        while (iterator.hasNext()) {
            final NTuple nTuple = new NTuple(childNRSMD);
            final JsonNode tuple = iterator.next();
            for (int index = 0; index < childNRSMD.types.length; index++) {
                final String name = childNRSMD.colNames[index];
                final JsonNode innerValue = tuple.get(name);
                final JsonType jsonType = getJSONElementType(innerValue);
                switch (jsonType) {
                    case STRING:
                        nTuple.addString(innerValue.asText());
                        break;
                    case INTEGER:
                        nTuple.addInteger(innerValue.asInt());
                        break;
                    case ARRAY:
                        nTuple.addNestedField(transformNestedTuple(innerValue, childNRSMD.getNestedChild(index)));
                        break;
                    default:
                        final String message =
                                String.format("PhyAsterixDBEval only handles string, integer, and nested types");
                        LOGGER.error(message);
                        throw new TatooineExecutionException(message);
                }

            }
            children.add(nTuple);
        }

        return children;
    }

    /** Enumerated type representing all supported JSON types **/
    private enum JsonType {
        INTEGER,
        STRING,
        ARRAY,
        OBJECT;
    }

    /**
     * Get the JSON element type.
     * 
     * @param jsonNode
     *            the JSON element
     * @return the JSON element type.
     */
    private JsonType getJSONElementType(final JsonNode jsonNode) {

        if (jsonNode.isTextual()) {
            return JsonType.STRING;
        }
        if (jsonNode.isInt() || jsonNode.isBigInteger() || jsonNode.isDouble() || jsonNode.isBigDecimal()) {
            return JsonType.INTEGER;
        }
        if (jsonNode.isArray()) {
            return JsonType.ARRAY;
        }
        if (jsonNode.isObject()) {
            return JsonType.ARRAY;
        }

        return null;
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setParameters(NTuple params) throws IllegalArgumentException {
        tuples = null;
        this.params = params;
        paramStr = queryStr;
        final NRSMD paramsNRSMD = params.nrsmd;
        for (int i = 0; i < paramsNRSMD.colNames.length; i++) {
            final String name = paramsNRSMD.colNames[i];
            final TupleMetadataType type = paramsNRSMD.types[i];

            switch (type) {
                case STRING_TYPE:
                    final String value = new String((char[]) params.getValue(name));
                    paramStr = paramStr.replaceAll(PLACEHOLDER + Integer.toString(i), "\"" + value + "\"");
                    break;
                case INTEGER_TYPE:
                    paramStr =
                            paramStr.replaceAll(PLACEHOLDER + Integer.toString(i), (params.getValue(name)).toString());
                    break;
                case TUPLE_TYPE:
                    final List<NTuple> nTuples = params.getNestedField(i);
                    final String nestedValue = nestedFieldToString(nTuples);
                    paramStr = paramStr.replaceAll(PLACEHOLDER + Integer.toString(i), nestedValue);
                    break;
                default:
                    String msg = String.format("Unsupported NTupleMD type: %s", type);
                    throw new IllegalArgumentException(msg);
            }

        }
    }

    /**
     * Convert nested tuples to string parameter
     * 
     * @param nTuples
     *            the list of nested NTuples
     * @return the constructed string from nested NTuples.
     */
    private String nestedFieldToString(final List<NTuple> nTuples) {
        final NRSMD childNRSDM = nTuples.get(0).nrsmd;
        final JsonArray jsonArray = new JsonArray();
        for (final NTuple nTuple : nTuples) {
            final JsonObject jsonObject = new JsonObject();
            for (int i = 0; i < childNRSDM.types.length; i++) {
                final TupleMetadataType type = childNRSDM.types[i];
                switch (type) {
                    case STRING_TYPE:
                        final String strValue = new String((char[]) nTuple.getStringField(i));
                        jsonObject.addProperty(childNRSDM.getColNames()[i], strValue);
                        break;
                    case INTEGER_TYPE:
                        final int intValue = nTuple.getIntegerField(i);
                        jsonObject.addProperty(childNRSDM.getColNames()[i], intValue);
                        break;
                    case TUPLE_TYPE:;
                        final String nestedValue = nestedFieldToString(nTuple.getNestedField(i));
                        jsonObject.addProperty(childNRSDM.getColNames()[i], nestedValue);
                        break;
                    default:
                        String msg = String.format("Unsupported NTupleMD type: %s", type);
                        throw new IllegalArgumentException(msg);
                }
            }
            jsonArray.add(jsonObject);
        }
        return jsonArray.toString();
    }

    /** Get the query **/
    public String getQuery() {
        return queryStr;
    }

    /** Get the NRSMD **/
    public NRSMD getNRSMD() {
        return nrsmd;
    }

    /** Get storage reference **/
    public GSR getStorageReference() {
        return gsr;
    }

}
