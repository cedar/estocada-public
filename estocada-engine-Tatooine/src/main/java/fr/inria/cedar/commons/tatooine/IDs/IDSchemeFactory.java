package fr.inria.cedar.commons.tatooine.IDs;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.ElementIDScheme;

/**
 * Factory class that creates the appropriate kind of ID scheme depending on the properties defined for Tatooine.
 * @author Ioana MANOLESCU
 */
class IDSchemeFactory{
	private static final Logger log = Logger.getLogger(IDSchemeFactory.class);
	private final static ElementIDScheme USE_ELEMENT_ID_SCHEME = 
			Boolean.parseBoolean(Parameters.getProperty("childAxis")) ? 
					ElementIDScheme.PREPOSTDEPTH : ElementIDScheme.PREPOST;

	static IDScheme getElementIDScheme(){
		switch (USE_ELEMENT_ID_SCHEME) {
		case PREPOSTDEPTH:
			return new PrePostDepthIDScheme();
		case PREPOST:
			return new PrePostIDScheme();
		default:
			log.error("Don't know which ID scheme to use");
			return null;
		}
	}
}