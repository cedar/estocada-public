package fr.inria.cedar.commons.tatooine.operators.physical.join;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/**
 * A naive implementation of the bind join that iteratively takes one tuple from the left, 
 * extracts the bindings and sets them on the right, extracts iteratively tuples from the 
 * right using these bindings, reconstructs output tuples and returns them. 
 * In particular, this uses no caching.
 * 
 * @author Raphael BONAQUE
 */
public class PhyBindJoin extends NIterator {
	
	private static final long serialVersionUID = 6401375305716291864L;

	protected NIterator left;
	protected BindAccess right;
	
	/**
	 * The binding pattern is the columns to keep from the tuples coming from
	 * left (with their order) to create a binding to set in right
	 */
	protected int[] bindingPattern;
	protected NRSMD bindingNRSMD;
	
	/**
	 * Not currently in use, because we use the very simple default output:
	 * protected int[] outputPattern;
	 */
	// The current tuple from left to use in the join
	protected NTuple currentLeftTuple; 
	
	protected NTuple nextTuple;
	protected boolean nextTupleIsAvailable = false;

	public PhyBindJoin(NIterator left, BindAccess right, int[] bindingPattern) throws TatooineExecutionException {
		super(left, right);
		this.left = left;
		this.right = right;
		this.bindingPattern = bindingPattern;
		bindingNRSMD = NRSMD.makeProjectRSMD(left.nrsmd, bindingPattern);		
		nrsmd = NRSMD.appendNRSMDList((List<NRSMD>) Arrays.asList(left.nrsmd, right.nrsmd));
	}

	public PhyBindJoin(NIterator left, BindAccess right, Predicate pred) throws TatooineExecutionException {
		this(left, right, constructBindingPattern(pred, left.nrsmd.colNo));
	}

	/** 
	 * Transforms the predicate into a binding pattern
	 * @param pred The predicate to construct a binding pattern: must use only conjunction of equalities
	 * @param leftCols The number of columns in the tuples coming from left
	 */
	public static int[] constructBindingPattern(Predicate pred, int leftCols) {		
		Map<Integer,Integer> bindingMap = new HashMap<Integer,Integer>();
		// Subpredicates contains the predicates remaining to be integrated into the pattern
		Queue<Predicate> subpredicates = new ArrayDeque<Predicate>();
		subpredicates.add(pred);
		
		while (!subpredicates.isEmpty()){
			Predicate localPredicate = subpredicates.poll();
			// When we need to integrate an equality we reflect it directly into the pattern
			if (localPredicate instanceof SimplePredicate){
				SimplePredicate sp = (SimplePredicate) localPredicate;
				
				if (sp.getPredCode() != PredicateType.PREDICATE_EQUAL){
					throw new Error("BindJoin can only be used for equality joins");
				}
				
				bindingMap.put(sp.getOtherColumn() - leftCols, sp.getColumn());
			
			/* 
			 * When we need to integrate a conjunction of predicates we add them to subpredicates 
			 * so that they will get treated at a later iteration
			 */
			} else if (localPredicate instanceof ConjunctivePredicate) {
				subpredicates.addAll(Arrays.asList(((ConjunctivePredicate) localPredicate).getPreds()));
			} else {
				throw new Error("BindJoin requires simple (or conjunctive) predicate, received "  + localPredicate.getClass().getName());
			}
			
		};
		
		int[] bindingPattern = new int[bindingMap.size()];
		for(int i = 0; i<bindingMap.size(); i++) {
			if (!bindingMap.containsKey(i))
				throw new Error("The bindings must be the first columns of right");
			bindingPattern[i] = bindingMap.get(i);
		}
		
		return bindingPattern;
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (!nextTupleIsAvailable) {
			nextTuple = computeNext();
			nextTupleIsAvailable = true;
			if (nextTuple != null) numberOfTuples++;
		}
		return (nextTuple != null);
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		if (nextTupleIsAvailable) {
			nextTupleIsAvailable = false;
			return nextTuple;
		} else {
			nextTuple = computeNext();
			if (nextTuple != null) numberOfTuples++;
			else nextTupleIsAvailable = true;
			return nextTuple;
		}
	}
	
	protected NTuple computeBinding(NTuple leftTuple) {
		return NTuple.project(leftTuple, bindingNRSMD, bindingPattern);
	}

	/** Computes a NTuple to output, given a tuple from left and a tuple from right: this is the function that actually realizes the join */
	protected NTuple computeOutput(NTuple leftTuple, NTuple rightTuple) throws TatooineExecutionException {
		return NTuple.append(nrsmd, leftTuple, rightTuple);
	}

	/** Computes the next NTuple to output, if this is the end of this iterator then it should return null */
	protected NTuple computeNext() throws TatooineExecutionException{
		while (true) {
			// We check if we need a new tuple from the left
			if (currentLeftTuple == null) {
				if (!left.hasNext()) return null;
				currentLeftTuple = left.next();			
				right.setParameters(computeBinding(currentLeftTuple));
			}
			
			if (!right.hasNext()) currentLeftTuple = null;
			else {
				return computeOutput(currentLeftTuple, right.next());
			}
		}
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		left.open();
		right.open();
		initialize();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		right.close();
		left.close();
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
	}

	@Override
	public String getName() {
		String bindingstr = bindingPattern.toString();
		return "BindJoin(" + left.getName(1) + ", " + right.getName(1) + "[" + bindingstr + "]\n)";
	}

	@Override
	public String getName(int depth) {
		String bindingstr = bindingPattern.toString();
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return ("\n" + spaceForIndent + "BindJoin(" + left.getName(1 + depth) + ","
				+ right.getName(1 + depth) + "\n" +  spaceForIndent + "[" + 
				bindingstr  + "]\n" + spaceForIndent + ")");
	}
	
	@Override
	public String getDetailedName() {
		String bindingstr = "";
		for (int rightCol = 0; rightCol < bindingPattern.length; rightCol++) {
			if (rightCol > 0) bindingstr += ", ";
			int leftCol = bindingPattern[rightCol];
			bindingstr += left.nrsmd.colNames[leftCol] != null ? left.nrsmd.colNames[leftCol] : ("col " + leftCol);}
		return "BindJoin(" + bindingstr + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyBindJoin(left.copy(), (BindAccess) right.copy(), bindingPattern);
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException();
	}
}
