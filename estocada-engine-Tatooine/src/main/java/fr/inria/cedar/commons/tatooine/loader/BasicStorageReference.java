package fr.inria.cedar.commons.tatooine.loader;

import java.util.Properties;

public class BasicStorageReference implements StorageReference {

  protected final Properties prop = new Properties();

  /** Return the number of properties characterizing this reference */
  @Override
  public int getNumberOfProperties(){
    return prop.size();
  }

  /** Return the name of the ith property */
  @Override
  public String getPropertyName(int i){
    throw new UnsupportedOperationException("BasicStorageReference does not support integer indexing");
  }

  /** Return the value of the ith property */
  @Override
  public String getPropertyValue(int i){
    throw new UnsupportedOperationException("BasicStorageReference does not support integer indexing");
  }

  /** Return the value for a given property */
  @Override
  public String getPropertyValue(String propertyName){
    return prop.getProperty(propertyName);
  }

  /** Sets the property to a given value */
  @Override
  public void setProperty(String propertyName, String propertyValue) throws Exception {
    prop.setProperty(propertyName, propertyValue);
  }

}
