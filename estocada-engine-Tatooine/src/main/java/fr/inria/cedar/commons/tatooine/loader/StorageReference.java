package fr.inria.cedar.commons.tatooine.loader;

/**
 * Information about how and where is a given data collection reachable
 * One of the fields ends up being the XAM itself... (as a string) :-(
 * It would be better to kill the storedXAM pair class from StorageConfiguration,
 * and simply nest the XAM string (or the XAM itself, for that matter) in the storage
 * reference implementations.
 * And then, no need to parse the XAM multiple times later.
 * 
 * @author Ioana MANOLESCU
 */
public interface StorageReference {
	
	/** Return the number of properties characterizing this reference */
	public int getNumberOfProperties();
	
	/** Return the name of the ith property */
	public String getPropertyName(int i);

	/** Return the value of the ith property */
	public String getPropertyValue(int i);
	
	/** Return the value for a given property */
	public String getPropertyValue(String propertyName);
	
	/** Sets the property to a given value */
	public void setProperty(String propertyName, String propertyValue) throws Exception;	
}