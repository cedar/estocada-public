package fr.inria.cedar.commons.tatooine.loader.multidoc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.DocNtwID;
import fr.inria.cedar.commons.tatooine.storage.database.StorageContext;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

/**
 * Manager for XML documents. Executes the corresponding actions when an XML document
 * arrives to a peer.
 *
 * @author Spyros ZOUPANOS
 * @author Jesus CAMACHO RODRIGUEZ
 * @author Asterios KATSIFODIMOS
 */
public class DocumentsManager {
	private static final Logger log = Logger.getLogger(DocumentsManager.class);
	/* Constants */
	private static final int TUPLE_SENDING_CONCURRENCY = Integer.parseInt(Parameters.getProperty("tupleSendingConcurrencyLevel"));

	/* How many*/
	private int MAX_PATTERNS_TO_EXTRACT_IN_PARALLEL = Integer.parseInt(Parameters.getProperty("MaxPatternsToExtractInParallel"));

	private int MAX_EXTRACTION_TIME = Integer.parseInt(Parameters.getProperty("MaximumPatternExtractionTime"));

	/* How many extractors will there be extracting at any given time.*/
	private int NUMBER_OF_EXTRACTORS = Integer.parseInt(Parameters.getProperty("MaximumConcurrentExtractors"));

	public static final AtomicBoolean materializationInProgress = new AtomicBoolean(false);

	private final StorageContext storageContext;
	
	/**
	 * Default constructor ==
	 */
	public DocumentsManager(final StorageContext storageContext) {
		this.storageContext = storageContext;
		MAX_EXTRACTION_TIME = MAX_EXTRACTION_TIME==0?(Integer.MAX_VALUE/1000) - 1:MAX_EXTRACTION_TIME;
	}

	/*
	 * Materializes the given view for the given document.
	 * Materialization consists in extracting for each view all matching tuples
	 * from the document and sending them to the appropriate peer for storage.
	 * @param docId
	 * @param gsrsForDoc
	 * @throws Exception
	 */
	private void materialize(DocNtwID docId,
			final Set<TreePatternGSR> gsrsForDoc, final String dbHolderId)
					throws Exception {
		if (gsrsForDoc == null || gsrsForDoc.size() == 0) {
			return;
		}

		int  counterTreePatterns = 0;

		// This variable will keep the correspondence between the JoinedPatternGSR of a JoinedPattern that does not contain
		// value joins and the tree pattern that is contained in that JoinedPattern.
		Map<TreePatternGSR, TreePattern> TreePatternGSR2TreePattern = new HashMap<TreePatternGSR, TreePattern>();

		log.info("Starting to extract data from " + docId.docName
				+ " using " + gsrsForDoc.size() + " joined patterns");

		// We extract all the tree patterns from the JoinedPatternGSRs.
		for (TreePatternGSR jPatGSR : gsrsForDoc) {
			TreePatternGSR2TreePattern.put(
					jPatGSR,
					XAMParserUtility.getTreePatternFromString(
							jPatGSR.getPropertyValue("XAMString"),
							jPatGSR.getPropertyValue("ENVIRONMENT_PARA")));
		}
		Set<TreePattern> list = new HashSet<TreePattern>(
				TreePatternGSR2TreePattern.values());

		// We create a list with unique tree patterns.
		// We extract the tuples from the document for the given tree patterns.
		HashMap<TreePattern, ArrayList<NTuple>> multiTuples = null;
		try {
			class ExtractorThread extends Thread {

				HashMap<TreePattern, ArrayList<NTuple>> multiTuples = null;
				DocNtwID docId = null;
				Set<TreePattern> list = null;
				boolean killed = false;

				public ExtractorThread(DocNtwID docId, Set<TreePattern> list){
					this.docId = docId;
					this.list = list;
				}

				@Override
				public void run(){
					try {
						multiTuples = (new MultiTuplesExtractor()).extractMultiTuples(docId, new ArrayList<TreePattern>(list));
					} catch (Exception e) {
						log.warn(e);
					}
				}
			}


			final ExtractorThread extractor = new ExtractorThread(docId, list);
			extractor.start();

			Timer timer = new Timer();

			//Kill the thread after MAX_EXTRACTION_TIME seconds
			timer.schedule(new TimerTask() {
				@Override
				@SuppressWarnings("deprecation")
				public void run() {
					log
					.error("Killing the extractor. Max extraction time exceeded. Strange extractor exceptions after this brutal stop (Thread.stop()) can be considered normal.");
					extractor.stop(); //Terminate the timer thread
					extractor.killed = true;
					extractor.multiTuples = new HashMap<TreePattern, ArrayList<NTuple>>();
				}
			}, MAX_EXTRACTION_TIME *1000);


			//Wait for the extractor to be either killed or finish its work
			while(extractor.multiTuples==null){
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					log.warn(e.toString());
				}
			}

			//if it managed to finish, just cancel the killing task
			timer.cancel();

			multiTuples = extractor.multiTuples;

			//If the extractor was finally killed, make sure that the size of the
			//view is advertised as MAX_VALUE and that it gets logged.
			if (extractor.killed) {
				return;
			}
		} catch (Exception e) {
			log.error("Exception", e);
		}

		/* Create a blocking queue of TupleBuckets. This list  holds the buckets that need to be sent to the view holders.
		 * The documentArrival module adds elements to the queue and the TupleSender threads consume them and send them
		 * to the appropriate view holders.
		 * */
		LinkedBlockingQueue<TupleBucket> tuple_queue = new LinkedBlockingQueue<TupleBucket>();

		// Create a set of therads and put them in an array. Start them so that they are ready to get tuple buckets for sending
		TupleSender tuple_senders[] = new TupleSender[TUPLE_SENDING_CONCURRENCY];
		for(int i=0;i<tuple_senders.length;i++){
			tuple_senders[i] = new TupleSender(tuple_queue, dbHolderId);
			tuple_senders[i].start();
		}

		// Finally we store the tuples for each pattern, treating different the JoinedPatterns without value joins and the
		// ones that had at least one value join (for the first ones we can store the tuples directly in the database and
		// for the latest we should trigger the method that will evaluate the value joins before storing the definitive
		// tuples in the database).

		for (TreePatternGSR jPatGSR : gsrsForDoc) {
			List<NTuple> tuples = multiTuples.get(TreePatternGSR2TreePattern
					.get(jPatGSR));
			// if (tuples == null || tuples.size() == 0) {
			// log.warn(docId.docName + " did not materialize "
			// + jPatGSR.getPropertyValue("ENVIRONMENT_PARA")
			// + " views (no tuples)");
			// continue;
			// }

			tuple_queue.add(new TupleBucket(jPatGSR, docId, tuples));


			counterTreePatterns++;
		}

		/* Wait until the queue is empty (so that all the tuple buckets have been taken over by a thread)*/
		while(!tuple_queue.isEmpty()){Thread.sleep(20);}

		/*Wait for every thread to finish*/
		for(int i=0;i<tuple_senders.length;i++){
			tuple_senders[i].stop = true;
			tuple_senders[i].join();
		}

		log.info("Completed materialization of " + docId.docName + " for "
				+ gsrsForDoc.size() + " joined patterns that contained "
				+ counterTreePatterns + " tree patterns");

	}

	/**
	 * Returns the complete path to the folder that contains the documents in
	 * this peer.
	 *
	 * @return the path to the folder
	 */
	public static String getFilepath() {
		return Parameters.getProperty("resources");
	}

	/**
	 * Returns the complete path to the file that contains the document.
	 *
	 * @return the filepath to the given document
	 */
	public static String getFilepath(String docName) {
		return Parameters.getProperty("resources") + "/" + docName;
	}

	/**
	 * Gets the content of an XML document from one peer.
	 *
	 * @param documentFileName the filename of the XML document
	 * @return the content of the XML document
	 * @throws RemoteException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public byte[] getDocumentFileContents(String documentFileName)
			throws FileNotFoundException, IOException {
		File documentFile = new File(getFilepath(documentFileName));
		FileInputStream fin = new FileInputStream(documentFile);
		byte fileContent[] = new byte[(int)documentFile.length()];
		fin.read(fileContent);
		fin.close();
		return fileContent;
	}

	private void parallelMaterialize(final DocNtwID document,
			Set<TreePatternGSR> jgsrs, final String dbHolderId) {
		ExecutorService threadpool = Executors.newFixedThreadPool(NUMBER_OF_EXTRACTORS);
		List<TreePatternGSR> gsrs = new ArrayList<TreePatternGSR>(jgsrs);

		int bucket_size = MAX_PATTERNS_TO_EXTRACT_IN_PARALLEL;
		int scheduled_patterns = 0;

		while(scheduled_patterns<gsrs.size()){
			final Set<TreePatternGSR> bucket = new TreeSet<TreePatternGSR>();

			for(int i=0; i< bucket_size && scheduled_patterns<gsrs.size();i++){
				bucket.add(gsrs.get(scheduled_patterns++));
			}

			threadpool.execute(new Runnable() {
				@Override
				public void run() {
					try {
						materialize(document, bucket, dbHolderId);
					} catch (Exception e) {
						log.warn(e.toString());
					}
				}
			});
		}
		threadpool.shutdown();
		try {
			while(!threadpool.awaitTermination(50, TimeUnit.MILLISECONDS)) {
				;
			}
		} catch (InterruptedException e) {
			log.warn(e.toString());
		}
	}

	/**
	 * This class is used to hold the triplet: {gsr, documentID, tuples}
	 * It is used as an element of the BlockingQueue on which the extractor
	 * puts the tuples that need to be sent to a view.
	 */
	private class TupleBucket {

		public TreePatternGSR gsr;
		public DocNtwID docId;
		public List<NTuple> tuples;

		TupleBucket(TreePatternGSR gsr, DocNtwID docId, List<NTuple> tuples) {
			this.gsr = gsr;
			this.docId = docId;
			this.tuples = tuples;
		}
	}

	/**
	 * This thread class is used in the simple thread pool that
	 * passes the tuples to the view holder after the extraction
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	private class TupleSender extends Thread{
		private String dbHolderId;
		private BlockingQueue<TupleBucket> tuple_queue;
		public boolean stop = false;

		TupleSender(BlockingQueue<TupleBucket> queue, final String dbHolderId) {
			this.tuple_queue = queue;
			this.dbHolderId = dbHolderId;
		}

		@Override
		public void run(){

			/* While you have not been told to stop, keep checking for elements in the blocking list.
			 * Do not block in the queue for more than 20ms (you may have been told to shutdown).
			 * If you get a non null TupleBucket, call the storage context and pass the tuples to the view holder.
			 * */
			while(!stop){
				try {
					TupleBucket bucket = this.tuple_queue.poll(20, TimeUnit.MILLISECONDS);
					if(bucket!=null){
						storageContext.storeTuples(bucket.gsr,
								bucket.docId, bucket.tuples, dbHolderId);
						log.info("Stored data from: "
								+ bucket.docId.docName
								+ " "
								+ bucket.gsr
								.getPropertyValue("ENVIRONMENT_PARA")
								+ " views (" + bucket.tuples.size()
								+ " tuples)");
					}
				} catch (Exception e) {
					log.error("Failed to store tuples: " + e);
				}
			}
		}
	}

	public void publishDocuments(final List<String> documents,
			final Set<TreePatternGSR> gsrs, final String dbHolderId)
					throws Exception {
		Collections.sort(documents);

		log.info("Data source speaking here: I have " + gsrs.size()
				+ " gsrs here.");

		final ExecutorService exec = Executors.newFixedThreadPool(1);

		//List<DocNtwID> docs = new LinkedList<DocNtwID>();
		for(final String document: documents){
			exec.submit(new Runnable() {
				@Override
				public void run() {
					parallelMaterialize(new DocNtwID(document), gsrs,
							dbHolderId);

					try {
						Thread.sleep(700);
					} catch (InterruptedException e) {
						;
					}
				}
			});
		}

		exec.shutdown();

		try {
			while(!exec.awaitTermination(100, TimeUnit.MILLISECONDS)) {
				;
			}
		} catch (InterruptedException e) {
			log.error(e);
		}

	}

}