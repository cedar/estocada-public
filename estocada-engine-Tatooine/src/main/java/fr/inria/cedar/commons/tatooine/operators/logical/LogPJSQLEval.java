package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical PJQL query operator for PostgreJSON
 * 
 * @author ranaalotaibi
 */
public class LogPJSQLEval extends LogLeafOperator {

    private String query;

    public LogPJSQLEval(final String query, final NRSMD meta) {
        this.query = query;
        this.setOwnName("LogPJQLEval");
        this.setVisible(true);
        this.setNRSMD(meta);
    }

    public LogPJSQLEval(final String query, final NRSMD meta, final StorageReference ref) {
        this.query = query;
        this.ref = ref;
        this.setOwnName("LogPJQLEval");
        this.setVisible(true);
        this.setNRSMD(meta);
    }

    @Override
    public int getJoinDepth() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public String getName() {
        return "LogPJQLEval";
    }

    @Override
    public String toString() {
        return "[LogPJQLEval query: " + query + "]";
    }

    @Override
    public void accept(LogOperatorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public LogOperator deepCopy() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedCardinality() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedIOCost() {
        throw new UnsupportedOperationException("Unsupported");
    }

    public String getQueryString() {
        return query;
    }

    public StorageReference getGSR() {
        return ref;
    }

}
