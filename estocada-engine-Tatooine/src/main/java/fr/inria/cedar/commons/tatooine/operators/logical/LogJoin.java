package fr.inria.cedar.commons.tatooine.operators.logical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/**
 * Join logical operator between both children. The column indexes in the SimplePredicate are
 * the ones of the Cartesian product of left and right. Thus, if left has 5 columns, the first
 * column of right will be 5. Further, the column indexes go from 0 to n-1.
 *
 * @author Ioana MANOLESCU
 */
public class LogJoin extends LogBinaryOperator {
	private static final Logger log = Logger.getLogger(LogJoin.class);
	private boolean useStackTreeAnc;
	private double cost;

	public LogJoin(LogOperator left, LogOperator right, Predicate pred) {
		super(left, right);
		this.setPredicate(pred);
		this.setOwnName("Join");
		this.setVisible(true);
		this.useStackTreeAnc = false;
		this.cost = 0;

		try {
			SimplePredicate sp = (SimplePredicate) pred;
			int[] ancPath = sp.ancPath;
			int[] aux;

			if (sp.ancPath != null && sp.ancPath.length > 0) {
				aux = new int[ancPath.length - 1];
				for (int i = 0; i < aux.length; i++) {
					aux[i] = ancPath[i];
				}
			} else {
				aux = new int[0];
			}
			if (aux.length > 0) {
				try {
					this.setNRSMD(NRSMD.addNestedField(left.getNRSMD(), aux, right.getNRSMD()));
				} catch (TatooineExecutionException e) {
					log.error("Wrong metadata! TatooineExecutionException: ", e);
				}
			} else {
				try {
					this.setNRSMD(NRSMD.appendNRSMD(left.getNRSMD(), right.getNRSMD()));
				} catch (TatooineExecutionException e) {
					log.error("Wrong metadata! TatooineExecutionException: ", e);
				}
			}
		} catch (ClassCastException e) {
			// The predicate is a conjunctive one. In this case we do not care about nesting
			// and just append the NRSMDs.
			// The main application for conjunctive join predicates is to evaluate id and structural
			// joins that compare nodeIDs and docIDs.
			try {
				this.setNRSMD(NRSMD.appendNRSMD(left.getNRSMD(), right.getNRSMD()));
			} catch (TatooineExecutionException e2) {
				log.error("Wrong metadata! TatooineExecutionException: ", e2);
			}
		}

	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object myClone = null;
		try {
			myClone = new LogStructJoin((LogOperator) getLeft().clone(), (LogOperator) getRight().clone(),
					getPredicate());
			((LogJoin) myClone).useStackTreeAnc = this.useStackTreeAnc;
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return myClone;
	}

	/**
	 * Makes a deep copy of the current operator.
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogJoin deepCopy() {
		LogJoin copy = null;
		copy = new LogJoin(this.getLeft().deepCopy(), this.getRight().deepCopy(), (Predicate) this.getPredicate()
				.clone());
		copy.setName(this.getName());
		copy.useStackTreeAnc = this.useStackTreeAnc;
		copy.setEquivalentPattern(this.getEquivalentPattern());
		return copy;
	}

	@Override
	public String toString() {
		return this.getOwnName() + " " + this.getLeft() + " " + this.getRight() + " ";
	}

	@Override	public void accept(LogOperatorVisitor visitor){
		getLeft().accept(visitor);
		getRight().accept(visitor);
		visitor.visit(this);
	}

	/**
	 * True if {@link StackTreeAnc} should be used,
	 * false if {@link StackTreeDesc} should be
	 * used.
	 *
	 * @return a boolean
	 */
	public boolean useStackTreeAnc() {
		return this.useStackTreeAnc;
	}

	/**
	 * True if {@link StackTreeAnc} should be used,
	 * false if {@link StackTreeDesc} should be
	 * used.
	 *
	 * @param useStackTreeAnc the boolean
	 */
	public void setUseStackTreeAnc(boolean useStackTreeAnc) {
		this.useStackTreeAnc = useStackTreeAnc;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getCost() {
		return cost;
	}

	@Override
	public double estimatedCardinality() {
		return Math.ceil(this.getLeft().estimatedCardinality() * this.getRight().estimatedCardinality() * 0.01d);
	}

	@Override
	public double estimatedIOCost() {
		return this.getLeft().estimatedIOCost() + this.getRight().estimatedIOCost();
	}
}
