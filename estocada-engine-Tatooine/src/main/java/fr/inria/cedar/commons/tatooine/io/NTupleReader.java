package fr.inria.cedar.commons.tatooine.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.CompactDynamicDeweyElementID;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.IDs.PrePostElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.utilities.NTupleUtils;

/**
 * This reader class creates NTuples objects from TXT files with specific formats described below
 * @author Oscar Mendoza
 */
public class NTupleReader {
	
	private static final Logger log = Logger.getLogger(NTupleReader.class);
	private static final char DELIMITER = ',';
	private static final char L_PAREN   = '(';
	private static final char R_PAREN   = ')';
	
	/* Note: Format of the files */
	// Each line is a different tuple
	// () = indicates a tuple
	// A delimiter character is used to separate different elements
	// Characters in between quotation marks ("") are of type string
	// Numbers without prefixes are of type integer
	// Numbers with prefixes "u", "o", "s" or "x" are of type ID
	// U = unique ID, O = ordered ID, S = structural ID, X = update ID
	// Nested tuples are represented by values inside of balanced parentheses (())

	// Path of the file to scan
	private String path;
	
	// Line of the file we're currently scanning
	private static String line;
	
	// Number of the line of the file we're currently scanning
	private static int current;
	
	// Map of matching opening and closing parentheses found in the line we're currently scanning
	private static HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();    

	// Constructor
	public NTupleReader(String path) {
		this.path = path;
	}	
	
	/** Checks if a string has balanced parentheses */
    private boolean isBalanced(String str) {
    	if (Strings.isNullOrEmpty(str)) {
    		return false;
    	} else if (str.charAt(0) != L_PAREN || str.charAt(str.length() - 1) != R_PAREN) {
    		// A line should start with an opening parenthesis and end with a closing one
    		return false;
    	} else {
    		Stack<Character> stack = new Stack<Character>();
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == L_PAREN) {
                	stack.push(L_PAREN);
                }
                else if (str.charAt(i) == R_PAREN) {
                    if (stack.isEmpty()) {
                    	return false;
                    }
                    if (stack.pop() != L_PAREN) {
                    	return false;
                    }
                }
            }
            return stack.isEmpty();
    	}        
    }
    
    /** Returns a map of matching positions of opening and closing parentheses in a string */
    private HashMap<Integer, Integer> findMatchingParentheses(String str) {
    	int depth = 0;
    	
    	// Finds the position of all opening parentheses in a string
    	List<Integer> positions = new ArrayList<Integer>();
    	for (int index = str.indexOf(L_PAREN); index >= 0; index = str.indexOf(L_PAREN, index + 1)) {
    		positions.add(index);
    	}
    	
    	// Finds the position of the matching closing parentheses for all opening parentheses in a string
    	for (int i : positions) {    	    		
    		for (int j = i; j < str.length(); j++) {
    			if (str.charAt(j) == L_PAREN) {
        			depth++;
        		} else if (str.charAt(j) == R_PAREN) {
        			depth--;
        		}    			
    			if (i != j && depth == 0) {
    				map.put(i, j);
    				j = str.length();
    			}
    		}
    	}
    	return map;
    }
	
	/** Returns a list of NTuple objects built from NTuple definitions that it reads from a TXT file */
	public ArrayList<NTuple> read() throws TatooineExecutionException {
		ArrayList<NTuple> tuples = new ArrayList<NTuple>();
		
		// Resets static variables
		current = 1;
		NTupleUtils utils = new NTupleUtils();
		map = new HashMap<Integer, Integer>(); 
		
		// Reads the TXT file
		File f = new File(path);
		if (!f.exists() || !f.isFile()) {
			log.error("The file " + f.getAbsolutePath() + " was not found. Please make sure it exists in the supplied location.");
			System.exit(0);
		}
				
		// Scans the TXT file
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			while (br.ready()) {									
				line = br.readLine().trim();
				
				// Checks if the line is a comment
				if (Strings.isNullOrEmpty(line) || line.startsWith("#")) {
					continue;
				}
				
				// Checks for balanced parameters
				boolean isBalanced = isBalanced(line);
				if (isBalanced) {
					// Finds the position within the line of the TXT file of all matching opening and closing parameters
					map = findMatchingParentheses(line);
				}
								
				// Fetches content in between parentheses
				String content = line.substring(1, line.length() - 1);			
				final int len = content.length();
				
				// We parse each line character by character and build NTuple fields out of each element we find
				for (int i = 0; i < len; i++) {		
					utils = readCharacter(i, content, utils);
					i = utils.counter;
				}

				// Builds NTuple
				tuples.add(utils.buildTuple());
				
				// Resets utility class
				utils = new NTupleUtils();
				
				current++;
			}
			
		} catch (IOException e) {
			log.error(String.format("Unknown error reading + parsing line %d of \'%s\'", current, path));
			System.exit(0);
		}

		return tuples;
	}
	
	/**
	 * Creates an NTupleReaderUtils from each line or substring of a TXT file containing NTuple definitions. 
	 * @param pos: Position of the line or substring we're currently parsing.
	 * @param str: Line of substring we want to parse.
	 * @param utils: Utility object used to store NTuple fields found when parsing.
	 * @return
	 * @throws TatooineExecutionException
	 */
	private NTupleUtils readCharacter(int pos, String str, NTupleUtils utils) throws TatooineExecutionException {
		char c = str.charAt(pos);
		int len = str.length();
		
		// Control variables
		int index = 0;
		String sub = "";
		
		switch (c) {
		
		// String
		case '"':
			// A quotation mark signals the presence of a string field
			// Let's first look for a closing quotation mark in the remaining characters in the line
			sub = str.substring(pos + 1, len);
			index = sub.indexOf('"');
			
			// If no closing quotation mark exists then the line is badly formatted
			if (index == -1) {
				log.error(String.format("Error on line %d of \'%s\': Missing closing quotation mark for string value", current, path));
				System.exit(0);
			}
			
			// Otherwise let's fetch the content in between the opening and closing quotation marks
			else {
				String s = sub.substring(0, index);
				// We add to our pointer the position of the index + 2 extra spaces (to account for the two quotation marks)
				pos += index + 2; 
				utils.addString(s);
				utils.addType(TupleMetadataType.STRING_TYPE);				
			}
			break;
		
		// Nested tuple
		case '(':
			// An open parenthesis marks the presence of a nested tuple
			// Let's fetch its matching closing parenthesis
			
			// The variable STR is a substring of the line we're scanning and POS is a pointer to a position in STR
			// We need to add an offset to have POS refer to a position in our original line
			int offset = line.indexOf(str);
			
			// Fetches the position of the closing parenthesis that matches our opening parenthesis
			index = map.get(pos + offset); 
						
			// Fetches content in between parentheses
			// The same offset computed above is needed for this
			String content = line.substring(pos + offset + 1, index);
			final int length = content.length();
				
			// This utility object will store all fields of the nested tuple
			NTupleUtils xtils = new NTupleUtils();
			
			// Recursion alert!
			// We parse each line character by character and build NTuple fields out of each element we find
			for (int i = 0; i < length; i++) {
				xtils = readCharacter(i, content, xtils);
				i = xtils.counter;
			}			
			
			// Builds NTuple
			utils.addNestedField(xtils.buildTuple());
			utils.addType(TupleMetadataType.TUPLE_TYPE);
			utils.addChild(new NRSMD(xtils.getTypes().size(), xtils.getTypes().toArray(new TupleMetadataType[xtils.getTypes().size()])));
			
			// We advance our pointer to the position of the index (the end of the nested tuple)
			// The offset computed above is needed for this as well
			pos = index - line.indexOf(str);
			break;
			
		// IDs
		case 'u':
		case 'o':
		case 's':
		case 'x':
			// One of our known prefixes ("u", "o", "s", "x") signals the presence of an ID element
			// Let's first look for a closing character (comma or closing parenthesis)
						
			// sub = str.substring(pos + 1, len);
			// index = sub.indexOf('"');
			sub = str.substring(pos + 1, len);
			index = sub.indexOf(',') != -1 ? sub.indexOf(',') : sub.indexOf(')');

			// If there are no more characters in the string after the ID we found we can fetch its value right away 
			if (StringUtils.isNumeric(sub)) {
				pos += sub.length() + 1;
				utils = buildID(c, Integer.valueOf(sub), utils);
			}
			
			// If no closing character exists then the line is badly formatted
			else if (index == -1) {
				log.error(String.format("Error on line %d of \'%s\': Missing delimiter for ID value", current, path));
				System.exit(0);
			}
			
			// Otherwise let's fetch the ID found after the prefix
			else {
				int n = 0;
				try {
					n = Integer.valueOf(sub.substring(0, index));
				} catch (NumberFormatException nfe) {
					log.error(String.format("Error on line %d of \'%s\': ID value contains alphanumerical characters", current, path));
					System.exit(0);
				}
				
				// We advance our pointer to the position of the index
				pos += index + 1;
				
				// Adds corresponding ElementID
				utils = buildID(c, n, utils);						
			}
			break;
			
		case DELIMITER:
			break;
			
		default:
			// Integer						
			if (Character.isDigit(c)) {
				// A digit with no prefix signals the presence of an integer field
				sub = str.substring(pos, len);
				index = sub.indexOf(',') != -1 ? sub.indexOf(',') : sub.indexOf(')');
				
				// If there are no more characters in the string after the integer we found we can fetch its value right away 
				if (StringUtils.isNumeric(sub)) {
					pos += sub.length();
					utils.addInt(Integer.parseInt(sub));
					utils.addType(TupleMetadataType.INTEGER_TYPE);
				}
				
				// Otherwise let's look for a closing character (comma or closing parenthesis)
				// If no closing character exists then the line is badly formatted
				else if (sub.length() > 1 && index == -1) {
						log.error(String.format("Error on line %d of \'%s\': Missing delimiter for integer value", current, path));
						System.exit(0);
					} 
				
				// If a closing character is present then let's fetch the value of our integer
				else {
						int n = 0;
						try {
							n = Integer.valueOf(sub.substring(0, index));
						} catch (NumberFormatException nfe) {
							log.error(String.format("Error on line %d of \'%s\': Integer value contains alphanumerical characters", current, path));
							System.exit(0);
						}
						pos += index;
						utils.addInt(n);
						utils.addType(TupleMetadataType.INTEGER_TYPE);
				}
			} else {
				log.error(String.format("Error on line %d of \'%s\': \'%s\' is not a valid type prefix", current, path, c));
				System.exit(0);
			}								
		}
		
		utils.counter = pos;
		return utils;
	}
	
	/** Builds ElementID depending on the given prefix */
	private NTupleUtils buildID(char prefix, int n, NTupleUtils utils) {
		switch (prefix) {
		case 'u':
		case 'o':
			utils.addId(new OrderedIntegerElementID(n));
			utils.addType(TupleMetadataType.ORDERED_ID);
			break;
		case 's':
			utils.addId(new PrePostElementID(n));
			utils.addType(TupleMetadataType.STRUCTURAL_ID);
			break;
		case 'x':
			utils.addId(new CompactDynamicDeweyElementID(new int[]{n}, new int[0]));
			utils.addType(TupleMetadataType.UPDATE_ID);
			break;
		}
		return utils;
	}
	
}
