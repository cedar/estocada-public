package fr.inria.cedar.commons.tatooine.operators.physical.join;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.utilities.Utilities;

/**
 * Does a (naive) bindjoin where the tuple from the right child get aggregated
 * into a nested tuple field
 * 
 * @author Raphael BONAQUE
 */
public class NestedBindJoin extends PhyBindJoin {

	private static final long serialVersionUID = 222161437593154262L;
	private NRSMD nestedFieldNRSMD;

	public NestedBindJoin(NIterator left, BindAccess right,	int[] bindingPattern) throws TatooineExecutionException {
		super(left, right, bindingPattern);
		TupleMetadataType[] nestedFieldMetas = {TupleMetadataType.STRUCTURAL_ID};
		String[] nestedFieldNames = {"nested_field"};
		NRSMD[] nestedFieldSubtype = {right.nrsmd};
		nestedFieldNRSMD = new NRSMD(1, nestedFieldMetas, nestedFieldNames, nestedFieldSubtype);
		nrsmd = NRSMD.appendNRSMD(left.nrsmd, nestedFieldNRSMD);
	}
	
	
	public NestedBindJoin(NIterator left, BindAccess right, Predicate pred) throws TatooineExecutionException {
		this(left, right, constructBindingPattern(pred,left.nrsmd.colNo));
	}

	@Override
	protected NTuple computeNext() throws TatooineExecutionException{
		while (true) {
			//We check if we need a new tuple from the left
			if (currentLeftTuple == null) {
				if (!left.hasNext()) return null;
				currentLeftTuple = left.next();
				right.setParameters(computeBinding(currentLeftTuple));
			}
			
			if (!right.hasNext()) currentLeftTuple = null;
			else {
				NTuple aggregationTuple = new NTuple(nestedFieldNRSMD);
				aggregationTuple.setNestedField(0, Utilities.listFromNIterator(right));
				return computeOutput(currentLeftTuple, right.next());
			}
		}
	}
	
	}
