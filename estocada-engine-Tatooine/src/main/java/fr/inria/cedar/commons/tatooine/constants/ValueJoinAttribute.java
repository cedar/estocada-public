package fr.inria.cedar.commons.tatooine.constants;

import com.google.common.base.Strings;

/**
 * Enumerated type representing the possible attributes of a PatternNode on which a value join may refer
 * 
 * @author Konstantinos KARANASOS
 * @created 21/1/2010
 */
public enum ValueJoinAttribute {
	DOCID, ID, VAL, CONT;

	/**
	 * Returns the corresponding member of the enumerated type for a given string.
	 * @param stringAttr
	 * @return
	 */
	public static ValueJoinAttribute getAttrEnum(String stringAttr) {
		if (Strings.isNullOrEmpty(stringAttr)) {
			return null;
		}
		try {
			return ValueJoinAttribute.valueOf(stringAttr.toUpperCase());
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Returns the string representation of the current value of the enumerated type.
	 * @return
	 */
	public String toString() {
		switch (this) {
		case DOCID:
			return "docID";
		case ID:
			return "ID";
		case VAL:
			return "Val";
		case CONT:
			return "Cont";
		default:
			return null;
		}
	}
}
