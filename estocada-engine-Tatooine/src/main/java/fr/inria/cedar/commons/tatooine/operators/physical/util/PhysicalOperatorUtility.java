package fr.inria.cedar.commons.tatooine.operators.physical.util;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.Buffer;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * @author Asterios KATSIFODIMOS
 * @author Ioana MANOLESCU
 */
public class PhysicalOperatorUtility {

	public static int compareCharArrays(char[] c1, char[] c2) {
		boolean smallerAnc = false;
		boolean smallerDesc = false;
		int i = 0;
		while (i < c1.length && i < c2.length && !smallerAnc && !smallerDesc) {
			if (c1[i] < c2[i]) {
				smallerAnc = true;
			} else {
				if (c1[i] > c2[i]) {
					smallerDesc = true;
				}
			}
			i++;
		}
		if (smallerAnc) {
			return -1;
		}
		if (smallerDesc) {
			return 1;
		}
		return 0;
	}

	public static void alignDoc(Buffer left, Buffer right) throws TatooineExecutionException {
		// Parameters.logger.info("AlignDoc just started!");
		String leftDoc = "", rightDoc = "";

		do {
			// to proceed to a join, we need to have tuples of the same
			// document in both inputs.
			if (leftDoc == "" && left.hasNext())
				leftDoc = left.next().getDocID();

			if (rightDoc == "" && right.hasNext())
				rightDoc = right.next().getDocID();

			// depending on the input, we are going to skip
			// tuples of the side with the side with the "older" document (here we make the assumption that
			// the document ID also denotes its publication time)
			if (leftDoc.compareTo(rightDoc) < 0) {
				leftDoc = skip(left, leftDoc); // throw away all tuples with leftDoc document
			} else if (leftDoc.compareTo(rightDoc) > 0) {
				rightDoc = skip(right, rightDoc); // throw away all tuples with rightDoc document
			}

		} while (!leftDoc.equals(rightDoc) && !NIterator.timeout);

		left.rollBack();
		right.rollBack();
		// Parameters.logger.info("AlignDoc just finished!");
	}

	private static String skip(NIterator operator, String docToSkip) throws TatooineExecutionException {
		String curDoc = docToSkip;
		while (curDoc.equals(docToSkip) && operator.hasNext()) {
			curDoc = operator.next().getDocID();
		}

		return curDoc;
	}

}
