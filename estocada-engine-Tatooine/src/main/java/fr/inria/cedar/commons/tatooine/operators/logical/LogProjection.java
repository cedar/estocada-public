package fr.inria.cedar.commons.tatooine.operators.logical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Projection logical operator.
 *
 * @author Ioana MANOLESCU
 */
public class LogProjection extends LogUnaryOperator implements Cloneable {
	private static final Logger log = Logger.getLogger(LogProjection.class);
	public int[] columns;

	/**
	 * This constructor is used when considering unnested tables.
	 *
	 * @param child
	 * @param columns
	 * @throws TatooineExecutionException
	 */
	public LogProjection(LogOperator child, int[] columns) {
		super(child);
		this.setOwnName("LogProj");
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 0; i < columns.length; i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append(columns[i]);
		}
		sb.append("]");
		this.setOwnDetails(new String(sb));
		this.columns = columns;
		this.setVisible(true);
		try {
			this.setNRSMD(NRSMD.makeProjectRSMD(child.getNRSMD(), columns));
		} catch (TatooineExecutionException e) {
			log.error("Wrong metadata! TatooineExecutionException: ", e);
		}
	}

	@Override	public void accept(LogOperatorVisitor visitor){
		getChild().accept(visitor);
		visitor.visit(this);
	}

	@Override
	public void recDisplayNRSMD(StringBuffer sb) {
		sb.append(this.getOwnName() + " " + this.getNRSMD().toString() + "(");
		getChild().recDisplayNRSMD(sb);
		sb.append(" [");
		for (int i = 0; i < columns.length; i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append(columns[i]);
		}
		sb.append("]");
		sb.append(")");
	}

	/**
	 * Makes a deep copy of the current operator
	 * Important note: The current deep copy does not take into consideration the existence of a projMask
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogProjection deepCopy() {
		LogProjection copy = null;
		copy = new LogProjection(this.getChild().deepCopy(), this.columns.clone());
		copy.setName(this.getName());
		copy.setOwnDetails(this.getOwnDetails());
		copy.setEquivalentPattern(this.getEquivalentPattern());
		return copy;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return new LogProjection((LogOperator) getChild().clone(), this.columns);
	}

	@Override
	public double estimatedCardinality() {
		return inputCardinality();
	}

	@Override
	public double estimatedIOCost() {
		return childrenIOCost();
	}
}
