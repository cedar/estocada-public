package fr.inria.cedar.commons.tatooine.loader.multidoc;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

/**
 * Subclass of GSR that represents the Global Storage Reference (GSR) used by JoinedPatterns. These and only these are
 * the GSRs that will be indexed in the DHT (these actually refer to the JoinedPattern views that exist in the system).
 * <br>
 * The objects of this class have the following <b>string</b> parameters: 
 * <li>ENVIRONMENT_PARA -> the directory path to the database. 
 * <li>XAMString -> when it refers to a Pattern, it keeps the string representation of the expanded version of it. 
 * <li>initialXAMString -> when it refers to a Pattern, it keeps the initial string representation of the Pattern.
 *
 * @author Konstantinos KARANASOS
 */
public class TreePatternGSR extends GSR {
	private static final Logger log = Logger.getLogger(TreePatternGSR.class);
	
	/** Universal version identifier for the TreePatternGSR class */
	private static final long serialVersionUID = -8766802762857347557L;

	/**
	 * @param givenViewFName The filename of the JoinedPattern where this PatternGSR points to
	 */
	public TreePatternGSR(String givenViewFName) {
		super(givenViewFName);
		try {
			setProperty("XAMString", XAMParserUtility.getTreePatternFromFile(givenViewFName).toXAMString());
		} catch (Exception e) {
			log.error("Could not build the internal PatternGSR for the pattern: ", e);
		}
	}

	public TreePatternGSR(TreePattern pattern) {

		super(pattern.getName());

		try {
			setProperty("XAMString", pattern.toXAMString());
		} catch (Exception e) {
			log.error("Could not build the internal PatternGSR for the pattern: ", e);
		}

	}

}
