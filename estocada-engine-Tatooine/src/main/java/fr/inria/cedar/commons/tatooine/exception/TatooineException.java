package fr.inria.cedar.commons.tatooine.exception;

/**
 * Class that defines a Tatooine exception. Tatooine exceptions will be thrown when Tatooine is supposed to crash.
 *
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public class TatooineException extends Exception {

	private static final long serialVersionUID = -3819688241961585279L;

	public TatooineException(String s) {
		super(s);
	}

}