package fr.inria.cedar.commons.tatooine.operators.physical.join;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.NTupleStack;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyArrayIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.util.PhysicalOperatorUtility;

/**
 * Stack-based structural join that returns result ordered by the ancestor ID.
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 */
public class StackTreeAnc extends StructuralJoin {
	private static final Logger log = Logger.getLogger(StackTreeAnc.class);

	/** Universal version identifier for the StackTreeAnc class */
	private static final long serialVersionUID = 5292410661008617639L;

	private ArrayList<NTuple> result;
	private NTuple currentTuple;

	private boolean resultsLocked;

	private HashMap<NTuple, ArrayList<NTuple>> nestedResults;
	private HashMap<NTuple, Integer> nestedCounters;

	public StackTreeAnc(NIterator left, NIterator right, int leftIdx, int rightIdx, PredicateType idPredCode)
			throws TatooineExecutionException {
		// child == true means we are only interested in direct children
		super(left, right, leftIdx, rightIdx, idPredCode);
		log.info("StackTreeAnc this.getName(): " + this.getName());
		log.info("left.nrsmd");
		left.nrsmd.display();
		log.info("right.nrsmd");
		right.nrsmd.display();
		this.nrsmd = NRSMD.appendNRSMD(left.nrsmd, right.nrsmd);
		log.info("after append");
		this.nrsmd.display();

		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.setOrderMaker(left.getOrderMaker());
		} else {
			this.setOrderMaker(OrderMarker.shift(right.getOrderMaker(), left.nrsmd.colNo));
		}

		// logger.debug(this.getName() + " LEFT INPUT:");
		// this.left.display();
		// logger.debug(this.getName() + " RIGHT INPUT:");
		// this.right.display();

		increaseOpNo();
	}

	// Constructor for multi-doc data
	public StackTreeAnc(NIterator left, NIterator right, int leftIdx, int rightIdx, int leftDocIdx, int rightDocIdx,
			PredicateType idPredCode) throws TatooineExecutionException {
		super(left, right, leftIdx, rightIdx, leftDocIdx, rightDocIdx, idPredCode);

		this.nrsmd = NRSMD.appendNRSMD(left.nrsmd, right.nrsmd);

		int leftSize = left.nrsmd.colNo;
		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.setOrderMaker(left.getOrderMaker());
			this.setOrderMaker(this.getOrderMaker().addToGroup(leftDocIdx, (rightDocIdx + leftSize)));
		} else {
			this.setOrderMaker(OrderMarker.shift(right.getOrderMaker(), left.nrsmd.colNo));
			this.setOrderMaker(this.getOrderMaker().addToGroup((rightDocIdx + leftSize), leftDocIdx));
		}

		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		ancestorChild.open();
		descendantChild.open();
		initialize();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public void initialize() throws TatooineExecutionException {
		this.noMoreAncestors = false;
		this.noMoreDescendants = false;
		this.joinStack = new NTupleStack();
		result = new ArrayList<NTuple>();
		nestedResults = new HashMap<NTuple, ArrayList<NTuple>>();
		nestedCounters = new HashMap<NTuple, Integer>();
		resultsLocked = true;

		noMoreChildrenTuples = false;
		currentTupleAncestor = null;
		currentTupleDescendant = null;

		// CONTINUOUS: MAKE SURE YOU RETAIN THE LOST TUPLE HERE. ADD A BOOLEAN TO CHECK IF THE FIRST WAS SKIPPED AND
		// RETAINED
		noMoreAncestors = (!ancestorChild.hasNext());
		if (!noMoreAncestors) {
			currentTupleAncestor = ancestorChild.next();
		}
		noMoreDescendants = (!descendantChild.hasNext());
		if (!noMoreDescendants) {
			currentTupleDescendant = descendantChild.next();
		}
		noMoreChildrenTuples = noMoreDescendants || noMoreAncestors;
		if (!noMoreChildrenTuples) {
			alignDoc();
		}
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// logger.debug("\n\n\nSTA.next");

		if (timeout) {
			return false;
		}

		if (noMoreChildrenTuples && joinStack.empty()) {
			return false;
		}

		while (!noMoreChildrenTuples || !joinStack.empty()) {
			if (timeout) {
				return false;
			}

			// logger.debug("\n\nInside the outer loop searching for results ");

			if (!resultsLocked) {
				Iterator<NTuple> itStack = joinStack.elems.iterator();
				while (itStack.hasNext()) {

					if (timeout) {
						return false;
					}

					NTuple crtStack = itStack.next();
					result = nestedResults.get(crtStack);
					Integer thisCursor = nestedCounters.get(crtStack);
					if (result != null && thisCursor.intValue() < result.size()) {
						currentTuple = result.get(thisCursor.intValue());
						nestedCounters.put(crtStack, new Integer(thisCursor.intValue() + 1));
						// logger.debug("Extracting result from the stack");
						return true;
					}
				}
				// if we get to here, the stack has been exhausted
				// we pop everything
				// logger.debug("The stack has been exhausted, popping all");
				while (!joinStack.empty()) {

					if (timeout) {
						return false;
					}

					NTuple t = joinStack.pop();
					nestedResults.remove(t);
					nestedCounters.remove(t);
				}
				// locking back the results (empty for now)
				resultsLocked = true;
			}

			// If we exit from here, there was no result ready to return
			boolean mustBreak = false;

			while (!mustBreak && (!noMoreChildrenTuples || !joinStack.empty())) {

				if (timeout) {
					return false;
				}

				boolean mustAlign = false;
				// logger.debug("\n\n Inside the inner loop ");

				// serves to make sure that only no branch is enabled after another
				boolean choiceMade = false;

				// stack not empty
				boolean branch1 = true;
				// !left is over && leftID!= null && rightID != null
				boolean branch2 = true;
				// tRight != null
				boolean branch3 = true;

				ElementID ancID = null;
				if (currentTupleAncestor != null) {
					ancID = currentTupleAncestor.getIDField(ancestorColumn);
				}
				ElementID descID = null;
				if (currentTupleDescendant != null) {
					descID = currentTupleDescendant.getIDField(descendantColumn);
				}
				NTuple tStack = null;
				ElementID stackID = null;

				if (joinStack.empty()) {
					branch1 = false;
					// logger.debug("Branch 1 invalidated 1");
				}
				if (noMoreAncestors) {
					// branch1 = false;
					branch2 = false;
					// logger.debug("Branches 1 and 2 invalidated 2");
				}
				if ((ancID == null) || (descID == null)) {
					branch2 = false;
					// logger.debug("Branch 2 invalidated 3");
				}
				if (currentTupleDescendant == null) {
					branch3 = false;
					// logger.debug("Branch 3 invalidated 4");
				}

				if (!branch1 && !branch2 && !branch3) {
					noMoreChildrenTuples = true;
					// //logger.debug("Going out !");
					break;
				}

				if (branch1 && !choiceMade) {
					// logger.debug("Branch 1, deciding if we unlock results or not");

					// unlock if we are at the end of execution and there is nothing left
					if (descID == null) {
						resultsLocked = false;
						// logger.debug("Unlocked results, end of right");
					} else {
						ElementID stackID0 = joinStack.elems.get(0).getIDField(ancestorColumn);
						if (stackID0.startsAfter(descID) || descID.endsAfter(stackID0)) {
							resultsLocked = false;
							// logger.debug("Unlocked results, element IDs don't fit");
						}
					}
					if (!resultsLocked) {
						mustBreak = true;
						choiceMade = true;
					} else {
						// logger.debug("Not unlocked, moving to next branches");
					}
				}

				if (branch2 && !choiceMade) {
					// logger.debug("Branch 2, trying to push ");
					if ((descID.startsAfter(ancID) || descID.equals(ancID))
							&& PhysicalOperatorUtility.compareCharArrays(
									currentTupleAncestor.getUriField(ancestorDocColumn),
									currentTupleDescendant.getUriField(descendantDocColumn)) == 0) {
						// logger.debug("Branch 2, pushed: " + tLeft.toString());
						joinStack.push(currentTupleAncestor);
						nestedResults.put(currentTupleAncestor, new ArrayList<NTuple>());
						nestedCounters.put(currentTupleAncestor, new Integer(0));
						noMoreAncestors = (!ancestorChild.hasNext());
						if (!noMoreAncestors) {
							currentTupleAncestor = ancestorChild.next();
						} else {
							currentTupleAncestor = null;
						}
						choiceMade = true;
					}
				} else {
					// logger.debug("No branch 2");
				}

				if (branch3 && !choiceMade) {
					// print("Branch 3! Producing results from the whole stack and ", tRight);
					Iterator<NTuple> it = joinStack.iterator();
					int iProduced = 0;
					while (it.hasNext()) {

						if (timeout) {
							return false;
						}

						tStack = it.next();
						stackID = tStack.getIDField(ancestorColumn);
						descID = currentTupleDescendant.getIDField(descendantColumn);
						char[] stackDoc = tStack.getUriField(ancestorDocColumn);
						char[] descDoc = currentTupleDescendant.getUriField(descendantDocColumn);
						if (PhysicalOperatorUtility.compareCharArrays(stackDoc, descDoc) != 0) {
							continue;
						}
						if (stackID.startsAfter(descID)) {
							continue;
						}
						if (descID.endsAfter(stackID)) {
							continue;
						}
						if (this.predicate == PredicateType.PREDICATE_PARENT
								|| this.predicate == PredicateType.PREDICATE_CHILD) {
							if (!stackID.isParentOf(descID)) {
								continue;
							}
						}
						NTuple tRes;
						if (this.predicate == PredicateType.PREDICATE_ANCESTOR
								|| this.predicate == PredicateType.PREDICATE_PARENT) {
							tRes = NTuple.append(this.nrsmd, tStack, currentTupleDescendant);
						} else {
							tRes = NTuple.append(this.nrsmd, currentTupleDescendant, tStack);
						}
						// add this result tuple to the pack of tuples corresponding to
						// the left-hand ingredient.
						ArrayList<NTuple> v = nestedResults.get(tStack);
						if (v == null) {
							v = new ArrayList<NTuple>();
						}
						v.add(tRes);
						iProduced++;
						// print("Added to " + stackID.toString() + " ", tRes);
						// print("There are now " + v.size() + " tuples associated to ", tStack);
						nestedResults.put(tStack, v);
					}
					noMoreDescendants = (!descendantChild.hasNext());
					if (!noMoreDescendants) {
						currentTupleDescendant = descendantChild.next();
						// print("Got from right: ", tDesc);
						if (ancestorDocColumn > -1) {
							char[] newTDescDocID = currentTupleDescendant.getUriField(descendantDocColumn);
							int k2 = PhysicalOperatorUtility.compareCharArrays(currentDocID, newTDescDocID);
							if (k2 < 0) {// this new right tuple has a bigger doc ID
								mustAlign = true;
							} else {
								assert (k2 <= 0) : "Wrong docID order"; // this new right tuple has a smaller docID, not
																		// supposed to happen
							}
						}
					} else {
						currentTupleDescendant = null;
					}
					choiceMade = true;
					if (iProduced > 0) {
						// logger.debug("We have new tuples, going out");
						mustBreak = true;
					}
				} else {
					// logger.debug("No branch 3");
				}
				if (mustAlign) {
					// logger.debug("Aligning");
					alignDoc();
				}
				noMoreChildrenTuples = noMoreAncestors && noMoreDescendants;
			}
			// logger.debug("Out of inner loop");
		}

		// logger.debug("Out of outer loop");
		return false;
	}

	@Override
	public final void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());

		ancestorChild.close();
		descendantChild.close();

		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		this.numberOfTuples++;
		return currentTuple;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return new PhyArrayIterator(currentTuple.getNestedField(i), currentTuple.nrsmd.getNestedChild(i));
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		if (leftDocIdx < 0) {
			return new StackTreeAnc(this.leftChild.copy(), this.rightChild.copy(), this.leftIdx, this.rightIdx,
					this.predicate);
		} else {
			return new StackTreeAnc(this.leftChild.copy(), this.rightChild.copy(), this.leftIdx, this.rightIdx,
					this.leftDocIdx, this.rightDocIdx, this.predicate);
		}
	}

	@Override
	public String getName() {
		String predToString;

		if (this.predicate == null) {
			predToString = "null pred";
		} else {
			predToString = Integer.toString(this.leftIdx) + this.predicate.toString() + Integer.toString(this.rightIdx);
		}

		String tabs = getTabs(PRINTING_INDENTATION_TABS);

		return "StackTreeAnc(" + leftChild.getName(1) + "," + rightChild.getName(1) + "," + "\n" + tabs + predToString
				+ "\n" + ")";
	}

	@Override
	public String getName(int depth) {
		String predToString;
		if (this.predicate == null) {
			predToString = "null pred";
		} else {
			predToString = Integer.toString(this.leftIdx) + this.predicate.toString() + Integer.toString(this.rightIdx);
		}

		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);

		return "\n" + spaceForIndent + "StackTreeAnc(" + leftChild.getName(1 + depth) + ","
				+ rightChild.getName(1 + depth) + "," + "\n" + tabs + predToString + "\n" + spaceForIndent + ")";
	}
}