package fr.inria.cedar.commons.tatooine.constants;

import com.google.common.base.Strings;

/**
 * Enumerated type representing the possible types of metadata in a tuple.
 * 
 * @author Jesus CAMACHO RODRIGUEZ
 */
public enum TupleMetadataType {
	ORDERED_ID, UNIQUE_ID, STRUCTURAL_ID, UPDATE_ID, NULL_ID, TUPLE_TYPE, STRING_TYPE, URI_TYPE, INTEGER_TYPE, NULL;
	
	/**
	 * Returns the corresponding member of the enumerated type for a given string.
	 * @param stringType
	 * @return
	 */
	public static TupleMetadataType getTypeEnum(String stringType) {
		if (Strings.isNullOrEmpty(stringType)) {
			return null;
		}
		try {
			return TupleMetadataType.valueOf(stringType.toUpperCase());
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
}
