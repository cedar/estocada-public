package fr.inria.cedar.commons.tatooine.operators.physical;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Physical operator that accesses data from a SOLR collection and returns results that may not preserve the metadata associated with such collection.
 * In this case the resulting metadata is determined by each query used as input.
 * @author Oscar Mendoza
 */
public class PhySOLREval extends SolrJSONQueryParameterisedOperator implements PhyEvalOperator {

	private static final Logger log = Logger.getLogger(PhySOLREval.class);

	/** Universal version identifier for the PhySOLREval class */
	private static final long serialVersionUID = -8912360206565859874L;

	/* Constructor */
	public PhySOLREval(String query, String fields, GSR ref, NRSMD nrsmd) throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		super(query, fields, ref);		
		if (fields == null || fields.isEmpty()) {
			String msg = "Eval operators are meant for queries that do NOT preserve the metadata of the data source they're querying; for queries that DO preserve it, use Scan operators";
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		NRSMD resultNRSMD = inferMetadata(nrsmd, fields);
		this.nrsmd = resultNRSMD;
	}
	
	/* Constructor */
	public PhySOLREval(String query, String fields, GSR ref, ViewSchema schema) throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		this(query, fields, ref, schema.getNRSMD());
	}

	@Override
	public String getName() {
		return String.format("PhySOLREval(%s, %s)", queryStr, fieldStr);
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return String.format("\n%sPhySOLREval(%s, %s)", spaceForIndent, queryStr, fieldStr);
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		try {
			return new PhySOLREval(queryStr, fieldStr, ref, nrsmd);
		} catch (Exception e) {
			log.error("Exception copying PhySOLREval: ", e);
			return null;
		}
	}

	@Override
	public NRSMD inferMetadata(NRSMD inputMD, String fieldsParam) throws TatooineExecutionException {
		List<String> fields = Arrays.asList(fieldsParam.split(","));		
		int counterCol = 0;
		int[] keepColumns = new int[fields.size()];
		
		for (int i = 0; i < inputMD.colNames.length; i++) {
			String colName = inputMD.colNames[i];
			if (fields.contains(colName)) {
				keepColumns[counterCol] = i;
				counterCol++;
			}					
		}		
		NRSMD resultMD = NRSMD.makeProjectRSMD(inputMD, keepColumns);
		return resultMD;
	}
	
}
