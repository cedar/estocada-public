package fr.inria.cedar.commons.tatooine.constants;

import com.google.common.base.Strings;

/**
 * Enumerated type representing a set of keywords used to associate each column of an NTuple to the formatting it should have if it is to be displayed in HTML.
 * @author Oscar Mendoza
 */
public enum LayoutTemplate {
	 	
	PLAIN, BOLD, ITALIC, URI, IMG, GRAPH;	
	
	/** Returns the corresponding member of the enumerated type for a given string */
	public static LayoutTemplate getTypeEnum(String type) {
		if (Strings.isNullOrEmpty(type)) {
			return null;
		}
		try {
			return LayoutTemplate.valueOf(type.toUpperCase());
		} catch (IllegalArgumentException e) {
			return null;
		}
	}
}
