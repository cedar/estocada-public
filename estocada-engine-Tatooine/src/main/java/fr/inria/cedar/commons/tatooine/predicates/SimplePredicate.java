package fr.inria.cedar.commons.tatooine.predicates;

import java.io.Serializable;
import java.util.Arrays;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.constants.PredicateDataType;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * This class is meant for very SimplePredicate selections: atomic field = constant, or atomic field = atomic field. 
 * The column indexes in the SimplePredicate are the ones of the Cartesian product of left and right. 
 * Thus, if left have 5 columns, the first column of right will be 5. Further, the column indexes go from 0 to n-1.
 *
 * @author Ioana MANOLESCU
 */
public class SimplePredicate extends Predicate implements Serializable {
	private static final Logger log = Logger.getLogger(SimplePredicate.class);
	
	/** Universal version identifier for the SimplePredicate class */
	private static final long serialVersionUID = -1282419749973424931L;

	public char[] theChars;
	public int number;

	public boolean onString;
	public boolean onJoin;
	public boolean onInteger;

	/**
	 * the column to which the selection is applied
	 */
	public int column;
	public int otherColumn;
	public int[] ancPath;
	String ancPathString;

	public PredicateType predCode = PredicateType.PREDICATE_EQUAL;

	public SimplePredicate(int k, int value, PredicateType predCode, PredicateDataType predType) {
		this.column = k;
		this.number = value;
		this.predCode = predCode;		
		switch (predType) {
			case PREDICATE_ON_JOIN:
				this.onJoin = true;
				this.onString = false;
				this.onInteger = false;
				break;
			case PREDICATE_ON_STRING:
				this.onJoin = false;
				this.onString = true;
				this.onInteger = false;				
				break;
			case PREDICATE_ON_INTEGER:
				this.onJoin = false;
				this.onString = false;
				this.onInteger = true;				
				break;
		}
	}
	
	public SimplePredicate(String s, int k, PredicateType predCode) {
		this(s, k);
		this.predCode = predCode;
	}

	public SimplePredicate(String s, int k) {
		this.column = k;
		this.theChars = s.toCharArray();
		this.onString = true;
		this.onJoin = false;
		this.onInteger = false;
		this.predCode = PredicateType.PREDICATE_EQUAL;
	}

	public SimplePredicate(char[] c, int k) {
		this.column = k;
		this.theChars = c;
		this.onString = true;
		this.onJoin = false;
		this.onInteger = false;
		this.predCode = PredicateType.PREDICATE_EQUAL;
	}
	
	/**
	 * Checks if the integer value of the Kth column of a given tuple and a given integer are equal.
	 * @param k: column number
	 * @param value
	 */
	public SimplePredicate(int k, Integer value) {
		this.column = k;
		this.onString = false;
		this.onJoin = false;
		this.onInteger = true;
		this.number = value;
		this.predCode = PredicateType.PREDICATE_EQUAL;
	}
	
	/**
	 * Compares the integer value of the Kth column of a given tuple to a given integer.
	 * Example: If the Kth column of our tuple has the value "12" and the "value" passed in is "10" 
	 * then isTrue would be true for predicate GREATER_THAN but false for predicate LESS_THAN.
	 * @param k: column number
	 * @param value 
	 * @param predCode
	 */
	public SimplePredicate(int k, Integer value, PredicateType predCode) {
		this.column = k;
		this.onString = false;
		this.onJoin = false;
		this.onInteger = true;
		this.number = value;
		this.predCode = predCode;
	}

	public SimplePredicate(char[] c, int k, PredicateType predCode) {
		this(c, k);
		this.predCode = predCode;
	}

	public SimplePredicate(int k1, int k2) {
		this.column = k1;
		this.ancPath = new int[1];
		this.ancPath[0] = k1;
		this.otherColumn = k2;
		this.onString = false;
		this.onJoin = true;
		this.onInteger = false;
		this.predCode = PredicateType.PREDICATE_EQUAL;
	}

	public SimplePredicate(int k1, int k2, PredicateType predCode) {
		this(k1, k2);
		this.predCode = predCode;
	}

	// this means: the field reachable by k1[0].k1[1]. ... k1[nk1] is compared with
	// the field reachable at k2
	// both have to be atomic
	public SimplePredicate(int[] k1, int k2) {
		this(k1, k2, PredicateType.PREDICATE_EQUAL);
	}

	public SimplePredicate(int[] k1, int k2, PredicateType predCode) {
		this.ancPath = k1;
		if (this.ancPath.length == 1) {
			this.column = k1[0];
		}
		this.otherColumn = k2;
		this.onString = false;
		this.onJoin = true;
		this.predCode = predCode;
		setAncPathString();
	}

	// this means: the field reachable by k1[0].k1[1]. ... k1[nk1] is compared with
	// the field reachable at k2
	// both have to be atomic
	public SimplePredicate(int[] k1, String constValue) {
		this(k1, constValue, PredicateType.PREDICATE_EQUAL);
	}

	public SimplePredicate(int[] k1, String constValue, PredicateType predCode) {
		this.ancPath = k1;
		this.column = -1;
		this.otherColumn = -1;
		this.theChars = constValue.toCharArray();
		this.onString = true;
		this.onJoin = false;
		this.predCode = predCode;
		setAncPathString();
	}

	/**
	 * @return the column of the predicate
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * @return the otherColumn of the predicate
	 */
	public int getOtherColumn() {
		return otherColumn;
	}

	/**
	 * @return the predCode of the predicate
	 */
	public PredicateType getPredCode() {
		return predCode;
	}

	/**
	 *
	 */
	private void setAncPathString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 0; i < ancPath.length; i++) {
			sb.append(ancPath[i]);
			if (i < ancPath.length - 1) {
				sb.append(".");
			}
		}
		sb.append("]");
		this.ancPathString = new String(sb);
	}

	/*
	 * (non-Javadoc) !!! Caution: this never works if ancPath is used (and this.column is not filled in). Such
	 * predicates should never be actually tested.
	 */
	@Override
	public boolean isTrue(NTuple t) throws TatooineExecutionException {
		assert (this.column >= 0) : "Cannot test the predicate ! Use GeneralizedSelect instead.";
		if (this.onString) {
            char[] columnValue = new char[0];
            switch (t.nrsmd.types[this.column]) {
            case STRING_TYPE:
              columnValue = t.getStringField(this.column);
              break;
            case URI_TYPE:
                columnValue = t.getUriField(this.column);
                break;
            default:
                log.error("Type "+ t.nrsmd.types[this.column]+ "not identifiable as a String");
            }
            
			switch (this.predCode) {
			case PREDICATE_EQUAL:
				return equal(columnValue, this.theChars);
			case PREDICATE_NOT_EQUAL:
				return (!equal(columnValue, this.theChars));
			default:
				log.error("Predicate " + this.predCode.toString() + " not applicable to type String");
			}
		} else if (this.onInteger) {
			switch (this.predCode) {
			case PREDICATE_EQUAL:
				return t.getIntegerField(this.column) == this.number;
			case PREDICATE_NOT_EQUAL:
				return t.getIntegerField(this.column) != this.number;
			case PREDICATE_GREATER_THAN:
				return t.getIntegerField(this.column) > this.number;
			case PREDICATE_GREATER_THAN_OR_EQUAL:
				return t.getIntegerField(this.column) >= this.number;
			case PREDICATE_LESS_THAN:
				return t.getIntegerField(this.column) < this.number;
			case PREDICATE_LESS_THAN_OR_EQUAL:
				return t.getIntegerField(this.column) <= this.number;
				default:
					log.error("Predicate " + this.predCode.toString() + " not applicable to type Integer");
			}
		} else if (this.onJoin) {

			switch (t.nrsmd.types[this.column]) {
			case STRING_TYPE:
                char[] string1 = t.getStringField(this.column);
                char[] string2 = t.getStringField(this.otherColumn);

                switch (this.predCode) {
				case PREDICATE_EQUAL:
					return equal(string1, string2);
				case PREDICATE_NOT_EQUAL:
					return !equal(string1, string2);
				default:
					log.error("Predicate " + this.predCode.toString() + " not handled in String join");
				}

                break;
            case URI_TYPE:
                string1 = t.getUriField(this.column);
                string2 = t.getUriField(this.otherColumn);

				switch (this.predCode) {
				case PREDICATE_EQUAL:
					return equal(string1, string2);
				case PREDICATE_NOT_EQUAL:
					return !equal(string1, string2);
				default:
					log.error("Predicate " + this.predCode.toString() + " not handled in String join");
				}
                break;
            case INTEGER_TYPE:
                int int1 = t.getIntegerField(this.column);
                int int2 = t.getIntegerField(this.otherColumn);
                switch (this.predCode) {
				case PREDICATE_EQUAL:
					return int1 == int2;
				case PREDICATE_NOT_EQUAL:
					return int1 != int2;
				default:
					log.error("Predicate " + this.predCode.toString() + " not handled in String join");
				}
                break;
			case UPDATE_ID:
			case UNIQUE_ID:
			case ORDERED_ID:
			case STRUCTURAL_ID:
				ElementID id1 = t.getIDField(this.column);
				ElementID id2 = t.getIDField(this.otherColumn);
				switch (this.predCode) {
				case PREDICATE_EQUAL:
					return id1.equals(id2);
				case PREDICATE_NOT_EQUAL:
					return !id1.equals(id2);
				case PREDICATE_ANCESTOR:
					return id1.isAncestorOf(id2);
				case PREDICATE_DESCENDANT:
					return id2.isAncestorOf(id1);
				case PREDICATE_PARENT:
					return id1.isParentOf(id2);
				case PREDICATE_CHILD:
					return id2.isParentOf(id1);
				case PREDICATE_BEFORE:
					return id2.startsAfter(id1);
				default:
					log.error("Predicate " + this.predCode.toString() + " not supported in ID join");
				}
			default:
				log.error("Type unhandled by SimplePredicates " + t.nrsmd.types[this.column].toString());
			}
		}
		return false;
	}
	
	public static boolean equal(char[] aux1, char[] aux2) {
		if (aux1.length != aux2.length) {
			return false;
		}
		for (int j = 0; j < aux1.length; j++) {
			if (aux1[j] != aux2[j]) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		if (this.onJoin) {
			return (((this.column != -1) ? (this.column + "") : this.ancPathString) + this.predCode.toString() + this.otherColumn);
		} else {
			if (this.onString) {
				return (((this.column != -1) ? (this.column + "") : this.ancPathString) + this.predCode.toString()
						+ "\"" + new String(this.theChars) + "\"");
			}
		}
		return "";
	}

	@Override
	public boolean equals(Object spred) {
		SimplePredicate pred = (SimplePredicate) spred;

		if (Arrays.equals(theChars, pred.theChars)
				&& onString == pred.onString
				&& onJoin == pred.onJoin
				&& ((ancPath == null && pred.ancPath == null) || Arrays.equals(ancPath, pred.ancPath))
				&& ((ancPathString == null && pred.ancPathString == null) || (ancPathString != null && ancPathString
						.equals(pred.ancPathString))) && predEquals(pred)) {
			return true;
		}

		return false;
	}

	private boolean predEquals(SimplePredicate pred) {
		return (column == pred.column && otherColumn == pred.otherColumn && predCode.equals(pred.predCode))

		||

		(column == pred.otherColumn && otherColumn == pred.column && predCode.equals(pred.predCode.revert()));
	}

	@Override
	public Object clone() {
		// TODO: add support for ancPath?
		if (onJoin) {
			return new SimplePredicate(column, otherColumn, this.predCode);
		} else if (onString) {
			return new SimplePredicate(theChars, column);
		} else {
			return null;
		}
	}

	/**
	 * @author Karan AGGARWAL
	 * @return String representation of Predicate separated by , Follows the Plan file (.phyp) grammar
	 */
	@Override
	public String getName() {

		if (this.onJoin) {
			if (this.column != -1) {
				return this.column + this.predCode.toString() + this.otherColumn;
			} else {
				return this.ancPathString + this.predCode.toString() + this.otherColumn;
			}

		} else {

			if (this.onString) {
				if (this.column != -1) {
					return new String(this.theChars) + this.column + this.predCode.toString() + this.column;
				} else {
					return new String(this.theChars) + this.ancPathString + this.predCode.toString() + this.column;
				}
			}
		}

		return "";

	}

}
