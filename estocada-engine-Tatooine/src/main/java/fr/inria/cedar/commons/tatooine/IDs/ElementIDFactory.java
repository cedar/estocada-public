package fr.inria.cedar.commons.tatooine.IDs;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.ElementIDScheme;

/**
 * Factory class that creates the appropriate kind of ID depending on the properties defined for Tatooine.
 * @author Ioana MANOLESCU
 */
public class ElementIDFactory {
	private static final Logger log = Logger.getLogger(ElementIDFactory.class);
	private final static ElementIDScheme USE_ELEMENT_ID = 
			Boolean.parseBoolean(Parameters.getProperty("childAxis")) ? 
					ElementIDScheme.PREPOSTDEPTH : ElementIDScheme.PREPOST;

	/** Creates the appropriate kind of ID depending on the properties of Tatooine */
	public static ElementID getElementID(int pre, int depth) {
		switch (USE_ELEMENT_ID) {
		case PREPOSTDEPTH:
			return new PrePostDepthElementID(pre, depth);
		case PREPOST:
			return new PrePostElementID(pre);
		default:
			log.error("Not sure what kind of IDs I should use!");
			return null;
		}
	}
}
