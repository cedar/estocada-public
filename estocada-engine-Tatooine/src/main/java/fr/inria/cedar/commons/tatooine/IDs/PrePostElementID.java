package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Pre-post element ID.
 *
 * @author Alin TILEA
 */
public class PrePostElementID implements ElementID, Serializable {
	private static final Logger log = Logger.getLogger(PrePostElementID.class);
	
	/** Universal version identifier for the PrePostElementID class */
	private static final long serialVersionUID = -8248867143045592758L;

	/* Constants */
	private static final String DELIMITER = Parameters.getProperty("delimiter");

	public int pre;
	public int post;

	public static PrePostElementID theNull = new PrePostElementID(-1);

	public PrePostElementID(int pre) {
		this.pre = pre;
	}
	
	@Override
	public int getType() {
		// TODO
		return 0;
	}
	
	@Override
	public String toString() {
		if (this == theNull) {
			return TupleMetadataType.NULL.toString();
		}
		return new String(pre + DELIMITER + post);
	}

	public final void setPost(int post) {
		this.post = post;
	}

	/** Returns true if ID1 is a parent of ID2 */
	@Override
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException {
		return false;
	}

	/** Returns true if ID1 is an ancestor of ID2 */
	@Override
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException {
		try {
			PrePostElementID other = (PrePostElementID) id2;
			if (other.pre > this.pre) {
				if (other.post < this.post) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Exception: ", e);
			throw new TatooineExecutionException(e.toString());
		}
	}

	/** Returns the parent ID if it can be computed */
	@Override
	public ElementID getParent() throws TatooineExecutionException {
		throw new TatooineExecutionException("PrePostElementID cannot infer parent");
	}

	/** Returns the null element for this kind of ID */
	@Override
	public ElementID getNull() {
		return theNull;
	}

	/** Returns true if this element ID is null */
	@Override
	public boolean isNull() {
		return (this == getNull());
	}

	/** Returns true if ID1 starts strictly after ID2 */
	@Override
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException {
		PrePostElementID other = null;
		try {
			other = (PrePostElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply startAfter on different ID schemes");
		}
		return (this.pre > other.pre);
	}

	/** Returns true if ID1 ends strictly after ID2 */
	@Override
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException {
		PrePostElementID other = null;
		try {
			other = (PrePostElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply startAfter on different ID schemes");
		}
		return (this.post > other.post);
	}
	
	@Override
	public boolean equals(Object o) {
		try {
			PrePostElementID id2 = (PrePostElementID) o;
			return (this.pre == id2.pre && this.post == id2.post);
		} catch (ClassCastException cce) {
			log.warn("ClassCastException: ", cce);
			return false;
		}
	}

	@Override
	public int hashCode() {
		return ((new Integer(this.pre)).hashCode());
	}

}
