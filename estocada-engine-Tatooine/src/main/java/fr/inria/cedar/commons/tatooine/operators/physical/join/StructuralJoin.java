package fr.inria.cedar.commons.tatooine.operators.physical.join;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.NTupleStack;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.util.PhysicalOperatorUtility;

/**
 * Common ancestor for all stack-based structural join variants.
 * 
 * @author Ioana MANOLESCU
 * @created 15/08/2005
 */
public abstract class StructuralJoin extends NIterator {
	private static final Logger log = Logger.getLogger(StructuralJoin.class);
	
	/** Universal version identifier for the StructuralJoin class */
	private static final long serialVersionUID = 9083542637608605660L;

	public NIterator leftChild;
	public NIterator rightChild;

	protected int leftIdx;
	protected int rightIdx;
	// for multi-doc joins: the index of the columns holding the document IDs
	// and the current docID
	protected int leftDocIdx;
	protected int rightDocIdx;

	protected NIterator ancestorChild;
	protected NIterator descendantChild;

	protected int ancestorColumn;
	protected int descendantColumn;
	protected int ancestorDocColumn;
	protected int descendantDocColumn;

	protected PredicateType predicate;

	protected char[] currentDocID;

	protected boolean noMoreAncestors;
	protected boolean noMoreDescendants;
	protected boolean noMoreChildrenTuples;

	protected NTuple currentTupleAncestor;
	protected NTuple currentTupleDescendant;

	protected NTupleStack joinStack;

	public StructuralJoin(NIterator left, NIterator right, int leftIdx, int rightIdx, PredicateType idPredCode)
			throws TatooineExecutionException {
		// child == true means we are only interested in direct children
		super(left, right);
		this.leftChild = left;
		this.rightChild = right;
		this.leftIdx = leftIdx;
		this.rightIdx = rightIdx;
		this.ancestorDocColumn = -1;
		this.descendantDocColumn = -1;

		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.ancestorChild = left;
			this.descendantChild = right;
			this.ancestorColumn = leftIdx;
			this.descendantColumn = rightIdx;
			this.ancestorDocColumn = -1;
			this.descendantDocColumn = -1;
		} else {
			this.ancestorChild = right;
			this.descendantChild = left;
			this.ancestorColumn = rightIdx;
			this.descendantColumn = leftIdx;
			this.ancestorDocColumn = -1;
			this.descendantDocColumn = -1;
		}

		this.noMoreChildrenTuples = false;

		this.predicate = idPredCode;

		checkTypes();
		checkOrder();
	}

	public StructuralJoin(NIterator left, NIterator right, int leftIdx, int rightIdx, int leftDocIdx, int rightDocIdx,
			PredicateType idPredCode) throws TatooineExecutionException {
		// child == true means we are only interested in direct children
		super(left, right);
		this.leftChild = left;
		this.rightChild = right;
		this.leftIdx = leftIdx;
		this.rightIdx = rightIdx;
		this.leftDocIdx = leftDocIdx;
		this.rightDocIdx = rightDocIdx;

		if (idPredCode == PredicateType.PREDICATE_ANCESTOR || idPredCode == PredicateType.PREDICATE_PARENT) {
			this.ancestorChild = left;
			this.descendantChild = right;
			this.ancestorColumn = leftIdx;
			this.descendantColumn = rightIdx;
			this.ancestorDocColumn = leftDocIdx;
			this.descendantDocColumn = rightDocIdx;
		} else {
			this.ancestorChild = right;
			this.descendantChild = left;
			this.ancestorColumn = rightIdx;
			this.descendantColumn = leftIdx;
			this.ancestorDocColumn = rightDocIdx;
			this.descendantDocColumn = leftDocIdx;
		}

		this.noMoreChildrenTuples = false;

		this.predicate = idPredCode;

		checkTypes();
		checkOrder();
	}

	private void checkOrder() {
		if (ancestorChild.getOrderMaker().order.length == 0 || descendantChild.getOrderMaker().order.length == 0) {
			log.error("Unordered input");
		}
		if (this.ancestorDocColumn == -1) {
			if (!ancestorChild.getOrderMaker().dominantOrder(ancestorColumn)) {
				log.error("Ancestor is not ordered by ancestor ID");
			}
			if (!descendantChild.getOrderMaker().dominantOrder(descendantColumn)) {
				log.error("Descendant is not ordered by descendant ID");
			}
		}
	}

	private void checkTypes() {
		try {
			log.debug("anc.nrsmd.types[" + ancestorColumn + "] " + ancestorChild.nrsmd.types[ancestorColumn]);

			switch (ancestorChild.nrsmd.types[ancestorColumn]) {
			case STRUCTURAL_ID:
				if (descendantChild.nrsmd.types[descendantColumn] != TupleMetadataType.STRUCTURAL_ID) {
					log.error("Uncomparable ID types " + ancestorChild.nrsmd.types[ancestorColumn].toString()
							+ " in the ancestor at " + ancestorColumn + " and "
							+ descendantChild.nrsmd.types[descendantColumn].toString() + " in the descendant at "
							+ descendantColumn);
				}
				break;
			case UPDATE_ID:
				if (descendantChild.nrsmd.types[descendantColumn] != TupleMetadataType.UPDATE_ID) {
					log.error("Uncomparable ID types " + ancestorChild.nrsmd.types[ancestorColumn].toString()
							+ " in the ancestor at " + ancestorColumn + " and "
							+ descendantChild.nrsmd.types[descendantColumn].toString() + " in the descendant at "
							+ descendantColumn);
				}
				break;
			default:
				log.error("Cannot do structural joins on such types");
			}
		} catch (Exception e) {
			log.error("error: " + e);

		}
	}

	protected void alignDoc() throws TatooineExecutionException {
		// logger.debug("StructJoin aligning: tLeft is null: " + (tLeft == null) + " tRight is null: " + (tRight ==
		// null));
		// logger.debug("OverLeft: " + overLeft + " overRight: " + overRight + " all over: " + allOver);

		noMoreChildrenTuples = noMoreAncestors || noMoreDescendants;
		char[] ancestorDocID = null;
		if (currentTupleAncestor != null) {
			ancestorDocID = currentTupleAncestor.getUriField(ancestorDocColumn);
		} else {
			return;
		}
		char[] descendantDocID = null;
		if (currentTupleDescendant != null) {
			descendantDocID = currentTupleDescendant.getUriField(descendantDocColumn);
		} else {
			return;
		}

		// logger.debug("---> alignDoc docIDAnc :" + new String(docIDAnc) + ", docIDDesc:" + new String(docIDDesc));

		int doc1VSdoc2 = 0;
		boolean alignedOrDone = false;

		while (!alignedOrDone) {
			doc1VSdoc2 = PhysicalOperatorUtility.compareCharArrays(ancestorDocID, descendantDocID);
			while (doc1VSdoc2 < 0) {
				if (ancestorChild.hasNext()) {
					currentTupleAncestor = ancestorChild.next();
					ancestorDocID = currentTupleAncestor.getUriField(ancestorDocColumn);
					doc1VSdoc2 = PhysicalOperatorUtility.compareCharArrays(ancestorDocID, descendantDocID);
				} else {
					noMoreAncestors = true;
					noMoreChildrenTuples = true;
					break;
				}
			}
			if (noMoreAncestors || doc1VSdoc2 == 0) {
				alignedOrDone = true;
				break;
			}
			while (doc1VSdoc2 > 0) {
				if (descendantChild.hasNext()) {
					currentTupleDescendant = descendantChild.next();
					descendantDocID = currentTupleDescendant.getUriField(descendantDocColumn);
					doc1VSdoc2 = PhysicalOperatorUtility.compareCharArrays(ancestorDocID, descendantDocID);
				} else {
					noMoreDescendants = true;
					noMoreChildrenTuples = true;
					break;
				}
			}
			if (noMoreDescendants || doc1VSdoc2 == 0) {
				alignedOrDone = true;
				break;
			}
		}
		if (doc1VSdoc2 == 0) {
			currentDocID = ancestorDocID;
			// logger.debug("---> alignDoc exit with currentDocID:" + new String(currentDocID));
		}
	}
	
	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 * @author Aditya SOMANI
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"" + "(" + this.getOperatorID().physOpNo + ")" + "STD(");
		switch (predicate) {
		case PREDICATE_PARENT:
			sb.append("$" + leftIdx + " par $" + rightIdx);
			break;
		case PREDICATE_ANCESTOR:
			sb.append("$" + leftIdx + " anc $" + rightIdx);
			break;
		case PREDICATE_CHILD:
			sb.append("$" + leftIdx + " child $" + rightIdx);
			break;
		case PREDICATE_DESCENDANT:
			sb.append("$" + leftIdx + " desc $" + rightIdx);
			break;
		}
		sb.append(")" + "\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		int childNumber1 = leftChild.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
		int childNumber2 = rightChild.recursiveDotString(sb, firstAvailableNo, childNumber1);

		return childNumber2;
	}
}
