package fr.inria.cedar.commons.tatooine.operators.physical.util;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Omit repeated lines like the uniq command line
 * 
 * @author Maxime Buron
 */
public class PhyUniq extends NIterator {

	private static final long serialVersionUID = 1L;
	private NIterator child;
	private NTuple currentTuple;


	public PhyUniq(NIterator child) {
		super(child);
		this.child = child;
		this.nrsmd = child.nrsmd;
	}

	@Override
	public void open() throws TatooineExecutionException {
        this.child.open();
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
        if (this.child.hasNext()) {
            NTuple candidate = this.child.next();

            if(this.currentTuple == null) {
                this.currentTuple = candidate;
                return true;
            } else {
                while(candidate.equals(this.currentTuple) && this.child.hasNext()) {
                    candidate = this.child.next();
                }
                
                if(candidate.equals(this.currentTuple)) {
                    return false;
                } else {
                    this.currentTuple = candidate;
                    return true;
                }
            }
        } else {
            return false;
        }
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		return this.currentTuple;
	}

	@Override
	public void close() throws TatooineExecutionException {
        this.child.close();
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return null;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public String getName(int depth) {
		return null;
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyUniq(child.copy());
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"Uniq\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return child.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
	}

    public NIterator getChild() {
		return child;
	}

	public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
