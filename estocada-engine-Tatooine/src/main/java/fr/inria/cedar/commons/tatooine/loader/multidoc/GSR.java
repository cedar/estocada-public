package fr.inria.cedar.commons.tatooine.loader.multidoc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.constants.ViewType;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * This is a Storage Reference that uniquely identifies a JoinedPattern or a Pattern in the system.
 * The GSR keeps also information about the peer where the (Joined)Pattern exists so that we can
 * distinguish between the same (Joined)Patterns that come from different peers.<br><br>
 *
 * All objects of this class have (at least) the following <b>string</b> parameters:
 * <li>ENVIRONMENT_PARA -> the directory path to the database.
 * <li>XAMString -> when the GSR refers to a JoinedPattern it keeps the string representation of it;
 *    when it refers to a Pattern, it keeps the string representation of the expanded version of it.
 *    Thus, in all cases it keeps the (Joined)Pattern that is <b>actually stored in the database</b>.
 *
 * @author Spyros ZOUPANOS
 * @author Konstantinos KARANASOS
 *
 */
public class GSR implements StorageReference, Comparable<GSR>, Serializable {
	private static final Logger log = Logger.getLogger(GSR.class);
	
	/** Universal version identifier for the GSR class */
	private static final long serialVersionUID = 1932411506140389750L;

	ArrayList<String> propertyNames;
	ArrayList<String> propertyValues;
	HashMap<String, String> properties;

	private String viewName;
	private ViewType viewType;

	/**
	 * The official PatternGSR constructor.
	 * @param givenPeerId The peerId of the peer that stores the view where this PatternGSR points to.
	 * @param givenViewFName The filename of the view where this PatternGSR points to.
	 */
	public GSR(String givenViewFName) {
		this.propertyNames = new ArrayList<String>();
		this.propertyValues = new ArrayList<String>();
		this.properties = new HashMap<String, String>();
		this.setViewName(givenViewFName);

		try {
			this.setProperty("ENVIRONMENT_PARA", givenViewFName);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
	}

	@Override
	public int getNumberOfProperties() {
		return properties.size();
	}

	@Override
	public String getPropertyValue(String propertyName) {
		return properties.get(propertyName);
	}

	@Override
	public void setProperty(String propertyName, String propertyValue) {
		properties.put(propertyName, propertyValue);
		propertyNames.add(propertyName);
		propertyValues.add(propertyValue);
	}

	@Override
	public String getPropertyName(int i) {
		return propertyNames.get(i);
	}

	@Override
	public String getPropertyValue(int i) {
		return propertyValues.get(i);
	}

	public String getViewName() {
		return this.viewName;
	}

	@Override
	public int compareTo(GSR o) {
		return this.getViewName().compareTo(o.getViewName());
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof GSR)) {
			return false;
		}

		GSR gsrObj = (GSR) obj;

		if (this.getViewName().compareTo(gsrObj.getViewName()) == 0) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return this.getViewName().hashCode();
	}

	@Override
	public String toString() {
		return this.getPropertyValue("ENVIRONMENT_PARA");
	}

	public String getViewURI() {
		return this.getPropertyValue("ENVIRONMENT_PARA");
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public void setViewType(ViewType viewType) {
		this.viewType = viewType;
	}

	public ViewType getViewType() {
		return viewType;
	}
}
