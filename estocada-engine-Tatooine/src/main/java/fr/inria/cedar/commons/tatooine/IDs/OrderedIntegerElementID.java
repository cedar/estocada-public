package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Ordered integer element ID.
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public class OrderedIntegerElementID implements ElementID, Serializable {
	private static final Logger log = Logger.getLogger(OrderedIntegerElementID.class);
	
	/** Universal version identifier for the OrderedIntegerElementID class */
	private static final long serialVersionUID = 5294160855145350727L;

	public static OrderedIntegerElementID theNull = new OrderedIntegerElementID(-1);

	/** The actual ID */
	public int n;

	public OrderedIntegerElementID(int n){
		this.n = n;
	}

	/**
	 * TODO: Refine this into better type description (3 integers...).
	 * @return The type code, in a type system to be defined...
	 */
	@Override
	public int getType() {
		return 0;
	}

	@Override
	public String toString(){
		if (this == theNull){
			return TupleMetadataType.NULL.toString();
		}
		return ("" + n);
	}

	/** Returns true if ID1 is a parent of ID2 */
	@Override
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("IntegerIDs cannot answer isParentOf");
	}

	/** Returns true if ID1 is an ancestor of ID2 */
	@Override
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("IntegerIDs cannot answer isAncestorOf");
	}

	/** Returns the parent ID if it can be computed */
	@Override
	public ElementID getParent() throws TatooineExecutionException {
		throw new TatooineExecutionException("IntegerIDs cannot answer getParent");
	}

	/** Returns the null element for this kind of ID */
	@Override
	public ElementID getNull() {
		return theNull;
	}

	/** Returns true if this element ID is null */
	@Override
	public boolean isNull(){
		return (this == getNull());
	}

	@Override
	public boolean equals(Object o){
		try{
			OrderedIntegerElementID iid = (OrderedIntegerElementID)o;
			return (this.n == iid.n);
		}
		catch(ClassCastException cce){
			log.warn("ClassCastException: ", cce);
			return false;
		}
	}

	@Override
	public int hashCode(){
		return (new Integer(n)).hashCode();
	}

	/** Returns true if ID1 starts strictly after ID2 */
	@Override
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException {
		OrderedIntegerElementID other = (OrderedIntegerElementID)id2;
		return (this.n > other.n);
	}

	/** Returns true if ID1 ends strictly after ID2 */
	@Override
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("EndsAfter undefined for integer IDs");
	}

}