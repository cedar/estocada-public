package fr.inria.cedar.commons.tatooine.operators.logical;

/**
 * A visitor interface for logical operators.
 * 
 * @author Romain Primet
 */
public interface LogOperatorVisitor {
    public default void visit(LogJoin op) {
        throwNotImplemented();
    }

    public default void visit(LogKQLEval op) {
        throwNotImplemented();
    }

    public default void visit(LogOperator op) {
        op.accept(this);
    }

    public default void visit(LogProjection op) {
        throwNotImplemented();
    }

    public default void visit(LogSelection op) {
        throwNotImplemented();
    }

    public default void visit(LogSQLEval op) {
        throwNotImplemented();
    }

    public default void visit(LogPJSQLEval op) {
        throwNotImplemented();
    }

    public default void visit(LogSolrEval op) {
        throwNotImplemented();
    }

    public default void visit(LogAQLEval op) {
        throwNotImplemented();
    }

    public default void visit(LogTupleReaderScan op) {
        throwNotImplemented();
    }

    static void throwNotImplemented() {
        throw new IllegalStateException("Not implemented.");
    }
}
