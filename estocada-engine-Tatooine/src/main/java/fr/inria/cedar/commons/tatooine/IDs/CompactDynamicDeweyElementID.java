package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Class that represents a compact dynamic Dewey ID.
 *
 * @author Martin GOODFELLOW
 */
public class CompactDynamicDeweyElementID implements ElementID, Serializable {
	private static final Logger log = Logger.getLogger(CompactDynamicDeweyElementID.class);
	
	/** Universal version identifier for the CompactDynamicDeweyElementID class */
	private static final long serialVersionUID = 3953851492404427753L;

	public static CompactDynamicDeweyElementID theNull = new CompactDynamicDeweyElementID(new int[0], 0, new int[0], "");

	/** The current id */
	int[] path;

	/** The integer describing the last sibling position */
	int n;

	/** The tag at this level */
	int[] tag;

	/* Result path for this node (ivma algorithm) */
	private LinkedList<CompactDynamicDeweyElementID> resultPath = new LinkedList<CompactDynamicDeweyElementID>();

	public CompactDynamicDeweyElementID(int[] path, int[] tag) {
		this.path = path;
		this.n = path[path.length - 1];
		this.tag = tag;
	}

	public CompactDynamicDeweyElementID(int[] path, int n, int[] tag, String xmlTag) {
		log.debug("COMPACT DYNAMIC DEWEY ID CREATED!");
		if (path.length == 0) {
			this.path = new int[] { n };
		} else {
			int[] tempPath = new int[path.length + 1];
			System.arraycopy(path, 0, tempPath, 0, path.length);
			tempPath[path.length] = n;
			this.path = tempPath;
		}
		if (!xmlTag.equals("")) {
			if (tag.length == 0) {
				this.tag = new int[] { CompactDynamicDeweyIDScheme.tagDictionary.get(xmlTag) };
			} else {
				this.tag = new int[tag.length + 1];
				System.arraycopy(tag, 0, this.tag, 0, tag.length);
				this.tag[tag.length] = CompactDynamicDeweyIDScheme.tagDictionary.get(xmlTag);
			}
		}
		this.n = n;
	}

	public CompactDynamicDeweyElementID(int[] path, int[] tag, String xmlTag) {
		log.debug("COMPACT DYNAMIC DEWEY ID CREATED!");
		this.path = path;
		this.n = path[path.length - 1];
		this.tag = tag;
		if (!xmlTag.equals("")) {
			if (tag.length == 0) {
				this.tag = new int[] { CompactDynamicDeweyIDScheme.tagDictionary.get(xmlTag) };
			} else {
				this.tag = new int[tag.length + 1];
				System.arraycopy(tag, 0, this.tag, 0, tag.length);
				this.tag[tag.length] = CompactDynamicDeweyIDScheme.tagDictionary.get(xmlTag);
			}
		}
	}

	public CompactDynamicDeweyElementID(ArrayList<Integer> v, ArrayList<Integer> t) {
		int[] tempPath = new int[v.size()];
		for (int i = 0; i < v.size(); i++) {
			Integer n = v.get(i);
			tempPath[i] = n;
		}
		this.path = tempPath;
		if (v.size() == 0) {
			this.n = 1;
		} else {
			this.n = (v.get(v.size() - 1)).intValue();
		}
		int[] tempTag = new int[t.size()];
		for (int i = 0; i < t.size(); i++) {
			Integer n = t.get(i);
			tempTag[i] = n;
		}
		this.tag = tempTag;
	}

	/**
	 * TODO: Refine this into better type description (3 integers...).
	 * @return The type code, in a type system to be defined...
	 */
	@Override
	public int getType() {
		// FIXME: Is this right?
		return 0;
	}

	@Override
	public String toString() {
		if (this == theNull) {
			return "null";
		}
		String path = "";
		for (int i = 0; i < this.path.length - 1; i++) {
			path = path + this.path[i] + ".";
		}
		path = path + this.path[this.path.length - 1];
		return path + " " + this.translateTag();
	}

	/** Returns true if ID1 is a parent of ID2 */
	@Override
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException {
		CompactDynamicDeweyElementID ddeid2 = null;
		try {
			ddeid2 = (CompactDynamicDeweyElementID) id2;
		} catch (Exception e) {
			throw new TatooineExecutionException("Wrong ID class");
		}
		if (this.path.length != (ddeid2.path.length - 1)) {
			return false;
		}
		for (int i = (this.path.length - 1); i >= 0; i--) {
			if (this.path[i] != ddeid2.path[i]) {
				return false;
			}
		}
		return true;
	}

	/** Returns true if ID1 is an ancestor of ID2 */
	@Override
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException {
		CompactDynamicDeweyElementID ddeid2 = null;
		try {
			ddeid2 = (CompactDynamicDeweyElementID) id2;
		} catch (Exception e) {
			throw new TatooineExecutionException("Wrong ID class");
		}

		if (this.path.length >= ddeid2.path.length) {
			return false;
		}

		for (int i = 0; i < this.path.length; i++) {
			if (this.path[i] != ddeid2.path[i]) {
				return false;
			}
		}
		return true;
	}

	/** Returns the parent ID if it can be computed */
	@Override
	public ElementID getParent() throws TatooineExecutionException {
		if (this.path.length == 1) {
			return null;
		}
		int[] parent = new int[this.path.length - 1];
		for (int i = 0; i < this.path.length - 1; i++) {
			parent[i] = this.path[i];
		}
		int[] parentTag = new int[this.tag.length - 1];
		for (int i = 0; i < this.tag.length - 1; i++) {
			parentTag[i] = this.tag[i];
		}
		return new CompactDynamicDeweyElementID(parent, parentTag);
	}

	/** Returns the null element for this kind of ID */
	@Override
	public ElementID getNull() {
		return theNull;
	}

	@Override
	public boolean equals(Object o) {
		try {
			CompactDynamicDeweyElementID id2 = (CompactDynamicDeweyElementID) o;
			if (this.path.length != id2.path.length) {
				return false;
			}
			for (int i = 0; i < this.path.length; i++) {
				if (this.path[i] != id2.path[i]) {
					return false;
				}
			}
			return true;
		} catch (ClassCastException cce) {
			log.warn("ClassCastException: ", cce);
			return false;
		}
	}

	@Override
	public int hashCode() {
		return this.path.hashCode();
	}

	/** Returns true if ID1 starts strictly after ID2 */
	@Override
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException {
		CompactDynamicDeweyElementID other = null;
		try {
			other = (CompactDynamicDeweyElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply startAfter on different ID schemes");
		}

		int[] currentPath = this.path;
		int[] otherPath = other.path;
		int[] tempArray;

		while (true) {
			if (currentPath.length == 1) {
				if (otherPath.length == 1) {
					if (currentPath[0] > otherPath[0]) {
						return true;
					} else {
						return false;
					}
				} else {
					// this.path is "a", other.path is "b.something"
					if (currentPath[0] > otherPath[0]) {
						return true;
					} else {
						return false;
					}
				}
			} else {
				// this.path is "a.something"
				if (otherPath.length == 1) {
					// other.path is "b."
					if (currentPath[0] > otherPath[0]) {
						return true;
					} else {
						if (currentPath[0] < otherPath[0]) {
							return false;
						} else {
							// They are equal, but this.path is longer
							return true;
						}
					}
				} else {
					// this.path is a.something1, other.path is b.something2
					if (currentPath[0] > otherPath[0]) {
						return true;
					} else {
						if (currentPath[0] < otherPath[0]) {
							return false;
						} else {
							// m1 == m2
							tempArray = new int[currentPath.length - 1];
							for (int i = 1; i < currentPath.length; i++) {
								tempArray[i - 1] = currentPath[i];
							}
							currentPath = tempArray;
							tempArray = new int[otherPath.length - 1];
							for (int i = 1; i < otherPath.length; i++) {
								tempArray[i - 1] = otherPath[i];
							}
							otherPath = tempArray;
						}
					}
				}
			}
		}
	}

	/** Returns true if ID1 ends strictly after ID2 */
	@Override
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException {
		CompactDynamicDeweyElementID other = null;
		try {
			other = (CompactDynamicDeweyElementID) id2;
		} catch (ClassCastException e) {
			throw new TatooineExecutionException("Cannot apply endsAfter on different ID schemes");
		}
		if (this.isAncestorOf(other)) {
			return true;
		}
		boolean b1 = this.startsAfter(other);
		boolean b2 = other.isAncestorOf(this);
		return (b1 && !b2);
	}

	/** Returns the null element for this kind of ID */
	@Override
	public boolean isNull() {
		return (this == getNull());
	}
	
	/** Gets the path to the current node as a string */
	public String getPathAsString() {
		String s = "";
		for (int element : this.path) {
			s = s + element;
		}
		return s;
	}
	
	/** Gets the tag path to the current node as a string */
	public String getTagPathAsString() {
		String s = "";
		for (int element : this.tag) {
			s = s + element;
		}
		return s;
	}
	
	/** 
	 * Translates the tag into its original representation using tag names instead of numbers
	 * which were introduced for compression purposes. 
	 */
	public String translateTag() {
		String s = "";
		for (int i = 0; i < tag.length; i++) {
			for (String key : CompactDynamicDeweyIDScheme.tagDictionary.keySet()) {
				if (CompactDynamicDeweyIDScheme.tagDictionary.get(key) == tag[i]) {
					if (i != tag.length - 1) {
						s = s + key + ".";
					} else {
						s = s + key;
					}
				}
			}
		}
		return s;
	}

	/** 
	 * Determines if the given tag exists in the path
	 * @return True if the path contains the tag, False otherwise 
	 */
	public boolean inPath(String tag) {
		if (tag != null) {
			for (int element : this.path) {
				if (element == 0) {
					return true;
				}
			}
		}
		return false;
	}

	public int[] getPath() {
		return this.path;
	}

	public void setPath(int[] path) {
		this.path = path;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int[] getTag() {
		return tag;
	}

	public void setTag(int[] tag) {
		this.tag = tag;
	}

	public static void main(String[] argv) throws TatooineExecutionException {
		int[] firstID = new int[argv[0].length()];
		for (int i = 0; i < argv[0].length(); i++) {
			firstID[i] = Integer.parseInt((Character.toString(argv[0].charAt(i))));
		}
		int[] secondID = new int[argv[1].length()];
		for (int i = 0; i < argv[1].length(); i++) {
			secondID[i] = Integer.parseInt((Character.toString(argv[1].charAt(i))));
		}

		CompactDynamicDeweyElementID id1 = new CompactDynamicDeweyElementID(firstID, new int[0]);
		CompactDynamicDeweyElementID id2 = new CompactDynamicDeweyElementID(secondID, new int[0]);

		log.info(id1.toString() + " starts after " + id2.toString() + ":" + id1.startsAfter(id2));
		log.info(id2.toString() + " starts after " + id1.toString() + ":" + id2.startsAfter(id1));
		log.info(id1.toString() + " ends after " + id2.toString() + ":" + id1.endsAfter(id2));
		log.info(id2.toString() + " ends after " + id1.toString() + ":" + id2.endsAfter(id1));
	}

	public void setResultPath(LinkedList<CompactDynamicDeweyElementID> resultPath) {
		this.resultPath = resultPath;
	}

	public LinkedList<CompactDynamicDeweyElementID> getResultPath() {
		return resultPath;
	}

}
