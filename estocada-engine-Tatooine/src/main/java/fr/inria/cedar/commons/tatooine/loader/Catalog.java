package fr.inria.cedar.commons.tatooine.loader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

public class Catalog {
	private static Catalog instance;
	private static final Charset CHARSET = Charsets.UTF_8;
	private static final Logger log = Logger.getLogger(Catalog.class);
	private final Map<String, Tuple<StorageReference, ViewSizeBundle, ViewSchema>> catalog;

	/* Default location of the catalog */
	private static final String path = Parameters.getProperty("catalogPath");

	private Catalog() {
		catalog = new HashMap<String, Tuple<StorageReference, ViewSizeBundle, ViewSchema>>();
	}

	/* Singleton */
	public static Catalog getInstance() {
		if (instance == null) {
			final File file = new File(path);
			if (file.exists() && file.length() > 0) {
				try {
					instance = load();
				} catch (IOException e) {
					log.error("Couldn't load from the catalog file!", e);
				}
			} else {
				instance = new Catalog();
				try {
					write(instance);
				} catch (IOException e) {
					log.error("Couldn't write to the catalog file!", e);
				}
			}
		}
		return instance;
	}

	/** Attempts to read, JSON-decode, and return the contents of a catalog */
	public static Catalog load() throws IOException {
		final Gson gson = createGson();
		final String json = Files.toString(new File(path), CHARSET);
		final Type type = new TypeToken<Catalog>() {
		}.getType();
		return gson.fromJson(json, type);
	}

	/**
	 * Receives a catalog and attempts to JSON-encode it and write it into a
	 * file
	 */
	public static void write(final Catalog catalog) throws IOException {
		final Gson gson = createGson();
		final String json = gson.toJson(catalog);
		Files.write(json, new File(path), CHARSET);
	}

	/** Attempts to JSON-encode and write a catalog into a file */
	public void write() throws IOException {
		write(this);
	}

	/** Adds a new item to the catalog */
	public void add(final String key, final StorageReference ref, final ViewSizeBundle stats,
			final ViewSchema viewSchema) {
		catalog.put(key, new Tuple<StorageReference, ViewSizeBundle, ViewSchema>(ref, stats, viewSchema));
		try {
			write();
		} catch (IOException e) {
			log.error("Couldn't write to the catalog file!", e);
		}
	}

	/** Deletes the catalog */
	public void delete() {
		final File file = new File(path);
		if (file.exists()) {
			file.delete();
		}
		catalog.clear();
	}

	/** Searches the catalog for a specified string key */
	public boolean contains(String key) {
		return catalog.containsKey(key);
	}

	@Override
	public String toString() {
		return catalog.toString();
	}

	private static Gson createGson() {
		return new GsonBuilder().registerTypeAdapter(StorageReference.class, new InterfaceAdapter<StorageReference>())
				.create();
	}
	
	/* Getters and setters */
	public ViewSizeBundle lookUpViewSize(String key) {
		return catalog.get(key).second();
	}

	public StorageReference getStorageReference(String key) {
		return catalog.get(key).first();
	}

	public ViewSchema getViewSchema(String key) {
		return catalog.get(key).third();
	}
}
