package fr.inria.cedar.commons.tatooine.storage.database;

import fr.inria.cedar.commons.tatooine.Parameters;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.tdb.TDBLoader;

import java.io.File;
import java.io.IOException;

/**
 * Created by DucCao on 11/10/16.
 * NOTE: put the path of tdbloader tool under tatooine.conf (you must have Apache Jena on your system)
 */
public class JENADatabaseHolderWithTDB extends JENADatabaseHolder {

    public JENADatabaseHolderWithTDB(String model) {
		super(model);
	}

	@Override
    protected Model loadModel() {
        String modelFileName = modelStr.split("/")[modelStr.split("/").length - 1];
        String datasetName = modelFileName.substring(0, modelFileName.indexOf(".")) + "-tdb";

        File tdbDataset = new File(datasetName);
        if (!tdbDataset.exists()) {
            try {
                String tdbloaderPath = Parameters.getProperty("tdbloaderPath");
                Process process = Runtime.getRuntime().exec(
                        String.format("%s --loc %s %s", tdbloaderPath, datasetName, modelStr)
                );
                process.waitFor();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Model model = ModelFactory.createDefaultModel();

        Dataset dataset = TDBFactory.createDataset(datasetName) ;
        dataset.begin(ReadWrite.READ) ;
        TDBLoader.loadModel(model, modelStr);
        dataset.end() ;

        return model;
    }
}
