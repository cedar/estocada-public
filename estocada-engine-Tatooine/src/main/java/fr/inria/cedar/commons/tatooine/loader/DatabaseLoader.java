package fr.inria.cedar.commons.tatooine.loader;

import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Created by DucCao on 05/10/16.
 * Interface to load data file to database
 */
public abstract class DatabaseLoader {
    public static final String KEY_CATALOG_ENTRY_NAME = "ENVIRONMENT_PARA";

    protected StorageReference storageReference;

    /**
     * Load a data file to database
     * @param filePath
     * @return true if the loading process is successfully, otherwise return false
     */
    public abstract boolean load(String filePath);

    /**
     * Delete (drop) all data from a collection (or table)
     * @param dataCollectionName
     */
    public abstract void deleteAllData(String dataCollectionName);

    /**
     * Delete the collection (or table) from database
     * @param dataCollectionName
     */
    public abstract void deleteCollection(String dataCollectionName);

    /**
     * Check if the data collection is loaded into database
     * @param dataCollectionName
     * @return true if it is loaded, otherwise return false
     */
    public abstract boolean isDataLoaded(String dataCollectionName);

	/**
	 * Register the new catalog entry corresponding to new loaded data
     */
    public abstract void registerNewCatalogEntry();

	/**
     * Register the StorageReference object in order to access the data source later
     * @param storageReference
     */
    public void setStorageReference(StorageReference storageReference) {
        this.storageReference = storageReference;
    }

	/**
     * @return name of the corresponding catalog entry
     */
    public String getCatalogEntryName() {
        return ((GSR) storageReference).getViewURI();
    }
}
