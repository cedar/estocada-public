package fr.inria.cedar.commons.tatooine.IDs;

/**
 * Interface that will be implemented by classes that represent different ID schemes.
 * 
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public interface IDScheme {

	/** True if the scheme is order-preserving (e.g., 1, 2, 3...) */
	public boolean isOrderPreserving();

	/** True if the scheme allows to infer parent-child relationships (e.g., pre, post, depth) */
	public boolean isParentAncestorPreserving();

	/** True if the scheme allows parent navigation (e.g., Dewey) */
	public boolean allowsParentNavigation();

	/** True if the scheme allows updates (e.g., Dewey or floating point) */
	public boolean allowsUpdates();

	/** To be called when a document starts, for possible initialization */
	public void beginDocument();

	/** To be called when an element or attribute starts. Convention: This is called in element pre-order. */
	public void beginNode();

	/** 
	 * To be called when an element or attribute starts. Convention: This is called in element pre-order.
	 * This is used when the IDScheme is required to store the tag associated with each ID. 
	 */
	public void beginNode(String tag);

	/** To be called when an element or attribute ends. Convention: This is called in element post-order. */
	public void endNode();

	/** To be called at the end of the document */
	public void endDocument();
	
	/** 
	 * Returns the ID of the last element for which endElement has been called.
	 * Convention: This is called after endElement() and before the next beginElement(). 
	 */
	public ElementID getLastID();
	
	/**
	 * Returns a string snippet containing the JDBC type(s) associated to the IDs produced by this scheme.
	 * @param suffix
	 * @return
	 */
	public String getSignature(String suffix);
	
	/**
	 * Returns a string snippet containing the name of the ID attribute produced by this scheme.
	 * This is used to declare an index via JDBC.
	 * @param suffix
	 * @return
	 */
	public String getIndexSignature(String suffix);
	
	/** Returns a string that will be used to enter null ID values in an RDB */
	public String nullIDStringImage();
}
