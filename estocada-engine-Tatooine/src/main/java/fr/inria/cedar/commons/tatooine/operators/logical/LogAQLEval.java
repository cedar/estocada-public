package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical AQL query operator.
 * 
 * @author Romain Primet
 */
public class LogAQLEval extends LogLeafOperator {

    private final String query;
    private StorageReference aqlRef;

    public LogAQLEval(String query, NRSMD meta, StorageReference aqlRef) {
        this.query = query;
        this.aqlRef = aqlRef;
        this.setOwnName("LogAQLEval");
        this.setVisible(true);
        this.setNRSMD(meta);
    }

    @Override
    public int getJoinDepth() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public String getName() {
        return "LogAQLEval";
    }

    public StorageReference getStorageReference() {
        return aqlRef;
    }

    @Override
    public String toString() {
        return "[LogAQLEval query: " + query + "]";
    }

    @Override
    public void accept(LogOperatorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public LogOperator deepCopy() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedCardinality() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedIOCost() {
        throw new UnsupportedOperationException("Unsupported");
    }

    public String getQueryString() {
        return query;
    }
}
