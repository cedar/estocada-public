package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.predicates.Predicate;

/**
 * Class that represents a selection logical operator and that extends {@link LogUnaryOperator}.
 *
 * @author Ioana MANOLESCU
 */
public class LogSelection extends LogUnaryOperator{
	public Predicate pred;

	public LogSelection(LogOperator child, Predicate pred){
		super(child);
		this.pred = pred;
		this.setVisible(true);
		this.setOwnName("LogSel");
		this.setOwnDetails("[" + pred.toString() + "]");
		this.setNRSMD(child.getNRSMD());
	}

	@Override	public void accept(LogOperatorVisitor visitor){
		getChild().accept(visitor);
		visitor.visit(this);
	}

	/**
	 * Makes a deep copy of the current operator
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogSelection deepCopy() {
		LogSelection copy = null;
		copy = new LogSelection(this.getChild().deepCopy(), (Predicate) this.pred.clone());
		copy.setName(this.getName());
		copy.setOwnDetails(this.getOwnDetails());
		copy.setEquivalentPattern(this.getEquivalentPattern());
		return copy;
	}

	@Override
	public double estimatedCardinality() {
		return Math.ceil(this.getChild().estimatedCardinality() * 0.5d);
	}

	@Override
	public double estimatedIOCost() {
		return this.getChild().estimatedIOCost();
	}
}
