package fr.inria.cedar.commons.tatooine.operators.physical;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * PostgresJSON Query Parameterized Operator.
 * 
 * @author ranaalotaibi
 *
 */
public abstract class PostgresJSONQueryParameterizedOperator extends QueryParameterisedOperator {

    /** Universal version identifier for the PhySQLOperator class **/
    private static final long serialVersionUID = 8654358043466128435L;
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(PostgresJSONQueryParameterizedOperator.class);

    /** Storage Reference **/
    protected StorageReference storageReference;
    /** Query string **/
    protected String queryStr;
    /** SQL Connection **/
    protected Connection connection;
    /** SQL PreparedStatement **/
    protected PreparedStatement preparedStatement;
    /** SQL Result set **/
    protected ResultSet resultSet;
    /** NTuple **/
    protected NTuple current = null;
    /** Query parameters **/
    protected NTuple params;

    /** Constructor **/
    public PostgresJSONQueryParameterizedOperator(final String query, final StorageReference storageReference,
            final NRSMD nrsmd) {
        super();
        queryStr = query;
        this.storageReference = storageReference;
    }

    @Override
    public void open() throws TatooineExecutionException {
        try {
            connection = DriverManager.getConnection(storageReference.getPropertyValue("url"));
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(queryStr);
            preparedStatement.setFetchSize(1000000);
        } catch (SQLException exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        try {
            if (resultSet == null) {
                resultSet = preparedStatement.executeQuery();
            }
            if (current != null) {
                return true;
            } else {
                resultSet.next();
                current = toNTuple();
                if (isNullOrEmpty(current)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (SQLException excpetion) {
            throw new TatooineExecutionException(excpetion);
        }
    }

    private boolean isNullOrEmpty(NTuple tuple) {
        if (tuple == null) {
            return true;
        } else if ((tuple.iID == 0) && (tuple.iInteger == 0) && (tuple.iString == 0) && (tuple.iUri == 0)
                && (tuple.iNested == 0)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        if (resultSet == null) {
            final String message = "Uninitialized result set";
            LOGGER.error(message);
            throw new TatooineExecutionException(message);
        }
        NTuple tuple = current;
        current = null;
        return tuple;
    }

    @Override
    public void close() throws TatooineExecutionException {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void initialize() throws TatooineExecutionException {
        try {
            resultSet.beforeFirst();
        } catch (SQLException exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setParameters(NTuple params) throws IllegalArgumentException {
        this.params = params;
        try {
            checkParameters();
            fillParameters();
        } catch (TatooineExecutionException excpetion) {
            throw new IllegalArgumentException(excpetion);
        }
    }

    /**
     * Check if the query is parameterized.
     * 
     * @throws TatooineExecutionException
     */
    private void checkParameters() throws TatooineExecutionException {
        try {
            final int queryParamCount = preparedStatement.getParameterMetaData().getParameterCount();

            if (queryParamCount == 0 || queryParamCount > params.nrsmd.colNo) {
                final String message = String.format("Invalid parameter count: expected <= %s, got %s",
                        params.nrsmd.colNo, queryParamCount);
                throw new TatooineExecutionException(message);
            }
        } catch (SQLException exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    /**
     * Replaces the placeholder in the parameterized query with the actual values.
     * 
     * @throws TatooineExecutionException
     */
    private void fillParameters() throws TatooineExecutionException {
        try {
            final ParameterMetaData pmd = preparedStatement.getParameterMetaData();
            for (int i = 1; i <= pmd.getParameterCount(); i++) {
                switch (pmd.getParameterType(i)) {
                    case Types.INTEGER:
                        preparedStatement.setInt(i, params.getIntegerField(i - 1));
                        break;

                    case Types.VARCHAR:
                        preparedStatement.setString(i, new String(params.getStringField(i - 1)));
                        break;

                    default:
                        final String message = String.format("Unsupported SQL typecode: %s", pmd.getParameterType(i));
                        throw new TatooineExecutionException(message);
                }
            }
        } catch (SQLException exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    /**
     * Transform the current tuple from PostgresJSON into NTuple.
     * 
     * @return the constructed NTuple.
     * @throws TatooineExecutionException
     */
    protected NTuple toNTuple() throws TatooineExecutionException {
        final NTuple tuple = new NTuple(nrsmd);

        try {
            final TupleMetadataType[] tupleMetadataTypes = nrsmd.getColumnsMetadata();
            final String[] colNames = nrsmd.getColNames();
            for (int index = 0; index < nrsmd.types.length; index++) {
                switch (tupleMetadataTypes[index]) {
                    case INTEGER_TYPE:
                        tuple.addInteger(resultSet.getInt(colNames[index]));
                        break;

                    case STRING_TYPE:
                        tuple.addString(resultSet.getString(colNames[index]));
                        break;

                    case TUPLE_TYPE:
                        final ObjectMapper objectMapper = new ObjectMapper();
                        JsonNode jsonNode;
                        try {
                            jsonNode = objectMapper.readTree(resultSet.getString((colNames[index])));
                            tuple.addNestedField(transformNestedTuple(jsonNode, nrsmd.getNestedChild(index)));

                        } catch (IOException exception) {
                            throw new TatooineExecutionException(exception);
                        }
                        break;
                    default: {
                        final String message =
                                String.format("PhyPostgresJSONEval only handles string, integer, and nested types");
                        LOGGER.error(message);
                        throw new TatooineExecutionException(message);
                    }
                }
            }
            return tuple;
        } catch (SQLException exception) {
            resultSet = null;
            return null;
        }
    }

    /**
     * Transform JSON array element to List<NTuple>
     * 
     * @param value
     *            the nested JSON element
     * @param childNRSMD
     *            the NRSMD of the nested value
     * @return The list of NTuple
     * @throws TatooineExecutionException
     */
    private List<NTuple> transformNestedTuple(final JsonNode value, final NRSMD childNRSMD)
            throws TatooineExecutionException {
        final List<NTuple> children = new ArrayList<NTuple>();
        final Iterator<JsonNode> iterator = value.elements();
        while (iterator.hasNext()) {
            final NTuple nTuple = new NTuple(childNRSMD);
            final JsonNode tuple = iterator.next();
            for (int index = 0; index < childNRSMD.types.length; index++) {
                final String name = childNRSMD.colNames[index];
                final JsonNode innerValue = tuple.get(name);
                final JsonType jsonType = getJSONElementType(innerValue);
                switch (jsonType) {
                    case STRING:
                        nTuple.addString(innerValue.asText());
                        break;
                    case INTEGER:
                        nTuple.addInteger(innerValue.asInt());
                        break;
                    case ARRAY:
                        nTuple.addNestedField(transformNestedTuple(innerValue, childNRSMD.getNestedChild(index)));
                        break;
                    default:
                        final String message =
                                String.format("PhyPostgresJSONEval only handles string, integer, and nested types");
                        LOGGER.error(message);
                        throw new TatooineExecutionException(message);
                }

            }
            children.add(nTuple);
        }

        return children;
    }

    /**
     * Get the JSON element type.
     * 
     * @param jsonNode
     *            the JSON element
     * @return the JSON element type.
     */
    private JsonType getJSONElementType(final JsonNode jsonNode) {

        if (jsonNode.isTextual()) {
            return JsonType.STRING;
        }
        if (jsonNode.isInt() || jsonNode.isBigInteger() || jsonNode.isDouble() || jsonNode.isBigDecimal()) {
            return JsonType.INTEGER;
        }
        if (jsonNode.isArray()) {
            return JsonType.ARRAY;
        }
        if (jsonNode.isObject()) {
            return JsonType.ARRAY;
        }

        return null;
    }

    /** Enumerated type representing all supported JSON types **/
    private enum JsonType {
        INTEGER,
        STRING,
        ARRAY,
        OBJECT;
    }

    /** Get the query **/
    public String getQuery() {
        return queryStr;
    }

    /** Get the NRSMD **/
    public NRSMD getNRSMD() {
        return nrsmd;
    }

    /** Get the storage reference **/
    public StorageReference getStorageReference() {
        return storageReference;
    }

}
