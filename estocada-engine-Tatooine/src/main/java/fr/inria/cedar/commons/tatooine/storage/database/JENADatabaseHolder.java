package fr.inria.cedar.commons.tatooine.storage.database;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.FileManager;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.JenaDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.multidoc.EngineSystem;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * This class handles reading and querying RDF data using Apache Jena.
 * @author Oscar Mendoza
 */
public class JENADatabaseHolder implements DatabaseHolder {

	private static final Logger log = Logger.getLogger(JENADatabaseHolder.class);

	/* Default constants */
	public static final String DEFAULT_ID = "JENADatabaseHolder";
	public static final String DEFAULT_FORMAT = "TTL";

	/* Unique identifier for this database holder */
	private final String id;

	// to check whether a model has been loaded before
	// and store the loaded model(s) for later usage
	protected static Map<String, Model> mapLoadedCatalogEntries = new HashMap<>();

	// map between the loaded RDF file and the corresponding model
	protected static Map<String, Model> mapLoadedRdfFiles = new HashMap<>();

	/* Path to the RDF model */
	public String modelStr = "";

	// view URI (also the catalog entry name) of the model that has been loaded recently
	protected String currentCatalogEntry = "";

	/* Query that selects all RDF triplets */
	public String queryStr = "SELECT $s $p $o WHERE { $s $p $o }";

	/* MD of the collection */
	protected NRSMD nrsmd;

	// Query placeholder
	protected static final String PLACEHOLDER = "%s";

	// Results
	List<NTuple> tuples = new ArrayList<NTuple>();

	/* Constructor */
	protected JENADatabaseHolder(String model) {
		this.id = DEFAULT_ID;
		modelStr = model;
	}

	/**
	 * Discard the loaded models
	 */
	public static void clearCache() {
		mapLoadedRdfFiles.clear();
		mapLoadedCatalogEntries.clear();
	}

	/* Getters and setters */
	/** Returns the RDF model that was read and loaded */
	public Model getModel() {
		return mapLoadedCatalogEntries.get(currentCatalogEntry);
	}

	/**
	 * Perform the loading RDF model process using Jena
	 * @return an RDF model
	 */
	protected Model loadModel() {
		InputStream in = FileManager.get().open(modelStr);
		if (in == null) {
			String message = "File: '" + modelStr + "' not found";
			log.error(message);
			throw new IllegalArgumentException(message);
		}

		// Load RDF model
		Model model = ModelFactory.createDefaultModel();
		model.read(in, null, DEFAULT_FORMAT);

		return model;
	}

	/**
	 * Load the RDF model from a given GSR parameter
	 * @param ref GSR object that holds the detailed information to retrieve the desired RDF model
	 */
	public void setModel(GSR ref) {
		modelStr = ref.getPropertyValue(JenaDatabaseLoader.MODEL_RDF);
		currentCatalogEntry = ref.getViewURI();

		if (hasRdfFileBeenLoaded(modelStr)) {
			log.debug(String.format("RDF file %s has been loaded", modelStr));
			Model loadedModel = mapLoadedRdfFiles.get(modelStr);
			mapLoadedCatalogEntries.put(currentCatalogEntry, loadedModel);
		} else if (!hasModelBeenLoaded(currentCatalogEntry)) {
			log.debug("setModel: " + currentCatalogEntry + ", " + modelStr);
			Model loadedModel = loadModel();
			mapLoadedCatalogEntries.put(currentCatalogEntry, loadedModel);
			mapLoadedRdfFiles.put(modelStr, loadedModel);
		}
	}

	public static boolean hasRdfFileBeenLoaded(String rdfFilePath) {
		return mapLoadedRdfFiles.get(rdfFilePath) != null;
	}

	/**
	 * @param catalogEntryName the catalog entry name of the RDF model we want to check
	 * @return true if this model has been loaded, otherwise return false
	 */
	public static boolean hasModelBeenLoaded(String catalogEntryName) {
		return mapLoadedCatalogEntries.get(catalogEntryName) != null;
	}

	public void setQuery(String query) {
		queryStr = query;
	}

	public NRSMD getNRSMD() {
		return nrsmd;
	}

	public void setNRSMD(NRSMD nrsmd) {
		this.nrsmd = nrsmd;
	}

	/* Method overriding */
	@Override
	public String getId() {
		return id;
	}

	@Override
	public EngineSystem getEngineSystem() {
		return EngineSystem.JENA;
	}

	@Override
	public void open(GSR view) throws Exception {
		// Auto-generated method stub
	}

	@Override
	public boolean containsDB(GSR view) throws Exception {
		// Auto-generated method stub
		return false;
	}

	@Override
	public void remove(GSR view) throws Exception {
		// Auto-generated method stub
	}

	@Override
	public void storeTuples(GSR view, Packet wrappedTuples) throws Exception {
		// Auto-generated method stub
	}

	@Override
	public void close() {
		// Auto-generated method stub
	}

	@Override
	public void closeCursor(int cursorId) {
		// Auto-generated method stub
	}

	@Override
	public boolean useOSFile() {
		return false;
	}

	@Override
	public int getCursor(GSR ref) throws Exception {
		// Read RDF model
		setModel(ref);

		// The query shouldn't have placeholders in it
		if (!queryStr.contains(PLACEHOLDER)) {
			ResultSet results = query(queryStr);
			try {
				// Transforming the response from JENA into an NTuple
				tuples = transformJENADocumentToNTuple(results);
			} catch (TatooineExecutionException e) {
				log.error("Error transforming JENA response into an NTuple");
				e.printStackTrace();
			}
		}

		return -1;
	}

	@Override
	public NTuple getNextRecord(int cursorId) {
		if (NIterator.timeout) {
			return null;
		}
		cursorId++;
		NTuple tuple = null;

		try {
			tuple = tuples.get(cursorId);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}

		return tuple;
	}

	/**
	 * Transforms the results of a SPARQL query into a list of NTuple objects.
	 * @param results
	 * @return
	 * @throws TatooineExecutionException
	 */
	private List<NTuple> transformJENADocumentToNTuple(ResultSet results) throws TatooineExecutionException {
		List<NTuple> records = new ArrayList<NTuple>();

		List<QuerySolution> querySolutions = iterateResultSet(results);

		for (QuerySolution result : querySolutions) {
			// Loop over the view's NRSMD (columns)			
			NTuple tuple = new NTuple(nrsmd);
			for (int index = 0; index < nrsmd.types.length; index++) {
				String name = nrsmd.colNames[index];
				RDFNode node = result.get(name);
                                TupleMetadataType type = nrsmd.getColumnsMetadata()[index];
                                String errorMessage;
                                String stringValue;

                                switch(type) {
                                case URI_TYPE:
                                    if(node.isURIResource()){
                                        stringValue = node.asResource().getURI();
                                        tuple.addUri(stringValue);
                                    } else {
                                        errorMessage = String.format("Tuple metadata %s only compatible URI RDF node, but receive the RDF node : %s", type, node);
                                        throw new TatooineExecutionException(errorMessage);
                                    }
                                    break;
				case INTEGER_TYPE:
                                    if(node.isLiteral()){
                                        stringValue = node.asLiteral().getString();
                                        try {
                                            tuple.addInteger(Integer.parseInt(stringValue));
                                        } catch (NumberFormatException e) {
                                            e.printStackTrace();
                                            errorMessage = String.format("Tuple metadata %s needs a well formed integer literal, but receive the RDF node : %s", type, node);
                                            throw new TatooineExecutionException(errorMessage);
                                        }
                                    } else {
                                        errorMessage = String.format("Tuple metadata %s only compatible literal RDF node, but receive the RDF node : %s", type, node);
                                        throw new TatooineExecutionException(errorMessage);
                                    }
					break;
				case STRING_TYPE:
                                    if(node.isLiteral()){
                                        stringValue = node.asLiteral().getString();
                                    } else {
                                        stringValue = node.asResource().getURI();
                                    }
                                    tuple.addString(stringValue);
					break;
				default:
                                    errorMessage = String.format("Tuple metadata %s is not supported as a translation of RDF node", type);
                                    throw new UnsupportedOperationException(errorMessage);
                                }
                        }
                        records.add(tuple);
		}

		return records;
	}

	/**
	 * https://jena.apache.org/documentation/javadoc/arq/org/apache/jena/query/QueryExecution.html#execSelect--
	 * The query only gets evaluated when you actually start iterating over the results.
	 * So we should measure the execution time of SPARQL query in this function.
	 */
	private List<QuerySolution> iterateResultSet(ResultSet resultSet) throws TatooineExecutionException {
		List<QuerySolution> records = new ArrayList<>();

		long current = System.currentTimeMillis();
		while (resultSet.hasNext()) {
			QuerySolution result = resultSet.next();
			records.add(result);
		}
		log.debug("Sparql query time = " + (System.currentTimeMillis() - current));

		return records;
	}

	/** Queries a RDF source and returns its response */
	private ResultSet query(String sparql) {
		// Fetches response
		Query query = QueryFactory.create(sparql);
		QueryExecution exec = QueryExecutionFactory.create(query, getModel());
		return exec.execSelect();
	}

}
