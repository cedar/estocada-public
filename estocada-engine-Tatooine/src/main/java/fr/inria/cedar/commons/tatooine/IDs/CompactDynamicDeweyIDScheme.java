package fr.inria.cedar.commons.tatooine.IDs;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;

/**
 * Class that represents a compact dynamic Dewey ID scheme.
 *
 * @author Martin GOODFELLOW
 */
public class CompactDynamicDeweyIDScheme implements IDScheme {
	private static final Logger log = Logger.getLogger(CompactDynamicDeweyIDScheme.class);
	private static String nullIDStringImage = "null";

	private static int tagCount = 0;

	public int[] currentId;
	int currentSiblingNo;
	int[] parentId;
	int[] currentTag;
	int[] parentTag;

	/** The integer representing the current child of a parent, initially 0, then 1, 2, 3... */
	HashMap<int[], Integer> currentChildren;

	Stack<CompactDynamicDeweyElementID> s;

	int depth;
	int lastDepth;

	CompactDynamicDeweyElementID lastID;

	public static Map<String, Integer> tagDictionary = new HashMap<String, Integer>();

	public CompactDynamicDeweyIDScheme() {
		currentId = new int[0];
		parentId = new int[0];
		currentTag = new int[0];
		parentTag = new int[0];
		currentSiblingNo = 0;
		s = new Stack<CompactDynamicDeweyElementID>();
		lastID = null;
		depth = 0;
		lastDepth = 0;
		currentChildren = new HashMap<int[], Integer>();
	}

	public CompactDynamicDeweyIDScheme(CompactDynamicDeweyElementID id) {
		int[] tempPath = new int[id.path.length + 1];
		System.arraycopy(id.path, 0, tempPath, 0, id.path.length);
		tempPath[id.path.length] = 0;
		currentId = tempPath;
		parentId = id.path;
		int[] tempTag = new int[id.tag.length + 1];
		System.arraycopy(id.tag, 0, tempTag, 0, id.tag.length);
		tempPath[id.tag.length] = -1;
		currentTag = tempTag;
		parentTag = id.tag;

		currentSiblingNo = 0;
		s = new Stack<CompactDynamicDeweyElementID>();
		// Is this needed?
		s.push(id);
		lastID = null;
		// Depth and lastDepth will be wrong but I don't think it's relevant for the update purpose
		depth = 0;
		lastDepth = 0;
		currentChildren = new HashMap<int[], Integer>();
		currentChildren.put(parentId, currentSiblingNo);
	}

	// FIXME: Needs tests
	public CompactDynamicDeweyIDScheme(CompactDynamicDeweyElementID id, CompactDynamicDeweyElementID lastSibling) {
		int[] tempPath = new int[id.path.length + 1];
		System.arraycopy(id.path, 0, tempPath, 0, id.path.length);
		tempPath[id.path.length] = lastSibling.n;
		currentId = tempPath;
		parentId = id.path;
		int[] tempTag = new int[id.tag.length + 1];
		System.arraycopy(id.tag, 0, tempTag, 0, id.tag.length);
		tempPath[id.tag.length] = -1;
		currentTag = tempTag;
		parentTag = id.tag;
		currentSiblingNo = lastSibling.n;
		s = new Stack<CompactDynamicDeweyElementID>();
		// Is this needed?
		s.push(id);
		lastID = null;
		// Depth and lastDepth will be wrong but I don't think it's relevant for the update purpose
		depth = 0;
		lastDepth = 0;
		currentChildren = new HashMap<int[], Integer>();
		currentChildren.put(parentId, currentSiblingNo);
	}

	/** True if the scheme is order-preserving */
	@Override
	public boolean isOrderPreserving() {
		return true;
	}

	/** True if the scheme allows to infer parent-child relationships */
	@Override
	public boolean isParentAncestorPreserving() {
		return true;
	}

	/** True if the scheme allows parent navigation */
	@Override
	public boolean allowsParentNavigation() {
		return true;
	}

	/** True if the scheme allows updates */
	@Override
	public boolean allowsUpdates() {
		return true;
	}

	/** To be called when a document starts, for possible initialization */
	@Override
	public void beginDocument() {
		currentChildren.put(this.parentId, new Integer(0));
	}

	/** To be called when an element or attribute starts. Convention: This is called in element pre-order. */
	@Override
	public void beginNode() {
		// Nothing...
	}

	/** 
	 * To be called when an element or attribute starts. Convention: This is called in element pre-order.
	 * This is used when the IDScheme is required to store the tag associated with each ID. 
	 */
	@Override
	public void beginNode(String tag) {
		log.debug("COMPACT DYNAMIC DEWEY BEGIN NODE");
		lastDepth = depth;
		depth++;

		Integer x = currentChildren.get(parentId);
		int k = x.intValue();

		CompactDynamicDeweyIDScheme.updateDictionary(tag);

		this.lastID = new CompactDynamicDeweyElementID(parentId, (k + 1), this.parentTag, tag);

		s.push(lastID);
		currentChildren.put(parentId, new Integer(k + 1));

		this.parentId = lastID.path;
		currentChildren.put(parentId, new Integer(0));

		this.parentTag = lastID.tag;

		log.debug("Created Compact Dynamic Dewey " + CompactDynamicDeweyIDScheme.getPathAsString(lastID.path)
				+ " parent String is " + CompactDynamicDeweyIDScheme.getPathAsString(parentId));
	}

	/** To be called when an element or attribute ends. Convention: This is called in element post-order. */
	@Override
	public void endNode() {
		log.debug("COMPACT DYNAMIC DEWEY END NODE");
		this.lastID = s.pop();

		if (s.empty()) {
			this.parentId = new int[0];
			this.parentTag = new int[0];
		} else {
			this.parentId = s.peek().path;
			this.parentTag = s.peek().tag;
		}

		log.debug("Now parentString is " + CompactDynamicDeweyIDScheme.getPathAsString(parentId));
		log.debug("Now parentTag is " + CompactDynamicDeweyIDScheme.getPathAsString(parentTag));
	}

	/** To be called at the end of the document */
	@Override
	public void endDocument() {
		// Nothing...
	}

	/** Returns the ID of the last element for which endElement has been called */
	@Override
	public ElementID getLastID() {
		return lastID;
	}

	/**
	 * Returns a string snippet containing the JDBC type(s) associated to the IDs produced by this scheme.
	 * @param suffix
	 * @return
	 */
	@Override
	public String getSignature(String suffix) {
		return " ID" + suffix + " varchar";
	}

	/**
	 * Returns a string snippet containing the name of the ID attribute produced by this scheme.
	 * This is used to declare an index via JDBC.
	 * @param suffix
	 * @return
	 */
	@Override
	public String getIndexSignature(String suffix) {
		return " ID" + suffix;
	}

	/** Returns a string that will be used to enter null ID values in an RDB */
	@Override
	public String nullIDStringImage() {
		return nullIDStringImage;
	}

	public static String getPathAsString(int[] path) {
		String s = "";
		for (int element : path) {
			s = s + element + ".";
		}
		return s;
	}

	public static void updateDictionary(String tag) {
		if (CompactDynamicDeweyIDScheme.tagDictionary.get(tag) == null) {
			CompactDynamicDeweyIDScheme.tagDictionary.put(tag, CompactDynamicDeweyIDScheme.tagCount);
			CompactDynamicDeweyIDScheme.tagCount++;
		}
	}

	public int[] getCurrentID() {
		return this.currentId;
	}

	public HashMap<int[], Integer> getCurrentChildren() {
		return this.currentChildren;
	}

	public static void main(String[] args) {
		CompactDynamicDeweyIDScheme dds = new CompactDynamicDeweyIDScheme();
		// 1
		dds.beginDocument();
		dds.beginNode("a");

		// 1.1
		dds.beginNode("b");
		dds.endNode();

		// 1.2
		dds.beginNode("c");
		// 1.2.1
		dds.beginNode("d");
		dds.endNode();

		// 1.2.2
		dds.beginNode("e");
		dds.endNode();

		// 1.2.3
		dds.beginNode("f");
		dds.endNode();
		dds.endNode();

		// 1.3
		dds.beginNode("g");
		dds.endNode();

		// 1.4
		dds.beginNode("h");

		// 1.4.1
		dds.beginNode("i");
		dds.endNode();
		dds.endNode();

		// 1.5
		dds.beginNode("j");
		dds.endNode();

		dds.endNode();
		dds.endDocument();
	}

}