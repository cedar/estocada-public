package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.JenaDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolder;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolderWithTDB;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolderWithTDBAndRDFSSaturation;

/**
 * Physical operator that queries an RDF data source using Apache JENA.
 * @author Oscar Mendoza
 */
public abstract class JenaSPARQLQueryParameterisedOperator extends QueryParameterisedOperator {
	
	/** Universal version identifier for the PhyJENAOperator class */
	private static final long serialVersionUID = 3068801040773742264L;

	private static final Logger log = Logger.getLogger(JenaSPARQLQueryParameterisedOperator.class);

	// Connection information
	protected GSR ref;

	// Query and parameters
	protected NTuple params;
	protected String queryStr = "";
	protected String paramStr = "";

	// Database holder
	protected int cursorId = -1;
	protected NTuple tuple = null;
	protected JENADatabaseHolder dbHolder = null;

    protected boolean useRDFSSaturation = false;

	// Query placeholder
	public static final String PLACEHOLDER = "%s";
	
	/* Constructor */
	public JenaSPARQLQueryParameterisedOperator(String query, GSR ref) {
		this.queryStr = query;
		this.ref = ref;
		increaseOpNo();
	}
	
	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());

		// Reading catalog server values
		String modelRDF = ref.getPropertyValue(JenaDatabaseLoader.MODEL_RDF);
		if (Strings.isNullOrEmpty(modelRDF)) {
			log.error("The RDF model path specified in the catalog is null or empty");
			throw new IllegalArgumentException("Can't load RDF model because \"modelRDF\" not found in GSR ref");
		}

		// DBHolder
		try {
            if(useRDFSSaturation) {
                dbHolder = new JENADatabaseHolderWithTDBAndRDFSSaturation(modelRDF);
            } else {
                dbHolder = new JENADatabaseHolderWithTDB(modelRDF);
            }
			dbHolder.setQuery(queryStr);
			dbHolder.setNRSMD(nrsmd);
			cursorId = dbHolder.getCursor(ref);
		} catch (Exception e) {
			throw new TatooineExecutionException(e);
		}

		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	/** Checks if a NTuple is null or empty */
	private boolean isNullOrEmpty(NTuple tuple) {
		if (tuple == null) {
			return true;
		} else if (tuple.uriFields == null || tuple.integerFields == null || tuple.stringFields == null
                   || tuple.idFields == null || tuple.nestedFields == null) {
			return true;
		} else if (tuple.uriFields.length == 0 && tuple.integerFields.length == 0 && tuple.stringFields.length == 0
                   && tuple.idFields.length == 0 && tuple.nestedFields.length == 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (timeout) {
			return false;
		}

		tuple = dbHolder.getNextRecord(cursorId);

		if (!isNullOrEmpty(tuple) && !timeout) {
			return true;
		}

		return false;
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		NTuple next = tuple;
		tuple = null;
		numberOfTuples++;
		cursorId++;
		return next;
	}
	
	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		dbHolder.closeCursor(cursorId);
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		List<NTuple> v = tuple.nestedFields[i];
		return new PhyArrayIterator(v, tuple.nrsmd.getNestedChild(i));
	}
	
	@Override
	public Map<String,String> getDetails() {
		Map<String,String> details = new HashMap<String,String>();
		details.put("query", queryStr);
		details.put("database", dbHolder!= null ? dbHolder.modelStr : "");
		return details;
	}

    @Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"JENASPARL\n"+ this.queryStr +"\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return firstAvailableNo + 1;
	}

	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		this.params = params;
		paramStr = super.updateParameters(params, queryStr, PLACEHOLDER, null);
		dbHolder.setQuery(paramStr);
		try {
			cursorId = dbHolder.getCursor(ref);
		} catch (Exception e) {
			e.printStackTrace();
			String msg = "Error transforming JENA response into an NTuple"; 
			log.error(msg);
			throw new IllegalArgumentException(msg);
		}
	}	
			
	/* Getters and setters */
	
	public String getQuery() {
		return queryStr;
	}

	@Override
	public void setQuery(String query) {
		this.queryStr = query;
		if (dbHolder != null) {
			dbHolder.setQuery(query);
			cursorId = -1;
		}
	}
	
	public GSR getRef() {
		return ref;
	}

	/**
	 * @param useRDFSSaturation the useRDFSSaturation to set
	 */
	public void setUseRDFSSaturation(boolean useSaturation) {
		this.useRDFSSaturation = useSaturation;
	}

}
