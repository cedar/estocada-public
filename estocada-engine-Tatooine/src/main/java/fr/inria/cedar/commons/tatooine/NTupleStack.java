package fr.inria.cedar.commons.tatooine;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Class with a stack of NTuple objects and methods for handling the stack. 
 * It will be used by the structural join physical operators.
 * @author Ioana MANOLESCU
 */
public class NTupleStack {
	public ArrayList<NTuple> elems;

	public NTupleStack() {
		elems = new ArrayList<NTuple>();
	}

	public void push(NTuple t) {
		elems.add(t);
	}

	public NTuple pop() {
		NTuple t = (NTuple) elems.get(elems.size() - 1);
		elems.remove(elems.size() - 1);
		return t;
	}

	public NTuple top() {
		return elems.get(elems.size() - 1);
	}

	public Iterator<NTuple> iterator() {
		return elems.iterator();
	}

	public boolean empty() {
		return (elems.size() == 0);
	}

}