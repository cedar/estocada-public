package fr.inria.cedar.commons.tatooine.IDs;

import java.util.Stack;

/**
 * Ordered integer element ID scheme.
 * 
 * @author Ioana MANOLESCU
 * 
 * @created 13/06/2005
 */
public class OrderedIntegerIDScheme implements IDScheme {
	
	int n;

	OrderedIntegerElementID ied;

	Stack<OrderedIntegerElementID> s;
	
	private static String nullIDStringImage = "null";
	
	public OrderedIntegerIDScheme() {
		n = 0;
		ied = null;
		s = new Stack<OrderedIntegerElementID>();
	}

	/** True if the scheme is order-preserving */
	public final boolean isOrderPreserving() {
		return true;
	}

	/** True if the scheme allows to infer parent-child relationships */
	public final boolean isParentAncestorPreserving() {
		return false;
	}

	/** True if the scheme allows parent navigation */
	public final boolean allowsParentNavigation() {
		return false;
	}

	/** True if the scheme allows updates */
	public final boolean allowsUpdates() {
		return false;
	}

	/** To be called when a document starts, for possible initialization */
	public final void beginDocument() {
		n = 0;
	}

	/** To be called when an element or attribute starts. Convention: This is called in element pre-order. */
	public final void beginNode() {
		s.push(new OrderedIntegerElementID(n));
		n ++;
	}
	
	/** 
	 * To be called when an element or attribute starts. Convention: This is called in element pre-order.
	 * This is used when the IDScheme is required to store the tag associated with each ID. 
	 */
	public final void beginNode(String tag) {
		// TODO Auto-generated method stub
	}

	/** To be called when an element or attribute ends. Convention: This is called in element post-order. */
	public final void endNode() {
		ied = (OrderedIntegerElementID)(s.pop());
		// Nothing...
	}

	/** To be called at the end of the document */
	public final void endDocument() {
		// Nothing...
	}

	/** 
	 * Returns the ID of the last element for which endElement has been called.
	 * Convention: This is called after endElement() and before the next beginElement(). 
	 */
	public final ElementID getLastID() {
		return ied;
	}
	
	/**
	 * Returns a string snippet containing the JDBC type(s) associated to the IDs produced by this scheme.
	 * @param suffix
	 * @return
	 */
	public final String getSignature(String suffix){
		return new String("ID" + suffix + " int");
	}
	
	/** Returns a string that will be used to enter null ID values in an RDB */
	public final String nullIDStringImage(){
		return nullIDStringImage;
	}

	/**
	 * Returns a string snippet containing the name of the ID attribute produced by this scheme.
	 * This is used to declare an index via JDBC.
	 * @param suffix
	 * @return
	 */
	public String getIndexSignature(String suffix) {
		return ("ID" + suffix);
	}

}
