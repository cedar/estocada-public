package fr.inria.cedar.commons.tatooine.operators.physical;

public abstract class QueryParameterisedOperator extends BindAccess {

	/** Universal version identifier for the PhyLeafOperator class */
	private static final long serialVersionUID = -1044611722752591118L;
	
	protected String queryStr = "";
	
	public String getQuery() {
		return queryStr;
	}
	
	public void setQuery(String query) {
		queryStr = query;
	}
	
}
