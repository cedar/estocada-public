package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;

/**
 * This logical operator scans NTuples with the help of {@link NTupleReader} which parses TXT files written in a specific format
 * @author Oscar Mendoza
 */
public class LogTupleReaderScan extends LogLeafOperator {
	private static final Logger log = Logger.getLogger(LogTupleReaderScan.class);
	public List<NTuple> tuples = new ArrayList<NTuple>();	
	
	// Constructor
	public LogTupleReaderScan(String path) throws TatooineExecutionException {
		NTupleReader reader = new NTupleReader(path);
		ArrayList<NTuple> tuples = reader.read();
		this.tuples = tuples;
		this.setVisible(true);
		this.setNRSMD(tuples.get(0).nrsmd);
		this.setOwnName("LogTupleReaderScan");
	}

	@Override
	public int getJoinDepth() {
		return 0;
	}

	@Override
	public String getName() {
		return this.getOwnName();
	}

	/** Makes a deep copy of the current operator */
	@Override
	public LogTupleReaderScan deepCopy() {
		try {
			return (LogTupleReaderScan) this.clone();
		} catch (CloneNotSupportedException e) {
			log.error("Exception: ", e);
			return null;
		}
	}

	@Override
	public double estimatedCardinality() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double estimatedIOCost() {
		// TODO Auto-generated method stub
		return 0;
	}

}
