package fr.inria.cedar.commons.tatooine.IDs;

import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * Factory class that creates and returns the ID scheme specified.
 * @author Ioana MANOLESCU
 */
public class IDSchemeAssignator {
	
	/** Returns the kind of ID scheme that the node passed as a parameter specifies */
	public static final IDScheme getIDScheme(TreePatternNode pn) {
		if (pn.storesID()) {
			if (pn.isIdentityIDType()) {
				return getIdentityScheme();
			}
			if (pn.isOrderIDType()) {
				return getOrderPreservingScheme();
			}
			if (pn.isStructIDType()) {
				return getStructuralScheme();
			}
			if (pn.isUpdateIDType()) {
				return getUpdateScheme();
			}
			return null;
		}
		else{
			return null;
		}
	}
	
	/** Returns an IDScheme whose only commitment is to respect node identity */
	public static IDScheme getIdentityScheme() {
		return new OrderedIntegerIDScheme();
	}
	
	/** Returns an IDScheme which furthermore promises to respect order */
	public static IDScheme getOrderPreservingScheme() {
		return new OrderedIntegerIDScheme();
	}
	
	/**
	 * Returns an IDScheme which allows, by comparing two IDs, to know whether they are in an
	 * ancestor-descendant (or parent-child) relationship or not.
	 */
	public static IDScheme getStructuralScheme(){
		return IDSchemeFactory.getElementIDScheme();
	}
	
	/** Returns an IDScheme which tolerates updates */
	public static IDScheme getUpdateScheme(){
		return new CompactDynamicDeweyIDScheme();
	}
	
}
