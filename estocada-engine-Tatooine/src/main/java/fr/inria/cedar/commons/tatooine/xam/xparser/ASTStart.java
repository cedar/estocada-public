/* Generated By:JJTree: Do not edit this line. ASTStart.java */

package fr.inria.cedar.commons.tatooine.xam.xparser;

public class ASTStart extends SimpleNode {
	public ASTStart(int id) {
		super(id);
	}

	public ASTStart(XamParserVJ p, int id) {
		super(p, id);
	}

	/** Accept the visitor */
	public Object jjtAccept(XamParserVJVisitor visitor, Object data) {
		return visitor.visit(this, data);
	}
}
