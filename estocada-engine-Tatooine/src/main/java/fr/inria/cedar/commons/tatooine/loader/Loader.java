package fr.inria.cedar.commons.tatooine.loader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;

/**
 * Interface that a tuples loader should implement.
 * @author Ioana MANOLESCU
 * @created 27/07/2005
 */
public interface Loader {

	public HashMap<TreePattern, ArrayList<NTuple>> extractTuples(String XMLFileName, Collection<TreePattern> xs);

	public StorageReference[] load(String configFileName, String XMLFileName, TreePattern[] p);

}