package fr.inria.cedar.commons.tatooine.operators.physical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Physical operator that queries JSON data source using Postgres with JSON support.
 * The operator handles the nested results obtained from PostgresJSON.
 * 
 * @author ranaalotaibi
 *
 */
public class PhyPostgresJSONEval extends PostgresJSONQueryParameterizedOperator implements PhyEvalOperator {

    /** Universal version identifier for the PhyPostgresJSONEval class */
    private static final long serialVersionUID = -1751712833235647239L;
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(PhyPostgresJSONEval.class);

    /** Constructor **/
    public PhyPostgresJSONEval(final String query, final StorageReference storageReference, final NRSMD nrsmd)
            throws TatooineExecutionException {
        super(query, storageReference, nrsmd);
        this.nrsmd = nrsmd;
    }

    @Override
    public String getName() {
        return "PhyPostgresJSONEval";
    }

    @Override
    public String getName(int depth) {
        return getName();
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        try {
            return new PhyPostgresJSONEval(queryStr, storageReference, nrsmd);
        } catch (Exception exception) {
            throw new TatooineExecutionException(exception);
        }
    }

    @Override
    public NRSMD inferMetadata(NRSMD inputMD, String queryParams) throws TatooineExecutionException {
        throw new TatooineExecutionException("Unsupported");

    }

    @Override
    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        throw new UnsupportedOperationException("Unsupported");

    }

}
