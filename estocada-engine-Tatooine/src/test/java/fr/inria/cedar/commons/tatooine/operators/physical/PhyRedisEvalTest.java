package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

import org.junit.Test;
import org.mockito.Mockito;

import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;
import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.storage.database.RedisSourceLayer;

public class PhyRedisEvalTest extends BaseTest {

	private NRSMD getNRSMDLeaders() throws TatooineExecutionException {
		return new NRSMD(3, 
				new TupleMetadataType[] {
					TupleMetadataType.STRING_TYPE, 
					TupleMetadataType.STRING_TYPE, 
					TupleMetadataType.STRING_TYPE },
				new String[] {"name", "dob", "country"},
				new NRSMD[0]);
	}
	
	private Object getResponseLeaders() {
		SetType set = new SetType();
		set.addToSet(createMap("Justin Trudeau", "25-12-71", "CA"));
		set.addToSet(createMap("Michel Temer", "23-09-40", "BR"));
		set.addToSet(createMap("Theresa May", "01-10-56", "UK"));
		set.addToSet(createMap("Angela Merkel", "17-07-54", "DE"));
		set.addToSet(createMap("Andrzej Duda", "16-05-72", "PO"));
		return set;
	}
	
	private MapType createMap(String name, String dob, String country) {
		MapType map = new MapType();
		map.putToMap("name", name);
		map.putToMap("dob", dob);
		map.putToMap("country", country);
		return map;
	}
	
	@Test
	/** Queries a set of maps stored in Redis */
	public void testSetOfMaps() throws Exception {
		String queryStr = "SELECT leaderInfo FROM follower IN MAP[leaderSet], leaderInfo IN MAP[leader]";
		PhyRedisEval searcher = mock(PhyRedisEval.class, Mockito.CALLS_REAL_METHODS);
		RedisSourceLayer source = mock(RedisSourceLayer.class);
		searcher.setDBSourceLayer(source);
		searcher.setQuery(queryStr);
		searcher.setQueryKQL(null);
		searcher.setNRSMD(getNRSMDLeaders());
		
		Mockito.doNothing().when(searcher).close();
		Mockito.doNothing().when(source).connect();		
		when(source.getBatchRecords(any())).thenReturn(getResponseLeaders());
					
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertEquals(3, tuple.stringFields.length);
		}
		searcher.close();		
	}
	
}
