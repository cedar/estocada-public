package fr.inria.cedar.commons.tatooine.operators.logical;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class LogProjectionTest extends BaseTest {

	@Test
	// Test the projection of simple tuples
	public void testProjectionSimpleTuples() throws TatooineExecutionException {
		// "P01.txt" contains simple tuples
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p01.txt");
		
		// The original tuple has 7 columns
		assertEquals(7, scan.getNRSMD().colNo);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan.getNRSMD().types[1]);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan.getNRSMD().types[2]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan.getNRSMD().types[3]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan.getNRSMD().types[4]);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan.getNRSMD().types[5]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan.getNRSMD().types[6]);
		
		// Project only the first three columns
		int[] columns = new int[] { 0, 1, 2 };		
		
		// Creates a projection logical operator
		LogProjection projection = new LogProjection(scan, columns);	
		
		// The new tuple will have 3 columns
		assertEquals(3, projection.getNRSMD().colNo);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan.getNRSMD().types[1]);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan.getNRSMD().types[2]);
	}
	
	@Test
	// Test the projection of nested tuples
	public void testProjectionNestedTuples() throws TatooineExecutionException {
		// "P02.txt" contains nested tuples
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p02.txt");
		
		// The original tuple has 18 columns
		assertEquals(18, scan.getNRSMD().colNo);
		assertEquals(TupleMetadataType.TUPLE_TYPE, scan.getNRSMD().types[2]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, scan.getNRSMD().types[13]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, scan.getNRSMD().types[14]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, scan.getNRSMD().types[15]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, scan.getNRSMD().types[16]);
		
		// Project only columns with nested data types
		int[] columns = new int[] { 2, 13, 14, 15, 16 };		
		
		// Creates a projection logical operator
		LogProjection proj = new LogProjection(scan, columns);	
		
		// The new tuple will have 5 columns
		assertEquals(5, proj.getNRSMD().colNo);
		assertEquals(TupleMetadataType.TUPLE_TYPE, proj.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, proj.getNRSMD().types[1]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, proj.getNRSMD().types[2]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, proj.getNRSMD().types[3]);
		assertEquals(TupleMetadataType.TUPLE_TYPE, proj.getNRSMD().types[4]);
	}
	
}
