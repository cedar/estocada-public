package fr.inria.cedar.commons.tatooine.loader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import fr.inria.cedar.commons.tatooine.Parameters;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

public class CatalogTest extends BaseTest {

	// NOTE: configure the value of solrConfigSets in tatooine.conf to run this test properly
	protected static final String SOLR_CONFIG_SETS = Parameters.getProperty("solrConfigSets");

	protected StorageReference getSolrStorageReference(String collectionName) throws Exception {
		StorageReference gsr = new GSR(collectionName);
		gsr.setProperty("serverUrl", SolrDatabaseLoader.DEFAULT_SOLR_URL);
		gsr.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);
		return gsr;
	}

	@Test
	@Ignore
	// Run this test whenever you need to clear out all values stored in the catalog
	public void reset() throws TatooineExecutionException {
		Catalog catalog = Catalog.getInstance();
		catalog.delete();
	}

	@Test
	/** 
	 * Adds connection information and metadata of a SOLR view to the catalog.
	 * This view represents a locally-hosted SOLR core/collection named "SocialMediaSOLR" that contains tweets and FB posts.
	 * The format of such documents is specified in (see @PhySOLRScanTest) 
	 */
	public void addSocialMediaSOLR() throws Exception {
		String collectionName = "SocialMediaSOLR";
		StorageReference storageReference = getSolrStorageReference(collectionName);
		String jsonDataFile = path + "/json/SocialMediaSOLR.json";

		DatabaseLoader databaseLoader = new SolrDatabaseLoader(SOLR_CONFIG_SETS, storageReference);
		String dataCollectionName = databaseLoader.getCatalogEntryName();
		// load the collection if it's not in SOLR yet
		if (!databaseLoader.isDataLoaded(dataCollectionName)) {
			// load the data into SOLR and verify the loading process is successful
			databaseLoader.load(jsonDataFile);
			databaseLoader.registerNewCatalogEntry();
			assertTrue(databaseLoader.isDataLoaded(dataCollectionName));
		}

		Catalog catalog = Catalog.getInstance();
		assertEquals(10, catalog.getViewSchema(collectionName).getNRSMD().colNo);
	}

	@Test
	/** 
	 * Adds metadata and connection info of a SOLR view to the catalog.
	 * This view represents a locally-hosted SOLR core/collection named "BookSOLR" that contains info on books.
	 * The format of such documents is specified in (see @PhySOLRScanTest) 
	 */
	public void addBookSOLR() throws Exception {
		Catalog catalog = Catalog.getInstance();
		
		NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "format" }, new NRSMD[0]);
		NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { "rating" }, new NRSMD[0]);

		if (!catalog.contains("BookSOLR")) {
			NRSMD nrsmd = new NRSMD(9, new TupleMetadataType[] {
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE }, new String[] { "id", "name_s", 
					"author_s", "series_s", "sequence_i", "pages_i", "formats", 
					"price_i", "ratings" }, new NRSMD[] { child1, child2 });
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("name_s", Integer.valueOf(1))
					.put("author_s", Integer.valueOf(2))
					.put("series_s", Integer.valueOf(3))
					.put("sequence_i", Integer.valueOf(4))
					.put("pages_i", Integer.valueOf(5))
					.put("formats", Integer.valueOf(6))
					.put("price_i", Integer.valueOf(7))
					.put("ratings", Integer.valueOf(8)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("BookSOLR");
			gsr.setProperty("serverUrl", "http://localhost");
			gsr.setProperty("serverPort", "8983");
			gsr.setProperty("coreName", "BookSOLR");

			catalog.add("BookSOLR", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view BookSOLR
		assertTrue(catalog.contains("BookSOLR"));

		// Assert the schema of BookSOLR contains 9 columns
		assertEquals(9, catalog.getViewSchema("BookSOLR").getNRSMD().colNo);
	}
	
	@Test
	public void addNamesOfPoliticiansJENA() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("NamesOfPoliticiansJENA")) {
			NRSMD nrsmd = new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
					new String[] { "name_s", "surname_s" }, new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("name_s", Integer.valueOf(0))
					.put("surname_s", Integer.valueOf(1)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("NamesOfPoliticiansJENA");
			gsr.setProperty("modelRDF", path + "/rdf/model.ttl");	
			catalog.add("NamesOfPoliticiansJENA", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view NamesOfPoliticiansJENA
		assertTrue(catalog.contains("NamesOfPoliticiansJENA"));

		// Assert the schema of NamesOfPoliticiansJENA contains 2 columns
		assertEquals(2, catalog.getViewSchema("NamesOfPoliticiansJENA").getNRSMD().colNo);
	}
	
	@Test
	public void addNamesAndCurrents() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("NamesAndCurrentsJENA")) {
			NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
					new String[] { "name_s", "surname_s", "current_s" }, new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("name_s", Integer.valueOf(0))
					.put("surname_s", Integer.valueOf(1))
					.put("current_s", Integer.valueOf(2)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("NamesAndCurrentsJENA");
			gsr.setProperty("modelRDF", path + "/rdf/model.ttl");	
			catalog.add("NamesAndCurrentsJENA", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view NamesAndCurrentsJENA
		assertTrue(catalog.contains("NamesAndCurrentsJENA"));

		// Assert the schema of NamesAndCurrentsJENA contains 3 columns
		assertEquals(3, catalog.getViewSchema("NamesAndCurrentsJENA").getNRSMD().colNo);
	}
	
	@Test
	public void addLeMondeArticlesSOLR() throws Exception {
		Catalog catalog = Catalog.getInstance();
		
		if (!catalog.contains("LeMondeArticlesSOLR")) {
			NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
					new String[] { "id", "processed_text_s", "word_to_stem_s" }, new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("processed_text_s", Integer.valueOf(1))
					.put("word_to_stem_s", Integer.valueOf(2)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("LeMondeArticlesSOLR");
			gsr.setProperty("serverUrl", "http://localhost");
			gsr.setProperty("serverPort", "8983");
			gsr.setProperty("coreName", "LeMondeArticlesSOLR");

			catalog.add("LeMondeArticlesSOLR", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view LeMondeArticlesSOLR
		assertTrue(catalog.contains("LeMondeArticlesSOLR"));

		// Assert the schema of LeMondeArticlesSOLR contains 10 columns
		assertEquals(3, catalog.getViewSchema("LeMondeArticlesSOLR").getNRSMD().colNo);
	}
	
	@Test
	public void addSPARQL() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("BudgetJENA")) {
			NRSMD nrsmd = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, 
					new String[] { "budget" }, new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("budget", Integer.valueOf(0)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("BudgetJENA");
			gsr.setProperty("modelRDF", "datasets/model.ttl");	
			catalog.add("BudgetJENA", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view BudgetJENA
		assertTrue(catalog.contains("BudgetJENA"));

		// Assert the schema of BudgetJENA contains 3 columns
		assertEquals(1, catalog.getViewSchema("BudgetJENA").getNRSMD().colNo);
	}
	
	@Test
	public void addTwitterIDsJENA() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("TwitterIDsJENA")) {
			NRSMD nrsmd = new NRSMD(3, 
					new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
					new String[] { "user_screen_name_s", "name_s", "surname_s" }, 
					new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("user_screen_name_s", Integer.valueOf(0))
					.put("name_s", Integer.valueOf(1))
					.put("surname_s", Integer.valueOf(2)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("TwitterIDsJENA");
			gsr.setProperty("modelRDF", path + "/rdf/model.ttl");	
			catalog.add("TwitterIDsJENA", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view TwitterIDsJENA
		assertTrue(catalog.contains("TwitterIDsJENA"));

		// Assert the schema of TwitterIDsJENA contains 3 columns
		assertEquals(3, catalog.getViewSchema("TwitterIDsJENA").getNRSMD().colNo);
	}
	
	@Test
	public void addStudentsSQLScan() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("StudentsSQLScan")) {
			NRSMD nrsmd = new NRSMD(2, 
				new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE },
				new String[] { "id", "name" }, new NRSMD[0] );
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("name", Integer.valueOf(1)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("StudentsSQLScan");	
			gsr.setProperty("url", "jdbc:derby:memory:testDB;create=true;user=scantest;password=scantest");
			catalog.add("StudentsSQLScan", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view StudentsSQL
		assertTrue(catalog.contains("StudentsSQLScan"));

		// Assert the schema of StudentsSQL contains 2 columns
		assertEquals(2, catalog.getViewSchema("StudentsSQLScan").getNRSMD().colNo);
	}
	
	@Test
	public void addStudentsSQLEval() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("StudentsSQLEval")) {
			NRSMD nrsmd = new NRSMD(2, 
				new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE },
				new String[] { "id", "name" }, new NRSMD[0] );
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("name", Integer.valueOf(1)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("StudentsSQLEval");	
			gsr.setProperty("url", "jdbc:derby:memory:testDB;create=true;user=evaltest;password=evaltest");
			catalog.add("StudentsSQLEval", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view StudentsSQL
		assertTrue(catalog.contains("StudentsSQLEval"));

		// Assert the schema of StudentsSQL contains 2 columns
		assertEquals(2, catalog.getViewSchema("StudentsSQLEval").getNRSMD().colNo);
	}
	
	@Test
	public void addPoliticiansSQL() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("PoliticiansSQL")) {
			NRSMD nrsmd = new NRSMD(4, 
				new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
				new String[] { "id", "name", "twitter", "dob" }, new NRSMD[0] );
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("name", Integer.valueOf(1))
					.put("twitter", Integer.valueOf(2))
					.put("dob", Integer.valueOf(3)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("PoliticiansSQL");	
			gsr.setProperty("url", "jdbc:derby:memory:testDB;create=true;user=unittest;password=unittest");
			catalog.add("PoliticiansSQL", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view PoliticiansSQL
		assertTrue(catalog.contains("PoliticiansSQL"));

		// Assert the schema of PoliticiansSQL contains 4 columns
		assertEquals(4, catalog.getViewSchema("PoliticiansSQL").getNRSMD().colNo);
	}
	
	@Test
	public void addTwitterIDsCurrentsJENA() throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains("TwitterIDsCurrentsJENA")) {
			NRSMD nrsmd = new NRSMD(1, 
					new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, 
					new String[] { "current_s" }, 
					new NRSMD[0]);
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("current_s", Integer.valueOf(0)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR("TwitterIDsCurrentsJENA");
			gsr.setProperty("modelRDF", path + "/rdf/model.ttl");	
			catalog.add("TwitterIDsCurrentsJENA", gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Assert the catalog contains an entry for the view TwitterIDsCurrentsJENA
		assertTrue(catalog.contains("TwitterIDsCurrentsJENA"));

		// Assert the schema of TwitterIDsCurrentsJENA contains 1 column
		assertEquals(1, catalog.getViewSchema("TwitterIDsCurrentsJENA").getNRSMD().colNo);
	}
	
	@Test
	public void addBooksSQL() throws Exception {
		Catalog catalog = Catalog.getInstance();

		NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "format" }, new NRSMD[0]);
		NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { "rating" }, new NRSMD[0]);

		if (!catalog.contains("BooksSQL")) {
			NRSMD nrsmd = new NRSMD(9, new TupleMetadataType[] {
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE }, new String[] { "id", "name_s", 
					"author_s", "series_s", "sequence_i", "pages_i", "formats", 
					"price_i", "ratings" }, new NRSMD[] { child1, child2 });
			Map<String, Integer> colNameToIndexMap = ImmutableMap
					.<String, Integer> builder()
					.put("id", Integer.valueOf(0))
					.put("name_s", Integer.valueOf(1))
					.put("author_s", Integer.valueOf(2))
					.put("series_s", Integer.valueOf(3))
					.put("sequence_i", Integer.valueOf(4))
					.put("pages_i", Integer.valueOf(5))
					.put("formats", Integer.valueOf(6))
					.put("price_i", Integer.valueOf(7))
					.put("ratings", Integer.valueOf(8)).build();
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);
			
			StorageReference gsr = new GSR("BooksSQL");
			gsr.setProperty("table", "books");
			gsr.setProperty("frame", path + "/json/books.json");
			catalog.add("BooksSQL", gsr, new ViewSizeBundle(5, 5), schema);
		}
		
		// Assert the catalog contains an entry for the view BooksSQL
		assertTrue(catalog.contains("BooksSQL"));

		// Assert the schema of BooksSQL contains 9 columns
		assertEquals(9, catalog.getViewSchema("BooksSQL").getNRSMD().colNo);
	}
}
