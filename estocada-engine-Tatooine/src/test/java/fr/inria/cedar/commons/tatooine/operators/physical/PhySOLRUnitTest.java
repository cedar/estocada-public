package fr.inria.cedar.commons.tatooine.operators.physical;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.util.NamedList;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class PhySOLRUnitTest extends BaseTest {
	
	protected NRSMD getNRSMDBooks() throws TatooineExecutionException {
		// The columns of the JSON file are: "author_s", "formats", "id", "name_s", "pages_i", "price_i", "ratings", "sequence_i", "series_s"
		// The fields "ratings" and "format" are nested		
		NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "format" }, new NRSMD[0]);
		NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { "rating" }, new NRSMD[0]);
		NRSMD nrsmd = new NRSMD(9, 
				new TupleMetadataType[] {
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE }, 
				new String[] { "id", "name_s", "author_s", "series_s", "sequence_i", "pages_i", "formats", "price_i", "ratings" }, 
				new NRSMD[] { child1, child2 });
		return nrsmd;
	}
	
	protected QueryResponse getQueryResponseBooks() throws TatooineExecutionException, NumberFormatException, IOException {
		QueryResponse response = new QueryResponse();
				
		NamedList<Object> results = new NamedList<Object>();		
		BufferedReader br = new BufferedReader(new FileReader(path + "/mock/BookSOLR.txt"));
		String line;
		SolrDocumentList list = new SolrDocumentList();
		while ((line = br.readLine()) != null) {
			if (line.startsWith("#")) {
				continue;
			}
			SolrDocument doc = new SolrDocument();
			String[] tokens = line.trim().split("\\|");
			doc.setField("id", tokens[0]);
			doc.setField("formats", new ArrayList<String>(Arrays.asList(tokens[1].split(","))));
			doc.setField("name_s", tokens[2]);
			doc.setField("author_s", tokens[3]);
			doc.setField("series_s", tokens[4]);
			doc.setField("sequence_i", Integer.parseInt(tokens[5]));
			doc.setField("price_i", Integer.parseInt(tokens[6]));
			doc.setField("pages_i", Integer.parseInt(tokens[7]));			
			String[] ratingsStr = tokens[8].split(",");
			List<Integer> ratingsInt = new ArrayList<Integer>();
			for (String rating : ratingsStr) {
				ratingsInt.add(Integer.parseInt(rating));
			}			
			doc.setField("ratings", ratingsInt);
			doc.setField("_version_", tokens[9]);
			list.add(doc);
        }
		list.setStart(0);
		list.setNumFound(8);
		br.close();
		
		results.add("response", list);					
		response.setResponse(results);	
		
		return response;
	}

}
