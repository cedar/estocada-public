package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import ucsd.edu.sparkAQL.runtime.SparkSQLConnection;

public class PhySparkAQLEvalTest extends BaseTest {

    @Test
    public void testSingle() throws TatooineExecutionException, TatooineException {
        GSR gsr = new GSR("AQL");
        gsr.setProperty("tableName", "persons");
        Dataset<Row> df = SparkSQLConnection.sqlContext.read().json(path + "/AQLTest/test02/persons.json");
        df.registerTempTable("persons");
        String queryString = "";
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(path + "/AQLTest/test02/query"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            queryString = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PhySparkAQLEval aqlEval = new PhySparkAQLEval(queryString,
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }), gsr);
        aqlEval.open();

        while (aqlEval.hasNext()) {
            NTuple tuple = aqlEval.next();
            assertNotNull(tuple);
        }
        aqlEval.close();
    }

    @Test
    public void testNested() throws TatooineExecutionException, TatooineException {
        System.out.println("PATH: " + path);

        GSR gsr = new GSR("AQL");
        gsr.setProperty("tableName", "persons");
        Dataset<Row> df = SparkSQLConnection.sqlContext.read().json(path + "/AQLTest/test01/persons.json");
        df.registerTempTable("persons");
        String queryString = "";
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(path + "/AQLTest/test01/query"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            queryString = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PhySparkAQLEval aqlEval = new PhySparkAQLEval(queryString, new NRSMD(1,
                new TupleMetadataType[] { TupleMetadataType.TUPLE_TYPE }, new String[] { "person" },
                new NRSMD[] { new NRSMD(2,
                        new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
                        new String[] { "ssn", "name" }, new NRSMD[0]) }),
                gsr);
        aqlEval.open();

        while (aqlEval.hasNext()) {
            NTuple tuple = aqlEval.next();
            assertNotNull(tuple);
        }
        aqlEval.close();
    }
}
