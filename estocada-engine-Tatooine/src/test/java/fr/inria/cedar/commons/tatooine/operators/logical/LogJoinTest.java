package fr.inria.cedar.commons.tatooine.operators.logical;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

public class LogJoinTest extends BaseTest {

	@Test
	// Test the creation of a join between two tables
	public void testJoin() throws TatooineExecutionException {
		// Assert metadata type of 1st table
		LogTupleReaderScan scan1 = new LogTupleReaderScan(path + "/txt/p08.txt");		
		assertEquals(TupleMetadataType.ORDERED_ID, scan1.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan1.getNRSMD().types[1]);		
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan1.getNRSMD().types[2]);
		assertEquals(TupleMetadataType.INTEGER_TYPE, scan1.getNRSMD().types[3]);
		assertEquals(TupleMetadataType.ORDERED_ID, scan1.getNRSMD().types[4]);
		
		// Assert metadata type of 2nd table
		LogTupleReaderScan scan2 = new LogTupleReaderScan(path + "/txt/p09.txt");
		assertEquals(TupleMetadataType.ORDERED_ID, scan2.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.STRING_TYPE, scan2.getNRSMD().types[1]);
		
		// Creates a join logical operator
		LogJoin join = new LogJoin(scan1, scan2, new SimplePredicate(4, 5));
		
		// Assert metadata of the join of both tables
		assertEquals(TupleMetadataType.ORDERED_ID, join.getNRSMD().types[0]);
		assertEquals(TupleMetadataType.STRING_TYPE, join.getNRSMD().types[1]);		
		assertEquals(TupleMetadataType.INTEGER_TYPE, join.getNRSMD().types[2]);
		assertEquals(TupleMetadataType.INTEGER_TYPE, join.getNRSMD().types[3]);
		assertEquals(TupleMetadataType.ORDERED_ID, join.getNRSMD().types[4]);		
		assertEquals(TupleMetadataType.ORDERED_ID, join.getNRSMD().types[5]);
		assertEquals(TupleMetadataType.STRING_TYPE, join.getNRSMD().types[6]);
	}	
}
