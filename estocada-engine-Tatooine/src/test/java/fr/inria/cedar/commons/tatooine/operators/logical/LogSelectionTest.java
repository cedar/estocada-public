package fr.inria.cedar.commons.tatooine.operators.logical;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.constants.PredicateDataType;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

public class LogSelectionTest extends BaseTest {
	
	@Test
	// Test the selection of a string using a simple predicate
	public void testSelectionSimplePredicateStrings() throws TatooineExecutionException {
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p02.txt");
		
		// We want tuples whose column 7 is the string "ab" 
		SimplePredicate pred = new SimplePredicate("ab", 7);
		
		// Creates a selection logical operator
		LogSelection selection = new LogSelection(scan, pred);

		// The first four tuples of our TXT file meet the condition of the predicate
		// But the last tuple doesn't
		assertTrue(selection.pred.isTrue(scan.tuples.get(0)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(1)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(2)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(3)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(4)));
	}
	
	@Test
	// Test the selection of an integer using a simple predicate
	public void testSelectionSimplePredicateIntegers() throws TatooineExecutionException {
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p08.txt");
		
		// We want tuples whose column 2 is an integer greater than 1985
		SimplePredicate pred = new SimplePredicate(2, 1985, PredicateType.PREDICATE_GREATER_THAN, PredicateDataType.PREDICATE_ON_INTEGER);
		
		// Creates a selection logical operator
		LogSelection selection = new LogSelection(scan, pred);
		
		// Some tuples of our TXT file meet the condition of the predicate and some do not
		assertFalse(selection.pred.isTrue(scan.tuples.get(0)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(1)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(2)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(3)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(4)));
	}
	
	@Test
	// Test the selection of two columns with equal values
	public void testSelectionSimplePredicateColumns() throws TatooineExecutionException {
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p07.txt");
		
		// We want tuples whose columns 2 and 3 have the same value
		SimplePredicate pred = new SimplePredicate(2, 3, PredicateType.PREDICATE_EQUAL);
		
		// Creates a selection logical operator
		LogSelection selection = new LogSelection(scan, pred);
		
		// Columns 2 and 3 of the 1st and 3rd tuples have the same value
		assertTrue(selection.pred.isTrue(scan.tuples.get(0)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(1)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(2)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(3)));
	}
	
	@Test
	// Test a selection that uses a conjunctive predicate
	public void testSelectionConjunctivePredicate() throws TatooineExecutionException {
		LogTupleReaderScan scan = new LogTupleReaderScan(path + "/txt/p07.txt");
		
		// We want tuples whose columns 2 and 3 have different values
		// And whose columns 2 and 5 have the same value
		SimplePredicate pred1 = new SimplePredicate(2, 3, PredicateType.PREDICATE_NOT_EQUAL);
		SimplePredicate pred2 = new SimplePredicate(2, 5, PredicateType.PREDICATE_EQUAL);
		ConjunctivePredicate pred = new ConjunctivePredicate(pred1, pred2);
		
		// Creates a selection logical operator
		LogSelection selection = new LogSelection(scan, pred);
		
		// Only the last tuple meets the conditions of our conjunctive predicate
		assertFalse(selection.pred.isTrue(scan.tuples.get(0)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(1)));
		assertFalse(selection.pred.isTrue(scan.tuples.get(2)));
		assertTrue(selection.pred.isTrue(scan.tuples.get(3)));
	}
}
