package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Assert;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.AggregateFunction;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * This test class requires setting up a local SOLR collection (with the name "twitterdump") that stores tweets retrieved from the Twitter API (in JSON format).
 * It should work with a remote connection as well if the params "serverURL" and "serverPort" are specified in the GSR (see @PhySOLREval).
 * Here's the format of one such document (tweet):
 * { 
 *   "id":"734504175250333697",
 *   "user_name":["Dominique Bouissou"],
 *   "user_screen_name":["dombouissou"],
 *   "text":["#Irak bombardements continus sur #Falloujah"],
 *   "created_at":["Sun May 22 22:00:16 +0000 2016"],
 *	 "hashtags.indices":[13, 18, 26],
 *   "hashtags.text":["Irak", "Falloujah", "EI"],   
 *	 "retweeted":[true],
 *   "retweet_count":[502],
 *   "_version_":1535499256898519040
 * }        
 * @author Oscar Mendoza
 */
// FIXME
public class PhyGroupByTest extends BaseTest {

//	/** Pre-processing: Setting up a SOLR connection 
//	 * @throws IOException 
//	 * @throws SolrServerException 
//	 * @throws TatooineExecutionException */
//	private PhySOLRScan setupSOLRConnection() throws TatooineExecutionException, SolrServerException, IOException {
//		// Setting properties
//		GSR gsr = new GSR("twitter");
//		gsr.setProperty("coreName", "twitterdump");
//		
//		// This query returns 3 records:
//		// ([("GMothron")], [("TWEET1")], [("Sun May 22 22:02:23 +0000 2016")], [("true")], [(2)], [("Georges Mothron")], "734504707121680386")
//		// ([("GMothron")], [("TWEET2")], [("Sun May 22 22:02:34 +0000 2016")], [("true")], [(2)], [("Georges Mothron")], "734504752139096065")
//		// ([("GMothron")], [("TWEET3")], [("Mon May 23 10:58:52 +0000 2016")], [(97, 103)], [("Lycée")], [("true")], [(2)], [("Georges Mothron")], "734700117815623680")
//		
//		String query = "user_name:Georges Mothron";
//		return new PhySOLRScan(query, "", gsr, null);
//	}
//	
//	@Test
//	// Tests grouping NTuples by only one attribute
//	public void testGroupByOneAttribute() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();
//
//		// Let's group records by "user_name"
//		List<String> columns = Arrays.asList("user_name");	
//		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns);
//		operator.open();
//		
//		int counter = 0;
//		while (operator.hasNext()) {
//			NTuple tuple = operator.next();
//			assertNotNull(tuple.nrsmd);
//			
//			// Grouping records by "user_name" should yield results with 2 columns: 1 "user_name" column + 1 column for all grouped attributes
//			assertEquals(2, tuple.nrsmd.colNo);			
//						
//			// Assert name and types of the "user_name" column (our GROUP BY attribute)
//			assertEquals("user_name", tuple.nrsmd.colNames[0]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[0].get(0).nrsmd.types[0]);
//			
//			// Assert name and types of grouped attributes
//			assertEquals("group", tuple.nrsmd.colNames[1]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nrsmd.types[1]);
//			assertEquals("sub-group", tuple.nestedFields[1].get(0).nrsmd.colNames[0]);
//			assertEquals("sub-group", tuple.nestedFields[1].get(0).nrsmd.colNames[1]);
//			assertEquals("sub-group", tuple.nestedFields[1].get(0).nrsmd.colNames[2]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nestedFields[1].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nestedFields[1].get(0).nrsmd.types[1]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nestedFields[1].get(0).nrsmd.types[2]);
//			
//			assertEquals("user_screen_name", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[0]);
//			assertEquals("text", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[1]);
//			assertEquals("created_at", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[2]);
//			assertEquals("retweeted", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[3]);
//			assertEquals("retweet_count", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[4]);
//			assertEquals("id", tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.colNames[5]);
//			
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nestedFields[0].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nestedFields[1].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nestedFields[2].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nestedFields[3].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.INTEGER_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nestedFields[4].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nestedFields[0].get(0).nrsmd.types[5]);
//			
//			counter++;
//		}
//		
//		// Grouping records by "user_name" should yield only 1 record
//		assertEquals(1, counter);
//		
//		operator.close();
//		searcher.close();
//	}
//	
//	@Test
//	// Tests grouping NTuples by several attributes
//	public void testGroupBySeveralAttributes() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();			
//				
//		// Let's group records by "user_name", "user_screen_name", "retweeted", "retweet_count"
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		columns.add("user_screen_name");
//		columns.add("retweeted");
//		columns.add("retweet_count");
//		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns);
//		operator.open();
//		
//		int counter = 0;
//		while (operator.hasNext()) {
//			NTuple tuple = operator.next();
//			
//			// Grouping records by "user_name", "user_screen_name", "retweeted", and "retweet_count" should yield results with 5 columns: 
//			// 1 column for each GROUP BY attribute (there are 4 of them) + 1 column for all grouped attributes
//			assertEquals(5, tuple.nrsmd.colNo);
//						
//			// Assert name and types of fields
//			assertEquals("user_screen_name", tuple.nrsmd.colNames[0]);
//			assertEquals("retweeted", tuple.nrsmd.colNames[1]);
//			assertEquals("retweet_count", tuple.nrsmd.colNames[2]);
//			assertEquals("user_name", tuple.nrsmd.colNames[3]);
//			assertEquals("group", tuple.nrsmd.colNames[4]);			
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[0].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[1].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.INTEGER_TYPE, tuple.nestedFields[2].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.STRING_TYPE, tuple.nestedFields[3].get(0).nrsmd.types[0]);
//			assertEquals(TupleMetadataType.TUPLE_TYPE, tuple.nestedFields[4].get(0).nrsmd.types[0]);
//			
//			counter++;
//		}
//		
//		// Grouping records by "user_name", "user_screen_name", "retweeted", and "retweet_count" should yield only 1 record
//		assertEquals(1, counter);
//		
//		operator.close();
//		searcher.close();
//	}
//		
//	@Test
//	// Tests grouping NTuples by one attribute and specifying the aggregation function COUNT and an aggregation column
//	public void testGroupByCountWithAggregationColumn() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();						 
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		String column = "retweet_count";
//		Aggregator aggregator = new Aggregator(AggregateFunction.COUNT, column);		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns, aggregator);
//		operator.open();		
//		// Our COUNT function should return the value "3" because we have 3 tuples
//		testCountAggregatedColumns(operator, 3);		
//		operator.close();
//		searcher.close();
//	}
//	
//	@Test
//	// Tests grouping NTuples by one attribute and specifying the aggregation function COUNT and NO aggregation column
//	public void testGroupByCountWithNoAggregationColumn() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();						 
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		Aggregator aggregator = new Aggregator(AggregateFunction.COUNT, null);		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns, aggregator);
//		operator.open();		
//		// Our COUNT function should return the value "3" because we have 3 tuples
//		testCountAggregatedColumns(operator, 3);		
//		operator.close();
//		searcher.close();
//	}
//	
//	@Test
//	// Tests grouping NTuples by one attribute and specifying the aggregation function SUM
//	public void testGroupBySum() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();						 
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		String column = "retweet_count";
//		Aggregator aggregator = new Aggregator(AggregateFunction.SUM, column);		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns, aggregator);
//		operator.open();		
//		// Our SUM function should return the value "6" because we have 3 tuples and each one has a "retweet_count" value of "2"
//		testCountAggregatedColumns(operator, 6);		
//		operator.close();
//		searcher.close();
//	}
//	
//	@Test
//	// Tests grouping NTuples by one attribute and specifying the aggregation function AVG
//	public void testGroupByAvg() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();						 
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		String column = "retweet_count";
//		Aggregator aggregator = new Aggregator(AggregateFunction.AVG, column);		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns, aggregator);
//		operator.open();		
//		// Our AVG function should return the value "6" because we have 3 tuples and each one has a "retweet_count" value of "2"
//		testCountAggregatedColumns(operator, 2);		
//		operator.close();
//		searcher.close();
//	}
//	
//	@Test
//	// Tests grouping NTuples by one attribute and specifying an aggregation column that is not numeric
//	public void testGroupByNonNumericAggregationColumn() throws TatooineExecutionException, SolrServerException, IOException {
//		PhySOLRScan searcher = setupSOLRConnection();
//		searcher.open();						 
//		List<String> columns = new ArrayList<String>();
//		columns.add("user_name");
//		// "ID" is stored as a string in our SOLR collection
//		String column = "id";
//		Aggregator aggregator = new Aggregator(AggregateFunction.MAX, column);		
//		PhyGroupBy operator = new PhyGroupBy(searcher, columns, aggregator);
//			
//		// Our MAX function should fail because the column specified does not contain numeric values
//		try {
//			operator.open();
//		} catch (TatooineExecutionException e) {
//			operator.close();
//			searcher.close();
//			return;
//		}
//		
//		operator.close();
//		searcher.close();
//		Assert.fail();
//	}
//		
//	private void testCountAggregatedColumns(PhyGroupBy operator, int expectedAggregatedValue) throws TatooineExecutionException {
//		while (operator.hasNext()) {
//			NTuple tuple = operator.next();
//
//			// Grouping records by "user_name" and using the function COUNT should give us results with 3 columns: 
//			// 1 column for our grouping attribute ("user_name") + 1 column our aggregation function + 1 column for all grouped attributes
//			assertEquals(3, tuple.nrsmd.colNo);
//			
//			assertEquals("user_name", tuple.nrsmd.colNames[0]);
//			assertEquals("aggregator", tuple.nrsmd.colNames[1]);
//			assertEquals("group", tuple.nrsmd.colNames[2]);
//			
//			// Our COUNT function should return the value "3" because we have 3 tuples
//			assertEquals(expectedAggregatedValue, tuple.integerFields[0]);
//		}
//	}
}
