package fr.inria.cedar.commons.tatooine.loader;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

public class CatalogTest02 extends BaseTest {
	/*
	 * Test the creation of the Catalog for some current running Scenarios. This
	 * test class will be used by Rana, Damian and Romain
	 */
	@Test
	public void testCreationScenario01() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artistid", 0);
		mappingColumnsToIndexes01.put("name", 1);
		mappingColumnsToIndexes01.put("area", 2);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferencePostgreSQL("V2",
		        "jdbc:postgresql://localhost:5432/musicbrainz?user=postgress&password=postgress");
		ViewSchema viewSchema01 = new ViewSchema(new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
		        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }), mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V2", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V2"));

		// Mapping
		Map<String, Integer> mappingColumnsToIndexes02 = new HashMap<String, Integer>();
		mappingColumnsToIndexes02.put("artist_id", 0);
		mappingColumnsToIndexes02.put("track_name", 1);
		mappingColumnsToIndexes02.put("release_name", 2);
		mappingColumnsToIndexes02.put("medium_format", 3);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle02 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr02 = createStorageReferencePostgreSQL("V1",
		        "jdbc:postgresql://localhost:5432/musicbrainz?user=postgress&password=postgress");
		ViewSchema viewSchema02 = new ViewSchema(
		        new NRSMD(4,
		                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE,
		                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes02);
		// Adding the above information to the catalog
		catalog.add("V1", gsr02, viewSizeBundle02, viewSchema02);
		catalog.write();

		assertTrue(catalog.contains("V1"));

	}

	@Test
	public void testCreationScenario02() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artistid", 0);
		mappingColumnsToIndexes01.put("name", 1);
		mappingColumnsToIndexes01.put("area", 2);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferencePostgreSQL("V2",
		        "jdbc:postgresql://localhost:5432/musicbrainz?user=postgress&password=postgress");
		ViewSchema viewSchema01 = new ViewSchema(new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
		        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }), mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V2", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V2"));

		// Mapping
		Map<String, Integer> mappingColumnsToIndexes02 = new HashMap<String, Integer>();
		mappingColumnsToIndexes02.put("artist_id", 0);
		mappingColumnsToIndexes02.put("track_name", 1);
		mappingColumnsToIndexes02.put("release_name", 2);
		mappingColumnsToIndexes02.put("medium_format", 3);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle02 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr02 = createStorageReferenceSolr("V3", "v");
		ViewSchema viewSchema02 = new ViewSchema(
		        new NRSMD(4,
		                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE,
		                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes02);
		// Adding the above information to the catalog
		catalog.add("V3", gsr02, viewSizeBundle02, viewSchema02);
		catalog.write();

		assertTrue(catalog.contains("V3"));

	}

	@Test
	public void testCreationScenario3() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.load();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artist_id", 0);
		mappingColumnsToIndexes01.put("track_name", 1);
		mappingColumnsToIndexes01.put("release_name", 2);
		mappingColumnsToIndexes01.put("medium_format", 3);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceSolr("V3", "v");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(4,
		                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE,
		                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V3", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();

		assertTrue(catalog.contains("V3"));

		// Mapping
		Map<String, Integer> mappingColumnsToIndexes02 = new HashMap<String, Integer>();
		mappingColumnsToIndexes02.put("name", 0);
		mappingColumnsToIndexes02.put("area", 1);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle02 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr = createStorageReferenceRedis("V4", "v");
		ViewSchema viewSchema02 = new ViewSchema(
		        new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes02);
		// Adding the above information to the catalog
		catalog.add("V4", gsr, viewSizeBundle02, viewSchema02);
		catalog.write();
		assertTrue(catalog.contains("V4"));

	}

	public void testCreationScenario4() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.load();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artist_id", 0);
		mappingColumnsToIndexes01.put("track_name", 1);
		mappingColumnsToIndexes01.put("release_name", 2);
		mappingColumnsToIndexes01.put("medium_format", 3);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceSolr("V3", "v");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(4,
		                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE,
		                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V3", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();

		assertTrue(catalog.contains("V3"));

		// Mapping
		Map<String, Integer> mappingColumnsToIndexes02 = new HashMap<String, Integer>();
		mappingColumnsToIndexes02.put("name", 0);
		mappingColumnsToIndexes02.put("area", 1);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle02 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr02 = createStorageReferenceRedis("V4", "v");
		ViewSchema viewSchema02 = new ViewSchema(
		        new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes02);
		// Adding the above information to the catalog
		catalog.add("V4", gsr02, viewSizeBundle02, viewSchema02);
		catalog.write();
		assertTrue(catalog.contains("V4"));

		// Mapping
		Map<String, Integer> mappingColumnsToIndexes03 = new HashMap<String, Integer>();
		mappingColumnsToIndexes03.put("request", 0);
		mappingColumnsToIndexes03.put("status", 1);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle03 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr03 = createStorageReferenceSparkSQL("V5", "log");
		ViewSchema viewSchema03 = new ViewSchema(
		        new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes02);
		// Adding the above information to the catalog
		catalog.add("V4", gsr03, viewSizeBundle03, viewSchema03);
		catalog.write();
		assertTrue(catalog.contains("V4"));
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferenceSolr(String gsrName, String coreName) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("jsonPath", coreName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferenceRedis(String gsrName, String dbNumber) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("dbNumber", dbNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferenceSparkSQL(String gsrName, String jsonPath) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("jsonPath", jsonPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferencePostgreSQL(String gsrName, String url) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("url", url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}
}
