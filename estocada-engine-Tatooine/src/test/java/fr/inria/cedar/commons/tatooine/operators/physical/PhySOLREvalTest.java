package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;
import org.mockito.Mockito;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.SOLRSourceLayer;

public class PhySOLREvalTest extends PhySOLRUnitTest {
	 
	@Test	
	/** Queries for all information available on books stored as nested JSON documents in SOLR */
	public void testBooks() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		String queryStr = "*.*";
		String fieldStr = "name_s";
		NRSMD nrsmd = getNRSMDBooks();
		QueryResponse response = getQueryResponseBooks();
		
		PhySOLREval tmp = new PhySOLREval(queryStr,fieldStr, new GSR("SOLR"), nrsmd);
		tmp.close();
		
		PhySOLREval searcher = mock(PhySOLREval.class, Mockito.CALLS_REAL_METHODS);
		SOLRSourceLayer source = mock(SOLRSourceLayer.class);
		searcher.setDBSourceLayer(source);
		searcher.setQuery(queryStr);
		searcher.setField(fieldStr);
		searcher.setNRSMD(tmp.getNRSMD());
		
		Mockito.doNothing().when(searcher).close();
		Mockito.doNothing().when(source).connect();
		when(source.getBatchRecords(new String[] {queryStr, fieldStr})).thenReturn(response, new QueryResponse());
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
			assertEquals(1, tuple.nrsmd.colNo);
			assertEquals("name_s", tuple.nrsmd.colNames[0]);
		}
		searcher.close();
	}

}
