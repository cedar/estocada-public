package fr.inria.cedar.commons.tatooine.operators.physical.join;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import fr.inria.cedar.commons.tatooine.loader.CatalogTest;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyJENAEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLRScan;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import fr.inria.cedar.commons.tatooine.utilities.Utilities;
import fr.inria.cedar.commons.tatooine.utilities.Utilities.CountingBindAccess;
import fr.inria.cedar.commons.tatooine.utilities.Utilities.NTupleReaderIterator;

public class PhyBindJoinTest extends CatalogTest {

	@Test
	// Tests if BindJoin respects the NRSMD and perform correctly the join
	public void testBindJoin() throws TatooineExecutionException {

		String testFilePath = path + "/txt/p09.txt";
		int testFileColNo = 2;

		NIterator left = new NTupleReaderIterator(testFilePath); // Which has 2 column
		BindAccess right = new CountingBindAccess();
		Predicate pred = new SimplePredicate(0, testFileColNo);

		PhyBindJoin join = new PhyBindJoin(left, right, pred);

		join.open();

		List<NTuple> leftList = Utilities.listFromNIterator(left.copy());
		List<NTuple> joinList = Utilities.listFromNIterator(join);

		// We get one element per binding
		assertEquals(leftList.size(), joinList.size());

		// The join does join the NRSMD
		assertEquals(left.nrsmd.colNo + 1, join.nrsmd.colNo);
		assertEquals(TupleMetadataType.INTEGER_TYPE, join.nrsmd.types[testFileColNo]);
		assertEquals(NRSMD.appendNRSMD(left.nrsmd, right.nrsmd), join.nrsmd);

		// All the elements have the same, correct, NRSMD
		for (int i = 0; i < joinList.size(); i++) {
			assertEquals(join.nrsmd, joinList.get(i).nrsmd);
		}

		// Check that the join is correct on the line 0, 2 and 5
		assertEquals(new OrderedIntegerElementID(33890), joinList.get(0).getIDField(0));
		assertEquals("Stephen King".length(), joinList.get(0).getStringField(1).length);
		assertEquals(0, joinList.get(0).getIntegerField(2));

		assertEquals(new OrderedIntegerElementID(61797), joinList.get(2).getIDField(0));
		assertEquals("René Barjavel".length(), joinList.get(2).getStringField(1).length);
		assertEquals(2, joinList.get(2).getIntegerField(2));

		assertEquals(new OrderedIntegerElementID(22385), joinList.get(5).getIDField(0));
		assertEquals("Isabel Allende".length(), joinList.get(5).getStringField(1).length);
		assertEquals(5, joinList.get(5).getIntegerField(2));

		join.close();
	}
	
	@Test
	// Tests a bind join between a data source stored in SOLR (left side) and another one stored in JENA (right side, parameterised).
	public void testBindJoinSOLRJENA() throws Exception {
		// Asserts there is an entry for our test views in the catalog
		addSocialMediaSOLR();

		Catalog catalog = Catalog.getInstance();
		assertTrue(catalog.contains("SocialMediaSOLR"));
		assertTrue(catalog.contains("NamesAndCurrentsJENA"));
		
		// Retrieve the metadata of our test views from the catalog
		ViewSchema schemaSOLR = catalog.getViewSchema("SocialMediaSOLR");
		GSR gsrSOLR = (GSR) catalog.getStorageReference("SocialMediaSOLR");
		ViewSchema schemaJENA = catalog.getViewSchema("NamesAndCurrentsJENA");
		GSR gsrJENA = (GSR) catalog.getStorageReference("NamesAndCurrentsJENA");
	
		// Setting the SOLR query
		// Find tweets and Facebook posts that mention Manuel Valls
		String querySOLR = "Manuel Valls";
		PhySOLRScan searcherSOLR = new PhySOLRScan(querySOLR, "", gsrSOLR, schemaSOLR);
		
		/* SOLR collection: SocialMediaSOLR */
		// Columns are: [comments_i, created_at_s, id, likes_i, retweeted_s, shares_i, text_s, type_s, user_name_s, user_screen_name_s]

		// Setting the JENA query
		// We want to retrieve 3 fields from our RDF file for a given politician: name, surname, and political current
		String queryJENA = "PREFIX fm: <http://example.org/schemas/> SELECT DISTINCT ?current_s ?name_s ?surname_s "
				+ "WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:party ?par . ?par fm:current ?cur . ?cur fm:abbreviation ?current_s . "
				+ "FILTER regex(\"%s\", ?name_s) . FILTER regex(\"%s\", ?surname_s) . }";
		PhyJENAEval searcherJENA = new PhyJENAEval(queryJENA, gsrJENA, schemaJENA);

		// Find index in NRSMD for "user_name_s"
		int index = 8;

		// Our JOIN will retrieve tweets and FB posts that mention Manuel Valls
		// and enrich them with the political currents supported by each author (a French politician)
		PhyBindJoin join = new PhyBindJoin(searcherSOLR, searcherJENA, new int[] { index, index });
		
		join.open();	
		assertTrue(join.hasNext());
		while (join.hasNext()) {			
			NTuple next = join.next();
			assertNotNull(next);
		}
		join.close();
	}

	@Test
	// Tests a bind join between a data source stored in JENA (left side) and another one stored in SOLR (right side, parameterised).
	public void testBindJoinJENASOLR() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		// Asserts there is an entry for our test views in the catalog
		Catalog catalog = Catalog.getInstance();
		assertTrue(catalog.contains("SocialMediaSOLR"));
		assertTrue(catalog.contains("NamesOfPoliticiansJENA"));
		
		// Retrieve the metadata of our test views from the catalog
		ViewSchema schemaSOLR = catalog.getViewSchema("SocialMediaSOLR");
		GSR gsrSOLR = (GSR) catalog.getStorageReference("SocialMediaSOLR");
		ViewSchema schemaJENA = catalog.getViewSchema("NamesOfPoliticiansJENA");
		GSR gsrJENA = (GSR) catalog.getStorageReference("NamesOfPoliticiansJENA");
		
		// Find full names of politicians that belong to the Parti Socialiste
		String queryJENA = "PREFIX fm: <http://example.org/schemas/> SELECT DISTINCT ?name_s ?surname_s WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:party fm:PAR00070 . }";
		PhyJENAEval searcherJENA = new PhyJENAEval(queryJENA, gsrJENA, schemaJENA);
		
		// Setting the SOLR query
		/* SOLR collection: SocialMediaSOLR */
		String querySOLR = "user_name_s:%s*%s";
		PhySOLRScan searcherSOLR = new PhySOLRScan(querySOLR, "", gsrSOLR, schemaSOLR);			
		
		// Positions 0 and 1 are for "name_s" and "surname_s"
		// Our JOIN will retrieve all tweets and FB posts made by Parti Socialiste politicians
		PhyBindJoin join = new PhyBindJoin(searcherJENA, searcherSOLR, new int[] { 0, 1 });
		join.open();
		assertTrue(join.hasNext());
		while (join.hasNext()) {
			NTuple next = join.next();
			assertNotNull(next);
		}
		join.close();
	}

}
