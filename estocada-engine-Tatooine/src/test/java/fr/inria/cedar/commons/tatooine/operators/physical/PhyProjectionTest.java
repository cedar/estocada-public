package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;

public class PhyProjectionTest extends BaseTest {

	/**
	 * Creates a string representation of an NTuple for visualisation purposes.
	 * @param tuples
	 * @throws TatooineExecutionException
	 */
	private void display(ArrayList<NTuple> tuples) throws TatooineExecutionException {
		Iterator<NTuple> it = tuples.iterator();
		while (it.hasNext()) {
			NTuple tuple = it.next();
			tuple.display();
		}
	}

	@Test
	// Test projection of simple (non-nested) tuples
	public void testProjectionSimpleTuples() throws TatooineExecutionException {
		// "P01.txt" contains 2 simple (non-nested) tuples
		NTupleReader reader = new NTupleReader(path + "/txt/p01.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Project only the first two columns
		int[] columns1 = new int[] { 0, 1 };
		// Project only the third column
		int[] columns2 = new int[] { 2 };
		// Project the second, fourth and seventh column
		int[] columns3 = new int[] { 1, 3, 6 };

		for (NTuple tuple : tuples) {
			// Assert number of fields of original tuple
			assertEquals(4, tuple.stringFields.length);
			assertEquals(3, tuple.integerFields.length);

			// Assert number of fields of 1st projection (columns 0, 1)
			NTuple tuple1 = NTuple.project(tuple, NRSMD.makeProjectRSMD(tuple.nrsmd, columns1), columns1);
			assertEquals(1, tuple1.stringFields.length);
			assertEquals(1, tuple1.integerFields.length);

			// Assert number of fields of 2nd projection (column 2)
			NTuple tuple2 = NTuple.project(tuple, NRSMD.makeProjectRSMD(tuple.nrsmd, columns2), columns2);
			assertEquals(0, tuple2.stringFields.length);
			assertEquals(1, tuple2.integerFields.length);

			// Assert number of fields of 3rd projection (columns 2, 4, 7)
			NTuple tuple3 = NTuple.project(tuple, NRSMD.makeProjectRSMD(tuple.nrsmd, columns3), columns3);
			assertEquals(3, tuple3.stringFields.length);
			assertEquals(0, tuple3.integerFields.length);
		}
	}

	@Test
	// Test projection of simply nested tuples
	public void testProjectionSimplyNestedTuples() throws TatooineExecutionException {
		// "P02.txt" contains 5 simply-nested tuples
		NTupleReader reader = new NTupleReader(path + "/txt/p02.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Project columns 2, 5, 11, 12, 16
		int[] columns1 = new int[] { 2, 5, 11, 12, 16 };
		// Project columns 10, 13, 14, 15, 17
		int[] columns2 = new int[] { 10, 13, 14, 15, 17 };

		for (NTuple tuple : tuples) {
			// Assert number of fields of original tuple
			assertEquals(4, tuple.stringFields.length);
			assertEquals(9, tuple.integerFields.length);

			// Assert number of fields of 1st projection
			NTuple tuple1 = NTuple.project(tuple, NRSMD.makeProjectRSMD(tuple.nrsmd, columns1), columns1);
			assertEquals(2, tuple1.nestedFields.length);
			assertEquals(2, tuple1.stringFields.length);
			assertEquals(1, tuple1.integerFields.length);

			// Assert number of fields of 2nd projection
			NTuple tuple2 = NTuple.project(tuple, NRSMD.makeProjectRSMD(tuple.nrsmd, columns2), columns2);
			assertEquals(3, tuple2.nestedFields.length);
			assertEquals(1, tuple2.stringFields.length);
			assertEquals(1, tuple2.integerFields.length);
		}
	}
}
