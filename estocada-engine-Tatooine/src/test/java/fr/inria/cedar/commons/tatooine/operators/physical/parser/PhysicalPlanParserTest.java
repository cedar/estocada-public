package fr.inria.cedar.commons.tatooine.operators.physical.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.CatalogTest;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/** Tests the creation of operators from physical plan files and view entries in the catalog */
public class PhysicalPlanParserTest extends CatalogTest {

	private static final String SQL_DB_URL = "jdbc:derby:memory:testDB;create=true;user=unittest;password=unittest";
	
	/* Setup */	
	@BeforeClass
	public static void setUp() throws Exception {
		DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		Connection conn = DriverManager.getConnection(SQL_DB_URL);
		createTable(conn);
		insertPolitician(conn, 1, "Nicolas Sarkozy", "nicolassarkozy", "28-01-1955");
		insertPolitician(conn, 2, "Alain Juppé", "alainjuppe", "15-08-1945");
		insertPolitician(conn, 3, "François Fillon", "francoisfillon", "04-03-1954");
		insertPolitician(conn, 4, "Jean-François Copé", "jf_cope", "05-05-1964");
		insertPolitician(conn, 5, "Bruno Le Maire", "brunolemaire", "15-04-1969");
		insertPolitician(conn, 6, "Nathalie Kosciusko-Morizet", "nk_m", "14-05-1973");
		insertPolitician(conn, 7, "Jean-Frédéric Poisson", "jfpoisson78", "22-01-1963");
		insertPolitician(conn, 8, "François Hollande", "francoishollande.fr", "12-08-1954");
		
		loadCollectionSOLR("SocialMediaSOLR", path + "/json/SocialMediaSOLR.json");
	}
		
	private static void createTable(Connection conn) throws Exception {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE SCHEMA UNITTEST");
		stmt.executeUpdate("CREATE TABLE UNITTEST.Politicians(id INTEGER NOT NULL, name VARCHAR(50), twitter VARCHAR(50), dob VARCHAR(10))");
	}

	private static void insertPolitician(Connection conn, int id, String name, String twitter, String dob) throws Exception {
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO UNITTEST.Politicians VALUES (?, ?, ?, ?)");
		stmt.setInt(1, id);
		stmt.setString(2, name);
		stmt.setString(3, twitter);
		stmt.setString(4, dob);
		stmt.executeUpdate();
	}
	
	/* Tests */	
	@Test(expected = TatooineExecutionException.class)
	// Creation of an operator that tries to get data from a SOLR instance with no corresponding entry in the catalog
	public void testNotInCatalog() throws Exception {
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan00.phyp");
		plan.open();
		while (plan.hasNext()) {
			plan.next();
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhySOLRScan object
	public void testPhySOLRScan() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan01.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// All fields should be returned by PhySOLRScan
			assertEquals(10, next.nrsmd.colNames.length);
			String text = new String((char[]) next.getValue("text_s"));
			assertFalse(StringUtils.isBlank(text));
			// All documents returned should contain the keyword "Trump"
			assertTrue(text.toLowerCase().contains("trump"));
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhySOLRScan object
	public void testPhySOLRScanFromString() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		
		NIterator plan = PhysicalPlanParser.parseString("PhySOLRScan(\"trump\", \"\", \"SocialMediaSOLR\")");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// All fields should be returned by PhySOLRScan
			assertEquals(10, next.nrsmd.colNames.length);
			String text = new String((char[]) next.getValue("text_s"));
			assertFalse(StringUtils.isBlank(text));
			// All documents returned should contain the keyword "Trump"
			assertTrue(text.toLowerCase().contains("trump"));
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhySOLREval object
	public void testPhySOLREval() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
				
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan02.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// Only 2 fields should be returned by PhySOLREval
			assertEquals(2, next.nrsmd.colNames.length);
			String text = new String((char[]) next.getValue("text_s"));
			assertFalse(StringUtils.isBlank(text));
			// All documents returned should contain the keyword "Clinton"
			assertTrue(text.toLowerCase().contains("clinton"));
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhyJENAEval object
	public void testPhyJENAEval() throws Exception {
		// Check for existence of catalog entries used in this test
		addNamesOfPoliticiansJENA();
				
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan03.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 2 fields should be returned by PhyJENAEval
			assertEquals(2, next.nrsmd.colNames.length);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhyProjection object
	public void testProjection() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan04.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// Only 3 fields should be returned by PhyProjection
			assertEquals(3, next.nrsmd.colNames.length);
		}
		plan.close();		
	}	
	
	@Test
	// Creation of a PhyMemorySort object
	public void testSort() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan05.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		long pre = 0L;
		long cur = 0L;
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// Assert values are sorted
			cur = Long.valueOf((int) next.getValue("shares_i"));
			if (pre > cur) {
				fail();
			}
			pre = cur;
			// All fields should be returned by PhyMemorySort
			assertEquals(10, next.nrsmd.colNames.length);
		}
		plan.close();		
	}	
	
	@Test
	// Creation of a PhySelection operator that uses a predicate that compares integer values
	public void testSelection() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan06.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// All documents returned must have been shared or retweeted at least 75 times
			assertTrue(((int) next.getValue("shares_i")) > 75);
		}
		plan.close();		
	}	
	
	@Test
	// Creation of a PhyBindJoin operator (SOLR + JENA)
	public void testBindJoinSOLRJENAFromString() throws Exception {
		// Check for existence of catalog entries used in this test
		addSocialMediaSOLR();
		addNamesAndCurrents();
		
		String query = "PREFIX fm: <http://example.org/schemas/> "
				+ "SELECT DISTINCT ?current_s ?name_s ?surname_s "
				+ "WHERE { "
				+ "?pol fm:firstName ?name_s . "
				+ "?pol fm:lastName ?surname_s . "
				+ "?pol fm:party ?par . "
				+ "?par fm:current ?cur . "
				+ "?cur fm:abbreviation ?current_s . "
				+ "FILTER regex('%s', ?name_s) . "
				+ "FILTER regex('%s', ?surname_s) . }";
		String opl = "PhySOLRScan(\"Manuel Valls\", \"\", \"SocialMediaSOLR\")";
		String opr = String.format("PhyJENAEval(\"%s\", \"NamesAndCurrentsJENA\")", query);
		
		// Find index in NRSMD for "user_name_s"
		int index = 8;

		String columns = String.format("[%s, %s]", index, index);		
		String stream = String.format("PhyBindJoin(%s, %s, %s)", opl, opr, columns);
		
		NIterator plan = PhysicalPlanParser.parseString(stream);		
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 13 fields should be returned by PhyBindJoin (10 from SOLR, 3 from JENA)
			assertEquals(13, next.nrsmd.colNames.length);
		}
		plan.close();		
	}	
	
	@Test
	// Creation of a PhyHashJoin operator (SOLR + JENA)
	public void testHashJoinSOLRJENA() throws Exception {
		// Check for existence of catalog entries used in this test
		addTwitterIDsJENA();
		addSocialMediaSOLR();
				
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan08.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 13 fields should be returned by PhyHashJoin (10 from SOLR, 3 from JENA)
			assertEquals(13, next.nrsmd.colNames.length);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhySQLScan operator
	public void testPhySQLScan() throws Exception {
		// Check for existence of catalog entries used in this test
		addPoliticiansSQL();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan09.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 4 fields should be returned by PhySQLScan: (id, name, twitter, dob)
			assertEquals(4, next.nrsmd.colNames.length);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhySQLEval operator
	public void testPhySQLEval() throws Exception {
		// Check for existence of catalog entries used in this test
		addPoliticiansSQL();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan10.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 2 fields should be returned by PhySQLEval: (name, dob)
			assertEquals(2, next.nrsmd.colNames.length);
		}
		plan.close();		
	}	

	@Test
	// Creation of a PhyBindJoin operator (SOLR + SQL)
	public void testBindJoinSOLRSQLFromString() throws Exception {
		// Check for existence of catalog entries used in this test
		addPoliticiansSQL();
		addSocialMediaSOLR();
		
		String query = "SELECT dob FROM Politicians WHERE twitter = ?";		
		String opl = "PhySOLRScan(\"disparition\", \"\", \"SocialMediaSOLR\")";
		String opr = String.format("PhySQLEval(\"%s\", \"PoliticiansSQL\")", query);
		
		// Find index in NRSMD for "user_screen_name_s"
		int index = 9;
		
		String column = String.format("[%s]", index);		
		String stream = String.format("PhyBindJoin(%s, %s, %s)", opl, opr, column);
		
		NIterator plan = PhysicalPlanParser.parseString(stream);		
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 12 fields should be returned by PhyBindJoin (10 from SOLR, 2 from SQL)
			assertEquals(11, next.nrsmd.colNames.length);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhyJQScan operator
	public void testPhyJQScan() throws Exception {
		// Check for existence of catalog entries used in this test
		addBooksSQL();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan12.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 9 fields should be returned by PhyJQScan
			assertEquals(9, next.nrsmd.colNames.length);
			assertTrue(((int) next.getValue("pages_i")) > 500);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhyJQEval operator
	public void testPhyJQEval() throws Exception {
		// Check for existence of catalog entries used in this test
		addBooksSQL();
		
		NIterator plan = PhysicalPlanParser.parseFile(path + "/phyp/plan13.phyp");
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			// 4 fields should be returned by PhyJQEval (price_i, name_s, author_s, pages_i)
			assertEquals(4, next.nrsmd.colNames.length);
			assertTrue(((int) next.getValue("pages_i")) < 500);
		}
		plan.close();		
	}
	
	@Test
	// Creation of a PhyTwitterStreamer operator
	public void testPhyTwitterStreamer() throws Exception {
		// Listen for tweets about some of the most important French political figures of today 
		String[] politicians = { "fillon", "sarkozy", "hamon", "valls", "le pen", "macron", "mélenchon" };
		String opr = String.format("PhyTwitterStreamer(%s, %s, %s, %d)", "\"\"", "\"" + String.join(",", politicians) + "\"", "\"en,fr\"", 30);
		NIterator plan = PhysicalPlanParser.parseString(opr);	
		plan.open();
		assertTrue(plan.hasNext());
		while (plan.hasNext()) {
			NTuple next = plan.next();
			assertNotNull(next);
			String text = String.valueOf((char[]) next.getValue("text")).toLowerCase();
			// Note: Sometimes keywords are not present in the text of the tweet itself
			// But are part of the expanded URLs present in the tweet
			boolean textContainsKW = Arrays.stream(politicians).parallel().anyMatch(text::contains);
			boolean hasExtendedURL = text.contains("https");
			assertTrue(textContainsKW || hasExtendedURL);			
		}
		plan.close();		
	}
}
