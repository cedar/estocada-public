package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.BasicStorageReference;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class PhySQLEvalTest extends BaseTest {

	static final String DB_URL = "jdbc:derby:memory:testDB;create=true;user=evaltest;password=evaltest";
	static final String QUERY_EVAL_S = "SELECT name FROM Students ORDER BY ID ASC";
	static final String QUERY_EVAL_P = "SELECT name FROM Students WHERE ID = ?";
	static final String QUERY_SCAN = "SELECT * FROM Students ORDER BY ID ASC";
	static boolean setupComplete = false;

	@BeforeClass
	public static void setUp() throws Exception {
		if (!setupComplete) {
			DriverManager.registerDriver(new EmbeddedDriver());
			Connection conn = DriverManager.getConnection(DB_URL);
			createTable(conn);
			insertStudent(conn, 1, "Mike");
			insertStudent(conn, 2, "Brad");
			insertStudent(conn, 3, "Lucy");
		}
		setupComplete = true;
	}	
	
	@AfterClass
	public static void shutDown() throws SQLException {
		DriverManager.deregisterDriver(new EmbeddedDriver());
	}
	
	@Test
	public void testLoop() throws Exception {
		NIterator eval = makeSQLEval(QUERY_EVAL_S);
		eval.open();
		assertTrue(eval.hasNext());
		while (eval.hasNext()) {
			NTuple tuple = eval.next();
			assertNotNull(tuple);
		}
		eval.close();
	}	
	
	@Test
	// Test PhySQLEval constructor that receives a ViewSchema object
	public void testSchema() throws Exception {
		// Asserts there is an entry for our test view in the catalog
		Catalog catalog = Catalog.getInstance();
		assertTrue(catalog.contains("StudentsSQLEval"));
						
		// Retrieve view metadata from the catalog
		ViewSchema schema = catalog.getViewSchema("StudentsSQLEval");
		GSR gsr = (GSR) catalog.getStorageReference("StudentsSQLEval");
		
		// Setting the query
		PhySQLEval searcher = new PhySQLEval(QUERY_EVAL_S, gsr, schema);
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
			assertEquals(1, tuple.nrsmd.colNo);
		}

		searcher.close();
	}	
	
	@Test
	public void testSimpleQuery() throws Exception {
		NIterator eval = makeSQLEval(QUERY_EVAL_S);
		eval.open();
		assertTrue(eval.hasNext());
		
		eval.hasNext();
		NTuple tuple = eval.next();
		assertEquals(1, tuple.nrsmd.colNo);
		assertEquals("name", tuple.nrsmd.colNames[0]);
		assertEquals("Mike", new String((char[]) tuple.getValue("name")));
		
		eval.hasNext();
		tuple = eval.next();
		assertEquals(1, tuple.nrsmd.colNo);
		assertEquals("name", tuple.nrsmd.colNames[0]);
		assertEquals("Brad", new String((char[]) tuple.getValue("name")));
		
		eval.hasNext();
		tuple = eval.next();
		assertEquals(1, tuple.nrsmd.colNo);
		assertEquals("name", tuple.nrsmd.colNames[0]);
		assertEquals("Lucy", new String((char[]) tuple.getValue("name")));
		
		eval.close();
	}

	@Test
	public void testPlaceholderQuery() throws Exception {
		BindAccess eval = makeSQLEval(QUERY_EVAL_P);
		NTuple params = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }));
		params.addInteger(2);
		eval.open();
		eval.setParameters(params);
		assertTrue(eval.hasNext());
		if (eval.hasNext()) {
			NTuple tuple = eval.next();
			assertEquals("Brad", new String(tuple.getStringField(0)));
		}
		eval.close();
	}
	
	@Test(expected = TatooineExecutionException.class)
	public void testScan() throws Exception {
		makeSQLEval(QUERY_SCAN);
	}
	
	/** Returns a PhySQLEval object */
	protected PhySQLEval makeSQLEval(String query) throws Exception {
		final StorageReference sref = new BasicStorageReference();
		final NRSMD meta = new NRSMD(2, 
				new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE },
				new String[] { "id", "name" }, new NRSMD[0] );		
		sref.setProperty("url", DB_URL);
		return new PhySQLEval(query, sref, meta);
	}

	protected static void createTable(Connection conn) throws Exception {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE SCHEMA EVALTEST");
		stmt.executeUpdate("CREATE TABLE EVALTEST.Students(id INTEGER NOT NULL, name VARCHAR(250))");
	}

	protected static void insertStudent(Connection conn, int id, String name) throws Exception {
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO EVALTEST.Students VALUES (?, ?)");
		stmt.setInt(1, id);
		stmt.setString(2, name);
		stmt.executeUpdate();
	}
}
