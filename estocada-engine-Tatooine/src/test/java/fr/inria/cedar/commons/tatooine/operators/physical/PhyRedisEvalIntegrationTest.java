package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import redis.clients.jedis.Jedis;
import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import ucsd.edu.redis.RedisKQL.runtime.RedisConnection;

@Ignore
public class PhyRedisEvalIntegrationTest extends BaseTest {

	private static final Jedis jedis = RedisConnection.jedis; 
	
	@Before
	public void flush() {
		jedis.flushAll();
	}
	
	@Test
	public void testSingleString() throws Exception {		
		jedis.set("name", "rana");
		
		GSR gsr = new GSR("redis");
		gsr.setProperty("dbNumber", "0");
		
		String query = "SELECT Name FROM Name IN MAP[name]";
		NRSMD nrsmd = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE });
		BindAccess eval = new PhyRedisEval(query, nrsmd, gsr);
		eval.open();
		assertTrue(eval.hasNext());
		NTuple t = eval.next();
		assertEquals("rana", new String(t.getStringField(0)));
		eval.close();
	}
	
	@Test
	public void testSetOfMaps() throws Exception {
		jedis.sadd("DavidFollowersSet", "@Nora");
		jedis.sadd("DavidFollowersSet", "@Danney");
		jedis.sadd("DavidFollowersSet", "@Dmitry");

		Map<String, String> nora = ImmutableMap.of("country", "UAE", "city", "Dubai", "age", "23");
		Map<String, String> danney = ImmutableMap.of("country", "USA", "city", "SD", "age", "25");
		Map<String, String> dmitry = ImmutableMap.of("country", "Russia", "city", "Moscow", "age", "22");
		
		jedis.hmset("@Nora", nora);
		jedis.hmset("@Danney", danney);
		jedis.hmset("@Dmitry", dmitry);

		GSR gsr = new GSR("redis");
		gsr.setProperty("dbNumber", "0");
		
		String queryString = "SELECT followerInfo FROM follower IN MAP[?], followerInfo IN MAP[follower]";
		NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] {
		        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE });
		BindAccess eval = new PhyRedisEval(queryString, nrsmd, gsr);
		NTuple params = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }));
		params.addString("DavidFollowersSet");
				
		eval.open();
		eval.setParameters(params);
		
		assertTrue(eval.hasNext());
		while (eval.hasNext()) {
			NTuple t = eval.next();
			assertEquals(3, t.nrsmd.colNo);
		}
		eval.close();
	}

	@Test
	public void testSetOfStrings() throws Exception {
		jedis.sadd("followers", "@dimitry");
		jedis.sadd("followers", "@nora");
		jedis.sadd("followers", "@rana");
		Set<String> followers = new HashSet<String>(Arrays.asList(new String[] { "@dimitry", "@nora", "@rana" }));

		GSR gsr = new GSR("redis");
		gsr.setProperty("dbNumber", "0");

		String queryString = "SELECT followerNames FROM followerNames IN MAP[?]";
		NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] {
		        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE });
		BindAccess eval = new PhyRedisEval(queryString, nrsmd, gsr);
		NTuple params = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }));
		params.addString("followers");

		eval.open();
		eval.setParameters(params);
		while (eval.hasNext()) {
			NTuple t = eval.next();
			String follower1 = new String(t.getStringField(0));
			String follower2 = new String(t.getStringField(1));
			String follower3 = new String(t.getStringField(2));
			assertTrue(followers.contains(follower1));
			assertTrue(followers.contains(follower2));
			assertTrue(followers.contains(follower3));
			assertEquals(3, t.nrsmd.colNo);
		}
		eval.close();
	}

	@Test
	public void testMultipleSets() throws Exception {
		jedis.sadd("Letters", "A");
		jedis.sadd("Letters", "B");
		jedis.sadd("A", "1");
		jedis.sadd("A", "2");
		jedis.sadd("A", "3");
		jedis.sadd("B", "4");
		jedis.sadd("B", "5");
		jedis.sadd("B", "6");

		GSR gsr = new GSR("redis");
		gsr.setProperty("dbNumber", "0");

		String queryString = "SELECT Digits FROM Letter IN MAP[?], Digits IN MAP[Letter] ";
		NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] {
		        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE });
		BindAccess eval = new PhyRedisEval(queryString, nrsmd, gsr);
		NTuple params = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }));
		params.addString("Letters");

		eval.open();
		eval.setParameters(params);
		int count = 0;
		while (eval.hasNext()) {
			NTuple t = eval.next();
			assertEquals(3, t.nrsmd.colNo);
			count++;
		}
		assertEquals(2, count);
		eval.close();
	}
}
