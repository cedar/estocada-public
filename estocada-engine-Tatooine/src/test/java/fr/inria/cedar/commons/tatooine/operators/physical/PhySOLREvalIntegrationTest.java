package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient.RemoteSolrException;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

@Ignore
public class PhySOLREvalIntegrationTest extends BaseTest {

	@Test
	/** Queries for all information available on books stored as nested JSON documents in SOLR */
	public void testBooks() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		// The columns of the JSON file are: "author_s", "formats", "id", "name_s", "pages_i", "price_i", "ratings", "sequence_i", "series_s"
		// The fields "ratings" and "format" are nested		

		try {
			loadCollectionSOLR("BookSOLR", path + "/json/BookSOLR.json");
		} catch (Exception e) {
			String msg = String.format("Error creating collection 'BookSOLR' from JSON file '%s': %s", path + "/json/BookSOLR.json", e.getMessage());
			throw new TatooineExecutionException(msg);
		}
		
		// Asserts there is an entry for our test view in the catalog
		Catalog catalog = Catalog.getInstance();
		assertTrue(catalog.contains("BookSOLR"));

		// Retrieve view metadata from the catalog
		ViewSchema schema = catalog.getViewSchema("BookSOLR");
		GSR gsr = (GSR) catalog.getStorageReference("BookSOLR");

		// Setting the query
		String query = "*:*";
		String fields = "name_s";
		// We are only interested in the "name_s" field
		PhySOLREval searcher = new PhySOLREval(query, fields, gsr, schema);
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
			assertEquals(1, tuple.nrsmd.colNo);
			assertEquals("name_s", tuple.nrsmd.colNames[0]);
		}

		searcher.close();
	}
	
	@Test(expected = RemoteSolrException.class)
	public void testRemoteConnectionWrongCredentials1() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		PhySOLREval searcher = createPhySOLREval(null, null);
		searcher.hasNext();
	}

	@Test(expected = RemoteSolrException.class)
	public void testRemoteConnectionWrongCredentials2() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		PhySOLREval searcher = createPhySOLREval("solr", null);
		searcher.hasNext();
	}

	@Test(expected = RemoteSolrException.class)
	public void testRemoteConnectionWrongCredentials3() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		PhySOLREval searcher = createPhySOLREval(null, "SolrRocks");
		searcher.hasNext();
	}

	@Test(expected = RemoteSolrException.class)
	public void testRemoteConnectionWrongCredentials4() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		PhySOLREval searcher = createPhySOLREval("incorrectUserName", "incorrectPassword");
		searcher.hasNext();
	}
	
	@Test
	public void testRemoteConnectionRightCredentials() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		PhySOLREval searcher = createPhySOLREval("solr", "SolrRocks");
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
		}		
		searcher.close();
	}

	/** Creates a PhySOLREval operator used to test SOLR remote connections */
	private PhySOLREval createPhySOLREval(String userName, String password) throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		// The format of documents for remote connection tests is as follows:
		// {
		// 	  "id": "40392834",
		// 	  "name_s": "El Cantinero",
		//    "address": {
		//       "building_s": "86",
		// 	     "coord": [ -73.9932394, 40.7336411 ],
		// 	     "street_s": "University Place",
		// 		 "zipcode_s": "10003"
		//    },
		// 	  "borough_s": "Manhattan",
		//    "cuisine_s": "Mexican",
		// }

		// Metadata
		NRSMD nrsmd = new NRSMD(5, new TupleMetadataType[] {
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE }, new String[] {
				"address.building_s", "address.street_s",
				"cuisine_s", "id", "name_s" }, new NRSMD[0]);

		// GSR properties
		GSR gsr = new GSR("server");
		gsr.setProperty("coreName", "RestaurantsSOLR");
		gsr.setProperty("serverUrl", "http://195.83.212.130");
		gsr.setProperty("serverPort", "8983");

		// Optional basic authentication
		if (!Strings.isNullOrEmpty(userName)) {
			gsr.setProperty("serverUsr", userName);
		}
		if (!Strings.isNullOrEmpty(password)) {
			gsr.setProperty("serverPwd", password);
		}

		// Setting the query
		String query = "cuisine_s:Mexican";
		PhySOLREval searcher = new PhySOLREval(query, "id,name_s,cuisine_s,address.building_s,address.street_s", gsr, nrsmd);
		searcher.open();
		return searcher;
	}

}
