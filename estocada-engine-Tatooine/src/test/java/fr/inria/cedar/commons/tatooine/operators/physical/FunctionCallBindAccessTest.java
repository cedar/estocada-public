package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.utilities.NTupleFunction;
import fr.inria.cedar.commons.tatooine.utilities.Utilities;
import fr.inria.cedar.commons.tatooine.utilities.Utilities.NIteratorFromCollection;

public class FunctionCallBindAccessTest extends BaseTest{

	@Test
	public void testSimpleEchoCall() throws TatooineExecutionException {
		
		/*
		 * We load the tuples from p01.txt. The two tuples have 7 columns:
		 * (int, string, int, string, string, int, string)
		 */
		NTupleReader reader = new NTupleReader(path + "/txt/p01.txt");
		List<NTuple> tupleList = reader.read();
		
		//We join on the columns (3,1)
		NRSMD fileTupleNrsmd = tupleList.get(0).nrsmd;
		int[] boundColumns = {3, 1};
		NRSMD bindingNrsmd = NRSMD.makeProjectRSMD(fileTupleNrsmd, boundColumns);
		Predicate equivalentPredicate = new ConjunctivePredicate(
				new SimplePredicate(3, fileTupleNrsmd.colNo),
				new SimplePredicate(1, fileTupleNrsmd.colNo + 1));
		

		NTupleFunction echoFunction = new Utilities.NTupleEchoFunction(bindingNrsmd);
		FunctionCallBindAccess echoBind = new FunctionCallBindAccess(echoFunction);
		NIteratorFromCollection readerIterator = new Utilities.NIteratorFromCollection(tupleList);
		PhyBindJoin echoJoin = new PhyBindJoin(readerIterator, echoBind, equivalentPredicate);

		List<NTuple> echoJoinResults = Utilities.listFromNIterator(echoJoin);
		
		//Check the NRSMD
		assertEquals(fileTupleNrsmd.colNo + 2, echoJoin.nrsmd.colNo);
		assertEquals(NRSMD.appendNRSMD(fileTupleNrsmd, bindingNrsmd) , echoJoin.nrsmd);
		
		//Check the length of the results
		assertEquals(tupleList.size(), echoJoinResults.size());
		
		//Check the results
		NTuple tTuple = echoJoinResults.get(0);
		assertEquals(1, tTuple.getIntegerField(0));
		assertEquals("ON", String.valueOf(tTuple.getStringField(1)));
		assertEquals(2, tTuple.getIntegerField(2));
		assertEquals("BC", String.valueOf(tTuple.getStringField(3)));
		assertEquals("QC", String.valueOf(tTuple.getStringField(4)));
		assertEquals(13, tTuple.getIntegerField(5));
		assertEquals("NBA", String.valueOf(tTuple.getStringField(6)));
		assertEquals("BC", String.valueOf(tTuple.getStringField(7)));
		assertEquals("ON", String.valueOf(tTuple.getStringField(8)));
		
		tTuple = echoJoinResults.get(1);
		assertEquals(14, tTuple.getIntegerField(0));
		assertEquals("SK", String.valueOf(tTuple.getStringField(1)));
		assertEquals(22, tTuple.getIntegerField(2));
		assertEquals("M", String.valueOf(tTuple.getStringField(3)));
		assertEquals("AB", String.valueOf(tTuple.getStringField(4)));
		assertEquals(5, tTuple.getIntegerField(5));
		assertEquals("MLS", String.valueOf(tTuple.getStringField(6)));
		assertEquals("M", String.valueOf(tTuple.getStringField(7)));
		assertEquals("SK", String.valueOf(tTuple.getStringField(8)));
	}
}
