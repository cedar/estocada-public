package fr.inria.cedar.commons.tatooine.loader;

import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolder;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolderWithTDB;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by DucCao on 05/10/16.
 */
public class DatabaseLoaderTest extends CatalogTest {

    private StorageReference getPostgreStorageReference() throws Exception {
        StorageReference gsr = new GSR("artist");
        gsr.setProperty(PostgresDatabaseLoader.KEY_HOST, "localhost");
        gsr.setProperty(PostgresDatabaseLoader.KEY_PORT, "5432");
        gsr.setProperty(PostgresDatabaseLoader.KEY_DATABASE, "mydb");
        gsr.setProperty(PostgresDatabaseLoader.KEY_USERNAME, "admin");
        gsr.setProperty(PostgresDatabaseLoader.KEY_PASSWORD, "pwd");
        return gsr;
    }

    @Test
    public void testSolr() throws Exception {
        StorageReference storageReference = getSolrStorageReference("SocialMediaSOLRv2");
        String jsonDataFile = path + "/json/SocialMediaSOLR.json";

        DatabaseLoader databaseLoader = new SolrDatabaseLoader(SOLR_CONFIG_SETS, storageReference);
        String dataCollectionName = databaseLoader.getCatalogEntryName();
        // delete the collection if it was in SOLR before
        if (databaseLoader.isDataLoaded(dataCollectionName)) {
            databaseLoader.deleteCollection(dataCollectionName);
        }

        // verify that the data is not in SOLR
        assertFalse(databaseLoader.isDataLoaded(dataCollectionName));

        // load the data into SOLR and verify the loading process is successful
        databaseLoader.load(jsonDataFile);
        databaseLoader.registerNewCatalogEntry();
        assertTrue(databaseLoader.isDataLoaded(dataCollectionName));

        // Assert the catalog contains an entry for the view SocialMediaSOLR
        Catalog catalog = Catalog.getInstance();
        assertTrue(catalog.contains(dataCollectionName));

        // Assert the schema of SocialMediaSOLR contains 10 columns
        assertEquals(10, catalog.getViewSchema(dataCollectionName).getNRSMD().colNo);

        // verify that column names should be sorted in alphabetically order
        String[] colNames = catalog.getViewSchema(dataCollectionName).getNRSMD().getColNames();
        assertArrayEquals(colNames, new String[] {"comments_i","created_at_s","id","likes_i","retweeted_s","shares_i","text_s","type_s","user_name_s","user_screen_name_s"});
    }

    @Test
    public void testJena() throws Exception {
        String catalogEntry = "NamesOfPoliticiansJENA";
        String rdfFilePath  = path + "/rdf/model.ttl";

        StorageReference storageReference = new GSR(catalogEntry);
        storageReference.setProperty(JenaDatabaseLoader.MODEL_RDF, rdfFilePath);

        JENADatabaseHolder.clearCache();

        JenaDatabaseLoader databaseLoader = new JenaDatabaseLoader(storageReference);
        assertFalse(databaseLoader.isDataLoaded(catalogEntry));

        databaseLoader.load(rdfFilePath);
        databaseLoader.setColumns(new String[] {"name_s", "surname_s"});
        databaseLoader.registerNewCatalogEntry();
        assertTrue(databaseLoader.isDataLoaded(catalogEntry));

        Catalog catalog = Catalog.getInstance();
        assertTrue(catalog.contains(catalogEntry));
        assertEquals(2, catalog.getViewSchema(catalogEntry).getNRSMD().colNo);

        // verify that column names should be sorted in alphabetically order
        String[] colNames = catalog.getViewSchema(catalogEntry).getNRSMD().getColNames();
        assertArrayEquals(colNames, new String[] {"name_s", "surname_s"});
    }

    @Test
    public void testJenaMultipleModels() throws Exception {
        // load NamesOfPoliticiansJENA model
        String catalogEntry = "NamesOfPoliticiansJENA";
        String rdfFilePath  = path + "/rdf/model.ttl";

        StorageReference storageReference = new GSR(catalogEntry);
        storageReference.setProperty(JenaDatabaseLoader.MODEL_RDF, rdfFilePath);

        JENADatabaseHolder.clearCache();

        JenaDatabaseLoader databaseLoader = new JenaDatabaseLoader(storageReference);
        assertFalse(databaseLoader.isDataLoaded(catalogEntry));

        databaseLoader.load(rdfFilePath);
        databaseLoader.setColumns(new String[] {"name_s", "surname_s"});
        databaseLoader.registerNewCatalogEntry();
        assertTrue(databaseLoader.isDataLoaded(catalogEntry));

        Catalog catalog = Catalog.getInstance();
        assertTrue(catalog.contains(catalogEntry));
        assertEquals(2, catalog.getViewSchema(catalogEntry).getNRSMD().colNo);

        // verify that column names should be sorted in alphabetically order
        String[] colNames = catalog.getViewSchema(catalogEntry).getNRSMD().getColNames();
        assertArrayEquals(colNames, new String[] {"name_s", "surname_s"});

        // load copy of NamesOfPoliticiansJENA model
        // the database loader shouldn't load the RDF file again
        String newCatalogEntry = "NamesOfPoliticiansJENA-copy";

        storageReference = new GSR(newCatalogEntry);
        storageReference.setProperty(JenaDatabaseLoader.MODEL_RDF, rdfFilePath);

        databaseLoader.setStorageReference(storageReference);
        assertFalse(databaseLoader.isDataLoaded(newCatalogEntry));

        assertTrue(JENADatabaseHolderWithTDB.hasRdfFileBeenLoaded(rdfFilePath));
        databaseLoader.load(rdfFilePath);
        databaseLoader.setColumns(new String[] {"name_s", "surname_s", "twitter_s"});
        databaseLoader.registerNewCatalogEntry();
        assertTrue(databaseLoader.isDataLoaded(newCatalogEntry));

        assertTrue(catalog.contains(catalogEntry));
        assertEquals(3, catalog.getViewSchema(newCatalogEntry).getNRSMD().colNo);

        // verify if NamesOfPoliticiansJENA model is still accessible
        assertTrue(catalog.contains(catalogEntry));
        assertTrue(databaseLoader.isDataLoaded(catalogEntry));

        // verify that column names should be sorted in alphabetically order
        colNames = catalog.getViewSchema(newCatalogEntry).getNRSMD().getColNames();
        assertArrayEquals(colNames, new String[] {"name_s", "surname_s", "twitter_s"});
    }

    @Test
	public void testPostgres() throws Exception {
		String dataCollectionName = "artist";

		DatabaseLoader databaseLoader = new PostgresDatabaseLoader(getPostgreStorageReference());

		databaseLoader.deleteCollection(dataCollectionName);

		assertFalse(databaseLoader.isDataLoaded(dataCollectionName));
		databaseLoader.load(path + "/postgresql/db.dump");
		assertTrue(databaseLoader.isDataLoaded(dataCollectionName));

		databaseLoader.registerNewCatalogEntry();

		Catalog catalog = Catalog.getInstance();
		assertTrue(catalog.contains(dataCollectionName));

		assertEquals(2, catalog.getViewSchema(dataCollectionName).getNRSMD().colNo);
		assertArrayEquals(
				catalog.getViewSchema(dataCollectionName).getNRSMD().colNames,
				new String[] {"artist_id", "name"}
		);
		assertArrayEquals(
				catalog.getViewSchema(dataCollectionName).getNRSMD().getColumnsMetadata(),
				new TupleMetadataType[] {
						TupleMetadataType.INTEGER_TYPE,
						TupleMetadataType.STRING_TYPE
				}
		);
	}

	@Test
	public void testPostgresShouldDiscardDatabase() throws Exception {
		PostgresDatabaseLoader databaseLoader = new PostgresDatabaseLoader(getPostgreStorageReference(), true);
		assertTrue(databaseLoader.isDatabaseCreated());

		databaseLoader = new PostgresDatabaseLoader(getPostgreStorageReference());
		assertFalse(databaseLoader.isDatabaseCreated());
	}

}
