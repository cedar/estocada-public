# ID | [formats] | name_s | author_s | series_s | sequence_i | price_i | pages_i | [ratings], _version_
978-0641723441|softcover|The Philosopher's Stone|J.K. Rowling|Harry Potter|1|12|223|1|1558232519054196736
978-0641723442|softcover,hardcover|The Chamber of Secrets|J.K. Rowling|Harry Potter|2|14|251|1,2|1558232520049295360
978-0641723443|softcover,hardcover,paperback|The Prisoner of Azkaban|J.K. Rowling|Harry Potter|3|15|317|1,2,3|1558232520082849792
978-0641723444|softcover,hardcover|The Goblet of Fire|J.K. Rowling|Harry Potter|4|18|636|1,2,3,4|1558232520083898368
978-0641723445|softcover|The Order of the Phoenix|J.K. Rowling|Harry Potter|5|18|766|1,2,3,4,5|1558232520085995520
978-0641723446|softcover,hardcover|The Half-Blood Prince|J.K. Rowling|Harry Potter|6|18|607|1,2,3,4,5,6|1558232520087044096
978-0641723447|softcover,hardcover,paperback|The Deathly Hallows|J.K. Rowling|Harry Potter|7|20|607|1,2,3,4,5,6,7|1558232520089141248
978-0641723448|softcover,hardcover|The Cursed Child|J.K. Rowling|Harry Potter|8|15|320|1,2,3,4,5,6,7,8|1558232520105918464