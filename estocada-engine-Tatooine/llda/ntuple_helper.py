

def file_to_text(file_path):
	file = open(file_path, 'r')
	text = file.read()
	file.close()

	return text


def file_to_list(file_path):
	file = open(file_path, 'r')
	lines = file.readlines()
	file.close()

	return [line.replace('\n', '') for line in lines]


def topic_keys(topic_keys_file):
	lines = file_to_list(topic_keys_file)
	d = dict()
	for line in lines:
		items = line.split('\t')
		d[items[0]] = int(items[1])
	return d


def predict_labels(id_topic_map, line):
	pred_labels = dict()
	items = line.split('\t')
	for topic_id, prob in enumerate(items[2:]):
		pred_labels[id_topic_map[str(topic_id)]] = float(prob)

	return pred_labels


def find_topic_distributions(topic_key_file, doc_topic_file, tuple_file):
	id_topic_map = topic_keys(topic_key_file)
	prediction = predict_labels(id_topic_map, file_to_text(doc_topic_file))

	f = open(tuple_file, 'w+')
	for topic_id, prob in prediction.iteritems():
		f.write('(%d,"%f")\n' % (topic_id, prob))
	f.close()


if __name__ == '__main__':
    find_topic_distributions('model/zirn-1.keys', 'doc_topics.txt', 'output_tuples.txt')