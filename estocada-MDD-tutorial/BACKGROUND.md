# Background
We provide a brief introduction to conjunctive queries and integrity constraints. 

## Conjunctive Queries:
A conjunctive query (or simply CQ) is an expression of the form:

```math
Q (\vec x):- R_1(\vec y_1),...,R_n(\vec y_n)
```

where each R<sub>i</sub> for every i = 1, . . . , n, is a predicate (relation) of some finite arity, and `Q` is a fresh relation name, and it is called intensional relation since its content is given by definition through the query.  

```math
\vec x ,\vec y_1,... , \vec y_n
```
are tuples of variables or constants. Each R<sub>i</sub> is called a relational atom or subgoal. It is also called extensional relation since it is provided as input to the query. 

The expression $`Q (\vec x)`$ is the head of the query, while the conjunction of relational atoms R<sub>i</sub>, ...,R<sub>n</sub> is its body. All variables in the head are called distinguished} variables. Also, every variable in ``x`` must appear at least once in y<sub>1</sub>, ...,y<sub>n</sub>  tuples. Below some examples of conjunctive queries. 

### CQ Examples:
```math
Q_1 (x,z):- R(x,y), S(y,z)\\
Q_2 (x,z):- R(x,y), S(y,z), R(x,e)\\
Q_3 ():- R(x,y), S(y,z), T(y)\\
Q_4 (x,y): - R(x,y),y=c, \text{where $c$ is a constant}
```

## Integrity Constraints:

Different forms of integrity constraints (a.k.a, database dependencies in the relational model) have 
been introduced and studied in the literature. They are used to express properties that must be 
satisfied by all instances of a database schema. These properties arise in a specific application 
domain. As an example of a property: "there are no two distinct students that can have the same ID." 
Constraints can be expressed using a fragment of the language of first-order logic, known as the class 
of embedded dependencies. The follwoing is the general form:

```math
\forall x_1,\dots x_n  \phi (x_1,\dots x_n) \rightarrow \exists z_1,\dots,z_k \psi(y_1,\dots,y_m) 
```
where
```math
\{z_1, \dots, z_k \} = \{y_1, \dots, y_m\} \char`\\ \{x_1,\dots, x_n\}
```

The constraint's premise $`\phi`$ is a conjunction (possibly empty) of
relational atoms over variables $`x_1,\dots, x_n`$ or constants. The constraint's conclusion $`\psi`$  is a non-empty conjunction of relational atoms and/or equality atoms -- of the form  $`w = w'`$ -- over variables $`y_1,\dots, y_m`$ or constants. The case when all atoms in $`\psi`$ are relational atoms is called `Tuple Generating Dependencies` (**TGDs**), while the case when all atoms in $`\psi`$ are equalities defines ``Equality Generating Dependencies`` (**EGD**).

### Example of Dependencies
Consider a relation **Review(paper, reviewer, track)** listing reviewers of papers submitted to a conference's tracks, and a relation **PC (member, affiliation)** listing the affiliation of every program committee member. The fact that a paper can only be submitted to a single track is captured by the following EGD:

```math
 \forall p \forall r \forall t  \forall r' \forall t' Review(p,r,t) \wedge Review(p,r',t') \rightarrow t=t'
```
We can also express that papers be reviewed only by PC members by the following TGD: 

```math
\forall p \forall r \forall t~ Review(p, r,t) \rightarrow \exists a ~PC (r, a)
```

