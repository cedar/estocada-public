cp mimictutorialJSON.json /tmp/
cp mimic_dlabitems.json /tmp/
cp vpjpj.json /tmp/
cp vpjpr.csv /tmp/
cp noteeventsNew.csv /tmp/
/etc/init.d/postgresql start
sudo -i -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres';"
sudo -i -u postgres psql -c "CREATE DATABASE mimictut;"
sudo -i -u postgres psql -d mimictut -c "CREATE SCHEMA mimictutorial;"
sudo -i -u postgres psql -d mimictut -c "CREATE TABLE mimictutorial.mimicdata ( p jsonb );"
sudo -i -u postgres psql -d mimictut -c "CREATE TABLE mimictutorial.mimiclabitems ( data jsonb );"
sudo -i -u postgres psql -d mimictut -c "CREATE table mimictutorial.VPJPR ( SUBJECT_ID int, DOB text);"
sudo -i -u postgres psql -d mimictut -c "CREATE table mimictutorial.VPJPJ ( p jsonb);"
sudo -i -u postgres psql -d mimictut -c "COPY mimictutorial.mimicdata from '/tmp/mimictutorialJSON.json';"
sudo -i -u postgres psql -d mimictut -c "copy mimictutorial.mimiclabitems from '/tmp/mimic_dlabitems.json';"
sudo -i -u postgres psql -d mimictut -c "COPY mimictutorial.VPJPJ from '/tmp/vpjpj.json';"
sudo -i -u postgres psql -d mimictut -c "COPY mimictutorial.VPJPR from '/tmp/vpjpr.csv' DELIMITER ',' CSV HEADER NULL '';"
curl --max-time 0 'http://localhost:8983/solr/mimicTutoText/update' --data-binary @/tmp/noteeventsNew.csv -H 'Content-type:application/csv'