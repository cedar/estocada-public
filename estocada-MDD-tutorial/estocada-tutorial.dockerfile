# Copyright 2022 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# http://www.apache.org/licenses/LICENSE-2.0
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:18.04
MAINTAINER RALOTAIBI
USER root

#Install Prerequisites
ENV DEBIAN_FRONTEND noninteractive
RUN apt update && apt install -y tcl
RUN apt-get clean
RUN apt-get update -y
RUN apt update -y
RUN apt-get install openjdk-8-jdk -y
RUN java -version

RUN apt-get install --reinstall systemd -y
RUN apt-get install -y maven
RUN apt-get install -y curl
RUN apt-get install -y unzip
RUN apt-get install -y wget
RUN apt-get install -y sudo
RUN apt-get install -y git
RUN apt-get install -y vim 
RUN apt-get install -y nano
RUN apt-get install -y graphviz

#Install PostgreSQL and Solr 
WORKDIR /project/systems
RUN sudo apt install postgresql postgresql-contrib -y 
RUN    /etc/init.d/postgresql start
RUN wget https://archive.apache.org/dist/lucene/solr/6.0.0/solr-6.0.0.zip
RUN unzip solr-6.0.0.zip


#Build ESTOCADA
WORKDIR /project
COPY . .
WORKDIR /project/estocada
RUN  chmod +x build-skip-tests.sh
RUN ./build-skip-tests.sh
WORKDIR /project/estocada/estocada-tutorial
RUN  chmod +x runExample.sh
WORKDIR /project/dataConfig/
RUN chmod +x runSetup.sh
WORKDIR /project/systems/solr-6.0.0/server/solr/configsets/
RUN cp -R /project/dataConfig/mimicTutText .
WORKDIR /project/estocada/estocada-tutorial