# Welcome! 
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/ESTOCADA.png"  width=150  >
    <br>
</p>

# Overview
This page is a step-by-step tutorial of ESTOCADA for the [MDD Summer School 2022](https://cedric.cnam.fr/lab/confs/mdd2022/). This tutorial is aimed at getting you up and running with `ESTOCADA` and its internal representation. 

We will focus on two main parts:
* **[Part 1: ESTOCADA Pivot Model](https://gitlab.inria.fr/cedar/estocada-public/-/tree/master/estocada-MDD-tutorial#part-1-estocada-pivot-model)**: We will study how different (data model, language pairs) can be 
encoded relationally, with the support of integrity constraints that capture the key features of 
various data models. 
	- For each supported data model, we show the _relational encoding_ of queries and 
views, where views are encoded as a set of integrity constraints. 
	- Then, we _reformulate relational 
queries under constraints_. 
	- Finally, we illustrate the _decoding 
of the relational reformulation_, which translation it in the query language specific to each data model.  

If needed, background on relational conjunctive queries and 
constraints is provided [here](https://gitlab.inria.fr/cedar/estocada-public/-/blob/master/estocada-MDD-tutorial/BACKGROUND.md).

* **[Part 2: ESTOCADA Integration on top of Tatooine Execution Engine](https://gitlab.inria.fr/cedar/estocada-public/-/tree/master/estocada-MDD-tutorial#part-1-estocada-pivot-model)**: In this part, we demonstrate an end-to-end scenario that rewrites a query using cross-model views (JSON, text, and relational) and then transforms the rewriting into a data integration plan, executed using the Tatooine data integration engine. More details about Tatooine can be found [here](http://www.vldb.org/pvldb/vol9/p1513-bonaque.pdf).

<img align="left" width="50" height="50" src="https://www.docker.com/wp-content/uploads/2022/03/vertical-logo-monochromatic.png">

# Tutorial Docker Setup 

## Prerequisites
* To work with ESTOCADA tutorial, you only need to have **Docker** installed: 
	* Docker for MacOS: https://docs.docker.com/desktop/mac/install/
	* Docker for Windows: https://docs.docker.com/desktop/windows/install/
	* Docker for Linux: https://docs.docker.com/engine/install/ubuntu/
* Make sure that the Docker daemon is running before running any docker commands. For example, using MacOS, you can see the status in the menu bar.
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/dockerStatus.png"  width=200  >
    <br>
</p>

* We tested our Docker container using version 20.10.11.

## Pull ESTOCADA Tutorial From the [Docker Hub](https://hub.docker.com/repository/docker/ralotaib/estocada-docker)

```bash
$ docker pull ralotaib/estocada-docker:latest
```
Here is a screenshot after you pull the container:
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/dockerPull.png"  width=450  >
    <br>
</p>

* The size of the container is `~` 3.09GB.
* You can now list the images/containers using the following command:

```bash
$ docker image ls
```
The image/container ``ralotaib/estocada-docker`` must be listed as shown below:
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/dockerImgeLS.png"  width=300  >
    <br>
</p>

* If you face any issues when pulling the container from the Docker hub, we provide you the Docker file ``estocada-tutorial.dockerfile``. By using this file, you can build the container locally ( :warning: this will take `~` 15 mins!). 
* Before building the container, make sure to assign at least 4GB RAM to Docker. Open your Docker desktop: Preferences > Resources. See the screenshot below.

<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/dockerResources.png"  width=600  >
    <br>
</p>

```bash 
# Building the container locally: Linux or MacOS command
$ git clone https://gitlab.inria.fr/cedar/estocada-public.git
$ cd estocada-MDD-tutorial/
$ DOCKER_BUILDKIT=0 docker build --progress=plain -t ralotaib/estocada-docker -f estocada-tutorial.dockerfile .
```

## Docker Container Installed Prerequisites:
* We have already installed in the container the follwoing: 
	* Ubuntu v18.4
	* Java v1.8 
	* PostgreSQL v10.2 
	* Solr v6.0.0
	* Text editors: Vim and nano
	* Wget 
	* Git 
	* Maven
	* Curl 
	* unzip
	* Graphviz
	* Sudo

## Useful Docker Commands 
* We list here some useful Docker commands you may need: 
	* List containers and their ids
	```bash
	$ docker ps -a
	```
	* Remove Container 
	```bash
	$ docker container rm [CONTAINER ID]
	```
	* List Images 
	```bash
	$ docker image ls
	```
	* Delete Image 
	```bash
	$ docker image rmi [IMAGE ID]
	```
	* Docker System DF 
	```bash
	$ docker system df
	```

<img align="left" width="40" height="40" src="https://img.icons8.com/stickers/344/play.png">

# Start the Tutorial

## Run the Container 
```bash
$ mkdir mnt
$ cd mnt 
$ docker run -it --memory=4g  -v $(pwd):/tmp/ ralotaib/estocada-docker:latest  bash
```

* Container Project Structure:
	* project/estocada
		* ``estocada-commons/`` - contains the conjunctive query compiler and classes for relations and schemas
		* ``estocada-engine-Tatooine/`` - contains the Tatooine mediator.
		* ``estocada-PACB/`` - contains the relational rewriting engine 
		* ``estocada-main/`` -  contains ``ESTOCADA`` core classes, including supported language grammars, encoders, and decoders
		* ``estocada-tutorial/`` - contains the tutorial material
	* project/systems
		* Solr v6.0.0
	* **Note**: all above Maven project/modules are already compiled and built. You do not need to re-build them.
* Any file placed under ``tmp/`` folder in the Docker container will appear in your host machine under ``mnt/`` folder.

* :warning: If you `exit` the container, then you need to re-run it again.
* :information_source: If you need to exit the container and want to save the latest status before existing, you can use the following command:

```bash
docker commit [Container ID] ralotaib/estocada-docker:latest
``` 

## Tutorial Directory 
* Navigate to ``estocada-tutorial/`` to start working on the tutorial materials.

```bash
cd estocada-tutorial/
```

## Tutorial Main Config File 
The main coontrol over running Estocada goes through  the **``estocada-tutorial/config.json`` config file** You will need to modify this file several times.   
```bash
{	
	"exampleFolder":"examples/<exampleFolderName>/",
	"lang": "<Used Query Language>",
	"visualizeConstraints":"<Flag (y or n)>"
}
```
The `config.json` file **info** example:
* `"exampleFolder":"examples/AJExample/"` - The `exampleFolderName` is `AJExample`. In the example folder, we add a query rewritten in the [AQL language](https://nightlies.apache.org/asterixdb/aql/manual.html) in the ``examples/AJExample/Query``file, and we add a view in the ``examples/AJExample/View.V`` file.
* `"lang": "AQL"` - The JSON query language used in this example is ``AQL``.
* ``"visualizeConstraints":"y"`` -  `y` or `n` for visualizing the applied constraints. When this is `y`, constraints are shown in the order in which they are applied. 

## Part 1: ESTOCADA Pivot Model
ESTOCADA's pivot model is relational, and it makes prominent use of expressive integrity constraints. This part of the tutorial shows how we encode various model/language pairs relationally. 

### JSON Data Model - AQL Language 
* **Step 1**: Add your AQL query in the ``Query`` file and the view in ``View.v`` file under `examples/AJExample/` folder. We already provide the follwoing example of a query and view under `examples/AJExample/` folder. 

* **AQL Query**:
```bash
Q = for d in dataset D, 
        p in d.site.person,
          ssn in p.ssn,
          a in p.address,
            zip in a.zip,
            streetName in a.street
return ssn,zip,streetName
```
* **AQL View**:
```bash
V = 
for d in dataset D, p in d.site.person, ssn in p.ssn
return {"id": ssn, 
        "addresses": for a in p.address
                     return {"address": 
                                {"zip": for z in a.zip return z, 
                                "streetName": for st in a.street return st}
                            }
        }
```
* **Step 2**: Make sure to have the following info in the ``config.json``file. **Note**: you can disable visualizing the constraints by adding ``n`` instead of ``y``.  

```bash
{
	
	"exampleFolder":"examples/AJExample/",
	"lang": "AQL",
	"visualizeConstraints":"y"
}
```

* **Step 3**: Run the following command to get (1) query `Q` encoded using our JSON virtual encoding relations, (2) view `V` encoded as a set of constraints capturing the level of nesting in `V`, (3)  the relational reformulation of `Q` using `V`, and (4) the decoded rewriting expressed in ``AQL`` language.

```bash 
$ ./runExample.sh
```

You can see the following output in the terminal:
``` 
****** Encoded CQ Query *****
Q<b_40> :- root_aj_68(b_0),child_aj(b_1,b_2,"ssn","o"),child_aj(b_0,c_1,"site","o"),child_aj(c_1,b_1,"person","o"),child_aj(b_3,b_4,"zip","o"),child_aj(b_1,b_3,"address","o"),eq_aj(b_4,b_40);

****** CQ Rewritings *****
RW0<b_40> :- child_aj_V(SK_4,SK_6,SK_7,"a"),eq_aj_V(SK_6,b_40),child_aj_V(SK_3,SK_4,"zip","o"),child_aj_V(SK_2,SK_3,"SDAddress","o"),child_aj_V(SK_1,SK_2,"addresses","o"),V(SK_1);

****** Decoded Rewritings *****
 FOR SK_1 IN  DATASET V,
        SK_0 IN SK_1.id
        SK_2 IN SK_1.addresses, 
           SK_3 IN SK_2.address,
              SK_5 IN SK_3.streetName, 
              SK_4 IN SK_3.zip 
 RETURN SK_0 , SK_4 , SK_5
```
* `AJExample/constraints_chase`, `AJExample/constraints_bkchase` and `AJExample/schemas` files will be generated. You can see the set of generated view `V` related constraints in `AJExample/constraints_chase` and `AJExample/constraints_bkchase` files. Also, you can see the JSON model encoding relations in `AJExample/schemas`.
* The visualization of the applied constraints ``constraintsAJExample.png `` will be placed in your host machine under ``mnt/`` folder. Click on the image below and zoom in.
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/constraintsAJExample.png"  width=1000  >
    <br>
</p>

* Notice that finding the rewriting of ``Q`` using ``V`` requires reasoning about the nested blocks and the construction of a new JSON object in the return clause of ``V``. The view ``V`` cannot be directly matched against ``Q``.

* After the JSON model and view constraints are generated, you can disable any constraints and observe how this can affect the output rewriting. To do so:

(1) Update the `config.json` file to use the `CONJUNCTIVE_QUERY` language since everything is purely relational after the encoding step. 
```bash
{
	
	"exampleFolder":"examples/AJExample/",
	"lang": "CONJUNCTIVE_QUERY",
	"visualizeConstraints":"y"
}
```
(2) Open the generated file `AJExample/constraints_chase` using ``vim`` or ``nano`` (they are already installed in the container). For example, if we comment out the following constraint (adding `#` at the beginning of the line), there will be no rewriting: 

```bash
# copy_aj_V(x,x1),eq_aj(x,y)->eq_aj_V(x1,y);
```

### Relational Data Model - SQL Language
* In this simple example, the query `Q` and views `VRS` and ``VZ`` are expressed in the SQL language. To run the example, you need to update the `config.json` file as follows:

```bash
{
	
	"exampleFolder":"examples/SQLExample/",
	"lang": "SQL",
	"visualizeConstraints":"y"
}
```
* **SQL Query**:  
```bash
Q = SELECT r.A
    FROM TR AS r , TS AS s, TZ AS z
    WHERE r.C=s.C AND s.D=z.D AND z.E="S"
```

* **View VRS**:  
```bash
VRS = SELECT r.A, s.D 
      FROM TR AS r, TS AS s 
      WHERE r.C=s.C
```


* **View VZ**:  
```bash
VZ = SELECT z.D,z.E
     FROM TZ AS z
```

* The schema of the base tables ``TR ``, ``TS`` and ``TZ`` appears in ``SQLExample/baseschema`` file.

Running the example:
```bash 
$ ./runExample.sh
```
The output:
```
****** Encoded CQ Query *****
Q<sg_0> :- TR(sg_0,sg_1,sg_2),TS(sg_2,sg_4),TZ(sg_4,"S");

****** CQ Rewritings *****
RW0<sg_0> :- VRS(sg_0,sg_4),VZ(sg_4,"S");

****** Decoded Rewritings *****
SELECT VRS.sg_0 
FROM VRS,VZ 
WHERE VRS.sg_4=VZ.sg_4  AND VZ.sg_6='S'
```

### JSON Data Model - SQL++ Language
* In this simple example, the JSON query `Q` and view `V` are expressed in the [SQL++ JSON declarative language](https://www.datanami.com/2018/10/22/how-sql-makes-json-more-queryable/). To run the example, you need to update the `config.json` file as follows:
```bash
{
	
	"exampleFolder":"examples/SPPJExample/",
	"lang": "SQLPlusPlus",
	"visualizeConstraints":"y"
}
```
* **SQL++ Query**:  
```bash
Q = FROM X AS D,  D.Addresses AS AD
    WHERE AD.state="CA"
    SELECT AD
```
* **SQL++ View**:
```bash

V = FROM X AS D
    SELECT {"CAaddresses": FROM D.Addresses AS AD WHERE AD.state="CA" SELECT AD}
```

Running the example:
```bash 
$ ./runExample.sh
```

The output:
```
****** Encoded CQ Query *****
Q<b_10> :- root_sppj_88(b_0),child_sppj(b_0,b_1,"Addresses","o"),child_sppj(b_1,c_2,"state","o"),val_sppj(c_2,"CA"),root_sppj_88(b_0),child_sppj(b_0,b_1,"Addresses","o"),eq_sppj(b_1,b_10);

****** CQ Rewritings *****
RW0<b_10> :- child_sppj_V(SK_0,SK_1,"CAaddresses","o"),child_sppj_V(SK_1,SK_2,SK_3,"a"),V(SK_0),eq_sppj_V(SK_2,b_10);

****** Decoded Rewritings *****
 SELECT SK_2 FROM V AS SK_0,SK_0.CAaddresses AS SK_2
```


### Key-Value Data Model - KQL Language
* In this example, the query `Q` and view `V` are expressed in KQL (Key-value query language). To run the example, you need to update the `config.json` file as follows:
```bash
{
	
	"exampleFolder":"examples/RKExample/",
	"lang": "KQL",
	"visualizeConstraints":"y"
}
```
* **KQL Query**:
```bash

Q = SELECT mathGrade
    FROM AlexInfo IN MAIN ['Alex'], grades IN AlexInfo ['grades'], mathGrade IN grades ['math']
```
* **KQL View**:
```bash
V = SELECT "AlexGrades"->{"Math":mathGrade} 
    FROM AlexInfo IN MAIN ['Alex'], grades IN AlexInfo ['grades'], mathGrade IN grades ['math']
```

* Notice that in this example, the body of the view `V` and query `Q` matches, but in order to rewrite `Q` using `V`, this requires more reasoning about the newly constructed map in the `SELECT` clause in `V`, which the view generated constraints capture. Run the following command to inspect the view `V` generated relational constraints and get the equivalent rewriting.

Running the example:
```bash 
$ ./runExample.sh
```

The output:
``` 
****** Encoded CQ Query *****
Q<f_50> :- collection_rk(g_0,g_1,"grades","im"),collection_rk(g_1,f_3,"0","s"),mainmap_rk_1702218425(f_0),collection_rk(f_0,g_0,"Alex","m"),collection_rk(f_3,g_2,"math","im"),collection_rk(g_2,f_5,"0","s"),eq_rk(f_5,f_50);

****** CQ Rewritings *****
RW0<f_50> :- eq_rk(f_5,f_50),collection_rk_V(f_0,SK_0,"AlexGrades","m"),collection_rk_V(SK_0,f_5,"Math","im"),mainmap_rk_1702218425(f_0);

****** Decoded Rewritings *****
SELECT f_5 FROM  SK_0 IN MAIN["AlexGrades"],f_5 IN SK_0['Math']
```

### Graph Data Model - Cypher Language
* We currently support a fragment of the Cypher graph-based language. This fragment includes
(i) pattern-matching (MATCH), (ii) WHERE clause, containing simple predicates, and (ii) construction of a new node and a relationship between nodes. We do not
support the encoding of ``Variable-length pattern matching`` in the form: (n1)-[``*``]→(n2). This kind
of matching cannot be expressed directly in a conjunctive query form. 

To run the graph example, you need to update the ``config.json`` file as follows:

```bash
{
	
	"exampleFolder":"examples/CGExample/",
	"lang": "CYPHER",
	"visualizeConstraints":"y"
}
```
* **Cypher Query**: Note the following query modified from interactive-short-2 in idbc benchmark.
```bash

MATCH (:Person {personId: "4398046512167"})<-[:HAS_CREATOR]-(m:Message)-[:REPLY_OF]->(p:Post)
MATCH (p)-[:HAS_CREATOR]->(c)
RETURN
  m.commentId as messageId,
  m.creationDate AS messageCreationDate,
  p.postId AS originalPostId,
  c.personId AS originalPostAuthorId,
  c.firstName as originalPostAuthorFirstName,
  c.lastName as originalPostAuthorLastName

```
* **Cypher View**:
```bash
MATCH (po:Post)<-[:REPLY_OF]-(m:Message)-[:HAS_CREATOR]->(p:Person {personId: "4398046512167"})
CREATE (po)<-[:REPLY_OF_v3]-(:Message_v3 {commentId:m.commentId, creationDate:m.creationDate, content:m.content, length:m.length})-[:HAS_CREATOR_v3]->(p)
```
Now, running the command below: 
```bash 
$ ./runExample.sh
```

The output:
```
****** Encoded CQ Query *****
q<v0,v1,v2,v3,v4,v5> :- Label_I(id0,"Person"),Property_I(id0,"personId",dtId0,vId0),Label_I(m,"Message"),Label_I(id1,"HAS_CREATOR"),Connection_I(m,id0,id1),Label_I(p,"Post"),Label_I(id2,"REPLY_OF"),Connection_I(m,p,id2),Label_I(id3,"HAS_CREATOR"),Connection_I(p,c,id3),Property_I(m,"commentId",dtId1,vId1),Property_I(m,"creationDate",dtId2,vId2),Property_I(p,"postId",dtId3,vId3),Property_I(c,"personId",dtId4,vId4),Property_I(c,"firstName",dtId5,vId5),Property_I(c,"lastName",dtId6,vId6),Kind_I(c,"N"),Value_I(vId0,"4398046512167"),Value_I(vId1,v0),Value_I(vId2,v1),Value_I(vId3,v2),Value_I(vId4,v3),Value_I(vId5,v4),Value_I(vId6,v5);

****** CQ Rewritings *****
RW0<v0,v1,v2,v3,v4,v5> :- Connection_I(m,p,id2),Value_I(vId1,v0),
Label_I(id1,"HAS_CREATOR"),Label_I(id2,"REPLY_OF"),
Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),
Property_I(m,"creationDate",dtId2,vId2),Value_I(vId6,v5),
Value_I(vId0,"4398046512167"),Property_I(c,"personId",dtId4,vId4),
Label_I(m,"Message"),Value_I(vId5,v4),Connection_I(m,id0,id1),
Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),
Label_I(id0,"Person"),Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(m,"commentId",dtId1,vId1),Property_I(c,"lastName",dtId6,vId6),Value_I(vId2,v1);

RW1<v0,v1,v2,v3,v4,v5> :- Connection_I(m,p,id2),Value_I(vId1,v0),Label_I(id1,"HAS_CREATOR"),
Label_I(id2,"REPLY_OF"),Label_I(SK_53,"Message_v3"),Connection_I(SK_53,id0,SK_63),
Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),Property_I(m,"creationDate",dtId2,vId2),Value_I(vId6,v5),Property_I(c,"personId",dtId4,vId4),
Label_I(m,"Message"),Value_I(vId5,v4),Connection_I(m,id0,id1),Connection_I(SK_53,p,SK_62),
Label_I(SK_63,"HAS_CREATOR_v3"),Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),Label_I(id0,"Person"),
Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(m,"commentId",dtId1,vId1),
Property_I(c,"lastName",dtId6,vId6),Value_I(vId2,v1);

RW2<v0,v1,v2,v3,v4,v5> :- Value_I(vId1,v0),Label_I(SK_53,"Message_v3"),Connection_I(SK_53,id0,SK_63),Property_I(c,"firstName",dtId5,vId5),Label_I(id3,"HAS_CREATOR"),Value_I(vId6,v5),Property_I(c,"personId",dtId4,vId4),Property_I(SK_53,"creationDate",dtId2,vId2),
Value_I(vId5,v4),Property_I(SK_53,"commentId",dtId1,vId1),Connection_I(SK_53,p,SK_62),
Label_I(SK_63,"HAS_CREATOR_v3"),Property_I(id0,"personId",dtId0,vId0),Value_I(vId4,v3),
Connection_I(p,c,id3),Property_I(p,"postId",dtId3,vId3),Label_I(id0,"Person"),
Label_I(p,"Post"),Value_I(vId3,v2),Kind_I(c,"N"),Property_I(c,"lastName",dtId6,vId6),
Value_I(vId2,v1);

****** Decoded Rewritings *****
MATCH (SK_53:Message_v3)-[SK_63:HAS_CREATOR_v3]->(id0:Person)<-[id1:HAS_CREATOR]-(m:Message)-[id2:REPLY_OF]->(p:Post)<-[SK_62]-(SK_53:Message_v3)
MATCH (p:Post)-[id3:HAS_CREATOR]->(c)
RETURN m.commentId, m.creationDate, c.firstName, c.lastName, p.postId, c.personId


MATCH (c)<-[id3:HAS_CREATOR]-(p:Post)<-[SK_62]-(SK_53:Message_v3)-[SK_63:HAS_CREATOR_v3]->(id0:Person)
RETURN c.firstName, c.lastName, SK_53.creationDate, p.postId, SK_53.commentId, c.personId
```

## Part 2: Cross Model Rewriting Into Integration Plan - End-to-End Scenario
* **Step 1**: Solr Server Setup. Run the following command:
```bash
$ /project/systems/solr-6.0.0/bin/solr start -e cloud
```
Then follow the configuration in the screenshot below: 
<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/SolrSetup.png"  width=800  >
    <br>
</p>

* **Step 2**: PostgreSQL server setup and data loading. Run the following command:
```bash
$ cd /project/dataConfig/
$ ./runSetup.sh >log
```

* **Step 3**: Now, we are ready to run our cross-model rewriting example.
* **Query**: It queries the MIMIC JSON data stored in PostgreSQL(with JSON support). The query asks: "for the female patients transferred into the ICU due to "tachypnea" issue, with "abnormal" blood test results, and they are prescribed "MAIN Potassium Chloride" drug, find their ids, hospital admission ids and date of birth". 
```bash
Q = FOR PJ:{
SELECT P.p->>'SUBJECT_ID' AS SUBJECT_ID, A->>'hadm_id' AS HADM_ID, P.p->>'DOB' AS DOB
FROM mimictutorial.mimicdata AS P, mimictutorial.mimiclabitems AS D, 
	 jsonb_array_elements(P.p->'ADMISSIONS') AS A, 
	 jsonb_array_elements(A->'noteeventst') AS NOTEVEENTS,
	 jsonb_array_elements(A->'labevents') AS LABEVENTS,
	 jsonb_array_elements(A->'icustays') AS ICUSTAYS, 
	 jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS 
	
WHERE D.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	  LABEVENTS->>'flag' = 'abnormal' AND
	  PRESCRIPTIONS->>'drug_type'= 'MAIN' AND
	  PRESCRIPTIONS->>'drug'= 'Potassium Chloride' AND 
	  D.data->>'FLUID'='Blood' AND
	  P.p->>'GENDER' ='F' AND
	  NOTEVEENTS->>'text' LIKE '%tachypnea%'} 

RETURN	SUBJECT_ID, HADM_ID, DOB
```
* **Cross-Model Views**: Consider that we had at our disposal the following three views:

* **Text View** : It stores the IDs of patients and their hospital admission ids, as well as the caregivers’ reports, including notes on the patients’ stay (e.g., detailed diagnosis, etc). The view is materialized in Solr.
```bash
mimicTutoText= 
FOR  PJ:{ 
SELECT P.p->'SUBJECT_ID' AS SUBJECT_ID, A->'hadm_id' AS HADM_ID, NOTEVEENTS->'text' AS TEXT 
FROM mimictutorial.mimicdata AS P, 
	jsonb_array_elements(P.p->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteeventst') AS NOTEVEENTS}
	 
RETURN	 SJ:{ solr_build_object ( "SUBJECT_ID":SUBJECT_ID, "HADM_ID":HADM_ID, "TEXT":TEXT )}
```
* **Relational View** : It stores the IDs of female patients and their date of birth. The view is materialized in PostgreSQL (relational).  
```bash
VPJPR= 
FOR  PJ:{ 
SELECT P.p->'SUBJECT_ID' AS SUBJECT_ID , P.p->'DOB' AS DOB
FROM mimictutorial.mimicdata AS P
WHERE P.p->>'GENDER' ="F" } 
RETURN	 PR:{SUBJECT_ID,DOB}
```
* **JSON View** : It stores all drugs information that are prescribed for patients with "abnormal blood" test results. The view is materialized in Postgres (JSON).  

```bash
VPJPJ = FOR PJ:{
SELECT P.p->'SUBJECT_ID' AS SUBJECT_ID,  A->'hadm_id' AS HADM_ID, ICUSTAYS->'prescriptions' AS PRESCRIPTIONS
FROM  mimictutorial.mimicdata AS P, mimictutorial.mimiclabitems AS D, 
	  jsonb_array_elements(P.p->'ADMISSIONS') AS A, 
	  jsonb_array_elements(A->'labevents') AS LABEVENTS,
	  jsonb_array_elements(A->'icustays') AS ICUSTAYS 
		
WHERE  D.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	   D.data->>'FLUID'='Blood' AND
	   LABEVENTS->>'flag' = 'abnormal' }
	   
RETURN	 PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID, 'HADM_ID',HADM_ID,'PRESCRIPTIONS', PRESCRIPTIONS) }
```
* **Running End-to-End Scenario**: To run the scenario, you need to update the `config.json` file as follows:
```bash
{
	
	"exampleFolder":"examples/Scenario/",
	"lang": "QBT",
	"visualizeConstraints":"n"
}
```

The query appears in the ``Query.Q`` file  and the views in ``.V`` files under ``examples/Scenario`` folder.

```bash
$ cd /project/estocada/estocada-tutorial/
$ ./runExample.sh
```

ESTOCADA finds the cross-model views-based rewriting, shows the relational encoding of the query, the relational rewriting of the query, and the integration plan using Tatooine (with the decoded sources' subqueries). You can view the generated integration plan in your host mahchine under ``mnt/RWTatooinePlanScenario.png`` (click on the image below and zoom in to view the generated plan of the scenario).

<p>
    <img src="https://gitlab.inria.fr/cedar/estocada-public/-/raw/master/estocada-MDD-tutorial/doc/RWTatooinePlanScenario.png"  width=1000  >
    <br>
</p>

Also, the script executes the original query, the rewriting, and prints their results in ``Scenario/QueryResults.csv`` and ``Scenario/RewritingResults.csv`` files. You can view the generated 
constraints in ``Scenario/constraints_chase`` and ``Scenario/constraints_bkchase``files.

Now, let's change the query by removing the text search predicate. The new query is: 
```bash
Q = FOR PJ:{
SELECT P.p->>'SUBJECT_ID' AS SUBJECT_ID, A->>'hadm_id' AS HADM_ID, P.p->>'DOB' AS DOB
FROM mimictutorial.mimicdata AS P, mimictutorial.mimiclabitems AS D, 
	 jsonb_array_elements(P.p->'ADMISSIONS') AS A, 
	 jsonb_array_elements(A->'labevents') AS LABEVENTS,
	 jsonb_array_elements(A->'icustays') AS ICUSTAYS, 
	 jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS 
	
WHERE D.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	  LABEVENTS->>'flag' = 'abnormal' AND
	  PRESCRIPTIONS->>'drug_type'= 'MAIN' AND
	  PRESCRIPTIONS->>'drug'= 'Potassium Chloride' AND 
	  D.data->>'FLUID'='Blood' AND
	  P.p->>'GENDER' ='F'} 

RETURN	SUBJECT_ID, HADM_ID, DOB
```
Then, run again ``runExample.sh``. Notice that the plan of the new view-based rewriting of the query will no longer access the text view in Solr. The rewriting will only use the ``VPJPJ`` and ``VPJPR``views.

