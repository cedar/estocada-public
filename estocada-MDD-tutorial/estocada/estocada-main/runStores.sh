mvn exec:java -Dexec.mainClass="fr.inria.oak.estocada.stores.executores.StoresExperiments" -Dexec.args="M" -Dexec.cleanupDaemonThreads=false > resultM 2>&1
mvn exec:java -Dexec.mainClass="fr.inria.oak.estocada.stores.executores.StoresExperiments" -Dexec.args="S" -Dexec.cleanupDaemonThreads=false > resultS 2>&1
mvn exec:java -Dexec.mainClass="fr.inria.oak.estocada.stores.executores.StoresExperiments" -Dexec.args="A" -Dexec.cleanupDaemonThreads=false > resultA 2>&1
mvn exec:java -Dexec.mainClass="fr.inria.oak.estocada.stores.executores.StoresExperiments" -Dexec.args="P" -Dexec.cleanupDaemonThreads=false > resultP 2>&1

