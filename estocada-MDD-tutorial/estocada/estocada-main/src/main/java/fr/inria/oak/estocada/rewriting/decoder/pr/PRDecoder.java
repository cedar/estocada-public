package fr.inria.oak.estocada.rewriting.decoder.pr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSQLEval;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.IntegerConstant;
import fr.inria.oak.commons.conjunctivequery.StringConstant;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.exceptions.QueryTranslationException;
import fr.inria.oak.estocada.rewriting.logical.Decoder;
import fr.inria.oak.estocada.rewriting.logical.Semantics;

/**
 * PRDecoder implements the decoder interface and returns LogSQLEval operator.
 *
 * @author Romain Primet
 * @author ranaalotaibi (Clean up Romain's implementation)
 */
public class PRDecoder implements Decoder {
    protected final Semantics semantics;
    private static final Logger LOGGER = Logger.getLogger(PRDecoder.class);
    private String translatedQuery;

    public PRDecoder(final Semantics semantics) {
        if (semantics == null) {
            throw new IllegalArgumentException("Non-null semantics expected.");
        }
        this.semantics = semantics;
    }

    @Override
    public LogOperator decode(final Catalog catalog, final ConjunctiveQuery conjunctiveQuery)
            throws TatooineExecutionException {
        try {
            final NRSMD nrsmd = inferMetadata(catalog, conjunctiveQuery);
            final PRTranslator trans = new PRTranslator(nrsmd, catalog, semantics);
            final String query = trans.translate(conjunctiveQuery);
            translatedQuery = query;
            LOGGER.info("Translated SQL Query : " + query);
            final String viewName = conjunctiveQuery.getBody().iterator().next().getPredicate().toString();
            return new LogSQLEval(query, nrsmd, catalog.getStorageReference(viewName));
        } catch (QueryTranslationException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Infer the metadata of the query head
     * 
     * @param catalog
     *            the catalog
     * @param conjunctiveQuery
     *            the conjunctive query
     * @return
     * @throws TatooineExecutionException
     */
    protected static NRSMD inferMetadata(final Catalog catalog, final ConjunctiveQuery conjunctiveQuery)
            throws TatooineExecutionException {
        final ArrayList<NRSMD> nrsmds = new ArrayList<NRSMD>();
        final Collection<Term> head = conjunctiveQuery.getHead();
        for (Term term : head) {
            if (term.isVariable()) {
                final Variable variable = (Variable) term;
                nrsmds.add(columnNRSMD(catalog, conjunctiveQuery, variable));
            } else {
                if (term instanceof IntegerConstant) {
                    nrsmds.add(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, null));
                } else if (term instanceof StringConstant) {
                    nrsmds.add(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, null));
                } else {
                    throw new IllegalStateException("Cannot infer metadata from constant " + term);
                }
            }
        }
        try {
            return NRSMD.appendNRSMDList(nrsmds);
        } catch (TatooineExecutionException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Get the column NRSMD
     * 
     * @param catalog
     *            the catalog
     * @param conjunctiveQuery
     *            the conjunctive query
     * @param variable
     *            the variable
     * @return
     * @throws TatooineExecutionException
     */
    protected static NRSMD columnNRSMD(final Catalog catalog, final ConjunctiveQuery conjunctiveQuery,
            final Variable variable) throws TatooineExecutionException {
        final AtomPos atomPos = findFirstAtomContainingEx(conjunctiveQuery, variable);
        final NRSMD nrsmd = catalog.getViewSchema(atomPos.atom.getPredicate()).getNRSMD();
        final List<String> name = new ArrayList<String>();
        name.add(nrsmd.getColNames()[atomPos.pos]);
        return new NRSMD(1, new TupleMetadataType[] { nrsmd.getColumnsMetadata()[atomPos.pos] }, name);

    }

    /**
     * Find first atom containing ex
     * 
     * @param conjunctiveQuery
     *            the conjunctive query
     * @param variable
     *            the variable
     * @return
     */
    protected static AtomPos findFirstAtomContainingEx(final ConjunctiveQuery conjunctiveQuery,
            final Variable variable) {
        for (Atom atom : conjunctiveQuery.getBody()) {
            int idx = atom.indexOf(variable);
            if (idx != -1) {
                return new AtomPos(atom, idx);
            }
        }
        throw new IllegalArgumentException(
                "No atom containing the variable " + variable + " was found in query " + conjunctiveQuery);
    }

    /**
     * Get translated query
     * 
     * @return translatedQuery
     */
    public String tranlstedQuery() {
        return translatedQuery;
    }
}

class AtomPos {
    Atom atom;
    int pos;

    AtomPos(Atom atom, int pos) {
        this.atom = atom;
        this.pos = pos;
    }
}
