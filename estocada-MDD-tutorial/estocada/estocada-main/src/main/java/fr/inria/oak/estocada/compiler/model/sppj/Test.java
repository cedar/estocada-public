package fr.inria.oak.estocada.compiler.model.sppj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveModule;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.TimedReformulations;

public class Test {
    private static final Logger LOGGER = Logger.getLogger(Test.class);
    private static final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testSPPJ/constraints_chase";
    private static final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testSPPJ/constraints_bkchase";
    private static final String OUTPUT_SCHEMA_FILE = "src/main/resources/testSPPJ/schemas";
    private static final String INPUT_QUERY_FILE = "src/main/resources/testSPPJ/v.view";
    private static final String INPUT_QUERY_FILE2 = "src/main/resources/testSPPJ/Q.view";

    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        Injector injector = null;
        switch (COMPILER) {
            case 0:
                injector = Guice.createInjector(new SPPJNaiveModule());
                break;
        }

        SPPJQueryBlockTreeBuilder builder = injector.getInstance(SPPJQueryBlockTreeBuilder.class);
        final QueryBlockTree qbt = builder.buildQueryBlockTree(FileUtils.readFileToString(new File(INPUT_QUERY_FILE)));
        LOGGER.debug(qbt.toString());
        Context context = null;
        switch (COMPILER) {
            case 0:
                SPPJNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class);
                context = compiler0.compileContext(qbt, new RelationalSchema(new ArrayList<Relation>()), true);
                break;
        }

        LOGGER.debug(context.getForwardConstraints());
        LOGGER.debug(context.getBackwardConstraints());

        Utils.writeConstraints(OUTPUT_FORWARD_CONSTRAINTS_FILE, context.getForwardConstraints());
        Utils.writeConstraints(OUTPUT_BACKWARD_CONSTRAINTS_FILE, context.getBackwardConstraints());
        Utils.writeSchemas(OUTPUT_SCHEMA_FILE, context.getGlobalSchema(), context.getTargetSchema());
        Utils.writeConstraints(OUTPUT_FORWARD_CONSTRAINTS_FILE, context.getForwardConstraints());
        Utils.writeConstraints(OUTPUT_BACKWARD_CONSTRAINTS_FILE, context.getBackwardConstraints());
        Utils.writeSchemas(OUTPUT_SCHEMA_FILE, context.getGlobalSchema(), context.getTargetSchema());

        final QueryBlockTree qbtQuery =
                builder.buildQueryBlockTree(FileUtils.readFileToString(new File(INPUT_QUERY_FILE2)));
        LOGGER.debug(qbtQuery.toString());

        final Context context01 = fr.inria.oak.estocada.rewriter.server.Utils.parseContext(OUTPUT_SCHEMA_FILE,
                OUTPUT_FORWARD_CONSTRAINTS_FILE, OUTPUT_BACKWARD_CONSTRAINTS_FILE);
        final ConjunctiveQueryRewriter rewriter = new PACBConjunctiveQueryRewriter(context01);
        final BufferedReader query = new BufferedReader(new FileReader(new File("src/main/resources/testSPPJ/query")));
        final TimedReformulations timedRewritings = rewriter.getTimedReformulations(
                fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query)));
        System.out.println(timedRewritings.getRewritings());
    }
}
