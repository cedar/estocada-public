package fr.inria.oak.estocada.compiler.model.tm;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.estocada.compiler.QueryBlockTree;

public class Test {
    private static final Logger LOGGER = Logger.getLogger(Test.class);
    private static final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testTM/constraints_chase";
    private static final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testTM/constraints_bkchase";
    private static final String OUTPUT_SCHEMA_FILE = "src/main/resources/testTM/schemas";
    private static final String INPUT_QUERY_FILE = "src/main/resources/testTM/v.view";

    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        Injector injector = null;
        switch (COMPILER) {
            case 0:
                injector = Guice.createInjector(new TMNaiveModule());
                break;
        }

        TMQueryBlockTreeBuilder builder = injector.getInstance(TMQueryBlockTreeBuilder.class);
        final QueryBlockTree nbt = builder.buildQueryBlockTree(FileUtils.readFileToString(new File(INPUT_QUERY_FILE)));
        LOGGER.debug(nbt.toString());
    }
}
