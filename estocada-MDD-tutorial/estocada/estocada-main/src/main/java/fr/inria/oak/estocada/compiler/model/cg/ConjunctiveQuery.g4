
grammar ConjunctiveQuery;

@header{
    package fr.inria.oak.estocada.compiler.model.cg;
}

conjunctiveQuery: queryHeader ':-' queryBody;

queryHeader: RwName '<' Identifier (',' Identifier)* '>';

queryBody: (labelI | kindI | propertyI | compI | valueI | connectionI | nestValueI) ( ',' (labelI | kindI | propertyI | compI | valueI | connectionI | nestValueI))* ';';

labelI: 'Label_I' '(' Identifier ',' StringConstant ')';

kindI: 'Kind_I' '(' Identifier ',' ('"N"' | '"E"') ')';

propertyI: 'Property_I' '(' Identifier ',' StringConstant ',' Identifier ',' Identifier ')';

compI: ('Eq_I' | 'Ne_I' | 'Lt_I' | 'Gt_I' | 'Le_I' | 'Ge_I') '(' Identifier ',' Identifier ')';

valueI: 'Value_I' '(' Identifier ',' (Identifier | StringConstant) ')';

connectionI: 'Connection_I' '(' Identifier ',' Identifier ',' Identifier ')';

nestValueI: 'NestValue_I' '(' Identifier ',' Identifier ',' StringConstant ',' ('"a"' | '"o"') ')';


RwName: 'RW' [0-9]+;

Identifier: ([a-zA-Z0-9] | [-|_])+;

StringConstant: '"' [a-zA-Z0-9,.!?; _'"-]+ '"';


WS : [ \t\r\n]+ -> skip;