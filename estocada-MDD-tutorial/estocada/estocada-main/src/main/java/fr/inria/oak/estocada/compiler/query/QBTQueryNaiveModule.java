package fr.inria.oak.estocada.compiler.query;

import fr.inria.oak.estocada.compiler.model.qbt.QBTModule;

public class QBTQueryNaiveModule extends QBTModule {
	@Override
	protected String getPropertiesFileName() {
		return "mixed.properties";
	}
}
