package fr.inria.oak.estocada.rewriting.decoder.sm;

import java.io.File;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Utils;

/** Temporary Main Test **/
public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/main/resources/testSM/decoderTest");

    public static void main(String[] args) throws Exception {
        final ConjunctiveQuery rewriting = Utils.parseQuery((INPUT_QUERY_FILE));
        SMTranslator smTanslator = new SMTranslator(rewriting);
        smTanslator.translate();
        long time = 0;
        for (int i = 1; i <= 99; i++) {
            long start = System.nanoTime();
            smTanslator = new SMTranslator(rewriting);
            smTanslator.translate();
            long end = System.nanoTime();
            time += (end - start);
        }
        System.out.println(((time / 99) * 1e-9));
    }

}
