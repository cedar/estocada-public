package fr.inria.oak.estocada.compiler.model.pm;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class PMModel extends Model {
    public final static String ID = "PM";

    @Inject
    public PMModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
        super(ID, Format.MATRIX, Language.MADLIB, queryBlockTreeBuilder, blockEncoder);
    }
}
