package fr.inria.oak.estocada.compiler.exceptions;

/**
 * 
 * {@code EstocadaException} is the superclass of those
 * exceptions that can be thrown by ESTOCADA components.
 * 
 * @author Rana Alotaibi
 *
 */
public class EstocadaException extends RuntimeException {
    static final long serialVersionUID = 6924186540900115003L;

    /**
     * Constructs a new estocada exception with {@code null} as its
     * detail message.
     */
    public EstocadaException() {
        super();
    }

    /**
     * Constructs a new estocada exception with the specified detail message.
     * 
     * @param message
     *            the detail message.
     */
    public EstocadaException(String message) {
        super(message);
    }

    /**
     * Constructs a new estocada exception with the specified detail message and
     * cause.
     * 
     * @param message
     *            the detail message.
     * @param cause
     *            the cause. (A <tt>null</tt> value is
     *            permitted, and indicates that the cause is nonexistent or
     *            unknown.)
     */
    public EstocadaException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new estocada exception with the specified cause and a
     * detail message.
     *
     * @param cause
     *            the cause. (A <tt>null</tt> value is
     *            permitted, and indicates that the cause is nonexistent or
     *            unknown.)
     */
    public EstocadaException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new estocada exception with the specified detail
     * message, cause, suppression enabled or disabled, and writable
     * stack trace enabled or disabled.
     *
     * @param message
     *            the detail message.
     * @param cause
     *            the cause.
     * @param enableSuppression
     *            whether or not suppression is enabled
     *            or disabled
     * @param writableStackTrace
     *            whether or not the stack trace should
     *            be writable
     *
     */
    protected EstocadaException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
