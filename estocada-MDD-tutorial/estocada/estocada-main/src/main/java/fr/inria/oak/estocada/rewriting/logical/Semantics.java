package fr.inria.oak.estocada.rewriting.logical;

/**
 * Supported semantics.
 *
 * @author Damian Bursztyn
 */
public enum Semantics {
    BAG,
    SET;
}