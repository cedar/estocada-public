package fr.inria.oak.estocada.rewriting.physical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogPJSQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSolrEval;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLREval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;

/**
 * Temporary class to construct different PhyEvalOperator.
 * 
 * @author ranaalotaibi
 *
 */
public class PhyEvalConstructorUtils {
    private static final Logger LOGGER = Logger.getLogger(PhyEvalConstructorUtils.class);
    private static String subQuery = null;

    /**
     * Construct PhyPJQLEval
     * 
     * @param logPJSQLEval
     *            the logical PJQLEval.
     * @param bindPatternIndexes
     *            the indexes of NRSMD column to be joined with in case this operator is a right operator in the bind join.
     * @return The newly constructed PJQLPhyEval.
     * @throws Exception
     */
    private static PhyPostgresJSONEval constructPhyPJQLEvalWithBindCondition(final LogPJSQLEval logPJSQLEval,
            final int[] bindPatternIndexes, final NRSMD nrsmd) throws Exception {
        final String updatedQuery = updatePJQueryWithBindCondition(logPJSQLEval.getQueryString(),
                logPJSQLEval.getNRSMD(), bindPatternIndexes, nrsmd);
        LOGGER.debug("PJQL with Bind Condition: " + updatedQuery);
        return new PhyPostgresJSONEval(updatedQuery, logPJSQLEval.getGSR(), logPJSQLEval.getNRSMD());

    }

    /**
     * Update the PJQL query with a bind condition
     * 
     * @param nrsmd
     *            the nrsmd to get the name of columns to be joined with.
     * @param bindPatternIndexes
     *            the index of the columns to be added in the bind join condition.
     * @return The updated query with a bind condition.
     * @throws Exception
     */
    private static String updatePJQueryWithBindCondition(final String query, final NRSMD nrsmd,
            final int[] bindPatternIndexes, final NRSMD JoinNRSMD) throws Exception {
        final StringBuilder builder = new StringBuilder(query);
        final Map<String, String> varBindings = new HashMap<>();
        final Pattern pattern = Pattern.compile("SELECT(\\s+DISTINCT)?\\s+(.*?)\\s+FROM");
        final Matcher mtacher = pattern.matcher(query);
        if (!mtacher.find() || mtacher.group(2) == null || mtacher.group(2).isEmpty()) {
            throw new Exception("No Match found!");
        }

        //Binding Order -- TODO:FIX
        final List<String> nrsmdColumns = new ArrayList<>();
        final List<String> JoinNRSMCols = Arrays.asList(JoinNRSMD.getColNames());
        for (int i = 0; i < bindPatternIndexes.length; i++) {
            nrsmdColumns.add(JoinNRSMCols.get(bindPatternIndexes[i]));
        }
        for (final String column : Arrays.asList(mtacher.group(2).trim().split(","))) {
            final String[] varBinding = column.split("AS");
            for (int i = 0; i < nrsmdColumns.size(); i++) {
                if (nrsmdColumns.get(i).equals(varBinding[1].trim())) {
                    varBindings.put(nrsmdColumns.get(i), varBinding[0].trim());
                }
            }
        }
        boolean containsWhere = false;
        if (!(query.contains("WHERE") || query.contains("where"))) {
            containsWhere = true;
            builder.append(" WHERE ");
        }
        for (int i = 0; i < nrsmdColumns.size(); i++) {
            if (containsWhere && i == 0) {
                builder.append(varBindings.get(nrsmdColumns.get(i)) + "=?");
                continue;
            }
            builder.append(" AND " + varBindings.get(nrsmdColumns.get(i)) + "=?");
        }
        subQuery = builder.toString();
        return subQuery;
    }

    /**
     * Construct PhySQLEval
     * 
     * @param LogSQLEval
     *            the logical LogSQLEval operator.
     * @param bindPatternIndexes
     *            the indexes of NRSMD column to be joined with in case this operator is a right operator in the bind join.
     * @return The newly constructed PhySQLEval.
     * @throws Exception
     */
    private static PhySQLEval constructPhySQLEvalWithBindCondition(final LogSQLEval logSQLEval,
            final int[] bindPatternIndexes) throws Exception {
        final String updatedQuery =
                updateSQLQueryWithBindCondition(logSQLEval.getQueryString(), logSQLEval.getNRSMD(), bindPatternIndexes);
        LOGGER.debug("SQL with Bind Condition: " + updatedQuery);
        return new PhySQLEval(updatedQuery, logSQLEval.getStorageReference(), logSQLEval.getNRSMD());

    }

    /**
     * Construct PhySolrEval
     * 
     * @param LogSQLEval
     *            the logical LogSQLEval operator.
     * @param bindPatternIndexes
     *            the indexes of NRSMD column to be joined with in case this operator is a right operator in the bind join.
     * @return The newly constructed PhySQLEval.
     * @throws Exception
     */
    private static PhySOLREval constructPhySOLREvalWithBindCondition(final LogSolrEval logSolrEval,
            final int[] bindPatternIndexes) throws Exception {
        final String updatedFields =
                updateSOLRQueryWithBindCondition(logSolrEval.getFields(), logSolrEval.getNRSMD(), bindPatternIndexes);
        LOGGER.info("Solr with Bind Condition: " + logSolrEval.getQueryString() + updatedFields);
        return new PhySOLREval(logSolrEval.getQueryString() + updatedFields, logSolrEval.getFields(),
                (GSR) logSolrEval.getGSR(), logSolrEval.getNRSMD());

    }

    /**
     * Update the SQL query with a bind condition
     * 
     * @param nrsmd
     *            the nrsmd to get the name of columns to be joined with.
     * @param bindPatternIndexes
     *            the index of the columns to be added in the bind join condition.
     * @return The updated query with a bind condition.
     * @throws Exception
     */
    private static String updateSQLQueryWithBindCondition(final String query, final NRSMD nrsmd,
            final int[] bindPatternIndexes) throws Exception {

        final StringBuilder builder = new StringBuilder(query);
        final List<String> joinColumns = new ArrayList<String>();
        final Pattern pattern = Pattern.compile("SELECT(\\s+DISTINCT)?\\s+(.*?)\\s+FROM");
        final Matcher mtacher = pattern.matcher(query);
        if (!mtacher.find() || mtacher.group(2) == null || mtacher.group(2).isEmpty()) {
            throw new Exception("No Match found!");
        }
        final List<String> nrsmdColumns = Arrays.asList(nrsmd.getColNames());
        for (final String column : Arrays.asList(mtacher.group(2).trim().split(","))) {
            for (int i = 0; i < bindPatternIndexes.length; i++) {
                final String[] varBinding = column.split("AS");
                if (nrsmdColumns.get(i).equals(varBinding[0].trim())) {
                    joinColumns.add(varBinding[0].trim());
                }
            }
        }
        boolean containsWhere = false;
        if (!(query.contains("WHERE") || query.contains("where"))) {
            containsWhere = true;
            builder.append(" WHERE ");
        }
        for (int i = 0; i < joinColumns.size(); i++) {
            if (containsWhere && i == 0) {
                builder.append(joinColumns.get(i) + "=?");
                continue;
            }

            builder.append(" AND " + joinColumns.get(i) + "=?");

        }
        subQuery = builder.toString();

        return subQuery;

    }

    /**
     * Update the SOLR query with a bind condition (Placeholder)
     * 
     * @param nrsmd
     *            the nrsmd to get the name of columns to be joined with.
     * @param bindPatternIndexes
     *            the index of the columns to be added in the bind join condition.
     * @return The updated query with a bind condition.
     * @throws Exception
     */
    private static String updateSOLRQueryWithBindCondition(final String query, final NRSMD nrsmd,
            final int[] bindPatternIndexes) throws Exception {
        final StringBuilder builder = new StringBuilder();
        final List<String> fields = Arrays.asList(query.split(","));
        final List<String> nrsmdColumns = Arrays.asList(nrsmd.getColNames());
        for (final String column : fields) {
            for (int i = 0; i < bindPatternIndexes.length; i++) {
                if (nrsmdColumns.get(i).equals(column)) {
                    builder.append("&");
                    builder.append(column);
                    builder.append(":");
                    builder.append("%s");

                }
            }
        }
        subQuery = builder.toString();

        return subQuery;

    }

    /**
     * Construct the bind access for a given logical operator.
     * 
     * @param logOperator
     *            the logical operator
     * @param bindPatternIndexes
     *            the indexes of NRSMD column to be joined with.
     * @return This function returns that bind access physical operator for a given logical operator.
     * @throws Exception
     */
    public static BindAccess constructBindAccess(final LogOperator logOperator, final int[] bindPatternIndexes)
            throws Exception {

        if (logOperator instanceof LogSQLEval) {
            return constructPhySQLEvalWithBindCondition((LogSQLEval) logOperator, bindPatternIndexes);
        }
        if (logOperator instanceof LogSolrEval) {

            return constructPhySOLREvalWithBindCondition((LogSolrEval) logOperator, bindPatternIndexes);

        }
        if (logOperator instanceof LogPJSQLEval) {

            return constructPhyPJQLEvalWithBindCondition((LogPJSQLEval) logOperator, bindPatternIndexes, null);

        }
        throw new Exception(logOperator.getName() + " physical constructor is not implemented!");
    }

    /**
     * Construct the bind access for a given logical operator.
     * 
     * @param logOperator
     *            the logical operator
     * @param bindPatternIndexes
     *            the indexes of NRSMD column to be joined with.
     * @return This function returns that bind access physical operator for a given logical operator.
     * @throws Exception
     */
    public static BindAccess constructBindAccessNew(final LogOperator logOperator, final int[] bindPatternIndexes,
            final NRSMD nrsmd) throws Exception {

        if (logOperator instanceof LogSQLEval) {
            return constructPhySQLEvalWithBindCondition((LogSQLEval) logOperator, bindPatternIndexes);
        }
        if (logOperator instanceof LogSolrEval) {

            return constructPhySOLREvalWithBindCondition((LogSolrEval) logOperator, bindPatternIndexes);

        }
        if (logOperator instanceof LogPJSQLEval) {

            return constructPhyPJQLEvalWithBindCondition((LogPJSQLEval) logOperator, bindPatternIndexes, nrsmd);

        }
        throw new Exception(logOperator.getName() + " physical constructor is not implemented!");
    }

    public static String getSubQuery() {
        return subQuery;
    }
}
