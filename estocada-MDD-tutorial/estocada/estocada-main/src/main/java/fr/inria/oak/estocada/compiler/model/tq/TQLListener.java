// Generated from TQL.g4 by ANTLR 4.5.3

package fr.inria.oak.estocada.compiler.model.tq;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TQLParser}.
 */
public interface TQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TQLParser#tqlquery}.
	 * @param ctx the parse tree
	 */
	void enterTqlquery(TQLParser.TqlqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#tqlquery}.
	 * @param ctx the parse tree
	 */
	void exitTqlquery(TQLParser.TqlqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(TQLParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(TQLParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(TQLParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(TQLParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#sfwQuery}.
	 * @param ctx the parse tree
	 */
	void enterSfwQuery(TQLParser.SfwQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#sfwQuery}.
	 * @param ctx the parse tree
	 */
	void exitSfwQuery(TQLParser.SfwQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void enterSelectClause(TQLParser.SelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void exitSelectClause(TQLParser.SelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectStatement(TQLParser.SelectStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectStatement(TQLParser.SelectStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#selectItem}.
	 * @param ctx the parse tree
	 */
	void enterSelectItem(TQLParser.SelectItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#selectItem}.
	 * @param ctx the parse tree
	 */
	void exitSelectItem(TQLParser.SelectItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#returnVariable}.
	 * @param ctx the parse tree
	 */
	void enterReturnVariable(TQLParser.ReturnVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#returnVariable}.
	 * @param ctx the parse tree
	 */
	void exitReturnVariable(TQLParser.ReturnVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void enterFromClause(TQLParser.FromClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#fromClause}.
	 * @param ctx the parse tree
	 */
	void exitFromClause(TQLParser.FromClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void enterVarBinding(TQLParser.VarBindingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void exitVarBinding(TQLParser.VarBindingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#columnvariableBinding}.
	 * @param ctx the parse tree
	 */
	void enterColumnvariableBinding(TQLParser.ColumnvariableBindingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#columnvariableBinding}.
	 * @param ctx the parse tree
	 */
	void exitColumnvariableBinding(TQLParser.ColumnvariableBindingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#source}.
	 * @param ctx the parse tree
	 */
	void enterSource(TQLParser.SourceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#source}.
	 * @param ctx the parse tree
	 */
	void exitSource(TQLParser.SourceContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#columnLookUp}.
	 * @param ctx the parse tree
	 */
	void enterColumnLookUp(TQLParser.ColumnLookUpContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#columnLookUp}.
	 * @param ctx the parse tree
	 */
	void exitColumnLookUp(TQLParser.ColumnLookUpContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#columnVar}.
	 * @param ctx the parse tree
	 */
	void enterColumnVar(TQLParser.ColumnVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#columnVar}.
	 * @param ctx the parse tree
	 */
	void exitColumnVar(TQLParser.ColumnVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#key}.
	 * @param ctx the parse tree
	 */
	void enterKey(TQLParser.KeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#key}.
	 * @param ctx the parse tree
	 */
	void exitKey(TQLParser.KeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#stringKey}.
	 * @param ctx the parse tree
	 */
	void enterStringKey(TQLParser.StringKeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#stringKey}.
	 * @param ctx the parse tree
	 */
	void exitStringKey(TQLParser.StringKeyContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#filename}.
	 * @param ctx the parse tree
	 */
	void enterFilename(TQLParser.FilenameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#filename}.
	 * @param ctx the parse tree
	 */
	void exitFilename(TQLParser.FilenameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(TQLParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link TQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(TQLParser.VariableContext ctx);
}