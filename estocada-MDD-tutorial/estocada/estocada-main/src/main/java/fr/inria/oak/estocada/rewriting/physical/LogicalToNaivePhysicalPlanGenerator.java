package fr.inria.oak.estocada.rewriting.physical;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * A class that generates and executes a naive physical plan. The join order is random and not optimized.
 * 
 * @author ranaalotaibi
 *
 */
public class LogicalToNaivePhysicalPlanGenerator {
    private static final Logger LOGGER = Logger.getLogger(LogicalToNaivePhysicalPlanGenerator.class);
    private List<String> subQueires;
    final LogOperator rootOperator;

    /** Constructor **/
    public LogicalToNaivePhysicalPlanGenerator(final LogOperator rootOperator) {
        this.rootOperator = rootOperator;
        subQueires = new ArrayList<>();
    }

    /**
     * Return the root of the physical plan.
     * 
     * @return return the root.
     */
    public NIterator generate() {
        if (rootOperator == null) {
            throw new IllegalArgumentException("Expected logical root operator!");
        }
        final LogPlanListener logPlanListener = new LogPlanListener();
        rootOperator.accept(logPlanListener);
        subQueires = logPlanListener.getSubQueries();
        return logPlanListener.physicalPlanRootOperator();
    }

    public List<String> getSubQueries() {
        return subQueires;
    }
}
