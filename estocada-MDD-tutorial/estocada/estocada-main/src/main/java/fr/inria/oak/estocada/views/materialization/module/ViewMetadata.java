package fr.inria.oak.estocada.views.materialization.module;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;

/**
 * Register the metadata of the materialized view.
 * 
 * @author ranaalotaibi
 *
 */
public final class ViewMetadata {

    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(ViewMetadata.class);
    private final Catalog catalog;
    private final StorageReference storageReference;
    private final String viewName;
    private final NRSMD nrsmd;

    /** Constructor **/
    public ViewMetadata(final Catalog catalog, final StorageReference storageReference, final String viewName,
            final NRSMD nrsmd) {
        this.catalog = checkNotNull(catalog);
        this.storageReference = checkNotNull(storageReference);
        this.viewName = checkNotNull(viewName);
        this.nrsmd = checkNotNull(nrsmd);
    }

    /**
     * Register the view meta data in the catalog.
     * 
     * @throws MaterializerException
     */
    public void register() throws MaterializerException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Registering" + viewName + "in the catalog");
        }
        if (!isRegistered()) {
            final ViewSchema viewSchema = new ViewSchema(nrsmd, new HashMap<String, Integer>());
            catalog.add(viewName, storageReference, null, viewSchema);
        }
        throw new MaterializerException("View metadata cannot be registered.");
    }

    /**
     * Returns <code>true</code> if the view already registered.
     * 
     * @return <code>true</code> if the view already registered.
     */
    private boolean isRegistered() {
        return (catalog.getViewSchema(viewName) == null) ? false : true;
    }

    /**
     * Returns the storage reference of the view.
     * 
     * @return
     */
    public StorageReference getStorageReference() {

        return storageReference;
    }

    /**
     * Returns the view name.
     * 
     * @return the view name.
     */
    public String getViewName() {
        return viewName;
    }

    /**
     * Returns the view NRSMD.
     * 
     * @return the view NRSMD.
     */
    public NRSMD getViewNRSMD() {
        return nrsmd;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ View name :" + viewName + "-");
        stringBuilder.append("[ NRSMD :" + nrsmd.toString() + "-");
        stringBuilder.append("[ StorageReference :" + storageReference.toString() + "]");
        return stringBuilder.toString();
    }
}
