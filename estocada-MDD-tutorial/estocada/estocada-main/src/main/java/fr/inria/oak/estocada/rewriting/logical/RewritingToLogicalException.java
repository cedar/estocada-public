package fr.inria.oak.estocada.rewriting.logical;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Used when an exception occurs during translation from rewriting to a logical plan.
 *
 * @author ranaalotaibi
 */
public final class RewritingToLogicalException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with the specified illegal state
     * exception.
     *
     * @param e
     *            the detail message.
     */
    public RewritingToLogicalException(Exception e) {
        super(checkNotNull(e));
    }
}
