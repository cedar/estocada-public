package fr.inria.oak.estocada.compiler.model.qbt;

import static java.sql.Types.INTEGER;
import static java.sql.Types.VARCHAR;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.model.pr.Relation;

/**
 * QBT StructuralListener
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger log = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener() {
        log.setLevel(Level.OFF);
    }

    @Override
    public void enterQbtPattern(QBTParser.QbtPatternContext ctx) {
        log.debug("Pattern: " + ctx.getText());
        switch (ctx.annotation().getText()) {
            case "AJ":
                addPattern(QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder
                        .buildPattern("for " + AntlrUtils.getFullText(ctx.modelPattern().ajPattern())));
                QBTQueryBlockTreeBuilder.variableMapper
                        .addAll((QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder.getVariableMapper()));
                break;
            case "PJ":
                final Pattern pattern = QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder
                        .buildQueryBlockTree(AntlrUtils.getFullText(ctx.modelPattern().pjPattern())).getRoot()
                        .getPattern();
                addPattern(pattern);
                //                QBTQueryBlockTreeBuilder.variableMapper
                //                        .addAll((QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder.getVariableMapper()));

                final Map<String, Variable> tempPJ =
                        QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder.getVariableMapper().getVariableMapping();
                final Map<String, Variable> tempQBT = QBTQueryBlockTreeBuilder.variableMapper.getVariableMapping();
                for (Map.Entry<String, Variable> entry : tempPJ.entrySet()) {
                    if (tempQBT.containsKey(entry.getKey())) {
                        QBTQueryBlockTreeBuilder.variableMapper.updateVariable(entry.getKey(), entry.getValue());
                    }
                    if (!(tempQBT.containsKey(entry.getKey()))) {
                        QBTQueryBlockTreeBuilder.variableMapper.addVariable(entry.getKey(), entry.getValue());
                    }
                }

                break;
            case "SPPJ":
                addPattern(QBTQueryBlockTreeBuilder.sppjBlockNestedTreeBuilder
                        .buildPattern("FROM " + AntlrUtils.getFullText(ctx.modelPattern().sppjPattern())));
                QBTQueryBlockTreeBuilder.variableMapper
                        .addAll((QBTQueryBlockTreeBuilder.sppjBlockNestedTreeBuilder.getVariableMapper()));
                break;
            case "SJ":
                addPattern(QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder
                        .buildPattern(AntlrUtils.getFullText(ctx.modelPattern().sjPattern())));
                QBTQueryBlockTreeBuilder.variableMapper
                        .addAll((QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder.getVariableMapper()));
                break;
            case "RK":
                addPattern(QBTQueryBlockTreeBuilder.rkBlockNestedTreeBuilder
                        .buildPattern(AntlrUtils.getFullText(ctx.modelPattern().rkPattern())));
                QBTQueryBlockTreeBuilder.variableMapper
                        .addAll((QBTQueryBlockTreeBuilder.rkBlockNestedTreeBuilder.getVariableMapper()));
                break;
            case "PR":
                //                addPattern(QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder
                //                        .buildPattern(AntlrUtils.getFullText(ctx.modelPattern().prPattern())));
                //                QBTQueryBlockTreeBuilder.variableMapper
                //                        .addAll((QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder.getVariableMapper()));
                QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder.setRelations(getRelations());
                final Pattern pattern1 = QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder
                        .buildQueryBlockTree(AntlrUtils.getFullText(ctx.modelPattern().prPattern())).getRoot()
                        .getPattern();
                addPattern(pattern1);

                final Map<String, Variable> temp1 =
                        QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder.getVariableMapper().getVariableMapping();
                final Map<String, Variable> temp2 = QBTQueryBlockTreeBuilder.variableMapper.getVariableMapping();
                for (Map.Entry<String, Variable> entry : temp1.entrySet()) {
                    if (temp2.containsKey(entry.getKey())) {
                        QBTQueryBlockTreeBuilder.variableMapper.updateVariable(entry.getKey(), entry.getValue());
                    }
                    if (!(temp2.containsKey(entry.getKey()))) {
                        QBTQueryBlockTreeBuilder.variableMapper.addVariable(entry.getKey(), entry.getValue());
                    }
                }
                //                QBTQueryBlockTreeBuilder.variableMapper
                //                        .addAll((QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder.getVariableMapper()));
                break;
        }
    }

    @Override
    protected ParserRuleContext createParseTree(QBTParser parser) {
        return parser.qbtForPattern();
    }

    //TODO:FIX-This hard coded
    private Map<String, Relation> getRelations() {
        Map<String, Relation> relations = new HashMap<String, Relation>();
        relations.put("PatientsF",
                new Relation("PatientsF", Arrays.asList(new String[] { "SUBJECT_ID", "gender", "DOB" }),
                        Arrays.asList(new Integer[] { INTEGER, VARCHAR, VARCHAR })));

        return relations;

    }
}
