package fr.inria.oak.estocada.compiler.model.xq.alternative.secondstep;

import fr.inria.oak.estocada.compiler.model.xq.XQueryModule;

public class XQAlternativeModule extends XQueryModule {
	@Override
	protected String getPropertiesFileName() {
		return "xq.alternative.second_step.properties";
	}
}
