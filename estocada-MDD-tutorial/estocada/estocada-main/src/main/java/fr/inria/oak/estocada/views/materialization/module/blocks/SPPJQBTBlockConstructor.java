package fr.inria.oak.estocada.views.materialization.module.blocks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyAsterixDBJSONEval;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.qbt.walker.QBTMBaseListener;
import fr.inria.oak.estocada.qbt.walker.QBTMLexer;
import fr.inria.oak.estocada.qbt.walker.QBTMParser;
import fr.inria.oak.estocada.views.materialization.module.Annotation;;

/**
 * Constructs SPPJ QBT block.
 * The SPPJ QBT block comprises of a list of variables appear in the block and
 * the constructed AsterixBD physical operator to be evaluated by the source.
 * 
 * @author ranaalotaibi
 *
 */
public final class SPPJQBTBlockConstructor implements QBTBlockConstructor {
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(SPPJQBTBlockConstructor.class);
    /** SPPJ block query **/
    private final String sppjQuery;

    /** Constructor **/
    public SPPJQBTBlockConstructor(final String sppjQuery) {
        this.sppjQuery = sppjQuery;

    }

    @Override
    public QBTPattern constructQBTBlock() throws ParseException, TatooineExecutionException {
        final QBTMLexer lexer = new QBTMLexer(CharStreams.fromString(sppjQuery));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTMParser parser = new QBTMParser(tokens);
        final ParserRuleContext tree = parser.sppjQuery();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final SPPJListenerAux listener = new SPPJListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return new QBTPattern(sppjQuery, Annotation.SPPJ, listener.getBlockVariables(),
                new PhyAsterixDBJSONEval(sppjQuery, listener.getGSR(), listener.getBlockNRSMD()),
                listener.getBlockNRSMD());
    }

    /** Construct SPPJ QBT Block **/
    private class SPPJListenerAux extends QBTMBaseListener {
        private GSR gsr;
        private Catalog catalog;
        private NRSMD datasetNRSMD;
        private List<String> blockVariables;
        private String key;
        private Map<String, String> projectedVariablesMapping;
        private NRSMD blockNRSMD;

        /** Constructor **/
        public SPPJListenerAux() {
            catalog = Catalog.getInstance();
            blockVariables = new ArrayList<>();
            projectedVariablesMapping = new HashMap<String, String>();

        }

        /** Get storage reference **/
        public GSR getGSR() {
            return gsr;
        }

        /** Get block projected variables **/
        public List<String> getBlockVariables() {
            return blockVariables;
        }

        /** Get the block variables NRSDM **/
        public NRSMD getBlockNRSMD() {
            return blockNRSMD;
        }

        @Override
        public void enterSppjSource(QBTMParser.SppjSourceContext ctx) {
            LOGGER.debug("Entering SPPJRelation: " + ctx.dataSetName().getText());
            gsr = (GSR) catalog.getStorageReference(ctx.dataSetName().getText());
            datasetNRSMD = catalog.getViewSchema(ctx.dataSetName().getText()).getNRSMD();
        }

        @Override
        public void enterSppjVariable(QBTMParser.SppjVariableContext ctx) {
            LOGGER.debug("Entering SPPJVariable: " + ctx.getText());
            blockVariables.add(ctx.NAME().getText());
        }

        @Override
        public void exitSppjQuery(QBTMParser.SppjQueryContext ctx) {
            LOGGER.debug("Exiting SppjQuery: " + ctx.getText());
            final List<NRSMD> nrsmds = new ArrayList<NRSMD>();
            try {
                nrsmds.addAll(getFieldsNRSMD());
                blockNRSMD = NRSMD.appendNRSMDList(nrsmds);
            } catch (TatooineExecutionException exception) {
                LOGGER.error("Cannot append NRSMDs :" + exception.getMessage());
            }
        }

        @Override
        public void enterSppjObjStep(QBTMParser.SppjObjStepContext ctx) {
            LOGGER.debug("Entering SppjObjStep: " + ctx.getText());
            key = ctx.key().getText();
        }

        @Override
        public void enterSppjSelectVar(QBTMParser.SppjSelectVarContext ctx) {
            LOGGER.debug("Entering SppjSelectVar: " + ctx.getText());
            final String alias = ctx.sppjVariable().NAME().getText();
            projectedVariablesMapping.put(key.replace("\'", ""), alias);

        }

        /**
         * Get the NRSMD of the projected variables.
         * 
         * @throws TatooineExecutionException
         **/
        private List<NRSMD> getFieldsNRSMD() throws TatooineExecutionException {
            final List<NRSMD> projectedNRSMD = new ArrayList<NRSMD>();
            final List<String> colNames = Arrays.asList(datasetNRSMD.getColNames());
            final TupleMetadataType[] tupleMetadataType = datasetNRSMD.getColumnsMetadata();

            for (Map.Entry<String, String> entery : projectedVariablesMapping.entrySet()) {
                if (colNames.contains(entery.getKey())) {
                    int colInx[] = new int[1];
                    colInx[0] = datasetNRSMD.getColIndexFromName(entery.getKey());
                    final List<String> newColName = new ArrayList<>();
                    newColName.add(entery.getValue());
                    NRSMD nrsmd = null;
                    final TupleMetadataType meta = tupleMetadataType[datasetNRSMD.getColIndexFromName(entery.getKey())];
                    if (meta.equals(TupleMetadataType.TUPLE_TYPE)) {
                        String[] itemsArray = new String[newColName.size()];
                        itemsArray = newColName.toArray(itemsArray);

                        nrsmd = new NRSMD(1, NRSMD.makeProjectRSMD(datasetNRSMD, colInx).getColumnsMetadata(),
                                itemsArray, new NRSMD[] { datasetNRSMD
                                        .getNestedChild(datasetNRSMD.getColIndexFromName(entery.getKey())) });
                        projectedNRSMD.add(nrsmd);
                    } else {
                        nrsmd = new NRSMD(1, NRSMD.makeProjectRSMD(datasetNRSMD, colInx).getColumnsMetadata(),
                                newColName);
                        projectedNRSMD.add(nrsmd);
                    }
                }
            }
            return projectedNRSMD;
        }
    }
}
