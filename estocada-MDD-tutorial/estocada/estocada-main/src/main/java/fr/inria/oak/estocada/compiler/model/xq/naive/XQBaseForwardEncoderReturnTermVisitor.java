package fr.inria.oak.estocada.compiler.model.xq.naive;

import java.util.Map.Entry;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnLeafTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.model.xq.Predicate;
import fr.inria.oak.estocada.compiler.model.xq.Utils;

abstract class XQBaseForwardEncoderReturnTermVisitor implements
		ReturnTemplateVisitor {
	protected ImmutableList.Builder<Atom> builder;
	protected String viewName;

	@Inject
	public XQBaseForwardEncoderReturnTermVisitor() {
	}

	@Override
	public void visit(final ReturnTemplate template) {
		// NOP (no encoding for the template optionals)
	}

	@Override
	public void visitPost(final ReturnConstructTerm term) {
		// NOP (no encoding after the children are processed)
	}

	@Override
	public void visitPre(final ReturnConstructTerm term) {
		builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName,
				getParentBlockCreatedNode(term), term.getCreatedNode()));
		builder.add(new Atom(Predicate.TAG.toString() + "_" + viewName, term
				.getCreatedNode(), term.getElement().toTerm()));
		for (final Entry<String, ReturnLeafTerm> attr : term.getAttributes()
				.entrySet()) {
			builder.add(new Atom(Predicate.HAS_ATTRIBUTE.toString() + "_"
					+ viewName, term.getCreatedNode(), Utils.toTerm(attr
					.getKey())));
		}
	}

	@Override
	public void visit(final ReturnVariableTerm term) {
		final Predicate predicate = Utils.isText(term) ? Predicate.TEXT
				: Predicate.CHILD;
		builder.add(new Atom(predicate.toString() + "_" + viewName,
				getParentBlockCreatedNode(term), term.getCreatedNode()));
	}

	@Override
	public void visit(final ReturnStringTerm term) {
		builder.add(new Atom(Predicate.TEXT.toString() + "_" + viewName,
				getParentBlockCreatedNode(term), term.toTerm()));
	}

	protected abstract Term getParentBlockCreatedNode(final ReturnTerm term);
}