package fr.inria.oak.estocada.stores.executores;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fr.inria.oak.estocada.utils.Utils;

/**
 * 
 * Stores Experiments
 * 
 * @author Rana Alotaibi
 *
 */
public class StoresExperiments {
    private static final Logger LOGGER = Logger.getLogger(StoresExperiments.class);

    public static void main(String[] args) {
        //final String argVal = args[0];
        final String argVal = "M";
        /* Run baseline query evaluations*/
        switch (argVal) {
            case "M":
                MongoDBExp();
                break;
            case "S":
                SparkSQL();
                break;
            case "A":
                AsterixDB();
                break;
            case "P":
                PostgresJSON();
                break;
        }
    }

    /**
     * Execute MongoDB queries
     */
    private static void MongoDBExp() {

        final String OUTPUT = "src/main/resources/nativeQueries/output";
        final String queriesDirectory = "src/main/resources/nativeQueries/mongodb/";
        final Map<String, Long> mongoDBQueryExTime = new HashMap<String, Long>();
        final MongoDBExecutor mongoDBExecutor = new MongoDBExecutor();
        mongoDBExecutor.createConnection();
        //mongoDBExecutor.createIndexes();
        File directory = new File(queriesDirectory);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File child : directoryListing) {
                if (child.getName().equals("Q_59.json")) {
                    final String queryID = child.getName();
                    try {
                        final BufferedReader bufferReader = new BufferedReader(new FileReader(child));
                        final JsonParser parser = new JsonParser();
                        final JsonObject queryJSON = parser.parse(bufferReader).getAsJsonObject();
                        int i = 0;
                        long total = 0;
                        while (i < 1) {
                            total += mongoDBExecutor.executeQuery(queryJSON.toString());
                            i++;
                        }
                        mongoDBQueryExTime.put(queryID, total / 3);
                        Utils.writeQueriesTimes(OUTPUT, mongoDBQueryExTime);
                    } catch (Exception e) {
                        LOGGER.info(e.getMessage());
                    }
                }
            }

        }

    }

    /**
     * Execute AsterixDB queries
     */
    private static void AsterixDB() {
        final String OUTPUT = "src/main/resources/nativeQueries/output";
        final String queriesDirectory = "src/main/resources/nativeQueries/asterixdb/";
        final AsterixDBExecutor asterixDBExecutor = new AsterixDBExecutor();
        asterixDBExecutor.createConnection();
        final Map<String, Long> asterixDBQueryExTime = new HashMap<String, Long>();
        File directory = new File(queriesDirectory);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File child : directoryListing) {
                LOGGER.info(child.getName());
                final String queryID = child.getName();
                try {
                    final BufferedReader query = new BufferedReader(new FileReader(child));
                    int i = 0;
                    long total = 0;
                    while (i < 3) {
                        total += asterixDBExecutor.executeQuery(IOUtils.toString(query));
                        i++;
                    }
                    asterixDBQueryExTime.put(queryID, total / 3);
                    Utils.writeQueriesTimes(OUTPUT, asterixDBQueryExTime);
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                }
            }
        }

        asterixDBExecutor.destroy();

    }

    /**
     * Execute PostgreJSON queries
     */
    private static void PostgresJSON() {
        final String OUTPUT = "src/main/resources/nativeQueries/output";
        final String queriesDirectory = "src/main/resources/nativeQueries/postgresjson/";
        final PostgresJSONExecutor postgresJSONExecutor = new PostgresJSONExecutor();
        postgresJSONExecutor.createConnection();
        final Map<String, Long> asterixDBQueryExTime = new HashMap<String, Long>();
        File directory = new File(queriesDirectory);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File child : directoryListing) {
                LOGGER.info(child.getName());
                final String queryID = child.getName();
                try {
                    final BufferedReader query = new BufferedReader(new FileReader(child));
                    int i = 0;
                    long total = 0;
                    while (i < 3) {
                        total += postgresJSONExecutor.executeQuery(IOUtils.toString(query));
                        i++;
                    }
                    asterixDBQueryExTime.put(queryID, total / 3);
                    Utils.writeQueriesTimes(OUTPUT, asterixDBQueryExTime);
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                }
            }
        }
    }

    /**
     * Execute SparkSQL queries
     */
    private static void SparkSQL() {
        final String OUTPUT = "src/main/resources/nativeQueries/output";
        final String queriesDirectory = "src/main/resources/nativeQueries/SparkSQL(JSON)/input";
        final SparkSQLExecutor sparkJSONExecutor = new SparkSQLExecutor();
        sparkJSONExecutor.createConnection();
        final Map<String, Long> asterixDBQueryExTime = new HashMap<String, Long>();
        File directory = new File(queriesDirectory);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File child : directoryListing) {
                LOGGER.info(child.getName());
                final String queryID = child.getName();
                try {
                    final BufferedReader query = new BufferedReader(new FileReader(child));
                    int i = 0;
                    long total = 0;
                    while (i < 3) {
                        total += sparkJSONExecutor.executeQuery(IOUtils.toString(query));
                        i++;
                    }
                    asterixDBQueryExTime.put(queryID, total / 3);
                    Utils.writeQueriesTimes(OUTPUT, asterixDBQueryExTime);
                } catch (Exception e) {
                    LOGGER.info(e.getMessage());
                }
            }
        }
        sparkJSONExecutor.destroy();
    }
}
