package fr.inria.oak.estocada.compiler.model.tm;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class TMModel extends Model {
    public final static String ID = "TM";

    @Inject
    public TMModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
        super(ID, Format.MATRIX, Language.TFM, queryBlockTreeBuilder, blockEncoder);
    }
}
