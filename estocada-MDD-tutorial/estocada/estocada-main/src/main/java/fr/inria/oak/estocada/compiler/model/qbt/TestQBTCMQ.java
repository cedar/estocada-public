package fr.inria.oak.estocada.compiler.model.qbt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.pj.full.Predicate;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.Comment;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.utils.RunnerUtils;

public class TestQBTCMQ {
    private static final String CHARSET = "utf-8";
    private static final String INPUT_FILE = "src/main/resources/testQBTCMQ/";
    private static final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testQBTCMQ/constraints_chase";
    private static final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testQBTCMQ/constraints_bkchase";
    private static final String OUTPUT_SCHEMA_FILE = "src/main/resources/testQBTCMQ/schemas";
    private static Writer writerFW;
    private static Writer writerBW;
    private static Writer writerSchema;

    private static final int COMPILER = 1;
    private static final Logger log = Logger.getLogger(TestQBTCMQ.class);
    private static Injector injector = null;

    public static void main(String[] args) throws Exception {
        writerFW = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(OUTPUT_FORWARD_CONSTRAINTS_FILE), CHARSET));
        writerBW = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(OUTPUT_BACKWARD_CONSTRAINTS_FILE), CHARSET));

        writerSchema = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(OUTPUT_SCHEMA_FILE), CHARSET));
        switch (COMPILER) {
            case 1:
                injector = Guice.createInjector(new QBTNaiveModule());
                long time = System.currentTimeMillis();
                test1();
                System.out.println(System.currentTimeMillis() - time);
                break;
        }

    }

    static void test1() throws Exception {
        final String qbtQuery = RunnerUtils.readQuery(INPUT_FILE);
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        //CMVQ - Query 
        final QueryBlockTree nbt = builder.buildQueryBlockTree(qbtQuery);
        final ConjunctiveQuery query = Utils.parseQuery(getCQ(nbt));
        System.out.println(query);
        QBTQueryBlockTreeBuilder.reIntilize();

        //SMV
        final File directory = new File(INPUT_FILE);
        File[] files = directory.listFiles();
        final List<QueryBlockTree> views = new ArrayList<QueryBlockTree>();
        final QBTNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
        final StringBuilder fwStrBuilder = new StringBuilder();
        final StringBuilder bwStrBuilder = new StringBuilder();
        final List<Constraint> fw = new ArrayList<>();
        final List<Constraint> bw = new ArrayList<>();
        RelationalSchema globalSchema = null;
        RelationalSchema targetSchema = null;
        for (File file : files) {
            if (file.getName().contains(".V")) {
                String view = FileUtils.readFileToString(file);
                try {
                    QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
                    QueryBlockTree nbtView = mixedBuilder.buildQueryBlockTree(view);
                    System.out.print(nbtView);
                    views.add(nbtView);

                    final Context context = compiler0.compileContext(nbtView,
                            new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()),
                            true);

                    context.getForwardConstraints().stream()
                            .forEach(r -> fwStrBuilder.append(r.toString()).append("\n"));
                    fw.addAll(context.getForwardConstraints());
                    context.getBackwardConstraints().stream()
                            .forEach(r -> bwStrBuilder.append(r.toString()).append("\n"));
                    bw.addAll(context.getBackwardConstraints());
                    QBTQueryBlockTreeBuilder.reIntilize();
                } catch (fr.inria.oak.estocada.compiler.exceptions.ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        //        fr.inria.oak.estocada.compiler.Utils.writeConstraints(OUTPUT_FORWARD_CONSTRAINTS_FILE, fw);
        //        fr.inria.oak.estocada.compiler.Utils.writeConstraints(OUTPUT_BACKWARD_CONSTRAINTS_FILE, bw);

        //********************
        //* Find rewriting
        //********************
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
        }

        System.out.println(reformulations.get(0));

    }

    private static List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
        return compilers;
    }

    private static List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private static List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts,
            final Injector injector) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

    public static void writeConstraintsFW(final Collection<? extends Constraint> constraints) throws IOException {
        try {
            for (final Constraint constraint : constraints) {
                writerFW.write(constraint.toString() + (constraint instanceof Comment ? "" : ";\n"));
            }
        } catch (IOException e) {
            throw e;
        }
    }

    public static List<Atom> modifiedPremise(final List<Atom> premise) {
        final List<Atom> copyList = new ArrayList<>(premise);
        final List<Atom> updatedAtoms = new ArrayList<>();
        final List<Term> joinTerms = new ArrayList<Term>();
        final Iterator<Atom> itertaor = copyList.listIterator();
        while (itertaor.hasNext()) {
            final Atom atom = itertaor.next();
            if (atom.getTerms().size() == 2 && atom.getPredicate().equals(Predicate.VAL.toString())) {
                if (atom.getTerms().get(0) instanceof Variable && atom.getTerms().get(1) instanceof Variable) {
                    joinTerms.add(atom.getTerms().get(0));
                    joinTerms.add(atom.getTerms().get(1));
                    itertaor.remove();
                }
            }
        }
        for (final Atom atom : copyList) {
            int index = -1;
            if (atom.getTerms().contains(joinTerms.get(0))) {
                index = atom.getTerms().indexOf(joinTerms.get(0));

            } else {
                if (atom.getTerms().contains(joinTerms.get(1))) {
                    index = atom.getTerms().indexOf(joinTerms.get(1));
                } else {
                    updatedAtoms.add(atom);
                }
            }
            if (index != -1) {
                final List<Term> terms = new ArrayList<Term>();
                terms.addAll(atom.getTerms());
                terms.set(index, joinTerms.get(0));
                final Atom atomNew = new Atom(atom.getPredicate(), terms);
                updatedAtoms.add(atomNew);
            }
        }
        return updatedAtoms;
    }

    final static String getCQ(final QueryBlockTree nbt) {
        final StringBuilder cqBuilder = new StringBuilder();
        cqBuilder.append(nbt.getQueryName());
        cqBuilder.append("<");
        int count = 0;
        List<ReturnTerm> terms = nbt.getRoot().getReturnTemplate().getTerms();
        for (final ReturnTerm term : terms) {
            cqBuilder.append(term.getReferredVariables().iterator().next());
            if (count != (terms.size() - 1))
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(">:-");
        //Body (Path)
        final List<Atom> atomsPath = modifiedPremise(nbt.getRoot().getPattern()
                .encoding(fr.inria.oak.estocada.compiler.model.pj.full.Utils.conditionEncoding));
        count = 0;
        for (final Atom atom : atomsPath) {
            //            if (atom.getPredicate().contains("child_pj")) {
            //                if (atom.getTerm(2).toString().contains("SUBJECT_ID")) {
            //                    if (atom.getTerm(1).toString().contains("b_6")) {
            //                        count++;
            //                        continue;
            //                    }
            //                }
            //            }
            cqBuilder.append(atom);
            if (count != atomsPath.size() - 1)
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(";");
        return cqBuilder.toString();
    }

}
