package fr.inria.oak.estocada.rewriting.decoder.pj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;

/**
 * Translate the rewriting into PJQL query.
 * 
 * @author ranaalotaibi
 *
 */
public class PJTranslatorTypeFixedCol {

    private static final Logger LOGGER = Logger.getLogger(PJTranslatorTypeFixedCol.class);
    private static final Character QUOTE = '\'';
    private static final Character LParenthesis = '(';
    private static final Character RParenthesis = ')';
    private static final String SELECT = " SELECT ";
    private static final String FROM = " FROM ";
    private static final String JSON_ARRAYS_ELEMENTS = " JSONB_ARRAY_ELEMENTS ";
    private static final String ARROW = "->";
    private static final String EQUAL = "=";
    private static final String DOT = ".";
    private static final String CAST = "::";
    private static final String DOUBLEARROW = "->>";
    private static final String AS = " AS ";
    private static final String WHERE = " WHERE ";
    private static final String AND = " AND ";
    private static final String COMMA = ",";
    private final ConjunctiveQuery query;
    private String jsonCol;
    private String schema;
    private NRSMD nrsmd;
    private Variable rootVar;
    private StringBuilder fromClause;
    private StringBuilder whereClause;
    private StringBuilder selectClause;
    private String alias;

    /* Constructor */
    public PJTranslatorTypeFixedCol(final ConjunctiveQuery query) {
        this.query = query;
    }

    /* Constructor */
    public PJTranslatorTypeFixedCol(final ConjunctiveQuery query, final NRSMD nrsmd, final String jsonCol,
            final String schema) {
        this(query);
        this.nrsmd = nrsmd;
        this.jsonCol = jsonCol;
        this.schema = schema;

    }

    /**
     * Translate the given rewriting into PJQL query
     * 
     * @return
     */
    public String translate() {
        final StringBuilder pjqlQuery = new StringBuilder();
        fromClause = new StringBuilder(translateFromClause());
        whereClause = new StringBuilder(translateWhereClause());
        selectClause = new StringBuilder(translateSelectClause());
        pjqlQuery.append(selectClause);
        pjqlQuery.append(fromClause);
        pjqlQuery.append(whereClause);
        return pjqlQuery.toString();
    }

    /**
     * Translate the from clause
     * 
     * @return the translated select clause
     */
    private String translateFromClause() {
        final StringBuilder fromClause = new StringBuilder();
        final Collection<Atom> queryBody = new ArrayList<>(query.getBody());
        for (final Atom atom : queryBody) {
            //Case root predicate
            if (atom.getTerms().size() == 1) {
                rootVar = (Variable) atom.getTerm(0);
                fromClause.append(FROM);
                fromClause.append((schema == null ? "" : schema));
                fromClause.append(atom.getPredicate());
                fromClause.append(AS);
                fromClause.append(atom.getTerm(0));
                alias = atom.getTerm(0).toString();
                if (queryBody.size() != 2) {
                    fromClause.append(COMMA);
                }
                continue;
            }
            //Case not nested and not appear in the head
            if (atom.getTerms().size() > 2) {
                final List<Term> bodyTerms = getAllTerms(query.getBody());
                final List<Term> term = new ArrayList<>();
                term.add(atom.getTerms().get(1));
                bodyTerms.retainAll(term);
                if (bodyTerms.size() == 1)
                    continue;

            }
            //Case navigation into variable should appear in where clause and not the from clause
            if (atom.getTerms().size() > 2 && getAllConditionalTerms(query.getBody()).contains(atom.getTerm(1))) {
                continue;
            }
            //Case nested 
            if (atom.getTerms().size() > 2 && !(atom.getTerm(3).toString().replace("\"", "").equals("a"))
                    && !(query.getHead().contains(atom.getTerms().get(1)))) {
                fromClause.append(JSON_ARRAYS_ELEMENTS);
                if (atom.getTerm(0).equals(rootVar)) {
                    fromClause.append(LParenthesis + atom.getTerm(0).toString() + DOT + jsonCol + ARROW + QUOTE
                            + atom.getTerm(2).toString().replace("\"", "") + QUOTE + RParenthesis + AS
                            + atom.getTerm(1).toString());
                    fromClause.append(COMMA);
                } else {
                    fromClause.append(LParenthesis + atom.getTerm(0).toString() + ARROW + QUOTE
                            + atom.getTerm(2).toString().replace("\"", "") + QUOTE + RParenthesis + AS
                            + atom.getTerm(1).toString());
                    fromClause.append(COMMA);
                }

            }
        }
        if (!fromClause.substring(fromClause.toString().length() - 1).equals(COMMA))
            return fromClause.toString();
        else
            return fromClause.substring(0, fromClause.toString().length() - 1);
    }

    /**
     * Translate the where clause
     * 
     * @return The translated where clause.
     */
    private String translateWhereClause() {
        final StringBuilder whereClause = new StringBuilder();
        boolean flag = false;
        for (final Atom atomOuter : query.getBody()) {
            if (atomOuter.getPredicate().startsWith("val")) {
                for (final Atom atomInner : query.getBody()) {
                    if (atomInner.getTerms().size() == 4 && atomOuter.getTerm(0).equals(atomInner.getTerm(1))
                            && !atomInner.getTerm(0).equals(rootVar)) {
                        if (!flag) {
                            whereClause.append(WHERE);
                            whereClause.append(atomInner.getTerm(0));
                            whereClause.append(DOUBLEARROW);
                            whereClause.append(atomInner.getTerm(2).toString().replace("\"", "'"));
                            whereClause.append(EQUAL);
                            whereClause.append(atomOuter.getTerm(1).toString().replace("\"", "'"));
                            flag = true;

                        } else {
                            whereClause.append(AND);
                            whereClause.append(atomInner.getTerm(0));
                            whereClause.append(DOUBLEARROW);
                            whereClause.append(atomInner.getTerm(2).toString().replace("\"", "'"));
                            whereClause.append(EQUAL);
                            whereClause.append(atomOuter.getTerm(1).toString().replace("\"", "'"));
                        }
                    }

                    if (atomInner.getTerms().size() == 4 && atomOuter.getTerm(0).equals(atomInner.getTerm(1))
                            && atomInner.getTerm(0).equals(rootVar)) {

                        if (!flag) {
                            whereClause.append(WHERE);
                            whereClause.append(atomInner.getTerm(0));
                            whereClause.append('.');
                            whereClause.append(jsonCol);
                            whereClause.append(DOUBLEARROW);
                            whereClause.append(atomInner.getTerm(2).toString().replace("\"", "'"));
                            whereClause.append(EQUAL);
                            whereClause.append(atomOuter.getTerm(1).toString().replace("\"", "'"));
                            flag = true;

                        } else {
                            whereClause.append(AND);
                            whereClause.append(atomInner.getTerm(0));
                            whereClause.append('.');
                            whereClause.append(jsonCol);
                            whereClause.append(DOUBLEARROW);
                            whereClause.append(atomInner.getTerm(2).toString().replace("\"", "'"));
                            whereClause.append(EQUAL);
                            whereClause.append(atomOuter.getTerm(1).toString().replace("\"", "'"));
                        }
                    }

                }

            }
        }
        return whereClause.toString();
    }

    /**
     * Translate select clause (Type checking is important to introduce the cast operator
     * This is special case for PostgresJSON).
     * 
     * @return This function returns the translated SELECT clause
     * @throws TatooineExecutionException
     *             Tatooine exception due to the use of the NRSMD to infer the column type and add the cast operator in the query head.
     */
    private String translateSelectClause() {

        final StringBuilder selectClause = new StringBuilder();
        final Collection<Atom> queryBody = new ArrayList<>(query.getBody());
        final List<Term> head = new ArrayList<>(query.getHead());
        final TupleMetadataType[] types = nrsmd.getColumnsMetadata();
        selectClause.append(SELECT);
        try {
            for (final Term term : head) {
                for (final Atom atom : queryBody) {
                    if (atom.getTerms().size() > 2 && atom.getTerm(1).equals(term)) {
                        int colIndx;
                        //colIndx = nrsmd.getColIndexFromName(atom.getTerm(1).toString());
                        colIndx = nrsmd.getColIndexFromName(atom.getTerm(2).toString().replace("\"", ""));

                        TupleMetadataType colType = types[colIndx];
                        if (colType.equals(TupleMetadataType.INTEGER_TYPE)) {
                            if (rootVar.equals(atom.getTerm(0))) {
                                selectClause.append(LParenthesis);
                                selectClause.append(atom.getTerm(0).toString());
                                selectClause.append(DOT);
                                selectClause.append(jsonCol);
                                selectClause.append(DOUBLEARROW);
                                selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                selectClause.append(RParenthesis);
                            } else {
                                selectClause.append(LParenthesis);
                                selectClause.append(atom.getTerm(0).toString());
                                selectClause.append(DOUBLEARROW);
                                selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                selectClause.append(RParenthesis);
                            }
                            selectClause.append(CAST + "INT");
                            selectClause.append(AS + term);
                        } else {
                            if (colType.equals(TupleMetadataType.STRING_TYPE)) {
                                if (rootVar.equals(atom.getTerm(0))) {
                                    selectClause.append(LParenthesis);
                                    selectClause.append(atom.getTerm(0).toString());
                                    selectClause.append(DOT);
                                    selectClause.append(jsonCol);
                                    selectClause.append(DOUBLEARROW);
                                    selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                    selectClause.append(RParenthesis);
                                } else {
                                    selectClause.append(LParenthesis);
                                    selectClause.append(atom.getTerm(0).toString());
                                    selectClause.append(DOUBLEARROW);
                                    selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                    selectClause.append(RParenthesis);
                                }
                                selectClause.append(CAST + "TEXT");
                                selectClause.append(AS + term);
                            }

                            else {
                                if (rootVar.equals(atom.getTerm(0))) {
                                    selectClause.append(atom.getTerm(0).toString());
                                    selectClause.append(DOT);
                                    selectClause.append(jsonCol);
                                    selectClause.append(DOUBLEARROW);
                                    selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                } else {
                                    selectClause.append(atom.getTerm(0).toString());
                                    selectClause.append(DOUBLEARROW);
                                    selectClause.append(atom.getTerm(2).toString().replace("\"", "\'"));
                                }
                                selectClause.append(AS + term);
                            }
                        }

                    }
                }
                selectClause.append(COMMA);
            }
        } catch (TatooineExecutionException e) {
            LOGGER.error("NRSMD Execption:-" + e.getMessage());
        }
        if (!selectClause.substring(selectClause.toString().length() - 1).equals(COMMA))
            return selectClause.toString();
        else
            return selectClause.substring(0, selectClause.toString().length() - 1);

    }

    /**
     * Get all terms that appear in query body.
     * 
     * @param atoms
     *            the query body atoms
     * @return
     */
    private List<Term> getAllTerms(final Collection<Atom> atoms) {
        final List<Term> terms = new ArrayList<>();
        for (final Atom atom : atoms) {
            terms.addAll(atom.getTerms());
        }
        return terms;
    }

    /**
     * Get all conditional terms.
     * 
     * @param atoms
     *            the query atoms
     * @return return a list of conditional terms.
     */
    private List<Term> getAllConditionalTerms(final Collection<Atom> atoms) {
        final List<Term> terms = new ArrayList<>();
        for (final Atom atom : atoms) {
            if (atom.getPredicate().startsWith("val"))
                terms.add(atom.getTerm(0));
        }
        return terms;
    }

    public StringBuilder getFromClauseBuilder() {
        return fromClause;
    }

    public StringBuilder getWhereClauseBuilder() {
        return whereClause;
    }

    public StringBuilder getSelectClauseBuilder() {
        return selectClause;
    }

    public String getAlias() {
        return alias;
    }
}
