// Generated from QBTE.g4 by ANTLR 4.7.2

package fr.inria.oak.estocada.qbt.executor;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class QBTELexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, NAME=19, WHITESPACE=20, STRING=21, RETURN=22, AJ=23, RK=24, 
		PR=25, SPPJ=26, PJ=27, SJ=28, XQ=29, TQ=30, AND=31, FROM=32, IN=33, SELECT=34, 
		VALUE=35, AS=36, LIKE=37, QUERY=38, USE=39;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
			"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
			"T__17", "NAME", "WHITESPACE", "STRING", "ESCAPE", "RETURN", "AJ", "RK", 
			"PR", "SPPJ", "PJ", "SJ", "XQ", "TQ", "AND", "FROM", "IN", "SELECT", 
			"VALUE", "AS", "LIKE", "QUERY", "USE", "UNICODE", "HEX", "A", "B", "C", 
			"D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", 
			"R", "S", "T", "U", "V", "W", "X", "Y", "Z"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'FOR'", "','", "':'", "'{'", "'}'", "'PR'", "'PJ'", "'SJ'", "'('", 
			"')'", "'AND'", "'='", "'/'", "'query?q='", "'&'", "'fl='", "'WHERE'", 
			"'RETURN'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, "NAME", "WHITESPACE", "STRING", 
			"RETURN", "AJ", "RK", "PR", "SPPJ", "PJ", "SJ", "XQ", "TQ", "AND", "FROM", 
			"IN", "SELECT", "VALUE", "AS", "LIKE", "QUERY", "USE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public QBTELexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "QBTE.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2)\u0177\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\3\2\3\2\3\2\3\2\3\3\3"+
		"\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n"+
		"\3\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\7\24\u00cb\n\24"+
		"\f\24\16\24\u00ce\13\24\3\25\6\25\u00d1\n\25\r\25\16\25\u00d2\3\25\3\25"+
		"\3\26\3\26\3\26\7\26\u00da\n\26\f\26\16\26\u00dd\13\26\3\26\3\26\3\26"+
		"\3\26\7\26\u00e3\n\26\f\26\16\26\u00e6\13\26\3\26\5\26\u00e9\n\26\3\27"+
		"\3\27\3\27\5\27\u00ee\n\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31"+
		"\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\35\3\35"+
		"\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3"+
		"\"\3\"\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3%\3&\3&\3&\3\'\3"+
		"\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3+\3+\3"+
		",\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3"+
		"\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38\39\39\3:\3:\3;\3;\3<\3<\3=\3="+
		"\3>\3>\3?\3?\3@\3@\3A\3A\3B\3B\3C\3C\3D\3D\3E\3E\2\2F\3\3\5\4\7\5\t\6"+
		"\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24"+
		"\'\25)\26+\27-\2/\30\61\31\63\32\65\33\67\349\35;\36=\37? A!C\"E#G$I%"+
		"K&M\'O(Q)S\2U\2W\2Y\2[\2]\2_\2a\2c\2e\2g\2i\2k\2m\2o\2q\2s\2u\2w\2y\2"+
		"{\2}\2\177\2\u0081\2\u0083\2\u0085\2\u0087\2\u0089\2\3\2\"\6\2\62;C\\"+
		"aac|\5\2\13\f\17\17\"\"\4\2$$^^\4\2))^^\13\2$$))\61\61^^ddhhppttvv\5\2"+
		"\62;CHch\4\2CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4\2HHhh\4\2IIii\4\2J"+
		"Jjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOoo\4\2PPpp\4\2QQqq\4\2RRrr\4"+
		"\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2WWww\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{"+
		"{\4\2\\\\||\2\u0161\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2"+
		"\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3"+
		"\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2"+
		"\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2"+
		"\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2"+
		"\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2"+
		"\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\3\u008b"+
		"\3\2\2\2\5\u008f\3\2\2\2\7\u0091\3\2\2\2\t\u0093\3\2\2\2\13\u0095\3\2"+
		"\2\2\r\u0097\3\2\2\2\17\u009a\3\2\2\2\21\u009d\3\2\2\2\23\u00a0\3\2\2"+
		"\2\25\u00a2\3\2\2\2\27\u00a4\3\2\2\2\31\u00a8\3\2\2\2\33\u00aa\3\2\2\2"+
		"\35\u00ac\3\2\2\2\37\u00b5\3\2\2\2!\u00b7\3\2\2\2#\u00bb\3\2\2\2%\u00c1"+
		"\3\2\2\2\'\u00c8\3\2\2\2)\u00d0\3\2\2\2+\u00e8\3\2\2\2-\u00ea\3\2\2\2"+
		"/\u00ef\3\2\2\2\61\u00f6\3\2\2\2\63\u00f9\3\2\2\2\65\u00fc\3\2\2\2\67"+
		"\u00ff\3\2\2\29\u0104\3\2\2\2;\u0107\3\2\2\2=\u010a\3\2\2\2?\u010d\3\2"+
		"\2\2A\u0110\3\2\2\2C\u0114\3\2\2\2E\u0119\3\2\2\2G\u011c\3\2\2\2I\u0123"+
		"\3\2\2\2K\u0129\3\2\2\2M\u012c\3\2\2\2O\u0131\3\2\2\2Q\u0137\3\2\2\2S"+
		"\u013b\3\2\2\2U\u0141\3\2\2\2W\u0143\3\2\2\2Y\u0145\3\2\2\2[\u0147\3\2"+
		"\2\2]\u0149\3\2\2\2_\u014b\3\2\2\2a\u014d\3\2\2\2c\u014f\3\2\2\2e\u0151"+
		"\3\2\2\2g\u0153\3\2\2\2i\u0155\3\2\2\2k\u0157\3\2\2\2m\u0159\3\2\2\2o"+
		"\u015b\3\2\2\2q\u015d\3\2\2\2s\u015f\3\2\2\2u\u0161\3\2\2\2w\u0163\3\2"+
		"\2\2y\u0165\3\2\2\2{\u0167\3\2\2\2}\u0169\3\2\2\2\177\u016b\3\2\2\2\u0081"+
		"\u016d\3\2\2\2\u0083\u016f\3\2\2\2\u0085\u0171\3\2\2\2\u0087\u0173\3\2"+
		"\2\2\u0089\u0175\3\2\2\2\u008b\u008c\7H\2\2\u008c\u008d\7Q\2\2\u008d\u008e"+
		"\7T\2\2\u008e\4\3\2\2\2\u008f\u0090\7.\2\2\u0090\6\3\2\2\2\u0091\u0092"+
		"\7<\2\2\u0092\b\3\2\2\2\u0093\u0094\7}\2\2\u0094\n\3\2\2\2\u0095\u0096"+
		"\7\177\2\2\u0096\f\3\2\2\2\u0097\u0098\7R\2\2\u0098\u0099\7T\2\2\u0099"+
		"\16\3\2\2\2\u009a\u009b\7R\2\2\u009b\u009c\7L\2\2\u009c\20\3\2\2\2\u009d"+
		"\u009e\7U\2\2\u009e\u009f\7L\2\2\u009f\22\3\2\2\2\u00a0\u00a1\7*\2\2\u00a1"+
		"\24\3\2\2\2\u00a2\u00a3\7+\2\2\u00a3\26\3\2\2\2\u00a4\u00a5\7C\2\2\u00a5"+
		"\u00a6\7P\2\2\u00a6\u00a7\7F\2\2\u00a7\30\3\2\2\2\u00a8\u00a9\7?\2\2\u00a9"+
		"\32\3\2\2\2\u00aa\u00ab\7\61\2\2\u00ab\34\3\2\2\2\u00ac\u00ad\7s\2\2\u00ad"+
		"\u00ae\7w\2\2\u00ae\u00af\7g\2\2\u00af\u00b0\7t\2\2\u00b0\u00b1\7{\2\2"+
		"\u00b1\u00b2\7A\2\2\u00b2\u00b3\7s\2\2\u00b3\u00b4\7?\2\2\u00b4\36\3\2"+
		"\2\2\u00b5\u00b6\7(\2\2\u00b6 \3\2\2\2\u00b7\u00b8\7h\2\2\u00b8\u00b9"+
		"\7n\2\2\u00b9\u00ba\7?\2\2\u00ba\"\3\2\2\2\u00bb\u00bc\7Y\2\2\u00bc\u00bd"+
		"\7J\2\2\u00bd\u00be\7G\2\2\u00be\u00bf\7T\2\2\u00bf\u00c0\7G\2\2\u00c0"+
		"$\3\2\2\2\u00c1\u00c2\7T\2\2\u00c2\u00c3\7G\2\2\u00c3\u00c4\7V\2\2\u00c4"+
		"\u00c5\7W\2\2\u00c5\u00c6\7T\2\2\u00c6\u00c7\7P\2\2\u00c7&\3\2\2\2\u00c8"+
		"\u00cc\t\2\2\2\u00c9\u00cb\t\2\2\2\u00ca\u00c9\3\2\2\2\u00cb\u00ce\3\2"+
		"\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cd(\3\2\2\2\u00ce\u00cc"+
		"\3\2\2\2\u00cf\u00d1\t\3\2\2\u00d0\u00cf\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2"+
		"\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d5\b\25"+
		"\2\2\u00d5*\3\2\2\2\u00d6\u00db\7$\2\2\u00d7\u00da\5-\27\2\u00d8\u00da"+
		"\n\4\2\2\u00d9\u00d7\3\2\2\2\u00d9\u00d8\3\2\2\2\u00da\u00dd\3\2\2\2\u00db"+
		"\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00de\3\2\2\2\u00dd\u00db\3\2"+
		"\2\2\u00de\u00e9\7$\2\2\u00df\u00e4\7)\2\2\u00e0\u00e3\5-\27\2\u00e1\u00e3"+
		"\n\5\2\2\u00e2\u00e0\3\2\2\2\u00e2\u00e1\3\2\2\2\u00e3\u00e6\3\2\2\2\u00e4"+
		"\u00e2\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00e7\3\2\2\2\u00e6\u00e4\3\2"+
		"\2\2\u00e7\u00e9\7)\2\2\u00e8\u00d6\3\2\2\2\u00e8\u00df\3\2\2\2\u00e9"+
		",\3\2\2\2\u00ea\u00ed\7^\2\2\u00eb\u00ee\t\6\2\2\u00ec\u00ee\5S*\2\u00ed"+
		"\u00eb\3\2\2\2\u00ed\u00ec\3\2\2\2\u00ee.\3\2\2\2\u00ef\u00f0\5y=\2\u00f0"+
		"\u00f1\5_\60\2\u00f1\u00f2\5}?\2\u00f2\u00f3\5\177@\2\u00f3\u00f4\5y="+
		"\2\u00f4\u00f5\5q9\2\u00f5\60\3\2\2\2\u00f6\u00f7\5W,\2\u00f7\u00f8\5"+
		"i\65\2\u00f8\62\3\2\2\2\u00f9\u00fa\5y=\2\u00fa\u00fb\5k\66\2\u00fb\64"+
		"\3\2\2\2\u00fc\u00fd\5u;\2\u00fd\u00fe\5y=\2\u00fe\66\3\2\2\2\u00ff\u0100"+
		"\5{>\2\u0100\u0101\5u;\2\u0101\u0102\5u;\2\u0102\u0103\5i\65\2\u01038"+
		"\3\2\2\2\u0104\u0105\5u;\2\u0105\u0106\5i\65\2\u0106:\3\2\2\2\u0107\u0108"+
		"\5{>\2\u0108\u0109\5i\65\2\u0109<\3\2\2\2\u010a\u010b\5\u0085C\2\u010b"+
		"\u010c\5w<\2\u010c>\3\2\2\2\u010d\u010e\5}?\2\u010e\u010f\5w<\2\u010f"+
		"@\3\2\2\2\u0110\u0111\5W,\2\u0111\u0112\5q9\2\u0112\u0113\5]/\2\u0113"+
		"B\3\2\2\2\u0114\u0115\5a\61\2\u0115\u0116\5y=\2\u0116\u0117\5s:\2\u0117"+
		"\u0118\5o8\2\u0118D\3\2\2\2\u0119\u011a\5g\64\2\u011a\u011b\5q9\2\u011b"+
		"F\3\2\2\2\u011c\u011d\5{>\2\u011d\u011e\5_\60\2\u011e\u011f\5m\67\2\u011f"+
		"\u0120\5_\60\2\u0120\u0121\5[.\2\u0121\u0122\5}?\2\u0122H\3\2\2\2\u0123"+
		"\u0124\5\u0081A\2\u0124\u0125\5W,\2\u0125\u0126\5m\67\2\u0126\u0127\5"+
		"\177@\2\u0127\u0128\5_\60\2\u0128J\3\2\2\2\u0129\u012a\5W,\2\u012a\u012b"+
		"\5{>\2\u012bL\3\2\2\2\u012c\u012d\5m\67\2\u012d\u012e\5g\64\2\u012e\u012f"+
		"\5k\66\2\u012f\u0130\5_\60\2\u0130N\3\2\2\2\u0131\u0132\5w<\2\u0132\u0133"+
		"\5\177@\2\u0133\u0134\5_\60\2\u0134\u0135\5y=\2\u0135\u0136\5\u0087D\2"+
		"\u0136P\3\2\2\2\u0137\u0138\5\177@\2\u0138\u0139\5{>\2\u0139\u013a\5_"+
		"\60\2\u013aR\3\2\2\2\u013b\u013c\7w\2\2\u013c\u013d\5U+\2\u013d\u013e"+
		"\5U+\2\u013e\u013f\5U+\2\u013f\u0140\5U+\2\u0140T\3\2\2\2\u0141\u0142"+
		"\t\7\2\2\u0142V\3\2\2\2\u0143\u0144\t\b\2\2\u0144X\3\2\2\2\u0145\u0146"+
		"\t\t\2\2\u0146Z\3\2\2\2\u0147\u0148\t\n\2\2\u0148\\\3\2\2\2\u0149\u014a"+
		"\t\13\2\2\u014a^\3\2\2\2\u014b\u014c\t\f\2\2\u014c`\3\2\2\2\u014d\u014e"+
		"\t\r\2\2\u014eb\3\2\2\2\u014f\u0150\t\16\2\2\u0150d\3\2\2\2\u0151\u0152"+
		"\t\17\2\2\u0152f\3\2\2\2\u0153\u0154\t\20\2\2\u0154h\3\2\2\2\u0155\u0156"+
		"\t\21\2\2\u0156j\3\2\2\2\u0157\u0158\t\22\2\2\u0158l\3\2\2\2\u0159\u015a"+
		"\t\23\2\2\u015an\3\2\2\2\u015b\u015c\t\24\2\2\u015cp\3\2\2\2\u015d\u015e"+
		"\t\25\2\2\u015er\3\2\2\2\u015f\u0160\t\26\2\2\u0160t\3\2\2\2\u0161\u0162"+
		"\t\27\2\2\u0162v\3\2\2\2\u0163\u0164\t\30\2\2\u0164x\3\2\2\2\u0165\u0166"+
		"\t\31\2\2\u0166z\3\2\2\2\u0167\u0168\t\32\2\2\u0168|\3\2\2\2\u0169\u016a"+
		"\t\33\2\2\u016a~\3\2\2\2\u016b\u016c\t\34\2\2\u016c\u0080\3\2\2\2\u016d"+
		"\u016e\t\35\2\2\u016e\u0082\3\2\2\2\u016f\u0170\t\36\2\2\u0170\u0084\3"+
		"\2\2\2\u0171\u0172\t\37\2\2\u0172\u0086\3\2\2\2\u0173\u0174\t \2\2\u0174"+
		"\u0088\3\2\2\2\u0175\u0176\t!\2\2\u0176\u008a\3\2\2\2\13\2\u00cc\u00d2"+
		"\u00d9\u00db\u00e2\u00e4\u00e8\u00ed\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}