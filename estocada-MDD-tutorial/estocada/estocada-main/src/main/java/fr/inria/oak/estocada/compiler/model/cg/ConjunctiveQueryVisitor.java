// Generated from ConjunctiveQuery.g4 by ANTLR 4.7.2

    package fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ConjunctiveQueryParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ConjunctiveQueryVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#conjunctiveQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConjunctiveQuery(ConjunctiveQueryParser.ConjunctiveQueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#queryHeader}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryHeader(ConjunctiveQueryParser.QueryHeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#queryBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryBody(ConjunctiveQueryParser.QueryBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#labelI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLabelI(ConjunctiveQueryParser.LabelIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#kindI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKindI(ConjunctiveQueryParser.KindIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#propertyI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPropertyI(ConjunctiveQueryParser.PropertyIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#compI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompI(ConjunctiveQueryParser.CompIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#valueI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValueI(ConjunctiveQueryParser.ValueIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#connectionI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConnectionI(ConjunctiveQueryParser.ConnectionIContext ctx);
	/**
	 * Visit a parse tree produced by {@link ConjunctiveQueryParser#nestValueI}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNestValueI(ConjunctiveQueryParser.NestValueIContext ctx);
}