package fr.inria.oak.estocada.compiler.model.cg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CypherVisitorHelper {
    protected List<PropertyI> propertyIContext;
    protected Map<String, List<Object>> varContext;

    public CypherVisitorHelper() {
        this.propertyIContext = new ArrayList<>();
        this.varContext = new HashMap<>();
    }

    // lookup aux functions
    protected String lookupPropertyI(PropertyI target) {
        for (PropertyI propertyI : propertyIContext) {
            if (propertyI.equals(target))
                return propertyI.getvId();
        }
        return null;
    }

    protected PropertyI getPropertyI(PropertyI target) {
        for (PropertyI propertyI : propertyIContext) {
            if (propertyI.equals(target))
                return propertyI;
        }
        return null;
    }

    protected boolean lookupLabelI(LabelI target, List<LabelI> labelIList) {
        for (LabelI atom : labelIList) {
            if (atom.equals(target))
                return true;
        }
        return false;
    }

    protected boolean lookupLabelI(String id, List<LabelI> labelIList) {
        for (LabelI atom : labelIList) {
            if (id.equals(atom.getId()))
                return true;
        }
        return false;
    }

    // redundancy removing aux functions
    protected List<Object> combineReadingReturn(List<Object> readingEncoding, List<Object> returnEncoding) {
        List<Object> encoding = new ArrayList<Object>();
        Map<String, String> vIdReplaceMap = new HashMap<String, String>();
        List<PropertyI> returnRemoveAtoms = new ArrayList<>();
        // find equivalent vId
        for (Object returnAtom : returnEncoding) {
            if (returnAtom instanceof PropertyI) {
                for (Object readingAtom : readingEncoding) {
                    if (readingAtom instanceof PropertyI && ((PropertyI) returnAtom).equals(readingAtom)) {
                        vIdReplaceMap.put(((PropertyI) returnAtom).getvId(), ((PropertyI) readingAtom).getvId());
                        returnRemoveAtoms.add((PropertyI) returnAtom);
                        break;
                    }
                }
            }
        }
        for (PropertyI returnAtom : returnRemoveAtoms) {
            for (Iterator<Object> it = readingEncoding.iterator(); it.hasNext();) {
                Object atom = it.next();
                if (atom instanceof PropertyI && returnAtom.equals((PropertyI) atom)) {
                    it.remove();
                    break;
                }
            }
        }
        // replace vId in return atoms
        for (Object returnAtom : returnEncoding) {
            if (returnAtom instanceof ValueI) {
                for (Map.Entry<String, String> entry : vIdReplaceMap.entrySet()) {
                    if (((ValueI) returnAtom).getvId().equals(entry.getKey()))
                        ((ValueI) returnAtom).setvId(entry.getValue());
                }
            }
        }
        encoding.addAll(readingEncoding);
        encoding.addAll(returnEncoding);
        return encoding;
    }

    protected List<Object> removeRedundantKindIAndLabelI(List<Object> encoding) {
        List<Object> newEncoding = new ArrayList<Object>();
        List<LabelI> labelIList = new ArrayList<LabelI>();
        List<String> idListOfKind = new ArrayList<String>();
        for (Object atom : encoding) {
            if (atom instanceof LabelI) {
                if (!lookupLabelI((LabelI) atom, labelIList)) {
                    newEncoding.add(atom);
                    labelIList.add((LabelI) atom);
                }
            } else if (!(atom instanceof KindI))
                newEncoding.add(atom);
        }
        for (Object atom : encoding) {
            if (atom instanceof KindI) {
                String id = ((KindI) atom).getId();
                if (!lookupLabelI(id, labelIList) && !idListOfKind.contains(id)) {
                    newEncoding.add(atom);
                    idListOfKind.add(id);
                }
            }
        }
        return newEncoding;
    }

    protected List<Object> removeRedundantPropertyI(List<Object> encoding) { // used for query
        List<PropertyI> propertyIList = new ArrayList<PropertyI>();
        List<Object> newEncoding = new ArrayList<Object>();
        for (Object atom : encoding) {
            if (atom instanceof PropertyI) {
                boolean find = false;
                for (PropertyI propertyI : propertyIList) {
                    if (propertyI.equals(atom)) {
                        find = true;
                        break;
                    }
                }
                if (!find) {
                    propertyIList.add((PropertyI) atom);
                    newEncoding.add(atom);
                }
            } else
                newEncoding.add(atom);
        }
        return newEncoding;
    }

    protected List<Object> removeRedundantPropertyI(List<Object> readingEncoding, List<Object> updatingEncoding) { // used for view
        List<PropertyI> propertyIList = new ArrayList<PropertyI>();
        List<Object> newReadingEncoding = new ArrayList<Object>(), newUpdatingEncoding = new ArrayList<Object>();
        for (Object atom : readingEncoding) {
            if (atom instanceof PropertyI) {
                boolean find = false;
                for (PropertyI propertyI : propertyIList) {
                    if (propertyI.equals(atom)) {
                        find = true;
                        break;
                    }
                }
                if (!find) {
                    propertyIList.add((PropertyI) atom);
                    newReadingEncoding.add(atom);
                }
            } else
                newReadingEncoding.add(atom);
        }
        for (Object atom : updatingEncoding) {
            if (atom instanceof PropertyI) {
                boolean find = false;
                for (PropertyI propertyI : propertyIList) {
                    if (propertyI.equals(atom)) {
                        find = true;
                        break;
                    }
                }
                if (!find) {
                    propertyIList.add((PropertyI) atom);
                    newUpdatingEncoding.add(atom);
                }
            } else
                newUpdatingEncoding.add(atom);
        }
        List<Object> encoding = new ArrayList<Object>();
        encoding.add(newReadingEncoding);
        encoding.add(newUpdatingEncoding);
        return encoding;
    }

    protected List<Object> removeQueryHead(List<Object> withEncoding) {
        List<Object> newWithEncoding = new ArrayList<Object>();
        for (Object atom : withEncoding) {
            if (atom instanceof QueryHead)
                continue;
            newWithEncoding.add(atom);
        }
        return newWithEncoding;
    }

    protected List<Object> removeRedundantValueI(List<Object> encoding) {
        List<Object> newEncoding = new ArrayList<Object>();
        List<String> valueVarList = new ArrayList<String>();
        List<ValueI> valueIList = new ArrayList<ValueI>();
        // get legible valueVar
        for (Object atom : encoding) {
            if (atom instanceof ValueI)
                continue;
            if (atom instanceof QueryHead)
                valueVarList.addAll(((QueryHead) atom).getVarList());
            newEncoding.add(atom);
        }
        // filter ValueI atoms
        for (Object atom : encoding) {
            if (atom instanceof ValueI) {
                String v = ((ValueI) atom).getV();
                if (v.contains("\"")) { // check if it's a constant
                    valueIList.add((ValueI) atom);
                } else if (valueVarList.contains(v) && !valueIList.contains(atom)) {
                    valueIList.add((ValueI) atom);
                }
            }
        }
        newEncoding.addAll(valueIList);
        return newEncoding;
    }

    protected List<Object> removeRedundantValueI(List<Object> readingEncoding, List<Object> updatingEncoding) {
        List<Object> newReadingEncoding = new ArrayList<Object>(), newUpdatingEncoding = new ArrayList<Object>();
        List<ValueI> valueIList = new ArrayList<ValueI>(), readingValueIList = new ArrayList<ValueI>(),
                updatingValueIList = new ArrayList<ValueI>();
        // get atoms except ValueI
        for (Object atom : readingEncoding) {
            if (atom instanceof ValueI)
                continue;
            newReadingEncoding.add(atom);
        }
        for (Object atom : updatingEncoding) {
            if (atom instanceof ValueI)
                continue;
            newUpdatingEncoding.add(atom);
        }
        // filter ValueI atoms
        for (Object atom : readingEncoding) {
            if (atom instanceof ValueI) {
                String v = ((ValueI) atom).getV();
                if (v.contains("\"") && !valueIList.contains(atom)) { // check if it's a constant and not redundant
                    valueIList.add((ValueI) atom);
                    readingValueIList.add((ValueI) atom);
                }
            }
        }
        for (Object atom : updatingEncoding) {
            if (atom instanceof ValueI) {
                String v = ((ValueI) atom).getV();
                if (v.contains("\"") && !valueIList.contains(atom)) { // check if it's a constant and not redundant
                    valueIList.add((ValueI) atom);
                    updatingValueIList.add((ValueI) atom);
                }
            }
        }
        newReadingEncoding.addAll(readingValueIList);
        newUpdatingEncoding.addAll(updatingValueIList);
        List<Object> encoding = new ArrayList<Object>(Arrays.asList(newReadingEncoding, newUpdatingEncoding));
        return encoding;
    }
}
