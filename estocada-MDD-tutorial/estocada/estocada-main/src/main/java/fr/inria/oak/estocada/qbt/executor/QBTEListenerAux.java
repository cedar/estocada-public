package fr.inria.oak.estocada.qbt.executor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLREval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;

public class QBTEListenerAux extends QBTEBaseListener {
    private Map<String, String> blocksNativeQuery;
    private Map<String, String> varBlocksMapping;
    private Map<String, String> varBindingMapping;
    private Catalog catalog;
    private String solrTextSearch;

    private Stack<PhyBindJoin> bindJoins;

    public QBTEListenerAux() {
        try {
            Parameters.init();
        } catch (TatooineException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catalog = Catalog.getInstance();
        blocksNativeQuery = new HashMap<>();
        varBlocksMapping = new HashMap<>();
        varBindingMapping = new HashMap<>();
        bindJoins = new Stack<>();
    }

    @Override
    public void enterQueryBlock(QBTEParser.QueryBlockContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitQueryBlock(QBTEParser.QueryBlockContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterForPattern(QBTEParser.ForPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitForPattern(QBTEParser.ForPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterBlockPattern(QBTEParser.BlockPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitBlockPattern(QBTEParser.BlockPatternContext ctx) {
        String qtr = ctx.pattern().getText();
        qtr = qtr.replaceAll("\"", "");
        qtr = qtr.replaceAll("\\\\", "\"");
        blocksNativeQuery.put(ctx.annotation().getText(), qtr);
        if (ctx.annotation().getText().equals("PJ") || ctx.annotation().getText().equals("PR")) {
            processSelectClause(qtr, ctx.annotation().getText());
        }

        if (ctx.annotation().getText().equals("SJ")) {
            processSJ(qtr, ctx.annotation().getText());
        }

    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterPattern(QBTEParser.PatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitPattern(QBTEParser.PatternContext ctx) {

    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterAnnotation(QBTEParser.AnnotationContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitAnnotation(QBTEParser.AnnotationContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjPattern(QBTEParser.SjPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjPattern(QBTEParser.SjPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterCondition(QBTEParser.ConditionContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitCondition(QBTEParser.ConditionContext ctx) {

    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterConditionAtom(QBTEParser.ConditionAtomContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitConditionAtom(QBTEParser.ConditionAtomContext ctx) {

        final String var1 = ctx.variable().get(0).getText();
        final String var2 = ctx.variable().get(1).getText();
        final NIterator phyEvalLeft = getOperatorLeft(var1);
        final BindAccess phyEvalRight = getOperatorRight(var2);
        try {
            final PhyBindJoin leftBindJoin = new PhyBindJoin(phyEvalLeft, phyEvalRight, new int[] { 0 });
            bindJoins.push(leftBindJoin);
        } catch (TatooineExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjQuery(QBTEParser.SjQueryContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjQuery(QBTEParser.SjQueryContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjCollectionName(QBTEParser.SjCollectionNameContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjCollectionName(QBTEParser.SjCollectionNameContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjTextSearch(QBTEParser.SjTextSearchContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjTextSearch(QBTEParser.SjTextSearchContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjProjectFields(QBTEParser.SjProjectFieldsContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjProjectFields(QBTEParser.SjProjectFieldsContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjFieldName(QBTEParser.SjFieldNameContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjFieldName(QBTEParser.SjFieldNameContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterSjConstant(QBTEParser.SjConstantContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitSjConstant(QBTEParser.SjConstantContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterVariable(QBTEParser.VariableContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitVariable(QBTEParser.VariableContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterWherePattern(QBTEParser.WherePatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitWherePattern(QBTEParser.WherePatternContext ctx) {

    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void enterReturnPattern(QBTEParser.ReturnPatternContext ctx) {
    }

    /**
     * {@inheritDoc}
     *
     * <p>
     * The default implementation does nothing.
     * </p>
     */
    @Override
    public void exitReturnPattern(QBTEParser.ReturnPatternContext ctx) {
    }

    private void processSelectClause(String query, String annotation) {
        query = query.trim();
        String selectClause = StringUtils.substringBefore(query, "FROM");
        selectClause = selectClause.trim();
        selectClause = StringUtils.substringAfter(selectClause, "SELECT");
        //TODO:Fix
        String binding = StringUtils.substringBefore(selectClause, "AS");
        binding = binding.trim();
        String var = StringUtils.substringAfter(selectClause, "AS");
        var = var.trim();
        varBindingMapping.put(var, binding);
        varBlocksMapping.put(var, annotation);

    }

    private void processSJ(String query, String annotation) {
        query = query.trim();
        solrTextSearch = StringUtils.substringBetween(query, "q=", "&").replace("\"", "");
        String varBinding = StringUtils.substringAfter(query, "fl=");
        varBinding = varBinding.trim();
        String var = StringUtils.substringBefore(varBinding, ":");
        String binding = StringUtils.substringAfter(varBinding, ":");
        varBlocksMapping.put(var, annotation);
        varBindingMapping.put(var, binding);

    }

    private NIterator getOperatorLeft(final String varName) {
        final String block = varBlocksMapping.get(varName);
        if (!bindJoins.isEmpty()) {
            return bindJoins.pop();
        }
        switch (block) {
            case "SJ":
                final String field = varBindingMapping.get(varName);
                final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
                final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");
                try {
                    return new PhySOLREval(solrTextSearch, field, gsrText, articlesNRSMD.getNRSMD());

                } catch (TatooineExecutionException | SolrServerException | IOException | TatooineException e) {
                    e.printStackTrace();
                }

        }
        return null;

    }

    private BindAccess getOperatorRight(final String varName) {
        final String block = varBlocksMapping.get(varName);
        switch (block) {
            case "PR":
                final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
                final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");
                final String queryRE = blocksNativeQuery.get("PR");
                final StringBuilder strRE = new StringBuilder();
                strRE.append(queryRE);
                strRE.append(" AND ");
                strRE.append(varBindingMapping.get(varName));
                strRE.append("=?");

                try {
                    return new PhySQLEval(strRE.toString(), gsrVRERE, VREreSchema.getNRSMD());

                } catch (TatooineExecutionException e) {
                    e.printStackTrace();
                }
                break;
            case "PJ":
                final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
                final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");
                final String queryPJ = blocksNativeQuery.get("PJ");
                final StringBuilder strPJ = new StringBuilder();
                strPJ.append(queryPJ);
                strPJ.append(" AND ");
                strPJ.append("(" + varBindingMapping.get(varName) + ")::INT");
                strPJ.append("=?");
                try {
                    return new PhyPostgresJSONEval(strPJ.toString(), gsrVREPJ, VREPJSchema.getNRSMD());

                } catch (TatooineExecutionException e) {
                    e.printStackTrace();
                }

        }
        return null;

    }

    public PhyBindJoin getPlanRoot() {
        return bindJoins.pop();
    }
}
