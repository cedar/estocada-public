package fr.inria.oak.estocada.compiler.model.tm;

/**
 * TM predicates
 * 
 * @author ranaalotaibi
 *
 */
public enum Predicate {
    NAME("name_tm"),
    SIZE("size_tm"),
    ZERO("zero_tm"),
    IDENTITY("identity_tm"),
    ADD("add_tm"),
    MULTI("multi_tm"),
    MULTIS("multi_s_tm"),
    MULTIE("multi_e_tm"),
    DIV("div_tm"),
    DIVS("div_s_tm"),
    TRANS("tr_tm"),
    INVERSE("inv_tm"),
    DET("det_tm"),
    ADJ("adj_tm"),
    COFA("cof_tm"),
    TRACE("trace_tm"),
    DIAG("diag_tm"),
    POW("pow_tm"),
    AVG("avg_tm"),
    MEAN("mean_tm"),
    VAR("var_tm"),
    SD("sd_tm"),
    COLSUM("colSums_tm"),
    COLMEAN("colMeans_tm"),
    COLVARS("colVars_tm"),
    COLSDS("colSds_tm"),
    COLMAX("colMaxs_tm"),
    COLMINS("colMins_tm"),
    ROWSUM("rowSums_tm"),
    ROWMEAN("rowMeans_tm"),
    ROWVARS("rowVars_tm"),
    ROWSDS("rowSds_tm"),
    ROWMAX("rowMaxs_tm"),
    ROWMIN("rowMins_tm"),
    CUMSUM("cumsum_tm"),
    CUMPROD("cumprod_tm"),
    CUMMIN("cummin_tm"),
    CUMMAX("cummax_tm"),
    EQUALS("eq_tm");

    private final String str;

    private Predicate(final String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
