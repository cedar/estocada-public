package fr.inria.oak.estocada.compiler.model.pj.full;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * PJ Full ConditionalStructuralListener which extends {@link StructuralBaseListener}.
 * 
 * @author Rana Alotaibi
 */
@Singleton
final class ConditionalStructuralListener extends StructuralBaseListener {
    private static final Logger log = Logger.getLogger(ConditionalStructuralListener.class);

    @Inject
    public ConditionalStructuralListener(final PathExpressionListener listener,
            @Named("PJQLVariableFactory") VariableFactory aqlVariableFactory, VariableMapper variableMapper) {
        super(listener, aqlVariableFactory, variableMapper);
    }

    @Override
    protected ParserRuleContext createParseTree(final PJQLParser parser) {
        // TODO Auto-generated method stub
        return null;
    }
}
