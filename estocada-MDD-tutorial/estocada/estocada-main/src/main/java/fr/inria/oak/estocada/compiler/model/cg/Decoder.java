package fr.inria.oak.estocada.compiler.model.cg;

import static fr.inria.oak.estocada.compiler.model.cg.Utils.parseCQ;
import static fr.inria.oak.estocada.compiler.model.cg.Utils.readFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.tuple.Pair;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

class Node {
    private String id;
    private List<String> labels;
    private Map<String, String> properties;

    public Node(String id) {
        this.id = id;
        this.labels = new ArrayList<String>();
        this.properties = new HashMap<String, String>();
    }

    public Node(String id, List<String> labels) {
        this.id = id;
        this.labels = labels;
        this.properties = new HashMap<String, String>();
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        return id.equals(((Node) o).id);
    }

    @Override
    public String toString() {
        String str = "(" + id;
        if (!labels.isEmpty()) {
            for (String label : labels)
                str += ":" + label;
        }
        if (!properties.isEmpty()) {
            str += " {";
            Set<Map.Entry<String, String>> entrySet = properties.entrySet();
            for (Map.Entry<String, String> entry : entrySet)
                str += entry.getKey() + ": " + entry.getValue() + ", ";
            str = str.substring(0, str.length() - 2) + "}";
        }
        str += ")";
        return str;
    }

    public void addLabel(String label) {
        labels.add(label);
    }

    public void addProperty(String pName, String pValue) {
        properties.put(pName, pValue);
    }
}

class Relationship {
    private String id;
    private String label;
    private Map<String, String> properties;

    public Relationship(String id) {
        this.id = id;
        this.label = null;
        this.properties = new HashMap<String, String>();
    }

    public Relationship(String id, String label) {
        this.id = id;
        this.label = label;
        this.properties = new HashMap<String, String>();
    }

    public String getId() {
        return id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean equals(Object o) {
        return id.equals(((Relationship) o).id);
    }

    @Override
    public String toString() {
        String str = "[" + id;
        if (label != null)
            str += ":" + label;
        if (!properties.isEmpty()) {
            str += " {";
            Set<Map.Entry<String, String>> entrySet = properties.entrySet();
            for (Map.Entry<String, String> entry : entrySet)
                str += entry.getKey() + ": " + entry.getValue() + ", ";
            str = str.substring(0, str.length() - 2) + "}";
        }
        str += "]";
        return str;
    }

    public void addProperty(String pName, String pValue) {
        properties.put(pName, pValue);
    }
}

class RelNodePair {
    private Relationship rel;
    private Node node;
    private boolean dir; // true: ()-[r]->(n)    false: ()<-[r]-(n)

    public RelNodePair(Relationship rel, Node node, boolean dir) {
        this.rel = rel;
        this.node = node;
        this.dir = dir;
    }

    public Relationship getRel() {
        return rel;
    }

    public Node getNode() {
        return node;
    }

    public boolean getDir() {
        return dir;
    }
}

class Path {
    private Node startNode;
    private Node endNode;
    private List<RelNodePair> relNodePairs;

    public Path(Node node) {
        this.startNode = node;
        this.endNode = node;
        this.relNodePairs = new ArrayList<RelNodePair>();
    }

    public Path(Node sNode, Node tNode, Relationship rel) {
        this.startNode = sNode;
        this.endNode = tNode;
        this.relNodePairs = new ArrayList<RelNodePair>(Arrays.asList(new RelNodePair(rel, tNode, true)));
    }

    public Node getStartNode() {
        return startNode;
    }

    public Node getEndNode() {
        return endNode;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }

    public List<RelNodePair> getRelNodePairs() {
        return relNodePairs;
    }

    @Override
    public String toString() {
        String str = startNode.toString();
        for (RelNodePair relNodePair : relNodePairs) {
            if (relNodePair.getDir())
                str += "-" + relNodePair.getRel().toString() + "->" + relNodePair.getNode().toString();
            else
                str += "<-" + relNodePair.getRel().toString() + "-" + relNodePair.getNode().toString();
        }
        return str;
    }

    public void addRelNodePair(RelNodePair relNodePair, boolean start) {
        if (start) {
            RelNodePair newRelNodePair = new RelNodePair(relNodePair.getRel(), startNode, !relNodePair.getDir());
            relNodePairs.add(0, newRelNodePair);
            startNode = relNodePair.getNode();
        } else {
            relNodePairs.add(relNodePair);
            endNode = relNodePair.getNode();
        }
    }
}

public class Decoder {
    private QueryHead queryHead;
    private List<LabelI> labelIAtoms;
    private List<PropertyI> propertyIAtoms;
    private List<ConnectionI> connectionIAtoms;
    private List<EqI> eqIAtoms;
    private List<CompI> compIAtoms;
    private List<ValueI> valueIAtoms;

    private List<Node> nodes;
    private List<Relationship> rels;
    private List<Path> patterns;
    private Set<Pair<String, String>> returnValues;
    private List<Set<Pair<String, String>>> whereEqComponents;
    private Map<String, Set<Pair<String, String>>> whereMapExceptEq;

    private List<Set<String>> eqComponents;
    private Map<String, Set<Pair<String, String>>> vIdMap;
    private Map<String, String> scalarValueMap, varValueMap; // scalarValueMap: (vId1, "Jack")     varValueMap: (v1, vId1)

    public Decoder(String cqQuery) {
        List<Object> cqAtoms = getCqAtoms(cqQuery);
        this.queryHead = null;
        this.labelIAtoms = new ArrayList<LabelI>();
        this.propertyIAtoms = new ArrayList<PropertyI>();
        this.connectionIAtoms = new ArrayList<ConnectionI>();
        this.eqIAtoms = new ArrayList<EqI>();
        this.compIAtoms = new ArrayList<>();
        this.valueIAtoms = new ArrayList<ValueI>();
        this.nodes = new ArrayList<Node>();
        this.rels = new ArrayList<Relationship>();
        this.patterns = new ArrayList<Path>();
        this.returnValues = new HashSet<Pair<String, String>>();
        this.whereEqComponents = new ArrayList<Set<Pair<String, String>>>();
        this.whereMapExceptEq = new HashMap<>();
        this.whereMapExceptEq.put("<>", new HashSet<Pair<String, String>>());
        this.whereMapExceptEq.put("<", new HashSet<Pair<String, String>>());
        this.whereMapExceptEq.put(">", new HashSet<Pair<String, String>>());
        this.whereMapExceptEq.put("<=", new HashSet<Pair<String, String>>());
        this.whereMapExceptEq.put(">=", new HashSet<Pair<String, String>>());
        this.eqComponents = new ArrayList<Set<String>>();
        this.vIdMap = new HashMap<String, Set<Pair<String, String>>>();
        this.scalarValueMap = new HashMap<String, String>();
        this.varValueMap = new HashMap<String, String>();
        classify(cqAtoms);
    }

    private void classify(List<Object> encoding) {
        for (Object atom : encoding) {
            if (atom instanceof QueryHead) {
                queryHead = (QueryHead) atom;
            } else if (atom instanceof LabelI) {
                labelIAtoms.add((LabelI) atom);
            } else if (atom instanceof PropertyI) {
                propertyIAtoms.add((PropertyI) atom);
            } else if (atom instanceof ConnectionI) {
                connectionIAtoms.add((ConnectionI) atom);
            } else if (atom instanceof KindI) {
                labelIAtoms.add(new LabelI(((KindI) atom).getId(), null));
            } else if (atom instanceof CompI) {
                if (atom instanceof EqI)
                    eqIAtoms.add((EqI) atom);
                else
                    compIAtoms.add((CompI) atom);
            } else if (atom instanceof ValueI) {
                valueIAtoms.add((ValueI) atom);
            }
        }
    }

    public String decode() {
        constructPatterns();
        constructLabels();
        constructEqComponents();
        constructProperties();
        constructWhereExceptEq();
        constructReturnValues();
        return genCypherQuery();
    }

    private Path reverseDir(Path path) {
        Path resPath = new Path(path.getStartNode());
        for (RelNodePair relNodePair : path.getRelNodePairs()) {
            resPath.addRelNodePair(relNodePair, true);
        }
        return resPath;
    }

    private Path joinPath(Path targetPath, Path joinPath, boolean targetStart) {
        /* targetStart = true -> (n3,n4,n5) join (n1,n2,n3)
                        false -> (n3,n4,n5) join (n5,n6,n7) */
        if (targetStart) {
            for (int i = joinPath.getRelNodePairs().size() - 1; i >= 0; i--) {
                RelNodePair relNodePair = joinPath.getRelNodePairs().get(i);
                targetPath.getRelNodePairs().add(0, relNodePair);
            }
            targetPath.setStartNode(joinPath.getStartNode());
        } else {
            for (RelNodePair relNodePair : joinPath.getRelNodePairs())
                targetPath.getRelNodePairs().add(relNodePair);
            targetPath.setEndNode(joinPath.getEndNode());
        }
        return targetPath;
    }

    private void addToPatterns(Node sNode, Node tNode, Relationship rel) {
        boolean checkConnected = false, dir = false, start = false;
        Path targetPath = null;
        // check if can be connected to an existing path
        for (Path path : patterns) {
            if (path.getStartNode().equals(sNode)) {
                checkConnected = true;
                dir = true;
                start = true;
                targetPath = path;
                break;
            } else if (path.getStartNode().equals(tNode)) {
                checkConnected = true;
                dir = false;
                start = true;
                targetPath = path;
                break;
            } else if (path.getEndNode().equals(sNode)) {
                checkConnected = true;
                dir = true;
                start = false;
                targetPath = path;
                break;
            } else if (path.getEndNode().equals(tNode)) {
                checkConnected = true;
                dir = false;
                start = false;
                targetPath = path;
                break;
            }
        }
        if (checkConnected) {
            if (dir)
                targetPath.addRelNodePair(new RelNodePair(rel, tNode, dir), start);
            else
                targetPath.addRelNodePair(new RelNodePair(rel, sNode, dir), start);
        } else {
            Path newPath = new Path(sNode, tNode, rel);
            patterns.add(newPath);
        }
    }

    private void constructPatterns() {
        // construct paths from ConnectionI atoms
        for (ConnectionI connectionIAtom : connectionIAtoms) {
            String sId = connectionIAtom.getsId(), tId = connectionIAtom.gettId(), eId = connectionIAtom.geteId();
            // source node and target node
            Node sNode = new Node(sId), tNode = new Node(tId);
            int index1 = nodes.indexOf(sNode), index2 = nodes.indexOf(tNode);
            if (index1 == -1)
                nodes.add(sNode);
            else
                sNode = nodes.get(index1);
            if (index2 == -1)
                nodes.add(tNode);
            else
                tNode = nodes.get(index2);
            // relationship
            Relationship rel = new Relationship(eId);
            rels.add(rel);
            // add to pattern
            addToPatterns(sNode, tNode, rel);
        }

        // combine paths if needed
        boolean changed;
        do {
            changed = false;
            Map<Path, Boolean> markMap = new HashMap<Path, Boolean>(); // mark if it has been joined with other path in this iteration
            List<Path> newPatterns = new ArrayList<Path>();
            for (Path path : patterns)
                markMap.put(path, false);
            for (Iterator<Path> it = patterns.iterator(); it.hasNext();) {
                Path targetPath = it.next();
                if (markMap.get(targetPath) == true)
                    continue;
                // try to join other paths as many as possible
                for (int i = patterns.indexOf(targetPath) + 1; i < patterns.size(); i++) {
                    Path testJoinPath = patterns.get(i);
                    if (targetPath.getStartNode().equals(testJoinPath.getStartNode())) {
                        markMap.put(testJoinPath, true);
                        testJoinPath = reverseDir(testJoinPath);
                        targetPath = joinPath(targetPath, testJoinPath, true);
                        changed = true;
                    } else if (targetPath.getStartNode().equals(testJoinPath.getEndNode())) {
                        markMap.put(testJoinPath, true);
                        targetPath = joinPath(targetPath, testJoinPath, true);
                        changed = true;
                    } else if (targetPath.getEndNode().equals(testJoinPath.getStartNode())) {
                        markMap.put(testJoinPath, true);
                        targetPath = joinPath(targetPath, testJoinPath, false);
                        changed = true;
                    } else if (targetPath.getEndNode().equals(testJoinPath.getEndNode())) {
                        markMap.put(testJoinPath, true);
                        testJoinPath = reverseDir(testJoinPath);
                        targetPath = joinPath(targetPath, testJoinPath, false);
                        changed = true;
                    }
                }
                newPatterns.add(targetPath);
            }
            patterns = newPatterns;
        } while (changed);
    }

    private void constructLabels() {
        for (LabelI labelIAtom : labelIAtoms) { // not know it's a node or a relationship
            String id = labelIAtom.getId(), label = labelIAtom.getLabel();
            // firstly check if it's a relationship, because if it is, then must have existed
            Relationship rel = new Relationship(id);
            int index = rels.indexOf(rel);
            if (index != -1) {
                rel = rels.get(index);
                if (label != null)
                    rel.setLabel(label);
                continue;
            }
            // now we can make sure it's a node
            Node node;
            if (label != null)
                node = new Node(id, new ArrayList<String>(Arrays.asList(label)));
            else
                node = new Node(id);
            index = nodes.indexOf(node);
            if (index == -1) {
                Path path = new Path(node);
                patterns.add(path);
                nodes.add(node);
            } else {
                if (label != null) {
                    node = nodes.get(index);
                    node.addLabel(label);
                }
            }
        }
    }

    private int getEqCompId(String vId) {
        for (int i = 0; i < eqComponents.size(); i++) {
            if (eqComponents.get(i).contains(vId))
                return i;
        }
        return -1;
    }

    private void combineComp(int eqCompId1, int eqCompId2) {
        Set<String> eqComp1 = eqComponents.get(eqCompId1), eqComp2 = eqComponents.get(eqCompId2);
        for (String migrateVar : eqComp2) {
            eqComp1.add(migrateVar);
        }
        eqComponents.remove(eqComp2);
    }

    private void constructEqComponents() {
        for (EqI eqIAtom : eqIAtoms) {
            String vId1 = eqIAtom.getvId1(), vId2 = eqIAtom.getvId2();
            if (vId1.equals(vId2))
                continue;
            int eqCompId1 = getEqCompId(vId1), eqCompId2 = getEqCompId(vId2);
            if (eqCompId1 == -1 && eqCompId2 == -1) {
                Set<String> newEqComp = new HashSet<String>(Arrays.asList(vId1, vId2));
                eqComponents.add(newEqComp);
            } else if (eqCompId1 != -1 && eqCompId2 != -1) {
                if (eqCompId1 != eqCompId2)
                    combineComp(eqCompId1, eqCompId2);
            } else if (eqCompId1 == -1) {
                eqComponents.get(eqCompId2).add(vId1);
            } else {
                eqComponents.get(eqCompId1).add(vId2);
            }
        }
        for (PropertyI propertyIAtom : propertyIAtoms) {
            String vId = propertyIAtom.getvId();
            if (getEqCompId(vId) == -1) {
                Set<String> newEqComp = new HashSet<String>(Arrays.asList(vId));
                eqComponents.add(newEqComp);
            }
        }
    }

    private void constructProperties() {
        // PropertyI atoms -> construct vIdMap
        for (PropertyI propertyIAtom : propertyIAtoms) {
            String id = propertyIAtom.getId(), pName = propertyIAtom.getPropertyKeyName(), vId = propertyIAtom.getvId();
            Set<Pair<String, String>> vIdInfoPairs = vIdMap.get(vId);
            if (vIdInfoPairs == null) {
                vIdInfoPairs = new HashSet<Pair<String, String>>(Arrays.asList(Pair.of(id, pName)));
                vIdMap.put(vId, vIdInfoPairs);
            } else {
                vIdInfoPairs.add(Pair.of(id, pName));
            }
        }
        // ValueI atoms -> construct scalarValueMap
        for (ValueI valueIAtom : valueIAtoms) {
            String vId = valueIAtom.getvId(), v = valueIAtom.getV();
            if (v.contains("\""))
                scalarValueMap.put(vId, v);
            else
                varValueMap.put(v, vId);
        }
        // construct where and attach properties:
        //      first get all <id, pName> pairs from one eqComponent, if has scalar value, then attach; else, if size > 2, make a whereEqComp
        for (Set<String> eqComp : eqComponents) {
            Set<Pair<String, String>> whereEqComp = new HashSet<Pair<String, String>>();
            boolean hasValue = false;
            String v = null;
            for (String vId : eqComp) {
                whereEqComp.addAll(vIdMap.get(vId));
                if (scalarValueMap.keySet().contains(vId)) {
                    hasValue = true;
                    v = scalarValueMap.get(vId);
                }
            }
            if (hasValue) { // attach properties
                for (Pair<String, String> pair : whereEqComp) {
                    String id = pair.getKey(), pName = pair.getValue();
                    // not know it's a node or edge
                    int index = rels.indexOf(new Relationship(id));
                    if (index != -1) {
                        Relationship rel = rels.get(index);
                        rel.addProperty(pName, v);
                        continue;
                    }
                    index = nodes.indexOf(new Node(id));
                    Node node = nodes.get(index);
                    node.addProperty(pName, v);
                }
            } else if (whereEqComp.size() > 1) { // add to whereEqComponents
                whereEqComponents.add(whereEqComp);
            }
        }
    }

    private void constructWhereExceptEq() {
        for (CompI atom : compIAtoms) {
            String vId1 = atom.getvId1(), vId2 = atom.getvId2();
            String condLeft, condRight;
            String scalarV1 = scalarValueMap.get(vId1), scalarV2 = scalarValueMap.get(vId2);
            if (scalarV1 != null)
                condLeft = scalarV1;
            else {
                Pair<String, String> pair = vIdMap.get(vId1).iterator().next(); // picking anyone is ok
                String id = pair.getKey(), pName = pair.getValue();
                condLeft = id + "." + pName;
            }
            if (scalarV2 != null)
                condRight = scalarV2;
            else {
                Pair<String, String> pair = vIdMap.get(vId2).iterator().next(); // picking anyone is ok
                String id = pair.getKey(), pName = pair.getValue();
                condRight = id + "." + pName;
            }
            if (atom instanceof NeI)
                whereMapExceptEq.get("<>").add(Pair.of(condLeft, condRight));
            else if (atom instanceof LtI)
                whereMapExceptEq.get("<").add(Pair.of(condLeft, condRight));
            else if (atom instanceof GtI)
                whereMapExceptEq.get(">").add(Pair.of(condLeft, condRight));
            else if (atom instanceof LeI)
                whereMapExceptEq.get("<=").add(Pair.of(condLeft, condRight));
            else if (atom instanceof GeI)
                whereMapExceptEq.get(">=").add(Pair.of(condLeft, condRight));
        }
    }

    private void constructReturnValues() {
        for (String v : queryHead.getVarList()) {
            String vId = varValueMap.get(v);
            Set<Pair<String, String>> vIdInfoPairs = vIdMap.get(vId);
            if (vIdInfoPairs != null) { // property return values
                returnValues.addAll(vIdInfoPairs);
            } else { // node/edge return values
                returnValues.add(Pair.of(v, null));
            }
        }
    }

    private String genCypherQuery() {
        String matchClause = "", whereClause = "WHERE ", returnClause = "RETURN ";
        // match
        for (Path path : patterns)
            matchClause += "MATCH " + path.toString() + "\n";
        // where
        for (Set<Pair<String, String>> whereEqComp : whereEqComponents) {
            String eqClause = "";
            for (Pair<String, String> pair : whereEqComp)
                eqClause += pair.getKey() + "." + pair.getValue() + " = ";
            eqClause = eqClause.substring(0, eqClause.length() - 3);
            whereClause += eqClause + " and ";
        }
        Set<String> keySet = whereMapExceptEq.keySet();
        for (String compSymbol : keySet) {
            Set<Pair<String, String>> condList = whereMapExceptEq.get(compSymbol);
            for (Pair<String, String> cond : condList) {
                whereClause += cond.getKey() + " " + compSymbol + " " + cond.getValue();
                whereClause += " and ";
            }
        }
        if (whereClause.equals("WHERE "))
            whereClause = "";
        else
            whereClause = whereClause.substring(0, whereClause.length() - 5) + "\n";
        // return
        for (Pair<String, String> pair : returnValues) {
            String varName = pair.getKey(), pName = pair.getValue();
            if (pName != null) { // property return value
                returnClause += varName + "." + pName + ", ";
            } else { // node/edge return value
                returnClause += varName + ", ";
            }
        }
        returnClause = returnClause.substring(0, returnClause.length() - 2) + "\n";
        return matchClause + whereClause + returnClause;
    }

    private List<Object> getCqAtoms(String cqQuery) {
        ParseTree tree = parseCQ(cqQuery);
        ConjunctiveQueryVisitor visitor = new ConjunctiveQueryVisitorImpl();
        List<Object> cqAtoms = (List<Object>) visitor.visit(tree);
        return cqAtoms;
    }

    public static List<String> decode(String fileName, boolean inThisProject) {
        List<String> cypherQueries = new ArrayList<>();
        String fileText = readFile(fileName, inThisProject);
        String[] cqQueries = fileText.split(";");
        for (String cqQuery : cqQueries) {
            if (cqQuery.equals("\n"))
                continue; // format diff
            cqQuery += ";";
            Decoder decoder = new Decoder(cqQuery);
            String cypherQuery = decoder.decode();
            cypherQueries.add(cypherQuery);
        }
        return cypherQueries;
    }

    public static String decode(final ConjunctiveQuery query) {
        Decoder decoder = new Decoder(query.toString());
        return decoder.decode();

    }

    public static void main(String[] args) {
        List<String> cypherQueries = decode("ldbc-cypher/results_bi_2", true);
        for (String cypherQuery : cypherQueries) {
            System.out.println(cypherQuery);
        }
    }
}
