package fr.inria.oak.estocada.main.scenarios;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLREval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.oak.commons.miscellaneous.FileUtils;

/**
 * Run MIMIC Queries on Tatooine (fixed plan order)
 * 
 * @author ranaalotaibi
 *
 */
public class TatooineScenariosMIMICCMQ {
    private static final Logger LOGGER = Logger.getLogger(TatooineScenariosMIMICCMQ.class);

    public static long Q1QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMIC");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        righBindjoin.close();
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q2QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW2");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q5QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q6QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q7QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q8QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q9QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q10QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "cannulaes";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q11QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q12QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q13QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q14QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q15QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q16QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q17QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q18QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q19QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q20QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q21QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q22QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q23QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q24QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "cannulaes";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q25QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q26QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q27QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q28QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q29QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q30QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q31QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q32QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q33QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q34QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q35QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q36QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q37QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q38QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q39QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q40QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q41QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q42QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q43QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "pneumonia";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q44QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q45QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q46QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "pneumonia";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q50QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q51QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema VREPJSchema = catalog.getViewSchema("MIMICPJ");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("MIMICPJ");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "PRQuery.query");
        final ViewSchema VREreSchema = catalog.getViewSchema("MIMICPR");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("MIMICPR");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }
}
