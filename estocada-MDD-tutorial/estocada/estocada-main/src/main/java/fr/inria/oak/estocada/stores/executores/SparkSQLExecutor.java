package fr.inria.oak.estocada.stores.executores;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

/**
 * SparkSQL Query Executor
 * 
 * @author Rana Alotaibi
 *
 */
public class SparkSQLExecutor implements ExecutorInterface {

    private static final Logger LOGGER = Logger.getLogger(SparkSQLExecutor.class);
    private static String dataPath1 = "dataset/parquet1";
    private static String dataPath2 = "dataset/parquet2";
    private JavaSparkContext context = null;
    private SQLContext sqlContext = null;
    private Dataset frame1 = null;
    private Dataset frame2 = null;
    private Timer timer;

    @Override
    public void createConnection() {
        SparkConf conf = new SparkConf();
        conf.set("spark.driver.memory", "60g").set("spark.sql.crossJoin.enabled", "true").setMaster("local[39]")
                .setAppName("sparkexecutor");
        context = new JavaSparkContext(conf);
        sqlContext = new SQLContext(context);
        frame1 = sqlContext.read().format("org.apache.spark.sql.execution.datasources.parquet.ParquetFileFormat")
                .load(dataPath1);
        frame1.registerTempTable("mimic");
        frame2 = sqlContext.read().format("org.apache.spark.sql.execution.datasources.parquet.ParquetFileFormat")
                .load(dataPath2);
        frame2.registerTempTable("d_labitems");
        timer = new Timer();
        LOGGER.info("Data Loaded!");
    }

    @Override
    public long executeQuery(String nativeQuery) {
        long queryTime = 0;
        final long start = System.currentTimeMillis();
        startTimer();
        Dataset response = sqlContext.sql(nativeQuery);
        Row[] rows = (Row[]) response.collect();
        final long end = System.currentTimeMillis();
        queryTime += (end - start);
        return queryTime;

    }

    /**
     * Set timer to cancel timeout queries
     */
    private void startTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                LOGGER.info("Query Canceled Timeout!");
                context.stop();
            }
        }, 1500000);
    }

    @Override
    public void destroy() {
        LOGGER.info("Executor Cancled!");
        timer.cancel();
    }
}
