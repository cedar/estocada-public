package fr.inria.oak.estocada.qbt.executor;

import java.io.File;

import org.apache.commons.io.FileUtils;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;

public class Test {

    final static String INPUT_QUERY_FILE = "src/main/resources/testQBTE/Query.Q";

    public static void main(String[] args) throws Exception {
        String query = FileUtils.readFileToString(new File(INPUT_QUERY_FILE));
        //        query = query.replaceAll("\"", "\\\\\"");
        //        query = query.replaceAll("\\{", "\\{\"");
        //        query = query.replaceAll("\\}", "\"}");
        //        query = query.replaceAll("\\\"\"}", "\\\"\\}");
        //        query = query.replaceAll("\\{\"\\\\\"", "\\{\\\\\"");
        final QBTExecutorBuilder builder = new QBTExecutorBuilder(query);
        builder.parse();
        final PhyBindJoin phyBindJoin = builder.getPhyPlan();
        int resultCount = 0;
        phyBindJoin.open();
        while (phyBindJoin.hasNext()) {
            NTuple next = phyBindJoin.next();
            resultCount++;
        }
        phyBindJoin.close();
        System.out.println(resultCount);
    }
}
