package fr.inria.oak.estocada.compiler.model.pm;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;

@Singleton
public class PMExtractVariableToCreatedNodeVisitor implements ReturnTemplateVisitor {
    private ImmutableList.Builder<Atom> builder;
    private Variable viewID;
    private String viewName;

    @Inject
    public PMExtractVariableToCreatedNodeVisitor() {
    }

    public List<Atom> encode(final ReturnTemplate template) {
        builder = ImmutableList.builder();
        template.accept(this);
        return builder.build();
    }

    public void encode(final Variable viewID, final String viewName) {
        this.viewID = viewID;
        this.viewName = viewName;

    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        // NOP (no encoding before the children are processed)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {

    }

    @Override
    public void visit(ReturnStringTerm term) {
        // NOP (no encoding for string terms)
    }
}