package fr.inria.oak.estocada.stores.executores;

/**
 * Executor interface to execute native queries against different stores (e.g,
 * MongoDB, AsterixDB, SparkSQL, PostgresJSON)
 * 
 * @author Rana Alotaibi
 */
public interface ExecutorInterface {

    /**
     * Create a store connection
     */
    public void createConnection();

    /**
     * Execute the query
     * 
     * @param nativeQuery
     */
    public long executeQuery(final String nativeQuery);

    /**
     * Destroy task-timer
     */
    public default void destroy() {
    };

}
