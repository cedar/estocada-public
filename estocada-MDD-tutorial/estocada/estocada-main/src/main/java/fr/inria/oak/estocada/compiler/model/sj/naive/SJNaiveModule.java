package fr.inria.oak.estocada.compiler.model.sj.naive;

import fr.inria.oak.estocada.compiler.model.sj.SJModule;

public class SJNaiveModule extends SJModule {
    @Override
    protected String getPropertiesFileName() {
        return "sj.naive.properties";
    }
}
