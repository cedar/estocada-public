package fr.inria.oak.estocada.views.materialization.module.SJ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * This is for test purposes
 *
 */
public class RegisterMetadata {

    public static void RegisterDataset() throws Exception {

        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        catalog.delete();
        // Register
        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("testM");
        // Columns names
        final List<String> colNames = new ArrayList<String>();
        colNames.add("name");
        colNames.add("age");
        colNames.add("id");
        // NRSMD
        final NRSMD NRSMD = new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE }, colNames);
        final Map<String, Integer> serviceNRSMDMapping = new HashMap<>();
        serviceNRSMDMapping.put("name", 0);
        serviceNRSMDMapping.put("age", 1);
        serviceNRSMDMapping.put("id", 2);
        ViewSchema serviceSchema = new ViewSchema(NRSMD, serviceNRSMDMapping);
        catalog.add("testM", storageReferencePostgresSQL1, null, serviceSchema);
    }

    /**
     * Storage reference for data in PostgreSQL
     * 
     * @param collectionName
     * @return
     * @throws Exception
     */
    private static StorageReference getPostgresSQLStorageReference(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        return gsr;
    }
}
