package fr.inria.oak.estocada.views.materialization.module;

import com.google.common.base.Strings;

/**
 * Enumerated type representing the possible types of data sources annotation.
 * 
 * @author ranaalotaibi
 */
public enum Annotation {
    AJ,
    PJ,
    SPPJ,
    PR,
    RK,
    SJ;

    /**
     * Returns the corresponding member of the enumerated type for a given string.
     * 
     * @param stringType
     * @return
     */
    public static Annotation getTypeEnum(String stringType) {
        if (Strings.isNullOrEmpty(stringType)) {
            return null;
        }
        try {
            return Annotation.valueOf(stringType.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
