package fr.inria.oak.estocada.compiler.model.pj.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.Element;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.StringElement;
import fr.inria.oak.estocada.compiler.model.pj.ArrayElementFactory;

@Singleton
public class PJChildBlockForwardEncoderReturnTermVisitor extends PJBaseForwardEncoderReturnTermVisitor {
	private Variable parentBlockCreatedNode;

	@Inject
	public PJChildBlockForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory) {
		super(arrayElementFactory);
	}

	public List<Atom> encode(final Variable parentBlockCreatedNode, final ReturnTemplate template,
	        final String viewName) {
		builder = ImmutableList.builder();
		this.parentBlockCreatedNode = parentBlockCreatedNode;
		this.viewName = viewName;
		template.accept(this);
		return builder.build();
	}

	@Override
	protected Element getParentBlockElement(final ReturnTerm term) {
		if (term.hasParent()) {
			if (!term.getParent().getElement().isEmpty()) {
				return term.getParent().getElement();
			} else {
				if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
					return term.getParent().getParent().getElement();
				}
			}
		}
		return new StringElement("");
	}

	@Override
	protected Variable getParentBlockCreatedNode(final ReturnTerm term) {
		if (term.hasParent()) {
			if (!term.getParent().getElement().isEmpty()) {
				return term.getParent().getCreatedNode();
			} else {
				if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
					return term.getParent().getParent().getCreatedNode();
				}
			}
		}
		return parentBlockCreatedNode;
	}
}