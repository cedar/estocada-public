package fr.inria.oak.estocada.views.materialization.module;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.qbt.walker.QBTMBaseListener;
import fr.inria.oak.estocada.qbt.walker.QBTMParser;
import fr.inria.oak.estocada.views.materialization.module.blocks.PJQBTBlockConstructor;
import fr.inria.oak.estocada.views.materialization.module.blocks.PRQBTBlockConstructor;
import fr.inria.oak.estocada.views.materialization.module.blocks.QBTPattern;
import fr.inria.oak.estocada.views.materialization.module.blocks.SPPJQBTBlockConstructor;

/**
 * QBTListenerAux traverses the QBT Pattern Blocks AST auto-generated by Antlr.
 * And constructs the corresponding physical operator to be executed by each block.
 * 
 * @author Rana Alotaibi
 *
 */
public final class QBTBlockListenerAux extends QBTMBaseListener {

    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(QBTViewListener.class);
    private final List<QBTPattern> qbtBlockConstructors;
    private QBTPattern root;
    private boolean containsCondition;
    private String viewName;

    /** Constructor **/
    public QBTBlockListenerAux() {
        viewName = null;
        root = null;
        containsCondition = false;
        qbtBlockConstructors = new ArrayList<QBTPattern>();
    }

    @Override
    public void enterViewName(QBTMParser.ViewNameContext ctx) {
        LOGGER.debug("Entering ViewName: " + ctx.getText());
        viewName = ctx.getText();
    }

    @Override
    public void exitQbtQuery(QBTMParser.QbtQueryContext ctx) {
        LOGGER.debug("Exiting QBT Query: " + ctx.getText());
        if (!containsCondition)
            root = qbtBlockConstructors.get(0);

    }

    @Override
    public void enterQbtCondition(QBTMParser.QbtConditionContext ctx) {
        LOGGER.debug("Entering QBT Condition: " + ctx.getText());
        containsCondition = true;

    }

    @Override
    public void enterQbtPattern(QBTMParser.QbtPatternContext ctx) {
        LOGGER.debug("Entering ForPattern Query: " + ctx.getText());
        switch (Annotation.getTypeEnum(ctx.annotation().getText())) {
            case PJ:
                try {
                    final PJQBTBlockConstructor pjBlockEvalConstructor =
                            new PJQBTBlockConstructor(AntlrUtils.getFullText(ctx.modelPattern()));
                    qbtBlockConstructors.add(pjBlockEvalConstructor.constructQBTBlock());
                } catch (ParseException | TatooineExecutionException e) {
                    LOGGER.error("Cannot consturct PJQBTBlockConstructor :" + e.getMessage());
                }
                break;
            case SPPJ:
                try {
                    final SPPJQBTBlockConstructor sppjBlockEvalConstructor =
                            new SPPJQBTBlockConstructor(AntlrUtils.getFullText(ctx.modelPattern()));
                    qbtBlockConstructors.add(sppjBlockEvalConstructor.constructQBTBlock());
                } catch (ParseException | TatooineExecutionException e) {
                    LOGGER.error("Cannot consturct SPPJQBTBlockConstructor :" + e.getMessage());
                }
                break;
            case PR:
                try {
                    final PRQBTBlockConstructor prBlockEvalConstructor =
                            new PRQBTBlockConstructor(AntlrUtils.getFullText(ctx.modelPattern()));
                    qbtBlockConstructors.add(prBlockEvalConstructor.constructQBTBlock());
                } catch (ParseException | TatooineExecutionException e) {
                    LOGGER.error("Cannot consturct PRQBTBlockConstructor :" + e.getMessage());
                }
                break;
            default:
                break;
        }
    }

    /**
     * Returns the qbt blocks constructors.
     * 
     * @return the qbt blocks constructors.
     */
    public List<QBTPattern> getQBTBlockConstructors() {
        return qbtBlockConstructors;
    }

    /**
     * Returns the last constructed qbt block.
     * 
     * @return the last constructed qbt block.
     */
    public QBTPattern getQBTBlockRoot() {

        return root;
    }

    /**
     * Returns the view name.
     * 
     * @return the view name.
     */
    public String getViewName() {
        return viewName;
    }

}