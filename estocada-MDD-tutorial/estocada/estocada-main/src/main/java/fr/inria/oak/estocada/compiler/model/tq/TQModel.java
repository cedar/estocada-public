package fr.inria.oak.estocada.compiler.model.tq;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class TQModel extends Model {
	public final static String ID = "TQ";

	@Inject
	public TQModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
		super(ID, Format.TSV, Language.TQL, queryBlockTreeBuilder, blockEncoder);
	}
}
