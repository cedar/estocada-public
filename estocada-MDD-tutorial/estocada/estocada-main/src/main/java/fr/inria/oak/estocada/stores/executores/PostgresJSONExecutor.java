package fr.inria.oak.estocada.stores.executores;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * PostgresJSON Executor
 * 
 * @author Rana Alotaibi
 *
 */
public class PostgresJSONExecutor implements ExecutorInterface {

    private static final Logger LOGGER = Logger.getLogger(PostgresJSONExecutor.class);
    private Connection conn = null;

    @Override
    public void createConnection() {
        final String url = "jdbc:postgresql://localhost:5432/mimic";
        final String user = "postgres";
        final String password = "postgres";
        try {
            conn = DriverManager.getConnection(url, user, password);
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
            LOGGER.info("Connection Created");
        } catch (SQLException e) {
            LOGGER.info(e.getMessage());
        }
    }

    @Override
    public long executeQuery(String nativeQuery) {
        Statement statement;
        ResultSet resultSet;
        long queryTime = 0;
        try {
            final long start = System.currentTimeMillis();
            statement = conn.createStatement();
            statement.setQueryTimeout(1500);
            resultSet = statement.executeQuery(nativeQuery);
            final long end = System.currentTimeMillis();
            queryTime += ((end - start));
            resultSet.close();
            statement.close();
            LOGGER.info("***Finished Executing**" + queryTime);
        } catch (SQLException e) {
            LOGGER.info(e.getMessage());
        }
        return queryTime;
    }
}
