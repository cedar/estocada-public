package fr.inria.oak.estocada.compiler.model.sm;

/**
 * SM predicates
 * 
 * @author ranaalotaibi
 *
 */
public enum Predicate {
    NAME("name"),
    SIZE("size"),
    ZERO("zero"),
    IDENTITY("identity"),
    ADD("add"),
    ADDS("add_s"),
    MULTI("multi"),
    MULTIS("multi_s"),
    MULTIE("multi_e"),
    DIV("div"),
    DIVS("div_s"),
    TRANS("tr"),
    INVERSE("in"),
    DET("det"),
    ADJ("adj"),
    COFA("cof"),
    TRACE("trace"),
    DIAG("diag"),
    POW("pow"),
    AVG("avg"),
    MEAN("mean"),
    VAR("var"),
    SD("sd_sm"),
    COLSUM("colSums_sm"),
    COLMEAN("colMeans_sm"),
    COLVARS("colVars_sm"),
    COLSDS("colSds_sm"),
    COLMAX("colMaxs_sm"),
    COLMINS("colMins_sm"),
    ROWSUM("rowSums_sm"),
    ROWMEAN("rowMeans_sm"),
    ROWVARS("rowVars_sm"),
    ROWSDS("rowSds_sm"),
    ROWMAX("rowMaxs_sm"),
    ROWMIN("rowMins_sm"),
    CUMSUM("cumsum_sm"),
    CUMPROD("cumprod_sm"),
    CUMMIN("cummin_sm"),
    CUMMAX("cummax_sm"),
    EQUALS("eq_sm");

    private final String str;

    private Predicate(final String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
