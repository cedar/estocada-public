package fr.inria.oak.estocada.compiler.model.pr;

import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateConstructor;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.Row;
import fr.inria.oak.estocada.compiler.exceptions.ReturnConstructException;

/**
 * PR ReturnTemplateConstructor which implements {@link ReturnTemplateConstructor}.
 * 
 * @author Rana Alotaibi
 */
public class PRReturnTemplateConstructor implements ReturnTemplateConstructor {
    private final PRConstructReturnTermVisitor constructVisitor;

    public PRReturnTemplateConstructor() {
        constructVisitor = new PRConstructReturnTermVisitor();
    }

    @Override
    public final String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
        return constructVisitor.construct(template, row);
    }

    private class PRConstructReturnTermVisitor implements ReturnTemplateVisitor {
        private StringBuilder builder;
        private Row row;

        public String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
            builder = new StringBuilder();
            this.row = row;
            template.accept(this);
            return builder.toString();
        }

        @Override
        public void visit(final ReturnTemplate template) {
            // NOP (no construction of the optionals)
        }

        @Override
        public void visitPre(final ReturnConstructTerm term) {
            // NOP (no construction)
        }

        @Override
        public void visitPost(final ReturnConstructTerm term) {
            // NOP (no construction)
        }

        @Override
        public void visit(final ReturnVariableTerm term) {
            if (builder.length() > 0) {
                builder.append(",");
            }
            if (row == null) {
                builder.append(term.getVariable().toString());
            } else {
                builder.append(row.getValue(term.getVariable()).toString());
            }
        }

        @Override
        public void visit(final ReturnStringTerm term) {
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(term.toString());
        }
    }
}
