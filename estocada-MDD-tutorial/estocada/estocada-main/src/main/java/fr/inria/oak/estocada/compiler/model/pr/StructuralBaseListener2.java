package fr.inria.oak.estocada.compiler.model.pr;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.miscellaneous.Tuple;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Structural;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * PR StructuralBaseListener
 * 
 * @author Rana Alotaibi
 */
abstract class StructuralBaseListener2 extends SQLBaseListener {
    protected final VariableFactory sqlVariableFactory;
    protected final VariableMapper variableMapper;
    protected final ReturnTemplateListener2 returnListener;
    static List<Tuple<Variable, PathExpression>> definitions;
    protected Variable currentVar;
    protected Variable leftJoinVariable;
    protected String bindingRelation;
    protected String relationName;
    protected Map<String, Object> relationColumnMapper;
    protected Map<String, Object> relationLefttermMapper;
    protected Map<String, Object> relationRighttermMapper;
    protected Map<String, String> relationMapper;
    static List<Atom> encoding;
    private List<String> models;

    public StructuralBaseListener2(final VariableFactory aqlVariableFactory, final VariableMapper variableMapper,
            final ReturnTemplateListener2 returnListener) {
        this.sqlVariableFactory = checkNotNull(aqlVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
        this.returnListener = returnListener;

    }

    public Structural parse(final String str) throws ParseException {
        definitions = new ArrayList<Tuple<Variable, PathExpression>>();
        currentVar = null;
        encoding = new ArrayList<Atom>();
        models = new ArrayList<String>();
        models.add(PRModel.ID);
        relationColumnMapper = new LinkedHashMap<String, Object>();
        relationLefttermMapper = new LinkedHashMap<String, Object>();
        relationRighttermMapper = new LinkedHashMap<String, Object>();
        relationMapper = new LinkedHashMap<String, String>();
        leftJoinVariable = null;
        returnListener.parse(BlockListener2.returnStatement);
        final SQLLexer lexer = new SQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final SQLParser parser = new SQLParser(tokens);
        final ParserRuleContext tree = createParseTree(parser);
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        for (String variable : returnListener.getReferdVariables()) {
            definitions.add(new Tuple<Variable, PathExpression>(variableMapper.getVariable(variable.split("\\.")[1]),
                    new PathExpression(PRModel.ID, new HashSet<Variable>(), encoding, null)));
        }

        return new Structural(definitions, models);
    }

    protected abstract ParserRuleContext createParseTree(final SQLParser parser);
}
