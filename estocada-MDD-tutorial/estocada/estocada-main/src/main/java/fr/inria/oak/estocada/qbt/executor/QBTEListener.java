// Generated from QBTE.g4 by ANTLR 4.7.2

package fr.inria.oak.estocada.qbt.executor;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link QBTEParser}.
 */
public interface QBTEListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link QBTEParser#queryBlock}.
	 * @param ctx the parse tree
	 */
	void enterQueryBlock(QBTEParser.QueryBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#queryBlock}.
	 * @param ctx the parse tree
	 */
	void exitQueryBlock(QBTEParser.QueryBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#forPattern}.
	 * @param ctx the parse tree
	 */
	void enterForPattern(QBTEParser.ForPatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#forPattern}.
	 * @param ctx the parse tree
	 */
	void exitForPattern(QBTEParser.ForPatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#blockPattern}.
	 * @param ctx the parse tree
	 */
	void enterBlockPattern(QBTEParser.BlockPatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#blockPattern}.
	 * @param ctx the parse tree
	 */
	void exitBlockPattern(QBTEParser.BlockPatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterPattern(QBTEParser.PatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitPattern(QBTEParser.PatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#annotation}.
	 * @param ctx the parse tree
	 */
	void enterAnnotation(QBTEParser.AnnotationContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#annotation}.
	 * @param ctx the parse tree
	 */
	void exitAnnotation(QBTEParser.AnnotationContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjPattern}.
	 * @param ctx the parse tree
	 */
	void enterSjPattern(QBTEParser.SjPatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjPattern}.
	 * @param ctx the parse tree
	 */
	void exitSjPattern(QBTEParser.SjPatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(QBTEParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(QBTEParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#conditionAtom}.
	 * @param ctx the parse tree
	 */
	void enterConditionAtom(QBTEParser.ConditionAtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#conditionAtom}.
	 * @param ctx the parse tree
	 */
	void exitConditionAtom(QBTEParser.ConditionAtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjQuery}.
	 * @param ctx the parse tree
	 */
	void enterSjQuery(QBTEParser.SjQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjQuery}.
	 * @param ctx the parse tree
	 */
	void exitSjQuery(QBTEParser.SjQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjCollectionName}.
	 * @param ctx the parse tree
	 */
	void enterSjCollectionName(QBTEParser.SjCollectionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjCollectionName}.
	 * @param ctx the parse tree
	 */
	void exitSjCollectionName(QBTEParser.SjCollectionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjTextSearch}.
	 * @param ctx the parse tree
	 */
	void enterSjTextSearch(QBTEParser.SjTextSearchContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjTextSearch}.
	 * @param ctx the parse tree
	 */
	void exitSjTextSearch(QBTEParser.SjTextSearchContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjProjectFields}.
	 * @param ctx the parse tree
	 */
	void enterSjProjectFields(QBTEParser.SjProjectFieldsContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjProjectFields}.
	 * @param ctx the parse tree
	 */
	void exitSjProjectFields(QBTEParser.SjProjectFieldsContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjFieldName}.
	 * @param ctx the parse tree
	 */
	void enterSjFieldName(QBTEParser.SjFieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjFieldName}.
	 * @param ctx the parse tree
	 */
	void exitSjFieldName(QBTEParser.SjFieldNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#sjConstant}.
	 * @param ctx the parse tree
	 */
	void enterSjConstant(QBTEParser.SjConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#sjConstant}.
	 * @param ctx the parse tree
	 */
	void exitSjConstant(QBTEParser.SjConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(QBTEParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(QBTEParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#wherePattern}.
	 * @param ctx the parse tree
	 */
	void enterWherePattern(QBTEParser.WherePatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#wherePattern}.
	 * @param ctx the parse tree
	 */
	void exitWherePattern(QBTEParser.WherePatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link QBTEParser#returnPattern}.
	 * @param ctx the parse tree
	 */
	void enterReturnPattern(QBTEParser.ReturnPatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link QBTEParser#returnPattern}.
	 * @param ctx the parse tree
	 */
	void exitReturnPattern(QBTEParser.ReturnPatternContext ctx);
}