package fr.inria.oak.estocada.rewriting.decoder.pj;

import java.io.File;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class Test {
    private static final File INPUT_QUERY_FILE =
            new File("src/test/resources/compiler/pj-translator/test03-Translator/rewriting");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {

        Parameters.init();
        //        Catalog catalog = Catalog.getInstance();
        //        catalog.delete();
        //        // Register
        //        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("testMM");
        //        // Columns names
        //        final List<String> colNames = new ArrayList<String>();
        //        colNames.add("SUBJECT_ID");
        //        colNames.add("HADM_ID");
        //        colNames.add("LABEL");
        //
        //        // NRSMD
        //        final NRSMD NRSMD = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
        //                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
        //        final Map<String, Integer> serviceNRSMDMapping = new HashMap<>();
        //        serviceNRSMDMapping.put("SUBJECT_ID", 0);
        //        serviceNRSMDMapping.put("HADM_ID", 1);
        //        serviceNRSMDMapping.put("LABEL", 2);
        //        ViewSchema serviceSchema = new ViewSchema(NRSMD, serviceNRSMDMapping);
        //        catalog.add("V1_JSONT", storageReferencePostgresSQL1, null, serviceSchema);
        //
        //        final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        //        final PJTranslator pjTranslator = new PJTranslator(rw, serviceSchema.getNRSMD());
        //        System.out.print(pjTranslator.translate());

        /*final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        final LogOperator pjTranslator = new PJDecoder().decode(catalog, rw);
        System.out.print(pjTranslator.toString());*/
        testPhy();
    }

    private static StorageReference getPostgresSQLStorageReference(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        return gsr;
    }

    static void testPhy() throws Exception {
        //
        //        final PhyPJQLEval eval = makePhyEval();
        //        System.out.print(Arrays.asList(eval.getNRSMD().getColNames()));
        //        eval.open();
        //        while (eval.hasNext()) {
        //            NTuple tuple = eval.next();
        //            System.out.print(tuple.getNestedField(1).get(0));
        //
        //        }
        //        eval.close();
    }

    //    private static PhyPJQLEval makePhyEval() throws Exception {
    //
    //        final String QUERY_EVAL_S =
    //                "SELECT  T.data ->>'name' AS name, T.data->'addresses' AS addresses FROM testMM AS T";
    //        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("testMM");
    //
    //        NRSMD child1 =
    //                new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
    //                        new String[] { "streetaddress", "city" }, new NRSMD[0]);
    //
    //        NRSMD nrsmd = new NRSMD(4,
    //                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.INTEGER_TYPE,
    //                        TupleMetadataType.INTEGER_TYPE, TupleMetadataType.TUPLE_TYPE },
    //                new String[] { "name", "age", "id", "addresses" }, new NRSMD[] { child1 });
    //
    //        return new PhyPJQLEval(QUERY_EVAL_S, storageReferencePostgresSQL1, nrsmd);
    //    }
}
