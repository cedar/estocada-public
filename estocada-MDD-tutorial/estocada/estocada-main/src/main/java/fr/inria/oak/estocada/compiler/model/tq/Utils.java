package fr.inria.oak.estocada.compiler.model.tq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.StringConstant;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.estocada.compiler.Block;
import fr.inria.oak.estocada.compiler.Condition;
import fr.inria.oak.estocada.compiler.ReturnTerm;

public class Utils {

	public static StringConstant toTerm(final String str) {
		return new StringConstant(str);
	}

	public static Variable toTermVar(final String str) {
		return new Variable(str);
	}

	private static String getCollectionPredicate(final Block block) {
		return Predicate.COLUMN + "_" + block.getQueryName();
	}

	public static Atom getColumnAtom(final Block block, final Variable rootVar, final Variable freshVar,
	        final String colIndex) {
		return new Atom(getCollectionPredicate(block), rootVar, freshVar, Utils.toTerm(colIndex));
	}

	public static Function<Condition, List<Atom>> conditionEncoding = c -> new ArrayList<Atom>(
	        Arrays.asList(new Atom(Predicate.EQUALS.toString(), c.getLeftOp().getTerm(), c.getRightOp().getTerm())));

	public static List<Relation> getGlobalRelationsToEnsure(final String documentNamePrefix,
	        final Set<String> documentNames, final String viewNamePrefix, final String viewName) {
		final List<Relation> relations = new ArrayList<Relation>();
		relations.add(new Relation(Predicate.COLUMN.toString(), 3));
		relations.add(new Relation(Predicate.EQUALS.toString(), 2));
		for (final String documentName : documentNames) {
			relations.add(new Relation(Predicate.FILE.toString() + documentNamePrefix + documentName, 1));
		}

		relations.addAll(getTargetRelationsToEnsure(viewNamePrefix, viewName));

		return relations;
	}

	public static List<Relation> getTargetRelationsToEnsure(final String viewNamePrefix, final String viewName) {
		final List<Relation> relations = new ArrayList<Relation>();
		relations.add(new Relation(Predicate.COLUMN.toString() + viewNamePrefix + viewName, 3));
		relations.add(new Relation(Predicate.EQUALS.toString() + viewNamePrefix + viewName, 2));
		relations.add(new Relation(Predicate.EQUALS.toString(), 2));
		relations.add(new Relation(fr.inria.oak.estocada.compiler.Predicate.EQUALS.toString(), 2));
		return relations;
	}

	public static boolean isObject(final ReturnTerm term) {
		return term.hasParent() && !term.getParent().getElement().isEmpty();
	}
}
