package fr.inria.oak.estocada.compiler.model.tq.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Tgd;
import fr.inria.oak.estocada.compiler.ChildBlock;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeVisitor;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.VariableCopier;
import fr.inria.oak.estocada.compiler.model.tq.TQExtractVariableToCreatedNodeVisitor;
import fr.inria.oak.estocada.compiler.model.tq.Utils;
import fr.inria.oak.estocada.rewriter.Comment;

@Singleton
class ForwardEncoderNestedBlockTreeVisitor implements QueryBlockTreeVisitor {
	private final TQExtractVariableToCreatedNodeVisitor	tQExtractVariableToCreatedNodeVisitor;
	private ImmutableList.Builder<Constraint>			builder;
	private boolean										includeComments;

	@Inject
	public ForwardEncoderNestedBlockTreeVisitor(final VariableCopier variableCopier,
	        final TQExtractVariableToCreatedNodeVisitor tQExtractVariableToCreatedNodeVisitor) {
		this.tQExtractVariableToCreatedNodeVisitor = tQExtractVariableToCreatedNodeVisitor;
	}

	public List<Constraint> compileConstraints(final QueryBlockTree nbt, boolean includeComments) {
		builder = ImmutableList.builder();
		this.includeComments = includeComments;
		nbt.accept(this);
		return builder.build();
	}

	@Override
	public void visit(final QueryBlockTree tree) {
		// TODO Auto-generated method stub
	}

	@Override
	public void visit(ChildBlock block) {
		// No Implementation Since There is no Child Block

	}

	@Override
	public void visit(final RootBlock block) {

		if (!block.getPattern().isEmpty()) {
			if (includeComments) {
				builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
			}
			builder.add(getForwardConstraintForBodyEncoding(block));
		}
	}

	private Constraint getForwardConstraintForBodyEncoding(final RootBlock block) {
		final String queryName = block.getQueryName();
		final List<Atom> premise = new ArrayList<Atom>();
		premise.addAll(block.getPattern().encoding(Utils.conditionEncoding));
		final List<Atom> conclusion = new ArrayList<Atom>();
		conclusion.addAll(tQExtractVariableToCreatedNodeVisitor.encode(block, queryName));
		return new Tgd(premise, conclusion);
	}

}