package fr.inria.oak.estocada.compiler.model.sppj.naive;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.compiler.Element;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.model.sppj.ArrayElementFactory;
import fr.inria.oak.estocada.compiler.model.sppj.DataType;
import fr.inria.oak.estocada.compiler.model.sppj.Predicate;
import fr.inria.oak.estocada.compiler.model.sppj.Utils;

abstract class SPPJBaseForwardEncoderReturnTermVisitor implements ReturnTemplateVisitor {
    protected final ArrayElementFactory arrayElementFactory;
    protected ImmutableList.Builder<Atom> builder;
    protected String viewName;

    @Inject
    public SPPJBaseForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory) {
        this.arrayElementFactory = arrayElementFactory;
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        if (!term.getElement().isEmpty() && (!term.hasChildren() || !term.hasNonConstructChild())) {
            builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, getParentBlockCreatedNode(term),
                    term.getCreatedNode(), term.getElement().toTerm(), Utils.toTerm(DataType.OBJECT.toString())));
        }
    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, getParentBlockCreatedNode(term),
                term.getCreatedNode(),
                Utils.isObject(term) ? getParentBlockElement(term).toTerm() : arrayElementFactory.getElement(term),
                Utils.getDataType(term)));
    }

    @Override
    public void visit(final ReturnStringTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, getParentBlockCreatedNode(term),
                term.toTerm(),
                Utils.isObject(term) ? getParentBlockElement(term).toTerm() : arrayElementFactory.getElement(term),
                Utils.getDataType(term)));
    }

    protected abstract Element getParentBlockElement(final ReturnTerm term);

    protected abstract Term getParentBlockCreatedNode(final ReturnTerm term);
}