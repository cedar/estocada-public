package fr.inria.oak.estocada.rewriting.decoder.aj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.exceptions.QueryTranslationException;

/**
 * AQL Translator. It translates a given AJ model conjunctive query into AQL syntax
 * 
 * @author ranaalotaibi
 *
 */
public class AJTranslator {

    /**
     * Some characters and keywords that are used in AQL Surface syntax
     */
    private static final Character COLON = ':';
    private static final Character SPACE = ' ';
    private static final Character DOT = '.';
    private static final Character EQUAL = '=';
    private static final Character LParenthesis = '(';
    private static final Character RParenthesis = ')';
    private static final String RETURN = " RETURN ";
    private static final String FOR = " FOR ";
    private static final String IN = " IN ";
    private static final String DATASET = " DATASET ";
    private static final String WHERE = " WHERE ";
    private static final String COMMA = " , ";

    private StringBuilder constructedRecord = new StringBuilder();
    private BiMap<String, String> headVariables = HashBiMap.create();
    private BiMap<String, String> conditionVariables = HashBiMap.create();

    /**
     * Returns AQL surface syntax query
     * 
     * @param query
     *            given AQL conjunctive query representation
     * @return Corresponding AQL Surface syntax
     * @throws QueryTranslationException
     */
    public String translate(ConjunctiveQuery cq) throws QueryTranslationException {
        Collection<Atom> body = cq.getBody();
        StringBuilder aqlquery = new StringBuilder();
        constructedFields(cq);
        aqlquery.append(generateForClause(body));
        aqlquery.append(generateReturnClause(body, cq.getHead()));

        return aqlquery.toString();
    }

    /**
     * Returns the corresponding ReturnClause for a given CQ
     * 
     * @param head
     *            the head of the AQL conjunctive query representation
     * @return returnClause the corresponding Return Clause for a given CQ
     * 
     * @throws QueryTranslationException
     */
    protected String generateReturnClause(Collection<Atom> body, final List<Term> head)
            throws QueryTranslationException {
        StringBuilder returnCaluse = new StringBuilder();
        final String[] varPositions = new String[head.size()];
        boolean first = true;
        for (Iterator<Atom> i = body.iterator(); i.hasNext();) {
            Atom atom = i.next();
            if (atom.toString().startsWith("val") || atom.toString().startsWith("eq") || atom.getTerms().size() == 1) {
                continue;
            } else {
                if (headVariables.containsValue(atom.getTerm(1).toString())) {
                    if (!first)
                        //constructedRecord.append(COMMA);
                        first = false;
                    if (atom.getTerm(3).toString().contains("a")) {
                        //constructedRecord.append(atom.getTerm(0));
                        for (Map.Entry<String, String> map : headVariables.entrySet()) {
                            if (map.getValue().equals(atom.getTerm(1).toString())) {
                                varPositions[head.indexOf(new Variable(map.getKey()))] = atom.getTerm(0).toString();
                            }
                        }

                    } else {
                        //constructedRecord.append(atom.getTerm(1));
                        for (Map.Entry<String, String> map : headVariables.entrySet()) {
                            if (map.getValue().equals(atom.getTerm(1).toString())) {
                                varPositions[head.indexOf(new Variable(map.getKey()))] = atom.getTerm(1).toString();
                            }
                        }

                    }

                }
            }
        }

        int i = 1;
        for (String str : varPositions) {
            constructedRecord.append(str);
            if (i != varPositions.length) {
                constructedRecord.append(COMMA);
            }
            i++;
        }
        return returnCaluse.append(RETURN + constructedRecord).toString();
    }

    /**
     * Tags the fields that need to be constructed
     * 
     * @param body
     *            CQ body
     * @throws QueryTranslationException
     */
    protected void constructedFields(ConjunctiveQuery cq) throws QueryTranslationException {
        boolean valAtom = false;
        final Collection<Atom> body = cq.getBody();
        final Collection<Term> head = cq.getHead();
        for (Iterator<Atom> i = body.iterator(); i.hasNext();) {
            Atom currentAtom = i.next();
            if (currentAtom.getPredicate().startsWith("eq") && !(currentAtom.getTerm(1).isConstant())) {
                if (!valAtom)
                    headVariables.put(currentAtom.getTerm(1).toString(), currentAtom.getTerm(0).toString());
                else
                    headVariables.put(headVariables.inverse().get(currentAtom.getTerm(0).toString()),
                            currentAtom.getTerm(1).toString());
            }
            if (currentAtom.getPredicate().startsWith("val")) {
                valAtom = true;
                headVariables.put(currentAtom.getTerm(1).toString(), currentAtom.getTerm(0).toString());
            }
            if (currentAtom.getTerms().size() > 2 && head.contains(currentAtom.getTerm(1))) {
                headVariables.put(currentAtom.getTerm(1).toString(), currentAtom.getTerm(1).toString());
            }
            if (currentAtom.getPredicate().startsWith("eq") && currentAtom.getTerm(1).isConstant()) {
                conditionVariables.put(currentAtom.getTerm(0).toString(), currentAtom.getTerm(1).toString());
            }
        }
    }

    /**
     * Returns the corresponding forClause for a given CQ
     * 
     * @param body
     *            CQ body
     * @return forCaluse the corresponding for Clause for a given CQ
     * 
     * @throws QueryTranslationException
     */
    protected String generateForClause(Collection<Atom> body) throws QueryTranslationException {
        final StringBuilder forClause = new StringBuilder();
        final StringBuilder forVariableBinding = new StringBuilder();
        final StringBuilder forVariableBindingLocal = new StringBuilder();

        final Map<Term, List<Term>> variableDependencies = new HashMap<>();
        final Map<Term, String> variableBinding = new HashMap<>();
        int countBinding = 0;
        Term root = null;

        boolean first = true;
        for (Iterator<Atom> i = body.iterator(); i.hasNext();) {
            Atom atom = i.next();
            if (atom.getTerms().size() == 4 && atom.getTerm(3).toString().contains("a")) {
                continue;
            }
            if (atom.toString().startsWith("eq") || atom.toString().startsWith("val")) {
                continue;
            } else {
                if ((atom.getTerms().size() == 1)) {
                    forVariableBinding.append(atom.getTerm(0));
                    forVariableBinding.append(IN);
                    forVariableBinding.append(DATASET);
                    forVariableBinding.append(atom.getPredicate());
                    root = atom.getTerm(0);

                } else {
                    if (!headVariables.containsValue(atom.getTerm(1).toString())
                            && !conditionVariables.containsKey(atom.getTerm(1).toString())) {
                        //                        if (!first)
                        //                            forVariableBinding.append(COMMA);
                        //                        first = false;
                        forVariableBindingLocal.append(atom.getTerm(1));
                        forVariableBindingLocal.append(IN);
                        forVariableBindingLocal.append(atom.getTerm(0));
                        forVariableBindingLocal.append(DOT);
                        forVariableBindingLocal.append(atom.getTerm(2).toString().replace("\"", ""));
                        forVariableBindingLocal.append(SPACE);

                        if (variableDependencies.get(atom.getTerm(0)) == null) {
                            final List<Term> dep = new ArrayList<>();
                            dep.add(atom.getTerm(1));
                            variableDependencies.put(atom.getTerm(0), dep);
                        } else {
                            final List<Term> dep = variableDependencies.get(atom.getTerm(0));
                            dep.add(atom.getTerm(1));
                            variableDependencies.put(atom.getTerm(0), dep);
                        }
                        variableBinding.put(atom.getTerm(1), forVariableBindingLocal.toString());
                        forVariableBindingLocal.setLength(0);
                        countBinding++;

                    } else {//TODO:Fix this should be moved to sperate where function.
                        if (conditionVariables.containsKey(atom.getTerm(1).toString())) {
                            forVariableBinding.append(WHERE);
                            forVariableBinding.append(atom.getTerm(0));
                            forVariableBinding.append(DOT);
                            forVariableBinding.append(atom.getTerm(2));
                            forVariableBinding.append(EQUAL);
                            forVariableBinding.append(conditionVariables.get(atom.getTerm(1).toString()));
                        } else {
                            if (headVariables.containsValue(atom.getTerm(1).toString())
                                    && !variableBinding.containsKey(atom.getTerm(0))) {
                                forVariableBindingLocal.append(atom.getTerm(1));
                                forVariableBindingLocal.append(IN);
                                forVariableBindingLocal.append(atom.getTerm(0));
                                forVariableBindingLocal.append(DOT);
                                forVariableBindingLocal.append(atom.getTerm(2).toString().replace("\"", ""));
                                forVariableBindingLocal.append(SPACE);
                                if (variableDependencies.get(atom.getTerm(0)) == null) {
                                    final List<Term> dep = new ArrayList<>();
                                    dep.add(atom.getTerm(1));
                                    variableDependencies.put(atom.getTerm(0), dep);
                                } else {
                                    final List<Term> dep = variableDependencies.get(atom.getTerm(0));
                                    dep.add(atom.getTerm(1));
                                    variableDependencies.put(atom.getTerm(0), dep);
                                }
                                variableBinding.put(atom.getTerm(1), forVariableBindingLocal.toString());
                                forVariableBindingLocal.setLength(0);
                                countBinding++;

                            }

                        }
                    }
                }
            }
        }

        Term parent = root;
        while (countBinding != 0) {
            final List<Term> term1 = variableDependencies.get(parent);
            for (Term term2 : term1) {
                final String bindingStr = variableBinding.get(term2);
                parent = term2;
                forVariableBinding.append(COMMA);
                forVariableBinding.append(bindingStr);
                countBinding--;
            }
        }
        return forClause.append(FOR + forVariableBinding).toString();
    }
}
