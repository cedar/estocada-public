package fr.inria.oak.estocada.compiler.model.tq;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * 
 * TQ StructuralListener
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger log = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener(final ColumnLookUpListener listener,
            @Named("TQLVariableFactory") VariableFactory tqlVariableFactory, VariableMapper variableMapper) {
        super(listener, tqlVariableFactory, variableMapper);
    }

    @Override
    public void enterColumnvariableBinding(TQLParser.ColumnvariableBindingContext ctx) {
        log.debug("Entering FromClasueVariableBinding: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("LookUpExpresstion expected.");
        }
        final Variable var = tqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.getText(), var);
        log.debug("VariableBinding" + ctx.getText());
        currentVar = var;
    }

    @Override
    public void enterSource(TQLParser.SourceContext ctx) {
        log.debug("Entering FromClauseBindingSource: " + ctx.getText());
        defineVariable(columnLookUpListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar));
    }

    @Override
    protected ParserRuleContext createParseTree(final TQLParser parser) {
        return parser.fromClause();
    }
}
