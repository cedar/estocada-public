package fr.inria.oak.estocada.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.conjunctivequery.parser.ParseException;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.pj.full.Predicate;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;

public class CMQRWRunnerUtils {

    public static String readQBTEQuery(final String folderName)
            throws IllegalArgumentException, ParseException, IOException {
        final File directory = new File(folderName);
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.getName().equals("Query.Q")) {
                return FileUtils.readFileToString(file);
            }

        }
        return null;
    }

    public static String readQuery(final String folderName)
            throws IllegalArgumentException, ParseException, IOException {
        final File directory = new File(folderName);
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.getName().equals("QBTQuery.Q")) {
                return FileUtils.readFileToString(file);
            }

        }
        return null;
    }

    public static List<String> readViews(final String folderName) throws IOException {
        final List<String> views = new ArrayList<>();
        final File directory = new File(folderName);
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.getName().contains(".V")) {
                String view = FileUtils.readFileToString(file);
                views.add(view);
            }

        }
        return views;

    }

    public static List<ConjunctiveQuery> findRewriting(final String query, final List<String> views) throws Exception {
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        //CMVQ - Query 
        final QueryBlockTree nbt = builder.buildQueryBlockTree(query);
        final ConjunctiveQuery cqQuery = Utils.parseQuery(getCQ(nbt));
        System.out.println(query);
        QBTQueryBlockTreeBuilder.reIntilize();

        //Views
        final List<QueryBlockTree> qbtViews = new ArrayList<QueryBlockTree>();
        final QBTNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
        for (final String view : views) {
            try {
                QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
                QueryBlockTree nbtView = mixedBuilder.buildQueryBlockTree(view);
                System.out.print(nbtView);
                qbtViews.add(nbtView);

                final Context context = compiler0.compileContext(nbtView,
                        new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), true);
                QBTQueryBlockTreeBuilder.reIntilize();
            } catch (fr.inria.oak.estocada.compiler.exceptions.ParseException e) {
                throw new RuntimeException(e);
            }
        }

        //********************
        //* Find rewriting
        //********************
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(qbtViews, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(cqQuery);
        }

        return reformulations;

    }

    private static List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
        return compilers;
    }

    private static List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private static List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts,
            final Injector injector) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

    final static String getCQ(final QueryBlockTree nbt) {
        final StringBuilder cqBuilder = new StringBuilder();
        cqBuilder.append(nbt.getQueryName());
        cqBuilder.append("<");
        int count = 0;
        List<ReturnTerm> terms = nbt.getRoot().getReturnTemplate().getTerms();
        for (final ReturnTerm term : terms) {
            cqBuilder.append(term.getReferredVariables().iterator().next());
            if (count != (terms.size() - 1))
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(">:-");
        //Body (Path)
        final List<Atom> atomsPath = modifiedPremise(nbt.getRoot().getPattern()
                .encoding(fr.inria.oak.estocada.compiler.model.pj.full.Utils.conditionEncoding));
        count = 0;
        for (final Atom atom : atomsPath) {
            //            if (atom.getPredicate().contains("child_pj")) {
            //                if (atom.getTerm(2).toString().contains("SUBJECT_ID")) {
            //                    if (atom.getTerm(1).toString().contains("b_6")) {
            //                        count++;
            //                        continue;
            //                    }
            //                }
            //            }
            cqBuilder.append(atom);
            if (count != atomsPath.size() - 1)
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(";");
        return cqBuilder.toString();
    }

    public static List<Atom> modifiedPremise(final List<Atom> premise) {
        final List<Atom> copyList = new ArrayList<>(premise);
        final List<Atom> updatedAtoms = new ArrayList<>();
        final List<Term> joinTerms = new ArrayList<Term>();
        final Iterator<Atom> itertaor = copyList.listIterator();
        while (itertaor.hasNext()) {
            final Atom atom = itertaor.next();
            if (atom.getTerms().size() == 2 && atom.getPredicate().equals(Predicate.VAL.toString())) {
                if (atom.getTerms().get(0) instanceof Variable && atom.getTerms().get(1) instanceof Variable) {
                    joinTerms.add(atom.getTerms().get(0));
                    joinTerms.add(atom.getTerms().get(1));
                    itertaor.remove();
                }
            }
        }
        for (final Atom atom : copyList) {
            int index = -1;
            if (atom.getTerms().contains(joinTerms.get(0))) {
                index = atom.getTerms().indexOf(joinTerms.get(0));

            } else {
                if (atom.getTerms().contains(joinTerms.get(1))) {
                    index = atom.getTerms().indexOf(joinTerms.get(1));
                } else {
                    updatedAtoms.add(atom);
                }
            }
            if (index != -1) {
                final List<Term> terms = new ArrayList<Term>();
                terms.addAll(atom.getTerms());
                terms.set(index, joinTerms.get(0));
                final Atom atomNew = new Atom(atom.getPredicate(), terms);
                updatedAtoms.add(atomNew);
            }
        }
        return updatedAtoms;
    }

}
