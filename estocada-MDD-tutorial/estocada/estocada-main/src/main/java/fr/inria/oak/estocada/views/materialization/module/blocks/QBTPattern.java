package fr.inria.oak.estocada.views.materialization.module.blocks;

import java.util.List;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.oak.estocada.views.materialization.module.Annotation;

/**
 * QBT Pattern.
 * 
 * @author ranaalotaibi
 *
 */
public final class QBTPattern {

    final String blockQuery;
    final Annotation blockAnnotation;
    final List<String> blockVariables;
    final NIterator blockPhyEval;
    final NRSMD blockNrsmd;

    /** Constructor **/
    public QBTPattern(final String blockQuery, final Annotation blockAnnotation, final List<String> blockVariables,
            final NIterator blockPhyEval, final NRSMD blockNrsmd) {
        this.blockQuery = blockQuery;
        this.blockVariables = blockVariables;
        this.blockPhyEval = blockPhyEval;
        this.blockNrsmd = blockNrsmd;
        this.blockAnnotation = blockAnnotation;

    }

    /** Get a list of the variables that appear in the block **/
    public List<String> getQBTBlockVariables() {
        return blockVariables;
    }

    /** Get the block PhyEval operator **/
    public NIterator getQBTBlockPhyEval() {
        return blockPhyEval;
    }

    /** Get block annotation **/
    public Annotation getAnnotation() {
        return blockAnnotation;
    }

    /** Get the block query **/
    public String getblockQuery() {
        return blockQuery;
    }

    /** Get the block NRSDM **/
    public NRSMD getBlockNRSMD() {
        return blockNrsmd;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("QBT Pattern [");
        stringBuilder.append("Annotation: " + ((blockAnnotation != null) ? "join" : blockAnnotation) + "\n");
        stringBuilder.append("blockVariables: " + blockVariables + "\n");
        stringBuilder.append("blockPhyEval: " + blockPhyEval.getName() + "\n");
        stringBuilder.append("blockNrsmd: " + blockNrsmd + "\n");
        stringBuilder.append("]");
        return stringBuilder.toString();

    }
}
