package fr.inria.oak.estocada.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.SolrDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Register Views in the Catalog
 * 
 * @author ranaalotaibi
 *
 */
public class RegisterMetaDataManual {

    public static void registerMetaData() throws Exception {

        Parameters.init();
        Catalog catalog = Catalog.getInstance();

        //PJ MIIMIC-Query
        final StorageReference storageReferencePostgresSQLRE = getPostgresSQLStorageReferenceMIMIC("MIMIC");
        // Columns names
        final List<String> gdeltRECols = new ArrayList<String>();
        gdeltRECols.add("psubject_id");

        // PJ MIIMIC NRSMD
        final NRSMD gdeltQueryRWNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, gdeltRECols);
        final Map<String, Integer> gdeltSchemaMapping = new HashMap<>();
        gdeltSchemaMapping.put("psubject_id", 0);
        ViewSchema gdeltschema = new ViewSchema(gdeltQueryRWNRSMD, gdeltSchemaMapping);
        catalog.add("MIMIC", storageReferencePostgresSQLRE, null, gdeltschema);

        final StorageReference storageReferencePostgresSQLPJ = getPostgresSQLStorageReferenceMIMIC("MIMICPJ");
        // Columns names
        final List<String> PJCols = new ArrayList<String>();
        PJCols.add("psubject_id");

        // PJ MIIMIC NRSMD
        final NRSMD PJColsNRSMD = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, PJCols);
        final Map<String, Integer> PJSchemaMapping = new HashMap<>();
        PJSchemaMapping.put("psubject_id", 0);
        ViewSchema PJSchema = new ViewSchema(PJColsNRSMD, PJSchemaMapping);
        catalog.add("MIMICPJ", storageReferencePostgresSQLPJ, null, PJSchema);

        ///PR MIMIC-Query 
        final StorageReference storageReferencePostgresSQLPR = getPostgresSQLStorageReferenceMIMIC("MIMICPR");
        // Columns names
        final List<String> PRCols = new ArrayList<String>();
        PRCols.add("SUBJECT_ID");
        PRCols.add("DOB");
        // PR MIIMIC NRSMD
        final NRSMD PRColsNRSMD = new NRSMD(2,
                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, PRCols);

        final Map<String, Integer> PRSchemaMapping = new HashMap<>();
        PRSchemaMapping.put("SUBJECT_ID", 0);
        PRSchemaMapping.put("DOB", 1);
        ViewSchema PRSchema = new ViewSchema(PRColsNRSMD, PRSchemaMapping);
        catalog.add("MIMICPR", storageReferencePostgresSQLPR, null, PRSchema);

//        //View mimicnote
//        final StorageReference gsrText = getSolrStorageReference("mimic_notes");
//
//        gsrText.setProperty("modelId", "SJ");
//        gsrText.setProperty("viewName", "mimic_notes");
//        gsrText.setProperty("zkhost", "localhost:9983");
//
//        final List<String> solrcolNames = new ArrayList<String>();
//        final Map<String, Integer> solrNRSMDMapping = new HashMap<>();
//        solrcolNames.add("subject_id");
//        solrNRSMDMapping.put("subject_id", 0);
//
//        NRSMD nrsmdsolr = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, solrcolNames);
//        ViewSchema solrschema = new ViewSchema(nrsmdsolr, solrNRSMDMapping);
//        catalog.add("mimic_notes", gsrText, null, solrschema);

	      //View mimicnote
//	      final StorageReference gsrText = getSolrStorageReference("mimic_notes");
//	
//	      gsrText.setProperty("modelId", "SJ");
//	      gsrText.setProperty("viewName", "mimic_notes");
//	      gsrText.setProperty("zkhost", "localhost:9983");
//	
//	      final List<String> solrcolNames = new ArrayList<String>();
//	      final Map<String, Integer> solrNRSMDMapping = new HashMap<>();
//	      solrcolNames.add("HADM_ID");
//	      solrNRSMDMapping.put("HADM_ID", 0);
//	
//	      NRSMD nrsmdsolr = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, solrcolNames);
//	      ViewSchema solrschema = new ViewSchema(nrsmdsolr, solrNRSMDMapping);
//	      catalog.add("mimic_notes", gsrText, null, solrschema);
        
//        //View VREF
//        StorageReference gsrRE = new GSR("VPJRE");
//        gsrRE.setProperty("modelId", "PR");
//        gsrRE.setProperty("viewName", "VPJRE");
//        gsrRE.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
//        gsrRE.setProperty("schema", "mimiciii");
//        final List<String> colNames = new ArrayList<String>();
//        final Map<String, Integer> NRSMDMapping = new HashMap<>();
//        colNames.add("SUBJECT_ID");
//        colNames.add("DOB");
//        colNames.add("GENDER");
//        NRSMDMapping.put("SUBJECT_ID", 0);
//        NRSMDMapping.put("DOB", 1);
//        NRSMDMapping.put("GENDER", 2);
//        NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
//                TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
//        ViewSchema vreschema = new ViewSchema(nrsmd, NRSMDMapping);
//        catalog.add("VPJRE", gsrRE, null, vreschema);

        //View VREM
        StorageReference gsrREM = new GSR("VPJREM");
        gsrREM.setProperty("modelId", "PR");
        gsrREM.setProperty("viewName", "VPJREM");
        gsrREM.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsrREM.setProperty("schema", "mimiciii");
        final List<String> VPJREMcolNames = new ArrayList<String>();
        final Map<String, Integer> VREMNRSMDMapping = new HashMap<>();
        VPJREMcolNames.add("SUBJECT_ID");
        VPJREMcolNames.add("DOB");
        VPJREMcolNames.add("GENDER");
        VREMNRSMDMapping.put("SUBJECT_ID", 0);
        VREMNRSMDMapping.put("DOB", 1);
        VREMNRSMDMapping.put("GENDER", 2);
        NRSMD VREMnrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, VPJREMcolNames);
        ViewSchema vremschema = new ViewSchema(VREMnrsmd, VREMNRSMDMapping);
        catalog.add("VPJREM", gsrREM, null, vremschema);

        //------- JSON Views----------//

//        //View V1_JSONT
//        StorageReference gsr1 = new GSR("VPJPJJSON");
//        gsr1.setProperty("modelId", "PJ");
//        gsr1.setProperty("viewName", "VPJPJJSON");
//        gsr1.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
//        gsr1.setProperty("schema", "mimiciii");
//        gsr1.setProperty("jsoncol", "p"); //TODO: needs to fix this later
//
//        final List<String> V1_JSONTcolNames = new ArrayList<String>();
//        V1_JSONTcolNames.add("SUBJECT_ID");
//        final NRSMD V1_JSONTNRSMD =
//                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE}, V1_JSONTcolNames);
//        final Map<String, Integer> V1_JSONTMapping = new HashMap<>();
//        V1_JSONTMapping.put("SUBJECT_ID", 0);
//        ViewSchema V1_JSONTSchema = new ViewSchema(V1_JSONTNRSMD, V1_JSONTMapping);
//        catalog.add("VPJPJJSON", gsr1, null, V1_JSONTSchema);
        
        
       
        
        //View V2_JSONT
        StorageReference gsr2 = new GSR("V2_JSONT");
        gsr2.setProperty("modelId", "PJ");
        gsr2.setProperty("viewName", "V2_JSONT");
        gsr2.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr2.setProperty("schema", "mimiciii");
        gsr2.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V2_JSONTcolNames = new ArrayList<String>();
        V2_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V2_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V2_JSONTcolNames);
        final Map<String, Integer> V2_JSONTMapping = new HashMap<>();
        V2_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V2_JSONTSchema = new ViewSchema(V2_JSONTNRSMD, V2_JSONTMapping);
        catalog.add("V2_JSONT", gsr2, null, V2_JSONTSchema);

        //View V3_JSONT
        StorageReference gsr3 = new GSR("V3_JSONT");
        gsr3.setProperty("modelId", "PJ");
        gsr3.setProperty("viewName", "V3_JSONT");
        gsr3.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr3.setProperty("schema", "mimiciii");
        gsr3.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V3_JSONTcolNames = new ArrayList<String>();
        V3_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V3_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V3_JSONTcolNames);
        final Map<String, Integer> V3_JSONTMapping = new HashMap<>();
        V3_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V3_JSONTSchema = new ViewSchema(V3_JSONTNRSMD, V3_JSONTMapping);
        catalog.add("V3_JSONT", gsr3, null, V3_JSONTSchema);

        //View V4_JSONT
        StorageReference gsr4 = new GSR("V4_JSONT");
        gsr4.setProperty("modelId", "PJ");
        gsr4.setProperty("viewName", "V4_JSONT");
        gsr4.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr4.setProperty("schema", "mimiciii");
        gsr4.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V4_JSONTcolNames = new ArrayList<String>();
        V4_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V4_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V4_JSONTcolNames);
        final Map<String, Integer> V4_JSONTMapping = new HashMap<>();
        V4_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V4_JSONTSchema = new ViewSchema(V4_JSONTNRSMD, V4_JSONTMapping);
        catalog.add("V4_JSONT", gsr4, null, V4_JSONTSchema);

        //View V5_JSONT
        StorageReference gsr5 = new GSR("V5_JSONT");
        gsr5.setProperty("modelId", "PJ");
        gsr5.setProperty("viewName", "V5_JSONT");
        gsr5.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr5.setProperty("schema", "mimiciii");
        gsr5.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V5_JSONTcolNames = new ArrayList<String>();
        V5_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V5_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V5_JSONTcolNames);
        final Map<String, Integer> V5_JSONTMapping = new HashMap<>();
        V5_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V5_JSONTSchema = new ViewSchema(V5_JSONTNRSMD, V5_JSONTMapping);
        catalog.add("V5_JSONT", gsr5, null, V5_JSONTSchema);

        //View V6_JSONT
        StorageReference gsr6 = new GSR("V6_JSONT");
        gsr6.setProperty("modelId", "PJ");
        gsr6.setProperty("viewName", "V6_JSONT");
        gsr6.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr6.setProperty("schema", "mimiciii");
        gsr6.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V6_JSONTcolNames = new ArrayList<String>();
        V6_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V6_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V6_JSONTcolNames);
        final Map<String, Integer> V6_JSONTMapping = new HashMap<>();
        V6_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V6_JSONTSchema = new ViewSchema(V6_JSONTNRSMD, V6_JSONTMapping);
        catalog.add("V6_JSONT", gsr6, null, V6_JSONTSchema);

        //View V7_JSONT
        StorageReference gsr7 = new GSR("V7_JSONT");
        gsr7.setProperty("modelId", "PJ");
        gsr7.setProperty("viewName", "V7_JSONT");
        gsr7.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr7.setProperty("schema", "mimiciii");
        gsr7.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V7_JSONTcolNames = new ArrayList<String>();
        V7_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V7_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V7_JSONTcolNames);
        final Map<String, Integer> V7_JSONTMapping = new HashMap<>();
        V7_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V7_JSONTSchema = new ViewSchema(V7_JSONTNRSMD, V7_JSONTMapping);
        catalog.add("V7_JSONT", gsr7, null, V7_JSONTSchema);

        //View V8_JSONT
        StorageReference gsr8 = new GSR("V8_JSONT");
        gsr8.setProperty("modelId", "PJ");
        gsr8.setProperty("viewName", "V8_JSONT");
        gsr8.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr8.setProperty("schema", "mimiciii");
        gsr8.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V8_JSONTcolNames = new ArrayList<String>();
        V8_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V8_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V8_JSONTcolNames);
        final Map<String, Integer> V8_JSONTMapping = new HashMap<>();
        V8_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V8_JSONTSchema = new ViewSchema(V8_JSONTNRSMD, V8_JSONTMapping);
        catalog.add("V8_JSONT", gsr8, null, V8_JSONTSchema);

        //View V9_JSONT
        StorageReference gsr9 = new GSR("V9_JSONT");
        gsr9.setProperty("modelId", "PJ");
        gsr9.setProperty("viewName", "V9_JSONT");
        gsr9.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr9.setProperty("schema", "mimiciii");
        gsr9.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V9_JSONTcolNames = new ArrayList<String>();
        V9_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V9_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V9_JSONTcolNames);
        final Map<String, Integer> V9_JSONTMapping = new HashMap<>();
        V9_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V9_JSONTSchema = new ViewSchema(V9_JSONTNRSMD, V9_JSONTMapping);
        catalog.add("V9_JSONT", gsr9, null, V9_JSONTSchema);

        //View V10_JSONT
        StorageReference gsr10 = new GSR("V10_JSONT");
        gsr10.setProperty("modelId", "PJ");
        gsr10.setProperty("viewName", "V10_JSONT");
        gsr10.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr10.setProperty("schema", "mimiciii");
        gsr10.setProperty("jsoncol", "p"); //TODO: needs to fix this later

        final List<String> V10_JSONTcolNames = new ArrayList<String>();
        V10_JSONTcolNames.add("SUBJECT_ID");
        final NRSMD V10_JSONTNRSMD =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, V10_JSONTcolNames);
        final Map<String, Integer> V10_JSONTMapping = new HashMap<>();
        V10_JSONTMapping.put("SUBJECT_ID", 0);
        ViewSchema V10_JSONTSchema = new ViewSchema(V10_JSONTNRSMD, V10_JSONTMapping);
        catalog.add("V10_JSONT", gsr10, null, V10_JSONTSchema);
        
        /////Tutorial 
        final StorageReference gsrText = getSolrStorageReference("mimicTutoText");
    	
	      gsrText.setProperty("modelId", "SJ");
	      gsrText.setProperty("viewName", "mimicTutoText");
	      gsrText.setProperty("zkhost", "localhost:9983");
	
	      final List<String> solrcolNames = new ArrayList<String>();
	      final Map<String, Integer> solrNRSMDMapping = new HashMap<>();
	      solrcolNames.add("SUBJECT_ID");
	      solrcolNames.add("HADM_ID");

	      solrNRSMDMapping.put("SUBJECT_ID", 0);
	      solrNRSMDMapping.put("HADM_ID", 1);
	
	      NRSMD nrsmdsolr = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE}, solrcolNames);
	      ViewSchema solrschema = new ViewSchema(nrsmdsolr, solrNRSMDMapping);
	      catalog.add("mimicTutoText", gsrText, null, solrschema);

	      
	      
	      
	       //View V1_JSONT
	        StorageReference gsr1 = new GSR("VPJPJ");
	        gsr1.setProperty("modelId", "PJ");
	        gsr1.setProperty("viewName", "VPJPJ");
	        gsr1.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
	        gsr1.setProperty("schema", "mimictutorial");
	        gsr1.setProperty("jsoncol", "p"); //TODO: needs to fix this later

	        final List<String> V1_JSONTcolNames = new ArrayList<String>();
	        V1_JSONTcolNames.add("SUBJECT_ID");
	        V1_JSONTcolNames.add("HADM_ID");
	        final NRSMD V1_JSONTNRSMD =
	                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE}, V1_JSONTcolNames);
	        final Map<String, Integer> V1_JSONTMapping = new HashMap<>();
	        V1_JSONTMapping.put("SUBJECT_ID", 0);
	        V1_JSONTMapping.put("HADM_ID", 1);
	        ViewSchema V1_JSONTSchema = new ViewSchema(V1_JSONTNRSMD, V1_JSONTMapping);
	        catalog.add("VPJPJ", gsr1, null, V1_JSONTSchema);
	        
	        
	        //View VREF
	        StorageReference gsrRE = new GSR("VPJPR");
	        gsrRE.setProperty("modelId", "PR");
	        gsrRE.setProperty("viewName", "VPJPR");
	        gsrRE.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
	        gsrRE.setProperty("schema", "mimictutorial");
	        final List<String> colNames = new ArrayList<String>();
	        final Map<String, Integer> NRSMDMapping = new HashMap<>();
	        colNames.add("SUBJECT_ID");
	        colNames.add("DOB");
	        colNames.add("GENDER");
	        NRSMDMapping.put("SUBJECT_ID", 0);
	        NRSMDMapping.put("DOB", 1);
	        NRSMDMapping.put("GENDER", 2);
	        NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
	                TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
	        ViewSchema vreschema = new ViewSchema(nrsmd, NRSMDMapping);
	        catalog.add("VPJPR", gsrRE, null, vreschema);


    }

    /**
     * Storage reference for data in PostgreSQL (MIMIC)
     * 
     * @param collectionName
     * @return Postgres StorgeReference
     * @throws Exception
     */
    private static StorageReference getPostgresSQLStorageReferenceMIMIC(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        gsr.setProperty("modelId", "PR");
        gsr.setProperty("schema", "mimiciii");
        return gsr;
    }

    /**
     * Storage reference for Solr collections
     * 
     * @param collectionName
     * @return Solr StorgeReference
     * @throws Exception
     */
    private static StorageReference getSolrStorageReference(String collectionName) throws Exception {
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("serverUrl", SolrDatabaseLoader.DEFAULT_SOLR_URL);
        gsr.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);
        gsr.setProperty("coreName", collectionName);
        return gsr;

    }

}
