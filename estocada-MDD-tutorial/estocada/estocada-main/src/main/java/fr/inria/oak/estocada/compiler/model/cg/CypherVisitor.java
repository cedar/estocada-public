// Generated from Cypher.g4 by ANTLR 4.7.2

    package  fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CypherParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CypherVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Cypher}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Cypher(CypherParser.OC_CypherContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Statement(CypherParser.OC_StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Query(CypherParser.OC_QueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_RegularQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RegularQuery(CypherParser.OC_RegularQueryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_SingleQuerySinglePart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SingleQuerySinglePart(CypherParser.OC_SingleQuerySinglePartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_SingleQueryMultiPart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SingleQueryMultiPart(CypherParser.OC_SingleQueryMultiPartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_SinglePartQueryNoUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SinglePartQueryNoUpdating(CypherParser.OC_SinglePartQueryNoUpdatingContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_SinglePartQueryUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SinglePartQueryUpdating(CypherParser.OC_SinglePartQueryUpdatingContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_MultiPartQuery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_MultiPartQuery(CypherParser.OC_MultiPartQueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_UpdatingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_UpdatingClause(CypherParser.OC_UpdatingClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ReadingClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ReadingClause(CypherParser.OC_ReadingClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Match}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Match(CypherParser.OC_MatchContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Create}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Create(CypherParser.OC_CreateContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Return}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Return(CypherParser.OC_ReturnContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_With}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_With(CypherParser.OC_WithContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ProjectionBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ProjectionBody(CypherParser.OC_ProjectionBodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ProjectionItemsStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ProjectionItemsStar(CypherParser.OC_ProjectionItemsStarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ProjectionItemsNoStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ProjectionItemsNoStar(CypherParser.OC_ProjectionItemsNoStarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ProjectionItemVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ProjectionItemVariable(CypherParser.OC_ProjectionItemVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ProjectionItemNoVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ProjectionItemNoVariable(CypherParser.OC_ProjectionItemNoVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Order}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Order(CypherParser.OC_OrderContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Limit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Limit(CypherParser.OC_LimitContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_SortItem}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SortItem(CypherParser.OC_SortItemContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Where(CypherParser.OC_WhereContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Pattern(CypherParser.OC_PatternContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PatternPartVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PatternPartVariable(CypherParser.OC_PatternPartVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PatternPartNoVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PatternPartNoVariable(CypherParser.OC_PatternPartNoVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_AnonymousPatternPart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AnonymousPatternPart(CypherParser.OC_AnonymousPatternPartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PatternElementNoParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PatternElementNoParanthesis(CypherParser.OC_PatternElementNoParanthesisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PatternElementParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PatternElementParanthesis(CypherParser.OC_PatternElementParanthesisContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_NodePattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_NodePattern(CypherParser.OC_NodePatternContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_PatternElementChain}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PatternElementChain(CypherParser.OC_PatternElementChainContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_RelationshipPatternBothDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelationshipPatternBothDir(CypherParser.OC_RelationshipPatternBothDirContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_RelationshipPatternLeftDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelationshipPatternLeftDir(CypherParser.OC_RelationshipPatternLeftDirContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_RelationshipPatternRightDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelationshipPatternRightDir(CypherParser.OC_RelationshipPatternRightDirContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_RelationshipDetail}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelationshipDetail(CypherParser.OC_RelationshipDetailContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PropertiesMapLiteral}
	 * labeled alternative in {@link CypherParser#oC_Properties}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PropertiesMapLiteral(CypherParser.OC_PropertiesMapLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_RelationshipTypes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelationshipTypes(CypherParser.OC_RelationshipTypesContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_NodeLabels}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_NodeLabels(CypherParser.OC_NodeLabelsContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_NodeLabel}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_NodeLabel(CypherParser.OC_NodeLabelContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_LabelName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LabelName(CypherParser.OC_LabelNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_RelTypeName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RelTypeName(CypherParser.OC_RelTypeNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Expression(CypherParser.OC_ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_AndExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AndExpression(CypherParser.OC_AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ComparisonSingle}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ComparisonSingle(CypherParser.OC_ComparisonSingleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_ComparisonSingleOrMultiple}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ComparisonSingleOrMultiple(CypherParser.OC_ComparisonSingleOrMultipleContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_StringListNullOperatorExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_StringListNullOperatorExpression(CypherParser.OC_StringListNullOperatorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ListOperatorExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ListOperatorExpression(CypherParser.OC_ListOperatorExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PropertyOrLabelsExpressionAtom}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PropertyOrLabelsExpressionAtom(CypherParser.OC_PropertyOrLabelsExpressionAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_PropertyOrLabelsExpressionLookup}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PropertyOrLabelsExpressionLookup(CypherParser.OC_PropertyOrLabelsExpressionLookupContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_AtomLiteral}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AtomLiteral(CypherParser.OC_AtomLiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_AtomVariable}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AtomVariable(CypherParser.OC_AtomVariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_AtomParenthesizedExpression}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AtomParenthesizedExpression(CypherParser.OC_AtomParenthesizedExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_AtomCase}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_AtomCase(CypherParser.OC_AtomCaseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_LiteralNumber}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LiteralNumber(CypherParser.OC_LiteralNumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_LiteralString}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LiteralString(CypherParser.OC_LiteralStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_LiteralBoolean}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LiteralBoolean(CypherParser.OC_LiteralBooleanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_LiteralMap}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LiteralMap(CypherParser.OC_LiteralMapContext ctx);
	/**
	 * Visit a parse tree produced by the {@code oC_LiteralList}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LiteralList(CypherParser.OC_LiteralListContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_BooleanLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_BooleanLiteral(CypherParser.OC_BooleanLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ListLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ListLiteral(CypherParser.OC_ListLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_PartialComparisonExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PartialComparisonExpression(CypherParser.OC_PartialComparisonExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ParenthesizedExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ParenthesizedExpression(CypherParser.OC_ParenthesizedExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_PropertyLookup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PropertyLookup(CypherParser.OC_PropertyLookupContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_CaseExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_CaseExpression(CypherParser.OC_CaseExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_CaseAlternatives}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_CaseAlternatives(CypherParser.OC_CaseAlternativesContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Variable(CypherParser.OC_VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_NumberLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_NumberLiteral(CypherParser.OC_NumberLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_MapLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_MapLiteral(CypherParser.OC_MapLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_PropertyKeyName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_PropertyKeyName(CypherParser.OC_PropertyKeyNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_IntegerLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_IntegerLiteral(CypherParser.OC_IntegerLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_DoubleLiteral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_DoubleLiteral(CypherParser.OC_DoubleLiteralContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_SchemaName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SchemaName(CypherParser.OC_SchemaNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_ReservedWord}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_ReservedWord(CypherParser.OC_ReservedWordContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_SymbolicName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_SymbolicName(CypherParser.OC_SymbolicNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_LeftArrowHead}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_LeftArrowHead(CypherParser.OC_LeftArrowHeadContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_RightArrowHead}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_RightArrowHead(CypherParser.OC_RightArrowHeadContext ctx);
	/**
	 * Visit a parse tree produced by {@link CypherParser#oC_Dash}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOC_Dash(CypherParser.OC_DashContext ctx);
}