package fr.inria.oak.estocada.compiler.model.sj;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class SJModel extends Model {
	public final static String ID = "SJ";

	@Inject
	public SJModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
		super(ID, Format.JSON, Language.SJQL, queryBlockTreeBuilder, blockEncoder);
	}
}
