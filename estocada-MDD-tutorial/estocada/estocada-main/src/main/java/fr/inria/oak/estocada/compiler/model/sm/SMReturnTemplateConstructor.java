package fr.inria.oak.estocada.compiler.model.sm;

import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateConstructor;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.Row;
import fr.inria.oak.estocada.compiler.exceptions.ReturnConstructException;

/**
 * SM Return template constructor
 * @author ranaalotaibi
 */
public class SMReturnTemplateConstructor implements ReturnTemplateConstructor {
    private final SJConstructReturnTermVisitor constructVisitor;

    public SMReturnTemplateConstructor() {
        constructVisitor = new SJConstructReturnTermVisitor();
    }

    @Override
    public final String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
        return constructVisitor.construct(template, row);
    }

    private class SJConstructReturnTermVisitor implements ReturnTemplateVisitor {
        private StringBuilder builder;
        private Row row;

        public String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
            builder = new StringBuilder();
            this.row = row;
            template.accept(this);
            return builder.toString();
        }

        @Override
        public void visit(final ReturnTemplate template) {
            // NOP (no construction of the optionals)
        }

        @Override
        public void visitPre(final ReturnConstructTerm term) {
            if (!term.getElement().isEmpty()) {
                builder.append("\"").append(term.getElement().toString());
                builder.append("\":");
            }
            if (isObject(term)) {
                builder.append("{");
            } else {
                builder.append("[");
            }
        }

        @Override
        public void visitPost(final ReturnConstructTerm term) {
            if (isObject(term)) {
                builder.append("}");
            } else {
                builder.append("]");
            }
        }

        @Override
        public void visit(final ReturnVariableTerm term) {
            if (row == null) {
                builder.append(term.getVariable().toString());
            } else {
                builder.append("\"").append(row.getValue(term.getVariable()).toString()).append("\"");
            }
        }

        @Override
        public void visit(final ReturnStringTerm term) {
            builder.append(term.toString());
        }

        private boolean isObject(final ReturnConstructTerm term) {
            return !isArray(term);
        }

        private boolean isArray(final ReturnConstructTerm term) {
            if (!term.hasChildren()) {
                return true;
            }
            final ReturnTerm firstChild = term.getChildren().get(0);
            if (firstChild instanceof ReturnVariableTerm || firstChild instanceof ReturnStringTerm) {
                return true;
            }
            if (firstChild instanceof ReturnConstructTerm
                    && ((ReturnConstructTerm) firstChild).getElement().isEmpty()) {
                return true;
            }
            return false;
        }
    }
}
