package fr.inria.oak.estocada.compiler.model.sppj;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class SPPJModel extends Model {
    public final static String ID = "SPPJ";

    @Inject
    public SPPJModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
        super(ID, Format.JSON, Language.SQLPlusPlus, queryBlockTreeBuilder, blockEncoder);
    }
}
