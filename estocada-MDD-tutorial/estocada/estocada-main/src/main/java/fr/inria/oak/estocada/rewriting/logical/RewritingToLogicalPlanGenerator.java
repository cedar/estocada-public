package fr.inria.oak.estocada.rewriting.logical;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;

/**
 * Generates a cross-model/single model logical plan for a given rewriting.
 * 
 * @author ranaalotaibi
 */
public class RewritingToLogicalPlanGenerator {

    private static final Logger LOGGER = Logger.getLogger(RewritingToLogicalPlanGenerator.class);
    private final ConjunctiveQuery rewriitng;
    private Catalog catalog;

    /** Constructor **/
    public RewritingToLogicalPlanGenerator(final ConjunctiveQuery rewriitng) {
        this.rewriitng = rewriitng;
        this.catalog = Catalog.getInstance();
    }

    /**
     * Generates a logical plan for a given cross-model/single model rewriting.
     * 
     * @return This function returns the root operator of the logical plan.
     */
    public LogOperator generate() {

        final RewritingSpliter rewritingSpliter = new RewritingSpliter(rewriitng, catalog);
        final Map<StorageReference, ConjunctiveQuery> subRewritings = rewritingSpliter.split();
        final LogicalPlanBuilder logicalPlanBuilder =
                new LogicalPlanBuilder(rewriitng.getHead(), subRewritings, catalog);
        return logicalPlanBuilder.build();

    }

    public List<Map.Entry<String, String>> getColumnSchemaMapping() {
        final List<Map.Entry<String, String>> mapping = new ArrayList<>();
        final List<Term> head = rewriitng.getHead();
        for (Term term : head) {
            for (Atom atom : rewriitng.getBody()) {
                if (atom.getTerms().size() == 4) {
                    if (atom.getTerm(1).equals(term)) {
                        Map.Entry<String, String> entry = new AbstractMap.SimpleEntry<String, String>(term.toString(),
                                atom.getTerm(2).toString().replace("\"", ""));
                        mapping.add(entry);
                        break;

                    }

                } else {
                    if (atom.getTerms().contains(term)) {
                        int index = atom.getTerms().indexOf(term);
                        ViewSchema viewSchema = catalog.getViewSchema(atom.getPredicate());
                        Map.Entry<String, String> entry = new AbstractMap.SimpleEntry<String, String>(term.toString(),
                                viewSchema.getNRSMD().colNames[index]);
                        mapping.add(entry);
                        break;
                    }

                }

            }
        }
        return mapping;

    }
}
