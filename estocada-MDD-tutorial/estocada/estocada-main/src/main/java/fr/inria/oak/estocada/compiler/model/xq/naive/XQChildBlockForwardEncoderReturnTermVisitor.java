package fr.inria.oak.estocada.compiler.model.xq.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;

@Singleton
public class XQChildBlockForwardEncoderReturnTermVisitor extends XQBaseForwardEncoderReturnTermVisitor {
	private Variable parentBlockCreatedNode;

	@Inject
	public XQChildBlockForwardEncoderReturnTermVisitor() {
		super();
	}

	public List<Atom> encode(final Variable parentBlockCreatedNode, final ReturnTemplate template,
	        final String viewName) {
		builder = ImmutableList.builder();
		this.parentBlockCreatedNode = parentBlockCreatedNode;
		this.viewName = viewName;
		template.accept(this);
		return builder.build();
	}

	@Override
	protected Variable getParentBlockCreatedNode(final ReturnTerm term) {
		return term.hasParent() ? term.getParent().getCreatedNode() : parentBlockCreatedNode;
	}
}