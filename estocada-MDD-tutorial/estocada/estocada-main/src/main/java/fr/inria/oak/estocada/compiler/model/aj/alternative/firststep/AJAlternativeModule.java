package fr.inria.oak.estocada.compiler.model.aj.alternative.firststep;

import fr.inria.oak.estocada.compiler.model.aj.AJModule;

public class AJAlternativeModule extends AJModule {
	@Override
	protected String getPropertiesFileName() {
		return "aj.alternative.first_step.properties";
	}
}
