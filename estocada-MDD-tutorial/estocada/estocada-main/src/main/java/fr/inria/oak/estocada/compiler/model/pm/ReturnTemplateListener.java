package fr.inria.oak.estocada.compiler.model.pm;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * PM Return Template Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
final class ReturnTemplateListener extends MADLIBBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ReturnTemplateListener.class);
    private final VariableMapper variableMapper;

    @Inject
    public ReturnTemplateListener(final VariableMapper variableMapper) {
        this.variableMapper = checkNotNull(variableMapper);
    }
}
