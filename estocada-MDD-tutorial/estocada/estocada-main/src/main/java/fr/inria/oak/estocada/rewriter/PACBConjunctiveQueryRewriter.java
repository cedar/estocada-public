package fr.inria.oak.estocada.rewriter;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import edu.ucsd.db.chase.Assertion;
import edu.ucsd.db.chase.Chase;
import edu.ucsd.db.datalogexpr.Value;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.miscellaneous.Tuple;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.rewriter.legacy.BackchaseWrapper;

public final class PACBConjunctiveQueryRewriter implements ConjunctiveQueryRewriter {
    /* The chase context */
    private final ChaseContext chaseContext;
    /* The backchase context */
    private final BackchaseContext backchaseContext;

    private final List<String> appliedFWConstraints;
    private final List<String> appliedBWConstraints;

    /**
     * Constructs a rewriter with the specified context.
     *
     * @param context
     *            The provenance aware chase & backchase algorithm context.
     */
    public PACBConjunctiveQueryRewriter(final Context context) {
        chaseContext = ChaseUtils.createChaseContext(checkNotNull(context));
        backchaseContext = BackchaseUtils.createBackchaseContext(checkNotNull(context));
        appliedFWConstraints = new ArrayList<>();
        appliedBWConstraints = new ArrayList<>();

    }

    @Override
    public TimedReformulations getTimedReformulations(final ConjunctiveQuery query) throws Exception {
        flush();

        long start = System.nanoTime();
        final Assertion queryAssertion = ChaseUtils.createQueryAssertion(chaseContext.getDb(), query);
        long finish = System.nanoTime();
        final long queryAssertionTimeInNano = finish - start;

        System.out.println("Assertion:- " + queryAssertionTimeInNano / 1e+9 + "s");

        final List<Assertion> assertions = new ArrayList<Assertion>();
        assertions.add(queryAssertion);
        assertions.addAll(chaseContext.getAssertions());

        final Chase chase = new Chase(chaseContext.getTgds(), chaseContext.getEgds(), assertions,
                ChaseUtils.tgdsMapping, ChaseUtils.egdsMapping);

        start = System.nanoTime();
        try {
            chase.run();
        } catch (RuntimeException e) {
            finish = System.nanoTime();
            if (isDocumentNameRelationRelatedException(e)) {
                return new TimedReformulations(queryAssertionTimeInNano, finish - start, -1, -1,
                        new ArrayList<ConjunctiveQuery>());
            } else {
                throw e;
            }
        }
        final ConjunctiveQuery chasedQuery = ChaseUtils.collectResult(query, chaseContext.getDb());
        finish = System.nanoTime();
        final long chaseExecTimeInNano = finish - start;
        //System.out.println("Chase:- " + chaseExecTimeInNano / 1e+9 + "s");

        start = System.nanoTime();
        final ConjunctiveQuery universalPlan = restrict(chasedQuery, backchaseContext.getTargetSchema());
        //System.out.println("Uplan" + universalPlan);
        finish = System.nanoTime();
        final long restrictExecTimeInNano = finish - start;

        final List<Constraint> bk = new ArrayList<Constraint>(backchaseContext.getBackwardConstraints());
        final List<Constraint> newBk = new ArrayList<Constraint>();

        start = System.nanoTime();
        //        for (final Constraint con : bk) {
        //            boolean execulde = false;
        //            for (final Atom uAtom : universalPlan.getBody()) {
        //                if (con.getPremise().get(0).getPredicate().equals(uAtom.getPredicate())) {
        //                    execulde = true;
        //                    break;
        //                }
        //            }
        //            if (execulde) {
        //                newBk.add(con);
        //            }
        //
        //        }
        //        finish = System.nanoTime();
        //        System.out.println("OPT:" + ((finish - start) / 1e+9) + "s");
        @SuppressWarnings("unchecked")
        final Tuple<Long, ArrayList<ConjunctiveQuery>> backchase = BackchaseWrapper.Backchase(query, universalPlan,
                (ArrayList<Constraint>) backchaseContext.getBackwardConstraints(), chaseExecTimeInNano);

        //        final Tuple<Long, ArrayList<ConjunctiveQuery>> backchase =
        //                BackchaseWrapper.Backchase(query, universalPlan, (ArrayList<Constraint>) newBk, chaseExecTimeInNano);
        final long backchaseExecTimeInNano = backchase.first();
        final List<ConjunctiveQuery> rewritings = backchase.second();
        //System.out.println(rewritings);
        // finish = System.nanoTime();
        // final long backchaseExecTimeInNano = finish - start;

        return new TimedReformulations(queryAssertionTimeInNano, chaseExecTimeInNano, restrictExecTimeInNano,
                backchaseExecTimeInNano, rewritings);
    }

    @Override
    public List<ConjunctiveQuery> getReformulations(final ConjunctiveQuery query) throws Exception {
        flush();
        final long startTime = System.nanoTime();
        final Assertion queryAssertion = ChaseUtils.createQueryAssertion(chaseContext.getDb(), query);

        final List<Assertion> assertions = new ArrayList<Assertion>();
        assertions.add(queryAssertion);
        assertions.addAll(chaseContext.getAssertions());
        final Chase chase = new Chase(chaseContext.getTgds(), chaseContext.getEgds(), assertions,
                ChaseUtils.tgdsMapping, ChaseUtils.egdsMapping);

        try {
            chase.run();
        } catch (RuntimeException e) {
            if (isDocumentNameRelationRelatedException(e)) {
                return new ArrayList<ConjunctiveQuery>();
            } else {
                throw e;
            }
        }
        final long chaseTime = (System.nanoTime() - startTime);
        final ConjunctiveQuery chasedQuery = ChaseUtils.collectResult(query, chaseContext.getDb());
        appliedFWConstraints.addAll(chase.appliedConstraints);
        final ConjunctiveQuery universalPlan = restrict(chasedQuery, backchaseContext.getTargetSchema());
        //System.out.println("Uplan " + universalPlan);
        @SuppressWarnings("unchecked")
        final List<ConjunctiveQuery> rewritings = BackchaseWrapper.Backchase(query, universalPlan,
                (ArrayList<Constraint>) backchaseContext.getBackwardConstraints(), chaseTime).second();

        return rewritings;
    }

    private boolean isDocumentNameRelationRelatedException(RuntimeException e) {
        return e.getMessage().startsWith("Retrieving relation \"root_");
    }

    private void flush() {
        Value.restartVarIndex();
        chaseContext.flush();
    }

    /**
     * Returns the restricted query wrt a target schema for the specified query
     * and schema.
     *
     * @param query
     *            The query to be restricted.
     * @param schema
     *            The target schema.
     * @return The restricted query with name "U".
     */
    /*
     * TODO: should we restrict also with the relation arity and access patterns
     * (ie. Relation::equals)?
     *
     * Currently the arity check is handled by the PACB and the access patterns
     * are handled in the encoding.
     */
    private static ConjunctiveQuery restrict(final ConjunctiveQuery query, final RelationalSchema targetSchema) {
        final List<Atom> body = new ArrayList<Atom>();

        for (final Atom atom : query.getBody()) {
            for (final Relation relation : targetSchema.getRelations()) {
                if (relation.getName().equals(atom.getPredicate())) {
                    body.add(atom);
                    break;
                }
            }
        }
        return new ConjunctiveQuery("U", query.getHead(), body);
    }

    public List<String> getAppliedFWConstraints() {
        return appliedFWConstraints;
    }

    public List<String> getAppliedBWConstraints() {
        return appliedBWConstraints;
    }
}
