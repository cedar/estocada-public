package fr.inria.oak.estocada.compiler.model.sj;

/**
 * PJ Data Type
 * 
 * @author ranaalotaibi
 *
 */
public enum DataType {
    OBJECT("o");

    private final String str;

    private DataType(final String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
