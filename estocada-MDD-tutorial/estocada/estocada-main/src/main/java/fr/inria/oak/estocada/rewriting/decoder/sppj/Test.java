package fr.inria.oak.estocada.rewriting.decoder.sppj;

import java.io.File;
import java.util.Collection;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Utils;

public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/main/resources/testSPPJDecoder/reformulation");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        final Collection<ConjunctiveQuery> expectedReformulations = Utils.parseQueries((INPUT_QUERY_FILE));
        final SPPJTranslator spjjTranslator = new SPPJTranslator(expectedReformulations.iterator().next());
        System.out.println(spjjTranslator.translate());
    }

}
