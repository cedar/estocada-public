package fr.inria.oak.estocada.views.materialization.module.RK;

public enum RKDataType {

    INTEGER("integer"),
    VARCHAR("string");

    private final String dataType;

    private RKDataType(final String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return dataType;
    }
}
