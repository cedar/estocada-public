package fr.inria.oak.estocada.compiler;

import fr.inria.oak.commons.conjunctivequery.Term;

/**
 * Part of nested block tree data structure.
 *
 * Interface for return template leaf terms.
 *
 * @author Rana Alotaibi
 * @author Damian Bursztyn
 * 
 */
public interface ReturnLeafTerm {
    /**
     * The conjunctive query term encoding for the return template leaf term.
     * 
     * @return the conjunctive query term encoding for the return template leaf
     *         term.
     */
    Term toTerm();
}