package fr.inria.oak.estocada.compiler.model.rk.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Tgd;
import fr.inria.oak.estocada.compiler.ChildBlock;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeVisitor;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.VariableCopier;
import fr.inria.oak.estocada.compiler.model.rk.RKExtractVariableToCreatedNodeVisitor;
import fr.inria.oak.estocada.compiler.model.rk.Utils;
import fr.inria.oak.estocada.rewriter.Comment;

@Singleton
class RKBackwardEncoderNestedBlockTreeVisitor implements QueryBlockTreeVisitor {
    private final RKExtractVariableToCreatedNodeVisitor rKExtractVariableToCreatedNodeVisitor;
    private ImmutableList.Builder<Constraint> builder;
    private Variable viewSetID;
    private Variable internalMapID;
    private boolean includeComments;

    @Inject
    public RKBackwardEncoderNestedBlockTreeVisitor(
            final RKExtractVariableToCreatedNodeVisitor rKExtractVariableToCreatedNodeVisitor,
            final VariableCopier variableCopier) {
        this.rKExtractVariableToCreatedNodeVisitor = rKExtractVariableToCreatedNodeVisitor;
    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, boolean includeComments,
            final Variable viewSetID, final Variable internalMapID) {
        this.viewSetID = viewSetID;
        this.internalMapID = internalMapID;
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(final RootBlock block) {
        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
            }
            builder.add(getBackwardConstraintForBodyEncoding(block));
        }
    }

    private Constraint getBackwardConstraintForBodyEncoding(final RootBlock block) {

        final String queryName = block.getQueryName();
        final List<Atom> premise = new ArrayList<Atom>();
        final List<Atom> conclusion = new ArrayList<Atom>();
        Atom mapConclusion = null;
        for (Atom atom : block.getPattern().getStructural().encoding()) {
            if (atom.getPredicate().contains("mainmap")) {
                mapConclusion = atom;
                break;
            }
        }
        //premise.addAll(rKExtractVariableToCreatedNodeVisitor.encode(viewSetID, queryName));
        premise.addAll(rKExtractVariableToCreatedNodeVisitor.encode(mapConclusion));
        premise.add(Utils.getCollectionAtom(block, viewSetID));

        if (block.getReturnTemplate().getTerms().get(0) instanceof ReturnConstructTerm) {
            premise.add(Utils.getInternalMapAtom(block, internalMapID));
            //premise.addAll(Utils.getCollectionAtomMap(block, internalMapID));
        }
        conclusion.addAll(block.getPattern().encoding(Utils.conditionEncoding));
        conclusion.addAll(rKExtractVariableToCreatedNodeVisitor.encode(block.getReturnTemplate(), queryName));
        return new Tgd(premise, conclusion);
    }

    @Override
    public void visit(ChildBlock block) {
        // No ChildBlcok so no implementation

    }

}