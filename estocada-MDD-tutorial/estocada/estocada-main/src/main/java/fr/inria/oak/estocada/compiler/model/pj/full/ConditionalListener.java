package fr.inria.oak.estocada.compiler.model.pj.full;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.Condition;
import fr.inria.oak.estocada.compiler.ConditionTerm;
import fr.inria.oak.estocada.compiler.Conditional;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * PJ ConditionalListener
 * 
 * @author Rana Alotaib
 *
 */
@Singleton
final class ConditionalListener extends PJQLBaseListener {
    private static final Logger log = Logger.getLogger(ConditionalListener.class);
    private final ConditionListener conditionListener;
    private List<PathExpression> pathExpressions;
    private List<Condition> conditions;
    private List<String> models;

    @Inject
    public ConditionalListener(final ConditionListener conditionListener) {
        log.setLevel(Level.OFF);
        this.conditionListener = conditionListener;
    }

    /**
     * Parse the condition
     * 
     * @param str
     *            the condition that need to be parsed
     * @return the conditional
     * @throws ParseException
     */
    public Conditional parse(final String str) throws ParseException {
        pathExpressions = new ArrayList<PathExpression>();
        conditions = new ArrayList<Condition>();
        models = new ArrayList<String>();
        models.add(PJModel.ID);
        final PJQLLexer lexer = new PJQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final PJQLParser parser = new PJQLParser(tokens);
        final ParserRuleContext tree = parser.pjWhereClause();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return new Conditional(conditions, pathExpressions, models);
    }

    @Override
    public void enterPjWhereCondEquality(PJQLParser.PjWhereCondEqualityContext ctx) {
        log.debug("Entering WhereCondEquality: " + ctx.getText());
        Condition condition = conditionListener.parse(AntlrUtils.getFullText(ctx));
        //TODO:Fix 
        if (!(condition.getLeftOp().getPathExpressions().isEmpty())) {
            final List<PathExpression> path = condition.getLeftOp().getPathExpressions();
            //Case1
            if (condition.getRightOp().getPathExpressions().isEmpty()
                    && condition.getRightOp().getTerm().isVariable()) {
                final Variable rightVar = (Variable) condition.getRightOp().getTerm();
                final Atom lefPath = path.get(0).encoding().get(0);
                final List<Atom> encoding = new ArrayList<>();
                final Atom updatedAtom = new Atom(lefPath.getPredicate(), lefPath.getTerm(0), rightVar,
                        lefPath.getTerm(2), lefPath.getTerm(3));
                encoding.add(updatedAtom);
                final PathExpression newPath = new PathExpression(path.get(0).getModel(),
                        path.get(0).getReferredVariables(), encoding, rightVar);
                final List<PathExpression> updatedPaths = new ArrayList<>();
                updatedPaths.add(newPath);
                final ConditionTerm left = new ConditionTerm(rightVar, new HashSet<>(), updatedPaths);
                conditions.add(new Condition(left, condition.getOperator(), condition.getRightOp()));

            }
            //Case2
            if (!(condition.getRightOp().getPathExpressions().isEmpty())) {
                final Variable rightVar = (Variable) condition.getRightOp().getTerm();
                final Atom lefPath = path.get(0).encoding().get(0);
                final List<Atom> encoding = new ArrayList<>();
                final Atom updatedAtom = new Atom(lefPath.getPredicate(), lefPath.getTerm(0), rightVar,
                        lefPath.getTerm(2), lefPath.getTerm(3));
                encoding.add(updatedAtom);
                final PathExpression newPath = new PathExpression(path.get(0).getModel(),
                        path.get(0).getReferredVariables(), encoding, rightVar);
                final List<PathExpression> updatedPaths = new ArrayList<>();
                updatedPaths.add(newPath);
                final ConditionTerm left = new ConditionTerm(rightVar, new HashSet<>(), updatedPaths);
                conditions.add(new Condition(left, condition.getOperator(), condition.getRightOp()));
            }
            //Case3 
            if (condition.getRightOp().getPathExpressions().isEmpty()
                    && condition.getRightOp().getTerm().isConstant()) {
                conditions.add(condition);
            }

        }
        //conditions.add(condition);
    }

}
