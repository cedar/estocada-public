package fr.inria.oak.estocada.compiler.model.tq;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.VariableCopier;

@Singleton
public class TQExtractVariableToCreatedNodeVisitor implements ReturnTemplateVisitor {
	public final VariableCopier			variableCopier;
	private ImmutableList.Builder<Atom>	builder;
	private String						queryName;

	@Inject
	public TQExtractVariableToCreatedNodeVisitor(final VariableCopier variableCopier) {
		this.variableCopier = variableCopier;
	}

	public List<Atom> encode(final ReturnTemplate template, final String queryName) {
		builder = ImmutableList.builder();
		this.queryName = queryName;
		template.accept(this);
		return builder.build();
	}

	public List<Atom> encode(final RootBlock block, final String queryName) {
		final List<Atom> viewConclusion = new ArrayList<Atom>();
		List<Term> terms = block.getReturnTemplate().getReferredVariables().stream().map(e -> (Term) e)
		        .collect(Collectors.toList());
		viewConclusion.add(new Atom(queryName, terms));
		return viewConclusion;
	}

	@Override
	public void visit(final ReturnTemplate template) {

	}

	@Override
	public void visitPre(final ReturnConstructTerm term) {

	}

	@Override
	public void visitPost(final ReturnConstructTerm term) {

	}

	@Override
	public void visit(final ReturnVariableTerm term) {

	}

	@Override
	public void visit(ReturnStringTerm term) {
		// NOP (no encoding for string terms)
	}
}