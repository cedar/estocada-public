package fr.inria.oak.estocada.compiler.model.xq;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class XQModel extends Model {
	public final static String ID = "XQ";

	@Inject
	public XQModel(final QueryBlockTreeBuilder queryBlockTreeBuilder,
			final BlockEncoder blockEncoder) {
		super(ID, Format.XML, Language.XQUERY, queryBlockTreeBuilder,
				blockEncoder);
	}
}
