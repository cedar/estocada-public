package fr.inria.oak.estocada.compiler.model.rk;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * @author Damian Bursztyn
 * @author ranaalotaibi (Used Damian's Convention for JQ)
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger log = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener(final LookUpExpressionListener listener,
            @Named("KQLVariableFactory") VariableFactory kqlVariableFactory, VariableMapper variableMapper) {
        super(listener, kqlVariableFactory, variableMapper);
        log.setLevel(Level.INFO);
    }

    @Override
    public void enterFromvariableBinding(KQLParser.FromvariableBindingContext ctx) {
        log.debug("Entering FromClasueVariableBinding: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("LookUpExpresstion expected.");
        }
        final Variable var = kqlVariableFactory.createFreshVar();
        setVarStr = ctx.getText();
        LookUpExpressionListener.setVar = null;
        variableMapper.define(ctx.getText(), var);
        log.debug("VariableBinding" + ctx.getText());
        currentVar = var;
    }

    @Override
    public void enterRkSource(KQLParser.RkSourceContext ctx) {
        log.debug("Entering FromClauseBindingSource: " + ctx.getText());
        defineVariable(lookUpExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar));
        if (LookUpExpressionListener.setVar != null) {
            variableMapper.updateVariable(setVarStr, LookUpExpressionListener.setVar);
        }
    }

    @Override
    public void exitRkFromClasue(KQLParser.RkFromClasueContext ctx) {
        LookUpExpressionListener.currentRootVar = null;
        LookUpExpressionListener.lookUpVariable = null;
        LookUpExpressionListener.currentInternalMap = null;
        LookUpExpressionListener.currentVarVal = null;
        LookUpExpressionListener.setVar = null;
    }

    @Override
    protected ParserRuleContext createParseTree(final KQLParser parser) {
        return parser.rkFromClasue();
    }
}
