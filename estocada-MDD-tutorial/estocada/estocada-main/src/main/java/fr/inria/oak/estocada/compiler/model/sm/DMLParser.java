// Generated from DML.g4 by ANTLR 4.4

package fr.inria.oak.estocada.compiler.model.sm;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DMLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__43=1, T__42=2, T__41=3, T__40=4, T__39=5, T__38=6, T__37=7, T__36=8, 
		T__35=9, T__34=10, T__33=11, T__32=12, T__31=13, T__30=14, T__29=15, T__28=16, 
		T__27=17, T__26=18, T__25=19, T__24=20, T__23=21, T__22=22, T__21=23, 
		T__20=24, T__19=25, T__18=26, T__17=27, T__16=28, T__15=29, T__14=30, 
		T__13=31, T__12=32, T__11=33, T__10=34, T__9=35, T__8=36, T__7=37, T__6=38, 
		T__5=39, T__4=40, T__3=41, T__2=42, T__1=43, T__0=44, ID=45, WHITESPACE=46, 
		STRING=47, INT=48, MATRIX=49, ROWS=50, COLS=51, READ=52;
	public static final String[] tokenNames = {
		"<INVALID>", "'t('", "'/'", "'rowMeans('", "'diag('", "'cumprod('", "'mean('", 
		"'colMaxs('", "'cummin('", "'rowSds('", "'det('", "'colVars('", "'||'", 
		"', ncol='", "';'", "'colMins('", "'&&'", "'='", "'^'", "'rowSums('", 
		"'sd('", "'colSds('", "'&'", "'var('", "'('", "'*'", "'matrix (scan('", 
		"'rowMins('", "'inv('", "',what=numeric(), skip=1), nrow='", "'%*%'", 
		"'colMeans('", "':'", "'cumsum('", "'trace('", "'|'", "'avg('", "'colSums('", 
		"'cummax('", "')'", "'rowMaxs('", "'%/%'", "'+'", "'-'", "'rowVars('", 
		"ID", "WHITESPACE", "STRING", "INT", "MATRIX", "ROWS", "COLS", "READ"
	};
	public static final int
		RULE_dmlQuery = 0, RULE_dmlScript = 1, RULE_dmlStatemnet = 2, RULE_source = 3, 
		RULE_dmlMatrixConstruction = 4, RULE_dmlMatrixConstructionMatrixSource = 5, 
		RULE_dmlExpression = 6, RULE_viewName = 7, RULE_matrixName = 8, RULE_matrixNameExpression = 9, 
		RULE_numericScalar = 10, RULE_filePath = 11;
	public static final String[] ruleNames = {
		"dmlQuery", "dmlScript", "dmlStatemnet", "source", "dmlMatrixConstruction", 
		"dmlMatrixConstructionMatrixSource", "dmlExpression", "viewName", "matrixName", 
		"matrixNameExpression", "numericScalar", "filePath"
	};

	@Override
	public String getGrammarFileName() { return "DML.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public DMLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class DmlQueryContext extends ParserRuleContext {
		public DmlScriptContext dmlScript() {
			return getRuleContext(DmlScriptContext.class,0);
		}
		public ViewNameContext viewName() {
			return getRuleContext(ViewNameContext.class,0);
		}
		public DmlQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterDmlQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitDmlQuery(this);
		}
	}

	public final DmlQueryContext dmlQuery() throws RecognitionException {
		DmlQueryContext _localctx = new DmlQueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_dmlQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(24); viewName();
			setState(25); match(T__12);
			setState(26); dmlScript();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DmlScriptContext extends ParserRuleContext {
		public DmlStatemnetContext dmlStatemnet(int i) {
			return getRuleContext(DmlStatemnetContext.class,i);
		}
		public List<DmlStatemnetContext> dmlStatemnet() {
			return getRuleContexts(DmlStatemnetContext.class);
		}
		public DmlScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlScript; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterDmlScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitDmlScript(this);
		}
	}

	public final DmlScriptContext dmlScript() throws RecognitionException {
		DmlScriptContext _localctx = new DmlScriptContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_dmlScript);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28); dmlStatemnet();
			setState(29); match(T__30);
			setState(35);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(30); dmlStatemnet();
				setState(31); match(T__30);
				}
				}
				setState(37);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DmlStatemnetContext extends ParserRuleContext {
		public SourceContext source() {
			return getRuleContext(SourceContext.class,0);
		}
		public MatrixNameContext matrixName() {
			return getRuleContext(MatrixNameContext.class,0);
		}
		public DmlStatemnetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlStatemnet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterDmlStatemnet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitDmlStatemnet(this);
		}
	}

	public final DmlStatemnetContext dmlStatemnet() throws RecognitionException {
		DmlStatemnetContext _localctx = new DmlStatemnetContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_dmlStatemnet);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38); matrixName();
			setState(39); match(T__27);
			setState(40); source();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceContext extends ParserRuleContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public DmlMatrixConstructionContext dmlMatrixConstruction() {
			return getRuleContext(DmlMatrixConstructionContext.class,0);
		}
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterSource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitSource(this);
		}
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_source);
		try {
			setState(44);
			switch (_input.LA(1)) {
			case T__18:
				enterOuterAlt(_localctx, 1);
				{
				setState(42); dmlMatrixConstruction();
				}
				break;
			case T__43:
			case T__41:
			case T__40:
			case T__39:
			case T__38:
			case T__37:
			case T__36:
			case T__35:
			case T__34:
			case T__33:
			case T__29:
			case T__25:
			case T__24:
			case T__23:
			case T__21:
			case T__20:
			case T__17:
			case T__16:
			case T__13:
			case T__11:
			case T__10:
			case T__8:
			case T__7:
			case T__6:
			case T__4:
			case T__0:
			case ID:
			case INT:
				enterOuterAlt(_localctx, 2);
				{
				setState(43); dmlExpression(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DmlMatrixConstructionContext extends ParserRuleContext {
		public DmlMatrixConstructionMatrixSourceContext dmlMatrixConstructionMatrixSource() {
			return getRuleContext(DmlMatrixConstructionMatrixSourceContext.class,0);
		}
		public DmlMatrixConstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlMatrixConstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterDmlMatrixConstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitDmlMatrixConstruction(this);
		}
	}

	public final DmlMatrixConstructionContext dmlMatrixConstruction() throws RecognitionException {
		DmlMatrixConstructionContext _localctx = new DmlMatrixConstructionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_dmlMatrixConstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46); dmlMatrixConstructionMatrixSource();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DmlMatrixConstructionMatrixSourceContext extends ParserRuleContext {
		public TerminalNode STRING(int i) {
			return getToken(DMLParser.STRING, i);
		}
		public List<TerminalNode> STRING() { return getTokens(DMLParser.STRING); }
		public DmlMatrixConstructionMatrixSourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlMatrixConstructionMatrixSource; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterDmlMatrixConstructionMatrixSource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitDmlMatrixConstructionMatrixSource(this);
		}
	}

	public final DmlMatrixConstructionMatrixSourceContext dmlMatrixConstructionMatrixSource() throws RecognitionException {
		DmlMatrixConstructionMatrixSourceContext _localctx = new DmlMatrixConstructionMatrixSourceContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_dmlMatrixConstructionMatrixSource);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); match(T__18);
			setState(49); match(STRING);
			setState(50); match(T__15);
			setState(51); match(STRING);
			setState(52); match(T__31);
			setState(53); match(STRING);
			setState(54); match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DmlExpressionContext extends ParserRuleContext {
		public DmlExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dmlExpression; }
	 
		public DmlExpressionContext() { }
		public void copyFrom(DmlExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RowsMeansExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowsMeansExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowsMeansExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowsMeansExperssion(this);
		}
	}
	public static class AtomicExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public AtomicExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterAtomicExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitAtomicExpression(this);
		}
	}
	public static class VarExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public VarExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterVarExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitVarExpression(this);
		}
	}
	public static class ColumnsSumExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsSumExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsSumExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsSumExperssion(this);
		}
	}
	public static class CumProdExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public CumProdExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterCumProdExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitCumProdExperssion(this);
		}
	}
	public static class ColumnsVarExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsVarExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsVarExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsVarExperssion(this);
		}
	}
	public static class CumMinExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public CumMinExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterCumMinExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitCumMinExperssion(this);
		}
	}
	public static class MatrixAddExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public MatrixAddExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixAddExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixAddExpression(this);
		}
	}
	public static class RowsMaxsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowsMaxsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowsMaxsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowsMaxsExperssion(this);
		}
	}
	public static class MatrixMulElementwiseExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public MatrixMulElementwiseExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixMulElementwiseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixMulElementwiseExpression(this);
		}
	}
	public static class RowsSumExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowsSumExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowsSumExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowsSumExperssion(this);
		}
	}
	public static class ColumnsMaxsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsMaxsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsMaxsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsMaxsExperssion(this);
		}
	}
	public static class MatrixMulExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public MatrixMulExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixMulExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixMulExpression(this);
		}
	}
	public static class AvgExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public AvgExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterAvgExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitAvgExpression(this);
		}
	}
	public static class MatrixDivisionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public MatrixDivisionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixDivision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixDivision(this);
		}
	}
	public static class RowsVarExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowsVarExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowsVarExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowsVarExperssion(this);
		}
	}
	public static class MeanExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MeanExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMeanExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMeanExpression(this);
		}
	}
	public static class CumSumExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public CumSumExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterCumSumExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitCumSumExperssion(this);
		}
	}
	public static class ColumnsMeansExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsMeansExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsMeansExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsMeansExperssion(this);
		}
	}
	public static class ColumnsSdsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsSdsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsSdsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsSdsExperssion(this);
		}
	}
	public static class MatrixScalarDivisionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public NumericScalarContext right;
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public NumericScalarContext numericScalar() {
			return getRuleContext(NumericScalarContext.class,0);
		}
		public MatrixScalarDivisionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixScalarDivision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixScalarDivision(this);
		}
	}
	public static class MatrixMulScalarExpressionContext extends DmlExpressionContext {
		public NumericScalarContext left;
		public Token op;
		public DmlExpressionContext right;
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public NumericScalarContext numericScalar() {
			return getRuleContext(NumericScalarContext.class,0);
		}
		public MatrixMulScalarExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixMulScalarExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixMulScalarExpression(this);
		}
	}
	public static class BooleanAndExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public BooleanAndExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterBooleanAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitBooleanAndExpression(this);
		}
	}
	public static class CumMaxExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public CumMaxExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterCumMaxExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitCumMaxExperssion(this);
		}
	}
	public static class ColumnsMinsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public ColumnsMinsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterColumnsMinsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitColumnsMinsExperssion(this);
		}
	}
	public static class RowsSdsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowsSdsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowsSdsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowsSdsExperssion(this);
		}
	}
	public static class RowssMinsExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public RowssMinsExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterRowssMinsExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitRowssMinsExperssion(this);
		}
	}
	public static class MatrixIdentifierContext extends DmlExpressionContext {
		public MatrixNameExpressionContext matrixNameExpression() {
			return getRuleContext(MatrixNameExpressionContext.class,0);
		}
		public MatrixIdentifierContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixIdentifier(this);
		}
	}
	public static class SDExperssionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public SDExperssionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterSDExperssion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitSDExperssion(this);
		}
	}
	public static class MatrixInverseExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixInverseExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixInverseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixInverseExpression(this);
		}
	}
	public static class MatrixDiagonalExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixDiagonalExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixDiagonalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixDiagonalExpression(this);
		}
	}
	public static class MatrixTraceExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixTraceExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixTraceExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixTraceExpression(this);
		}
	}
	public static class MatrixTransposeExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixTransposeExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixTransposeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixTransposeExpression(this);
		}
	}
	public static class MatrixPowerExpressionContext extends DmlExpressionContext {
		public Token op;
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixPowerExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixPowerExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixPowerExpression(this);
		}
	}
	public static class MatrixDeterExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext dmlExpression() {
			return getRuleContext(DmlExpressionContext.class,0);
		}
		public MatrixDeterExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixDeterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixDeterExpression(this);
		}
	}
	public static class MatrixSubExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public MatrixSubExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixSubExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixSubExpression(this);
		}
	}
	public static class BooleanOrExpressionContext extends DmlExpressionContext {
		public DmlExpressionContext left;
		public Token op;
		public DmlExpressionContext right;
		public List<DmlExpressionContext> dmlExpression() {
			return getRuleContexts(DmlExpressionContext.class);
		}
		public DmlExpressionContext dmlExpression(int i) {
			return getRuleContext(DmlExpressionContext.class,i);
		}
		public BooleanOrExpressionContext(DmlExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterBooleanOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitBooleanOrExpression(this);
		}
	}

	public final DmlExpressionContext dmlExpression() throws RecognitionException {
		return dmlExpression(0);
	}

	private DmlExpressionContext dmlExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		DmlExpressionContext _localctx = new DmlExpressionContext(_ctx, _parentState);
		DmlExpressionContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_dmlExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(166);
			switch (_input.LA(1)) {
			case INT:
				{
				_localctx = new MatrixMulScalarExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(57); ((MatrixMulScalarExpressionContext)_localctx).left = numericScalar();
				setState(58); ((MatrixMulScalarExpressionContext)_localctx).op = match(T__19);
				setState(59); ((MatrixMulScalarExpressionContext)_localctx).right = dmlExpression(34);
				}
				break;
			case ID:
				{
				_localctx = new MatrixIdentifierContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(61); matrixNameExpression();
				}
				break;
			case T__43:
				{
				_localctx = new MatrixTransposeExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(62); match(T__43);
				setState(63); dmlExpression(0);
				setState(64); match(T__5);
				}
				break;
			case T__16:
				{
				_localctx = new MatrixInverseExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(66); match(T__16);
				setState(67); dmlExpression(0);
				setState(68); match(T__5);
				}
				break;
			case T__34:
				{
				_localctx = new MatrixDeterExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(70); match(T__34);
				setState(71); dmlExpression(0);
				setState(72); match(T__5);
				}
				break;
			case T__10:
				{
				_localctx = new MatrixTraceExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(74); match(T__10);
				setState(75); dmlExpression(0);
				setState(76); match(T__5);
				}
				break;
			case T__40:
				{
				_localctx = new MatrixDiagonalExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(78); match(T__40);
				setState(79); dmlExpression(0);
				setState(80); match(T__5);
				}
				break;
			case T__20:
				{
				_localctx = new AtomicExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(82); match(T__20);
				setState(83); dmlExpression(0);
				setState(84); match(T__5);
				}
				break;
			case T__38:
				{
				_localctx = new MeanExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(86); match(T__38);
				setState(87); dmlExpression(0);
				setState(88); match(T__5);
				}
				break;
			case T__8:
				{
				_localctx = new AvgExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(90); match(T__8);
				setState(91); dmlExpression(0);
				setState(92); match(T__5);
				}
				break;
			case T__21:
				{
				_localctx = new VarExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(94); match(T__21);
				setState(95); dmlExpression(0);
				setState(96); match(T__5);
				}
				break;
			case T__24:
				{
				_localctx = new SDExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(98); match(T__24);
				setState(99); dmlExpression(0);
				setState(100); match(T__5);
				}
				break;
			case T__7:
				{
				_localctx = new ColumnsSumExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(102); match(T__7);
				setState(103); dmlExpression(0);
				setState(104); match(T__5);
				}
				break;
			case T__13:
				{
				_localctx = new ColumnsMeansExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(106); match(T__13);
				setState(107); dmlExpression(0);
				setState(108); match(T__5);
				}
				break;
			case T__33:
				{
				_localctx = new ColumnsVarExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(110); match(T__33);
				setState(111); dmlExpression(0);
				setState(112); match(T__5);
				}
				break;
			case T__23:
				{
				_localctx = new ColumnsSdsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(114); match(T__23);
				setState(115); dmlExpression(0);
				setState(116); match(T__5);
				}
				break;
			case T__37:
				{
				_localctx = new ColumnsMaxsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(118); match(T__37);
				setState(119); dmlExpression(0);
				setState(120); match(T__5);
				}
				break;
			case T__29:
				{
				_localctx = new ColumnsMinsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(122); match(T__29);
				setState(123); dmlExpression(0);
				setState(124); match(T__5);
				}
				break;
			case T__25:
				{
				_localctx = new RowsSumExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(126); match(T__25);
				setState(127); dmlExpression(0);
				setState(128); match(T__5);
				}
				break;
			case T__41:
				{
				_localctx = new RowsMeansExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(130); match(T__41);
				setState(131); dmlExpression(0);
				setState(132); match(T__5);
				}
				break;
			case T__0:
				{
				_localctx = new RowsVarExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(134); match(T__0);
				setState(135); dmlExpression(0);
				setState(136); match(T__5);
				}
				break;
			case T__35:
				{
				_localctx = new RowsSdsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(138); match(T__35);
				setState(139); dmlExpression(0);
				setState(140); match(T__5);
				}
				break;
			case T__4:
				{
				_localctx = new RowsMaxsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(142); match(T__4);
				setState(143); dmlExpression(0);
				setState(144); match(T__5);
				}
				break;
			case T__17:
				{
				_localctx = new RowssMinsExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(146); match(T__17);
				setState(147); dmlExpression(0);
				setState(148); match(T__5);
				}
				break;
			case T__11:
				{
				_localctx = new CumSumExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(150); match(T__11);
				setState(151); dmlExpression(0);
				setState(152); match(T__5);
				}
				break;
			case T__39:
				{
				_localctx = new CumProdExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(154); match(T__39);
				setState(155); dmlExpression(0);
				setState(156); match(T__5);
				}
				break;
			case T__36:
				{
				_localctx = new CumMinExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(158); match(T__36);
				setState(159); dmlExpression(0);
				setState(160); match(T__5);
				}
				break;
			case T__6:
				{
				_localctx = new CumMaxExperssionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(162); match(T__6);
				setState(163); dmlExpression(0);
				setState(164); match(T__5);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(196);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(194);
					switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
					case 1:
						{
						_localctx = new MatrixMulExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixMulExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(168);
						if (!(precpred(_ctx, 36))) throw new FailedPredicateException(this, "precpred(_ctx, 36)");
						setState(169); ((MatrixMulExpressionContext)_localctx).op = match(T__14);
						setState(170); ((MatrixMulExpressionContext)_localctx).right = dmlExpression(37);
						}
						break;
					case 2:
						{
						_localctx = new MatrixMulElementwiseExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixMulElementwiseExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(171);
						if (!(precpred(_ctx, 35))) throw new FailedPredicateException(this, "precpred(_ctx, 35)");
						setState(172); ((MatrixMulElementwiseExpressionContext)_localctx).op = match(T__19);
						setState(173); ((MatrixMulElementwiseExpressionContext)_localctx).right = dmlExpression(36);
						}
						break;
					case 3:
						{
						_localctx = new MatrixAddExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixAddExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(174);
						if (!(precpred(_ctx, 33))) throw new FailedPredicateException(this, "precpred(_ctx, 33)");
						setState(175); ((MatrixAddExpressionContext)_localctx).op = match(T__2);
						setState(176); ((MatrixAddExpressionContext)_localctx).right = dmlExpression(34);
						}
						break;
					case 4:
						{
						_localctx = new MatrixDivisionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixDivisionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(177);
						if (!(precpred(_ctx, 32))) throw new FailedPredicateException(this, "precpred(_ctx, 32)");
						setState(178); ((MatrixDivisionContext)_localctx).op = match(T__3);
						setState(179); ((MatrixDivisionContext)_localctx).right = dmlExpression(33);
						}
						break;
					case 5:
						{
						_localctx = new MatrixSubExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixSubExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(180);
						if (!(precpred(_ctx, 30))) throw new FailedPredicateException(this, "precpred(_ctx, 30)");
						setState(181); ((MatrixSubExpressionContext)_localctx).op = match(T__1);
						setState(182); ((MatrixSubExpressionContext)_localctx).right = dmlExpression(31);
						}
						break;
					case 6:
						{
						_localctx = new BooleanAndExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((BooleanAndExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(183);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(184);
						((BooleanAndExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__28 || _la==T__22) ) {
							((BooleanAndExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(185); ((BooleanAndExpressionContext)_localctx).right = dmlExpression(23);
						}
						break;
					case 7:
						{
						_localctx = new BooleanOrExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						((BooleanOrExpressionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(186);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(187);
						((BooleanOrExpressionContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==T__32 || _la==T__9) ) {
							((BooleanOrExpressionContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(188); ((BooleanOrExpressionContext)_localctx).right = dmlExpression(22);
						}
						break;
					case 8:
						{
						_localctx = new MatrixScalarDivisionContext(new DmlExpressionContext(_parentctx, _parentState));
						((MatrixScalarDivisionContext)_localctx).left = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(189);
						if (!(precpred(_ctx, 31))) throw new FailedPredicateException(this, "precpred(_ctx, 31)");
						setState(190); ((MatrixScalarDivisionContext)_localctx).op = match(T__42);
						setState(191); ((MatrixScalarDivisionContext)_localctx).right = numericScalar();
						}
						break;
					case 9:
						{
						_localctx = new MatrixPowerExpressionContext(new DmlExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_dmlExpression);
						setState(192);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(193); ((MatrixPowerExpressionContext)_localctx).op = match(T__26);
						}
						break;
					}
					} 
				}
				setState(198);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ViewNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(DMLParser.ID, 0); }
		public ViewNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_viewName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterViewName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitViewName(this);
		}
	}

	public final ViewNameContext viewName() throws RecognitionException {
		ViewNameContext _localctx = new ViewNameContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_viewName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(199); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(DMLParser.ID, 0); }
		public MatrixNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixName(this);
		}
	}

	public final MatrixNameContext matrixName() throws RecognitionException {
		MatrixNameContext _localctx = new MatrixNameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_matrixName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixNameExpressionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(DMLParser.ID, 0); }
		public MatrixNameExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixNameExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterMatrixNameExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitMatrixNameExpression(this);
		}
	}

	public final MatrixNameExpressionContext matrixNameExpression() throws RecognitionException {
		MatrixNameExpressionContext _localctx = new MatrixNameExpressionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_matrixNameExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericScalarContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(DMLParser.INT, 0); }
		public NumericScalarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericScalar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterNumericScalar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitNumericScalar(this);
		}
	}

	public final NumericScalarContext numericScalar() throws RecognitionException {
		NumericScalarContext _localctx = new NumericScalarContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_numericScalar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(205); match(INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilePathContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(DMLParser.STRING, 0); }
		public FilePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).enterFilePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DMLListener ) ((DMLListener)listener).exitFilePath(this);
		}
	}

	public final FilePathContext filePath() throws RecognitionException {
		FilePathContext _localctx = new FilePathContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_filePath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(207); match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 6: return dmlExpression_sempred((DmlExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean dmlExpression_sempred(DmlExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 36);
		case 1: return precpred(_ctx, 35);
		case 2: return precpred(_ctx, 33);
		case 3: return precpred(_ctx, 32);
		case 4: return precpred(_ctx, 30);
		case 5: return precpred(_ctx, 22);
		case 6: return precpred(_ctx, 21);
		case 7: return precpred(_ctx, 31);
		case 8: return precpred(_ctx, 24);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\66\u00d4\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3$\n\3\f\3"+
		"\16\3\'\13\3\3\4\3\4\3\4\3\4\3\5\3\5\5\5/\n\5\3\6\3\6\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00a9\n\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\7\b\u00c5\n\b\f\b\16\b\u00c8\13\b\3\t\3\t\3\n\3\n\3\13"+
		"\3\13\3\f\3\f\3\r\3\r\3\r\2\3\16\16\2\4\6\b\n\f\16\20\22\24\26\30\2\4"+
		"\4\2\22\22\30\30\4\2\16\16%%\u00ed\2\32\3\2\2\2\4\36\3\2\2\2\6(\3\2\2"+
		"\2\b.\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2\16\u00a8\3\2\2\2\20\u00c9\3\2"+
		"\2\2\22\u00cb\3\2\2\2\24\u00cd\3\2\2\2\26\u00cf\3\2\2\2\30\u00d1\3\2\2"+
		"\2\32\33\5\20\t\2\33\34\7\"\2\2\34\35\5\4\3\2\35\3\3\2\2\2\36\37\5\6\4"+
		"\2\37%\7\20\2\2 !\5\6\4\2!\"\7\20\2\2\"$\3\2\2\2# \3\2\2\2$\'\3\2\2\2"+
		"%#\3\2\2\2%&\3\2\2\2&\5\3\2\2\2\'%\3\2\2\2()\5\22\n\2)*\7\23\2\2*+\5\b"+
		"\5\2+\7\3\2\2\2,/\5\n\6\2-/\5\16\b\2.,\3\2\2\2.-\3\2\2\2/\t\3\2\2\2\60"+
		"\61\5\f\7\2\61\13\3\2\2\2\62\63\7\34\2\2\63\64\7\61\2\2\64\65\7\37\2\2"+
		"\65\66\7\61\2\2\66\67\7\17\2\2\678\7\61\2\289\7)\2\29\r\3\2\2\2:;\b\b"+
		"\1\2;<\5\26\f\2<=\7\33\2\2=>\5\16\b$>\u00a9\3\2\2\2?\u00a9\5\24\13\2@"+
		"A\7\3\2\2AB\5\16\b\2BC\7)\2\2C\u00a9\3\2\2\2DE\7\36\2\2EF\5\16\b\2FG\7"+
		")\2\2G\u00a9\3\2\2\2HI\7\f\2\2IJ\5\16\b\2JK\7)\2\2K\u00a9\3\2\2\2LM\7"+
		"$\2\2MN\5\16\b\2NO\7)\2\2O\u00a9\3\2\2\2PQ\7\6\2\2QR\5\16\b\2RS\7)\2\2"+
		"S\u00a9\3\2\2\2TU\7\32\2\2UV\5\16\b\2VW\7)\2\2W\u00a9\3\2\2\2XY\7\b\2"+
		"\2YZ\5\16\b\2Z[\7)\2\2[\u00a9\3\2\2\2\\]\7&\2\2]^\5\16\b\2^_\7)\2\2_\u00a9"+
		"\3\2\2\2`a\7\31\2\2ab\5\16\b\2bc\7)\2\2c\u00a9\3\2\2\2de\7\26\2\2ef\5"+
		"\16\b\2fg\7)\2\2g\u00a9\3\2\2\2hi\7\'\2\2ij\5\16\b\2jk\7)\2\2k\u00a9\3"+
		"\2\2\2lm\7!\2\2mn\5\16\b\2no\7)\2\2o\u00a9\3\2\2\2pq\7\r\2\2qr\5\16\b"+
		"\2rs\7)\2\2s\u00a9\3\2\2\2tu\7\27\2\2uv\5\16\b\2vw\7)\2\2w\u00a9\3\2\2"+
		"\2xy\7\t\2\2yz\5\16\b\2z{\7)\2\2{\u00a9\3\2\2\2|}\7\21\2\2}~\5\16\b\2"+
		"~\177\7)\2\2\177\u00a9\3\2\2\2\u0080\u0081\7\25\2\2\u0081\u0082\5\16\b"+
		"\2\u0082\u0083\7)\2\2\u0083\u00a9\3\2\2\2\u0084\u0085\7\5\2\2\u0085\u0086"+
		"\5\16\b\2\u0086\u0087\7)\2\2\u0087\u00a9\3\2\2\2\u0088\u0089\7.\2\2\u0089"+
		"\u008a\5\16\b\2\u008a\u008b\7)\2\2\u008b\u00a9\3\2\2\2\u008c\u008d\7\13"+
		"\2\2\u008d\u008e\5\16\b\2\u008e\u008f\7)\2\2\u008f\u00a9\3\2\2\2\u0090"+
		"\u0091\7*\2\2\u0091\u0092\5\16\b\2\u0092\u0093\7)\2\2\u0093\u00a9\3\2"+
		"\2\2\u0094\u0095\7\35\2\2\u0095\u0096\5\16\b\2\u0096\u0097\7)\2\2\u0097"+
		"\u00a9\3\2\2\2\u0098\u0099\7#\2\2\u0099\u009a\5\16\b\2\u009a\u009b\7)"+
		"\2\2\u009b\u00a9\3\2\2\2\u009c\u009d\7\7\2\2\u009d\u009e\5\16\b\2\u009e"+
		"\u009f\7)\2\2\u009f\u00a9\3\2\2\2\u00a0\u00a1\7\n\2\2\u00a1\u00a2\5\16"+
		"\b\2\u00a2\u00a3\7)\2\2\u00a3\u00a9\3\2\2\2\u00a4\u00a5\7(\2\2\u00a5\u00a6"+
		"\5\16\b\2\u00a6\u00a7\7)\2\2\u00a7\u00a9\3\2\2\2\u00a8:\3\2\2\2\u00a8"+
		"?\3\2\2\2\u00a8@\3\2\2\2\u00a8D\3\2\2\2\u00a8H\3\2\2\2\u00a8L\3\2\2\2"+
		"\u00a8P\3\2\2\2\u00a8T\3\2\2\2\u00a8X\3\2\2\2\u00a8\\\3\2\2\2\u00a8`\3"+
		"\2\2\2\u00a8d\3\2\2\2\u00a8h\3\2\2\2\u00a8l\3\2\2\2\u00a8p\3\2\2\2\u00a8"+
		"t\3\2\2\2\u00a8x\3\2\2\2\u00a8|\3\2\2\2\u00a8\u0080\3\2\2\2\u00a8\u0084"+
		"\3\2\2\2\u00a8\u0088\3\2\2\2\u00a8\u008c\3\2\2\2\u00a8\u0090\3\2\2\2\u00a8"+
		"\u0094\3\2\2\2\u00a8\u0098\3\2\2\2\u00a8\u009c\3\2\2\2\u00a8\u00a0\3\2"+
		"\2\2\u00a8\u00a4\3\2\2\2\u00a9\u00c6\3\2\2\2\u00aa\u00ab\f&\2\2\u00ab"+
		"\u00ac\7 \2\2\u00ac\u00c5\5\16\b\'\u00ad\u00ae\f%\2\2\u00ae\u00af\7\33"+
		"\2\2\u00af\u00c5\5\16\b&\u00b0\u00b1\f#\2\2\u00b1\u00b2\7,\2\2\u00b2\u00c5"+
		"\5\16\b$\u00b3\u00b4\f\"\2\2\u00b4\u00b5\7+\2\2\u00b5\u00c5\5\16\b#\u00b6"+
		"\u00b7\f \2\2\u00b7\u00b8\7-\2\2\u00b8\u00c5\5\16\b!\u00b9\u00ba\f\30"+
		"\2\2\u00ba\u00bb\t\2\2\2\u00bb\u00c5\5\16\b\31\u00bc\u00bd\f\27\2\2\u00bd"+
		"\u00be\t\3\2\2\u00be\u00c5\5\16\b\30\u00bf\u00c0\f!\2\2\u00c0\u00c1\7"+
		"\4\2\2\u00c1\u00c5\5\26\f\2\u00c2\u00c3\f\32\2\2\u00c3\u00c5\7\24\2\2"+
		"\u00c4\u00aa\3\2\2\2\u00c4\u00ad\3\2\2\2\u00c4\u00b0\3\2\2\2\u00c4\u00b3"+
		"\3\2\2\2\u00c4\u00b6\3\2\2\2\u00c4\u00b9\3\2\2\2\u00c4\u00bc\3\2\2\2\u00c4"+
		"\u00bf\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5\u00c8\3\2\2\2\u00c6\u00c4\3\2"+
		"\2\2\u00c6\u00c7\3\2\2\2\u00c7\17\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00ca"+
		"\7/\2\2\u00ca\21\3\2\2\2\u00cb\u00cc\7/\2\2\u00cc\23\3\2\2\2\u00cd\u00ce"+
		"\7/\2\2\u00ce\25\3\2\2\2\u00cf\u00d0\7\62\2\2\u00d0\27\3\2\2\2\u00d1\u00d2"+
		"\7\61\2\2\u00d2\31\3\2\2\2\7%.\u00a8\u00c4\u00c6";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}