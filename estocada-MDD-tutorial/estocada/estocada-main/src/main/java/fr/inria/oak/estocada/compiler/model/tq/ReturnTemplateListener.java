package fr.inria.oak.estocada.compiler.model.tq;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * @author Rana Alotaibi
 */
@Singleton
final class ReturnTemplateListener extends TQLBaseListener {
    private static final Logger log = Logger.getLogger(ReturnTemplateListener.class);

    /* Used to map aqlquery variables and the internal fresh variables */
    private final VariableMapper variableMapper;

    /* Used to keep the terms of the return template being parsed */
    private List<ReturnTerm> terms;

    private ReturnTemplate constructedReturnTemplate;

    @Inject
    public ReturnTemplateListener(final VariableMapper variableMapper) {
        this.variableMapper = checkNotNull(variableMapper);
    }

    public ReturnTemplate parse(final String str) throws ParseException {
        terms = new ArrayList<ReturnTerm>();

        final TQLLexer lexer = new TQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TQLParser parser = new TQLParser(tokens);
        final ParserRuleContext tree = parser.selectClause();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        constructedReturnTemplate = new ReturnTemplate(TQModel.ID, terms);
        return constructedReturnTemplate;
    }

    @Override
    public void enterReturnVariable(TQLParser.ReturnVariableContext ctx) {
        log.debug("Entering ReturnVar: " + ctx.getText());
        final Variable var;
        var = variableMapper.getVariable(ctx.getText());
        final Map<String, String> optionals = new HashMap<String, String>();
        terms.add((new ReturnVariableTerm(var, optionals)));

    }

    public ReturnTemplate getConstructedReturnTemplate() {
        return constructedReturnTemplate;
    }
}
