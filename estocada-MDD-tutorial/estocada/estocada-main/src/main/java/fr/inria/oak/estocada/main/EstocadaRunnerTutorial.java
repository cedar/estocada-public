package fr.inria.oak.estocada.main;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.main.scenarios.TatooineScenariosMIMICTutorial;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriting.logical.RewritingToLogicalPlanGenerator;
import fr.inria.oak.estocada.rewriting.physical.LogicalToNaivePhysicalPlanGenerator;
import fr.inria.oak.estocada.utils.RunnerUtils;

/**
 * This class is used to run an end-to-end ESTOCADA scenario on Tatooine.
 *
 * @author ralotaibi
 *
 */

public class EstocadaRunnerTutorial {
    private static final Logger LOGGER = Logger.getLogger(EstocadaRunnerTutorial.class);
    private final String inputQuerysAndViewsFolder;
    private List<String> subQueries;

    public EstocadaRunnerTutorial(final String inputQuerysAndViewsFolder) {
        Logger.getRootLogger().setLevel(Level.INFO);
        subQueries = new ArrayList<>();
        this.inputQuerysAndViewsFolder = checkNotNull(inputQuerysAndViewsFolder);
        try {
            Parameters.init();
            RegisterMetaDataManualTutorial.registerMetaData();
        } catch (Exception e) {
            LOGGER.error("Could not instantiates the execution engine (Tatooine): " + e.getMessage());
        }

    }

    /**
     * Run an end-to-end ESTOCADA scenario rewriting on top of Tatooine.
     * 
     * @throws Exception
     */
    public void runTattoineSMQCMV() throws Exception {

        final File folder = new File(inputQuerysAndViewsFolder);
        final String qbtQuery = RunnerUtils.readQuery(folder.getAbsolutePath());
        final List<String> views = RunnerUtils.readViews(folder.getAbsolutePath());
        final List<ConjunctiveQuery> rewritings = findRewritingTutorial(qbtQuery, views, folder.getAbsolutePath());
        System.out.println("\n****** CQ Rewritings ******");
        System.out.println(rewritings.get(0));
        if (!rewritings.isEmpty()) {
            //Logical
            final RewritingToLogicalPlanGenerator rewritingToLogicalPlanGenerator =
                    new RewritingToLogicalPlanGenerator(rewritings.get(0));
            final LogOperator logicalPlanRootOperator = rewritingToLogicalPlanGenerator.generate();
            final List<Map.Entry<String, String>> projectedColumns =
                    rewritingToLogicalPlanGenerator.getColumnSchemaMapping();

            //Phy
            final LogicalToNaivePhysicalPlanGenerator PhyOp =
                    new LogicalToNaivePhysicalPlanGenerator(logicalPlanRootOperator);

            final NIterator rewritingPhyPlan = PhyOp.generate();
            subQueries = PhyOp.getSubQueries();

            System.out.println("\n****** Execute Query & Export Results ******");
            TatooineScenariosMIMICTutorial.QueryEval(folder.getAbsolutePath() + "/");

            System.out.println("\n****** Execute Rewriting & Export Results******");
            rewritingPhyPlan.open();
            int count = 0;
            PrintWriter pw = new PrintWriter(new FileWriter(folder.getAbsolutePath() + "/RewritingResults.csv"));
            while (rewritingPhyPlan.hasNext()) {
                final NTuple ntuple = rewritingPhyPlan.next();
                List<String> names = Arrays.asList(ntuple.nrsmd.getColNames());
                for (int i = 0; i < projectedColumns.size(); i++) {
                    Object colValue = null;

                    if (!names.contains((projectedColumns.get(i).getValue()))) {
                        colValue = ntuple.getValue(projectedColumns.get(i).getKey());
                    } else {
                        colValue = ntuple.getValue(projectedColumns.get(i).getValue());
                    }
                    if (colValue instanceof char[]) {
                        String val = String.valueOf((char[]) (colValue)).trim();
                        pw.write(val);
                    }
                    if (colValue instanceof Integer) {
                        int val = (Integer) colValue;
                        pw.write(String.valueOf(val));
                    }

                    if (i != (projectedColumns.size() - 1))
                        pw.write(",");
                }
                pw.write("\n");
                count++;
            }
            rewritingPhyPlan.close();
            pw.close();
            System.out.println("--Rewriting Result Count=" + count);
            System.out.println("--Rewriting Result Exported To RewritingResults.csv");
            System.out.println("--Draw Rewriting Plan");
        }
    }

    /**
     * Process QBT scenario
     * 
     * @param qbtQuery
     *            qbt query
     * @param qbtViews
     *            qbt views
     * @return found rewritings
     * @throws Exception
     */
    private List<ConjunctiveQuery> findRewriting(final String qbtQuery, final List<String> qbtViews) throws Exception {

        //********************
        //* Encode Query
        //********************
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        final ConjunctiveQuery query =
                Utils.parseQuery(RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(qbtQuery)));
        System.out.println("\n****** Encoded CQ Query *****");
        System.out.println(query);

        //********************
        //* Encode Views
        //********************
        final List<QueryBlockTree> views = new ArrayList<QueryBlockTree>();
        final QBTNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
        final StringBuilder fwStrBuilder = new StringBuilder();
        final StringBuilder gsStrBuilder = new StringBuilder();
        for (final String view : qbtViews) {
            try {
                QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
                mixedBuilder.setRealtions(null);
                QueryBlockTree nbt = mixedBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(view));
                views.add(nbt);
                final Context context = compiler0.compileContext(nbt,
                        new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), true);

                context.getForwardConstraints().stream().forEach(r -> fwStrBuilder.append(r.toString()).append("\n"));
                context.getGlobalSchema().getRelations().stream()
                        .forEach(r -> gsStrBuilder.append(r.toString()).append("\n"));
                QBTQueryBlockTreeBuilder.reIntilize();
            } catch (fr.inria.oak.estocada.compiler.exceptions.ParseException e) {
                throw new RuntimeException(e);
            }
        }
        //********************
        //* Find rewriting
        //********************
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
        }

        return reformulations;
    }

    /**
     * Process QBT scenario
     * 
     * @param qbtQuery
     *            qbt query
     * @param qbtViews
     *            qbt views
     * @return found rewritings
     * @throws Exception
     */
    private List<ConjunctiveQuery> findRewritingTutorial(final String qbtQuery, final List<String> qbtViews,
            final String examplePath) throws Exception {

        //********************
        //* Encode Query
        //********************
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        final ConjunctiveQuery query =
                Utils.parseQuery(RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(qbtQuery)));
        System.out.println("\n****** Encoded CQ Query *****");
        System.out.println(query);

        //********************
        //* Encode Views
        //********************
        final List<QueryBlockTree> views = new ArrayList<QueryBlockTree>();
        final QBTNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
        final List<Constraint> fws = new ArrayList<>();
        final List<Constraint> bws = new ArrayList<>();
        final List<RelationalSchema> gs = new ArrayList<>();
        final List<RelationalSchema> ts = new ArrayList<>();
        for (final String view : qbtViews) {
            try {
                QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
                mixedBuilder.setRealtions(null);
                QueryBlockTree nbt = mixedBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(view));
                views.add(nbt);
                final Context context = compiler0.compileContext(nbt,
                        new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), true);

                fws.addAll(context.getForwardConstraints());
                bws.addAll(context.getBackwardConstraints());

                gs.add(context.getGlobalSchema());
                ts.add(context.getTargetSchema());

                QBTQueryBlockTreeBuilder.reIntilize();

            } catch (fr.inria.oak.estocada.compiler.exceptions.ParseException e) {
                throw new RuntimeException(e);
            }
        }

        fr.inria.oak.estocada.compiler.Utils.writeConstraints(Paths.get(examplePath + "/constraints_chase").toString(),
                fws);
        fr.inria.oak.estocada.compiler.Utils
                .writeConstraints(Paths.get(examplePath + "/constraints_bkchase").toString(), bws);
        //********************
        //* Find rewriting
        //********************
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
        }

        return reformulations;
    }

    private List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
        return compilers;
    }

    private List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

    public List<String> getSubQueires() {
        return subQueries;
    }

}