package fr.inria.oak.estocada.compiler.model.xq.naive;

import java.util.List;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnLeafTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.model.xq.Predicate;
import fr.inria.oak.estocada.compiler.model.xq.Utils;

@Singleton
public class XQRootBlockForwardEncoderReturnTermVisitor extends XQBaseForwardEncoderReturnTermVisitor {
	private final VariableFactory	elementVariableFactory;
	private Term					rootElementCreatedNode;

	@Inject
	public XQRootBlockForwardEncoderReturnTermVisitor(
	        @Named("ElementVariableFactory") final VariableFactory elementVariableFactory) {
		super();
		this.elementVariableFactory = elementVariableFactory;
	}

	public List<Atom> encode(final ReturnTemplate template, final String viewName) {
		builder = ImmutableList.builder();
		this.viewName = viewName;
		template.accept(this);
		return builder.build();
	}

	@Override
	public void visitPre(final ReturnConstructTerm term) {
		if (builder.build().isEmpty()) {
			buildRootElement(term.getCreatedNode());
			builder.add(new Atom(Predicate.TAG.toString() + "_" + viewName, term.getCreatedNode(),
			        term.getElement().toTerm()));
			for (final Entry<String, ReturnLeafTerm> attr : term.getAttributes().entrySet()) {
				builder.add(new Atom(Predicate.HAS_ATTRIBUTE.toString() + "_" + viewName, term.getCreatedNode(),
				        Utils.toTerm(attr.getKey())));
			}
		} else {
			super.visitPre(term);
		}
	}

	@Override
	public void visit(final ReturnVariableTerm term) {
		if (builder.build().isEmpty()) {
			if (Utils.isText(term)) {
				final Variable var = elementVariableFactory.createFreshVar();
				buildRootElement(var);
				builder.add(new Atom(Predicate.TEXT.toString() + "_" + viewName, var, term.getCreatedNode()));
			} else {
				buildRootElement(term.getCreatedNode());
			}
		} else {
			super.visit(term);
		}
	}

	@Override
	public void visit(final ReturnStringTerm term) {
		if (builder.build().isEmpty()) {
			final Variable var = elementVariableFactory.createFreshVar();
			buildRootElement(var);
			builder.add(new Atom(Predicate.TEXT.toString() + "_" + viewName, var, term.toTerm()));
		} else {
			super.visit(term);
		}
	}

	@Override
	protected Term getParentBlockCreatedNode(final ReturnTerm term) {
		return term.hasParent() ? term.getParent().getCreatedNode() : rootElementCreatedNode;
	}

	private void buildRootElement(final Term var) {
		rootElementCreatedNode = var;
		builder.add(new Atom(viewName, rootElementCreatedNode));
	}
}