package fr.inria.oak.estocada.compiler.model.pr;

public class PRNaiveModule extends PRModule {
    @Override
    protected String getPropertiesFileName() {
        return "sql.properties";
    }
}
