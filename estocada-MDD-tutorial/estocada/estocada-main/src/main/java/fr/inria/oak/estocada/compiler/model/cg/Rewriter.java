package fr.inria.oak.estocada.compiler.model.cg;

import static fr.inria.oak.estocada.compiler.model.cg.Utils.readFile;
import static fr.inria.oak.estocada.compiler.model.cg.Utils.writeFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.TimedReformulations;

public class Rewriter {

    public String queryOrFileString;
    public boolean isQuery;
    public List<String> viewFileNames;
    public String rewriteFolderPath;
    public String templatesPath;

    public String queryEncoding;
    public List<Map<String, String>> viewsEncoding;
    private List<ConjunctiveQuery> rws;
    private List<ConjunctiveQuery> appliedFW;


    public Rewriter(String queryOrFileString, List<String> viewFileNames, String rewriteFolderPath,
            String templatesPath, boolean isQuery) {
        this.queryOrFileString = queryOrFileString;
        this.isQuery = isQuery;
        this.viewFileNames = viewFileNames;
        this.rewriteFolderPath = rewriteFolderPath;
        this.templatesPath = templatesPath;
        this.queryEncoding = null;
        this.viewsEncoding = null;
        this.rws = new ArrayList<>();
    }

    public Rewriter(String examplePath, boolean isQuery) {
        this.queryOrFileString = examplePath + "Query";
        this.viewFileNames = Arrays.asList(examplePath + "View.v");
        this.isQuery = isQuery;
        this.rewriteFolderPath = examplePath;
        Path ab = Paths.get("src/main/resources/graph/templates/ldbc/").toAbsolutePath();
        this.templatesPath = ab.toString() + "/";
        this.queryEncoding = null;
        this.viewsEncoding = null;
        this.rws = new ArrayList<>();
    }

    public List<ConjunctiveQuery> rewrite() throws Exception {
        encode();
        rewriteCQ();
        decode();
        return rws;
    }

    //    public List<String> rewrite() throws Exception {
    //        encode();
    //        rewriteCQ();
    //        return decode();
    //    }

    public void encode() {
        Encoder encoder = new Encoder(queryOrFileString, viewFileNames, isQuery);
        queryEncoding = encoder.encodeQuery();
        viewsEncoding = encoder.encodeView();
        //System.out.println(queryEncoding);
        for (int i = 0; i < viewsEncoding.size(); i++) {
            //System.out.println("[View " + (i + 1) + "]");
            Map<String, String> viewEncoding = viewsEncoding.get(i);
            //            for (String s : viewEncoding.keySet()) {
            //                System.out.println(s + "\t" + viewEncoding.get(s));
            //            }
        }
    }

    public void rewriteCQ() throws Exception {
        // folder
        File file = new File(rewriteFolderPath);
        if (!file.exists()) {
            file.mkdirs();
        }

        // "view"
        file = new File(rewriteFolderPath + "views");
        if (!file.exists()) {
            file.createNewFile();
        }

        // "description"
        file = new File(rewriteFolderPath + "description");
        if (!file.exists()) {
            file.createNewFile();
        }

        // "schemas"
        file = new File(rewriteFolderPath + "schemas");
        if (!file.exists()) {
            file.createNewFile();
        }
        String content = readFile(templatesPath + "schemas", true);
        writeFile(file, false, content);

        // "query"
        file = new File(rewriteFolderPath + "cqQuery");
        if (!file.exists()) {
            file.createNewFile();
        }
        writeFile(file, false, queryEncoding);

        // "constraints_chase"
        file = new File(rewriteFolderPath + "constraints_chase");
        if (!file.exists()) {
            file.createNewFile();
        }
        content = readFile(templatesPath + "constraints_chase", true);
        for (int i = 0; i < viewsEncoding.size(); i++) {
            content += "\n# View " + (i + 1) + "\n";
            Map<String, String> viewEncoding = viewsEncoding.get(i);
            content += viewEncoding.get("PEC") + viewEncoding.get("chase");
        }
        writeFile(file, false, content);

        // "constraints_bkchase"
        file = new File(rewriteFolderPath + "constraints_bkchase");
        if (!file.exists()) {
            file.createNewFile();
        }
        content = readFile(templatesPath + "constraints_bkchase", true);
        for (int i = 0; i < viewsEncoding.size(); i++) {
            content += "\n# View " + (i + 1) + "\n";
            Map<String, String> viewEncoding = viewsEncoding.get(i);
            content += viewEncoding.get("PEC") + viewEncoding.get("bkchase");
        }
        writeFile(file, false, content);

        final Context context01 =
                fr.inria.oak.estocada.rewriter.server.Utils.parseContext(rewriteFolderPath + "schemas",
                        rewriteFolderPath + "constraints_chase", rewriteFolderPath + "constraints_bkchase");
        final ConjunctiveQueryRewriter rewriter = new PACBConjunctiveQueryRewriter(context01);
        final BufferedReader query = new BufferedReader(new FileReader(new File(rewriteFolderPath + "cqQuery")));
        final TimedReformulations timedRewritings = rewriter.getTimedReformulations(
                fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query)));
        rws = timedRewritings.getRewritings();
        

    }

    public  void visualizeConstraint (final String examplePath, final String flag) {
    	if(flag.equals("y")) {
		    String givenFileName = "constraints" + System.currentTimeMillis();
		    String fileNameDot = new String(givenFileName + ".dot");
			String fileNamePS = new String(givenFileName+ ".png");

    		StringBuffer sb = new StringBuffer();
    		sb.append("digraph { ");
    		for (int i=0;i<appliedFW.size();i++) {
    			sb.append("\""+appliedFW.get(i).toString().replaceAll("\"", "\\\\\"")+"\"");
    			if(i!=(appliedFW.size()-1)) {
    				sb.append("->");
    			}else {
    				sb.append("}");
    			}
    			
    		}
    		FileWriter file;
			try {
				file = new FileWriter(fileNameDot);
				file.write(new String(sb));;
				file.close();
				Runtime r = Runtime.getRuntime();
				String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePS);
				Process p = r.exec(com);
				p.waitFor();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
    	}
    	
    }
    public String encodedQuery() {
        return queryEncoding;
    }

    public List<String> decode() {
        List<String> cypherQueries = new ArrayList<>();
        for (ConjunctiveQuery cq : rws) {
            cypherQueries.add(Decoder.decode(cq));
        }
        return cypherQueries;
    }
}