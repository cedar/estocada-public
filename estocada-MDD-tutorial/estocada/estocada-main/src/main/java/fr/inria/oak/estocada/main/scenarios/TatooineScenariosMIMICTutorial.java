package fr.inria.oak.estocada.main.scenarios;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.oak.commons.miscellaneous.FileUtils;
import fr.inria.oak.estocada.utils.RunnerUtils;

/**
 * Run Original Query
 * 
 * @author ranaalotaibi
 *
 */
public class TatooineScenariosMIMICTutorial {
    private static final Logger LOGGER = Logger.getLogger(TatooineScenariosMIMICTutorial.class);

    public static void QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        //LOGGER.info("Start Executing Query");
        final String query = FileUtils.readStringFromFile(path + "Query.Q");
        final String queryPJ = RunnerUtils.exrtactQueryBlock(query);
        final ViewSchema gdeltSchema = catalog.getViewSchema("MIMIC");
        final GSR gsrRE = (GSR) catalog.getStorageReference("MIMIC");

        final PhyPostgresJSONEval queryEval = new PhyPostgresJSONEval(queryPJ, gsrRE, gdeltSchema.getNRSMD());
        int resultCount = 0;
        queryEval.open();
        List<NTuple> results = new ArrayList<>();
        int tupleSize = queryEval.getNRSMD().colNames.length;
        while (queryEval.hasNext()) {
            NTuple next = queryEval.next();
            results.add(next);
            resultCount++;
        }

        PrintWriter pw = new PrintWriter(new FileWriter(path + "/QueryResults.csv"));
        for (NTuple tuple : results) {
            final String[] tupleStr = tuple.toStringArray();
            for (int i = 0; i < tupleSize; i++) {
                pw.write(tupleStr[i]);
                if (i != (tupleSize - 1))
                    pw.write(",");
            }
            pw.write("\n");
        }
        pw.close();
        System.out.println("--Query Result Count=" + resultCount);
        System.out.println("--Query Result Exported To QueryResults.csv");
    }

}
