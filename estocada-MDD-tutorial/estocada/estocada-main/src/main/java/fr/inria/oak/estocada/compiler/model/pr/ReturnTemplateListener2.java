package fr.inria.oak.estocada.compiler.model.pr;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;

/**
 * PR Return Template Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
final class ReturnTemplateListener2 extends SQLBaseListener {
    private static final Logger log = Logger.getLogger(ReturnTemplateListener2.class);

    private final VariableMapper variableMapper;

    private List<ReturnTerm> terms;

    private ReturnTemplate constructedReturnTemplate;
    private List<String> returnVariables;

    @Inject
    public ReturnTemplateListener2(final VariableMapper variableMapper) {
        this.variableMapper = checkNotNull(variableMapper);
    }

    public ReturnTemplate parse(final String str) throws ParseException {
        terms = new ArrayList<ReturnTerm>();
        returnVariables = new ArrayList<String>();
        final SQLLexer lexer = new SQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final SQLParser parser = new SQLParser(tokens);
        final ParserRuleContext tree = parser.prSelectClause();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        constructedReturnTemplate = new ReturnTemplate(PRModel.ID, terms);
        return constructedReturnTemplate;
    }

    @Override
    public void enterPrTerm(SQLParser.PrTermContext ctx) {
        log.debug("Entering Term: " + ctx.getText());
        final Map<String, String> optionals = new HashMap<String, String>();

        if (ctx.realtion() == null && (PRQueryBlockTreeBuilderAlternative2.relations == null)
                && PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales.isEmpty()) {
            final Variable var = getVariableFromQBTBuilder(ctx);
            if (var != null) {
                terms.add(new ReturnVariableTerm(var, optionals));
            }
            returnVariables.add(ctx.getText());
        }

        if (ctx.realtion() == null && !(PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales.isEmpty())) {
            terms.add(new ReturnVariableTerm((Variable) variableMapper.getVariable(ctx.getText()), optionals));

        }

        //        if (PRQueryBlockTreeBuilderAlternative2.relations != null
        //                && PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales.isEmpty()) {
        //            final Variable var = new Variable(ctx.NAME().getText());
        //            terms.add(new ReturnVariableTerm(var, optionals));
        //            variableMapper.updateVariable(ctx.getText(), var);
        //            returnVariables.add(ctx.getText());
        //        }
        if (ctx.realtion() != null) {
            if (!PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales.isEmpty()) {
                if (PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales.get(ctx.realtion().getText())
                        .get(ctx.NAME().getText()) instanceof String) {
                    terms.add(new ReturnVariableTerm((Variable) variableMapper.getVariable(ctx.NAME().getText()),
                            optionals));
                } else {
                    terms.add(new ReturnVariableTerm(
                            (Variable) PRQueryBlockTreeBuilderAlternative2.realtionsFreshVaribales
                                    .get(ctx.realtion().getText()).get(ctx.NAME().getText()),
                            optionals));
                }
            } else {
                final Variable var = getVariableFromQBTBuilder(ctx);
                if (var != null) {
                    terms.add(new ReturnVariableTerm(var, optionals));
                }
                returnVariables.add(ctx.getText());
            }
        }

    }

    /**
     * Get variable from QBT builder variable mapper.
     * 
     * @param ctx
     *            term context
     * @return var found variable
     */
    private Variable getVariableFromQBTBuilder(final SQLParser.PrTermContext ctx) {
        if (variableMapper.isNotDefined(ctx.getText()) && QBTQueryBlockTreeBuilder.variableMapper != null
                && !QBTQueryBlockTreeBuilder.variableMapper.isEmptyVariablesMapping()) {

            return QBTQueryBlockTreeBuilder.variableMapper.getVariable(ctx.getText());
        }
        return null;
    }

    public List<String> getReferdVariables() {
        return this.returnVariables;
    }

    public ReturnTemplate getConstructedReturnTemplate() {
        return constructedReturnTemplate;
    }

    //    private String getVar() {
    //        Map<String, Relation> relations = PRQueryBlockTreeBuilderAlternative.relations;
    //        for (Map.Entry<String, Relation> entry : relations.entrySet()) {
    //            Relation en
    //        }
    //    }
}
