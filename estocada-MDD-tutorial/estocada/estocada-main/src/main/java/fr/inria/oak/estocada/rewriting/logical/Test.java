package fr.inria.oak.estocada.rewriting.logical;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.SolrDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.oak.estocada.compiler.Utils;

public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/main/resources/testLogicalPlan/rewriting");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {

        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        catalog.delete();
        // Register
        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference1("V1_JSON");
        // Columns names
        final List<String> colNames = new ArrayList<String>();
        colNames.add("SUBJECT_ID");
        colNames.add("HADM_ID");
        colNames.add("LABEL");

        // NRSMD
        final NRSMD NRSMD = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
        final Map<String, Integer> serviceNRSMDMapping = new HashMap<>();
        serviceNRSMDMapping.put("SUBJECT_ID", 0);
        serviceNRSMDMapping.put("HADM_ID", 1);
        serviceNRSMDMapping.put("LABEL", 2);
        ViewSchema serviceSchema = new ViewSchema(NRSMD, serviceNRSMDMapping);
        catalog.add("V1_JSON", storageReferencePostgresSQL1, null, serviceSchema);

        final StorageReference storageReferencePostgresSQL2 = getPostgresSQLStorageReference2("V_PATIENTS");
        // Columns names
        final List<String> colNames2 = new ArrayList<String>();
        colNames2.add("SUBJECT_ID");
        colNames2.add("HADM_ID");

        // NRSMD
        final NRSMD NRSMD2 = new NRSMD(2,
                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE }, colNames2);
        final Map<String, Integer> serviceNRSMDMapping2 = new HashMap<>();
        serviceNRSMDMapping2.put("SUBJECT_ID", 0);
        serviceNRSMDMapping2.put("HADM_ID", 1);
        ViewSchema serviceSchema2 = new ViewSchema(NRSMD2, serviceNRSMDMapping2);
        catalog.add("V_PATIENTS", storageReferencePostgresSQL2, null, serviceSchema2);

        final StorageReference storageReferenceSolr2 = getSolrStorageReference("Notes");
        // Columns names
        final List<String> colNames3 = new ArrayList<String>();
        colNames3.add("SUBJECT_ID");
        colNames3.add("HADM_ID");
        colNames3.add("TEXT");

        // NRSMD
        final NRSMD NRSMD3 = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, colNames3);
        final Map<String, Integer> serviceNRSMDMapping3 = new HashMap<>();
        serviceNRSMDMapping3.put("SUBJECT_ID", 0);
        serviceNRSMDMapping3.put("HADM_ID", 1);
        serviceNRSMDMapping3.put("TEXT", 2);
        ViewSchema serviceSchema3 = new ViewSchema(NRSMD3, serviceNRSMDMapping3);
        catalog.add("Notes", storageReferenceSolr2, null, serviceSchema3);

        final RewritingToLogicalPlanGenerator planGenerator =
                new RewritingToLogicalPlanGenerator(Utils.parseQuery((INPUT_QUERY_FILE)));

        System.out.println(planGenerator.generate().getName());

    }

    private static StorageReference getPostgresSQLStorageReference1(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        gsr.setProperty("modelId", "PJ");

        return gsr;
    }

    private static StorageReference getPostgresSQLStorageReference2(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        gsr.setProperty("modelId", "PR");

        return gsr;
    }

    private static StorageReference getSolrStorageReference(String collectionName) throws Exception {
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", SolrDatabaseLoader.DEFAULT_SOLR_URL);
        gsr.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);
        gsr.setProperty("coreName", collectionName);
        gsr.setProperty("modelId", "SJ");

        return gsr;

    }

}
