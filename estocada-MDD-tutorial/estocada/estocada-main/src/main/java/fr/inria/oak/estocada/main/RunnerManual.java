package fr.inria.oak.estocada.main;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.estocada.compiler.Language;

/**
 * ESTOCADA main runner - Fixed manual BindJoin order (hand optimized)
 * 
 * @author Rana Alotaibi
 */
public class RunnerManual {
    public static void main(String[] args) throws NumberFormatException,

            Exception {
        Injector injector = Guice.createInjector(new EstocadaModule());
        final EstocadaRunnerManual runner = injector.getInstance(EstocadaRunnerManual.class);
        final Language lang = Language.BIGDAWG;
        switch (lang) {
            case QBT:
                //                runner.runTatooineSMQCMV();
                //runner.runTatooineCMQSMV();
                break;
            case BIGDAWG:
                //runner.runBigDAWGSMQCMV();
                //runner.runBigDAWGCMQSMV();
                break;
            default:
                throw new IllegalArgumentException("Language Not Supported");
        }

    }
}
