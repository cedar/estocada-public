// Generated from MADLIB.g4 by ANTLR 4.4

package fr.inria.oak.estocada.compiler.model.pm;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MADLIBParser}.
 */
public interface MADLIBListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#matrixSourceName}.
	 * @param ctx the parse tree
	 */
	void enterMatrixSourceName(@NotNull MADLIBParser.MatrixSourceNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#matrixSourceName}.
	 * @param ctx the parse tree
	 */
	void exitMatrixSourceName(@NotNull MADLIBParser.MatrixSourceNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#matrixOutName}.
	 * @param ctx the parse tree
	 */
	void enterMatrixOutName(@NotNull MADLIBParser.MatrixOutNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#matrixOutName}.
	 * @param ctx the parse tree
	 */
	void exitMatrixOutName(@NotNull MADLIBParser.MatrixOutNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulExpression(@NotNull MADLIBParser.MatrixMulExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulExpression(@NotNull MADLIBParser.MatrixMulExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(@NotNull MADLIBParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(@NotNull MADLIBParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#madScript}.
	 * @param ctx the parse tree
	 */
	void enterMadScript(@NotNull MADLIBParser.MadScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#madScript}.
	 * @param ctx the parse tree
	 */
	void exitMadScript(@NotNull MADLIBParser.MadScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#madQuery}.
	 * @param ctx the parse tree
	 */
	void enterMadQuery(@NotNull MADLIBParser.MadQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#madQuery}.
	 * @param ctx the parse tree
	 */
	void exitMadQuery(@NotNull MADLIBParser.MadQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MADLIBParser#madStatemnet}.
	 * @param ctx the parse tree
	 */
	void enterMadStatemnet(@NotNull MADLIBParser.MadStatemnetContext ctx);
	/**
	 * Exit a parse tree produced by {@link MADLIBParser#madStatemnet}.
	 * @param ctx the parse tree
	 */
	void exitMadStatemnet(@NotNull MADLIBParser.MadStatemnetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixTransposeExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixTransposeExpression(@NotNull MADLIBParser.MatrixTransposeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixTransposeExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixTransposeExpression(@NotNull MADLIBParser.MatrixTransposeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulScalarExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulScalarExpression(@NotNull MADLIBParser.MatrixMulScalarExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulScalarExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulScalarExpression(@NotNull MADLIBParser.MatrixMulScalarExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixAddExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixAddExpression(@NotNull MADLIBParser.MatrixAddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixAddExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixAddExpression(@NotNull MADLIBParser.MatrixAddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixInverseExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixInverseExpression(@NotNull MADLIBParser.MatrixInverseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixInverseExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixInverseExpression(@NotNull MADLIBParser.MatrixInverseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulElementwiseExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulElementwiseExpression(@NotNull MADLIBParser.MatrixMulElementwiseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulElementwiseExpression}
	 * labeled alternative in {@link MADLIBParser#madExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulElementwiseExpression(@NotNull MADLIBParser.MatrixMulElementwiseExpressionContext ctx);
}