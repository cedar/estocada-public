package fr.inria.oak.estocada.rewriting.logical;

import java.util.HashMap;
import java.util.Map;

import fr.inria.oak.estocada.rewriting.decoder.aj.AJDecoder;
import fr.inria.oak.estocada.rewriting.decoder.pj.PJDecoder;
import fr.inria.oak.estocada.rewriting.decoder.pr.PRDecoder;
import fr.inria.oak.estocada.rewriting.decoder.rk.RKDecoder;
import fr.inria.oak.estocada.rewriting.decoder.sj.SJDecoder;
import fr.inria.oak.estocada.views.materialization.module.Annotation;

/**
 * A class that instantiates instances of all models decoders.
 * 
 * @author ranaalotaibi
 *
 */
public final class ModelDecodersInstantiator {
    final static Map<Annotation, Decoder> modelDecodersInstances = new HashMap<Annotation, Decoder>();

    public static Map<Annotation, Decoder> instantiate() {
        modelDecodersInstances.put(Annotation.AJ, (Decoder) new AJDecoder());
        modelDecodersInstances.put(Annotation.PJ, (Decoder) new PJDecoder());
        modelDecodersInstances.put(Annotation.PR, (Decoder) new PRDecoder(Semantics.BAG));
        modelDecodersInstances.put(Annotation.RK, (Decoder) new RKDecoder());
        modelDecodersInstances.put(Annotation.SJ, (Decoder) new SJDecoder());
        return modelDecodersInstances;

    }
}
