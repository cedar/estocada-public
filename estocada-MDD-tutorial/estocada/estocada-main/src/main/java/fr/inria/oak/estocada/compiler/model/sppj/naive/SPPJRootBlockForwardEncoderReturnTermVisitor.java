package fr.inria.oak.estocada.compiler.model.sppj.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.Element;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.StringElement;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.model.sppj.ArrayElementFactory;

@Singleton
public class SPPJRootBlockForwardEncoderReturnTermVisitor extends SPPJBaseForwardEncoderReturnTermVisitor {
    private final VariableFactory elementVariableFactory;
    private Term rootElementCreatedNode;

    @Inject
    public SPPJRootBlockForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory,
            @Named("ElementVariableFactory") final VariableFactory elementVariableFactory) {
        super(arrayElementFactory);
        this.elementVariableFactory = elementVariableFactory;
    }

    public List<Atom> encode(final ReturnTemplate template, final String viewName) {
        builder = ImmutableList.builder();
        this.viewName = viewName;
        template.accept(this);
        return builder.build();
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        if (!term.getElement().isEmpty()) {
            if (builder.build().isEmpty()) {
                if (term.hasChildren()) {
                    buildRootElement(term.getCreatedNode());
                } else {
                    final Variable var = elementVariableFactory.createFreshVar();
                    buildRootElement(var);
                }
            }
            super.visitPre(term);
        } else {
            if (builder.build().isEmpty()) {
                if (term.hasChildren()) {
                    buildRootElement(term.getCreatedNode());
                }
            }
        }
    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        if (builder.build().isEmpty()) {
            buildRootElement(term.getCreatedNode());
        } else {
            super.visit(term);
        }
    }

    @Override
    public void visit(final ReturnStringTerm term) {
        if (builder.build().isEmpty()) {
            buildRootElement(term.toTerm());
        } else {
            super.visit(term);
        }
    }

    @Override
    protected Element getParentBlockElement(final ReturnTerm term) {
        if (term.hasParent()) {
            if (!term.getParent().getElement().isEmpty()) {
                return term.getParent().getElement();
            } else {
                if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
                    return term.getParent().getParent().getElement();
                }
            }
        }
        return new StringElement("");
    }

    @Override
    protected Term getParentBlockCreatedNode(final ReturnTerm term) {
        if (term.hasParent()) {
            // TODO: test that we are not introducing a bug when the parent's
            // parent is not the root.
            if ((term instanceof ReturnVariableTerm || term instanceof ReturnStringTerm) && term.getParent().hasParent()
                    && term.getParent().getParent().getElement().isEmpty()) {
                return rootElementCreatedNode;
            }
            if (!term.getParent().getElement().isEmpty()) {
                return term.getParent().getCreatedNode();
            } else {
                if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
                    return term.getParent().getParent().getCreatedNode();
                }
            }
        }
        return rootElementCreatedNode;
    }

    private void buildRootElement(final Term var) {
        rootElementCreatedNode = var;
        builder.add(new Atom(viewName, rootElementCreatedNode));
    }
}