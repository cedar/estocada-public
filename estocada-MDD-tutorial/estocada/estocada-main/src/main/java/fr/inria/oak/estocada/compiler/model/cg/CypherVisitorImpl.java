package fr.inria.oak.estocada.compiler.model.cg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

public class CypherVisitorImpl extends CypherBaseVisitor<Object> {
    private Constraint constraint;
    private CypherVisitorHelper helper;

    public CypherVisitorImpl() {
        super();
        this.constraint = new Constraint();
        this.helper = new CypherVisitorHelper();
    }

    @Override
    public Object visitOC_Cypher(CypherParser.OC_CypherContext ctx) {
        return visit(ctx.oC_Statement());
    }

    @Override
    public Object visitOC_Statement(CypherParser.OC_StatementContext ctx) {
        return visit(ctx.oC_Query());
    }

    @Override
    public Object visitOC_Query(CypherParser.OC_QueryContext ctx) {
        return visit(ctx.oC_RegularQuery());
    }

    @Override
    public Object visitOC_RegularQuery(CypherParser.OC_RegularQueryContext ctx) {
        return visit(ctx.oC_SingleQuery());
    }

    @Override
    public Object visitOC_SingleQuerySinglePart(CypherParser.OC_SingleQuerySinglePartContext ctx) {
        return visit(ctx.oC_SinglePartQuery());
    }

    @Override
    public Object visitOC_SingleQueryMultiPart(CypherParser.OC_SingleQueryMultiPartContext ctx) {
        return visit(ctx.oC_MultiPartQuery());
    }

    @Override
    public Object visitOC_MultiPartQuery(CypherParser.OC_MultiPartQueryContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_With(i) != null; i++) {
            // reading clauses
            List<Object> readingEncoding = new ArrayList<Object>();
            for (int j = 0; ctx.oC_ReadingClause(j) != null; j++) {
                readingEncoding.addAll((List<Object>) visit(ctx.oC_ReadingClause(j)));
            }
            // with clause
            List<Object> withEncoding = (List<Object>) visit(ctx.oC_With(i));
            encoding.addAll(readingEncoding);
            encoding.addAll(withEncoding);
        }
        List<Object> singleQueryEncoding = (List<Object>) visit(ctx.oC_SinglePartQuery());
        encoding.addAll(singleQueryEncoding);
        encoding = helper.removeRedundantKindIAndLabelI(encoding);
        encoding = helper.removeRedundantPropertyI(encoding);
        encoding = helper.removeRedundantValueI(encoding);
        return encoding;
    }

    @Override
    public Object visitOC_With(CypherParser.OC_WithContext ctx) {
        List<Object> withEncoding = new ArrayList<Object>();
        withEncoding.addAll((List<Object>) visit(ctx.oC_ProjectionBody()));
        withEncoding = helper.removeQueryHead(withEncoding);
        if (ctx.oC_Where() != null)
            withEncoding.addAll((List<Object>) visit(ctx.oC_Where()));
        return withEncoding;
    }

    @Override
    public Object visitOC_SinglePartQueryNoUpdating(CypherParser.OC_SinglePartQueryNoUpdatingContext ctx) {
        // reading clause
        List<Object> readingEncoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_ReadingClause(i) != null; i++) {
            readingEncoding.addAll((List<Object>) visit(ctx.oC_ReadingClause(i)));
        }
        // return clause
        List<Object> returnEncoding = (List<Object>) visit(ctx.oC_Return());
        List<Object> encoding = helper.combineReadingReturn(readingEncoding, returnEncoding);
        encoding = helper.removeRedundantKindIAndLabelI(encoding);
        encoding = helper.removeRedundantPropertyI(encoding);
        encoding = helper.removeRedundantValueI(encoding);
        return encoding;
    }

    @Override
    public Object visitOC_SinglePartQueryUpdating(CypherParser.OC_SinglePartQueryUpdatingContext ctx) {
        // reading clause
        List<Object> readingEncoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_ReadingClause(i) != null; i++) {
            readingEncoding.addAll((List<Object>) visit(ctx.oC_ReadingClause(i)));
        }
        // updating clause
        List<Object> updatingEncoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_UpdatingClause(i) != null; i++) {
            updatingEncoding.addAll((List<Object>) visit(ctx.oC_UpdatingClause(i)));
        }
        List<Object> encoding = helper.removeRedundantPropertyI(readingEncoding, updatingEncoding);
        encoding = helper.removeRedundantValueI((List<Object>) encoding.get(0), (List<Object>) encoding.get(1));
        return encoding;
    }

    @Override
    public Object visitOC_ReadingClause(CypherParser.OC_ReadingClauseContext ctx) {
        return visit(ctx.oC_Match());
    }

    @Override
    public Object visitOC_Match(CypherParser.OC_MatchContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        // pattern clause
        encoding.addAll((List<Object>) visit(ctx.oC_Pattern()));
        // where clause
        if (ctx.oC_Where() != null)
            encoding.addAll((List<Object>) visit(ctx.oC_Where()));
        return encoding;
    }

    @Override
    public Object visitOC_Where(CypherParser.OC_WhereContext ctx) {
        return visit(ctx.oC_Expression());
    }

    @Override
    public Object visitOC_UpdatingClause(CypherParser.OC_UpdatingClauseContext ctx) {
        return visit(ctx.oC_Create());
    }

    @Override
    public Object visitOC_Create(CypherParser.OC_CreateContext ctx) {
        return visit(ctx.oC_Pattern());
    }

    @Override
    public Object visitOC_Pattern(CypherParser.OC_PatternContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_PatternPart(i) != null; i++) {
            encoding.addAll((List<Object>) visit(ctx.oC_PatternPart(i)));
        }
        return encoding;
    }

    @Override
    public Object visitOC_PatternPartNoVariable(CypherParser.OC_PatternPartNoVariableContext ctx) {
        return visit(ctx.oC_AnonymousPatternPart());
    }

    @Override
    public Object visitOC_AnonymousPatternPart(CypherParser.OC_AnonymousPatternPartContext ctx) {
        return visit(ctx.oC_PatternElement());
    }

    @Override
    public Object visitOC_PatternElementParanthesis(CypherParser.OC_PatternElementParanthesisContext ctx) {
        return visit(ctx.oC_PatternElement());
    }

    @Override
    public Object visitOC_PatternElementNoParanthesis(CypherParser.OC_PatternElementNoParanthesisContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        // nodePattern
        String varName = null, newVarName = null;
        encoding.addAll((List<Object>) visit(ctx.oC_NodePattern()));
        for (Object atom : encoding) {
            if (atom instanceof LabelI) {
                varName = ((LabelI) atom).getId();
                break;
            } else if (atom instanceof KindI) {
                varName = ((KindI) atom).getId();
                break;
            }
        }
        // patternElementChain
        for (int i = 0; ctx.oC_PatternElementChain(i) != null; i++) {
            List<Object> patternChainEncoding = (List<Object>) visit(ctx.oC_PatternElementChain(i));
            for (Object atom : patternChainEncoding) {
                if (atom instanceof ConnectionI) {
                    newVarName = ((ConnectionI) atom).getNotTempNId();
                    ((ConnectionI) atom).replaceTempToken(varName);
                }
            }
            varName = newVarName;
            encoding.addAll(patternChainEncoding);
        }
        return encoding;
    }

    @Override
    public Object visitOC_NodePattern(CypherParser.OC_NodePatternContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        // if variable exists, ok; otherwise, just encode using an anonymous id
        String varName;
        if (ctx.oC_Variable() != null)
            varName = ctx.oC_Variable().getText();
        else
            varName = constraint.getNextId();
        // encode labels
        if (ctx.oC_NodeLabels() != null) {
            List<String> nodeLabels = (List<String>) visit(ctx.oC_NodeLabels());
            for (String nodeLabel : nodeLabels) {
                encoding.add(new LabelI(varName, '\"' + nodeLabel + '\"'));
            }
        } else {
            encoding.add(new KindI(varName, '\"' + "N" + '\"'));
        }
        // encode properties
        if (ctx.oC_Properties() != null) {
            Map<String, Object> properties = (Map<String, Object>) visit(ctx.oC_Properties());
            for (Map.Entry<String, Object> entry : properties.entrySet()) {
                String propertyKeyName = entry.getKey();
                List<Object> propertyValueEncoding = (List<Object>) entry.getValue();
                String dtId = constraint.getNextDtId();
                if (propertyValueEncoding.size() == 1 && propertyValueEncoding.get(0) instanceof ValueI) { // (p: Person {name: "Jack"})
                    ValueI valueIAtom = (ValueI) propertyValueEncoding.get(0);
                    String vId = valueIAtom.getvId();
                    PropertyI propertyI = new PropertyI(varName, '\"' + propertyKeyName + '\"', dtId, vId);
                    encoding.add(propertyI);
                    helper.propertyIContext.add(propertyI);
                    encoding.add(valueIAtom);
                } else { // (n: Nv {p1: n1.p1})
                    String vId = constraint.getNextVId();
                    PropertyI propertyI = new PropertyI(varName, '\"' + propertyKeyName + '\"', dtId, vId);
                    encoding.add(propertyI);
                    helper.propertyIContext.add(propertyI);
                    // add eq for ex. (:Nv {p1: n1.p1})
                    String refVId = null;
                    for (Object atom : propertyValueEncoding) {
                        if (atom instanceof ValueI)
                            refVId = ((ValueI) atom).getvId();
                    }
                    encoding.add(new EqI(refVId, vId));
                    encoding.addAll(propertyValueEncoding);
                }
            }
        }
        return encoding;
    }

    @Override
    public Object visitOC_NodeLabels(CypherParser.OC_NodeLabelsContext ctx) {
        List<String> nodeLabels = new ArrayList<String>();
        for (int i = 0; ctx.oC_NodeLabel(i) != null; i++) {
            nodeLabels.add((String) visit(ctx.oC_NodeLabel(i)));
        } ;
        return nodeLabels;
    }

    @Override
    public Object visitOC_NodeLabel(CypherParser.OC_NodeLabelContext ctx) {
        return ctx.oC_LabelName().getText();
    }

    @Override
    public Object visitOC_PropertiesMapLiteral(CypherParser.OC_PropertiesMapLiteralContext ctx) {
        return visit(ctx.oC_MapLiteral());
    }

    @Override
    public Object visitOC_MapLiteral(CypherParser.OC_MapLiteralContext ctx) {
        Map<String, Object> properties = new HashMap<String, Object>();
        for (int i = 0; ctx.oC_PropertyKeyName(i) != null && ctx.oC_Expression(i) != null; i++) {
            String propertyKeyName = (String) visit(ctx.oC_PropertyKeyName(i));
            // property value can be scalar value or another property lookup (p: n.p')
            Object object = visit(ctx.oC_Expression(i));
            properties.put(propertyKeyName, object);
        }
        return properties;
    }

    @Override
    public Object visitOC_PropertyKeyName(CypherParser.OC_PropertyKeyNameContext ctx) {
        return ctx.oC_SchemaName().getText();
    }

    @Override
    public Object visitOC_PatternElementChain(CypherParser.OC_PatternElementChainContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        String nId = null, rId = null;
        // encode node
        encoding.addAll((List<Object>) visit(ctx.oC_NodePattern()));
        for (Object atom : encoding) {
            if (atom instanceof LabelI)
                nId = ((LabelI) atom).getId();
            else if (atom instanceof KindI)
                nId = ((KindI) atom).getId();
        }
        // encode relationship
        List<Object> edgeEncoding = (List<Object>) visit(ctx.oC_RelationshipPattern());
        for (Object atom : edgeEncoding) {
            if (atom instanceof LabelI)
                rId = ((LabelI) atom).getId();
            else if (atom instanceof KindI)
                rId = ((KindI) atom).getId();
        }
        encoding.addAll(edgeEncoding);
        // encode path atom. Nodir means disjunctive, so currently ignore it
        if ((ctx.oC_RelationshipPattern() instanceof CypherParser.OC_RelationshipPatternLeftDirContext))
            encoding.add(new ConnectionI(nId, Constraint.getTempToken(), rId));
        else if ((ctx.oC_RelationshipPattern() instanceof CypherParser.OC_RelationshipPatternRightDirContext))
            encoding.add(new ConnectionI(Constraint.getTempToken(), nId, rId));
        else if ((ctx.oC_RelationshipPattern() instanceof CypherParser.OC_RelationshipPatternBothDirContext)) {
            encoding.add(new ConnectionI(Constraint.getTempToken(), nId, rId));
            encoding.add(new ConnectionI(nId, Constraint.getTempToken(), rId));
        }
        return encoding;
    }

    @Override
    public Object visitOC_RelationshipPatternLeftDir(CypherParser.OC_RelationshipPatternLeftDirContext ctx) {
        return visit(ctx.oC_RelationshipDetail());
    }

    @Override
    public Object visitOC_RelationshipPatternRightDir(CypherParser.OC_RelationshipPatternRightDirContext ctx) {
        return visit(ctx.oC_RelationshipDetail());
    }

    @Override
    public Object visitOC_RelationshipPatternBothDir(CypherParser.OC_RelationshipPatternBothDirContext ctx) {
        return visit(ctx.oC_RelationshipDetail());
    }

    @Override
    public Object visitOC_RelationshipDetail(CypherParser.OC_RelationshipDetailContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        // if variable exists, ok; otherwise, just encode using an anonymous id
        String varName;
        if (ctx.oC_Variable() != null)
            varName = ctx.oC_Variable().getText();
        else
            varName = constraint.getNextId();
        // encode labels, here only single reltype corresponds to CQ, multi means disjunctive
        if (ctx.oC_RelationshipTypes() != null) {
            String edgeLabel = (String) visit(ctx.oC_RelationshipTypes());
            encoding.add(new LabelI(varName, '\"' + edgeLabel + '\"'));
        } else {
            encoding.add(new KindI(varName, '\"' + "E" + '\"'));
        }
        // rangeLiteral leads to disjunctive, so currently ignore it
        // encode properties
        if (ctx.oC_Properties() != null) {
            Map<String, Object> properties = (Map<String, Object>) visit(ctx.oC_Properties());
            for (Map.Entry<String, Object> entry : properties.entrySet()) {
                String propertyKeyName = entry.getKey();
                List<Object> propertyValueEncoding = (List<Object>) entry.getValue();
                String dtId = constraint.getNextDtId();
                if (propertyValueEncoding.size() == 1 && propertyValueEncoding.get(0) instanceof ValueI) { // (p: Person {name: "Jack"})
                    ValueI valueIAtom = (ValueI) propertyValueEncoding.get(0);
                    String vId = valueIAtom.getvId();
                    PropertyI propertyI = new PropertyI(varName, '\"' + propertyKeyName + '\"', dtId, vId);
                    encoding.add(propertyI);
                    helper.propertyIContext.add(propertyI);
                    encoding.add(valueIAtom);
                } else { // (n: Nv {p1: n1.p1})
                    String vId = constraint.getNextVId();
                    PropertyI propertyI = new PropertyI(varName, '\"' + propertyKeyName + '\"', dtId, vId);
                    encoding.add(propertyI);
                    helper.propertyIContext.add(propertyI);
                    // add eq for ex. (:Nv {p1: n1.p1})
                    String refVId = null;
                    for (Object atom : propertyValueEncoding) {
                        if (atom instanceof ValueI)
                            refVId = ((ValueI) atom).getvId();
                    }
                    encoding.add(new EqI(refVId, vId));
                    encoding.addAll(propertyValueEncoding);
                }
            }
        }
        return encoding;
    }

    @Override
    public Object visitOC_RelationshipTypes(CypherParser.OC_RelationshipTypesContext ctx) {
        return ctx.oC_RelTypeName().getText();
    }

    @Override
    public Object visitOC_Return(CypherParser.OC_ReturnContext ctx) {
        return visit(ctx.oC_ProjectionBody());
    }

    @Override
    public Object visitOC_ProjectionBody(CypherParser.OC_ProjectionBodyContext ctx) {
        return visit(ctx.oC_ProjectionItems());
    }

    @Override
    public Object visitOC_ProjectionItemsNoStar(CypherParser.OC_ProjectionItemsNoStarContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        List<String> returnVar = new ArrayList<String>();
        for (int i = 0; ctx.oC_ProjectionItem(i) != null; i++) {
            List<Object> projectionItem = (List<Object>) visit(ctx.oC_ProjectionItem(i));
            if (projectionItem.size() == 1 && projectionItem.get(0) instanceof String) { // return value is a node/edge
                String varName = (String) projectionItem.get(0);
                returnVar.add(varName);
            } else {
                encoding.addAll(projectionItem);
                for (Object atom : projectionItem) {
                    if (atom instanceof ValueI)
                        returnVar.add(((ValueI) atom).getV());
                }
            }
        }
        encoding.add(new QueryHead(returnVar));
        return encoding;
    }

    @Override
    public Object visitOC_ProjectionItemNoVariable(CypherParser.OC_ProjectionItemNoVariableContext ctx) {
        return visit(ctx.oC_Expression());
    }

    @Override
    public Object visitOC_ProjectionItemVariable(CypherParser.OC_ProjectionItemVariableContext ctx) {
        List<Object> encoding = (List<Object>) visit(ctx.oC_Expression());
        String varName = ctx.oC_Variable().getText();
        helper.varContext.put(varName, encoding);
        return encoding;
    }

    @Override
    public Object visitOC_Expression(CypherParser.OC_ExpressionContext ctx) {
        return visit(ctx.oC_AndExpression());
    }

    @Override
    public Object visitOC_AndExpression(CypherParser.OC_AndExpressionContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        for (int i = 0; ctx.oC_ComparisonExpression(i) != null; i++) {
            Object exprEncoding = visit(ctx.oC_ComparisonExpression(i)); // if tmp is List<Object>, then return value is a property; if is a String, then return value is a node/edge
            if (exprEncoding instanceof List) {
                encoding.addAll((List<Object>) exprEncoding);
            } else if (exprEncoding instanceof String) {
                encoding.add(exprEncoding);
            }
        }
        return encoding;
    }

    @Override
    public Object visitOC_ComparisonSingle(CypherParser.OC_ComparisonSingleContext ctx) {
        return visit(ctx.oC_StringListNullOperatorExpression());
    }

    @Override
    // b.title = "database"; b.title = c.title = d.title
    public Object visitOC_ComparisonSingleOrMultiple(CypherParser.OC_ComparisonSingleOrMultipleContext ctx) {
        List<Object> encoding = new ArrayList<Object>(); // record final result, need to remove all Value_I() atom
        String compVId1 = null, compVId2 = null; // a = b: a is compVId1, b is compVId2
        List<Object> firstExprEncoding = (List<Object>) visit(ctx.oC_StringListNullOperatorExpression()); // record all atoms of encoding this expression
        for (Object atom : firstExprEncoding) {
            if (atom instanceof PropertyI) {
                encoding.add(atom);
                compVId1 = ((PropertyI) atom).getvId();
                break;
            } else if (atom instanceof ValueI) {
                compVId1 = ((ValueI) atom).getvId();
                break;
            }
        }
        for (int i = 0; ctx.oC_PartialComparisonExpression(i) != null; i++) {
            Pair<String, List<Object>> partialCompEncodingPair =
                    (Pair<String, List<Object>>) visit(ctx.oC_PartialComparisonExpression(i));
            String compSymbol = partialCompEncodingPair.getKey();
            List<Object> partialCompEncoding = partialCompEncodingPair.getValue();
            if (partialCompEncoding.size() == 1 && partialCompEncoding.get(0) instanceof ValueI
                    && compSymbol.equals("=")) { // = "database", only has one ValueI(), and this only works for "="
                ValueI valueIAtom = (ValueI) partialCompEncoding.get(0);
                valueIAtom.setvId(compVId1);
            } else {
                for (Object atom : partialCompEncoding) {
                    if (atom instanceof PropertyI) {
                        encoding.add(atom);
                        compVId2 = ((PropertyI) atom).getvId();
                        break;
                    } else if (atom instanceof ValueI) {
                        compVId2 = ((ValueI) atom).getvId();
                        break;
                    }
                }
                switch (compSymbol) {
                    case "=":
                        encoding.add(new EqI(compVId1, compVId2));
                        break;
                    case "<>":
                        encoding.add(new NeI(compVId1, compVId2));
                        break;
                    case "<":
                        encoding.add(new LtI(compVId1, compVId2));
                        break;
                    case ">":
                        encoding.add(new GtI(compVId1, compVId2));
                        break;
                    case "<=":
                        encoding.add(new LeI(compVId1, compVId2));
                        break;
                    case ">=":
                        encoding.add(new GeI(compVId1, compVId2));
                        break;
                }
                compVId1 = compVId2;
            }
            encoding.addAll(partialCompEncoding);
        }
        return encoding;
    }

    @Override
    public Object visitOC_PartialComparisonExpression(CypherParser.OC_PartialComparisonExpressionContext ctx) {
        Pair<String, List<Object>> encoding =
                Pair.of(ctx.children.get(0).getText(), (List<Object>) visit(ctx.oC_StringListNullOperatorExpression()));
        return encoding;
    }

    @Override
    public Object visitOC_ParenthesizedExpression(CypherParser.OC_ParenthesizedExpressionContext ctx) {
        return visit(ctx.oC_Expression());
    }

    @Override
    public Object visitOC_StringListNullOperatorExpression(
            CypherParser.OC_StringListNullOperatorExpressionContext ctx) {
        return visit(ctx.oC_PropertyOrLabelsExpression());
    }

    @Override
    public Object visitOC_PropertyOrLabelsExpressionAtom(CypherParser.OC_PropertyOrLabelsExpressionAtomContext ctx) { // n.p1 as a; "jack"
        String varName = ctx.oC_Atom().getText();
        List<Object> varEncoding = helper.varContext.get(varName);
        if (varEncoding != null) {
            return varEncoding;
        } else
            return visit(ctx.oC_Atom());
    }

    @Override
    public Object visitOC_PropertyOrLabelsExpressionLookup(
            CypherParser.OC_PropertyOrLabelsExpressionLookupContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        // visit atom, currently only variable
        String varName = (String) visit(ctx.oC_Atom());
        // visit propertyKeyName
        for (int i = 0; ctx.oC_PropertyLookup(i) != null; i++) {
            String propertyKeyName = (String) visit(ctx.oC_PropertyLookup(i));
            // check if the propertyI has been defined, true then just extract vId
            // ??? sometimes still need a new vId, ex. create (:Nv {p1:n1.p1}) and n1.p1 occurs before
            String vId = helper.lookupPropertyI(new PropertyI(varName, '\"' + propertyKeyName + '\"',
                    Constraint.getTempToken(), Constraint.getTempToken()));
            if (vId == null) {
                String dtId = constraint.getNextDtId();
                vId = constraint.getNextVId();
                PropertyI propertyI = new PropertyI(varName, '\"' + propertyKeyName + '\"', dtId, vId);
                encoding.add(propertyI);
                helper.propertyIContext.add(propertyI);
            } else {
                encoding.add(helper.getPropertyI(new PropertyI(varName, '\"' + propertyKeyName + '\"',
                        Constraint.getTempToken(), Constraint.getTempToken())));
            }
            String v = constraint.getNextV();
            encoding.add(new ValueI(vId, v));
        }
        return encoding;
    }

    @Override
    public Object visitOC_AtomLiteral(CypherParser.OC_AtomLiteralContext ctx) {
        return visit(ctx.oC_Literal());
    }

    @Override
    public Object visitOC_AtomVariable(CypherParser.OC_AtomVariableContext ctx) {
        // warning: no distinguish between entity variables and other variables
        return ctx.oC_Variable().getText();
    }

    @Override
    public Object visitOC_PropertyLookup(CypherParser.OC_PropertyLookupContext ctx) {
        return ctx.oC_PropertyKeyName().getText();
    }

    @Override
    public Object visitOC_LiteralNumber(CypherParser.OC_LiteralNumberContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        String strConst = "\"" + ctx.getText() + "\"";
        String vId = constraint.getNextVId();
        encoding.add(new ValueI(vId, strConst));
        return encoding;
    }

    @Override
    public Object visitOC_LiteralString(CypherParser.OC_LiteralStringContext ctx) {
        List<Object> encoding = new ArrayList<Object>();
        String strConst = ctx.getText();
        String vId = constraint.getNextVId();
        encoding.add(new ValueI(vId, strConst));
        return encoding;
    }
}
