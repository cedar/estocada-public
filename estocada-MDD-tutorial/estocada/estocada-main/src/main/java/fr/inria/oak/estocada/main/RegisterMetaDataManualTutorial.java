package fr.inria.oak.estocada.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.SolrDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Register Views and Data in the Catalog
 * 
 * @author ranaalotaibi
 *
 */
public class RegisterMetaDataManualTutorial {

    public static void registerMetaData() throws Exception {

        Parameters.init();
        Catalog catalog = Catalog.getInstance();

        //PJ MIIMIC-Query
        final StorageReference storageReferencePostgresSQLRE = getPostgresSQLStorageReferenceMIMIC("MIMIC");
        // Columns names
        final List<String> gdeltRECols = new ArrayList<String>();
        //gdeltRECols.add("psubject_id");
        gdeltRECols.add("SUBJECT_ID");
        gdeltRECols.add("HADM_ID");
        gdeltRECols.add("DOB");

        // PJ MIIMIC NRSMD
        final NRSMD gdeltQueryRWNRSMD = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, gdeltRECols);
        final Map<String, Integer> gdeltSchemaMapping = new HashMap<>();
        gdeltSchemaMapping.put("SUBJECT_ID", 0);
        gdeltSchemaMapping.put("HADM_ID", 1);
        gdeltSchemaMapping.put("DOB", 2);
        ViewSchema gdeltschema = new ViewSchema(gdeltQueryRWNRSMD, gdeltSchemaMapping);
        catalog.add("MIMIC", storageReferencePostgresSQLRE, null, gdeltschema);

        final StorageReference storageReferencePostgresSQLPJ = getPostgresSQLStorageReferenceMIMIC("MIMICPJ");
        // Columns names
        final List<String> PJCols = new ArrayList<String>();
        PJCols.add("psubject_id");

        // PJ MIIMIC NRSMD
        final NRSMD PJColsNRSMD = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, PJCols);
        final Map<String, Integer> PJSchemaMapping = new HashMap<>();
        PJSchemaMapping.put("psubject_id", 0);
        ViewSchema PJSchema = new ViewSchema(PJColsNRSMD, PJSchemaMapping);
        catalog.add("MIMICPJ", storageReferencePostgresSQLPJ, null, PJSchema);

        ///PR MIMIC-Query 
        final StorageReference storageReferencePostgresSQLPR = getPostgresSQLStorageReferenceMIMIC("MIMICPR");
        // Columns names
        final List<String> PRCols = new ArrayList<String>();
        PRCols.add("SUBJECT_ID");
        PRCols.add("DOB");
        // PR MIIMIC NRSMD
        final NRSMD PRColsNRSMD = new NRSMD(2,
                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, PRCols);

        final Map<String, Integer> PRSchemaMapping = new HashMap<>();
        PRSchemaMapping.put("SUBJECT_ID", 0);
        PRSchemaMapping.put("DOB", 1);
        ViewSchema PRSchema = new ViewSchema(PRColsNRSMD, PRSchemaMapping);
        catalog.add("MIMICPR", storageReferencePostgresSQLPR, null, PRSchema);

        //Solr Data
        final StorageReference gsrText = getSolrStorageReference("mimicTutoText");
        gsrText.setProperty("modelId", "SJ");
        gsrText.setProperty("viewName", "mimicTutoText");
        gsrText.setProperty("zkhost", "localhost:9983");

        final List<String> solrcolNames = new ArrayList<String>();
        final Map<String, Integer> solrNRSMDMapping = new HashMap<>();
        solrcolNames.add("SUBJECT_ID");
        solrcolNames.add("HADM_ID");
        solrNRSMDMapping.put("SUBJECT_ID", 0);
        solrNRSMDMapping.put("HADM_ID", 1);
        NRSMD nrsmdsolr =
                new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE },
                        solrcolNames);
        ViewSchema solrschema = new ViewSchema(nrsmdsolr, solrNRSMDMapping);
        catalog.add("mimicTutoText", gsrText, null, solrschema);

        //View VPJPJ
        StorageReference gsr1 = new GSR("VPJPJ");
        gsr1.setProperty("modelId", "PJ");
        gsr1.setProperty("viewName", "VPJPJ");
        gsr1.setProperty("url", "jdbc:postgresql://localhost:5432/mimictut?user=postgres&password=postgres");
        //gsr1.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsr1.setProperty("schema", "mimictutorial");
        gsr1.setProperty("jsoncol", "p");
        final List<String> V1_JSONTcolNames = new ArrayList<String>();
        V1_JSONTcolNames.add("SUBJECT_ID");
        V1_JSONTcolNames.add("HADM_ID");
        final NRSMD V1_JSONTNRSMD =
                new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE },
                        V1_JSONTcolNames);
        final Map<String, Integer> V1_JSONTMapping = new HashMap<>();
        V1_JSONTMapping.put("SUBJECT_ID", 0);
        V1_JSONTMapping.put("HADM_ID", 1);
        ViewSchema V1_JSONTSchema = new ViewSchema(V1_JSONTNRSMD, V1_JSONTMapping);
        catalog.add("VPJPJ", gsr1, null, V1_JSONTSchema);

        //View VPJPR
        StorageReference gsrRE = new GSR("VPJPR");
        gsrRE.setProperty("modelId", "PR");
        gsrRE.setProperty("viewName", "VPJPR");
        gsrRE.setProperty("url", "jdbc:postgresql://localhost:5432/mimictut?user=postgres&password=postgres");
        //gsrRE.setProperty("url", "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres");
        gsrRE.setProperty("schema", "mimictutorial");
        final List<String> colNames = new ArrayList<String>();
        final Map<String, Integer> NRSMDMapping = new HashMap<>();
        colNames.add("SUBJECT_ID");
        colNames.add("DOB");
        colNames.add("GENDER");
        NRSMDMapping.put("SUBJECT_ID", 0);
        NRSMDMapping.put("DOB", 1);
        NRSMDMapping.put("GENDER", 2);
        NRSMD nrsmd = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
        ViewSchema vreschema = new ViewSchema(nrsmd, NRSMDMapping);
        catalog.add("VPJPR", gsrRE, null, vreschema);

    }

    /**
     * Storage reference for data in PostgreSQL (MIMIC)
     * 
     * @param collectionName
     * @return Postgres StorgeReference
     * @throws Exception
     */
    private static StorageReference getPostgresSQLStorageReferenceMIMIC(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimictut?user=postgres&password=postgres";
        //final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        gsr.setProperty("modelId", "PR");
        gsr.setProperty("schema", "mimictutorial");
        return gsr;
    }

    /**
     * Storage reference for Solr collections
     * 
     * @param collectionName
     * @return Solr StorgeReference
     * @throws Exception
     */
    private static StorageReference getSolrStorageReference(String collectionName) throws Exception {
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("serverUrl", SolrDatabaseLoader.DEFAULT_SOLR_URL);
        gsr.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);
        gsr.setProperty("coreName", collectionName);
        return gsr;

    }

}
