package fr.inria.oak.estocada.compiler.model.sm;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class SMModel extends Model {
	public final static String ID = "SM";

	@Inject
	public SMModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
		super(ID, Format.MATRIX, Language.DML, queryBlockTreeBuilder, blockEncoder);
	}
}
