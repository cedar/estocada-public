package fr.inria.oak.estocada.rewriting.decoder.pj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogPJSQLEval;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.rewriting.logical.Decoder;

/**
 * PJDecoder implements the decoder interface and returns LogPJQLEval operator.
 *
 * @author ranaalotaibi
 *
 */
public class PJDecoder implements Decoder {
    private static final Logger LOGGER = Logger.getLogger(PJDecoder.class);
    private String translatedQuery;

    @Override
    public LogOperator decode(final Catalog catalog, final ConjunctiveQuery conjunctiveQuery)
            throws TatooineExecutionException {
        final String viewName = conjunctiveQuery.getBody().stream().filter(atom -> atom.getTerms().size() == 1)
                .collect(Collectors.reducing((a, b) -> null)).get().getPredicate().toString();
        final NRSMD queryNRSMD = inferNRSDM(viewName, catalog, conjunctiveQuery);
        final PJTranslator pjTranslator = new PJTranslator(conjunctiveQuery, queryNRSMD);
        final String query = pjTranslator.translate();
        translatedQuery = query;
        LOGGER.info("Translated PJQL Query : " + query);
        return new LogPJSQLEval(query, queryNRSMD, catalog.getStorageReference(viewName));
    }

    /**
     * Get the schema of the query result
     * 
     * @param viewName
     *            the view name
     * @param catalog
     *            the catalog to get the nrsmd of the view
     * @param conjunctiveQuery
     *            the sub-rewriting
     * @return This function returns the nrsmd of the sub-rewriting result
     * @throws TatooineExecutionException
     */
    private NRSMD inferNRSDM(final String viewName, final Catalog catalog, final ConjunctiveQuery conjunctiveQuery)
            throws TatooineExecutionException {
        final NRSMD nrsmd = catalog.getViewSchema(viewName).getNRSMD();
        final List<NRSMD> nrsmds = new ArrayList<>();
        for (final Term term : conjunctiveQuery.getHead()) {
            for (final Atom atom : conjunctiveQuery.getBody()) {
                if (atom.getTerms().contains(term)) {
                    nrsmds.add(iterateOverNRSMD(atom.getTerm(2).toString().replace("\"", ""),
                            atom.getTerm(1).toString(), nrsmd));
                }
            }
        }

        return NRSMD.appendNRSMDList((nrsmds));
    }

    /**
     * Iterate over the nrsmd to get the column type since it could be under nested field
     * 
     * @param colName
     * @param inputNRSMD
     * @return
     * @throws TatooineExecutionException
     */
    private NRSMD iterateOverNRSMD(final String colName, final String aliasName, final NRSMD inputNRSMD)
            throws TatooineExecutionException {
        final List<String> colNames = Arrays.asList(inputNRSMD.getColNames());
        if (colNames.contains(colName)) {
            int colInx[] = new int[1];
            colInx[0] = inputNRSMD.getColIndexFromName(colName);
            final List<String> newColName = new ArrayList<>();
            newColName.add(aliasName);
            return new NRSMD(1, NRSMD.makeProjectRSMD(inputNRSMD, colInx).getColumnsMetadata(), newColName);
        } else {
            final TupleMetadataType[] tupleMetadataType = inputNRSMD.getColumnsMetadata();
            for (int i = 0; i < tupleMetadataType.length; i++) {
                if (tupleMetadataType[i].equals(TupleMetadataType.TUPLE_TYPE)) {
                    final NRSMD childNRSMD = inputNRSMD.getNestedChild(i);
                    return iterateOverNRSMD(colName, aliasName, childNRSMD);
                }
            }
            return null;
        }
    }

    /**
     * Get translated query
     * 
     * @return translatedQuery
     */
    public String tranlstedQuery() {
        return translatedQuery;
    }
}
