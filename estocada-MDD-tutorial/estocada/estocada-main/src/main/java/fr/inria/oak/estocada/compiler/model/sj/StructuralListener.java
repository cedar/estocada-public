package fr.inria.oak.estocada.compiler.model.sj;

import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * SJ Structural Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener(final PathExpressionListener pathExpressionlistener,
            @Named("SJQLVariableFactory") VariableFactory sjqlVariableFactory, VariableMapper variableMapper) {
        super(pathExpressionlistener, sjqlVariableFactory, variableMapper);
    }

    @Override
    public void enterSjQuery(SJQLParser.SjQueryContext ctx) {
        LOGGER.debug("Entering Query: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }
        final Variable var = sjqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.sjCollectionName().getText(), var);
        currentVar = var;
        final PathExpression expr = pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar);
        defineVariable(expr);
        for (Map.Entry<String, Variable> entry : variableMapper.getVariableMapping().entrySet()) {
            if (!entry.getKey().equals(ctx.sjCollectionName().getText())) {
                currentVar = entry.getValue();
                defineVariable(expr);
            }
        }
    }

    @Override
    protected ParserRuleContext createParseTree(final SJQLParser parser) {
        return parser.sjQuery();
    }
}
