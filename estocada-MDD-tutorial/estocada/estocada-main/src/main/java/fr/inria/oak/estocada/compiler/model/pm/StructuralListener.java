package fr.inria.oak.estocada.compiler.model.pm;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * PM Structural Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener(final PathExpressionListener pathExpressionlistener,
            @Named("MADLIBVariableFactory") VariableFactory dmlVariableFactory, VariableMapper variableMapper) {
        super(pathExpressionlistener, dmlVariableFactory, variableMapper);
    }

    @Override
    public void enterMadStatemnet(MADLIBParser.MadStatemnetContext ctx) {
        LOGGER.debug("Entering mad Statement Query: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }

        if (ctx.madExpression().getChildCount() == 7) {
            final Variable var = dmlVariableFactory.createFreshVar();
            variableMapper.define(ctx.madExpression().getChild(5).getText(), var);
            currentVar = var;
            final PathExpression expr = pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar);
            defineVariable(expr);
        } else {
            final Variable var = dmlVariableFactory.createFreshVar();
            variableMapper.define(ctx.madExpression().getChild(3).getText(), var);
            currentVar = var;
            final PathExpression expr = pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar);
            defineVariable(expr);
        }
    }

    @Override
    protected ParserRuleContext createParseTree(final MADLIBParser parser) {
        return parser.madScript();
    }
}
