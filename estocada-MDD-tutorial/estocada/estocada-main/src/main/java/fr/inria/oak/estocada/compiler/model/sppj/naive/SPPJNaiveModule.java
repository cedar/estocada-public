package fr.inria.oak.estocada.compiler.model.sppj.naive;

import fr.inria.oak.estocada.compiler.model.sppj.SPPJModule;

public class SPPJNaiveModule extends SPPJModule {
    @Override
    protected String getPropertiesFileName() {
        return "sppj.naive.properties";
    }
}
