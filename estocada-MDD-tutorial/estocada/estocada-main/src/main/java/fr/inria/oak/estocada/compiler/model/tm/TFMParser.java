// Generated from TFM.g4 by ANTLR 4.4

package fr.inria.oak.estocada.compiler.model.tm;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TFMParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__20=1, T__19=2, T__18=3, T__17=4, T__16=5, T__15=6, T__14=7, T__13=8, 
		T__12=9, T__11=10, T__10=11, T__9=12, T__8=13, T__7=14, T__6=15, T__5=16, 
		T__4=17, T__3=18, T__2=19, T__1=20, T__0=21, ID=22, WHITESPACE=23, STRING=24, 
		INT=25, MATRIX=26, ROWS=27, COLS=28, READ=29;
	public static final String[] tokenNames = {
		"<INVALID>", "'tf.norm('", "'tf.linalg.det('", "'tf.math.add('", "'import tensorflow as tf'", 
		"'tf.math.divide('", "'tf.linalg.adjoint('", "'tf.reduce_mean('", "'tf.math.matmul('", 
		"'tf.linalg.diag('", "':'", "'tf.linalg.trace('", "'='", "'tf.linalg.matrix_rank('", 
		"'tf.linalg.matrix_transpose('", "'matrix'", "'tf.linalg.inv('", "')'", 
		"'tf.math.scalar_mul('", "'tf.linalg.matmul('", "','", "'tf.math.subtract('", 
		"ID", "WHITESPACE", "STRING", "INT", "MATRIX", "ROWS", "COLS", "READ"
	};
	public static final int
		RULE_tfmQuery = 0, RULE_importstatement = 1, RULE_tfmScript = 2, RULE_tfmStatemnet = 3, 
		RULE_source = 4, RULE_tfmMatrixConstruction = 5, RULE_tfmMatrixConstructionMatrixSource = 6, 
		RULE_tfmExpression = 7, RULE_viewName = 8, RULE_matrixName = 9, RULE_matrixNameExpression = 10, 
		RULE_numericScalar = 11, RULE_filePath = 12;
	public static final String[] ruleNames = {
		"tfmQuery", "importstatement", "tfmScript", "tfmStatemnet", "source", 
		"tfmMatrixConstruction", "tfmMatrixConstructionMatrixSource", "tfmExpression", 
		"viewName", "matrixName", "matrixNameExpression", "numericScalar", "filePath"
	};

	@Override
	public String getGrammarFileName() { return "TFM.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TFMParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class TfmQueryContext extends ParserRuleContext {
		public ViewNameContext viewName() {
			return getRuleContext(ViewNameContext.class,0);
		}
		public TfmScriptContext tfmScript() {
			return getRuleContext(TfmScriptContext.class,0);
		}
		public ImportstatementContext importstatement() {
			return getRuleContext(ImportstatementContext.class,0);
		}
		public TfmQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterTfmQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitTfmQuery(this);
		}
	}

	public final TfmQueryContext tfmQuery() throws RecognitionException {
		TfmQueryContext _localctx = new TfmQueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_tfmQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26); viewName();
			setState(27); match(T__11);
			setState(28); importstatement();
			setState(29); tfmScript();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportstatementContext extends ParserRuleContext {
		public ImportstatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importstatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterImportstatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitImportstatement(this);
		}
	}

	public final ImportstatementContext importstatement() throws RecognitionException {
		ImportstatementContext _localctx = new ImportstatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_importstatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(31); match(T__17);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TfmScriptContext extends ParserRuleContext {
		public TfmStatemnetContext tfmStatemnet(int i) {
			return getRuleContext(TfmStatemnetContext.class,i);
		}
		public List<TfmStatemnetContext> tfmStatemnet() {
			return getRuleContexts(TfmStatemnetContext.class);
		}
		public TfmScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmScript; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterTfmScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitTfmScript(this);
		}
	}

	public final TfmScriptContext tfmScript() throws RecognitionException {
		TfmScriptContext _localctx = new TfmScriptContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_tfmScript);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33); tfmStatemnet();
			setState(37);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(34); tfmStatemnet();
				}
				}
				setState(39);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TfmStatemnetContext extends ParserRuleContext {
		public SourceContext source() {
			return getRuleContext(SourceContext.class,0);
		}
		public MatrixNameContext matrixName() {
			return getRuleContext(MatrixNameContext.class,0);
		}
		public TfmStatemnetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmStatemnet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterTfmStatemnet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitTfmStatemnet(this);
		}
	}

	public final TfmStatemnetContext tfmStatemnet() throws RecognitionException {
		TfmStatemnetContext _localctx = new TfmStatemnetContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_tfmStatemnet);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(40); matrixName();
			setState(41); match(T__9);
			setState(42); source();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceContext extends ParserRuleContext {
		public TfmMatrixConstructionContext tfmMatrixConstruction() {
			return getRuleContext(TfmMatrixConstructionContext.class,0);
		}
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public SourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterSource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitSource(this);
		}
	}

	public final SourceContext source() throws RecognitionException {
		SourceContext _localctx = new SourceContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_source);
		try {
			setState(46);
			switch (_input.LA(1)) {
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(44); tfmMatrixConstruction();
				}
				break;
			case T__20:
			case T__19:
			case T__18:
			case T__16:
			case T__15:
			case T__14:
			case T__13:
			case T__12:
			case T__10:
			case T__8:
			case T__7:
			case T__5:
			case T__3:
			case T__2:
			case T__0:
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(45); tfmExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TfmMatrixConstructionContext extends ParserRuleContext {
		public TfmMatrixConstructionMatrixSourceContext tfmMatrixConstructionMatrixSource() {
			return getRuleContext(TfmMatrixConstructionMatrixSourceContext.class,0);
		}
		public TfmMatrixConstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmMatrixConstruction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterTfmMatrixConstruction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitTfmMatrixConstruction(this);
		}
	}

	public final TfmMatrixConstructionContext tfmMatrixConstruction() throws RecognitionException {
		TfmMatrixConstructionContext _localctx = new TfmMatrixConstructionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_tfmMatrixConstruction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(48); tfmMatrixConstructionMatrixSource();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TfmMatrixConstructionMatrixSourceContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TFMParser.ID, 0); }
		public TfmMatrixConstructionMatrixSourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmMatrixConstructionMatrixSource; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterTfmMatrixConstructionMatrixSource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitTfmMatrixConstructionMatrixSource(this);
		}
	}

	public final TfmMatrixConstructionMatrixSourceContext tfmMatrixConstructionMatrixSource() throws RecognitionException {
		TfmMatrixConstructionMatrixSourceContext _localctx = new TfmMatrixConstructionMatrixSourceContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_tfmMatrixConstructionMatrixSource);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50); match(T__6);
			setState(51); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TfmExpressionContext extends ParserRuleContext {
		public TfmExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tfmExpression; }
	 
		public TfmExpressionContext() { }
		public void copyFrom(TfmExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MatrixAdjointExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixAdjointExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixAdjointExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixAdjointExpression(this);
		}
	}
	public static class MatrixNormExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixNormExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixNormExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixNormExpression(this);
		}
	}
	public static class MatrixIdentifierContext extends TfmExpressionContext {
		public MatrixNameExpressionContext matrixNameExpression() {
			return getRuleContext(MatrixNameExpressionContext.class,0);
		}
		public MatrixIdentifierContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixIdentifier(this);
		}
	}
	public static class MatrixAddExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext left;
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression(int i) {
			return getRuleContext(TfmExpressionContext.class,i);
		}
		public List<TfmExpressionContext> tfmExpression() {
			return getRuleContexts(TfmExpressionContext.class);
		}
		public MatrixAddExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixAddExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixAddExpression(this);
		}
	}
	public static class MatrixMulElementwiseExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext left;
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression(int i) {
			return getRuleContext(TfmExpressionContext.class,i);
		}
		public List<TfmExpressionContext> tfmExpression() {
			return getRuleContexts(TfmExpressionContext.class);
		}
		public MatrixMulElementwiseExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixMulElementwiseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixMulElementwiseExpression(this);
		}
	}
	public static class MatrixDiagonalExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixDiagonalExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixDiagonalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixDiagonalExpression(this);
		}
	}
	public static class MatrixDetExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixDetExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixDetExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixDetExpression(this);
		}
	}
	public static class MatrixMulExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext left;
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression(int i) {
			return getRuleContext(TfmExpressionContext.class,i);
		}
		public List<TfmExpressionContext> tfmExpression() {
			return getRuleContexts(TfmExpressionContext.class);
		}
		public MatrixMulExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixMulExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixMulExpression(this);
		}
	}
	public static class MatrixDivisionContext extends TfmExpressionContext {
		public TfmExpressionContext left;
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression(int i) {
			return getRuleContext(TfmExpressionContext.class,i);
		}
		public List<TfmExpressionContext> tfmExpression() {
			return getRuleContexts(TfmExpressionContext.class);
		}
		public MatrixDivisionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixDivision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixDivision(this);
		}
	}
	public static class MatrixTraceExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixTraceExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixTraceExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixTraceExpression(this);
		}
	}
	public static class MeanExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MeanExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMeanExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMeanExpression(this);
		}
	}
	public static class MatrixTransposeExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixTransposeExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixTransposeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixTransposeExpression(this);
		}
	}
	public static class MatrixRankExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixRankExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixRankExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixRankExpression(this);
		}
	}
	public static class MatrixSubExpressionContext extends TfmExpressionContext {
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression(int i) {
			return getRuleContext(TfmExpressionContext.class,i);
		}
		public List<TfmExpressionContext> tfmExpression() {
			return getRuleContexts(TfmExpressionContext.class);
		}
		public MatrixSubExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixSubExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixSubExpression(this);
		}
	}
	public static class MatrixMulScalarExpressionContext extends TfmExpressionContext {
		public NumericScalarContext left;
		public Token op;
		public TfmExpressionContext right;
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public NumericScalarContext numericScalar() {
			return getRuleContext(NumericScalarContext.class,0);
		}
		public MatrixMulScalarExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixMulScalarExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixMulScalarExpression(this);
		}
	}
	public static class MatrixInvExpressionContext extends TfmExpressionContext {
		public TfmExpressionContext tfmExpression() {
			return getRuleContext(TfmExpressionContext.class,0);
		}
		public MatrixInvExpressionContext(TfmExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixInvExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixInvExpression(this);
		}
	}

	public final TfmExpressionContext tfmExpression() throws RecognitionException {
		TfmExpressionContext _localctx = new TfmExpressionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_tfmExpression);
		try {
			setState(126);
			switch (_input.LA(1)) {
			case ID:
				_localctx = new MatrixIdentifierContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(53); matrixNameExpression();
				}
				break;
			case T__2:
				_localctx = new MatrixMulExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(54); match(T__2);
				setState(55); ((MatrixMulExpressionContext)_localctx).left = tfmExpression();
				setState(56); ((MatrixMulExpressionContext)_localctx).op = match(T__1);
				setState(57); ((MatrixMulExpressionContext)_localctx).right = tfmExpression();
				setState(58); match(T__4);
				}
				break;
			case T__13:
				_localctx = new MatrixMulElementwiseExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(60); match(T__13);
				setState(61); ((MatrixMulElementwiseExpressionContext)_localctx).left = tfmExpression();
				setState(62); ((MatrixMulElementwiseExpressionContext)_localctx).op = match(T__1);
				setState(63); ((MatrixMulElementwiseExpressionContext)_localctx).right = tfmExpression();
				setState(64); match(T__4);
				}
				break;
			case T__3:
				_localctx = new MatrixMulScalarExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(66); match(T__3);
				setState(67); ((MatrixMulScalarExpressionContext)_localctx).left = numericScalar();
				setState(68); ((MatrixMulScalarExpressionContext)_localctx).op = match(T__1);
				setState(69); ((MatrixMulScalarExpressionContext)_localctx).right = tfmExpression();
				setState(70); match(T__4);
				}
				break;
			case T__18:
				_localctx = new MatrixAddExpressionContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(72); match(T__18);
				setState(73); ((MatrixAddExpressionContext)_localctx).left = tfmExpression();
				setState(74); ((MatrixAddExpressionContext)_localctx).op = match(T__1);
				setState(75); ((MatrixAddExpressionContext)_localctx).right = tfmExpression();
				setState(76); match(T__4);
				}
				break;
			case T__16:
				_localctx = new MatrixDivisionContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(78); match(T__16);
				setState(79); ((MatrixDivisionContext)_localctx).left = tfmExpression();
				setState(80); ((MatrixDivisionContext)_localctx).op = match(T__1);
				setState(81); ((MatrixDivisionContext)_localctx).right = tfmExpression();
				setState(82); match(T__4);
				}
				break;
			case T__0:
				_localctx = new MatrixSubExpressionContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(84); match(T__0);
				setState(85); tfmExpression();
				setState(86); ((MatrixSubExpressionContext)_localctx).op = match(T__1);
				setState(87); ((MatrixSubExpressionContext)_localctx).right = tfmExpression();
				setState(88); match(T__4);
				}
				break;
			case T__7:
				_localctx = new MatrixTransposeExpressionContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(90); match(T__7);
				setState(91); tfmExpression();
				setState(92); match(T__4);
				}
				break;
			case T__10:
				_localctx = new MatrixTraceExpressionContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(94); match(T__10);
				setState(95); tfmExpression();
				setState(96); match(T__4);
				}
				break;
			case T__12:
				_localctx = new MatrixDiagonalExpressionContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(98); match(T__12);
				setState(99); tfmExpression();
				setState(100); match(T__4);
				}
				break;
			case T__15:
				_localctx = new MatrixAdjointExpressionContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(102); match(T__15);
				setState(103); tfmExpression();
				setState(104); match(T__4);
				}
				break;
			case T__19:
				_localctx = new MatrixDetExpressionContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(106); match(T__19);
				setState(107); tfmExpression();
				setState(108); match(T__4);
				}
				break;
			case T__5:
				_localctx = new MatrixInvExpressionContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(110); match(T__5);
				setState(111); tfmExpression();
				setState(112); match(T__4);
				}
				break;
			case T__8:
				_localctx = new MatrixRankExpressionContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(114); match(T__8);
				setState(115); tfmExpression();
				setState(116); match(T__4);
				}
				break;
			case T__20:
				_localctx = new MatrixNormExpressionContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(118); match(T__20);
				setState(119); tfmExpression();
				setState(120); match(T__4);
				}
				break;
			case T__14:
				_localctx = new MeanExpressionContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(122); match(T__14);
				setState(123); tfmExpression();
				setState(124); match(T__4);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ViewNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TFMParser.ID, 0); }
		public ViewNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_viewName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterViewName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitViewName(this);
		}
	}

	public final ViewNameContext viewName() throws RecognitionException {
		ViewNameContext _localctx = new ViewNameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_viewName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(128); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TFMParser.ID, 0); }
		public MatrixNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixName(this);
		}
	}

	public final MatrixNameContext matrixName() throws RecognitionException {
		MatrixNameContext _localctx = new MatrixNameContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_matrixName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(130); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixNameExpressionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TFMParser.ID, 0); }
		public MatrixNameExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixNameExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterMatrixNameExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitMatrixNameExpression(this);
		}
	}

	public final MatrixNameExpressionContext matrixNameExpression() throws RecognitionException {
		MatrixNameExpressionContext _localctx = new MatrixNameExpressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_matrixNameExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(132); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericScalarContext extends ParserRuleContext {
		public TerminalNode INT() { return getToken(TFMParser.INT, 0); }
		public NumericScalarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericScalar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterNumericScalar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitNumericScalar(this);
		}
	}

	public final NumericScalarContext numericScalar() throws RecognitionException {
		NumericScalarContext _localctx = new NumericScalarContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_numericScalar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(134); match(INT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilePathContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(TFMParser.STRING, 0); }
		public FilePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).enterFilePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TFMListener ) ((TFMListener)listener).exitFilePath(this);
		}
	}

	public final FilePathContext filePath() throws RecognitionException {
		FilePathContext _localctx = new FilePathContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_filePath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136); match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\37\u008d\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\7"+
		"\4&\n\4\f\4\16\4)\13\4\3\5\3\5\3\5\3\5\3\6\3\6\5\6\61\n\6\3\7\3\7\3\b"+
		"\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\5\t\u0081\n\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3"+
		"\16\3\16\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2\u0090\2\34\3"+
		"\2\2\2\4!\3\2\2\2\6#\3\2\2\2\b*\3\2\2\2\n\60\3\2\2\2\f\62\3\2\2\2\16\64"+
		"\3\2\2\2\20\u0080\3\2\2\2\22\u0082\3\2\2\2\24\u0084\3\2\2\2\26\u0086\3"+
		"\2\2\2\30\u0088\3\2\2\2\32\u008a\3\2\2\2\34\35\5\22\n\2\35\36\7\f\2\2"+
		"\36\37\5\4\3\2\37 \5\6\4\2 \3\3\2\2\2!\"\7\6\2\2\"\5\3\2\2\2#\'\5\b\5"+
		"\2$&\5\b\5\2%$\3\2\2\2&)\3\2\2\2\'%\3\2\2\2\'(\3\2\2\2(\7\3\2\2\2)\'\3"+
		"\2\2\2*+\5\24\13\2+,\7\16\2\2,-\5\n\6\2-\t\3\2\2\2.\61\5\f\7\2/\61\5\20"+
		"\t\2\60.\3\2\2\2\60/\3\2\2\2\61\13\3\2\2\2\62\63\5\16\b\2\63\r\3\2\2\2"+
		"\64\65\7\21\2\2\65\66\7\30\2\2\66\17\3\2\2\2\67\u0081\5\26\f\289\7\25"+
		"\2\29:\5\20\t\2:;\7\26\2\2;<\5\20\t\2<=\7\23\2\2=\u0081\3\2\2\2>?\7\n"+
		"\2\2?@\5\20\t\2@A\7\26\2\2AB\5\20\t\2BC\7\23\2\2C\u0081\3\2\2\2DE\7\24"+
		"\2\2EF\5\30\r\2FG\7\26\2\2GH\5\20\t\2HI\7\23\2\2I\u0081\3\2\2\2JK\7\5"+
		"\2\2KL\5\20\t\2LM\7\26\2\2MN\5\20\t\2NO\7\23\2\2O\u0081\3\2\2\2PQ\7\7"+
		"\2\2QR\5\20\t\2RS\7\26\2\2ST\5\20\t\2TU\7\23\2\2U\u0081\3\2\2\2VW\7\27"+
		"\2\2WX\5\20\t\2XY\7\26\2\2YZ\5\20\t\2Z[\7\23\2\2[\u0081\3\2\2\2\\]\7\20"+
		"\2\2]^\5\20\t\2^_\7\23\2\2_\u0081\3\2\2\2`a\7\r\2\2ab\5\20\t\2bc\7\23"+
		"\2\2c\u0081\3\2\2\2de\7\13\2\2ef\5\20\t\2fg\7\23\2\2g\u0081\3\2\2\2hi"+
		"\7\b\2\2ij\5\20\t\2jk\7\23\2\2k\u0081\3\2\2\2lm\7\4\2\2mn\5\20\t\2no\7"+
		"\23\2\2o\u0081\3\2\2\2pq\7\22\2\2qr\5\20\t\2rs\7\23\2\2s\u0081\3\2\2\2"+
		"tu\7\17\2\2uv\5\20\t\2vw\7\23\2\2w\u0081\3\2\2\2xy\7\3\2\2yz\5\20\t\2"+
		"z{\7\23\2\2{\u0081\3\2\2\2|}\7\t\2\2}~\5\20\t\2~\177\7\23\2\2\177\u0081"+
		"\3\2\2\2\u0080\67\3\2\2\2\u00808\3\2\2\2\u0080>\3\2\2\2\u0080D\3\2\2\2"+
		"\u0080J\3\2\2\2\u0080P\3\2\2\2\u0080V\3\2\2\2\u0080\\\3\2\2\2\u0080`\3"+
		"\2\2\2\u0080d\3\2\2\2\u0080h\3\2\2\2\u0080l\3\2\2\2\u0080p\3\2\2\2\u0080"+
		"t\3\2\2\2\u0080x\3\2\2\2\u0080|\3\2\2\2\u0081\21\3\2\2\2\u0082\u0083\7"+
		"\30\2\2\u0083\23\3\2\2\2\u0084\u0085\7\30\2\2\u0085\25\3\2\2\2\u0086\u0087"+
		"\7\30\2\2\u0087\27\3\2\2\2\u0088\u0089\7\33\2\2\u0089\31\3\2\2\2\u008a"+
		"\u008b\7\32\2\2\u008b\33\3\2\2\2\5\'\60\u0080";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}