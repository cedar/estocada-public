package fr.inria.oak.estocada.compiler.model.cg;

import static fr.inria.oak.estocada.compiler.model.cg.Utils.parseCypher;
import static fr.inria.oak.estocada.compiler.model.cg.Utils.readFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

public class Encoder {

    public String cypherQuery;
    public List<String> viewQueries;
    private ConjunctiveQuery cq;

    public Encoder(String queryOrFileString, List<String> viewFileNames, boolean isQuery) { // query can be read from a file or just as arg, but view can only be read from a file
        if (isQuery) {
            this.cypherQuery = queryOrFileString;
        } else {
            this.cypherQuery = readFile(queryOrFileString, true);
        }
        this.viewQueries = new ArrayList<>();
        for (String viewFileName : viewFileNames) {
            viewQueries.add(readFile(viewFileName, true));
        }
    }

    public static void execute(String filePath) {
        String queryFileName = filePath + "Ex_for_Rana/ex1/query.cypher";
        List<String> viewFileNames = new ArrayList<>(Arrays.asList(filePath + "Ex_for_Rana/ex1/view.cypher"));
        Encoder encoder = new Encoder(queryFileName, viewFileNames, false);
        String queryEncoding = encoder.encodeQuery();
        System.out.println(queryEncoding);
        List<Map<String, String>> viewsEncoding = encoder.encodeView();
        for (int i = 0; i < viewsEncoding.size(); i++) {
            System.out.println("[View " + (i + 1) + "]");
            Map<String, String> viewEncoding = viewsEncoding.get(i);
            for (String s : viewEncoding.keySet()) {
                System.out.println(s + "\t" + viewEncoding.get(s));
            }
        }
    }

    public String encodeQuery() {
        ParseTree tree = parseCypher(cypherQuery);
        CypherVisitor visitor = new CypherVisitorImpl();
        String queryEncoding = Constraint.genEncodedQuery((List<Object>) visitor.visit(tree));
        return queryEncoding;
    }

    public List<Map<String, String>> encodeView() {
        List<Map<String, String>> viewsEncoding = new ArrayList<>();
        for (String cypherQuery : viewQueries) {
            ParseTree tree = parseCypher(cypherQuery);
            CypherVisitor visitor = new CypherVisitorImpl();
            List<List<Object>> encoding = (List<List<Object>>) visitor.visit(tree);
            Map<String, String> viewEncoding = Constraint.genViewChaseEncodingText(copyEncoding(encoding));
            String viewBackchaseEncoding = Constraint.genViewBackchaseEncodingText(encoding);
            viewEncoding.put("bkchase", viewBackchaseEncoding);
            viewsEncoding.add(viewEncoding);
        }
        return viewsEncoding;
    }

    private List<List<Object>> copyEncoding(List<List<Object>> encoding) {
        List<List<Object>> copy = new ArrayList<List<Object>>();
        for (List<Object> atoms : encoding)
            copy.add((List<Object>) ((ArrayList<Object>) atoms).clone());
        return copy;
    }

    public static void main(String[] args) throws Exception {
        //        execute("ldbc-cypher/");
        execute("basicEx/ex_ret_entity1/");
    }
}
