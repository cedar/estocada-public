package fr.inria.oak.estocada.compiler.model.tq;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.StringElement;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * TQ BlockTreeListener
 * 
 * @author Rana Alotaibi
 * 
 */
@Singleton
final class BlockTreeListener {
    private static final Logger log = Logger.getLogger(BlockTreeListener.class);

    private final BlockListener blockListener;
    private final VariableFactory cqVariableFactory;
    private final VariableFactory tqlVariableFactory;
    private final VariableMapper variableMapper;

    /* Used to keep the root block of the query being parsed */
    private RootBlock root;

    @Inject
    public BlockTreeListener(final BlockListener blockListener,
            @Named("ConjunctiveQueryVariableFactory") final VariableFactory cqVariableFactory,
            @Named("TQLVariableFactory") final VariableFactory tqlVariableFactory,
            final VariableMapper variableMapper) {
        this.blockListener = blockListener;
        this.cqVariableFactory = cqVariableFactory;
        this.tqlVariableFactory = tqlVariableFactory;
        this.variableMapper = variableMapper;
    }

    public QueryBlockTree parse(final String str) throws ParseException {
        final TQLLexer lexer = new TQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TQLParser parser = new TQLParser(tokens);
        final ParserRuleContext tree = parser.tqlquery();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final BlockTreeListenerAux listener = new BlockTreeListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getQueryName() == null) {
            throw new ParseException(new IllegalStateException("Query name expected."));
        }
        if (listener.getRoot() == null) {
            throw new ParseException(new IllegalStateException("Root expected."));
        }
        if (!(listener.hasRootElement())) {
            root = blockListener.parse(listener.getQueryName(), listener.getRoot().getFlwr());
        }

        return new QueryBlockTree(root);
    }

    private class BlockTreeListenerAux extends TQLBaseListener {
        private BlockAux root;
        private BlockAux currentBlock;
        /* Used to keep the query name of the query being parsed */
        private String queryName;
        /* Used to keep the root element of the query being parsed */
        private StringElement rootElement;

        public BlockAux getRoot() {
            return root;
        }

        public String getQueryName() {
            return queryName;
        }

        public boolean hasRootElement() {
            return rootElement != null;
        }

        @Override
        public void enterTqlquery(TQLParser.TqlqueryContext ctx) {
            log.debug("Entering TQLQuery: " + ctx.getText());
            cqVariableFactory.reset();
            tqlVariableFactory.reset();
            variableMapper.reset();
        }

        @Override
        public void enterViewName(TQLParser.ViewNameContext ctx) {
            log.debug("Entering ViewName: " + ctx.getText());
            queryName = ctx.getText();
        }

        @Override
        public void enterQuery(TQLParser.QueryContext ctx) {
            log.debug("Entering query: " + ctx.getText());
            if (currentBlock == null) {
                root = new BlockAux(AntlrUtils.getFullText(ctx), null);
                currentBlock = root;
            }
        }

        @Override
        public void exitQuery(TQLParser.QueryContext ctx) {
            log.debug("Exiting query: " + ctx.getText());
            currentBlock = !currentBlock.hasParent() ? null : currentBlock.getParent();
        }
    }

    private class BlockAux {
        private final String sfw;
        private final BlockAux parent;

        public BlockAux(final String sfw, final BlockAux parent) {
            this.sfw = sfw;
            this.parent = parent;
        }

        public String getFlwr() {
            return sfw;
        }

        public boolean hasParent() {
            return parent != null;
        }

        public BlockAux getParent() {
            return parent;
        }

    }
}
