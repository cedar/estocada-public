package fr.inria.oak.estocada.views.materialization.module.SPPJ;

/** PR Supported Types **/
public enum SPPJDataType {

    INTEGER("integer"),
    VARCHAR("varchar");

    private final String dataType;

    private SPPJDataType(final String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return dataType;
    }
}
