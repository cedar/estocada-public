package fr.inria.oak.estocada.compiler;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Block Decoder
 * 
 * @author ranaalotaibi
 *
 */
@Deprecated
public interface BlockDecoder {
    String decode(final ConjunctiveQuery query, final ReturnTemplate template);
}
