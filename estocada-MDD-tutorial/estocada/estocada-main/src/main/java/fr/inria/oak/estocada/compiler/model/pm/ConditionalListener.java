package fr.inria.oak.estocada.compiler.model.pm;

import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
final class ConditionalListener extends MADLIBBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ConditionalListener.class);
    private final ConditionListener conditionListener;

    @Inject
    public ConditionalListener(final ConditionListener conditionListener) {
        this.conditionListener = conditionListener;
    }
}
