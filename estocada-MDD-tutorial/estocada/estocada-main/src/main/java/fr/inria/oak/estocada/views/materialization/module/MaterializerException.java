package fr.inria.oak.estocada.views.materialization.module;

import static com.google.common.base.Preconditions.checkNotNull;

import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;

/**
 * Used when an exception occurs during materializing data in a store.
 *
 * @author ranaalotaibi
 */
public final class MaterializerException extends EstocadaException {
    private static final long serialVersionUID = 1L;

    public MaterializerException(Exception e) {
        super(checkNotNull(e));
    }

    public MaterializerException(String str) {
        super(checkNotNull(str));
    }

}
