package fr.inria.oak.estocada.compiler.model.rk.naive;

import fr.inria.oak.estocada.compiler.model.rk.RKModule;

public class RKNaiveModule extends RKModule {
	@Override
	protected String getPropertiesFileName() {
		return "rk.properties";
	}
}
