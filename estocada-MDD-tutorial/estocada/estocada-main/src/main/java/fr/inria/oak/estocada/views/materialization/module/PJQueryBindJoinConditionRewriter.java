package fr.inria.oak.estocada.views.materialization.module;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.qbt.walker.QBTMBaseListener;
import fr.inria.oak.estocada.qbt.walker.QBTMLexer;
import fr.inria.oak.estocada.qbt.walker.QBTMParser;

/**
 * PJ Query BindJoin Condition Rewriter. Adds bind condition in the query
 * 
 * @author Rana Aloatibi
 *
 */
public class PJQueryBindJoinConditionRewriter {
    private static final Logger LOGGER = Logger.getLogger(PJQueryBindJoinConditionRewriter.class);
    private final String pjQuery;
    private final String variable;

    public PJQueryBindJoinConditionRewriter(final String pjQuery, final String variable) {
        this.pjQuery = pjQuery;
        this.variable = variable;

    }

    public String rewriteBindCondition() throws ParseException, TatooineExecutionException {
        final QBTMLexer lexer = new QBTMLexer(CharStreams.fromString(pjQuery));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTMParser parser = new QBTMParser(tokens);
        final ParserRuleContext tree = parser.pjQuery();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final PJListenerAux listener = new PJListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return listener.getNewQuery();
    }

    private class PJListenerAux extends QBTMBaseListener {
        private String varBinding;
        private String newQuery;
        private boolean whereClause;

        public PJListenerAux() {
            newQuery = null;
            varBinding = null;
            whereClause = false;
        }

        public String getNewQuery() {
            return newQuery;
        }

        @Override
        public void enterPjPathScalarLabel(QBTMParser.PjPathScalarLabelContext ctx) {
            LOGGER.debug("Entering PjPathScalarLabel: " + ctx.getText());
            if (variable.equals(ctx.pjSelectVar().getText()))
                varBinding = AntlrUtils.getFullText(ctx.pjPathScalar());

        }

        @Override
        public void enterPjCondition(QBTMParser.PjConditionContext ctx) {
            LOGGER.debug("Entering PjPathScalarLabel: " + ctx.getText());

        }

        @Override
        public void exitPjQuery(QBTMParser.PjQueryContext ctx) {
            LOGGER.debug("Existing PjQuery: " + ctx.getText());
            if (!whereClause) {
                newQuery = pjQuery + " WHERE " + varBinding + "=?";
            }
        }

    }
}
