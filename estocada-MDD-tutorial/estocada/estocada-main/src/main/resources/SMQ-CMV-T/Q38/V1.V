V3_JSONT = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID, A->'microbiologyevents' AS MICROBIOLOGYEVENTS
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'labevents') AS LABEVENTS
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   LABEVENTS->'flag' ="abnormal" AND
	   D->'Fluid'="Cerebrospinal Fluid (CSF)" }
	   
RETURN	 PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID, 'MICROBIOLOGYEVENTS', MICROBIOLOGYEVENTS) }      	
