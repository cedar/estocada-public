Q = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteevents') AS NOTEVEENTS,
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'microbiologyevents') AS MICROBIOLOGYEVENTS 
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
       LABEVENTS->'flag' ="abnormal" AND
	   D->'Fluid'="Cerebrospinal Fluid (CSF)" AND
	   MICROBIOLOGYEVENTS->'ab_name'='CEFEPIME' AND
	   P->'GENDER' ="M" AND
	   NOTEVEENTS->'TEXT'='tachypnea'} 

RETURN	SUBJECT_ID, DOB