mimicTutoText= FOR  PJ:{ 
SELECT P->'SUBJECT_ID' AS SUBJECT_ID, A->'HADM_ID' AS HADM_ID, NOTEVEENTS->'TEXT' AS TEXT 
FROM MIMIC AS P, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteevents') AS NOTEVEENTS}
	 
RETURN	 SJ:{ solr_json_build_object ( "SUBJECT_ID":SUBJECT_ID, "HADM_ID":HADM_ID, "TEXT":TEXT )}