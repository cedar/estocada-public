Q = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'microbiologyevents') AS MICROBIOLOGYEVENTS 
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   D->'Fluid'="Ascites" AND
	   MICROBIOLOGYEVENTS->'ab_name'='PENICILLIN' AND
	   P->'GENDER' ="M"} 

RETURN	SUBJECT_ID, DOB