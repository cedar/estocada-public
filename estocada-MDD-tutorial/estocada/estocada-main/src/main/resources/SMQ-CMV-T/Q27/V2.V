mimic_notes= FOR  PJ:{ 
SELECT P->'SUBJECT_ID' AS SUBJECT_ID,  NOTEVEENTS->'TEXT' AS TEXT 
FROM MIMIC AS P, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteevents') AS NOTEVEENTS}
	 
RETURN	 SJ:{ solr_json_build_object ("subject_id":SUBJECT_ID, "TEXT":TEXT )}