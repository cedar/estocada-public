VPJPJJSON = FOR PJ:{
FROM MIMIC AS P, MIMIC_D AS D, 
	JSONARRAYELEMENTS(P->'ADMISSIONS') AS A, 
	JSONARRAYELEMENTS(A->'labevents') AS LABEVENTS,
	JSONARRAYELEMENTS(A->'icustays') AS ICUSTAYS 
		
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   D->'Fluid'="Ascites"
	   
SELECT P->'SUBJECT_ID' AS SUBJECT_ID, ICUSTAYS->'prescriptions' AS PRESCRIPTIONS }
	   
RETURN	 PJ:{ JSONBUILDOBJECT ('SUBJECT_ID',SUBJECT_ID, 'PRESCRIPTIONS', PRESCRIPTIONS) }      	
