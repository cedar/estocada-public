Q= FOR  
SJ:{mimic/query?q=text:"tachypnea"&fl=id},
PR:{SELECT SUBJECT_ID FROM PatientsF AS P WHERE SUBJECT_ID=id AND P.gender="F"},
PJ:{FROM MIMIC AS P, MIMIC_D AS D,JSONARRAYELEMENTS(P->'ADMISSIONS') AS A, 
		JSONARRAYELEMENTS(A->'labevents') AS LABEVENTS, 
		JSONARRAYELEMENTS(A->'procedureevents_mv') AS PROCEDUREEVENTS_MV  
	WHERE D->'ITEMID'=LABEVENTS->'itemid' AND
		  LABEVENTS->'flag' ="abnormal" AND
		  D->'Fluid'= "Cerebrospinal Fluid (CSF)" AND
		  PROCEDUREEVENTS_MV->'ordercategoryname'="Invasive Lines" AND
		  PROCEDUREEVENTS_MV->'ordercategorydescription'="Task" AND
		  P->'SUBJECT_ID'=SUBJECT_ID
				  
	SELECT P->'SUBJECT_ID' AS PSUBJECT_ID }
	    

RETURN	PR:{ SUBJECT_ID }