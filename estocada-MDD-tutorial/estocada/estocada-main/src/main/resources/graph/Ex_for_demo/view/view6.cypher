match (country:Place {type: "Country", name: "United_States"})<-[:IS_PART_OF]-(city:Place {type: "City"})<-[:IS_LOCATED_IN]-(person:Person)
/*create (:Country_v6 {placeId:country.placeId, name:country.name, url:country.url, type:country.type})<-[:IS_PART_OF_v6]-(:City_v6 {placeId:city.placeId, name:city.name,
    url:city.url, type:city.type})<-[:IS_LOCATED_IN_v6]-(person)*/
create (country)<-[:IS_PART_OF_v6]-(:City_v6 {placeId:city.placeId, name:city.name, url:city.url, type:city.type})<-[:IS_LOCATED_IN_v6]-(person)