match (n:Person)-[:IS_LOCATED_IN]->(p:Place {name: "Los_Angeles"})
create (:Person_v1 {creationDate:n.creationDate, locationIP:n.locationIP, browserUsed:n.browserUsed, firstName:n.firstName, lastName:n.lastName, gender:n.gender,
    birthday:n.birthday})-[:IS_LOCATED_IN_v1]->(p)