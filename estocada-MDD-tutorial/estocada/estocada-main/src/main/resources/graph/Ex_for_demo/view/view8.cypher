match (tag:Tag {name: "Rumi"})<-[:HAS_TAG]-(message:Message)
create (:Tag_v8 {tagId:tag.tagId, name:tag.name, url:tag.url})<-[:HAS_TAG_v8]-(message)