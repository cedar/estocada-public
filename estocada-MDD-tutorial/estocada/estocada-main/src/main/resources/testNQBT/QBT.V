V=FOR PJ :{ 
			SELECT P->'SUBJECT_ID' AS SUBJECT_ID, A->'HADM_ID' AS HADM_ID, A->'microbiologyevents' AS MICROBIOLOGYEVENTS
	 		FROM MIMIC AS P, MIMIC_D AS D,
	 	 		 jsonb_array_elements(P->'ADMISSIONS') AS A, 
		         jsonb_array_elements(A->'labevents') AS LABEVENTS 
	        WHERE D->'ITEMID'=LABEVENTS->'itemid' AND
		         LABEVENTS->'flag' ="abnormal" AND
		         D->'Fluid'= "Cerebrospinal Fluid (CSF)"
     }
RETURN PJ : { jsonb_build_object ('SUBJECT_ID',SUBJECT_ID, 
								  'HADM_ID',HADM_ID, 
								  'MICROBIOLOGYEVENTS', 
											(SELECT JSONB_AGG(TO_JSONB(M))
											 FROM  (SELECT jsonb_build_object ('AB_NAME', MIC->'AB_NAME',
																			  'INTERPRETATION', MIC->'INTERPRETATION')
			    			  					  FROM MICROBIOLOGYEVENTS AS MIC) AS M)
			    					
     		}