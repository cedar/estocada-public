SELECT count(*)
FROM  "mimicJSON".mimic1 as D,
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS,
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'icustays') AS ICUSTAYS, 
      jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS

 
WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 	
      PRESCRIPTIONS->>'drug_type'='MAIN' AND 
      ITEMS.data@>'{"FLUID":"Ascites"}' AND
      PRESCRIPTIONS->>'drug'='Meropenem' AND  
      D.data@>'{"GENDER":"M"}'
