SELECT count(*)
FROM (SELECT D.data as data
	  FROM  "mimicJSON".mimic1 AS D
	  WHERE to_tsvector('English', D.data::text) @@ plainto_tsquery('English','respiratory failure')) AS D, "mimicJSON".d_labitems AS LI,
	  jsonb_array_elements(D.data->'ADMISSIONS') AS A,
	  jsonb_array_elements(A->'noteevents_0') AS T,
	  jsonb_array_elements(A->'labevents') AS LE

WHERE LI.data->'ITEMID'=LE->'itemid' AND
	  LI.data@>'{"FLUID":"Blood"}' AND
	  LI.data@>'{"CATEGORY":"Blood Gas"}' AND
	  LE->>'flag'='abnormal' AND
	  T->>'text' LIKE '%respiratory failure%';
