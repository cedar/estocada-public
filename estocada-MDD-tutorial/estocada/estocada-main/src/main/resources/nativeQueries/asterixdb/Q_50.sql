USE mimiciii;
SELECT count(*)
FROM mimic AS D, 
	 d_labitems LI, 
	 D.ADMISSIONS AS ADMISSIONS, 
	 ADMISSIONS.labevents AS LABEVENTS, 
	 ADMISSIONS.icustays AS ICUSTAYS,
	 ICUSTAYS.prescriptions AS PRESCRIPTIONS

WHERE LI.ITEMID=LABEVENTS.itemid AND
	  PRESCRIPTIONS.drug_type="MAIN" AND
	  LI.FLUID="Ascites" AND 
	  D.GENDER="F" AND
	  PRESCRIPTIONS.drug="Potassium Chloride";
