USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS ADMISSIONS, 
	   ADMISSIONS.labevents AS LABEVENTS,
	   ADMISSIONS.procedureevents_mv AS PROCEDUREEVENTS_MV,
	   ADMISSIONS.noteevents_0 AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LI.FLUID="Blood" AND 
	   LI.LABEL="Gentamicin" AND
	   PROCEDUREEVENTS_MV.ordercategoryname="Ventilation" AND
	   D.GENDER="M" AND
       N.text LIKE "%specis%";
	   