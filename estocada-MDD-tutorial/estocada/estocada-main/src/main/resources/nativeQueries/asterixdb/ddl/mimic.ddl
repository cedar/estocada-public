DROP DATAVERSE mimiciii IF EXISTS;
CREATE DATAVERSE mimiciii; 
-- Create dataset
USE mimiciii; 
CREATE TYPE mType AS { SUBJECT_ID: int };  
CREATE DATASET mimic(mType) 
PRIMARY KEY SUBJECT_ID;

-- Load 
USE mimiciii; 
LOAD DATASET mimic USING localfs 
(("path"="127.0.0.1:///tmp/mimic.json"), ("format"="json"));

-- Create dataset
USE mimiciii; 
CREATE TYPE lType AS { ITEMID: int };  
CREATE DATASET d_labitems(lType) 
PRIMARY KEY ITEMID;

-- Load
USE mimiciii; 
LOAD DATASET d_labitems USING localfs 
(("path"="127.0.0.1:///tmp/d_labitems.json"), ("format"="json"));

USE mimiciii;
CREATE index index_Label ON d_labitems (LABEL:string?) enforced;

CREATE index index_CATEGORY ON d_labitems (CATEGORY:string?) enforced;

CREATE index index_FLUID ON d_labitems (FLUID:string?) enforced;