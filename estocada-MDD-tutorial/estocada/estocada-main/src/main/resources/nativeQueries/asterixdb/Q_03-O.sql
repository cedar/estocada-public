USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS A, 
	   A.labevents AS LE,  
	   A.noteevents_0 AS N, 
	   A.microbiologyevents AS M

WHERE  LI.ITEMID=LE.itemid AND
	   LE.flag="abnormal" AND
	   LI.FLUID="Urine" AND 
	   M.ab_name="PENICILLIN" AND 
       N.text LIKE "%hematuria%";
	   