SELECT count(D.SUBJECT_ID)
FROM   d_labitems AS LI, mimic AS D LATERAL VIEW explode(D.ADMISSIONS) AS ADMISSIONS 
									LATERAL VIEW explode(ADMISSIONS.labevents) AS LABEVENTS 
									LATERAL VIEW explode(ADMISSIONS.microbiologyevents) AS MICROBIOLOGYEVENTS
									LATERAL VIEW explode(ADMISSIONS.procedureevents_mv) AS PROCEDUREEVENTS_MV
									LATERAL VIEW explode(ADMISSIONS.noteevents_0) AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LI.FLUID="Cerebrospinal Fluid (CSF)" AND 
	   LI.CATEGORY="Hematology" AND
	   MICROBIOLOGYEVENTS.ab_name="CEFEPIME" AND 
	   PROCEDUREEVENTS_MV.ordercategoryname="Peripheral Lines" AND
	   D.GENDER="F" AND
       N.text LIKE "%tachypnea%"
