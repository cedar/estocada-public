SELECT count(D.SUBJECT_ID)
FROM   d_labitems AS LI, mimic AS D LATERAL VIEW explode(D.ADMISSIONS) AS ADMISSIONS 
									LATERAL VIEW explode(ADMISSIONS.labevents) AS LABEVENTS 
									LATERAL VIEW explode(ADMISSIONS.microbiologyevents) AS MICROBIOLOGYEVENTS
									LATERAL VIEW explode(ADMISSIONS.noteevents_0) AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LABEVENTS.flag="abnormal" AND
	   LI.FLUID="Urine" AND 
	   MICROBIOLOGYEVENTS.ab_name="CEFEPIME" AND 
	   D.GENDER="M" AND
       N.text LIKE "%tachypnea%"