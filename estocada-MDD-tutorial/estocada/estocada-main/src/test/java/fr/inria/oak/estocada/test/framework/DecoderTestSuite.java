package fr.inria.oak.estocada.test.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.inria.oak.estocada.rewriting.decoder.aj.AJDecoderTest;
import fr.inria.oak.estocada.rewriting.decoder.pj.PJDecoderTest;
import fr.inria.oak.estocada.rewriting.decoder.rk.RKDecoderTest;
import fr.inria.oak.estocada.rewriting.decoder.sj.SJDecoderTest;
import fr.inria.oak.estocada.rewriting.decoder.sppj.SPPJDecoderTest;

/**
 * Decoders Test Suite.
 * PJ,SJ, and SPJJ
 * 
 * @author ranaalotaibi
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AJDecoderTest.class, RKDecoderTest.class, PJDecoderTest.class, SJDecoderTest.class,
        SPPJDecoderTest.class, })
public class DecoderTestSuite {

};
