package fr.inria.oak.estocada.rewriting.decoder.pr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import fr.inria.oak.estocada.compiler.model.encoder.pr.PRQueryBlockTreeCompilerTest;

/**
 * PJ Translator Test
 * 
 * @author Rana Alotaibi
 *
 */
public class PRDecoderTest {
    final static Logger LOGGER = Logger.getLogger(PRDecoderTest.class);
    final static String TEST_DIR = "/compiler/pr-decoder";

    /**
     * Get all tests directories
     * 
     * @return List<File>
     */
    private List<File> getTestsDirectories() {
        final File directory =
                new File(PRQueryBlockTreeCompilerTest.class.getResource(TEST_DIR).toString().substring(5));

        LOGGER.debug("Test home path: " + directory.toString());

        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    /**
     * Get test rewriting
     * 
     * @param testDirectory
     * @return file
     * @throws IOException
     */
    private File getTestRewriting(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("rewriting")) {
                return file;
            }
        }
        throw new IOException("Rewriting file not found.");
    }

    /**
     * Get test translated rewriting
     * 
     * @param testDirectory
     * @return file
     * @throws IOException
     */
    private File getTestTranslatedRewriitng(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("translatedrewriting")) {
                return file;
            }
        }
        throw new IOException("Translated Rewriting file not found.");
    }

    @Test
    public void test() {
        getTestsDirectories().stream().forEach(f -> test(f));
    }

    public void test(final File testDirectory) {

    }
}
