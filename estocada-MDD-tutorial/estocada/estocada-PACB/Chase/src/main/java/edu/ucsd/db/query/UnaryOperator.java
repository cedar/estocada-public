package edu.ucsd.db.query;

import java.util.LinkedList;

import edu.ucsd.db.canonicaldb.Tuple;

/**
 * @author Michalis Petropoulos
 * @author Yannis Katsis
 * 
 * Represents a relational operator having a single child operator.
 */
public abstract class UnaryOperator extends Operator {

	Operator childOp;

	LinkedList<Tuple> inputQueue = new LinkedList<Tuple>();
	
	private Tuple input;

	public UnaryOperator(Operator _childOp) {
		super();
		setChildOperator(_childOp);
	}

	/**
	 * Get the child operator of this operator.
	 * 
	 * @return the child operator
	 */
	public Operator childOperator() {
		return childOp;
	}

	/**
	 * Sets the argument as the current operator's child. It also sets the
	 * current operator as the argument's parent.
	 * 
	 * @param _childOp
	 */
	protected void setChildOperator(Operator _childOp) {
		childOp = _childOp;
		childOp.setParentOperator(this);
	}

	// TODO: Avoid copying the tuples from one list to another. Instead
	// if the input list is empty (i.e. there are no child tuples that have
	// not be consumed) instead of copying, get a pointer to the output
	// list of the child operator.

	/**
	 * Adds the output tuples returned by the child operator to the current
	 * operator's input queue.
	 */
	public void enqueue(LinkedList<Tuple> tuples, Object caller) {
		if (caller != childOp)
			throw new QueryEvaluationException("Enqueue operation in "
					+ this.getClass().getName() + " called by non-child object");
		inputQueue.addAll(tuples);
	}
	
	public void enqueue(Tuple tuple, Object caller) {
		if (caller != childOp)
			throw new QueryEvaluationException("Enqueue operation in "
					+ this.getClass().getName() + " called by non-child object");
		inputQueue.add(tuple);
	}
	
	public void enqueueSingle(Tuple tuple, Object caller) {
		if (caller != childOp)
			throw new QueryEvaluationException("Enqueue operation in "
					+ this.getClass().getName() + " called by non-child object");
		input = tuple;	
	}
	
	protected Tuple getInput() {
		Tuple result = input;
		input = null;
		
		return result;
	}

}
