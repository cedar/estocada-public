package edu.ucsd.db.query;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Vector;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This class implements the difference operator together with a projection
 * operator its output.
 */
public class Difference extends BinaryOperator {

	HashMap<String, Vector<Tuple>> leftTuples = new HashMap<String, Vector<Tuple>>();
	HashMap<String, Vector<Tuple>> rightTuples = new HashMap<String, Vector<Tuple>>();
	
	UnaryProjectComponent project;
	
	public Difference(Operator leftChildOp, Operator rightChildOp, CanonicalSchema outputSchema, LinkedHashMap<Integer, Value> newVars) {
		super(leftChildOp, rightChildOp);
		
		// Create a <code>ProjectComponent</code>
		CanonicalSchema childSchema = leftChildOp.getOutSchema();
		project = new UnaryProjectComponent(outputSchema, childSchema, newVars);
		
		outSchema = project.outSchema;
	}

	protected Tuple next() {
		Tuple inputTuple, tupleAfterProjection;
		String newlyInsertedTupleKey;
		
		// This works only for the first time and not when a new right tuple
		// is added, which should remove some other tuple from the output
		while (!rightInputQueue.isEmpty()) {
			inputTuple = rightInputQueue.removeFirst();
			insertRight(inputTuple);
		}
		
		while (!leftInputQueue.isEmpty()) {
			inputTuple = leftInputQueue.removeFirst();
			newlyInsertedTupleKey = insertLeft(inputTuple);
		
			if (!rightTuples.containsKey(newlyInsertedTupleKey)) {
				tupleAfterProjection = project.projectTuple(inputTuple);
				if (tupleAfterProjection != null)
					return tupleAfterProjection;
			}
		}
		
		return null;		
	}
	
	protected Tuple nextSingle() {
		Tuple left = getLeftInput();
		Tuple right = getRightInput();
		
		if (right != null) {
			insertRight(right);	
		}
		
		if (left != null) {
			String newlyInsertedTupleKey = insertLeft(left);
			
			if (!rightTuples.containsKey(newlyInsertedTupleKey))
				return left;
		}
		
		return null;
	}
	
	protected String insertLeft(Tuple tuple) {
		return insert(tuple, leftTuples);
	}

	protected String insertRight(Tuple tuple) {
		return insert(tuple, rightTuples);
	}
	
	// TODO: We calculate the key which might be calculated already in the projectComponent
	// Also we add to the key all output columns, which may also include constants and are thus not
	// required
	protected String insert(Tuple tuple,
			HashMap<String, Vector<Tuple>> hashTable) {
		StringBuffer keyBuf;
		Vector<Tuple> bucket;

		keyBuf = new StringBuffer();
		
		int outSchemaSize = outSchema.size();
		
		for (int i = 0; i < outSchemaSize; i++)
			keyBuf.append(tuple.getValue(i).getValueStr());

		String key = keyBuf.toString();

		// TODO: We add a tuple in the hash table even if the same tuple
		// already exists
		if (!hashTable.containsKey(key)) {
			bucket = new Vector<Tuple>();
			bucket.add(tuple);
			hashTable.put(key, bucket);
		} else {
			bucket = hashTable.get(key);
			bucket.add(tuple);
		}

		return key;
	}
	
	protected boolean hasEmptyInput() {
		return leftInputQueue.isEmpty() && rightInputQueue.isEmpty() ;
	}

	protected boolean isBlockedDefault() {
		return true;
	}
	
	protected StringBuffer printString() {
		StringBuffer retVal = new StringBuffer(project.printString());
		retVal.append(" Difference");
		
		return retVal;
	}

	// TODO: Instead of copying the output schema of the left iterator,
	// simply clone it, or use a copy constructor
	protected void setOutSchema() {
//
//		Iterator<String> leftColsIter = leftChildOp.getOutSchema().getColumnNames();
//		while (leftColsIter.hasNext()) {
//			outSchema.addColumnName((String) leftColsIter.next());
//		}
	}
	
	protected void flush() {
		project.flush();
		leftTuples.clear();
		rightTuples.clear();
		
		childOp1().flush();
		childOp2().flush();
	}

}
