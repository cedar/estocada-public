package edu.ucsd.db.query;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Tuple;

/**
 * Represents a physical relational operator.
 * 
 * The current implementation uses a push-based approach in query evaluation
 * where each operator computes its output and pushes them to the input queue of
 * its parent operator. However the implementation can be very easily converted
 * to a pull-based approach, where the parent operator asks its child operators
 * to provide tuples. To make this feasible, every operator has a function
 * <code>next()</code> returning its next output tuple. The two approaches can
 * be implemented by simply changing the caller of the function. If the operator
 * calls it itself, it leads to the push approach. On the other hand, if its
 * parent calls it during the evaluation of its results, then it leads to the
 * pull-based approach.
 */
public abstract class Operator {

	/**
	 * The schema of the tuples this operator outputs.
	 */
	CanonicalSchema outSchema = new CanonicalSchema();

	/**
	 * The parent of the current operator.
	 */
	Operator parentOp;

	/**
	 * Whether the operator outputs tuples now or not. It is used for operators,
	 * which can only output tuples when they know that their inputs are
	 * finalized (e.g. antisemijoin op).
	 */
	protected boolean blocked;
	
	protected boolean isTopOperator;
	
	protected boolean changed;

	/**
	 * Sets the schema of the tuples this operator outputs.
	 */
	protected abstract void setOutSchema();

	protected abstract StringBuffer printString();

	/**
	 * @return The default blocking status of the current type of operator
	 */
	protected abstract boolean isBlockedDefault();

	// Operators are build as non-top. To be top they have to be changed explicitly
	public Operator() {
		isTopOperator = false;
		blocked = isBlockedDefault();
		changed = false;
	}

	public void unblock() {
		blocked = false;
	}

	public void block() {
		blocked = true;
	}
	
	public void setTopOperator(boolean top) {
		isTopOperator = top;
	}

	/**
	 * Gets the schema of the tuples this operator outputs.
	 */
	public CanonicalSchema getOutSchema() {
		if (outSchema.isEmpty())
			setOutSchema();

		return outSchema;
	}

	/**
	 * Sets the argument as the current operator's parent.
	 * 
	 * @param _parentOp
	 */
	protected void setParentOperator(Operator _parentOp) {
		parentOp = _parentOp;
	}

	/**
	 * Gets the next <code>Tuple</code> produced by this operator.
	 * 
	 * @return a <code>Tuple</code>object
	 * @throws QueryEvaluationException
	 *             if there is no next <code>Tuple</code>
	 */
	protected abstract Tuple next();
	
	protected abstract Tuple nextSingle();

	// TODO: Should we instead use a relation (although it does duplicate
	// elimination)? If we do, we cannot get rid of the duplicate
	// elimination operator, because that keeps history of all tuples,
	// in contrast to the input queue that may have a subset of the tuples

	// Should we move the implementation of all here and have instead in
	// each operator only a method hasNext(), which returns true whenever
	// we have another input (in inputQueue or table)?
	/**
	 * Gets all next <code>Tuple</code> produced by this operator.
	 * 
	 * @return
	 */

	public abstract void enqueue(LinkedList<Tuple> tuples, Object caller);
	
	public abstract void enqueue(Tuple tuple, Object caller);
	
	public abstract void enqueueSingle(Tuple tuple, Object caller);

	protected abstract boolean hasEmptyInput();

	protected LinkedList<Tuple> all() {
//		if (blocked || hasEmptyInput())
//			return null;
		
		LinkedList<Tuple> output = new LinkedList<Tuple>();
		Tuple childTuple;

		if (!blocked) {
			while (!hasEmptyInput()) {
				childTuple = this.next();
				if (childTuple != null)
					output.add(childTuple);
				// output.addLast(childTuple);
			}
		}

		changed = false;
		
		return output;
	}
	
	/**
	 * Generates all output tuples and pushes them to its parent operator.
	 */

	// TODO: What are we doing with top-most operator?
	// See override
//	public void pushAll() {
//		if (isTopOperator)
//			return;
//			
//		LinkedList<Tuple> output = this.all();
//
//		if (!output.isEmpty()) {
//			parentOp.enqueue(output, this);
//			parentOp.pushAll();
//		}
//	}
	
	public void pushAll() {
		if (isTopOperator) {
			changed = true;
			return;
		}
		
		Tuple childTuple;
		boolean createdNewTuple = false; 

		if (!blocked) {
			while (!hasEmptyInput()) {	
				childTuple = this.next();
				if (childTuple != null) {
					parentOp.enqueue(childTuple, this);
					createdNewTuple = true;	
				}
				// output.addLast(childTuple);
			}
		}

		if (createdNewTuple) {
			parentOp.pushAll();
		}
	}
	
	public void pushSingle() {
		if (isTopOperator || blocked) {
			changed = true;
			return;	
		}
//		System.out.println("Operator: push single");
		Tuple childTuple = this.next();
		if (childTuple != null) {
			parentOp.enqueue(childTuple, this);
			parentOp.pushSingle();
		}
	}
	
	protected abstract void flush();

}
