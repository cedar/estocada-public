package edu.ucsd.db.query;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Value;

public class UnaryProjectComponent extends ProjectComponent {

	LinkedHashMap<Integer, Integer> newPosToOldPos;
	
	private boolean eliminateDuplicates;
	
	CanonicalSchema outSchema = new CanonicalSchema();
	CanonicalSchema childSchema;
	
	int outSchemaSize;
	
	// Auxiliary arrays
	boolean[] isNewValue;
	int[] oldPosition;
	Value[] newValue;

	public UnaryProjectComponent(CanonicalSchema outputSchema,
			CanonicalSchema childSchema, LinkedHashMap<Integer, Value> _newVars) {
		// TODO: Can we simply assign the entire canSchema to the outSchema?

		newPosToOldPos = new LinkedHashMap<Integer, Integer>();
		newPosToValue = new LinkedHashMap<Integer, Value>();

		this.childSchema = childSchema;

		Iterator<Entry<Integer, String>> outputSchemaIter = outputSchema
				.getEntries();

		// TODO: Do we gain anything from these structures?

		// Create output schema and auxiliary structures
		while (outputSchemaIter.hasNext()) {
			Entry<Integer, String> entry = outputSchemaIter.next();
			int pos = entry.getKey();
			String colName = entry.getValue();

			if (childSchema.hasColumnName(colName)) {
				newPosToOldPos.put(pos, childSchema.getPosition(colName));
				outSchema.addColumnName(colName);
			} else if (_newVars.containsKey(pos)) {
				newPosToValue.put(pos, _newVars.get(pos));
				outSchema.addColumnName(colName);
			} else
				throw new QueryEvaluationException("Cannot project in column "
						+ colName + ": Column does neither exist in the child "
						+ "operator, nor is it newly defined");
		}
		
		outSchemaSize = outputSchema.size();
		
		// Create auxiliary arrays to speed up projection
		createAuxArrays();
		
		// Set duplicate elimination
		eliminateDuplicates = needsDuplicateElimination();
		
		if (eliminateDuplicates)
			keySet = new HashSet<String>();
	}

	public Tuple projectTuple(Tuple childTuple) {
		String key;

		if (this.eliminateDuplicates) {
			key = getKeyAfterProjectionWithoutNewValues(childTuple);

			if (keySet.contains(key))
				return null;
			else
				keySet.add(key);
		}

		return createProjectedTuple(childTuple);
	}

	private String getKeyAfterProjectionWithoutNewValues(Tuple curTuple) {
		int oldPos;
		StringBuffer keyBuf = new StringBuffer();

		for (int pos = 0; pos < outSchemaSize; pos++) {
			if (!isNewValue[pos]) {
				oldPos = oldPosition[pos];
				keyBuf.append(curTuple.getValue(oldPos).getValueStr());
			}
		}

		return keyBuf.toString();
	}

	private Tuple createProjectedTuple(Tuple curTuple) {
		Tuple newTuple = new Tuple(outSchemaSize);
		
		for (int pos = 0; pos < outSchemaSize; pos++) {
			if (isNewValue[pos])
				newTuple.addValue(newValue[pos]);
			else
				newTuple.addValue(curTuple.getValue(oldPosition[pos]));
		}

		return newTuple;
	}
	
	protected boolean needsDuplicateElimination() {
		Iterator<String> childSchemaIter = childSchema.getColumnNames();

		while (childSchemaIter.hasNext()) {
			String childCol = childSchemaIter.next();
			if (!outSchema.hasColumnName(childCol))
				return true;
		}

		return false;
	}
	
	private void createAuxArrays() {
		isNewValue = new boolean[outSchemaSize];
		oldPosition = new int[outSchemaSize];
		newValue = new Value[outSchemaSize];
		
		int oldPos;
		Value value;
		
		for (int pos = 0; pos < outSchemaSize; pos++) {
			if (newPosToOldPos.containsKey(pos)) {
				oldPos = newPosToOldPos.get(pos);
				oldPosition[pos] = oldPos;
				isNewValue[pos] = false;
			} else if (newPosToValue.containsKey(pos)) {
				value = newPosToValue.get(pos);
				newValue[pos] = value;
				isNewValue[pos] = true;
			}
			else
				throw new QueryEvaluationException(
						"Cannot find column in position " + pos
								+ " of the output schema in either of the "
								+ "auxiliary data structures");
		}		
	}
	
	public String printString() {
		int pos;
		String colName;
		StringBuffer retVal = new StringBuffer();
		
		if (eliminateDuplicates)
			retVal.append("DuplicateElimination ");
		
		
		// Populate table with columns
		String[] columns = new String[outSchemaSize];
		
		Iterator<Entry<Integer, String>> outSchemaIter = outSchema.getEntries();
		
		while (outSchemaIter.hasNext()) {
			Entry<Integer, String> entry = outSchemaIter.next();
			pos = entry.getKey();
			colName = entry.getValue();
			
			if (isNewValue[pos])
				columns[pos] = newPosToValue.get(pos).getValueStr();
			else
				columns[pos] = colName;
		}
		
		// Print columns
		retVal.append("Project(");
		
		for (int i = 0; i < outSchemaSize - 1; i++) {
			retVal.append(columns[i]);
			retVal.append(",");
		}
		
		if (outSchemaSize > 0)
			retVal.append(columns[outSchemaSize - 1]);
		
		retVal.append(")");
		
		return retVal.toString();
	}
}
