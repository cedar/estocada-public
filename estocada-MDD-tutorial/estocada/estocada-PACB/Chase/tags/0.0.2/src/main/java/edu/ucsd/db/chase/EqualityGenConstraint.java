package edu.ucsd.db.chase;

import java.util.ArrayList;
import java.util.LinkedList;

import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.chaseexceptions.InconsistencyException;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Statement;
import edu.ucsd.db.datalogexpr.Value;
import edu.ucsd.db.query.QueryEvaluation;
import edu.ucsd.db.query.QueryTree;

/**
 * This class represents a constraint in the context of a
 * <code>CanonicalDB<code>, which generates an <code>Equality</code> 
 * when used by the <code>Chase</code>. Primary key constraints are 
 * of this type. Such constraint have a given premise and a conclusion 
 * that equates two <code>Value</code> objects.
 *
 */
public class EqualityGenConstraint extends Constraint {

	/**
	 * The premise of the constraint
	 */
	private Statement premise;

	/**
	 * The equalities that have to be enforced if the premise of
	 * the constraint is satisfied
	 */
	private ArrayList<Equality> equalities;
	
	/**
	 * The position in the query result of the left term for each equality
	 */
	private int[] leftTerms;

	/**
	 * The position in the query result of the right term for each equality
	 */
	private int[] rightTerms;
	
	/**
	 * The <code>QueryEvaluation</code> object used to evaluate the query
	 * corresponding to the constraint
	 */
	private QueryEvaluation queryEvaluation;
	
	public EqualityGenConstraint(Statement premise, ArrayList<Equality> equalities, Database db) {
		super(db);
		this.premise = premise;
		this.equalities = equalities;
		
		createEqualityArrays();
		createQueryEvaluation();
	}
	
	private EqualityGenConstraint(Database db) {
		super(db);
	}
	
	public static EqualityGenConstraint createCopyOfConstraint(EqualityGenConstraint oldEgd,
			Database db) {
		EqualityGenConstraint newEgd = new EqualityGenConstraint(db);
		
		newEgd.premise = oldEgd.premise;
		newEgd.equalities = oldEgd.equalities;
		newEgd.leftTerms = oldEgd.leftTerms;
		newEgd.rightTerms = oldEgd.rightTerms;
		
		newEgd.createQueryEvaluation();
		
		return newEgd;
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("\t\t");
		buf.append(premise);
		buf.append("\n\t\t           IMPLIES\n\t\t");
		
		buf.append("(");
		buf.append(equalities.get(0).getLeft().toString());
		buf.append(" = ");
		buf.append(equalities.get(0).getRight().toString());
		buf.append(")");
		
		for (int i = 1; i < equalities.size(); i++) {
			buf.append(" AND (");
			Equality curEquality = equalities.get(i);
			buf.append(curEquality.getLeft().toString());
			buf.append(" = ");
			buf.append(curEquality.getRight().toString());
			buf.append(")");
		}
		
		return buf.toString();
	}
	
	public ArrayList<Equality> getEqualities() {
		return equalities;
	}

	public Statement getPremise() {
		return premise;
	}
	
	private void createEqualityArrays() {
		Equality equality;
		
		Predicate queryHead = premise.head();
		int numOfEqualities = equalities.size();
		
		leftTerms = new int[numOfEqualities];
		rightTerms = new int[numOfEqualities];
		
		for (int i = 0; i < numOfEqualities; i++) {
			equality = equalities.get(i);
			leftTerms[i] = queryHead.variableIsAt(equality.getLeft());
			rightTerms[i] = queryHead.variableIsAt(equality.getRight());
		}
	}
	
	private void createQueryEvaluation() {
		QueryTree queryTree = QueryTree.CreateQueryTree(premise, db);
		queryEvaluation = new QueryEvaluation(queryTree);
	}
	
	@Override
	public boolean enforce() throws InconsistencyException {
		LinkedList<Tuple> queryAnswer = queryEvaluation.run();
		
		boolean changedInstance = false;
		int answerSize = queryAnswer.size();
		int numOfEqualities = leftTerms.length;
		
		for (int i = 0; i < answerSize; i++) {
	
			Tuple tuple = queryAnswer.removeFirst();
			
			for (int j = 0; j < numOfEqualities; j++) {
				Value left = tuple.getValue(leftTerms[j]);
				Value right = tuple.getValue(rightTerms[j]);
				String leftValue = left.getValueStr();
				String rightValue = right.getValueStr();
				
				//System.out.println("candidate "+leftValue+" and "+rightValue);
				if (!left.equalsByRepresentative(right)) {
					//System.out.println("equating "+leftValue+" and "+rightValue);
					boolean success = tuple.getValue(leftTerms[j]).equate(tuple.getValue(rightTerms[j]));
					if (!success) {
						throw new InconsistencyException(tuple.getValue(leftTerms[j]), tuple.getValue(rightTerms[j]), db);
					}
					//System.out.println("Equated: " + leftValue + " = " + rightValue + "\n");
					changedInstance = true;
				}
			}
		}
		
		return changedInstance;
	}
	
	@Override
	public void flush() {
		queryEvaluation.flush();
	}

}
