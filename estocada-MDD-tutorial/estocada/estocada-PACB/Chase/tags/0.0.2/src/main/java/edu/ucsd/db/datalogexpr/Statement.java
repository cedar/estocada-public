package edu.ucsd.db.datalogexpr;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import edu.ucsd.db.canonicaldb.CanonicalSchema;

/**
 * This class represents a query statement. This can represent a
 * conjunctive query with parentheses that specify the order in which
 * the joins have to be executed. It is a declarative query specification,
 * missing physical query evaluation information, such as projection
 * operators.
 */
public class Statement extends StatementOp {
	
	/**
	 * The query head
	 */
	private Predicate pred;
	
	/**
	 * The query body represented by a single operator (which can in
	 * turn have children)
	 */
	private StatementOp childOp;
	
	/**
	 * The list of predicates in the query body
	 */
	private ArrayList<Predicate> predicates;
	
	public Statement(Predicate _pred, StatementOp _childOp) {
		parent = null;
		pred = _pred;
		childOp = _childOp;
		childOp.parent = this;
	}

	protected void setAllAttribsUpToThisLevel() {
		allAttribsUpToThisLevel = union(
				childOp.getAllAttribsUpToThisLevel(),
				this.getRequiredAttribsByThisLevel());
	}

	protected void setRequiredAttribsByThisLevel() {
		Value val;
		
		requiredAttribsByThisLevel = new CanonicalSchema();
		
		for (int k = 0; k < pred.size(); k++) {
			val = pred.variableI(k);
			
			// TODO: We may get the same var multiple times (if we have join inside the same predicate)
			if (!val.isAnyConstant()) {
				requiredAttribsByThisLevel.addColumnName(val.getValueStr());
			}
		}
	}
	
	public StatementOp getChildOp() {
		return childOp;
	}
	
	public Predicate head() {
		return pred;
	}
	
	public String toString() {
		StringBuffer buffer = new StringBuffer(pred.toString());
		
		buffer.append(" :- ");
		buffer.append(childOp.toString());
		
		return buffer.toString();
	}
	
	public ArrayList<Predicate> getPredicates() {
		if (predicates == null)
			setPredicates();
		
		return predicates;
	}
	
	public void setPredicates() {
		predicates = new ArrayList<Predicate>();
		
		StatementOp curOp, prevOp;
		Stack<StatementOp> opStack = new Stack<StatementOp>();
		opStack.push(childOp);
		
		curOp = null;
		
		while (!opStack.isEmpty()) {
			prevOp = curOp;
			curOp = opStack.pop();
			if (curOp instanceof StatementScan)
				predicates.add(((StatementScan)curOp).getPredicate());
			else if (curOp instanceof StatementBinaryOp) {
				if (prevOp == ((StatementBinaryOp)curOp).childOp1) {
					opStack.push(curOp);
					opStack.push(((StatementBinaryOp)curOp).childOp2);
				}
				else if (prevOp != ((StatementBinaryOp)curOp).childOp2) {
					opStack.push(curOp);
					opStack.push(((StatementBinaryOp)curOp).childOp1);
				}	
			}
			// Add else throw exception
		}
	}

}
