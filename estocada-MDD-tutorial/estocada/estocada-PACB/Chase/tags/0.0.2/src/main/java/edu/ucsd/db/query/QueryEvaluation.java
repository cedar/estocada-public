package edu.ucsd.db.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Value;
import edu.ucsd.db.utilities.Profile;

/**
 * Takes as input a query tree and evaluates the corresponding query.
 */
public class QueryEvaluation {
	
	/**
	 * The query tree
	 */
	QueryTree queryTree;
	
	boolean flushed = false;


	/**
	 * Constructor.
	 * 
	 * @param _tree
	 * 				The query tree
	 */
	public QueryEvaluation(QueryTree _tree) {
		queryTree = _tree;
	}

	public LinkedList<Tuple> run() {
		// We have to unblock the root to get the result (if it is a blocking operator)
		
		if (flushed) {
			queryTree.initialize();
			flushed = false;
		}
		
		Operator root = queryTree.getRoot();
		boolean rootBlocked = root.blocked;
		root.unblock();
		
		LinkedList<Tuple> result = root.all();
		
		if (rootBlocked)
			root.block();
		
		return result;
	}
	
	public void flush() {
		queryTree.flush();
		flushed = true;
	}
	
	public boolean needsActivation() {
		return queryTree.getRoot().changed;
	}

	// TODO: Does it work on boolean queries (i.e. with empty head) and
	// what does it return?
	

	// TODO: If the query has constants in the head, at the last step the
	// produced tuples have to be extended by the constant at the right position
	// The same holds if the query has in the head the same variable
	// multiple times
	
	// TODO: Update comments
	
	// TODO: The static analysis has to change if we are not evaluating the
	// query through a left deep join tree

}