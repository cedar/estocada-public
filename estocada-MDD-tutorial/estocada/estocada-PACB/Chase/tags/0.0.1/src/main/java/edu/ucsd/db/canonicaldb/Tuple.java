package edu.ucsd.db.canonicaldb;

import edu.ucsd.db.datalogexpr.Value;
import edu.ucsd.db.query.QueryEvaluationException;

/**
 * This class represents a relational tuple.
 */
public class Tuple {
	
	/**
	 * The actual tuple represented by an array
	 */
	protected Value[] tuple;
	
	/**
	 * The last position inside the tuple to which a value has been assigned
	 */
	protected int lastPos = 0;
	
	/**
	 * If it is a fresh tuple, we have not yet put it in the canonical instance
	 * (i.e. it has been created by the suggestions generating algorithm)
	 */
	private boolean isFresh = false;

	/**
	 * The relational instance to which this tuple belongs
	 */
	protected Relation relation;
	
	/**
	 * Constructor.
	 */
	public Tuple(int size) {
		tuple = new Value[size];
	}

	/**
	 * Adds a <code>String</code> value to the end of the tuple.
	 * 
	 * @param val
	 *            The value to add
	 * @return True, if successful; false, otherwise
	 */
	public boolean addValue(Value val) {
		tuple[lastPos++] = val;
		
		// TODO: Check if not successful and return false
		return true;
	}
	
	/**
	 * Gets the <code>String</code> value at the given position.
	 * 
	 * @param pos
	 *            the position of the value to get
	 * @return a <code>Value</code> object
	 * @throws QueryEvaluationException
	 *             if the given position is out of range
	 */
	public Value getValue(int pos) { //throws QueryEvaluationException {
//		if (pos < 0 || pos >= lastPos)
//			throw new QueryEvaluationException("Tuple value not found: "
//					+ "specified position \"" + pos + "\" is out of range.");

		// TODO: Add error checking
		
		return tuple[pos];

	}

	/**
	 * Gets the arity of the tuple.
	 * 
	 * @return the arity of the tuple as an integer
	 */
	public int size() {
		return tuple.length;
	}
	
	public boolean isEmpty() {
		return lastPos == 0;
	}

	/**
	 * Clears all values in the tuple.
	 */
	public void clear() {
		lastPos = 0;
	}

	/**
	 * Prints the tuple for debugging purposes.
	 */
	public String toString() {
		String out = "Tuple: ";

		for (int i = 0; i < tuple.length; i++)
			out += "\t" + tuple[i].getValueStr();

		return out;
	}
	
	public void makeFresh() {
		isFresh = true;
	}

	public void makeNonFresh() {
		isFresh = false;
	}
	
	public boolean isFresh() {
		return isFresh;
	}
	
	public Relation relation() {
		return relation;
	}
}
