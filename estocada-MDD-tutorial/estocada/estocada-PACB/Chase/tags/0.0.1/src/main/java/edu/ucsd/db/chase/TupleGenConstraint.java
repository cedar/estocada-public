package edu.ucsd.db.chase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Relation;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Statement;
import edu.ucsd.db.datalogexpr.StatementOp;
import edu.ucsd.db.datalogexpr.Value;
import edu.ucsd.db.query.QueryEvaluation;
import edu.ucsd.db.query.QueryTree;

/**
 * This class represents a constraint in the context of a
 * <code>CanonicalDB<code>, which generates a <code>ConstraintTuple</code>
 * when used by the <code>Chase</code>. Foreign key and mapping 
 * constraints are of this type. Such constraints have a given 
 * premise and a conclusion, both being <code>Statement</code> objects.
 */
public class TupleGenConstraint extends Constraint {

	private Statement premise;

	private Statement conclusion;
	
	int predicateSize[];
	
	int valueHeadPos[][];
	
	int valueFreshPos[][];
	
	int valueConstantPos[][];
	
	//Value fresh[];
	
	ArrayList<Value> constant;
	
	boolean[] valueIsInHead[];
	
	boolean[] valueIsFresh[];
	
	Relation predicateRelation[];
	
	int freshCounter;
	
	//private QueryTree queryTree;
	
	private QueryEvaluation queryEvaluation;

	public TupleGenConstraint(Statement premise, Statement conclusion, Database db) {
		super(db);
		this.premise = premise;
		this.conclusion = conclusion;
		createQueryEvaluation();
		//createTables();
	}

	public static TupleGenConstraint createCopyOfConstraint(TupleGenConstraint oldTgd,
			Database db) {
		TupleGenConstraint newTgd = new TupleGenConstraint(oldTgd.premise, oldTgd.conclusion, db);
		return newTgd;
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("\t\t");
		buf.append(premise);
		buf.append("\n\t\t              IN\n\t\t");
		buf.append(conclusion);
		
		return buf.toString();
	}
	
	public Statement getPremise() {
		return premise;
	}

	public Statement getConclusion() {
		return conclusion;
	}
	
	public boolean needsActivation() {
		return queryEvaluation.needsActivation();
	}
	
	private void createQueryEvaluation() {
		QueryTree queryTree = QueryTree.CreateQueryTreeForDifference(premise, conclusion, db);
		queryEvaluation = new QueryEvaluation(queryTree);
	}
	
	public boolean enforce() {
		LinkedList<Tuple> queryAnswer = queryEvaluation.run();
		
//		if (queryAnswer == null)
//			return false;
		
		int size = queryAnswer.size();
		
		//Iterator<Tuple> tupleIter = queryAnswer.iterator();
		
		for (int i = 0; i < size; i++) {
			//createFreshInstanceOfQueryBody(conclusion, tupleIter.next());
			createFreshInstanceOfQueryBody(conclusion, queryAnswer.removeFirst(), true);
			//createFreshInstanceNew(queryAnswer.removeFirst());
		}
		
		return (size > 0);
	}
	
	public void flush() {
		queryEvaluation.flush();
	}
	
	private void createFreshInstanceNew(Tuple tuple) {
		// Create fresh values
		Value[] fresh = new Value[freshCounter];
		
		for (int i = 0; i < freshCounter; i++) {
			fresh[i] = Value.createFreshSkolem();
		}
		
		// For all predicates
		int numOfPreds = predicateSize.length;
		
		for (int i = 0 ; i < numOfPreds; i++) {
			int numOfValues = predicateSize[i];
			
			Tuple newTuple = new Tuple(numOfValues);
			
			for (int j = 0; j < numOfValues; j++) {
				Value newValue;
				
				if (valueIsInHead[i][j])
					newValue = tuple.getValue(valueHeadPos[i][j]);
				else if (valueIsFresh[i][j])
					newValue = fresh[valueFreshPos[i][j]];
				else
					newValue = constant.get(valueConstantPos[i][j]);
					
				newTuple.addValue(newValue);
			}
			
			predicateRelation[i].addTuple(newTuple);			
		}
	}
	
	private void createTables() {
		Tuple tuple;
		Value value, variable;
		int listSize, predSize;
		Predicate curPred;
		String varName;
		
		// Add to the hashtable the positions that correspond to the vars in the head
		HashMap<String, Integer> varsToHeadPos = new HashMap<String, Integer>();
		HashMap<String, Integer> varsToFreshPos = new HashMap<String, Integer>();
		
			Predicate queryHead = conclusion.head();
			int headSize = queryHead.size();
			
			for (int i = 0; i < headSize; i++) {
				variable = queryHead.variableI(i);
				
				if (!variable.isAnyConstant()) {
					varName = variable.getValueStr();
					varsToHeadPos.put(varName, i);
				}
			}
			
		ArrayList<Predicate> preds = conclusion.getPredicates();
		listSize = preds.size();
		
		// Create tables
		predicateSize = new int[listSize];
		predicateRelation = new Relation[listSize];
		
		valueHeadPos = new int[listSize][];
		valueFreshPos = new int[listSize][];
		valueConstantPos = new int[listSize][];
		valueIsInHead = new boolean[listSize][];
		valueIsFresh = new boolean[listSize][];
		
		constant = new ArrayList<Value>();
		
		int constantCounter = 0;
		freshCounter = 0;
		
		// Loop over all predicates
		for (int i = 0; i < listSize; i++) {
			curPred = preds.get(i);
			predSize = curPred.size();
			
			// Set properties of predicate
			predicateSize[i] = predSize;
			predicateRelation[i] = db.getRelationInstance(curPred.getFunctionHead());
			
			// Initialize multidimensional tables
			valueHeadPos[i] = new int[predSize];
			valueFreshPos[i] = new int[predSize];
			valueConstantPos[i] = new int[predSize];
			valueIsInHead[i] = new boolean[predSize];
			valueIsFresh[i] = new boolean[predSize];
			
			// For each variable in the predicate, add values
			for (int j = 0; j < predSize; j++) {
				value = curPred.variableI(j);
				
				if (value.isAnyConstant()) {
					valueIsInHead[i][j] = false;
					valueIsFresh[i][j] = false;
					constant.add(value);
					valueConstantPos[i][j] = constantCounter;
					constantCounter++;
				}
				else {
					varName = value.getValueStr();
					if (varsToHeadPos.containsKey(varName)) {
						valueIsInHead[i][j] = true;
						valueIsFresh[i][j] = false;
						valueHeadPos[i][j] = varsToHeadPos.get(varName);
					}
					else if (varsToFreshPos.containsKey(varName)){
						valueIsInHead[i][j] = false;
						valueIsFresh[i][j] = true;
						valueFreshPos[i][j] = varsToFreshPos.get(varName);
					}
					else
						valueIsInHead[i][j] = false;
						valueIsFresh[i][j] = true;
						valueFreshPos[i][j] = freshCounter;
						varsToFreshPos.put(varName, freshCounter);
						freshCounter++;
					}
				}
			}
		}
	}
	
//	public Statement toQuery() {
//		// TODO: What do we do about constants in the head of either query?
//		StatementOp body = new StatementAntiSemiJoin(premise.getBody(), conclusion.getBody());
//		Statement query = new Statement(premise.getHead(), body);
//		
//		return query;
//		
//	}
	
	// TODO: Add function to check that the two statements are
	// safe queries and have the same number of head variables
	// We can also check if they are unsatisfiable (e.g. if it
	// says that 3 has to be contained in 5)
	
	// TODO: Add function that "minimizes" the constraint. A
	// minimization is to convert constraints of the form
	// U(x,3) \subseteq V(x,y) to U(x,3) \subseteq V(x,3)
	
	// TODO: What are we doing about constraints of the form
	// U(x,y) /subseteq V(x,3)?
//	public Statement toQuery() {
//		Vector<Value> conclusionHeadVars = conclusion.getDistinguishedVariables();
//		Vector<Value> premiseHeadVars = premise.getDistinguishedVariables();
//		Hashtable<Value, String> newValues = new Hashtable<Value, String>();
//		
//		Predicate premiseHead = premise.getHead();
//		Predicate conclusionHead = conclusion.getHead();
//		
//		for (int i = 0; i < premiseHead.size() && i < conclusionHead.size(); i++) {
//			newValues.put(premiseHead.variableI(i).getValueStr(), conclusionHead.variableI(i).getValueStr());
//		}
//		
//		for (int i = 0; i < premise.)
//		
//		
//		
//		
//	}

