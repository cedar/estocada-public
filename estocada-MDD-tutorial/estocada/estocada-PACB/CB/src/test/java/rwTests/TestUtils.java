/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 */

package rwTests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Contains utility functions for the JUnit test classes.
 * 
 * @author Stamatis Zampetakis
 *
 */
public class TestUtils {
	
	/**
	 * Finds files within a given directory and its subdirectories. All files found are filtered by a FileFilter.
	 * 
	 * The behavior is similar with the listFiles method found in the Apache's FileUtils with the difference
	 * that if the a directory does not satisfy the filter the subdirectories are not excluded from the search.
	 *  
	 * @param dir - the directory to search in
	 * @param filter - filter to apply when finding files
	 * 
	 * @return an collection of java.io.File with the matching files
	 */
	public static Collection<File> listFiles(File dir,FileFilter filter){
		if(dir == null)
			throw new IllegalArgumentException("Parameter 'dir' cannot be null.");
		if(filter == null)
			throw new IllegalArgumentException("Parameter 'filter' cannot be null.");
		
 		List<File> files = new ArrayList<File>();
 		if(filter.accept(dir)){
 			files.add(dir);
 		}else if(dir.isDirectory()){
 			for(File subdir:dir.listFiles()){
 				files.addAll(listFiles(subdir,filter));
 			}
 		}
 		return files;	
 	}
	
	/**
	 * Asserts that two list of conjunctive queries are equivalent. 
	 * If they are not, an AssertionError is thrown with an appropriate message. 
	 * 
	 * @param expected - the expected value
	 * @param actual - the actual value
	 */
	public static void assertEquivalent(List<ConjunctiveQuery> expected,List<ConjunctiveQuery> actual){
		assertNotNull(expected);
		assertNotNull(actual);
			assertTrue("The number of returned rewrittings (" + actual.size()
					+ ") is"
					+ "different from the number of expected rewrittings ("
					+ expected.size() + ")", actual.size() == expected.size());

			// Since we want to remove queries from the list keep a copy
			// for providing meaningful debug information.
			List<ConjunctiveQuery> expectedCopy = new ArrayList<ConjunctiveQuery>(
					expected);

			// Check if each query in the results is equivalent with a
			// query in the expected results.
			for (ConjunctiveQuery q1 : actual) {
				ConjunctiveQuery found = null;
				for (ConjunctiveQuery q2 : expectedCopy) {
					if (q1.isEquivalent(q2)) {
						found = q2;
						break;
					}
				}
				assertNotNull("No equivalent query was found among queries "
						+ expected + " for query " + q1, found);
				expectedCopy.remove(found);
			}
		
	}
	
	/**
	 * Given a collection of elements wrap them arround an iterator of Object[] elements. Each element
	 * T in the collection will become an single element object array (Object[1]).
	 * 
	 * @param coll - the collection of input objects
	 * @return an iterator of Object[] elements
	 */
	public static <T> Iterable<Object[]> wrapAsSingleElements(Collection<T> coll){
		List<Object[]> result = new ArrayList<Object[]>();
		for(T t:coll){
			Object[] tmp = new Object[1];
			tmp[0] = t;
			result.add(tmp);
		}
		return result;
		
	}
 	

}