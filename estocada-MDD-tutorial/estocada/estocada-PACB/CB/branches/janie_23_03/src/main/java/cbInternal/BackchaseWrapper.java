/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package cbInternal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import atoms.AtomPositionTerm;
import atoms.DefEquality;
import atoms.DefRelAtom;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.IntegerConstant;
import fr.inria.oak.commons.conjunctivequery.StringConstant;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Egd;
import fr.inria.oak.commons.constraints.Equality;
import fr.inria.oak.commons.constraints.Tgd;

import instance.ChasedInstance;
import instance.Relation;


/**
 *  Wrapper class for the Backchase/reformulation engine
 */

public class BackchaseWrapper 
{
	/**
	 *  Runs the Backchase phase of Prov C&B 
	 *  
	 *  @param initquery
	 *  			  The query to be rewritten
	 *  @param uPlan
	 *  			  The universal plan
	 *  @param constraints
	 *  			  The set of constraints for the Backchase
	 *  
	 *  @return The reformulations
	 */
	public static ArrayList<ConjunctiveQuery> Backchase(ConjunctiveQuery initquery,
														ConjunctiveQuery uPlan,
														ArrayList<Constraint> bwconstraints) 
														throws Exception
	{
		
		
		
		
		ConjunctiveQuery uPlanAsInstance = addSpecialVars(uPlan);
		
		
	
		
		
		
		
		ArrayList<ConstraintWithEq> constraints = getConstraintsWithEq(bwconstraints);	
		BufferedWriter writer = new BufferedWriter(new FileWriter("bkchasetmp.in"));
		writer.write(uPlan.getBody().size()+1+"\n"); //for now, an additional symbol; should be revised
		//constraints
		writer.write(constraints.size()+"\n");

		for (int i = 0; i < constraints.size(); ++i)
		{
			ConstraintWithEq crtConstraint = constraints.get(i);

			if (crtConstraint instanceof TgdWithEq)
				writer.write("TGD\n");
			else
				writer.write("EGD\n");

			ArrayList<Atom> premRel = crtConstraint.getPremiseRel();
			for (int j = 0; j < premRel.size(); ++j)
			{
				Atom crtAtom = premRel.get(j);
				writer.write(toBackchaseInput(crtAtom));
				if (j < premRel.size()-1)
					writer.write(",");
			}
			if (premRel.size() == 0)
				writer.write("TRUE()");
			writer.write("\n");
		

			ArrayList<Equality> premEq = crtConstraint.getPremiseEq();
			for (int j = 0; j < premEq.size(); ++j)
			{
				Equality crtEq = premEq.get(j);
				writer.write(toBackchaseInput(crtEq));
				if (j < premEq.size()-1)
					writer.write(",");		
			}
			writer.write("\n");

			if (crtConstraint instanceof TgdWithEq)
			{
				ArrayList<Atom> concRel = ((TgdWithEq)crtConstraint).getConclusion();
				for (int j = 0; j < concRel.size(); ++j)
				{
					Atom crtAtom = concRel.get(j);
					writer.write(toBackchaseInput(crtAtom));
					if (j < concRel.size()-1)
						writer.write(",");	
				}
				writer.write("\n");
			}
			else
			{

				ArrayList<Equality> concEq = ((EgdWithEq)crtConstraint).getConclusion();
				for (int j = 0; j < concEq.size(); ++j)
				{
					Equality crtEq = concEq.get(j);
					writer.write(toBackchaseInput(crtEq));
					if (j < concEq.size()-1)
						writer.write(",");
				}
				writer.write("\n");
				
			}
		}

		
		//universal plan
		writer.write("instance\n");
		Collection<Atom> uPlanAtoms = uPlanAsInstance.getBody();
		int idx= 0;
		for (Atom crtAtom:uPlanAtoms) {
			writer.write(toBackchaseInput(crtAtom)+" p"+(idx++)+"\n");
		}
		writer.write("TRUE() true");

		writer.close();
		 
		ChasedInstance.Restart();
		ChasedInstance.zeInstance.readFromPABackChaseFile("bkchasetmp.in");
		ChasedInstance.zeInstance.PABackChase();
		
		
		
		
		
		ConjunctiveQuery queryAsInstance = addSpecialVars(initquery);
		CQWithEq query = new CQWithEq(queryAsInstance);
		instance.Query query_int = new instance.Query();

		ArrayList<DefRelAtom> premiseAtoms = new ArrayList<DefRelAtom>();
		ArrayList<DefEquality> premiseEqualities = new ArrayList<DefEquality>();
				
		for (int i = 0; i<query.getBodyRel().size(); ++i)
		{
			Atom oldAtom = query.getBodyRel().get(i);
			Relation crtRelation = ChasedInstance.zeInstance.getRelationByName(oldAtom.getPredicate());
			DefRelAtom crtAtom = new DefRelAtom(crtRelation, oldAtom.getTerms().size());
			premiseAtoms.add(crtAtom);
			int j = 0;
			for (Term term: oldAtom.getTerms()) {
					if (term.isConstant()) {
						crtAtom.m_terms[j] = new AtomPositionTerm(null,j);
						crtAtom.m_terms[j].m_constant = ChasedInstance.zeInstance.getSpecialTermByName("co."+term.toString().substring(1,term.toString().length()-1));
					}
					else {
						crtAtom.m_terms[j] = new AtomPositionTerm(crtAtom,j);
						query_int.m_premDef.m_varsToAtomPositions.put(((Variable)term).getName(), crtAtom.m_terms[j]);
					}
					j++;
			}
			System.out.println(crtAtom);
		}
		
		for (int i = 0; i<query.getBodyEq().size(); ++i)
		{
			Equality oldEq = query.getBodyEq().get(i);
			DefEquality crtEq = new DefEquality();
			crtEq.m_term1 = query_int.m_premDef.getTermFromString(((Variable)oldEq.getTerm1()).getName());
			crtEq.m_term2 = query_int.m_premDef.getTermFromString(((Variable)oldEq.getTerm2()).getName());
			premiseEqualities.add(crtEq);
			System.out.println(crtEq);

		}
	
		query_int.m_premDef.setContents(premiseAtoms, premiseEqualities);
		query_int.m_premDef.m_topJoinNode.m_parent = query_int;
		
		
	    ArrayList<DefRelAtom> conclusions = new ArrayList<DefRelAtom>();
		DefRelAtom conclusion = new DefRelAtom(ChasedInstance.zeInstance.getRelationByName(query.getHead().getPredicate()), query.getHead().getTerms().size());
		idx = 0;
		for (Term term: query.getHead().getTerms()) {
			String strValue = null;
			if (term.isVariable())
				strValue = ((Variable)term).getName();
			else
				strValue = "co."+term.toString();
			
			conclusion.m_terms[idx] = query_int.m_premDef.getTermFromString(((Variable)term).getName());
			if (conclusion.m_terms[idx] == null) //this is not a position in the premise, nor a constant
			{
					conclusion.m_terms[idx] = new AtomPositionTerm(null,-1);
					conclusion.m_terms[idx].m_skolemIndex = query_int.getSkolemIndex(strValue);
			}
			conclusions.add(conclusion);
			idx++;
		}
		
		query_int.setContents(conclusions);
		query_int.m_premDef.m_topJoinNode.displaySubtree(3);
		
		ChasedInstance.zeInstance.computeRewritings(query_int);
			
		
		
		ArrayList<ConjunctiveQuery> results = new ArrayList<ConjunctiveQuery>();
		if (query_int.m_provenance == null) //no rewritings
			return results; //empty list
		
		ArrayList<flatProvenance.FlatConjunct> rwconj = query_int.m_provenance.m_conjuncts;
		
		ArrayList<Atom> uplanAtoms = new ArrayList<Atom>();
		for (Atom tmp: uPlan.getBody())
			uplanAtoms.add(tmp);
		for (int i = 0; i< rwconj.size(); ++i)
		{
			ArrayList<Atom> rwAtoms = new ArrayList<Atom>();
			String conjStr = rwconj.get(i).toString();
			String[] symbols = conjStr.split(",");
			for (int j = 0; j< symbols.length; ++j)
				if (symbols[j].substring(0,1).equals("p")) {
					int index = Integer.parseInt(symbols[j].substring(1));
					rwAtoms.add(uplanAtoms.get(index));
				}
			ConjunctiveQuery rw = new ConjunctiveQuery("RW"+String.valueOf(i), uPlan.getHead(),rwAtoms);
			results.add(rw);
		}
		
		
		return results;
	}
	
	private static ConjunctiveQuery addSpecialVars(ConjunctiveQuery query)
	{
		HashSet<String> specialVars = new HashSet<String>();
		for (Term term: query.getHead()) if (term.isVariable())
			specialVars.add(((Variable)term).getName());
		ArrayList<Atom> newAtoms= new ArrayList<Atom>();
		for (Atom atom:query.getBody()) {
			ArrayList<Term> newTerms = new ArrayList<Term>();
			for (Term term: atom.getTerms()) 
				if (term.isVariable() && specialVars.contains(((Variable)term).getName()))
						newTerms.add(new StringConstant("$"+((Variable)term).getName())); 
				else newTerms.add(term);
			newAtoms.add(new Atom(atom.getPredicate(), newTerms));
		}
		return new ConjunctiveQuery(query.getName(), query.getHead(), newAtoms);
	}
	
	private static String toBackchaseInput(Atom atom) {
		String stringRep = atom.getPredicate()+"(";
		for (int i = 0; i < atom.getTerms().size(); ++i){
			Term crtTerm = atom.getTerm(i);
			if (crtTerm.isVariable())
				stringRep+=crtTerm;
			else {
				if (crtTerm instanceof StringConstant)
					stringRep+="co."+((StringConstant)crtTerm).getValue();
				else
					stringRep+="co."+((IntegerConstant)crtTerm).getValue();
			}
			if (i <  atom.getTerms().size()-1)
				stringRep+=",";
			else 
				stringRep+=")";
		}
		return stringRep;
	}
	
	private static String toBackchaseInput(Equality eq) {
		String stringRep="";
		Term crtTerm = eq.getTerm1();
		if (crtTerm instanceof Variable)
			stringRep+=eq.getTerm1();
		else{
			if (crtTerm instanceof StringConstant)
				stringRep+="co."+((StringConstant)crtTerm).getValue();
			else
				stringRep+="co."+((IntegerConstant)crtTerm).getValue();
		}
		stringRep+="=";

		crtTerm = eq.getTerm2();
		if (crtTerm instanceof Variable)
			stringRep+=eq.getTerm2();
		else{
			if (crtTerm instanceof StringConstant)
				stringRep+="co."+((StringConstant)crtTerm).getValue();
			else
				stringRep+="co."+((IntegerConstant)crtTerm).getValue();
		}
		return stringRep;
	}
	
	private static ArrayList<ConstraintWithEq> getConstraintsWithEq(ArrayList<Constraint> constraints) 
	{	
		ArrayList<ConstraintWithEq> result = new ArrayList<ConstraintWithEq>();
		for (Constraint constraint:constraints) {
			if (constraint instanceof Egd)
				result.add(new EgdWithEq((Egd)constraint));
			else
				result.add(new TgdWithEq((Tgd)constraint));
		}
		return result;
	}
	
	
}
