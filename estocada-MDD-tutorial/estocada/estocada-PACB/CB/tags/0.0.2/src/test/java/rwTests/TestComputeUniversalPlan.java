package rwTests;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import cbTool.ToolUtils;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
/**
 * The class contains the JUnits for reasoning about the correctness of the
 * computation of the Universal plan.
 *
 * It is a parameterized JUnit class which automatically detects all the valid
 * scenarios located in any subdirectory of the MAIN_DIR.
 * 
 * @author Stamatis Zampetakis
 *
 */
@RunWith(value = Parameterized.class)
public class TestComputeUniversalPlan {
	private static final File MAIN_DIR = new File(TestComputeUniversalPlan.class.getClassLoader().getResource("TestFiles").getFile());
	
	private String directory;
	public TestComputeUniversalPlan(File dir){
		this.directory=dir.getAbsolutePath();
	}
	
	/**
	 * The method is executed for any valid scenario directory.
	 */
	@Test
	public void runTest() {
		try {
			cbTool.ToolUtils.ChaseAndRestrict(directory);
		
			List<ConjunctiveQuery> queriesInFile1 = new ArrayList<ConjunctiveQuery>(ToolUtils.parseQueries(directory+"/results_chase"));
			List<ConjunctiveQuery> queriesInFile2 = new ArrayList<ConjunctiveQuery>(ToolUtils.parseQueries(directory+"/expectedresults_chase"));
			
			TestUtils.assertEquivalent(queriesInFile2, queriesInFile1);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @return all the valid scenario directories wrapped around an
	 * Iterable object as it is required by the annotation pattern.
	 */
	@Parameters(name= "{index}: testScenario({0})")
 	public static Iterable<Object[]> data() { 		
		Collection<File> validFiles = TestUtils.listFiles(
				MAIN_DIR,
				new DirectoryContentFilter(Arrays.asList("query",
						"expectedresults_chase", "constraints_chase",
						"schemas", "views")));
 		
		return TestUtils.wrapAsSingleElements(validFiles);
 	}
 	
 	
}
