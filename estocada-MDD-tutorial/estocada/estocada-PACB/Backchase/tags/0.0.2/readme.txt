A. Execution

1. Argument = input file name, which will be searched in the project directory. 
2. Output is in inputfilename.results, in the project directory

B. Structure of the input file

1) Description of the universal plan (implicit eqs)
	- string "universal plan"
	- number of atoms
	- atoms (one /line)
	
2) Description of query WITH EXPLICIT EQUALITIES
    - string "query"
    - one line for its relational atoms
    - one line for the equality atoms
    - one line for the head
    
3) Constraints:
	- string "constraints"
	- number of constraints
	- for each constraint
		- string EGD or TGD
		- premise on two lines WITH EXPLICIT EQUALITIES first relational atoms, then equalities
		- conclusion on one line (implicit eqs if TGD)
		
C. Structure of the output file
One line/rewriting available. Rewriting is expressed as a subquery (subset of atoms) of the provided universal plan.
Note that only bodies are outputted for now, the head being that of the query and U.
 	
D. Sample files: 
Input rwtest1.txt + its corresponding output rwtest1.txt.results