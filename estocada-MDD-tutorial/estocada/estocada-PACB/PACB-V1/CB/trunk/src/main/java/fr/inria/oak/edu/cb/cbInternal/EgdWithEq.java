/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package fr.inria.oak.edu.cb.cbInternal;

import java.util.ArrayList;

import fr.inria.oak.commons.constraints.Egd;
import fr.inria.oak.commons.constraints.Equality;

/**
 *  An EGD with explicit equalities
 */
public class EgdWithEq extends ConstraintWithEq {

	private ArrayList<Equality> conclusion;
	
	/**
	 *  Constructor
	 *  
	 *  @param egd
	 *  		The EGD without equalities
	 */
	public EgdWithEq(final Egd egd)
	{	
		super();
		Utils.FromBodyToBodyWithEq(egd.getPremise(), premiseRel, premiseEq);
		conclusion = new ArrayList<Equality>();
		for (Equality oldEq:egd.getConclusion()) {
			Equality newEq = Utils.GetTransformedEquality(oldEq, egd.getPremise(), premiseRel);
			conclusion.add(newEq);
		}
	}
	
	/**
	 *  Gets the conclusion of the EGD
	 *  
	 *  @return the conclusion of the EGD
	 */
	public ArrayList<Equality> getConclusion() {
		return conclusion;
	}
	
	@Override
	public String toString() {
		return Utils.fromAtomsToString(premiseRel)+","+Utils.fromEqsToString(premiseEq)+"->"+Utils.fromEqsToString(conclusion);
	}
	
}