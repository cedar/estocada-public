/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package fr.inria.oak.edu.cb.cbInternal;

import java.util.ArrayList;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.constraints.False;

/**
 * A Denial constraint with explicit equalities
 */
public class DenialWithEq extends ConstraintWithEq {
    /**
     * Constructor
     * 
     * @param denial
     *            The denial without equalities
     */
    public DenialWithEq(final False denial) {
        super();
        Utils.FromBodyToBodyWithEq(denial.getPremise(), premiseRel, premiseEq);
    }

    /**
     * Gets the Deianl's conclusion
     * 
     * @return the TGD's conclusion
     */
    public ArrayList<Atom> getConclusion() {
        return new ArrayList<>();
    }

    @Override
    public String toString() {

        final StringBuilder str = new StringBuilder();
        str.append(Utils.fromAtomsToString(premiseRel));
        str.append(",");
        str.append(Utils.fromEqsToString(premiseEq));
        str.append("->");
        str.append("false");
        return str.toString();
    }
}
