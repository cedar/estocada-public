########Example 2#########
V=tr(tr(M1))+0; Q= M1;

##########Query########### 
Q<m1>:-  name(m1,"M1");

##########Forward Constraints########### 
#EGDs
transp(o1,o2),transp(o1,o3)->o2=o2;
transp(o1,o2),transp(o2,o3)->o1=o3;
add(id1,id2,id3),add(id1,id2,id4)->id3=id4;
add(id1,o,id2),Zero(o)->id1=id2;

#TGDs
name(id1,n1)->transp(id1,id2), transp(id2,id3);
add(id1,id2,id3)-> add(id2,id1,id3);
name(id1,n1)->add(id1,o,id2),Zero(o);

#View

name(m1,"M1"),
Zero(o),
transp(m1,o1),
transp(o1,o2),
add(o2,o,o3)->name(o3,"V");

##########Backward Constraints########### 
#EGDs
transp(o1,o2),transp(o1,o3)->o2=o2;
transp(o1,o2),transp(o2,o3)->o1=o3;
add(id1,id2,id3),add(id1,id2,id4)->id3=id4;
add(id1,o,id2),Zero(o)->id1=id2;

#TGDs
name(id1,n1)->transp(id1,id2), transp(id2,id3);
add(id1,id2,id3)-> add(id2,id1,id3);
name(id1,n1)->add(id1,o,id2),Zero(o);

#View

name(o3,"V")->
name(m1,"M1"),
Zero(o),
transp(m1,o1),
transp(o1,o2),
add(o2,o,o3);

##########Schemas########### 
# All
transp(2),add(3),name(2);

#Target
transp(2),add(3),name(2);

######Chased Query##########

Q_C<m1> :- add(m1,SK_2,m1),add(SK_2,m1,m1),Zero(SK_2),
		   name(m1,"M1"),name(m1,"V"),transp(m1,SK_0),
		   transp(SK_0,m1);

######Universal Plan########

U<m1> :- add(m1,SK_2,m1),add(SK_2,m1,m1),Zero(SK_2),
		 name(m1,"M1"),name(m1,"V"),transp(m1,SK_0),
		 transp(SK_0,m1);

#########Rewriting##########

RW0<m1> :- name(m1,"M1");
RW1<m1> :- name(m1,"V");




