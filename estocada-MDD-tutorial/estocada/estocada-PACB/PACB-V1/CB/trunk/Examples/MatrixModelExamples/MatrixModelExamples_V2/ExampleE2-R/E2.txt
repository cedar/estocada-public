########Example 2#########

V=tr(tr(M2))+M1; Q= M1+M2;

##########Query########### 
Q<m3>:- name(m1,"M1"),name(m2,"M2"),add(m1,m2,m3);

##########Forward Constraints########### 
#EGDs
transp(o1,o2),transp(o1,o3)->o2=o2;
transp(o1,o2),transp(o2,o3)->o1=o3;
add(id1,id2,id3),add(id1,id2,id4)->id3=id4;

#TGDs
name(id1,n1)->transp(id1,id2), transp(id2,id3);
add(id1,id2,id3)-> add(id2,id1,id3);

#View
name(m1,"M1"),
name(m2,"M2"),
transp(m2,o1),
transp(o1,o2),
add(o2,m1,o3)-> name(o3,"V");

##########Backward Constraints########### 
#EGDs
transp(o1,o2),transp(o1,o3)->o2=o2;
transp(o1,o2),transp(o2,o3)->o1=o3;
add(id1,id2,id3),add(id1,id2,id4)->id3=id4;

#TGDs
M(id1,n1)->transp(id1,id2), transp(id2,id3);
add(id1,id2,id3)-> add(id2,id1,id3);


#View
name(o3,"V")->
name(m1,"M1"),
name(m2,"M2"),
transp(m2,o1),
transp(o1,o2),
add(o2,m1,o3);

##########Schemas########### 
# All
transp(2),add(3),name(2);

#Target
transp(2),add(3),name(2);

######Chased Query##########
Q_C<m3> :- add(m1,m2,m3),add(m2,m1,m3),name(m1,"M1"),name(m2,"M2"),name(m3,"V"),
		   transp(m1,SK_0),transp(SK_0,m1),transp(m2,SK_2),transp(SK_2,m2),
		   transp(m3,SK_4),transp(SK_4,m3);

######Universal Plan########

U<m3> :- add(m1,m2,m3),add(m2,m1,m3),name(m1,"M1"),name(m2,"M2"),name(m3,"V"),
		 transp(m1,SK_0),transp(SK_0,m1),transp(m2,SK_2),transp(SK_2,m2),
		 transp(m3,SK_4),transp(SK_4,m3);

#########Rewriting##########

RW0<m3> :- add(m1,m2,m3),name(m1,"M1"),name(m2,"M2");
RW1<m3> :- add(m2,m1,m3),name(m1,"M1"),name(m2,"M2");
RW2<m3> :- name(m3,"V");



