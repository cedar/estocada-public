/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package cbTool;

public class Main {

	public static void main(String args[]) throws Exception {
		if (args[0].equals("RW"))
				ToolUtils.ComputeRewritings(args[1]);
		else if (args[0].equals("RWFIN"))
			ToolUtils.ComputeRewritingsFinal(args[1]);
	}
	
}
