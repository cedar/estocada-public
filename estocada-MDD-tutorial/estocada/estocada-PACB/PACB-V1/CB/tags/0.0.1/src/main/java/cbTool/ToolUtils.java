/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */
package cbTool;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import cbAPI.CBProvider;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.parser.CQParser;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Utilities;
import fr.inria.oak.commons.constraints.parser.ConstraintParser;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.commons.relationalschema.parser.RelSchemaParser;

public class ToolUtils {
	
	/**
	 *  Computes the rewritings for an input configuration, outputs results in inputDir/results
	 *  
	 *  @param inputDir
	 *  			  The directory holding the input configuration
	 */
	public static void ComputeRewritings(String inputDir) throws Exception
	{
		//parse query
		FileReader fr = new FileReader(inputDir+"/"+"query");
		CQParser cqparser = new CQParser(fr);
		ConjunctiveQuery query = null;
		Map<String,ConjunctiveQuery> parsedCQs = cqparser.parse();
		for (String s:parsedCQs.keySet()) 
			query = parsedCQs.get(s);
		fr.close();
		
		//parse views
		fr = new FileReader(inputDir+"/"+"views");
		cqparser = new CQParser(fr);
		ArrayList<ConjunctiveQuery> views = new ArrayList<ConjunctiveQuery>();
		parsedCQs = cqparser.parse();
		for (String s:parsedCQs.keySet()) 
			views.add(parsedCQs.get(s));
		fr.close();
		
		//parse constraints applicable for chase
		fr = new FileReader(inputDir+"/"+"constraints_chase");
		ConstraintParser consparser = new ConstraintParser(fr);
		ArrayList<Constraint> constraints_chase = consparser.parse();
		fr.close();
	
		//parse constraints applicable for backchase
		fr = new FileReader(inputDir+"/"+"constraints_bkchase");
		consparser = new ConstraintParser(fr);
		ArrayList<Constraint> constraints_bkchase = consparser.parse();
		fr.close();
				
		//parse schemas
		fr = new FileReader(inputDir+"/"+"schemas");
		RelSchemaParser schparser = new RelSchemaParser(fr);
		ArrayList<RelationalSchema> schemas = schparser.parse();
		fr.close();
		
		ArrayList<Constraint> fwconstraints = new ArrayList<Constraint>();
		ArrayList<Constraint> bwconstraints = new ArrayList<Constraint>();
		fwconstraints.addAll(constraints_chase);
		bwconstraints.addAll(constraints_bkchase);
		
		Pair<List<Constraint>, List<Constraint>> constrFromViews=Utilities.getFWandBWConstraints(views);
		fwconstraints.addAll(constrFromViews.getLeft());
		bwconstraints.addAll(constrFromViews.getRight());
		
		ArrayList<ConjunctiveQuery> rws = CBProvider.getReformulations(query, schemas.get(0), schemas.get(1), fwconstraints, bwconstraints);
		
	    PrintWriter out = new PrintWriter(new FileWriter(inputDir+"/"+"results"));
	    for (ConjunctiveQuery rw: rws)
	    	out.println(cbInternal.Utils.fromQueryToString(rw));
	    out.close();
	}
	
	/**
	 *  Chases and restricts for an input configuration, outputs results in inputDir/results_chase
	 *  
	 *  @param inputDir
	 *  			  The directory holding the input configuration
	 */
	public static void ChaseAndRestrict(String inputDir) throws Exception
	{	
		//parse query
		FileReader fr = new FileReader(inputDir+"/"+"query");
		CQParser cqparser = new CQParser(fr);
		ConjunctiveQuery query = null;
		Map<String,ConjunctiveQuery> parsedCQs = cqparser.parse();
		for (String s:parsedCQs.keySet()) 
			query = parsedCQs.get(s);
		fr.close();
		
		//parse views
		fr = new FileReader(inputDir+"/"+"views");
		cqparser = new CQParser(fr);
		ArrayList<ConjunctiveQuery> views = new ArrayList<ConjunctiveQuery>();
		parsedCQs = cqparser.parse();
		for (String s:parsedCQs.keySet()) 
			views.add(parsedCQs.get(s));
		fr.close();
		
		//parse constraints
		fr = new FileReader(inputDir+"/"+"constraints_chase");
		ConstraintParser consparser = new ConstraintParser(fr);
		ArrayList<Constraint> constraints = consparser.parse();
		fr.close();
		
		//parse schemas
		fr = new FileReader(inputDir+"/"+"schemas");
		RelSchemaParser schparser = new RelSchemaParser(fr);
		ArrayList<RelationalSchema> schemas = schparser.parse();
		fr.close();
		
		ArrayList<Constraint> fwconstraints = new ArrayList<Constraint>();
		fwconstraints.addAll(constraints);
		
		Pair<List<Constraint>, List<Constraint>> constrFromViews=Utilities.getFWandBWConstraints(views);
		fwconstraints.addAll(constrFromViews.getLeft());
		
		ConjunctiveQuery chaseRes = CBProvider.Chase(query, schemas.get(0), fwconstraints);
		ConjunctiveQuery uPlan = CBProvider.Restrict(chaseRes, schemas.get(1));
		
	    PrintWriter out = new PrintWriter(new FileWriter(inputDir+"/"+"results_chase"));
	    out.println(cbInternal.Utils.fromQueryToString(uPlan));
	    out.close();
	}
}
