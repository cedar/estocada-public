/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package cbTool;

public class Main {

	public static void main(String args[]) throws Exception {
		ToolUtils.ComputeRewritings(args[0]);
	}
	
}
