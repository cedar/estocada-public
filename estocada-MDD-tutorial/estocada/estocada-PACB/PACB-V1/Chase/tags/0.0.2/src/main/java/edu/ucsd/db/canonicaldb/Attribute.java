package edu.ucsd.db.canonicaldb;

/**
 * It represents an attribute (column) in a relational table.
 */
public class Attribute {

	/**
	 * The object corresponding to the relation in which the attribute belongs
	 */
	private Relation relation;
	
	/**
	 * The index of the attribute in the relation
	 */
	private int position;
	
	/**
	 * The attribute's name
	 */
	private String name;
	
	
	public Attribute(Relation _rel, int _pos, String _name) {
		relation = _rel;
		position = _pos;
		name = new String(_name);
	}
	
	public Relation relation() {
		return relation;
	}
	
	public int pos() {
		return position;
	}
	
	public String name() {
		return name;
	}
	
	public void setName(String name) {
		this.name = new String(name);
	}
	
	public String toString() {
		return relation.name + "." + this.name;
	}
}
