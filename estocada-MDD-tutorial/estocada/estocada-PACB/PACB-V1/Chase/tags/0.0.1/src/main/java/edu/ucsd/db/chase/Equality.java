package edu.ucsd.db.chase;

import edu.ucsd.db.datalogexpr.Value;

/**
 * This class represents an equality between a variable and a constant, a
 * variable and a variable, or a constant and a variable. A variable can be
 * either distinguished or existential.
 */
public class Equality {

	private Value left;

	private Value right;

	public Equality(Value left, Value right) {
		this.left = left;
		this.right = right;
	}

	public Value getLeft() {
		return left;
	}

	public Value getRight() {
		return right;
	}

}
