package edu.ucsd.db.query;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Vector;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This operator implements the hash join algorithm.
 */
public class NaturalJoin extends AbstractJoin {

	public enum Side {
		LEFT, RIGHT
	};

	private enum NextStatus {
		MATCH, NO_MATCH
	};

	/**
	 * The current bucket of the hashtable that produces join tuples.
	 */
	ArrayList<Tuple> currentBucket;

	/**
	 * The current bucket position that produced the last join tuple.
	 */
	int currentBucketPos;

	/**
	 * The current right tuple that produced the last join tuple.
	 */
	Tuple newlyInsertedTuple;

	Tuple lookupTuple;

	String newlyInsertedTupleKey;

	HashMap<String, ArrayList<Tuple>> addTupleTable;

	HashMap<String, ArrayList<Tuple>> lookupTable;

	Side newlyInsertedTupleSide;

	/**
	 * In the case of cartesian product, this is the iterator over the buckets
	 * that hold the left tuples.
	 */
	Iterator<ArrayList<Tuple>> currentBucketIter;

	BinaryProjectComponent project;

	// TODO: Move the generation of the hash table to a function and
	// call it only when necessary

	/**
	 * Constructor.
	 * 
	 * @param leftChildOp
	 *            the left child operator.
	 * @param rightChildOp
	 *            the right child operator.
	 */
	public NaturalJoin(Operator leftChildOp, Operator rightChildOp,
			CanonicalSchema outputSchema, LinkedHashMap<Integer, Value> newVars) {
		super(leftChildOp, rightChildOp);

		// Create a <code>ProjectComponent</code>
		CanonicalSchema leftChildSchema = leftChildOp.getOutSchema();
		CanonicalSchema rightChildSchema = rightChildOp.getOutSchema();
		project = new BinaryProjectComponent(outputSchema, leftChildSchema,
				rightChildSchema, newVars);

		outSchema = project.outSchema;
	}

	// TODO: Change this and the nextTuple() function to return columns
	// in the right order (check also that the two functions use the same
	// order)

	/**
	 * Sets the schema of the tuples this operator outputs.
	 */
	protected void setOutSchema() {
		// TODO
	}

	// TODO: Be careful to return ALL tuples that join with the new tuple and
	// not
	// just the first join tuple and then go to the next input tuple

	// TODO: Should we just add all tuples for one operator?
	// TODO: Also what about condensing everything to one big function that
	// returns all?

	protected Tuple next() {
		Tuple inputTuple, tupleAfterProjection;
		NextStatus nextStatus;

		// Get next join output from tuple last added to one of the
		// hashtables
		//System.out.println("Natural Join calling next");
		//outSchema.display();
		while (!hasEmptyInput()) {
			if (newlyInsertedTuple != null) {

			} else if (!leftInputQueue.isEmpty()) {
				inputTuple = leftInputQueue.removeFirst();
				addTupleTable = leftTuples;
				lookupTable = rightTuples;
				newlyInsertedTuple = inputTuple;
				newlyInsertedTupleKey = insertLeft(inputTuple);
			} else {
				inputTuple = rightInputQueue.removeFirst();
				addTupleTable = rightTuples;
				lookupTable = leftTuples;
				newlyInsertedTuple = inputTuple;
				newlyInsertedTupleKey = insertRight(inputTuple);
			}

			nextStatus = nextTuple();

			if (nextStatus == NextStatus.MATCH) {
				tupleAfterProjection = getProjectedTuple();
				//System.out.println("got valid tuple "+tupleAfterProjection);
				if (tupleAfterProjection != null)
					return tupleAfterProjection;
			} else
				newlyInsertedTuple = null;
		}

		return null;
	}

	private Tuple getProjectedTuple() {
		Tuple tupleAfterProjection;

		if (lookupTable == leftTuples) {
			tupleAfterProjection = project.projectTuple(lookupTuple,
					newlyInsertedTuple);
		} else {
			tupleAfterProjection = project.projectTuple(newlyInsertedTuple,
					lookupTuple);
		}

		return tupleAfterProjection;
	}

	protected Tuple nextSingle() {
		return null;
	}

	// If a match is found, the matching tuples are newlyInsertedTuple
	// and lookupTuple
	protected NextStatus nextTuple() {

		if (lookupTable.isEmpty()) {
			currentBucket = null;
			return NextStatus.NO_MATCH;
		}

		// in case of cartesian product, simply iterate over the buckets that
		// hold the hash tuples
		if (leftJoinSchema.isEmpty()) {

			if (currentBucket == null) {
				currentBucketIter = lookupTable.values().iterator();
				currentBucket = currentBucketIter.next();
				currentBucketPos = 0;
				lookupTuple = currentBucket.get(currentBucketPos);

			} else if ((currentBucketPos + 1) == currentBucket.size()
					&& currentBucketIter.hasNext()) {
				// currentScanTuple = scanChildOp.next();
				currentBucket = currentBucketIter.next();
				currentBucketPos = 0;
				lookupTuple = currentBucket.get(currentBucketPos);
			} else if ((currentBucketPos + 1) != currentBucket.size()) {
				currentBucketPos++;
				lookupTuple = currentBucket.get(currentBucketPos);
			} else {
				// In this case, the iterator over the left tuple buckets is
				// done. So we get the next right tuple and we restart the
				// iterator.
				currentBucket = null;
				return NextStatus.NO_MATCH;
			}

			return NextStatus.MATCH;
		}

		// For join
		lookupTuple = null;

		if (currentBucket == null) {
			if (lookupTable.containsKey(newlyInsertedTupleKey)) {
				currentBucket = lookupTable.get(newlyInsertedTupleKey);
				currentBucketPos = 0;
				lookupTuple = currentBucket.get(currentBucketPos);
				return NextStatus.MATCH;
			}
		} else if (currentBucketPos + 1 < currentBucket.size()) {
			currentBucketPos++;
			lookupTuple = currentBucket.get(currentBucketPos);
			return NextStatus.MATCH;
		}

		currentBucket = null;
		newlyInsertedTuple = null;
		return NextStatus.NO_MATCH;
	}

	// TODO: Consider adding eveything from the input queues in the hash
	// table

	// TODO: Check that it is true
	protected boolean hasEmptyInput() {
		return (leftInputQueue.isEmpty() && rightInputQueue.isEmpty() && ((currentBucket == null) || (currentBucket != null && currentBucketPos == currentBucket
				.size())));
	}

	protected StringBuffer printString() {
		StringBuffer retVal = new StringBuffer(project.printString());
		retVal.append(" Join");

		return retVal;
	}

	protected boolean isBlockedDefault() {
		return false;
	}

	// Should we also change the temporary vars?
	protected void flush() {
		project.flush();
		leftTuples.clear();
		rightTuples.clear();
		
		childOp1().flush();
		childOp2().flush();
	}
}
