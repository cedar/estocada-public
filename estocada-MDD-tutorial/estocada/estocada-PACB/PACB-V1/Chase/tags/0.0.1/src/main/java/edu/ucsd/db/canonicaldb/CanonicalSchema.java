package edu.ucsd.db.canonicaldb;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import edu.ucsd.db.query.QueryEvaluationException;

/**
 * Represents the schema of a <code>Tuple</code>.
 */
public class CanonicalSchema {

	private static int newColNameCounter;
	
	/**
	 * The <code>LinkedHashMap</code> holding the schema of a tuple. The key
	 * is a position and the value the name of the column in the tuple.
	 */
	LinkedHashMap<Integer, String> schema;

	/**
	 * The <code>Hashtable</code> holding the inverted schema of a tuple. The
	 * key is a column name and the value is one of the possible many positions
	 * of the column in the tuple.
	 */
	Hashtable<String, Integer> invertedSchema;

	/**
	 * Constructor.
	 */
	public CanonicalSchema() {
		schema = new LinkedHashMap<Integer, String>();
		invertedSchema = new Hashtable<String, Integer>();
	}

	/**
	 * Adds a new column to the end of the schema.
	 * 
	 * @param col
	 *            the name of the column to add
	 * @return the position of the added column
	 * @throws QueryEvaluationException
	 *             if the column is already in the schema
	 */
	public int addColumnName(String col) {
		int position = schema.size();
		schema.put(position, col);
		invertedSchema.put(col, position);

		return position;
	}

	/**
	 * Adds a new column and position to the schema.
	 * 
	 * @param col
	 *            the name of the column to add
	 * @throws QueryEvaluationException
	 *             if the column is already in the schema, or if there is a
	 *             position collision.
	 */
	public void addColumnName(String col, int pos) {
		if (schema.containsKey(pos))
			throw new QueryEvaluationException("Adding column to schema failed: "
					+ "position already taken in the schema.");

		schema.put(pos, col);
		invertedSchema.put(col, pos);
	}

	/**
	 * Gets the position of a column in the schema.
	 * 
	 * @param col
	 *            the name of the column
	 * @return the position of the specified column
	 * @throws QueryEvaluationException
	 *             if the column is not in the schema
	 */
	public int getPosition(String col) {
		if (!invertedSchema.containsKey(col))
			throw new QueryEvaluationException("Getting column position failed: "
					+ "column not in the schema.");

		return invertedSchema.get(col);
	}

	/**
	 * Gets an iterator over the positions in the schema. Positions are
	 * <code>Integer</code> objects.
	 * 
	 * @return an iterator over the positions in the schema
	 */
	public Iterator<Integer> getPositions() {
		return schema.keySet().iterator();
	}

	/**
	 * Gets an iterator over the column names in the schema. Column names are
	 * <code>String</code> objects.
	 * 
	 * @return an iterator over the column names in the schema
	 */
	public Iterator<String> getColumnNames() {
		return schema.values().iterator();
	}

	/**
	 * Gets an iterator over the (position, column name) pairs in the schema.
	 * Pairs are <code>Map.Entry</code> objects, where the position (key) is
	 * an <code>Integer</code> object, and the column name (value) is a
	 * <code>String</code> object.
	 * 
	 * @return an iterator over the (column name, position) pairs in the schema.
	 */
	public Iterator<Map.Entry<Integer, String>> getEntries() {
		return schema.entrySet().iterator();
	}

	/**
	 * Determines if the specified position is part of the schema.
	 * 
	 * @param pos
	 *            a position to lookup
	 * @return <code>true</code>, if the specified position is part of the
	 *         schema; <code>false</code>, otherwise.
	 */
	public boolean hasPosition(int pos) {
		return schema.containsKey(pos);
	}

	/**
	 * Determines if the specified column name is part of the schema.
	 * 
	 * @param col
	 *            a column name to lookup
	 * @return <code>true</code>, if the specified column name is part of the
	 *         schema; <code>false</code>, otherwise.
	 */
	public boolean hasColumnName(String col) {
		return invertedSchema.containsKey(col);
	}

	/**
	 * Determines is the schema is empty.
	 * 
	 * @return <code>true</code>, if the schema contains no columns;
	 *         <code>false</code>, otherwise.
	 */
	public boolean isEmpty() {
		return schema.isEmpty();
	}

	/**
	 * Determines the size of the schema.
	 * 
	 * @return the size of the schema as an integer.
	 */
	public int size() {
		return schema.size();
	}

	public static String freshColName() {
		String res = "_x" + newColNameCounter;
		newColNameCounter++;
		
		return res;
	}
	
	public void display() {
		for (String col: schema.values())
			System.out.print(col+" ");
		System.out.println();
	}
}
