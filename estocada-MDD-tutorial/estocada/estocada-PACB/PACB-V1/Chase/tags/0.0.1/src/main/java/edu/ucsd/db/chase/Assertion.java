package edu.ucsd.db.chase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.chaseexceptions.InconsistencyException;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Statement;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This class represents an assertion in the context of a
 * <code>CanonicalDB<code>, which is a constraint with a 
 * given conclusion and a premise that always holds.
 */
public class Assertion extends Constraint {

	/**
	 * The conclusion of the assertion.
	 */
	private Statement conclusion;

	public Assertion(Statement conclusion, Database db) {
		super(db);
		this.conclusion = conclusion;
	}
	
	public static Assertion createCopyOfConstraint(Assertion oldAssertion, Database db) {
		Assertion newAssertion = new Assertion(oldAssertion.conclusion, db);
		return newAssertion;
	}

	public Statement getConclusion() {
		return conclusion;
	}
	
	public boolean enforce() {

		createFreshInstanceOfQueryBody(conclusion, null, false);
		
		return true;
	}
	
	public void flush() {
		// TODO: Add throw exception
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		
		buf.append("\t\t");
		buf.append(conclusion.toString());
		
		return buf.toString();
	}

}
