/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package fr.inria.oak.edu.backchase.atoms;

import org.apache.log4j.Logger;

/**
 * An equality on two positions of two atoms
 * part of the definition of the premise
 * 
 * @author ranaalotaibi
 * @author Ioana Ileana
 *
 */

public class DefEquality {
    private final static Logger LOGGER = Logger.getLogger(DefEquality.class);

    private AtomPositionTerm term1;
    private AtomPositionTerm term2;

    /** Constructor **/
    public DefEquality(final AtomPositionTerm term1, final AtomPositionTerm term2) {
        this.term1 = term1;
        this.term2 = term2;
    }

    /**
     * Set Term 1
     * 
     * @param term1
     */
    public void setTerm1(final AtomPositionTerm term1) {
        this.term1 = term1;
    }

    /**
     * Set Term 2
     * 
     * @param term2
     */
    public void setTerm2(final AtomPositionTerm term2) {
        this.term2 = term2;
    }

    /**
     * Get Term 1
     */
    public AtomPositionTerm getAtomPositionTerm1() {
        return term1;
    }

    /**
     * Get Term 2
     */
    public AtomPositionTerm getAtomPositionTerm2() {
        return term2;
    }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(term1);
        str.append("=");
        str.append(term2);

        return str.toString();
    }
}
