package fr.inria.oak.edu.backchase.costUtils;

/**
 * Back-chase Costing Manager
 * 
 * @author ranaalotaibi
 *
 */
public final class CostingManager {

    public static boolean ENABLE_COSTING = false;
    public static CostFunction COST_FUNCTION = null;
    public static CostType COSTING_TYPE = null;

    private CostingManager() {
    };
}
