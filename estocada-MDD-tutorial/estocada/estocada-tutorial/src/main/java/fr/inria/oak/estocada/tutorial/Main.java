package fr.inria.oak.estocada.tutorial;

import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;

public class Main {

    public static void main(String[] args) {
        try {
            TutorialWrapper.Run();
        } catch (EstocadaException e) {
            System.err.println(e.getMessage());
        }

    }
}
