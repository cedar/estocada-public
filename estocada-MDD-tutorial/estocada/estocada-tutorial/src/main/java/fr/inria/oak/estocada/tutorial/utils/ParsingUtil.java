package fr.inria.oak.estocada.tutorial.utils;

import static java.sql.Types.VARCHAR;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.compiler.model.aj.AJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveModule;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.pr.PRNaiveModule;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeBuilderAlternative2;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.rk.RKQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveModule;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.sppj.SPPJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveModule;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.utils.RunnerUtils;
//import fr.inria.oak.estocada.vldbdemo.scenarios.rewriting.BigDAWG.BigDAWGQBTQueryTranslator;
//import fr.inria.oak.estocada.vldbdemo.scenarios.rewriting.BigDAWG.BigDAWGQBTViewTranslator;

/**
 * Query and views ParsingUtils
 * 
 * @author ranaalotaibi
 */
public class ParsingUtil {
    private static final String CHARSET = "utf-8";

    /**
     * Get CQ of a query
     * 
     * @param lang
     *            query language
     * @return a string CQ of a query
     * @throws fr.inria.oak.commons.conjunctivequery.parser.ParseException
     * @throws IllegalArgumentException
     */
    public static ConjunctiveQuery getCQQuery(final Language lang, final String examplePath) throws EstocadaException,
            IllegalArgumentException, fr.inria.oak.commons.conjunctivequery.parser.ParseException {
        final Injector injector = getInjector(lang);
        final QueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(lang, injector);
        QueryBlockTree nbt = null;
        ConjunctiveQuery cqQuery = null;
        try {

            String query = readQuery(examplePath, lang);
            if (query != null && queryBlockTreeBuilder != null && !lang.equals(Language.QBT)
                    && !lang.equals(Language.BIGDAWG)) {
                if (lang.equals(Language.AQL)) {
                    query = preProcesssAQL(query);
                }
                nbt = queryBlockTreeBuilder.buildQueryBlockTree(query);
            }
            final String queryStr = processCQ(nbt, lang, query);
            if (queryStr != null) {
                cqQuery = fr.inria.oak.estocada.compiler.Utils.parseQuery(queryStr);
                writeCQQuery(Paths.get(examplePath + "cqQuery").toString(), cqQuery);
            }

        } catch (ParseException | IOException e) {
            throw new EstocadaException("Query couldn't be parsed: \n" + e.getMessage());
        }

        return cqQuery;

    }

    /**
     * Get CQ constraints of views
     * 
     * @param lang
     *            query language
     * @param examplePath
     *            example path
     * @return list of view constraints
     */
    public static final List<QueryBlockTree> getViews(final Language lang, final String examplePath)
            throws EstocadaException {
        final Injector injector = getInjector(lang);
        final QueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(lang, injector);
        final QueryBlockTreeViewCompiler compiler = getCompiler(lang, injector);
        QueryBlockTree nbt = null;
        final List<QueryBlockTree> views = new ArrayList<>();
        final List<Constraint> fws = new ArrayList<>();
        final List<Constraint> bws = new ArrayList<>();
        final List<RelationalSchema> gs = new ArrayList<>();
        final List<RelationalSchema> ts = new ArrayList<>();

        final File[] files = new File(examplePath).listFiles();

        if (compiler != null) {
            for (final File file : files) {
                if (file.getName().contains("View")) {
                    try {

                        String view = FileUtils.readFileToString(new File(examplePath + file.getName()));
                        view = preProcessView(view);
                        if (lang.equals(Language.QBT)) {//TODO:FIX
                            nbt = queryBlockTreeBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(view));
                        } else {
                            if (lang.equals(Language.BIGDAWG)) {
                                //                                final BigDAWGQBTViewTranslator bigDAWGToQBTView = new BigDAWGQBTViewTranslator(view);
                                //                                final String viewQBT = bigDAWGToQBTView.translateToQBT();
                                //                                nbt = queryBlockTreeBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(viewQBT));

                            } else {
                                nbt = queryBlockTreeBuilder.buildQueryBlockTree(view);
                            }
                        }

                        views.add(nbt);
                        Context context =
                                compiler.compileContext(nbt, new RelationalSchema(new ArrayList<Relation>()), true);
                        fws.addAll(context.getForwardConstraints());
                        bws.addAll(context.getBackwardConstraints());
                        final Relation constant = new Relation("constant", 2);
                        final List<Relation> gsrelations = context.getGlobalSchema().getRelations();
                        final List<Relation> tsrelations = context.getTargetSchema().getRelations();
                        gsrelations.add(constant);
                        tsrelations.add(constant);
                        final RelationalSchema gsreSchema = new RelationalSchema(gsrelations);
                        final RelationalSchema tsreSchema = new RelationalSchema(tsrelations);

                        if (gs.isEmpty()) {
                            gs.add(gsreSchema);

                        }
                        if (ts.isEmpty()) {
                            ts.add(tsreSchema);
                        }
                        if (queryBlockTreeBuilder instanceof QBTQueryBlockTreeBuilder) {
                            QBTQueryBlockTreeBuilder.reIntilize();
                        }
                        if (queryBlockTreeBuilder instanceof PRQueryBlockTreeBuilderAlternative2) {
                            PRQueryBlockTreeBuilderAlternative2.resetVariables();
                        }

                    } catch (ParseException | IOException e) {
                        throw new EstocadaException("View couldn't be parsed: \n" + e.getMessage());
                    }

                }

            }

            try {
                fr.inria.oak.estocada.compiler.Utils
                        .writeConstraints(Paths.get(examplePath + "constraints_chase").toString(), fws);
                fr.inria.oak.estocada.compiler.Utils
                        .writeConstraints(Paths.get(examplePath + "constraints_bkchase").toString(), bws);
                fr.inria.oak.estocada.compiler.Utils.writeSchemas(Paths.get(examplePath + "schemas").toString(),
                        gs.get(0), ts.get(0));
            } catch (IOException e) {
                throw new EstocadaException(e.getMessage());
            }

        }
        return views;

    }

    /**
     * Get language injector
     * 
     * @param lang
     *            query language
     * @return language injector
     */
    public static Injector getInjector(final Language lang) throws EstocadaException {
        Injector injector = null;
        switch (lang) {
            case AQL:
                injector = Guice.createInjector(new AJNaiveModule());
                return injector;
            case KQL:
                injector = Guice.createInjector(new RKNaiveModule());
                return injector;
            case SQLPlusPlus:
                injector = Guice.createInjector(new SPPJNaiveModule());
                return injector;
            case SQL:
                injector = Guice.createInjector(new PRNaiveModule());
                return injector;
            case CYPHER: //TODO:fix
                return null;
            case QBT:
            case BIGDAWG:
                injector = Guice.createInjector(new QBTNaiveModule());
                return injector;
            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    /**
     * Get language builder
     * 
     * @param lang
     *            query language
     * @param injector
     *            query language injector
     * @return language builder
     */
    private static QueryBlockTreeBuilder getBuilder(final Language lang, final Injector injector)
            throws EstocadaException {
        QueryBlockTreeBuilder builder = null;
        switch (lang) {
            case AQL:
                builder = injector.getInstance(AJQueryBlockTreeBuilder.class);
                return builder;
            case KQL:
                builder = injector.getInstance(RKQueryBlockTreeBuilder.class);
                return builder;
            case SQLPlusPlus:
                builder = injector.getInstance(SPPJQueryBlockTreeBuilder.class);
                return builder;

            case SQL:
                //TODO:Fix
                builder = injector.getInstance(PRQueryBlockTreeBuilderAlternative2.class);

                ((PRQueryBlockTreeBuilderAlternative2) builder).setRelations(readSchema());

                return builder;
            case CYPHER://TODO:fix
                return null;
            case QBT:
            case BIGDAWG:
                builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
                return builder;
            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    /**
     * Get language builder
     * 
     * @param lang
     *            query language
     * @param injector
     *            query language injector
     * @return language builder
     */
    private static QueryBlockTreeViewCompiler getCompiler(final Language lang, final Injector injector)
            throws EstocadaException {
        switch (lang) {
            case AQL:
                final AJNaiveQueryBlockTreeCompiler compilerAJ =
                        injector.getInstance(AJNaiveQueryBlockTreeCompiler.class);
                return compilerAJ;
            case KQL:
                final RKNaiveQueryBlockTreeCompiler compilerRK =
                        injector.getInstance(RKNaiveQueryBlockTreeCompiler.class);
                return compilerRK;
            case SQLPlusPlus:
                final SPPJNaiveQueryBlockTreeCompiler compilerSPPJ =
                        injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class);
                return compilerSPPJ;
            case SQL:
                final PRQueryBlockTreeCompiler compilerSQL = injector.getInstance(PRQueryBlockTreeCompiler.class);
                return compilerSQL;
            case CYPHER: //TODO:fix
                return null;
            case QBT:
            case BIGDAWG:
                final QBTNaiveQueryBlockTreeCompiler compilerQBT =
                        injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
                return compilerQBT;

            default:
                throw new EstocadaException("Langugae is not supported!");
        }
    }

    private static String processCQ(final QueryBlockTree nbt, final Language language, final String query) {

        final StringBuilder querystrBuilder = new StringBuilder();
        final List<Atom> atoms = new ArrayList<>();
        final List<String> returnVars = new ArrayList<>();
        final List<Atom> extraEqAtoms = new ArrayList<>();
        final List<Term> newReturnVars = new ArrayList<>();
        switch (language) {
            case AQL:
                //                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                //                    final Variable variable = new Variable(term.toString());
                //                    final Variable newVariable = new Variable(term.toString() + "0");
                //                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.aj.Predicate.EQUALS.toString(),
                //                            variable, newVariable));
                //                    newReturnVars.add(newVariable);
                //
                //                }
                ReturnTerm returnTerm = nbt.getRoot().getReturnTemplate().getTerms().get(0);
                for (Variable term2 : returnTerm.getReferredVariables()) {
                    final Variable variable = new Variable(term2.toString());
                    final Variable newVariable = new Variable(term2.toString() + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.aj.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                break;
            case KQL:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    String var = term.getReferredVariables().iterator().next().toString(); //TODO:Fix
                    final Variable variable = new Variable(var);
                    final Variable newVariable = new Variable(var + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.rk.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                break;

            case SQL:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    String var = term.getReferredVariables().iterator().next().toString(); //TODO:Fix
                    final Variable variable = new Variable(var);
                    newReturnVars.add(variable);
                }
                break;
            case SQLPlusPlus:
                for (ReturnTerm term : nbt.getRoot().getReturnTemplate().getTerms()) {
                    final Variable variable = new Variable(term.toString());
                    final Variable newVariable = new Variable(term.toString() + "0");
                    extraEqAtoms.add(new Atom(fr.inria.oak.estocada.compiler.model.sppj.Predicate.EQUALS.toString(),
                            variable, newVariable));
                    newReturnVars.add(newVariable);

                }
                final List<Atom> conditioAtoms = nbt.getRoot().getPattern()
                        .encoding(fr.inria.oak.estocada.compiler.model.sppj.Utils.conditionEncoding);
                atoms.addAll(conditioAtoms);
                break;

            case CYPHER: //TODO:fix
                return null;
            case QBT:
                return RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(query));
            //            case BIGDAWG:
            //                final BigDAWGQBTQueryTranslator bigDAWGTOQBT = new BigDAWGQBTQueryTranslator(preProcessBigDAWG(query));
            //                final String qbtQuery = bigDAWGTOQBT.translateToQBT();
            //                return RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(qbtQuery));
            default:
                throw new EstocadaException("Langugae is not supported!");

        }

        final String queryName = nbt.getRoot().getQueryName();
        //Build CQ String
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");

        //Head
        if (!newReturnVars.isEmpty()) {
            newReturnVars.forEach(r -> querystrBuilder.append(r.toString()).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        } else {
            returnVars.forEach(r -> querystrBuilder.append(r).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        }
        querystrBuilder.append(">:-");

        //Body
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }

        //ExtraEQ
        atoms.addAll(extraEqAtoms);

        if (!atoms.isEmpty()) {
            atoms.forEach(r -> querystrBuilder.append(r.toString()).append(","));
            querystrBuilder.setLength(querystrBuilder.length() - 1);
        }

        querystrBuilder.append(";");
        return querystrBuilder.toString();

    }

    private static String preProcessBigDAWG(String query) {

        query = query.replace("jsonb_array_elements", "JSONARRAYELEMENTS");
        return query;
    }

    private static String preProcessView(String view) {

        view = view.replace("jsonb_array_elements", "JSONARRAYELEMENTS");
        if (view.contains("solr")) {
            view = view.replace("solr_build_object", "SOLRJSONBUILDOBJECT");
        } else {
            view = view.replace("json_build_object", "JSONBUILDOBJECT");
        }

        return view;
    }

    private static String readQuery(final String examplePath, final Language lang) {
        String query = null;
        try {
            query = FileUtils.readFileToString(new File(examplePath + "/Query"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return query;
    }

    private static void writeCQQuery(final String fileName, final ConjunctiveQuery query) throws IOException {
        final File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), CHARSET));
        try {
            writer.write(query.toString());
        } catch (IOException e) {
            throw e;
        } finally {
            writer.close();
        }
    }

    private static Map<String, fr.inria.oak.estocada.compiler.model.pr.Relation> readSchema() { //TODO:Fix

        Map<String, fr.inria.oak.estocada.compiler.model.pr.Relation> Relations =
                new HashMap<String, fr.inria.oak.estocada.compiler.model.pr.Relation>();
        //        Relations.put("R",
        //                new fr.inria.oak.estocada.compiler.model.pr.Relation("R", Arrays.asList(new String[] { "A", "B", "C" }),
        //                        Arrays.asList(new Integer[] { VARCHAR, VARCHAR, VARCHAR })));
        //
        //        Relations.put("S", new fr.inria.oak.estocada.compiler.model.pr.Relation("S",
        //                Arrays.asList(new String[] { "C", "D" }), Arrays.asList(new Integer[] { VARCHAR, VARCHAR })));
        //
        //        Relations.put("T", new fr.inria.oak.estocada.compiler.model.pr.Relation("T",
        //                Arrays.asList(new String[] { "D", "E" }), Arrays.asList(new Integer[] { VARCHAR, VARCHAR })));

        BufferedReader reader;
        Path ab = Paths.get("examples/SQLExample/baseschema").toAbsolutePath();
        try {
            reader = new BufferedReader(new FileReader(ab.toString()));
            String line = reader.readLine();
            while (line != null) {
                String[] schema = line.split(",");
                String[] colums = new String[schema.length - 1];
                Integer[] types = new Integer[schema.length - 1];
                for (int i = 1; i < schema.length; i++) {
                    colums[i - 1] = schema[i];
                    types[i - 1] = VARCHAR;
                }
                Relations.put(schema[0], new fr.inria.oak.estocada.compiler.model.pr.Relation(schema[0],
                        Arrays.asList(colums), Arrays.asList(types)));
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Relations;
    }

    private static String preProcesssAQL(String query) {
        final String forClause = StringUtils.substringBefore(query, "return");
        final String returnClause = StringUtils.substringAfter(query, "return");
        String[] vars = returnClause.split(",");
        final StringBuilder str = new StringBuilder();
        int i = 1;
        str.append("{");
        for (String var : vars) {
            str.append("\"" + var + "\"" + ":" + var);
            if (i != vars.length) {
                str.append(",");
            }

            i++;
        }
        str.append("}");
        final StringBuilder newQuery = new StringBuilder();
        newQuery.append(forClause);
        newQuery.append("\n");
        newQuery.append("return " + str.toString());
        return newQuery.toString();

    }

}
