package fr.inria.oak.estocada.tutorial.utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.parser.ConstraintParser;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.commons.relationalschema.parser.RelSchemaParser;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.cg.Rewriter;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.main.EstocadaRunnerTutorial;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriting.decoder.aj.AJTranslator;
import fr.inria.oak.estocada.rewriting.decoder.pr.PRTranslatorNoSchema;
import fr.inria.oak.estocada.rewriting.decoder.rk.RKTranslator;
import fr.inria.oak.estocada.rewriting.decoder.sppj.SPPJTranslator;

/**
 * Rewriting Utils
 * 
 * @author ranaalotaibi
 */
public class RewritingUtil {
    private static ConjunctiveQuery encodedQuery = null;
    private static List<String> decodedRW = new ArrayList<>();
    private static List<ConjunctiveQuery> cqs = new ArrayList<>();
    private static List<String> appliedFW = new ArrayList<>();
    private static List<String> subQueries = new ArrayList<>();

    /**
     * Compute rewritings
     * 
     * @param examplePath
     *            example path
     * @param language
     *            query and view language
     * @return found rewritings
     * @throws Exception
     */
    public static List<ConjunctiveQuery> computeRewritings(final String examplePath, final Language language)
            throws Exception {
        //CQ Caae: query and constraints read from files.
        if (language.equals(Language.CONJUNCTIVE_QUERY)) {
            Path ab = Paths.get(examplePath).toAbsolutePath();
            cqs = computeRewritings(ab.toString());
            encodedQuery = getQuery(ab.toString());
            return cqs;

        }

        if (language.equals(Language.CYPHER)) {//TODO:Fix
            Path ab = Paths.get(examplePath).toAbsolutePath();
            Rewriter rewriter = new Rewriter(ab.toString() + "/", false);
            cqs = rewriter.rewrite();
            encodedQuery = fr.inria.oak.estocada.compiler.Utils.parseQuery(rewriter.encodedQuery());
            decodedRW = rewriter.decode();
            return cqs;
        }

        //Parse query in `query` file and generate the corresponding CQ query.
        final ConjunctiveQuery query = ParsingUtil.getCQQuery(language, examplePath);
        encodedQuery = query;
        //Get encoded views 
        final List<QueryBlockTree> encodedViews = ParsingUtil.getViews(language, examplePath);

        //Find rewritings of a given example configuration
        cqs = getRewriting(query, encodedViews, language);

        return cqs;
    }

    private static List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector, final Language lang) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        switch (lang) {
            case AQL:
                compilers.add(injector.getInstance(AJNaiveQueryBlockTreeCompiler.class));
                break;
            case KQL:
                compilers.add(injector.getInstance(RKNaiveQueryBlockTreeCompiler.class));
                break;
            case SQLPlusPlus:
                compilers.add(injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class));
                break;
            case SQL:
                compilers.add(injector.getInstance(PRQueryBlockTreeCompiler.class));
                break;
            case CYPHER:
                break;
            case QBT:
            case BIGDAWG:
                compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
                break;

            default:
                throw new IllegalArgumentException("Language is not supported!");
        }

        return compilers;
    }

    private static List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector,
            final Language lang) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector, lang).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private static List<PACBConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts,
            final Injector injector, final Language lang) {
        final List<PACBConjunctiveQueryRewriter> rewriters = new ArrayList<PACBConjunctiveQueryRewriter>();
        createContexts(nbts, injector, lang).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

    private static List<ConjunctiveQuery> getRewriting(final ConjunctiveQuery query, final List<QueryBlockTree> views,
            final Language lang) throws Exception {
        final Injector injector = ParsingUtil.getInjector(lang);
        final List<PACBConjunctiveQueryRewriter> rewriters = createRewriters(views, injector, lang);
        final Iterator<PACBConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final PACBConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
            appliedFW = rewriter.getAppliedFWConstraints();
        }
        return reformulations;
    }

    /**
     * Get encoded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return encodedQuery;
    }

    /**
     * Get decoded rw
     * 
     * @return decodedRW
     */
    public static List<String> getDeodedRWs(final Language lang) {
        switch (lang) {
            case AQL:
                final AJTranslator ajTranslator = new AJTranslator();
                for (ConjunctiveQuery cq : cqs) {
                    decodedRW.add(ajTranslator.translate(cq));
                }
                break;
            case KQL:
                final RKTranslator rkTranslator = new RKTranslator();
                for (ConjunctiveQuery cq : cqs) {
                    decodedRW.add(rkTranslator.translate(cq));
                }
                break;
            case SQLPlusPlus:
                for (ConjunctiveQuery cq : cqs) {
                    final SPPJTranslator spjjTranslator = new SPPJTranslator(cq);
                    decodedRW.add(spjjTranslator.translate());
                }
                break;
            case SQL:
                for (ConjunctiveQuery cq : cqs) {
                    final PRTranslatorNoSchema sqlTranslator = new PRTranslatorNoSchema(cq);
                    decodedRW.add(sqlTranslator.translate());
                }
                break;
            case CYPHER:
                break;
            case QBT:
            case BIGDAWG:
                break;
            default:
                throw new IllegalArgumentException("Language is not supported!");
        }
        return decodedRW;
    }

    public static void visualizeConstraint(final String examplePath, final String flag) {
        if (flag.equals("y")) {
            Path ab = Paths.get(examplePath).toAbsolutePath();
            String givenFileName = "constraints" + ab.getFileName();
            String fileNameDot = new String(givenFileName + ".dot");
            String fileNamePS = new String(givenFileName + ".png");

            StringBuffer sb = new StringBuffer();
            sb.append("digraph { ");
            for (int i = 0; i < appliedFW.size(); i++) {
                sb.append("\"" + appliedFW.get(i).replaceAll("\"", "\\\\\"") + "\"");
                if (i != (appliedFW.size() - 1)) {
                    sb.append("->");
                } else {
                    sb.append("}");
                }

            }
            FileWriter file;
            try {
                file = new FileWriter(fileNameDot);
                file.write(new String(sb));;
                file.close();
                Runtime r = Runtime.getRuntime();
                String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePS);
                Process p = r.exec(com);
                p.waitFor();
            } catch (IOException | InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    public static void visualizeRWPlan(final String examplePath) {

        Path ab = Paths.get(examplePath).toAbsolutePath();
        String givenFileName = "RWTatooinePlan" + ab.getFileName();
        String fileNameDot = new String(givenFileName + ".dot");
        String fileNamePS = new String(givenFileName + ".png");
        StringBuffer sb = new StringBuffer();
        sb.append("digraph { ");
        if (subQueries.size() == 2) {
            StringBuffer sb1 = new StringBuffer();
            StringBuffer sb2 = new StringBuffer();
            StringBuffer join1 = new StringBuffer();
            sb1.append("\"PostgreSQLSourceAccess(Relational):\n\n" + subQueries.get(0) + "\"");
            sb2.append("\"PostgreSQLSourceAccess(JSON):\\n\\n" + subQueries.get(1) + "\"");
            join1.append("BindJoinAcccess1");

            sb.append(join1.toString() + "->" + sb1.toString());
            sb.append(join1.toString() + "->" + sb2.toString());
        }

        if (subQueries.size() == 3) {
            StringBuffer sb1 = new StringBuffer();
            StringBuffer sb2 = new StringBuffer();
            StringBuffer sb3 = new StringBuffer();
            StringBuffer join1 = new StringBuffer();
            StringBuffer join2 = new StringBuffer();

            sb1.append("\"SolrSourceAccess:\n\n" + subQueries.get(0) + "\"");
            sb2.append("\"PostgreSQLSourceAccess(Relational):\n\n" + subQueries.get(1) + "\"");
            sb3.append("\"PostgreSQLSourceAccess(JSON):\n\n" + subQueries.get(2) + "\"");
            final String sb1s = sb1.toString();
            final String sb2s = sb2.toString();
            final String sb3s = sb3.toString();
            join1.append("BindJoinAcccess1");
            join2.append("BindJoinAcccess2");

            final String join1s = join1.toString();
            final String join2s = join2.toString();

            sb.append(join1s);
            sb.append("->");
            sb.append(sb1s + ";");
            sb.append(join1s);
            sb.append("->");
            sb.append(sb2s + ";");

            sb.append(join2s);
            sb.append("->");
            sb.append(join1s + ";");
            sb.append(join2s);
            sb.append("->");
            sb.append(sb3s + ";");

        }

        sb.append("}");

        FileWriter file;
        try {
            file = new FileWriter(fileNameDot);
            file.write(new String(sb));;
            file.close();
            Runtime r = Runtime.getRuntime();
            String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePS);
            Process p = r.exec(com);
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void RunScenario(final String examplePath) {
        Path ab = Paths.get(examplePath).toAbsolutePath();
        final EstocadaRunnerTutorial estoadaRunner = new EstocadaRunnerTutorial(ab.toString());

        try {
            estoadaRunner.runTattoineSMQCMV();
        } catch (Exception e) {
            e.printStackTrace();
        }
        subQueries = estoadaRunner.getSubQueires();
    }

    //#############################FOR READING FROM FILES ######################//
    private static PACBConjunctiveQueryRewriter getRewriter(final String examplePath) throws Exception {
        return new PACBConjunctiveQueryRewriter(getContext(examplePath));
    }

    private static List<ConjunctiveQuery> computeRewritings(final ConjunctiveQuery query, final String examplePath)
            throws Exception {
        final PACBConjunctiveQueryRewriter rewriter = getRewriter(examplePath);
        final List<ConjunctiveQuery> rws = rewriter.getReformulations(query);
        appliedFW = rewriter.getAppliedFWConstraints();
        return rws;
    }

    private static List<ConjunctiveQuery> computeRewritings(final String examplePath) throws Exception {
        return computeRewritings(getQuery(examplePath), examplePath);
    }

    private static List<Constraint> parseConstraints(final String fileName)
            throws IOException, fr.inria.oak.commons.constraints.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(fr);
        final List<Constraint> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static List<RelationalSchema> parseSchemas(final String fileName)
            throws IOException, fr.inria.oak.commons.relationalschema.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final RelSchemaParser parser = new RelSchemaParser(fr);
        final List<RelationalSchema> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static Context getContext(final String examplePath) throws Exception {
        final List<RelationalSchema> schemas = parseSchemas(examplePath + "/schemas");
        return new Context(schemas.get(0), schemas.get(1), parseConstraints(examplePath + "/constraints_chase"),
                parseConstraints(examplePath + "/constraints_bkchase"));
    }

    private static ConjunctiveQuery getQuery(final String examplePath) throws Exception {
        if (examplePath.contains("BP")) {//TODO:FIX-This just for testing!!
            final ConjunctiveQuery cq = Utils.parseQuery(new File(examplePath + "/cqQuery"));
            final List<Atom> cqBody = new ArrayList<>();
            for (Atom atom : cq.getBody()) {
                for (int i = 0; i < atom.getTerms().size(); i++) {
                    cqBody.add(new Atom("D", atom.getTerm(i)));
                }
                cqBody.add(atom);
            }

            return new ConjunctiveQuery(cq.getName(), cq.getHead(), cqBody);
        }
        return Utils.parseQuery(new File(examplePath + "/cqQuery"));
    }

}
