package fr.inria.oak.estocada.tutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.exceptions.EstocadaException;
import fr.inria.oak.estocada.tutorial.utils.MetadataUtil;
import fr.inria.oak.estocada.tutorial.utils.RewritingUtil;

/**
 * Tutorial Example Wrapper
 * 
 * @author ranaalotaibi
 * 
 */
public class TutorialWrapper {

    /** Found rewritings **/
    private static List<ConjunctiveQuery> rws = new ArrayList<>();
    /** Decoded rewritings **/
    private static List<String> decodedRWs = new ArrayList<>();

    /**
     * Find and decode rewritings for an example provided in config.json
     */
    @SuppressWarnings("unchecked")
    public static void Run() throws EstocadaException {

        final Map<String, Object> tutConfig = MetadataUtil.parseConfigTutorialFile();
        final Language language = Language.valueOf((String) tutConfig.get("lang"));
        final String examplePath = (String) tutConfig.get("exampleFolder");
        final String visualizeConstraint = (String) tutConfig.get("visualizeConstraints");

        try {

            if (examplePath.contains("Scenario")) {
                RewritingUtil.RunScenario(examplePath);
                RewritingUtil.visualizeRWPlan(examplePath);
            } else {

                rws = RewritingUtil.computeRewritings(examplePath, language);
                if (!language.equals(Language.CONJUNCTIVE_QUERY)) {
                    decodedRWs = RewritingUtil.getDeodedRWs(language);
                }
                RewritingUtil.visualizeConstraint(examplePath, visualizeConstraint);

                if (!language.equals(Language.CONJUNCTIVE_QUERY)) {
                    System.out.println("\n****** Encoded CQ Query *****");
                    System.out.println(RewritingUtil.getEncodedQuery());
                }
                System.out.println("\n****** CQ Rewritings *****");
                rws.forEach(System.out::println);
                if (rws.isEmpty()) {
                    System.out.println("No Rewriting Found!");
                }
                if (!language.equals(Language.CONJUNCTIVE_QUERY)) {
                    System.out.println("\n****** Decoded Rewritings *****");
                    decodedRWs.forEach(s -> System.out.print(s + "\n\n"));
                }

            }
        } catch (Exception e) {
            throw new EstocadaException(e.getMessage());
        }

    }

    /**
     * Get enocded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return RewritingUtil.getEncodedQuery();
    }

    /**
     * Get CQ rewritings
     * 
     * @return rws
     *         CQ rewritings
     */
    public static List<ConjunctiveQuery> getRWs() {
        return rws;
    }

    /**
     * Get decoded rewritings
     * 
     * @return decodedRWs decoded rewritings
     */
    public static List<String> getDecodedRWs() {
        return decodedRWs;
    }

}
