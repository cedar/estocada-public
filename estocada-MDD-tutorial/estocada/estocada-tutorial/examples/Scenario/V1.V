VPJPJ = FOR PJ:{
SELECT P.p->'SUBJECT_ID' AS SUBJECT_ID,  A->'hadm_id' AS HADM_ID, ICUSTAYS->'prescriptions' AS PRESCRIPTIONS
FROM  mimictutorial.mimicdata AS P, mimictutorial.mimiclabitems AS D, 
	  jsonb_array_elements(P.p->'ADMISSIONS') AS A, 
	  jsonb_array_elements(A->'labevents') AS LABEVENTS,
	  jsonb_array_elements(A->'icustays') AS ICUSTAYS 
		
WHERE  D.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	   D.data->>'FLUID'='Blood' AND
	   LABEVENTS->>'flag' = 'abnormal' }
	   
RETURN	 PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID, 'HADM_ID',HADM_ID,'PRESCRIPTIONS', PRESCRIPTIONS) }      	
