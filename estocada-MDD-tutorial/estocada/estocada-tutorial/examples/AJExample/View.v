V = 
for d in dataset D, p in d.site.person, ssn in p.ssn
	return {"id": ssn, 
			"addresses": for a in p.address  
				return {"address": 
							{"zip": for z in a.zip return z, 
							 "streetName": for st in a.street return st}
					   }
			}