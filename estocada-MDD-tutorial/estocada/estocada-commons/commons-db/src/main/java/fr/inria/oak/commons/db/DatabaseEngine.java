package fr.inria.oak.commons.db;

/**
 * Supported RDBMS engines.
 * @author Damian Bursztyn
 */
public enum DatabaseEngine {
	POSTGRESQL, DB2, MYSQL, VIRTUOSO, ALLEGRO_GRAPH, HIVE
};
