package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class PhyJQScanIntegrationTest extends BaseTest {
	
	@Test
	public void testTweets() throws TatooineExecutionException {
		String json = path + "/json/tweets.json";

		// Setting properties
		GSR gsr = new GSR("spark");
		gsr.setProperty("table", "tweets");
		gsr.setProperty("frame", json);
		
		NRSMD nrsmd = new NRSMD(10, 
			new TupleMetadataType[] {
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE }, 
				new String[] { "comments_i", "created_at_s", "id", "likes_i", "retweeted_s", "shares_i", "text_s", "type_s", "user_name_s", "user_screen_name_s" }, 
				new NRSMD[0]);
		
		String query = "SELECT * FROM tweets";
		PhyJQScan searcher = new PhyJQScan(query, gsr, nrsmd);
		searcher.open();
		
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertEquals(0, tuple.nestedFields.length);
			assertEquals(7, tuple.stringFields.length);
			assertEquals(3, tuple.integerFields.length);
		}
		searcher.close();		
	}
	
	private PhyJQScan buildScan(String query) throws TatooineExecutionException {
		String json = path + "/json/books.json";

		// Setting properties
		GSR gsr = new GSR("spark");
		gsr.setProperty("table", "books");
		gsr.setProperty("frame", json);
		
		NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "format" }, new NRSMD[0]);
		NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { "rating" }, new NRSMD[0]);
		
		NRSMD nrsmd = new NRSMD(9, 
				new TupleMetadataType[] {
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.STRING_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE,
					TupleMetadataType.INTEGER_TYPE,
					TupleMetadataType.TUPLE_TYPE }, 
				new String[] { "id", "name_s", "author_s", "series_s", "sequence_i", "pages_i", "formats", "price_i", "ratings" }, 
				new NRSMD[] { child1, child2 });		
		PhyJQScan searcher = new PhyJQScan(query, gsr, nrsmd);		
		return searcher;
	}
	
	@Test
	public void testBooks() throws TatooineExecutionException {
		PhyJQScan searcher = buildScan("SELECT * FROM books");
		searcher.open();
		
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertEquals(2, tuple.nestedFields.length);
			assertEquals(4, tuple.stringFields.length);
			assertEquals(3, tuple.integerFields.length);
		}
		searcher.close();
	}
	
	@Test(expected = TatooineExecutionException.class)
	public void testEval() throws TatooineExecutionException {
		buildScan("SELECT author_s FROM books");
	}
}
