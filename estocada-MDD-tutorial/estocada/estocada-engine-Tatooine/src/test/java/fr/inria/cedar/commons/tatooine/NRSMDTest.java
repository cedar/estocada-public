package fr.inria.cedar.commons.tatooine;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

public class NRSMDTest extends BaseTest {

	private TreePattern tp1;
	private TreePattern tp2;
	private TreePattern tp3;
	private TreePattern tp4;
	private NRSMD n1;
	private NRSMD n2;
	private NRSMD n3;
	private NRSMD n4;

	@Test
	// Test simple nesting
	public void testSimpleNesting() throws Exception {
		try {
			HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
			tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");
			tp2 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p02.xam");

			// Assert names
			assertEquals(tp1.getName(), "p01.xam");
			assertEquals(tp2.getName(), "p02.xam");

			// Assert number of nodes
			assertEquals(tp1.getNodesNo(), 4);
			assertEquals(tp2.getNodesNo(), 3);
			assertEquals(tp1.getValContRetNodesNo(), 2);
			assertEquals(tp2.getValContRetNodesNo(), 2);

			// Assert roots
			assertEquals(tp1.getActualRoot().toString(), "//r*");
			assertEquals(tp2.getActualRoot().toString(), "//r*C");

			n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true, map);
			n2 = NRSMD.getNRSMD((TreePatternNode) tp2.getRoot(), true, map);

			// Assert number of NRSMD columns
			assertEquals(n1.getColNo(), 6);
			assertEquals(n2.getColNo(), 5);

			// Assert type metadata of NRSMD columns
			assertEquals(n1.getColumnMetadata(0), TupleMetadataType.URI_TYPE.toString());
			assertEquals(n1.getColumnMetadata(1), TupleMetadataType.STRUCTURAL_ID.toString());
			assertEquals(n1.getColumnMetadata(2), TupleMetadataType.STRUCTURAL_ID.toString());
			assertEquals(n1.getColumnMetadata(3), TupleMetadataType.STRING_TYPE.toString());
			assertEquals(n1.getColumnMetadata(4), TupleMetadataType.STRUCTURAL_ID.toString());
			assertEquals(n1.getColumnMetadata(5), TupleMetadataType.STRING_TYPE.toString());

			assertEquals(n2.getColumnMetadata(0), TupleMetadataType.URI_TYPE.toString());
			assertEquals(n2.getColumnMetadata(1), TupleMetadataType.STRUCTURAL_ID.toString());
			assertEquals(n2.getColumnMetadata(2), TupleMetadataType.STRING_TYPE.toString());
			assertEquals(n2.getColumnMetadata(3), TupleMetadataType.STRUCTURAL_ID.toString());
			assertEquals(n2.getColumnMetadata(4), TupleMetadataType.STRING_TYPE.toString());

			// Assert single nested field
			NRSMD res = NRSMD.addNestedField(n1, n2);
			tp3 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p03.xam");
			n3 = NRSMD.getNRSMD((TreePatternNode) tp3.getRoot(), true, map);
			assertTrue(Arrays.deepEquals(n3.getColumnsMetadata(), res.getColumnsMetadata()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	// Test double nesting
	public void testDoubleNesting() throws Exception {
		try {
			HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
			tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");
			tp2 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p02.xam");

			// Assert number of nodes
			assertEquals(tp1.getNodesNo(), 4);
			assertEquals(tp2.getNodesNo(), 3);
			
			// Assert roots
			assertEquals(tp1.getActualRoot().toString(), "//r*");
			assertEquals(tp2.getActualRoot().toString(), "//r*C");

			n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true, map);
			n2 = NRSMD.getNRSMD((TreePatternNode) tp2.getRoot(), true, map);
			
			// Assert double nested field
			NRSMD res = NRSMD.addNestedField(n1, n2);					
			tp3 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p04.xam");
			n3 = NRSMD.getNRSMD((TreePatternNode) tp3.getRoot(), true,  map);			
			res = NRSMD.addNestedField(res, n3);
			
			tp4 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p05.xam");
			n4 = NRSMD.getNRSMD((TreePatternNode) tp4.getRoot(), true, map);
			
			assertTrue(Arrays.deepEquals(n4.getColumnsMetadata(), res.getColumnsMetadata()));				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	// Removes a nested field at the end of the tree on the first level
	public void testUnnest() throws Exception {	
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
		tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");
		tp2 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p06.xam");		
		n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true,  map);
		n2 = NRSMD.getNRSMD((TreePatternNode) tp2.getRoot(), true,  map);
		
		// Add nested field
		n3 = NRSMD.addNestedField(n1, n2);
		assertEquals(n3.getColumnsMetadata().length, 7);
		
		// Resolve expected metadata types
		TupleMetadataType[] comparison = new TupleMetadataType[n1.getColNo() + n2.getColNo()];		
		int i = 0;
		for (TupleMetadataType tmt : n1.getColumnsMetadata()) {
			comparison[i] = tmt;
			i++;
		}
		for (TupleMetadataType tmt : n2.getColumnsMetadata()) {
			comparison[i] = tmt;
			i++;
		}
		
		// Set the columns to unnest 
		int unnestCol = 6;
		int[] unnestMap = new int[1];
		unnestMap[0] = unnestCol;
		int nestColumnNumber = n3.getNestedChild(unnestCol).getColNo();
		
		// Unnest field
		NRSMD n4 = NRSMD.unnestField(n3, unnestMap);	
		assertTrue(n4.getColNo() == (n3.getColNo() + nestColumnNumber - 1));
		assertTrue(compare(n4.getColumnsMetadata(), comparison));
	}	
	
	/**
	 * Removes a nested field at a nested level
	 * @throws Exception
	 */
	@Test
	public void testUnNest2() throws Exception {
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
		tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");
		tp2 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p06.xam");		
		n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true,  map);
		n2 = NRSMD.getNRSMD((TreePatternNode) tp2.getRoot(), true,  map);
		
		// Add nested fields
		n3 = NRSMD.addNestedField(n2, n2);
		n4 = NRSMD.addNestedField(n1, n3);
		
		// Resolve expected metadata types
		TupleMetadataType[] comparison = new TupleMetadataType[n1.getColNo() + 1];		
		int i = 0;
		for (TupleMetadataType tmt : n1.getColumnsMetadata()) {
			comparison[i] = tmt;
			i++;
		}
		comparison[i] = TupleMetadataType.TUPLE_TYPE;
		i++;
		
		// Set the columns to unnest 
		int[] unnestMap = new int[2];
		// At this level we have to specify the complete path that goes from the 1st level to the last one to unnest.
		// For example, if we have [a, b, [c, d, [e, f]]] and we want to unnest [e, f] then we should use the map [2, 2].
		unnestMap[0] = 6;
		unnestMap[1] = 4;
		
		// Unnest field
		NRSMD n5 = NRSMD.unnestField(n4, unnestMap);
		assertTrue(compare(n5.getColumnsMetadata(), comparison));
	}
	
	/** Helper method that compares two arrays of TupleMetadataType */
	private boolean compare(TupleMetadataType[] first, TupleMetadataType[] second){
		boolean value = true;
		if(first.length != second.length)
			return false;
		for (int i = 0; i < first.length; i++) {
			if (!second[i].equals(first[i])) {
				return false;
			}
		}
		return value;
	}

}
