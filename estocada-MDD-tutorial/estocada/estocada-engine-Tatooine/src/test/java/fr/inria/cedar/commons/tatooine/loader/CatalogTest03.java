package fr.inria.cedar.commons.tatooine.loader;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

public class CatalogTest03 extends BaseTest {
	/*
	 * Test the creation of the Catalog for some current running Scenarios. This
	 * test class will be used by Rana, Damian and Romain
	 */
	@Test
	public void testCreationKQ01() throws TatooineExecutionException, IOException {

		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("name", 0);
		mappingColumnsToIndexes01.put("country", 1);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceRedis("V6", "0");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V6", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V6"));
	}

	@Test
	public void testCreationKQ02() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("internalMap", 0);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceRedis("V7", "0");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.TUPLE_TYPE }, new String[] { "internalMap" },
		                new NRSMD[] {
		                        new NRSMD(3,
		                                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
		                                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }) }),
		        mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V7", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V7"));
	}

	@Test
	public void testCreationKQ03() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artistID:123", 0);

		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceRedis("V8", "0");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.TUPLE_TYPE }, new String[] { "artistID:123" },
		                new NRSMD[] {
		                        new NRSMD(3,
		                                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
		                                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }) }),
		        mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V8", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V8"));
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferenceRedis(String gsrName, String dbNumber) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("dbNumber", dbNumber);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}

	@Test
	public void testCreationJQ01() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("name", 0);
		mappingColumnsToIndexes01.put("age", 1);
		mappingColumnsToIndexes01.put("country", 2);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceSparkSQL("V9", "info");
		ViewSchema viewSchema01 = new ViewSchema(new NRSMD(2, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE,
		        TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }), mappingColumnsToIndexes01);
		// Adding the above information to the catalog
		catalog.add("V9", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V9"));
	}

	@Test
	public void testCreationJQ02() throws TatooineExecutionException, IOException {
		Catalog catalog = Catalog.getInstance();
		// Mapping
		Map<String, Integer> mappingColumnsToIndexes01 = new HashMap<String, Integer>();
		mappingColumnsToIndexes01.put("artist_id", 0);
		mappingColumnsToIndexes01.put("track_name", 1);
		mappingColumnsToIndexes01.put("release_name", 2);
		mappingColumnsToIndexes01.put("format_name", 3);
		// viewSizeBundle
		ViewSizeBundle viewSizeBundle01 = new ViewSizeBundle(0, 0);
		// Storage References
		StorageReference gsr01 = createStorageReferenceRedis("V10", "log");
		ViewSchema viewSchema01 = new ViewSchema(
		        new NRSMD(4,
		                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE,
		                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }),
		        mappingColumnsToIndexes01);

		// Adding the above information to the catalog
		catalog.add("V10", gsr01, viewSizeBundle01, viewSchema01);
		catalog.write();
		Catalog.load();
		assertTrue(catalog.contains("V10"));
	}

	/** Returns a StorageReference object with some properties filled in */
	private StorageReference createStorageReferenceSparkSQL(String gsrName, String jsonPath) {
		StorageReference gsr = new GSR(gsrName);
		try {
			gsr.setProperty("jsonPath", jsonPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return gsr;
	}
}
