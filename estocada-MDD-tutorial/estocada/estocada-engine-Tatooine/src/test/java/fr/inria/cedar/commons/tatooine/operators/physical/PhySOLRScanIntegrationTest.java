package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

@Ignore
public class PhySOLRScanIntegrationTest extends BaseTest {
	
	@Test
	/** Finds all social media posts that have been shared a specific number of times */
	public void testSocialMedia() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		
		// The format of documents for this test is as follows:
		// {
		//    "id":"734785553636458498",
		//    "user_name_s": "François Hollande",    
		//    "user_screen_name_s": "fhollande",
		//    "text_s": "Je félicite chaleureusement @vanderbellen pour son élection à la présidence de l'Autriche et je me réjouis de coopérer avec lui",
		//    "created_at_s": "Mon May 23 16:38:22 +0000 2016",
		//    "shares_i": 32,
		//    "likes_i": 0,
		//    "comments_i": 0,
		//    "retweeted_s": "true",
		//    "type_s":"TW"
		// }
		
		try {
			loadCollectionSOLR("SocialMediaSOLR", path + "/json/SocialMediaSOLR.json");
		} catch (Exception e) {
			String msg = String.format("Error creating collection 'SocialMediaSOLR' from JSON file '%s': %s", path + "/json/SocialMediaSOLR.json", e.getMessage());
			throw new TatooineExecutionException(msg);
		}
		
		// Retrieve view metadata from the catalog
		Catalog catalog = Catalog.getInstance();
		ViewSchema schema = catalog.getViewSchema("SocialMediaSOLR");
		GSR gsr = (GSR) catalog.getStorageReference("SocialMediaSOLR");

		// Setting the query		
		String query = "shares_i:[100 TO 200]";
		PhySOLRScan searcher = new PhySOLRScan(query, "", gsr, schema);
		try {
			searcher.open();
			assertTrue(searcher.hasNext());
			while (searcher.hasNext()) {
				NTuple tuple = searcher.next();
				assertNotNull(tuple);
				assertNotNull(tuple.nrsmd);
				// Asserts tweets have been retweeted between 100 to 200 times
				int retweet_count = (Integer) tuple.getValue("shares_i");
				assertTrue((retweet_count >= 100) && (retweet_count <= 200));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		searcher.close();
	}
	
	@Test	
	/** Queries for all information available on books stored as nested JSON documents in SOLR */
	public void testBooks() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		// The columns of the JSON file are: "author_s", "formats", "id", "name_s", "pages_i", "price_i", "ratings", "sequence_i", "series_s"
		// The fields "ratings" and "format" are nested		
		
		try {
			loadCollectionSOLR("BookSOLR", path + "/json/BookSOLR.json");
		} catch (Exception e) {
			String msg = String.format("Error creating collection 'BookSOLR' from JSON file '%s': %s", path + "/json/BookSOLR.json", e.getMessage());
			throw new TatooineExecutionException(msg);
		}
						
		// Retrieve view metadata from the catalog
		Catalog catalog = Catalog.getInstance();
		ViewSchema schema = catalog.getViewSchema("BookSOLR");
		GSR gsr = (GSR) catalog.getStorageReference("BookSOLR");
		// Setting the query
		String query = "*:*";
		PhySOLRScan searcher = new PhySOLRScan(query, "", gsr, schema);
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
			
			// Formats
			List<NTuple> formats = tuple.getNestedField(1);
			assertFalse(formats.isEmpty());
			for (NTuple format: formats) {
				assertTrue(format.nrsmd.colNames[0].contains("format"));
				assertEquals(TupleMetadataType.STRING_TYPE.toString(), format.nrsmd.getColumnMetadata(0));
			}
			
			// Ratings
			List<NTuple> ratings = tuple.getNestedField(6);
			assertFalse(formats.isEmpty());
			for (NTuple rating: ratings) {
				assertTrue(rating.nrsmd.colNames[0].contains("rating"));
				assertEquals(TupleMetadataType.INTEGER_TYPE.toString(), rating.nrsmd.getColumnMetadata(0));				
			}
		}
		searcher.close();
	}

}
