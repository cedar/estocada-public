package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class PhyAsterixDBEvalTest extends BaseTest {
    @Test
    public void test00() throws TatooineExecutionException {

        final GSR gsr = new GSR("AsterixDB");
        gsr.setProperty("host", "127.0.0.1");
        gsr.setProperty("port", "19002");

        final NRSMD nrsmd = new NRSMD(4,
                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE,
                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
                new String[] { "created_at_s", "user_name_s", "user_screen_name_s", "text_s" }, new NRSMD[0]);

        final String query = "USE test0; SELECT created_at_s, user_name_s, user_screen_name_s, text_s FROM test0";
        final PhyAsterixDBJSONEval phyAsterixDBJSONEval = new PhyAsterixDBJSONEval(query, gsr, nrsmd);
        phyAsterixDBJSONEval.open();
        int count = 0;
        assertTrue(phyAsterixDBJSONEval.hasNext());
        while (phyAsterixDBJSONEval.hasNext()) {
            NTuple tuple = phyAsterixDBJSONEval.next();
            assertNotNull(tuple);
            assertEquals(0, tuple.nestedFields.length);
            assertEquals(4, tuple.stringFields.length);
            assertEquals(0, tuple.integerFields.length);
            count++;
        }
        phyAsterixDBJSONEval.close();
        System.out.print("******" + count + "\n");
    }

    @Test
    public void test01() throws TatooineExecutionException {

        final GSR gsr = new GSR("AsterixDB");
        gsr.setProperty("host", "127.0.0.1");
        gsr.setProperty("port", "19002");

        NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE },
                new String[] { "format" }, new NRSMD[0]);
        NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE },
                new String[] { "rating" }, new NRSMD[0]);

        NRSMD nrsmd = new NRSMD(3,
                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.TUPLE_TYPE,
                        TupleMetadataType.TUPLE_TYPE },
                new String[] { "id", "formats", "ratings" }, new NRSMD[] { child1, child2 });

        final String query = "USE test1; SELECT id, formats, ratings  FROM test1";
        final PhyAsterixDBJSONEval phyAsterixDBJSONEval = new PhyAsterixDBJSONEval(query, gsr, nrsmd);
        phyAsterixDBJSONEval.open();

        assertTrue(phyAsterixDBJSONEval.hasNext());
        while (phyAsterixDBJSONEval.hasNext()) {
            NTuple tuple = phyAsterixDBJSONEval.next();
            assertNotNull(tuple);
        }
        phyAsterixDBJSONEval.close();
    }

}
