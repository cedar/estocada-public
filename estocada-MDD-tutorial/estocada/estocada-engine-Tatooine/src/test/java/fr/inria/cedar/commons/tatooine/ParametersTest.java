package fr.inria.cedar.commons.tatooine;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;

public class ParametersTest extends BaseTest {
	
	@Test
	// Test that paths to general resources are defined in the configuration file
	public void testGeneralResources() throws TatooineException {
		assertNotNull(Parameters.getProperty("resources"));
		assertNotNull(Parameters.getProperty("DOTOutputFolder"));
	}
	
	@Test
	// Test that tuple parameters are defined in the configuration file
	public void testTupleParams() throws TatooineException {
		assertNotNull(Parameters.getProperty("childAxis"));
		assertNotNull(Parameters.getProperty("delimiter"));
	}
	
}
