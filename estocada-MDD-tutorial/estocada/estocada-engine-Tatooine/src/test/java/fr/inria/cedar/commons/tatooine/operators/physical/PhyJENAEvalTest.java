package fr.inria.cedar.commons.tatooine.operators.physical;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.JenaDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * This test depends on a RDF file containing information about French politicians: model.TTL.
 * For each test, create a catalog entry that represents each view (see @CatalogTest#addNamesOfPoliticiansJENA).
 * @author Oscar Mendoza
 */
public class PhyJENAEvalTest extends BaseTest {

	private static final int NUM_ITERATIONS = 100;

	private void registerNewEntry(String catalogEntry, String[] columns) throws Exception {
		Catalog catalog = Catalog.getInstance();

		if (!catalog.contains(catalogEntry)) {
			TupleMetadataType[] types = new TupleMetadataType[columns.length];
			for (int i = 0; i < columns.length; ++i) {
				types[i] = TupleMetadataType.STRING_TYPE;
			}

			NRSMD nrsmd = new NRSMD(columns.length, types, columns, new NRSMD[0]);

			Map<String, Integer> colNameToIndexMap = new HashMap<>();
			int columnIndex = 0;
			for (String columnName : columns) {
				colNameToIndexMap.put(columnName, columnIndex++);
			}
			ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

			StorageReference gsr = new GSR(catalogEntry);
			gsr.setProperty(JenaDatabaseLoader.MODEL_RDF, path + "/rdf/model.ttl");
			catalog.add(catalogEntry, gsr, new ViewSizeBundle(5, 5), schema);
		}

		// Asserts there is an entry for our test view in the catalog
		assertTrue(catalog.contains(catalogEntry));
	}

	private void benchmarkQuery(String query, String catalogEntry, String[] columns) throws Exception {
		registerNewEntry(catalogEntry, columns);

		// Retrieve view metadata from the catalog
		Catalog catalog = Catalog.getInstance();
		ViewSchema schema = catalog.getViewSchema(catalogEntry);
		GSR gsr = (GSR) catalog.getStorageReference(catalogEntry);

		PhyJENAEval searcher = new PhyJENAEval(query, gsr, schema);
		for (int i = 0; i < NUM_ITERATIONS; ++i) {
			searcher.open();
			while (searcher.hasNext()) {
				NTuple tuple = searcher.next();
				assertNotNull(tuple);
				assertNotNull(tuple.nrsmd);
			}
			searcher.close();
		}
	}

	// compare SPARQL query execution time ( in ms) of 3 approaches: Jena, Jena with TDB, Jena with tdbloader tool
	// normal 5.47	tdb 5.18	tdbloader 4.74
	// 56 results
	@Test
	public void testNamesOfPoliticians() throws Exception {
		benchmarkQuery(
			"PREFIX fm: <http://example.org/schemas/> " +
			"SELECT DISTINCT ?name_s ?surname_s " +
			"WHERE { " +
				"?pol fm:firstName ?name_s . " +
				"?pol fm:lastName ?surname_s . " +
				"?pol fm:party fm:PAR00087 . " +
			"}",
			"NamesOfPoliticiansJENA",
			new String[] {"name_s", "surname_s"}
		);
	}

	// compare SPARQL query execution time ( in ms) of 3 approaches: Jena, Jena with TDB, Jena with tdbloader tool
	// normal 11.37	tdb 12.22	tdbloader 8.68
	// 1080 results
	/**
	 * - the names and IDs and twitter IDs of all politicians together with their currents and their parties
	 */
	@Test
	public void testPoliticiansInfo() throws Exception {
		benchmarkQuery(
			"PREFIX fm: <http://example.org/schemas/> \n" +
			"SELECT DISTINCT ?name_s ?surname_s ?twitter_s ?party_s ?current_abbr_s\n" +
			"WHERE { \n" +
			"  ?pol fm:firstName ?name_s . \n" +
			"  ?pol fm:lastName ?surname_s . \n" +
			"  ?pol fm:twitter ?twitter_s . \n" +
			"  ?pol fm:party ?party .\n" +
			"  ?party fm:label ?party_s .\n" +
			"  ?party fm:current ?current .\n" +
			"  ?current fm:abbreviation ?current_abbr_s\n" +
			"}\n",
			"PoliticiansInfo",
			new String[] {"name_s", "surname_s", "twitter_s", "party_s", "current_abbr_s"}
		);
	}

	// compare SPARQL query execution time ( in ms) of 3 approaches: Jena, Jena with TDB, Jena with tdbloader tool
	// normal 8.89	tdb 8.08	tdbloader 7.24
	/**
	 * - the names of politicians that are (with the socialist party or with Les Républicains) and (male or female) or such (queries with an OR)
	 */
	// 644 results
	@Test
	public void testQueryWithFilterOR1() throws Exception {
		benchmarkQuery(
			"PREFIX fm: <http://example.org/schemas/> \n" +
			"SELECT DISTINCT ?name_s ?surname_s\n" +
			"WHERE { \n" +
			"  ?pol fm:firstName ?name_s . \n" +
			"  ?pol fm:lastName ?surname_s .\n" +
			"  ?pol fm:gender ?gender .\n" +
			"  ?pol fm:party ?party .\n" +
			"  filter ( \n" +
			"    (?party = fm:PAR00070 || ?party = fm:PAR00096 ) && \n" +
			"    (?gender = \"M\" || ?gender = \"F\") \n" +
			"  )\n" +
			"}",
			"NamesOfPoliticiansJENA",
			new String[] {"name_s", "surname_s"}
		);
	}

	// compare SPARQL query execution time ( in ms) of 3 approaches: Jena, Jena with TDB, Jena with tdbloader tool
	// normal 9.49	tdb 8.85	tdbloader 6.71
	// 644 results
	@Test
	public void testQueryWithFilterOR2() throws Exception {
		benchmarkQuery(
			"PREFIX fm: <http://example.org/schemas/> \n" +
			"SELECT DISTINCT ?name_s ?surname_s\n" +
			"WHERE { \n" +
			"  ?pol fm:firstName ?name_s . \n" +
			"  ?pol fm:lastName ?surname_s .\n" +
			"  ?pol fm:gender ?gender .\n" +
			"  ?pol fm:party ?party .\n" +
			"  filter ( \n" +
			"    (?party = fm:PAR00070 && ?gender = \"M\" ) || \n" +
			"    (?party = fm:PAR00070 && ?gender = \"F\" ) || \n" +
			"    (?party = fm:PAR00096 && ?gender = \"M\" ) || \n" +
			"    (?party = fm:PAR00096 && ?gender = \"F\" )\n" +
			"  )\n" +
			"}",
			"NamesOfPoliticiansJENA",
			new String[] {"name_s", "surname_s"}
		);
	}
	
}
