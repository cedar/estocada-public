package fr.inria.cedar.commons.tatooine.operators.physical.join;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.CatalogTest;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyJENAEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLRScan;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

public class PhyMemoryHashJoinTest extends CatalogTest {

	private static final Catalog catalog = Catalog.getInstance();	
	
	@Test
	public void testSimplePredicate() throws Exception {
		// Queries
		String querySolr = "Macron";
		// Select Twitter ID + first name + last name of all French politicians
		String queryJena = "PREFIX fm: <http://example.org/schemas/> "
			+ "SELECT DISTINCT ?user_screen_name_s ?name_s ?surname_s "
			+ "WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:twitter ?user_screen_name_s . }";
				
		// Properties
		ViewSchema schemaSolr = catalog.getViewSchema("SocialMediaSOLR");
		GSR refSolr = (GSR) catalog.getStorageReference("SocialMediaSOLR"); 
		ViewSchema schemaJena = catalog.getViewSchema("TwitterIDsJENA");
		GSR refJena = (GSR) catalog.getStorageReference("TwitterIDsJENA");
				
		// Operators
		PhySOLRScan opl = new PhySOLRScan(querySolr, "", refSolr, schemaSolr);
		PhyJENAEval opr = new PhyJENAEval(queryJena, refJena, schemaJena);				
		int indexSolr = opl.nrsmd.getColIndexFromName("user_screen_name_s");
		int indexJena = opr.nrsmd.getColIndexFromName("user_screen_name_s") + opl.nrsmd.colNo;
		SimplePredicate pred = new SimplePredicate(indexSolr, indexJena);
		
		PhyMemoryHashJoin join = new PhyMemoryHashJoin(opl, opr, pred);
		join.open();
		assertTrue(join.hasNext());
		while (join.hasNext()) {
			NTuple tup = join.next();
			assertNotNull(tup);
			String text = new String(tup.getStringField(6));
			// Assert the output tweet contains the requested keyword
			assertTrue(text.toLowerCase().contains("macron"));
		}
		join.close();
	}
	
	@Test
	public void testConjunctivePredicate() throws Exception {
		// Queries
		String querySolr = "Macron";
		// Select Twitter ID + first name + last name + type of social network (FB or TW) of all French politicians
		String queryJena = "PREFIX fm: <http://example.org/schemas/> "
			+ "SELECT DISTINCT ?user_screen_name_s ?name_s ?surname_s ?type_s "
			+ "WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:twitter ?user_screen_name_s . } "
			+ "VALUES ?type_s { 'TW' }";

		// Properties
		ViewSchema schemaSolr = catalog.getViewSchema("SocialMediaSOLR");
		GSR refSolr = (GSR) catalog.getStorageReference("SocialMediaSOLR"); 
		
		NRSMD nrsmdJena = new NRSMD(4, 
				new TupleMetadataType[] { 
					TupleMetadataType.STRING_TYPE, 
					TupleMetadataType.STRING_TYPE, 
					TupleMetadataType.STRING_TYPE, 
					TupleMetadataType.STRING_TYPE }, 
				new String[] { "user_screen_name_s", "name_s", "surname_s", "type_s" }, 
				new NRSMD[0]);
		GSR refJena = (GSR) catalog.getStorageReference("TwitterIDsJENA");
				
		// Operators
		PhySOLRScan opl = new PhySOLRScan(querySolr, "", refSolr, schemaSolr);
		PhyJENAEval opr = new PhyJENAEval(queryJena, refJena, nrsmdJena);
		
		int indexSolr1 = opl.nrsmd.getColIndexFromName("user_screen_name_s");
		int indexJena1 = opr.nrsmd.getColIndexFromName("user_screen_name_s") + opl.nrsmd.colNo;
		int indexSolr2 = opl.nrsmd.getColIndexFromName("type_s");
		int indexJena2 = opr.nrsmd.getColIndexFromName("type_s") + opl.nrsmd.colNo;	
		
		// Conjunctive predicate
		Predicate[] preds = new Predicate[2];
		preds[0] = new SimplePredicate(indexSolr1, indexJena1);
		preds[1] = new SimplePredicate(indexSolr2, indexJena2);		
		ConjunctivePredicate conj = new ConjunctivePredicate(preds);
		
		PhyMemoryHashJoin join = new PhyMemoryHashJoin(opl, opr, conj);
		join.open();
		assertTrue(join.hasNext());
		while (join.hasNext()) {
			NTuple tup = join.next();
			assertNotNull(tup);
			String text = new String(tup.getStringField(6));
			// Assert the output tweet contains the requested keyword
			assertTrue(text.toLowerCase().contains("macron"));
		}
		join.close();
	}
	
}
