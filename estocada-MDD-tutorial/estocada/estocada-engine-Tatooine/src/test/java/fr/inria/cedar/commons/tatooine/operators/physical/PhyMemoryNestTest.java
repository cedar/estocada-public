package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class PhyMemoryNestTest extends BaseTest {
	private static final Logger log = Logger.getLogger(PhyMemoryNestTest.class);
	
	private PhyArrayIterator makeChild() throws TatooineExecutionException{
		TupleMetadataType[] types = new TupleMetadataType[] {TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE};
		NRSMD nrsmd = new NRSMD(3, types);
		// 1st tuple
		NTuple t1 = new NTuple(nrsmd);
		t1.addString("Alpha");
		t1.addString("Beta");
		t1.addString("Gamma");
		// 2nd tuple
		NTuple t2 = new NTuple(nrsmd);
		t2.addString("Alpha"); 
		t2.addString("Delta");
		t2.addString("Epsilon");
		// 3rd tuple
		NTuple t3 = new NTuple(nrsmd);
		t3.addString("Zeta"); 
		t3.addString("Eta");
		t3.addString("Theta");
		List<NTuple> listOfTuples = Arrays.asList(t1, t2, t3);
		PhyArrayIterator op = new PhyArrayIterator(listOfTuples, nrsmd, "ThreeStrings");
		return op;
	}

	@Test
	public void testGrouping() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {
		NIterator op = makeChild();
		op.display();
		
		// Checking group by the 1st attribute, keeping 2nd and 3rd in the nested attribute
		List<Integer> groupByMask = Arrays.asList(0);
		List<Integer> nestMask = Arrays.asList(1, 2);
		
		boolean seenAlpha = false;
		boolean seenZeta = false;
		int groupAlpha = 0;
		int groupZeta = 0;
		int count = 0;
		PhyMemoryNest nest1 = new PhyMemoryNest(op, groupByMask, nestMask);
		log.info(nest1.getName(0));
		nest1.open();
		while (nest1.hasNext()){
			NTuple t = nest1.next();
			assertEquals(t.nrsmd.getColNo(), 2);
			t.display();
			count++;
			String s1 = new String(t.getStringField(0));
			if (s1.compareTo("Zeta")==0){ 
				seenZeta = true;
				groupZeta = t.getNestedField(1).size();
			}
			if (s1.compareTo("Alpha")==0){ 
				seenAlpha = true;
				groupAlpha = t.getNestedField(1).size();
			}
		}
		nest1.close();
		assertEquals(count, 2);
		assertEquals(seenZeta, true);
		assertEquals(groupZeta, 1);
		assertEquals(seenAlpha, true);
		assertEquals(groupAlpha, 2);
		
		// Checking group by the 3rd attribute, keeping only the 1st in the nested attribute
		// (the 2nd one is projected away) + we change order
		List<Integer> groupByMask2 = Arrays.asList(2);
		List<Integer> nestMask2 = Arrays.asList(0);
		PhyMemoryNest nest2 = new PhyMemoryNest(op, groupByMask2, nestMask2);
		int count2 = 0;
		boolean seenEpsilon = false;
		boolean seenGamma = false;
		boolean seenTheta = false;
		nest2.open();
		log.info(nest2.getName(0));
		while (nest2.hasNext()){
			NTuple t = nest2.next();
			assertEquals(t.nrsmd.getColNo(), 2);
			t.display();
			count2++;
			String s1 = new String(t.getStringField(0));
			if (s1.compareTo("Epsilon")==0){ 
				seenEpsilon= true;
				assertEquals(t.getNestedField(1).size(), 1);
			}
			if (s1.compareTo("Gamma")==0){ 
				seenGamma = true;
				assertEquals(t.getNestedField(1).size(), 1);
			}
			if (s1.compareTo("Theta")==0){ 
				seenTheta = true;
				assertEquals(t.getNestedField(1).size(), 1);
			}
		}
		nest2.close();
		assertEquals(count2, 3);
		assertEquals(seenEpsilon, true);
		assertEquals(seenGamma, true);
		assertEquals(seenTheta, true);
	}
}
