package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class PhyPostgresJSONEvalTest extends BaseTest {
    @Test
    public void test00() throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR("testDB");
        gsr.setProperty("url", POSTGRES_URL);

        final NRSMD nrsmd = new NRSMD(4,
                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE,
                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
                new String[] { "created_at_s", "user_name_s", "user_screen_name_s", "text_s" }, new NRSMD[0]);

        final String query =
                "select t.data->>'created_at_s' as created_at_s , t.data->>'user_name_s' as user_name_s, t.data->>'user_screen_name_s' as user_screen_name_s, t.data->>'text_s' as text_s from testPostgreJSON t";
        final PhyPostgresJSONEval phyPostgresJSONEval = new PhyPostgresJSONEval(query, gsr, nrsmd);
        phyPostgresJSONEval.open();

        assertTrue(phyPostgresJSONEval.hasNext());
        while (phyPostgresJSONEval.hasNext()) {
            NTuple tuple = phyPostgresJSONEval.next();
            assertNotNull(tuple);
            assertEquals(0, tuple.nestedFields.length);
            assertEquals(4, tuple.stringFields.length);
            assertEquals(0, tuple.integerFields.length);
        }
        phyPostgresJSONEval.close();
    }

    @Test
    public void test01() throws Exception {

        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR("testDB");
        gsr.setProperty("url", POSTGRES_URL);

        NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE },
                new String[] { "format" }, new NRSMD[0]);
        NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE },
                new String[] { "rating" }, new NRSMD[0]);

        NRSMD nrsmd = new NRSMD(3,
                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.TUPLE_TYPE,
                        TupleMetadataType.TUPLE_TYPE },
                new String[] { "id", "formats", "ratings" }, new NRSMD[] { child1, child2 });

        final String query =
                "select t.data->>'id' as id, t.data->'formats' as formats, t.data->'ratings' as ratings from testPostgreJSON1 t";
        final PhyPostgresJSONEval phyAsterixDBJSONEval = new PhyPostgresJSONEval(query, gsr, nrsmd);
        phyAsterixDBJSONEval.open();

        assertTrue(phyAsterixDBJSONEval.hasNext());
        while (phyAsterixDBJSONEval.hasNext()) {
            NTuple tuple = phyAsterixDBJSONEval.next();
            assertEquals(2, tuple.nestedFields.length);
            assertEquals(1, tuple.stringFields.length);
            assertEquals(0, tuple.integerFields.length);
            System.out.println(tuple.toString());

        }
        phyAsterixDBJSONEval.close();
    }

}
