package fr.inria.cedar.commons.tatooine.operators.logical;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/**
 * Test for the logical operator visitor.
 * @author Romain Primet
 */
public class LogOpVisitorTest {

  protected String str = "";

  void add(String s){
    str += s;
  }

  String getString(){
    return str;
  }

  @Test
  public void testPostOrderVisitor() throws Exception {

    LogOperator s = new LogSQLEval("SELECT foo FROM bar", new NRSMD(4, new TupleMetadataType[]{TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.INTEGER_TYPE}));
    LogOperator k = new LogKQLEval("SELECT bar FROM baz", new NRSMD(1, new TupleMetadataType[]{TupleMetadataType.STRING_TYPE}));
    LogOperator j = new LogJoin(s, k, new SimplePredicate("foo", 0, PredicateType.PREDICATE_EQUAL));
    LogOperator p = new LogProjection(j, new int[0]);

    p.accept(new LogOperatorVisitor(){
      @Override public void visit(LogSQLEval op){
        System.out.println("Visiting LogSQLEval");
        add("a");
      }

      @Override public void visit(LogKQLEval op){
        System.out.println("Visiting LogKQLEval");
        add("b");
      }

      @Override public void visit(LogJoin op){
        System.out.println("Visiting LogJoin");
        add("c");
      }

      @Override public void visit(LogProjection op){
        System.out.println("Visiting LogProjection");
        add("d");
      }
    });

    assertEquals("abcd", getString());
  }
}
