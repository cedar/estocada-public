package fr.inria.cedar.commons.tatooine.id;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.IDs.PrePostElementID;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;

public class NTupleReaderTest extends BaseTest {

	/**
	 * Creates a string representation of an NTuple for visualisation purposes.
	 * @param tuples
	 * @throws TatooineExecutionException
	 */
	private void display(ArrayList<NTuple> tuples) throws TatooineExecutionException {
		Iterator<NTuple> it = tuples.iterator();
		while (it.hasNext()) {
			NTuple tuple = it.next();
			tuple.display();
		}
	}

	@Test
	// Tests the creation of simple (non-nested) NTuples from a TXT file
	public void testSimple() throws TatooineExecutionException {
		// "P01.txt" contains 2 simple tuples with no nesting
		NTupleReader reader = new NTupleReader(path + "/txt/p01.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(2, tuples.size());
		assertEquals(4, tuples.get(0).stringFields.length);
		assertEquals(4, tuples.get(1).stringFields.length);
		assertEquals(3, tuples.get(0).integerFields.length);
		assertEquals(3, tuples.get(1).integerFields.length);
	}

	@Test
	// Tests the creation of simply-nested NTuples from a TXT file
	public void testSimplyNested() throws TatooineExecutionException {
		// "P02.txt" contains 5 simply-nested tuples
		NTupleReader reader = new NTupleReader(path + "/txt/p02.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(5, tuples.size());
		assertEquals(9, tuples.get(0).integerFields.length);
		assertEquals(4, tuples.get(0).stringFields.length);
		assertEquals(5, tuples.get(0).nestedFields.length);

		// ("abc")
		assertEquals('a', tuples.get(0).nestedFields[2].get(0).stringFields[0][0]);
		assertEquals('b', tuples.get(0).nestedFields[2].get(0).stringFields[0][1]);
		assertEquals('c', tuples.get(0).nestedFields[2].get(0).stringFields[0][2]);
	}

	@Test
	// Tests the creation of triply-nested NTuples from a TXT file
	public void testTriplyNested() throws TatooineExecutionException {
		// "P03.txt" contains 1 triply-nested tuple
		NTupleReader reader = new NTupleReader(path + "/txt/p03.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(1, tuples.size());
		assertEquals(2, tuples.get(0).integerFields.length);
		assertEquals(3, tuples.get(0).nestedFields.length);

		/* Integer fields */
		// INT: 1
		assertEquals(1, tuples.get(0).integerFields[0]);
		// INT: 5
		assertEquals(5, tuples.get(0).integerFields[1]);

		/* Nested fields */
		// (2,(3),4)
		assertEquals(2, tuples.get(0).nestedFields[0].get(0).integerFields[0]);
		assertEquals(4, tuples.get(0).nestedFields[0].get(0).integerFields[1]);
		assertEquals(3, tuples.get(0).nestedFields[0].get(0).nestedFields[0].get(0).integerFields[0]);
		// (6)
		assertEquals(6, tuples.get(0).nestedFields[1].get(0).integerFields[0]);
		// (7,8,(9))
		assertEquals(7, tuples.get(0).nestedFields[2].get(0).integerFields[0]);
		assertEquals(8, tuples.get(0).nestedFields[2].get(0).integerFields[1]);
		assertEquals(9, tuples.get(0).nestedFields[2].get(0).nestedFields[0].get(0).integerFields[0]);
	}

	@Test
	// Tests the creation of simple (non-nested) NTuples from a TXT file
	public void testSimpleIDs() throws TatooineExecutionException {
		// "P04.txt" contains 1 simple tuple with only ID elements and no nesting
		NTupleReader reader = new NTupleReader(path + "/txt/p04.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(1, tuples.size());

		/* ID fields */
		assertEquals(8, tuples.get(0).idFields.length);
		// ID: 89
		assertEquals(89, ((OrderedIntegerElementID) tuples.get(0).idFields[0]).n);
		// ID: 22
		assertEquals(22, ((OrderedIntegerElementID) tuples.get(0).idFields[1]).n);
		// ID: 45
		assertEquals(45, ((OrderedIntegerElementID) tuples.get(0).idFields[2]).n);
		// ID: 32
		assertEquals(32, ((OrderedIntegerElementID) tuples.get(0).idFields[7]).n);
		// ID: 9089
		assertEquals(9089, ((PrePostElementID) tuples.get(0).idFields[4]).pre);
		// ID: 9089
		assertEquals(351, ((PrePostElementID) tuples.get(0).idFields[5]).pre);
	}

	@Test
	// Tests the creation of nested NTuples from a TXT file
	public void testNestedIDs() throws TatooineExecutionException {
		// "P05.txt" contains 1 nested tuple with only ID elements
		NTupleReader reader = new NTupleReader(path + "/txt/p05.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(1, tuples.size());
		assertEquals(4, tuples.get(0).idFields.length);
		assertEquals(1, tuples.get(0).nestedFields.length);

		/* ID fields */
		// ID: 900
		assertEquals(900, ((OrderedIntegerElementID) tuples.get(0).idFields[0]).n);
		// ID: 315617
		assertEquals(315617, ((OrderedIntegerElementID) tuples.get(0).idFields[1]).n);
		// ID: 2
		assertEquals(2, ((OrderedIntegerElementID) tuples.get(0).idFields[2]).n);
		// ID: 7
		assertEquals(7, ((OrderedIntegerElementID) tuples.get(0).idFields[3]).n);

		/* Nested fields */
		// 89
		assertEquals(89, ((OrderedIntegerElementID) tuples.get(0).nestedFields[0].get(0).idFields[0]).n);
		// 45
		assertEquals(45, ((OrderedIntegerElementID) tuples.get(0).nestedFields[0].get(0).idFields[1]).n);
		// 22
		assertEquals(22,
				((OrderedIntegerElementID) tuples.get(0).nestedFields[0].get(0).nestedFields[0].get(0).idFields[0]).n);
	}

	@Test
	// Tests empty string values
	public void testEmptyString() throws TatooineExecutionException {
		// "P06.txt" contains a nested tuple with empty string values
		NTupleReader reader = new NTupleReader(path + "/txt/p06.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);

		// Number of fields
		assertEquals(2, tuples.get(0).stringFields.length);
		assertEquals(1, tuples.get(0).nestedFields.length);
		assertEquals(2, tuples.get(0).nestedFields[0].get(0).stringFields.length);
		assertEquals(1, tuples.get(0).nestedFields[0].get(0).nestedFields.length);

		/* String fields */
		assertEquals(0, tuples.get(0).stringFields[0].length);
		assertEquals(0, tuples.get(0).stringFields[1].length);

		/* Nested fields */
		assertEquals(0, tuples.get(0).nestedFields[0].get(0).stringFields[0].length);
		assertEquals(0, tuples.get(0).nestedFields[0].get(0).stringFields[1].length);
		assertEquals(1, tuples.get(0).nestedFields[0].get(0).nestedFields[0].get(0).stringFields.length);
	}
}
