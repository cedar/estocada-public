package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.junit.Test;
import org.mockito.Mockito;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.storage.database.SOLRSourceLayer;

public class PhySOLRScanTest extends PhySOLRUnitTest {
	
	@Test	
	/** Queries for all information available on books stored as nested JSON documents in SOLR */
	public void testBooks() throws TatooineExecutionException, SolrServerException, IOException, TatooineException {	
		String queryStr = "*.*";
		String fieldStr = "";
		NRSMD nrsmd = getNRSMDBooks();
		QueryResponse response = getQueryResponseBooks();

		PhySOLRScan searcher = mock(PhySOLRScan.class, Mockito.CALLS_REAL_METHODS);
		SOLRSourceLayer source = mock(SOLRSourceLayer.class);
		searcher.setDBSourceLayer(source);
		searcher.setQuery(queryStr);
		searcher.setField(fieldStr);
		searcher.setNRSMD(nrsmd);
		
		Mockito.doNothing().when(searcher).close();
		Mockito.doNothing().when(source).connect();
		when(source.getBatchRecords(new String[] {queryStr, fieldStr})).thenReturn(response, new QueryResponse());
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertNotNull(tuple.nrsmd);
			
			int indexF = tuple.nrsmd.getColIndexFromName("formats");
			int indexR = tuple.nrsmd.getColIndexFromName("ratings");
			
			// Formats
			List<NTuple> formats = tuple.getNestedField(indexF);
			assertFalse(formats.isEmpty());
			for (NTuple format: formats) {
				assertTrue(format.nrsmd.colNames[0].contains("format"));
				assertEquals(TupleMetadataType.STRING_TYPE.toString(), format.nrsmd.getColumnMetadata(0));
			}
			
			// Ratings
			List<NTuple> ratings = tuple.getNestedField(indexR);
			assertFalse(formats.isEmpty());
			for (NTuple rating: ratings) {
				assertTrue(rating.nrsmd.colNames[0].contains("rating"));
				assertEquals(TupleMetadataType.INTEGER_TYPE.toString(), rating.nrsmd.getColumnMetadata(0));				
			}
		}
		searcher.close();
	}
	
}
