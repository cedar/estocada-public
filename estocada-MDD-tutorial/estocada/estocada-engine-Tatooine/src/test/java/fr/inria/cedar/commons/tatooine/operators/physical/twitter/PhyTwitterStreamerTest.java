package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.OrderedLongElementID;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class PhyTwitterStreamerTest extends BaseTest {
	
	@Test
	// Test PhyTwitterStreamer with no input parameters: sample mode
	public void testTwitterStreamerSample() throws TatooineExecutionException {
		PhyTwitterStreamer streamer = new PhyTwitterStreamer(null, null, null, null, 15);		
		streamer.open();	
		assertTrue(streamer.hasNext());
		while (streamer.hasNext()) {
			NTuple tuple = streamer.next();
			if (tuple != null) {
				String text = String.valueOf((char[]) tuple.getValue("text"));
				assertFalse(Strings.isNullOrEmpty(text));
			}
		}
		streamer.close();
	}
	
	@Test
	// Test PhyTwitterStreamer with keywords as input parameters
	public void testTwitterStreamerFilteredByKWs() throws TatooineExecutionException {
		List<String> keywords = new ArrayList<String>();
		keywords.add("bieber");
		List<String> languages = new ArrayList<String>();
		languages.add("en");		
		PhyTwitterStreamer streamer = new PhyTwitterStreamer(null, keywords, languages, null, 15);		
		streamer.open();
		assertTrue(streamer.hasNext());
		while (streamer.hasNext()) {
			NTuple tuple = streamer.next();
			String text = String.valueOf((char[]) tuple.getValue("text"));
			// Note: Sometimes keywords are not present in the text of the tweet itself
			// But are part of the expanded URLs present in the tweet
			assertTrue(text.toLowerCase().contains("bieber") || text.toLowerCase().contains("https"));
		}
		streamer.close();
	}
	
	@Test
	// Test PhyTwitterStreamer with user IDs as input parameters
	public void testTwitterStreamerFilteredByIDs() throws TatooineExecutionException {
		// Twitter pages of famous French and international media (name, screen name, ID): 
		// Le Monde, @lemonde, 24744541
		// Le Figaro, @Le_Figaro, 8350912
		// Libération, @libe, 68440549
		// RFI, @RFI, 32861321
		// Europe1, @Europe1, 34570323
		// Rue89, @Rue89, 19341180
		// Vice FR, @vicefr, 34605611
		// Vice US, @vice, 23818581
		// The Guardian, @guardian, 87818409
		// Daily Mail US, @DailyMail, 380285402
		// USA Today, @USAToday, 15754281
		// The New York Times, @nytimes, 807095
		// CNN, @CNN, 759251
		// BBC News, @BBC, 612473
		// CBC News, @CBC, 6433472
		List<Long> userIDs = new ArrayList<>(Arrays.asList(24744541L, 8350912L, 68440549L,
				32861321L, 34570323L, 19341180L, 34605611L, 23818581L, 87818409L, 380285402L, 
				15754281L, 807095L, 759251L, 612473L, 6433472L));
		List<String> tags = new ArrayList<>(Arrays.asList("lemonde", "figaro", "libe", "rfi", "europe1", 
				"rue89", "vice", "guardian", "dailymail", "usatoday", "nytimes", "cnn", "bbc", "cbc"));
		
		PhyTwitterStreamer streamer = new PhyTwitterStreamer(userIDs, null, null, null, 30);		
		streamer.open();
		assertTrue(streamer.hasNext());
		while (streamer.hasNext()) {
			NTuple tuple = streamer.next();
			// Note: Sometimes user screen names are not present in the text of the tweet itself
			// But are part of the expanded URLs in the tweet or the twitter user description
			// Example: {@link https://twitter.com/NicolasBourcier/status/824553024622329856}
			// Example: {@link https://twitter.com/callummay/status/824920642290073601}
			Long userID = ((OrderedLongElementID) tuple.getValue("user_id")).n;
			String text = String.valueOf((char[]) tuple.getValue("text")).toLowerCase();
			String userDescription = String.valueOf((char[]) tuple.getValue("user_description")).toLowerCase();
			boolean isATweetByMedia = userIDs.contains(userID);
			boolean isARetweetOfTweetByMedia = tags.parallelStream().anyMatch(text::contains);
			boolean hasExtendedURL = text.contains("https");
			boolean tagIsInUserDescription = tags.parallelStream().anyMatch(userDescription::contains);
			assertTrue(isATweetByMedia || isARetweetOfTweetByMedia || hasExtendedURL || tagIsInUserDescription);
		}
		streamer.close();
	}			
}
