package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.PredicateDataType;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

public class PhySelectionTest extends BaseTest {

	/**
	 * Creates a string representation of an NTuple for visualisation purposes.
	 * @param tuples
	 * @throws TatooineExecutionException
	 */
	private void display(ArrayList<NTuple> tuples) throws TatooineExecutionException {
		Iterator<NTuple> it = tuples.iterator();
		while (it.hasNext()) {
			NTuple tuple = it.next();
			tuple.display();
		}
	}

	@Test
	// Test selection of tuples
	public void testSelection() throws TatooineExecutionException {
		// "P08.txt" contains 25 simple tuples containing information about books
		NTupleReader reader = new NTupleReader(path + "/txt/p08.txt");
		ArrayList<NTuple> tuples = reader.read();

		// Display
		display(tuples);
		
		// We want to retrieve books with less than 300 pages
		SimplePredicate pred = new SimplePredicate(3, 300, PredicateType.PREDICATE_LESS_THAN_OR_EQUAL, PredicateDataType.PREDICATE_ON_INTEGER);
				
		// Only some books in our inventory have less than 300 pages
		assertFalse(pred.isTrue(tuples.get(10)));
		assertTrue(pred.isTrue(tuples.get(11)));
		assertFalse(pred.isTrue(tuples.get(12)));
		assertFalse(pred.isTrue(tuples.get(13)));
		assertFalse(pred.isTrue(tuples.get(14)));
		assertFalse(pred.isTrue(tuples.get(15)));
		assertTrue(pred.isTrue(tuples.get(16)));
		assertFalse(pred.isTrue(tuples.get(17)));
		assertFalse(pred.isTrue(tuples.get(18)));
		assertTrue(pred.isTrue(tuples.get(19)));
	}

}
