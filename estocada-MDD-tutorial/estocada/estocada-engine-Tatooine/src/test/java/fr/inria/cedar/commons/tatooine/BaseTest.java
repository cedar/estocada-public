package fr.inria.cedar.commons.tatooine;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Ignore;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.loader.DatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.SolrDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

@Ignore
public class BaseTest {

	private static boolean initialized = false;
	protected static String path;
	protected static String solrConfig;

	@BeforeClass
	// Initializes parameters
	public static void initialize() throws IOException {
		if (!initialized) {
			try {
				Parameters.init();
				path = Parameters.getProperty("resources");
				solrConfig = Parameters.getProperty("solrConfigSets");
				initialized = true;
			} catch (TatooineException e) {
				e.printStackTrace();
			}
		}
	}

	/** Creates and loads a SOLR collection from an input JSON file */
	protected static void loadCollectionSOLR(String collectionName, String collectionPath) throws Exception {
		StorageReference ref = new GSR(collectionName);
		ref.setProperty("serverUrl", SolrDatabaseLoader.DEFAULT_SOLR_URL);
		ref.setProperty("serverPort", SolrDatabaseLoader.DEFAULT_SOLR_PORT);

		DatabaseLoader databaseLoader = new SolrDatabaseLoader(solrConfig, ref);
		String dataCollectionName = databaseLoader.getCatalogEntryName();

		// Checks if the collection already exists in SOLR
		if (databaseLoader.isDataLoaded(dataCollectionName)) {
			return;
		}

		// Loads the data into SOLR and registers a catalog entry
		databaseLoader.load(collectionPath);
		databaseLoader.registerNewCatalogEntry();
	}
}
