package fr.inria.cedar.commons.tatooine;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

public class NTupleTest extends BaseTest {

	@Test
	// Test correct creation of the string representation of an NTuple
	public void testDisplay() throws TatooineException, TatooineExecutionException {
		TupleMetadataType[] types = new TupleMetadataType[] { TupleMetadataType.ORDERED_ID,
				TupleMetadataType.STRING_TYPE, TupleMetadataType.INTEGER_TYPE };
		char[][] stringFields = new char[][] { { 'u', 'n', 'i', 't' } };
		ElementID[] idFields = new ElementID[] { new OrderedIntegerElementID(2500) };
		@SuppressWarnings("unchecked")
		ArrayList<NTuple>[] nestedFields = new ArrayList[0];
		NTuple tuple = new NTuple(3, types, null, stringFields, new char[][] {}, idFields, new int[] { 5 },
				nestedFields);
		tuple.display();
	}

	@Test
	// Test construction of a NTuple from a simple NRSMD
	public void testConstructionOfSimpleTuple() throws Exception {
		NRSMD n1;
		TreePattern tp1;
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();

		// TP column names: [Document ID, 1.ID, 2.ID, 3.Val, 4.ID, 4.Cont]
		tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");

		// NRSMD types: [URI, ID, ID, String, ID, String]
		n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true, map);

		// Assert number of NRSMD types
		assertEquals(n1.uriNo, 1);
		assertEquals(n1.stringNo, 2);
		assertEquals(n1.prePostIDsNo + n1.idsNo + n1.cddIDsNo, 3);

		// Create NTuple from NRSMD
		NTuple tuple = new NTuple(n1);

		// Assert number of NTuple types
		assertEquals(tuple.uriFields.length, 1);
		assertEquals(tuple.idFields.length, 3);
		assertEquals(tuple.stringFields.length, 2);
	}

	@Test
	// Test construction of a NTuple from a nested NRSMD
	public void testConstructionOfNestedTuple() throws Exception {
		NRSMD n1, n2;
		TreePattern tp1, tp2;
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> map = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();

		// TP column names: [Document ID, 1.ID, 2.ID, 3.Val, 4.ID, 4.Cont]
		tp1 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p01.xam");
		// TP column names: [Document ID, 1.ID, 1.Cont, 2.ID, 3.Cont]
		tp2 = XAMParserUtility.getTreePatternFromFile(path + "/xam/p02.xam");

		n1 = NRSMD.getNRSMD((TreePatternNode) tp1.getRoot(), true, map);
		n2 = NRSMD.getNRSMD((TreePatternNode) tp2.getRoot(), true, map);

		// Add nested field
		// NRSMD types: [URI, ID, ID, String, ID, String, Tuple]
		NRSMD n3 = NRSMD.addNestedField(n1, n2);

		// Assert number of NRSMD types
		assertEquals(n3.uriNo, 1);
		assertEquals(n3.stringNo, 2);
		assertEquals(n3.prePostIDsNo + n1.idsNo + n1.cddIDsNo, 3);
		assertEquals(n3.nestedNo, 1);

		// Create NTuple from NRSMD
		NTuple tuple = new NTuple(n3);

		// Assert number of NTuple types
		assertEquals(tuple.uriFields.length, 1);
		assertEquals(tuple.idFields.length, 3);
		assertEquals(tuple.stringFields.length, 2);
		assertEquals(tuple.nestedFields.length, 1);
	}
}
