package fr.inria.cedar.commons.tatooine.optimization;

import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.CatalogTest;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyJENAEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLRScan;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLScan;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyMemoryHashJoin;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/** 
 * Each test in this class creates a PhyHashJoin, transforms it into a PhyBindJoin, and verifies they both retrieve the same set of NTuple objects */
public class HashJoinToBindJoinTransformationTest extends CatalogTest {

	private static final Catalog catalog = Catalog.getInstance();
	private static final String SQL_DB_URL_POL = "jdbc:derby:memory:testDB;create=true;user=jointestpol;password=jointestpol";
	private static final String SQL_DB_URL_UNI = "jdbc:derby:memory:testDB;create=true;user=jointestuni;password=jointestuni";
	
	/* Setup */	
	@BeforeClass
	public static void setUp() throws Exception {
		DriverManager.registerDriver(new org.apache.derby.jdbc.EmbeddedDriver());
		
		Connection connPol = DriverManager.getConnection(SQL_DB_URL_POL);
		createTablePol(connPol);
		insertRowPol(connPol, 1, "Nicolas Sarkozy", "nicolassarkozy", "28-01-1955");
		insertRowPol(connPol, 2, "Alain Juppé", "alainjuppe", "15-08-1945");
		insertRowPol(connPol, 3, "François Fillon", "francoisfillon", "04-03-1954");
		insertRowPol(connPol, 4, "Jean-François Copé", "jf_cope", "05-05-1964");
		insertRowPol(connPol, 5, "Bruno Le Maire", "brunolemaire", "15-04-1969");
		insertRowPol(connPol, 6, "Nathalie Kosciusko-Morizet", "nk_m", "14-05-1973");
		insertRowPol(connPol, 7, "Jean-Frédéric Poisson", "jfpoisson78", "22-01-1963");
		insertRowPol(connPol, 8, "François Hollande", "francoishollande.fr", "12-08-1954");
		
		Connection connUni = DriverManager.getConnection(SQL_DB_URL_UNI);
		createTableUni(connUni);
		insertRowUni(connUni, 1, "Emanuela Guerra", "GEOG", "GEOG395");
		insertRowUni(connUni, 2, "Emanuela Guerra", "GEOG", "LANG221");
		insertRowUni(connUni, 3, "Emanuela Guerra", "GEOG", "LANG505");		
		insertRowUni(connUni, 4, "Jennifer Chan", "GEOG", "HIST380");
		insertRowUni(connUni, 5, "Jennifer Chan", "GEOG", "LANG435");
		insertRowUni(connUni, 6, "Jennifer Chan", "GEOG", "GEOG380");
		
		loadCollectionSOLR("SocialMediaSOLR", path + "/json/SocialMediaSOLR.json");
		loadCollectionSOLR("UniScheduleSOLR", path + "/json/courses.json");
	}
		
	private static void createTablePol(Connection conn) throws Exception {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE SCHEMA JOINTESTPOL");
		stmt.executeUpdate("CREATE TABLE JOINTESTPOL.Politicians(id_i INTEGER NOT NULL, name_s VARCHAR(50), user_screen_name_s VARCHAR(50), dob_s VARCHAR(10))");
	}
	
	private static void createTableUni(Connection conn) throws Exception {
		Statement stmt = conn.createStatement();
		stmt.executeUpdate("CREATE SCHEMA JOINTESTUNI");
		stmt.executeUpdate("CREATE TABLE JOINTESTUNI.Courses(id_i INTEGER NOT NULL, prof_name_s VARCHAR(50), dept_name_s VARCHAR(4), course_id_s VARCHAR(7))");
	}

	private static void insertRowPol(Connection conn, int id, String name, String twitter, String dob) throws Exception {
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO JOINTESTPOL.Politicians VALUES (?, ?, ?, ?)");
		stmt.setInt(1, id);
		stmt.setString(2, name);
		stmt.setString(3, twitter);
		stmt.setString(4, dob);
		stmt.executeUpdate();
	}
	
	private static void insertRowUni(Connection conn, int idProf, String profName, String deptName, String idCourse) throws Exception {
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO JOINTESTUNI.Courses VALUES (?, ?, ?, ?)");
		stmt.setInt(1, idProf);
		stmt.setString(2, profName);
		stmt.setString(3, deptName);
		stmt.setString(4, idCourse);
		stmt.executeUpdate();
	}
	
	private PhyJENAEval createJenaUniversityDS(String queryJena) throws Exception {
		NRSMD nrsmdJena = new NRSMD(3, 
			new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
			new String[] { "professor_name_s", "course_id_s", "dept_name_s" }, 
			new NRSMD[0]);		
		StorageReference refJena = new GSR("UniProfessorsJENA");
		refJena.setProperty("modelRDF", path + "/rdf/professors.ttl");		
		return new PhyJENAEval(queryJena, (GSR) refJena, nrsmdJena);
	}
	
	private PhySOLRScan createSolrUniversityDS(String querySolr) throws Exception {
		NRSMD nrsmdSolr = new NRSMD(4, 
			new TupleMetadataType[] { 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.INTEGER_TYPE, 
				TupleMetadataType.STRING_TYPE }, 
			new String[] { "course_id_s", "dept_name_s", "course_no_i", "course_name_s" }, 
			new NRSMD[0]);		
		StorageReference refSolr = new GSR("UniScheduleSOLR");
		refSolr.setProperty("serverUrl", "http://localhost");
		refSolr.setProperty("serverPort", "8983");
		refSolr.setProperty("coreName", "UniScheduleSOLR");		
		return new PhySOLRScan(querySolr, "", (GSR) refSolr, nrsmdSolr);
	}
		
	@Test
	/** Apache Jena is the right operator of the PhyBindJoin and there is only one common JOIN column */
	public void testJenaRightOPOneCommonCol() throws Exception {
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "Macron";
		// Select Twitter ID + first name + last name of all French politicians
		String queryJena = "PREFIX fm: <http://example.org/schemas/> "
			+ "SELECT DISTINCT ?user_screen_name_s ?name_s ?surname_s "
			+ "WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:twitter ?user_screen_name_s . }";
		
		// Properties
		ViewSchema schemaSolr = catalog.getViewSchema("SocialMediaSOLR");
		GSR refSolr = (GSR) catalog.getStorageReference("SocialMediaSOLR"); 
		ViewSchema schemaJena = catalog.getViewSchema("TwitterIDsJENA");
		GSR refJena = (GSR) catalog.getStorageReference("TwitterIDsJENA");
		
		// Operators
		PhySOLRScan opl = new PhySOLRScan(querySolr, "", refSolr, schemaSolr);
		PhyJENAEval opr = new PhyJENAEval(queryJena, refJena, schemaJena);				
		int indexSolr = opl.nrsmd.getColIndexFromName("user_screen_name_s");
		int indexJena = opr.nrsmd.getColIndexFromName("user_screen_name_s") + opl.nrsmd.colNo;
		SimplePredicate pred = new SimplePredicate(indexSolr, indexJena);
		
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, pred));		
	}	
	
	@Test
	/** Apache Jena is the right operator of the PhyBindJoin and there are two common JOIN columns */
	public void testJenaRightOPTwoCommonCol() throws Exception {
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "*:*";
		// Select Name of Professor + Course ID + Name of Department
		String queryJena = "PREFIX uni: <http://uni.courses.org/schemas/> "
			+ "SELECT DISTINCT ?professor_name_s ?course_id_s ?dept_name_s "
			+ "WHERE { ?prof uni:dept ?dept_name_s . ?prof uni:coid ?course_id_s . ?prof uni:name ?professor_name_s . } ";
				
		// Operators
		PhySOLRScan opl = createSolrUniversityDS(querySolr);
		PhyJENAEval opr = createJenaUniversityDS(queryJena);
		
		int indexSolr1 = opl.nrsmd.getColIndexFromName("course_id_s");
		int indexJena1 = opr.nrsmd.getColIndexFromName("course_id_s") + opl.nrsmd.colNo;
		int indexSolr2 = opl.nrsmd.getColIndexFromName("dept_name_s");
		int indexJena2 = opr.nrsmd.getColIndexFromName("dept_name_s") + opl.nrsmd.colNo;	
		
		// Conjunctive predicate
		Predicate[] preds = new Predicate[2];
		preds[0] = new SimplePredicate(indexSolr1, indexJena1);
		preds[1] = new SimplePredicate(indexSolr2, indexJena2);		
		ConjunctivePredicate conj = new ConjunctivePredicate(preds);
		
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, conj));	
	}
		
	@Test
	/** Apache Solr is the right operator of the PhyBindJoin and there is only one common JOIN column */
	public void testSolrRightOPOneCommonCol() throws Exception {		
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "*:*";
		// Select the Twitter ID of all Europe Écologie les Verts (EELV) senators
		String queryJena = "PREFIX fm: <http://example.org/schemas/> "
			+ "SELECT DISTINCT ?user_screen_name_s "
			+ "WHERE { ?pol fm:party fm:PAR00020 . ?pol fm:position fm:POS00006 . ?pol fm:twitter ?user_screen_name_s . }";
		
		// Properties
		ViewSchema schemaSolr = catalog.getViewSchema("SocialMediaSOLR");
		GSR refSolr = (GSR) catalog.getStorageReference("SocialMediaSOLR");
		GSR refJena = (GSR) catalog.getStorageReference("TwitterIDsJENA");
		NRSMD nrsmdJena = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "user_screen_name_s" }, new NRSMD[0]);	
		
		// Operators
		PhyJENAEval opl = new PhyJENAEval(queryJena, refJena, nrsmdJena);				
		PhySOLRScan opr = new PhySOLRScan(querySolr, "", refSolr, schemaSolr);
		int indexJena = opl.nrsmd.getColIndexFromName("user_screen_name_s");
		int indexSolr = opr.nrsmd.getColIndexFromName("user_screen_name_s") + opl.nrsmd.colNo;
		SimplePredicate pred = new SimplePredicate(indexJena, indexSolr);
		
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, pred));
	}	
	
	@Test
	/** Apache Solr is the right operator of the PhyBindJoin and there are two common JOIN columns */
	public void testSolrRightOPTwoCommonCol() throws Exception {	
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "*:*";
		// Select Name of Professor + Course ID + Name of Department
		String queryJena = "PREFIX uni: <http://uni.courses.org/schemas/> "
			+ "SELECT DISTINCT ?professor_name_s ?course_id_s ?dept_name_s "
			+ "WHERE { ?prof uni:dept ?dept_name_s . ?prof uni:coid ?course_id_s . ?prof uni:name ?professor_name_s . } ";
		
		// Operators
		PhyJENAEval opl = createJenaUniversityDS(queryJena);
		PhySOLRScan opr = createSolrUniversityDS(querySolr);
		
		int indexJena1 = opl.nrsmd.getColIndexFromName("course_id_s");
		int indexSolr1 = opr.nrsmd.getColIndexFromName("course_id_s") + opl.nrsmd.colNo;
		int indexJena2 = opl.nrsmd.getColIndexFromName("dept_name_s");
		int indexSolr2 = opr.nrsmd.getColIndexFromName("dept_name_s") + opl.nrsmd.colNo;
		
		// Conjunctive predicate
		Predicate[] preds = new Predicate[2];
		preds[0] = new SimplePredicate(indexJena1, indexSolr1);
		preds[1] = new SimplePredicate(indexJena2, indexSolr2);		
		ConjunctivePredicate conj = new ConjunctivePredicate(preds);
		
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, conj));
	}
	
	@Test
	/** SQL is the right operator of the PhyBindJoin and there is only one common JOIN column */
	public void testSQLRightOPOneCommonCol() throws Exception {		
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "france";
		// Select all info on politicians who were born in the 50s
		String querySQL = "SELECT * FROM Politicians WHERE dob_s LIKE '__-__-195_'";
		
		// Properties
		ViewSchema schemaSolr = catalog.getViewSchema("SocialMediaSOLR");
		GSR refSolr = (GSR) catalog.getStorageReference("SocialMediaSOLR");	
		StorageReference refSQL = new GSR("PoliticiansSQL");	
		refSQL.setProperty("url", "jdbc:derby:memory:testDB;create=true;user=jointestpol;password=jointestpol");		
		NRSMD nrsmdSQL = new NRSMD(4, 
			new TupleMetadataType[] { 
				TupleMetadataType.INTEGER_TYPE, 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.STRING_TYPE },
			new String[] { "id_i", "name_s", "user_screen_name_s", "dob_s" }, new NRSMD[0] );
						
		// Operators
		PhySOLRScan opl = new PhySOLRScan(querySolr, "", refSolr, schemaSolr);		
		PhySQLScan opr = new PhySQLScan(querySQL, refSQL, nrsmdSQL);				
		int indexSolr = opl.nrsmd.getColIndexFromName("user_screen_name_s");
		int indexSQL = nrsmdSQL.getColIndexFromName("user_screen_name_s") + opl.nrsmd.colNo;
		SimplePredicate pred = new SimplePredicate(indexSolr, indexSQL);
				
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, pred));
	}
	
	@Test
	/** SQL is the right operator of the PhyBindJoin and there are two common JOIN columns */
	public void testSQLRightOPTwoCommonCol() throws Exception {		
		// 1. Creation of PhyHashJoin
		// Queries
		String querySolr = "*:*";
		// Select all info on professors and the classes they teach
		String querySQL = "SELECT * FROM Courses";
		
		// Properties		
		StorageReference refSQL = new GSR("UniScheduleSQL");	
		refSQL.setProperty("url", "jdbc:derby:memory:testDB;create=true;user=jointestuni;password=jointestuni");		
		NRSMD nrsmdSQL = new NRSMD(4, 
			new TupleMetadataType[] { 
				TupleMetadataType.INTEGER_TYPE, 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.STRING_TYPE, 
				TupleMetadataType.STRING_TYPE },
			new String[] { "id_i", "prof_name_s", "dept_name_s", "course_id_s" }, new NRSMD[0] );
		
		// Operators	
		PhySOLRScan opl = createSolrUniversityDS(querySolr);
		PhySQLScan opr = new PhySQLScan(querySQL, refSQL, nrsmdSQL);	
		
		int indexSolr1 = opl.nrsmd.getColIndexFromName("course_id_s");
		int indexSql1 = opr.nrsmd.getColIndexFromName("course_id_s") + opl.nrsmd.colNo;
		int indexSolr2 = opl.nrsmd.getColIndexFromName("dept_name_s");
		int indexSql2 = opr.nrsmd.getColIndexFromName("dept_name_s") + opl.nrsmd.colNo;	
		
		// Conjunctive predicate
		Predicate[] preds = new Predicate[2];
		preds[0] = new SimplePredicate(indexSolr1, indexSql1);
		preds[1] = new SimplePredicate(indexSolr2, indexSql2);		
		ConjunctivePredicate conj = new ConjunctivePredicate(preds);
		
		transformAndAssert(new PhyMemoryHashJoin(opl, opr, conj));
	}
	
	/** Transforms a PhyHashJoin into a PhyBindJoin and verifies they produce the same set of NTuple objects as output */
	private void transformAndAssert(PhyMemoryHashJoin hash) throws TatooineExecutionException {
		Set<NTuple> hashNTuples = new HashSet<NTuple>();
		hash.open();
		while (hash.hasNext()) {
			NTuple tup = hash.next();
			System.out.println("@@@ [H]: " + tup);
			hashNTuples.add(tup);
		}
		
		// 2. Transformation		
		HashJoinToBindJoinTransformation transformer = new HashJoinToBindJoinTransformation();
		assertTrue(transformer.isApplicable(hash));
		PhyBindJoin bind = (PhyBindJoin) transformer.apply(hash);
		
		// 3. Verification
		Set<NTuple> bindNTuples = new HashSet<NTuple>();
		bind.open();
		while (bind.hasNext()) {
			NTuple tup = bind.next();
			System.out.println("@@@ [B]: " + tup);
			bindNTuples.add(tup);
		}
		
		assertTrue(hashNTuples.equals(bindNTuples));
	}
	
}
