package fr.inria.cedar.commons.tatooine.operators.physical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class PhyJQEvalIntegrationTest extends BaseTest {
	@Test
	public void testTweets() throws TatooineExecutionException {
		String json = path + "/json/tweets.json";

		// Setting properties
		GSR gsr = new GSR("spark");
		gsr.setProperty("table", "tweets");
		gsr.setProperty("frame", json);
		
		NRSMD nrsmd = new NRSMD(4, 
				new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE }, 
				new String[] {"created_at_s", "user_name_s", "user_screen_name_s", "text_s" }, 
				new NRSMD[0]);
		
		String query = "SELECT created_at_s, user_name_s, user_screen_name_s, text_s FROM tweets";
		PhyJQEval searcher = new PhyJQEval(query, gsr, nrsmd);
		searcher.open();
		
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertEquals(0, tuple.nestedFields.length);
			assertEquals(4, tuple.stringFields.length);
			assertEquals(0, tuple.integerFields.length);
		}
		searcher.close();
	}
	
	private PhyJQEval buildEval(String query) throws TatooineExecutionException {
		String json = path + "/json/books.json";

		// Setting properties
		GSR gsr = new GSR("spark");
		gsr.setProperty("table", "books");
		gsr.setProperty("frame", json);
		
		NRSMD child1 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { "format" }, new NRSMD[0]);
		NRSMD child2 = new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { "rating" }, new NRSMD[0]);
		
		NRSMD nrsmd = new NRSMD(9, new TupleMetadataType[] {
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.STRING_TYPE,
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.TUPLE_TYPE,
				TupleMetadataType.INTEGER_TYPE,
				TupleMetadataType.TUPLE_TYPE }, new String[] { "id", "name_s", 
				"author_s", "series_s", "sequence_i", "pages_i", "formats", 
				"price_i", "ratings" }, new NRSMD[] { child1, child2 });
		
		PhyJQEval searcher = new PhyJQEval(query, gsr, nrsmd);		
		return searcher;
	}
	
	@Test
	public void testBooks() throws TatooineExecutionException {
		PhyJQEval searcher = buildEval("SELECT name_s, pages_i FROM books");
		searcher.open();
		
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tuple = searcher.next();
			assertNotNull(tuple);
			assertEquals(0, tuple.nestedFields.length);
			assertEquals(1, tuple.stringFields.length);
			assertEquals(1, tuple.integerFields.length);
		}
		searcher.close();
	}
	
	@Test(expected = TatooineExecutionException.class)
	public void testScan() throws TatooineExecutionException {
		buildEval("SELECT * FROM books");
	}	

}
