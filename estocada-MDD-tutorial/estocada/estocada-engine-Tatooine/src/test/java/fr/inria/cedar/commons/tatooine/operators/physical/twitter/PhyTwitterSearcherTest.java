package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.BaseTest;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.OrderedLongElementID;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class PhyTwitterSearcherTest extends BaseTest {
	
	@Test
	// Retrieve the most recent 150 tweets by Donald Trump
	public void testTwitterSearcherUserId() throws TatooineExecutionException {
		// Twitter user ID of Donald Trump
		long userID = 25073877L;
		PhyTwitterSearcher searcher = new PhyTwitterSearcher(userID, null, null, TwitterSearcherEnum.USER_ID, 150);	
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tweet = searcher.next();
			assertEquals(((OrderedLongElementID) tweet.getValue("user_id")).n, 25073877L);
			assertEquals(String.valueOf((char[]) tweet.getValue("user_name")), "Donald J. Trump");
		}
		searcher.close();
	}
	
	@Test
	// Retrieve the most recent 50 tweets by Benoît Hamon
	public void testTwitterSearcherUsername() throws TatooineExecutionException {
		// Twitter user screen name of Benoît Hamon
		String username = "benoithamon";
		PhyTwitterSearcher searcher = new PhyTwitterSearcher(null, username, null, TwitterSearcherEnum.USER_NAME, 50);	
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tweet = searcher.next();
			assertEquals(((OrderedLongElementID) tweet.getValue("user_id")).n, 14389177L);
			assertEquals(String.valueOf((char[]) tweet.getValue("user_name")), "Benoît Hamon");
		}
		searcher.close();
	}
	
	@Test
	@SuppressWarnings("unchecked")
	// Retrieve the most recent 30 tweets with the hashtag #brexit
	public void testTwitterSearcherHashtag() throws TatooineExecutionException {
		// Hashtag KW
		String hashtag = "brexit";
		PhyTwitterSearcher searcher = new PhyTwitterSearcher(null, null, hashtag, TwitterSearcherEnum.HASHTAG, 30);							
		searcher.open();
		assertTrue(searcher.hasNext());
		while (searcher.hasNext()) {
			NTuple tweet = searcher.next();	
			
			boolean hashtagFoundInResults = false;
			List<NTuple> children;
			children = ((List<NTuple>) tweet.getValue("hashtags"));			
			for (NTuple child : children) {
				String tag = String.valueOf((char[]) child.getValue("hashtag")).toLowerCase();
				if (hashtag.equals(tag)) {
					hashtagFoundInResults = true;
					break;
				}
			}
			assertTrue(hashtagFoundInResults);
		}
		searcher.close();
	}
}
