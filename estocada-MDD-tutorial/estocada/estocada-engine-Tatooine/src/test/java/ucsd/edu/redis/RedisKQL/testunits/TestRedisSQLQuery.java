/**
 * 
 */
package ucsd.edu.redis.RedisKQL.testunits;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;
import ucsd.edu.redis.RedisKQL.query.RedisKQL;
import ucsd.edu.redis.RedisKQL.query.RedisKQLQueryParser;
import ucsd.edu.redis.RedisKQL.runtime.RedisConnection;
import ucsd.edu.redis.RedisKQL.runtime.RedisKQLEvaluator;

/**
 * Tests RedisKQL Queries.
 * 
 * @author Rana Alotaibi
 * 
 */
@Ignore
public class TestRedisSQLQuery {

	/**
	 * Checks whether the query is executed correctly
	 * 
	 * @param queryString
	 *            the query string.
	 * @param expectedValue
	 *            the expected value that the query should return as a result
	 */
	protected static void checkResult(SetType expectedValue, String queryString) {
		RedisKQLQueryParser queryParser = new RedisKQLQueryParser();
		RedisKQL xRedisQuery = queryParser.parse(queryString);
		RedisKQLEvaluator queryExecuter = new RedisKQLEvaluator();
		SetType resultValue = (SetType) queryExecuter.evaluateRedisKQLQuery(xRedisQuery);
		assertTrue(expectedValue.equals(resultValue));
	}
	
	/**
	 * Test #1 testReturnSetofSingleMap
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testReturnSetofSingleMap() {
		RedisConnection.jedis.flushAll();
		Map<String, String> nora = new HashMap<String, String>();
		nora.put("country", "UAE");
		nora.put("city", "Dubai");
		nora.put("age", "23");
		RedisConnection.jedis.hmset("NoraInfo", nora);
		String queryString = "SELECT userInfo FROM userInfo IN MAP['NoraInfo']";
		Map<String, ?> mapVal = nora;
		MapType mapResult = new MapType();
		mapResult.setAll((Map<String, Object>) mapVal);
		SetType expectedValue = new SetType();
		expectedValue.addToSet(mapResult);
		checkResult(expectedValue, queryString);
	}

	/**
	 * Test#2 testReturnSetofSingleString
	 */
	@Test
	public void testReturnSetofSingleString() {
		RedisConnection.jedis.flushAll();
		RedisConnection.jedis.set("name", "rana");
		String queryString = "SELECT Name FROM Name IN MAP['name']";
		String stirngValue = "rana";
		SetType expectedValue = new SetType();
		expectedValue.addToSet(stirngValue);
		checkResult(expectedValue, queryString);

	}

	/**
	 * Test#3 testReturnSetofSingleSet
	 */
	@Test
	public void testReturnSetofSingleSet() {
		RedisConnection.jedis.flushAll();
		RedisConnection.jedis.sadd("followers", "@dimitry");
		RedisConnection.jedis.sadd("followers", "@nora");
		RedisConnection.jedis.sadd("followers", "@rana");
		SetType setOfValues = new SetType();
		setOfValues.addToSet("@rana");
		setOfValues.addToSet("@nora");
		setOfValues.addToSet("@dimitry");
		SetType expectedValue = new SetType();
		expectedValue.addToSet(setOfValues);
		String queryString = "SELECT followerNames FROM followerNames IN MAP['followers']";
		checkResult(expectedValue, queryString);

	}

	/**
	 * Test#4 testReturnSetofSingleString
	 */
	@Test
	public void testReturnSetOfSets() {
		RedisConnection.jedis.flushAll();
		RedisConnection.jedis.sadd("Letters", "A");
		RedisConnection.jedis.sadd("Letters", "B");
		RedisConnection.jedis.sadd("Letters", "C");
		RedisConnection.jedis.sadd("A", "1");
		RedisConnection.jedis.sadd("A", "2");
		RedisConnection.jedis.sadd("A", "3");
		RedisConnection.jedis.sadd("B", "4");
		RedisConnection.jedis.sadd("B", "5");
		RedisConnection.jedis.sadd("B", "6");
		RedisConnection.jedis.sadd("C", "7");
		RedisConnection.jedis.sadd("C", "8");
		RedisConnection.jedis.sadd("C", "9");
		SetType setOfValuesA = new SetType();
		setOfValuesA.addToSet("1");
		setOfValuesA.addToSet("2");
		setOfValuesA.addToSet("3");
		SetType setOfValuesB = new SetType();
		setOfValuesB.addToSet("4");
		setOfValuesB.addToSet("5");
		setOfValuesB.addToSet("6");
		SetType setOfValuesC = new SetType();
		setOfValuesC.addToSet("7");
		setOfValuesC.addToSet("8");
		setOfValuesC.addToSet("9");
		SetType expectedValue = new SetType();
		expectedValue.addToSet(setOfValuesB);
		expectedValue.addToSet(setOfValuesA);
		expectedValue.addToSet(setOfValuesC);

		String queryString = "SELECT Digits FROM Letter IN MAP['Letters'], Digits IN MAP[Letter] ";
		checkResult(expectedValue, queryString);

	}

	/**
	 * Test#5 testReturnSetofMaps
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testReturnSetofMaps() {
		RedisConnection.jedis.flushAll();
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Nora");
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Danney");
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Dmitry");
		Map<String, String> nora = new HashMap<String, String>();
		nora.put("country", "UAE");
		nora.put("city", "Dubai");
		nora.put("age", "23");
		Map<String, ?> mapNoraVal = nora;
		MapType mapNora = new MapType();
		mapNora.setAll((Map<String, Object>) mapNoraVal);

		Map<String, String> Danney = new HashMap<String, String>();
		Danney.put("country", "USA");
		Danney.put("city", "SD");
		Danney.put("age", "25");
		Map<String, ?> mapDanneyVal = Danney;
		MapType mapDanney = new MapType();
		mapDanney.setAll((Map<String, Object>) mapDanneyVal);

		Map<String, String> Dmitry = new HashMap<String, String>();
		Dmitry.put("country", "Russia");
		Dmitry.put("city", "Moscow");
		Dmitry.put("age", "22");
		RedisConnection.jedis.hmset("@Nora", nora);
		RedisConnection.jedis.hmset("@Danney", Danney);
		RedisConnection.jedis.hmset("@Dmitry", Dmitry);
		Map<String, ?> mapDmitryVal = Dmitry;
		MapType mapDmitry = new MapType();
		mapDmitry.setAll((Map<String, Object>) mapDmitryVal);

		SetType expectedValue = new SetType();
		expectedValue.addToSet(mapDanney);
		expectedValue.addToSet(mapNora);
		expectedValue.addToSet(mapDmitry);
		String queryString = "SELECT followerInfo FROM follower IN MAP['DavidFollowersSet'], followerInfo IN MAP[follower]";
		checkResult(expectedValue, queryString);

	}

	/**
	 * Test#6 testRetrunSinglesSetAfterThreeLookUps
	 */
	@Test
	public void testRetrunSinglesSetAfterThreeLookUps() {
		RedisConnection.jedis.flushAll();
		Map<String, String> NoraInfo = new HashMap<String, String>();
		NoraInfo.put("country", "UAE");
		NoraInfo.put("city", "Dubai");
		NoraInfo.put("age", "23");
		NoraInfo.put("NorahFollowers", "FollowersSet");
		RedisConnection.jedis.hmset("NoraInfo", NoraInfo);
		RedisConnection.jedis.sadd("FollowersSet", "@rana");
		RedisConnection.jedis.sadd("FollowersSet", "@daniel");
		RedisConnection.jedis.sadd("FollowersSet", "@dimitry");
		SetType setOfValues = new SetType();
		setOfValues.addToSet("@rana");
		setOfValues.addToSet("@daniel");
		setOfValues.addToSet("@dimitry");
		SetType expectedValue = new SetType();
		expectedValue.addToSet(setOfValues);
		String queryString = "SELECT NoraFollowerSet FROM userInfo IN MAP['NoraInfo'], NoraFollowerSetKey IN userInfo['NorahFollowers'], NoraFollowerSet IN MAP[NoraFollowerSetKey]";
		checkResult(expectedValue, queryString);

	}

	/**
	 * Test#7 testManyLookUpsToReturnSetofStrings
	 */
	@Test
	public void testManyLookUpsToReturnSetofStrings() {
		RedisConnection.jedis.flushAll();
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Nora");
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Danney");
		RedisConnection.jedis.sadd("DavidFollowersSet", "@Dmitry");
		Map<String, String> nora = new HashMap<String, String>();
		nora.put("country", "UAE");
		nora.put("city", "Dubai");
		nora.put("age", "23");
		Map<String, String> Danney = new HashMap<String, String>();
		Danney.put("country", "USA");
		Danney.put("city", "SD");
		Danney.put("age", "25");
		Map<String, String> Dmitry = new HashMap<String, String>();
		Dmitry.put("country", "Russia");
		Dmitry.put("city", "Moscow");
		Dmitry.put("age", "22");
		RedisConnection.jedis.hmset("@Nora", nora);
		RedisConnection.jedis.hmset("@Danney", Danney);
		RedisConnection.jedis.hmset("@Dmitry", Dmitry);
		SetType expectedValue = new SetType();
		expectedValue.addToSet("Dubai");
		expectedValue.addToSet("SD");
		expectedValue.addToSet("Moscow");
		String queryString = "SELECT cities FROM follower IN MAP['DavidFollowersSet'], followerInfo IN MAP[follower], cities IN followerInfo['city'] ";
		checkResult(expectedValue, queryString);
	}

	/**
	 * Test #8 : testRestrunSetofStringsofMapValues
	 */
	@Test
	public void testSetofStringsofMapValues() {
		RedisConnection.jedis.flushAll();
		Map<String, String> nora = new HashMap<String, String>();
		nora.put("country", "UAE");
		nora.put("city", "Dubai");
		nora.put("age", "23");
		RedisConnection.jedis.hmset("NoraInfo", nora);
		String queryString = "SELECT Info FROM userInfo IN MAP['NoraInfo'], Key IN KEYS[userInfo], Info IN userInfo[Key]";
		SetType expectedValue = new SetType();
		expectedValue.addToSet("23");
		expectedValue.addToSet("Dubai");
		expectedValue.addToSet("UAE");
		checkResult(expectedValue, queryString);
	}

	/**
	 * Test #9 : testRestrunSetofStringsofMapValues
	 */
	@Test
	public void testTwoStepsMap() {
		RedisConnection.jedis.flushAll();
		Map<String, String> nora = new HashMap<String, String>();
		nora.put("country", "UAE");
		nora.put("city", "Dubai");
		nora.put("age", "23");
		RedisConnection.jedis.hmset("NoraInfo", nora);
		String queryString = "SELECT city FROM city IN MAP['NoraInfo']['city'] ";
		SetType expectedValue = new SetType();
		expectedValue.addToSet("Dubai");
		checkResult(expectedValue, queryString);
	}

}
