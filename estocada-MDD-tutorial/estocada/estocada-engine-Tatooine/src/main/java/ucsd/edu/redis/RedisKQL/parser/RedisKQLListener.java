// Generated from ucsd/edu/redis/RedisKQL/parser/RedisKQL.g4 by ANTLR 4.3
package ucsd.edu.redis.RedisKQL.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RedisKQLParser}.
 */
public interface RedisKQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#offest}.
	 * @param ctx the parse tree
	 */
	void enterOffest(@NotNull RedisKQLParser.OffestContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#offest}.
	 * @param ctx the parse tree
	 */
	void exitOffest(@NotNull RedisKQLParser.OffestContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#select}.
	 * @param ctx the parse tree
	 */
	void enterSelect(@NotNull RedisKQLParser.SelectContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#select}.
	 * @param ctx the parse tree
	 */
	void exitSelect(@NotNull RedisKQLParser.SelectContext ctx);

	/**
	 * Enter a parse tree produced by the {@code VarMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void enterVarMapLookUp(@NotNull RedisKQLParser.VarMapLookUpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VarMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void exitVarMapLookUp(@NotNull RedisKQLParser.VarMapLookUpContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#selectItem}.
	 * @param ctx the parse tree
	 */
	void enterSelectItem(@NotNull RedisKQLParser.SelectItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#selectItem}.
	 * @param ctx the parse tree
	 */
	void exitSelectItem(@NotNull RedisKQLParser.SelectItemContext ctx);

	/**
	 * Enter a parse tree produced by the {@code MainMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void enterMainMapLookUp(@NotNull RedisKQLParser.MainMapLookUpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MainMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void exitMainMapLookUp(@NotNull RedisKQLParser.MainMapLookUpContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void enterVarBinding(@NotNull RedisKQLParser.VarBindingContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void exitVarBinding(@NotNull RedisKQLParser.VarBindingContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#setConsturctor}.
	 * @param ctx the parse tree
	 */
	void enterSetConsturctor(@NotNull RedisKQLParser.SetConsturctorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#setConsturctor}.
	 * @param ctx the parse tree
	 */
	void exitSetConsturctor(@NotNull RedisKQLParser.SetConsturctorContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#redisKQLQuery}.
	 * @param ctx the parse tree
	 */
	void enterRedisKQLQuery(@NotNull RedisKQLParser.RedisKQLQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#redisKQLQuery}.
	 * @param ctx the parse tree
	 */
	void exitRedisKQLQuery(@NotNull RedisKQLParser.RedisKQLQueryContext ctx);

	/**
	 * Enter a parse tree produced by the {@code KeysMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void enterKeysMapLookUp(@NotNull RedisKQLParser.KeysMapLookUpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code KeysMapLookUp}
	 * labeled alternative in {@link RedisKQLParser#lookUpExpression}.
	 * @param ctx the parse tree
	 */
	void exitKeysMapLookUp(@NotNull RedisKQLParser.KeysMapLookUpContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#arrayConstructor}.
	 * @param ctx the parse tree
	 */
	void enterArrayConstructor(@NotNull RedisKQLParser.ArrayConstructorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#arrayConstructor}.
	 * @param ctx the parse tree
	 */
	void exitArrayConstructor(@NotNull RedisKQLParser.ArrayConstructorContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#mapConstructor}.
	 * @param ctx the parse tree
	 */
	void enterMapConstructor(@NotNull RedisKQLParser.MapConstructorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#mapConstructor}.
	 * @param ctx the parse tree
	 */
	void exitMapConstructor(@NotNull RedisKQLParser.MapConstructorContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(@NotNull RedisKQLParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(@NotNull RedisKQLParser.VariableContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#limit}.
	 * @param ctx the parse tree
	 */
	void enterLimit(@NotNull RedisKQLParser.LimitContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#limit}.
	 * @param ctx the parse tree
	 */
	void exitLimit(@NotNull RedisKQLParser.LimitContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#stringKey}.
	 * @param ctx the parse tree
	 */
	void enterStringKey(@NotNull RedisKQLParser.StringKeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#stringKey}.
	 * @param ctx the parse tree
	 */
	void exitStringKey(@NotNull RedisKQLParser.StringKeyContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#from}.
	 * @param ctx the parse tree
	 */
	void enterFrom(@NotNull RedisKQLParser.FromContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#from}.
	 * @param ctx the parse tree
	 */
	void exitFrom(@NotNull RedisKQLParser.FromContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#mapName}.
	 * @param ctx the parse tree
	 */
	void enterMapName(@NotNull RedisKQLParser.MapNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#mapName}.
	 * @param ctx the parse tree
	 */
	void exitMapName(@NotNull RedisKQLParser.MapNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void enterSelectStatement(@NotNull RedisKQLParser.SelectStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#selectStatement}.
	 * @param ctx the parse tree
	 */
	void exitSelectStatement(@NotNull RedisKQLParser.SelectStatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#selectFromWhereQuery}.
	 * @param ctx the parse tree
	 */
	void enterSelectFromWhereQuery(@NotNull RedisKQLParser.SelectFromWhereQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#selectFromWhereQuery}.
	 * @param ctx the parse tree
	 */
	void exitSelectFromWhereQuery(@NotNull RedisKQLParser.SelectFromWhereQueryContext ctx);

	/**
	 * Enter a parse tree produced by {@link RedisKQLParser#key}.
	 * @param ctx the parse tree
	 */
	void enterKey(@NotNull RedisKQLParser.KeyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RedisKQLParser#key}.
	 * @param ctx the parse tree
	 */
	void exitKey(@NotNull RedisKQLParser.KeyContext ctx);
}