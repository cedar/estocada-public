
package ucsd.edu.redis.RedisKQL.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.google.gson.Gson;

import ucsd.edu.redis.RedisKQL.datatype.ListType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.StringType;

/************
 * 
 * This Class is Not Used Now
 * 
 ************/
/**
 * A Map Constructor
 * 
 * @author ranaalotaibi
 *
 */
public class MapConstructor extends AbstractSelectItem {
	private String	keyName;
	private String	variableName;

	/**
	 * Create a Map Constructor from Key and Value.
	 * 
	 * @param keyName
	 *            The key name.
	 * @param variableName
	 *            the variable name
	 */
	public MapConstructor(String keyName, String variableName) {
		this.variableName = variableName;
		this.keyName = keyName;
	}

	/**
	 * Return the constructed map
	 * 
	 * @param map
	 *            The map that holds variable binding from the FROM clause
	 * @return the constructed map
	 */
	@Override
	public MapType evaluateSelectItem(MapType mapVal) {

		ListType keyList = new ListType();
		ListType valueList = new ListType();
		MapType constructtedMap = new MapType();
		keyList = (ListType) mapVal.getFromMap(keyName);
		valueList = (ListType) mapVal.getFromMap(variableName);
		Iterator<Object> keyIterator = keyList.returnArray().iterator();
		Iterator<Object> valueIterator = valueList.returnArray().iterator();

		while (keyIterator.hasNext() && valueIterator.hasNext()) {
			constructtedMap.putToMap(keyIterator.next().toString(),
			        new StringType(valueIterator.next().toString().replaceAll("\"", "")));
		}
		return constructtedMap;
	}

	/**
	 * Return JSONObject String Representation of a Map
	 * 
	 * @param mapVal
	 *            the mapVal to be converted to JSONObject
	 */
	public String JSONObject(MapType mapVal) {

		Map<String, String> mapObject = new HashMap<String, String>();
		return new Gson().toJson(mapObject);
	}

	/*
	 * ========================================================================
	 * String Representation of Map Constructor
	 * ========================================================================
	 */
	@Override
	public String toString() {
		return "map ( " + keyName + " : " + variableName + " )";
	}

}
