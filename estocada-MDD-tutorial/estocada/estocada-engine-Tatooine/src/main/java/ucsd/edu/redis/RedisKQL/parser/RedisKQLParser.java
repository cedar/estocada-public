// Generated from ucsd/edu/redis/RedisKQL/parser/RedisKQL.g4 by ANTLR 4.3
package ucsd.edu.redis.RedisKQL.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RedisKQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__5=1, T__4=2, T__3=3, T__2=4, T__1=5, T__0=6, FROM=7, SELECT=8, WHERE=9, 
		OFFEST=10, LIMIT=11, IN=12, MAP=13, KEYS=14, LIST=15, SET=16, NAME=17, 
		STRING=18, INTEGER=19, WHITESPACE=20;
	public static final String[] tokenNames = {
		"<INVALID>", "'('", "')'", "':'", "'['", "','", "']'", "FROM", "SELECT", 
		"WHERE", "OFFEST", "LIMIT", "IN", "MAP", "KEYS", "LIST", "SET", "NAME", 
		"STRING", "INTEGER", "WHITESPACE"
	};
	public static final int
		RULE_redisKQLQuery = 0, RULE_selectFromWhereQuery = 1, RULE_select = 2, 
		RULE_selectStatement = 3, RULE_selectItem = 4, RULE_mapConstructor = 5, 
		RULE_arrayConstructor = 6, RULE_setConsturctor = 7, RULE_from = 8, RULE_varBinding = 9, 
		RULE_lookUpExpression = 10, RULE_key = 11, RULE_stringKey = 12, RULE_variable = 13, 
		RULE_mapName = 14, RULE_limit = 15, RULE_offest = 16;
	public static final String[] ruleNames = {
		"redisKQLQuery", "selectFromWhereQuery", "select", "selectStatement", 
		"selectItem", "mapConstructor", "arrayConstructor", "setConsturctor", 
		"from", "varBinding", "lookUpExpression", "key", "stringKey", "variable", 
		"mapName", "limit", "offest"
	};

	@Override
	public String getGrammarFileName() { return "RedisKQL.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RedisKQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RedisKQLQueryContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(RedisKQLParser.EOF, 0); }
		public SelectFromWhereQueryContext selectFromWhereQuery() {
			return getRuleContext(SelectFromWhereQueryContext.class,0);
		}
		public RedisKQLQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_redisKQLQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterRedisKQLQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitRedisKQLQuery(this);
		}
	}

	public final RedisKQLQueryContext redisKQLQuery() throws RecognitionException {
		RedisKQLQueryContext _localctx = new RedisKQLQueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_redisKQLQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34); selectFromWhereQuery();
			setState(35); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectFromWhereQueryContext extends ParserRuleContext {
		public OffestContext offest() {
			return getRuleContext(OffestContext.class,0);
		}
		public FromContext from() {
			return getRuleContext(FromContext.class,0);
		}
		public SelectContext select() {
			return getRuleContext(SelectContext.class,0);
		}
		public LimitContext limit() {
			return getRuleContext(LimitContext.class,0);
		}
		public SelectFromWhereQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectFromWhereQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterSelectFromWhereQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitSelectFromWhereQuery(this);
		}
	}

	public final SelectFromWhereQueryContext selectFromWhereQuery() throws RecognitionException {
		SelectFromWhereQueryContext _localctx = new SelectFromWhereQueryContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_selectFromWhereQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37); select();
			setState(38); from();
			setState(40);
			_la = _input.LA(1);
			if (_la==LIMIT) {
				{
				setState(39); limit();
				}
			}

			setState(43);
			_la = _input.LA(1);
			if (_la==OFFEST) {
				{
				setState(42); offest();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectContext extends ParserRuleContext {
		public SelectStatementContext selectStatement() {
			return getRuleContext(SelectStatementContext.class,0);
		}
		public SelectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterSelect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitSelect(this);
		}
	}

	public final SelectContext select() throws RecognitionException {
		SelectContext _localctx = new SelectContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_select);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45); selectStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectStatementContext extends ParserRuleContext {
		public List<SelectItemContext> selectItem() {
			return getRuleContexts(SelectItemContext.class);
		}
		public SelectItemContext selectItem(int i) {
			return getRuleContext(SelectItemContext.class,i);
		}
		public TerminalNode SELECT() { return getToken(RedisKQLParser.SELECT, 0); }
		public SelectStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterSelectStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitSelectStatement(this);
		}
	}

	public final SelectStatementContext selectStatement() throws RecognitionException {
		SelectStatementContext _localctx = new SelectStatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_selectStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(47); match(SELECT);
			setState(48); selectItem();
			setState(53);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(49); match(T__1);
				setState(50); selectItem();
				}
				}
				setState(55);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectItemContext extends ParserRuleContext {
		public MapConstructorContext mapConstructor() {
			return getRuleContext(MapConstructorContext.class,0);
		}
		public ArrayConstructorContext arrayConstructor() {
			return getRuleContext(ArrayConstructorContext.class,0);
		}
		public SetConsturctorContext setConsturctor() {
			return getRuleContext(SetConsturctorContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public SelectItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterSelectItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitSelectItem(this);
		}
	}

	public final SelectItemContext selectItem() throws RecognitionException {
		SelectItemContext _localctx = new SelectItemContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_selectItem);
		try {
			setState(60);
			switch (_input.LA(1)) {
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(56); variable();
				}
				break;
			case MAP:
				enterOuterAlt(_localctx, 2);
				{
				setState(57); mapConstructor();
				}
				break;
			case LIST:
				enterOuterAlt(_localctx, 3);
				{
				setState(58); arrayConstructor();
				}
				break;
			case SET:
				enterOuterAlt(_localctx, 4);
				{
				setState(59); setConsturctor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapConstructorContext extends ParserRuleContext {
		public TerminalNode MAP() { return getToken(RedisKQLParser.MAP, 0); }
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public MapConstructorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mapConstructor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterMapConstructor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitMapConstructor(this);
		}
	}

	public final MapConstructorContext mapConstructor() throws RecognitionException {
		MapConstructorContext _localctx = new MapConstructorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_mapConstructor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62); match(MAP);
			setState(63); match(T__5);
			setState(64); variable();
			setState(65); match(T__3);
			setState(66); variable();
			setState(67); match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayConstructorContext extends ParserRuleContext {
		public TerminalNode LIST() { return getToken(RedisKQLParser.LIST, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public ArrayConstructorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayConstructor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterArrayConstructor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitArrayConstructor(this);
		}
	}

	public final ArrayConstructorContext arrayConstructor() throws RecognitionException {
		ArrayConstructorContext _localctx = new ArrayConstructorContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_arrayConstructor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69); match(LIST);
			setState(70); match(T__5);
			setState(71); variable();
			setState(72); match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetConsturctorContext extends ParserRuleContext {
		public TerminalNode SET() { return getToken(RedisKQLParser.SET, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public SetConsturctorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_setConsturctor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterSetConsturctor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitSetConsturctor(this);
		}
	}

	public final SetConsturctorContext setConsturctor() throws RecognitionException {
		SetConsturctorContext _localctx = new SetConsturctorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_setConsturctor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74); match(SET);
			setState(75); match(T__5);
			setState(76); variable();
			setState(77); match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FromContext extends ParserRuleContext {
		public VarBindingContext varBinding(int i) {
			return getRuleContext(VarBindingContext.class,i);
		}
		public List<VarBindingContext> varBinding() {
			return getRuleContexts(VarBindingContext.class);
		}
		public TerminalNode FROM() { return getToken(RedisKQLParser.FROM, 0); }
		public FromContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_from; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterFrom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitFrom(this);
		}
	}

	public final FromContext from() throws RecognitionException {
		FromContext _localctx = new FromContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_from);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79); match(FROM);
			setState(80); varBinding();
			setState(85);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(81); match(T__1);
				setState(82); varBinding();
				}
				}
				setState(87);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarBindingContext extends ParserRuleContext {
		public LookUpExpressionContext lookUpExpression() {
			return getRuleContext(LookUpExpressionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public TerminalNode IN() { return getToken(RedisKQLParser.IN, 0); }
		public VarBindingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varBinding; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterVarBinding(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitVarBinding(this);
		}
	}

	public final VarBindingContext varBinding() throws RecognitionException {
		VarBindingContext _localctx = new VarBindingContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_varBinding);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88); variable();
			setState(89); match(IN);
			setState(90); lookUpExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LookUpExpressionContext extends ParserRuleContext {
		public LookUpExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lookUpExpression; }
	 
		public LookUpExpressionContext() { }
		public void copyFrom(LookUpExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class KeysMapLookUpContext extends LookUpExpressionContext {
		public TerminalNode KEYS() { return getToken(RedisKQLParser.KEYS, 0); }
		public MapNameContext mapName() {
			return getRuleContext(MapNameContext.class,0);
		}
		public KeysMapLookUpContext(LookUpExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterKeysMapLookUp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitKeysMapLookUp(this);
		}
	}
	public static class VarMapLookUpContext extends LookUpExpressionContext {
		public TerminalNode NAME() { return getToken(RedisKQLParser.NAME, 0); }
		public List<KeyContext> key() {
			return getRuleContexts(KeyContext.class);
		}
		public KeyContext key(int i) {
			return getRuleContext(KeyContext.class,i);
		}
		public VarMapLookUpContext(LookUpExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterVarMapLookUp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitVarMapLookUp(this);
		}
	}
	public static class MainMapLookUpContext extends LookUpExpressionContext {
		public TerminalNode MAP() { return getToken(RedisKQLParser.MAP, 0); }
		public List<KeyContext> key() {
			return getRuleContexts(KeyContext.class);
		}
		public KeyContext key(int i) {
			return getRuleContext(KeyContext.class,i);
		}
		public MainMapLookUpContext(LookUpExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterMainMapLookUp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitMainMapLookUp(this);
		}
	}

	public final LookUpExpressionContext lookUpExpression() throws RecognitionException {
		LookUpExpressionContext _localctx = new LookUpExpressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_lookUpExpression);
		int _la;
		try {
			setState(123);
			switch (_input.LA(1)) {
			case NAME:
				_localctx = new VarMapLookUpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(92); match(NAME);
				setState(93); match(T__2);
				setState(94); key();
				setState(95); match(T__0);
				setState(102);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(96); match(T__2);
					setState(97); key();
					setState(98); match(T__0);
					}
					}
					setState(104);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case MAP:
				_localctx = new MainMapLookUpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(105); match(MAP);
				setState(106); match(T__2);
				setState(107); key();
				setState(108); match(T__0);
				setState(115);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(109); match(T__2);
					setState(110); key();
					setState(111); match(T__0);
					}
					}
					setState(117);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case KEYS:
				_localctx = new KeysMapLookUpContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(118); match(KEYS);
				setState(119); match(T__2);
				setState(120); mapName();
				setState(121); match(T__0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyContext extends ParserRuleContext {
		public StringKeyContext stringKey() {
			return getRuleContext(StringKeyContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public KeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitKey(this);
		}
	}

	public final KeyContext key() throws RecognitionException {
		KeyContext _localctx = new KeyContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_key);
		try {
			setState(127);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(125); stringKey();
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(126); variable();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringKeyContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(RedisKQLParser.STRING, 0); }
		public StringKeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stringKey; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterStringKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitStringKey(this);
		}
	}

	public final StringKeyContext stringKey() throws RecognitionException {
		StringKeyContext _localctx = new StringKeyContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_stringKey);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(129); match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(RedisKQLParser.NAME, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131); match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MapNameContext extends ParserRuleContext {
		public TerminalNode MAP() { return getToken(RedisKQLParser.MAP, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public MapNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mapName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterMapName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitMapName(this);
		}
	}

	public final MapNameContext mapName() throws RecognitionException {
		MapNameContext _localctx = new MapNameContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_mapName);
		try {
			setState(135);
			switch (_input.LA(1)) {
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(133); variable();
				}
				break;
			case MAP:
				enterOuterAlt(_localctx, 2);
				{
				setState(134); match(MAP);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(RedisKQLParser.INTEGER, 0); }
		public TerminalNode LIMIT() { return getToken(RedisKQLParser.LIMIT, 0); }
		public LimitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterLimit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitLimit(this);
		}
	}

	public final LimitContext limit() throws RecognitionException {
		LimitContext _localctx = new LimitContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_limit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137); match(LIMIT);
			setState(138); match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OffestContext extends ParserRuleContext {
		public TerminalNode OFFEST() { return getToken(RedisKQLParser.OFFEST, 0); }
		public TerminalNode INTEGER() { return getToken(RedisKQLParser.INTEGER, 0); }
		public OffestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_offest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).enterOffest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RedisKQLListener ) ((RedisKQLListener)listener).exitOffest(this);
		}
	}

	public final OffestContext offest() throws RecognitionException {
		OffestContext _localctx = new OffestContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_offest);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140); match(OFFEST);
			setState(141); match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\26\u0092\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\5\3+\n\3\3\3\5\3.\n\3\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\7\5\66\n\5\f\5\16\59\13\5\3\6\3\6\3\6\3\6\5\6?\n\6\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\7\n"+
		"V\n\n\f\n\16\nY\13\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\7\fg\n\f\f\f\16\fj\13\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\ft\n\f\f"+
		"\f\16\fw\13\f\3\f\3\f\3\f\3\f\3\f\5\f~\n\f\3\r\3\r\5\r\u0082\n\r\3\16"+
		"\3\16\3\17\3\17\3\20\3\20\5\20\u008a\n\20\3\21\3\21\3\21\3\22\3\22\3\22"+
		"\3\22\2\2\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\2\u008d\2$\3"+
		"\2\2\2\4\'\3\2\2\2\6/\3\2\2\2\b\61\3\2\2\2\n>\3\2\2\2\f@\3\2\2\2\16G\3"+
		"\2\2\2\20L\3\2\2\2\22Q\3\2\2\2\24Z\3\2\2\2\26}\3\2\2\2\30\u0081\3\2\2"+
		"\2\32\u0083\3\2\2\2\34\u0085\3\2\2\2\36\u0089\3\2\2\2 \u008b\3\2\2\2\""+
		"\u008e\3\2\2\2$%\5\4\3\2%&\7\2\2\3&\3\3\2\2\2\'(\5\6\4\2(*\5\22\n\2)+"+
		"\5 \21\2*)\3\2\2\2*+\3\2\2\2+-\3\2\2\2,.\5\"\22\2-,\3\2\2\2-.\3\2\2\2"+
		".\5\3\2\2\2/\60\5\b\5\2\60\7\3\2\2\2\61\62\7\n\2\2\62\67\5\n\6\2\63\64"+
		"\7\7\2\2\64\66\5\n\6\2\65\63\3\2\2\2\669\3\2\2\2\67\65\3\2\2\2\678\3\2"+
		"\2\28\t\3\2\2\29\67\3\2\2\2:?\5\34\17\2;?\5\f\7\2<?\5\16\b\2=?\5\20\t"+
		"\2>:\3\2\2\2>;\3\2\2\2><\3\2\2\2>=\3\2\2\2?\13\3\2\2\2@A\7\17\2\2AB\7"+
		"\3\2\2BC\5\34\17\2CD\7\5\2\2DE\5\34\17\2EF\7\4\2\2F\r\3\2\2\2GH\7\21\2"+
		"\2HI\7\3\2\2IJ\5\34\17\2JK\7\4\2\2K\17\3\2\2\2LM\7\22\2\2MN\7\3\2\2NO"+
		"\5\34\17\2OP\7\4\2\2P\21\3\2\2\2QR\7\t\2\2RW\5\24\13\2ST\7\7\2\2TV\5\24"+
		"\13\2US\3\2\2\2VY\3\2\2\2WU\3\2\2\2WX\3\2\2\2X\23\3\2\2\2YW\3\2\2\2Z["+
		"\5\34\17\2[\\\7\16\2\2\\]\5\26\f\2]\25\3\2\2\2^_\7\23\2\2_`\7\6\2\2`a"+
		"\5\30\r\2ah\7\b\2\2bc\7\6\2\2cd\5\30\r\2de\7\b\2\2eg\3\2\2\2fb\3\2\2\2"+
		"gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2i~\3\2\2\2jh\3\2\2\2kl\7\17\2\2lm\7\6\2"+
		"\2mn\5\30\r\2nu\7\b\2\2op\7\6\2\2pq\5\30\r\2qr\7\b\2\2rt\3\2\2\2so\3\2"+
		"\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2v~\3\2\2\2wu\3\2\2\2xy\7\20\2\2yz\7"+
		"\6\2\2z{\5\36\20\2{|\7\b\2\2|~\3\2\2\2}^\3\2\2\2}k\3\2\2\2}x\3\2\2\2~"+
		"\27\3\2\2\2\177\u0082\5\32\16\2\u0080\u0082\5\34\17\2\u0081\177\3\2\2"+
		"\2\u0081\u0080\3\2\2\2\u0082\31\3\2\2\2\u0083\u0084\7\24\2\2\u0084\33"+
		"\3\2\2\2\u0085\u0086\7\23\2\2\u0086\35\3\2\2\2\u0087\u008a\5\34\17\2\u0088"+
		"\u008a\7\17\2\2\u0089\u0087\3\2\2\2\u0089\u0088\3\2\2\2\u008a\37\3\2\2"+
		"\2\u008b\u008c\7\r\2\2\u008c\u008d\7\25\2\2\u008d!\3\2\2\2\u008e\u008f"+
		"\7\f\2\2\u008f\u0090\7\25\2\2\u0090#\3\2\2\2\f*-\67>Whu}\u0081\u0089";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}