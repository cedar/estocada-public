/**
 * 
 */
package ucsd.edu.redis.RedisKQL.query;

/**
 * 
 * Abstract class for implementing different SELECT items in the SELECT clause
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractSelectItem implements SelectItem {

}
