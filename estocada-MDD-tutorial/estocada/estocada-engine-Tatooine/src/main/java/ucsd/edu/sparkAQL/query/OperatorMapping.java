
package ucsd.edu.sparkAQL.query;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

public final class OperatorMapping {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(OperatorMapping.class);

    private static final OperatorMapping INSTANCE = new OperatorMapping();

    private Map<String, Operator> operatorMap = new LinkedHashMap<String, Operator>();

    private OperatorMapping() {
        add(new RecordPathExpression());
        add(new ArrayPathExpression());
        add(new EqualOperator());
    }

    public Operator find(String operatorName) {
        Operator operator = operatorMap.get(operatorName);
        return operator;
    }

    private void add(Operator operator) {

        String name = operator.getOperator().getOperatorName().toString();
        operatorMap.put(name, operator);
    }

    public static OperatorMapping get() {

        return new OperatorMapping();
    }

}
