/**
 * 
 */
package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class ParenthesizedExpression extends AbstractPrimaryExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(ParenthesizedExpression.class);

    private ForWhereReturnClause subForWhereReturnClause;
    public static String pathExpressionTag = "";

    public ParenthesizedExpression(ForWhereReturnClause subForWhereReturnClause) {

        this.subForWhereReturnClause = subForWhereReturnClause;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(\n");
        sb.append(subForWhereReturnClause);
        sb.append("\n)");
        return sb.toString();
    }

    @Override
    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        String tempNavPath = null;
        tempNavPath = RecordPathExpression.preNaviagteVar;
        Dataset<Row> result = subForWhereReturnClause.evaluateExpression(varBindingVal, jsonPath);
        pathExpressionTag = RecordPathExpression.preNaviagteVar;
        RecordPathExpression.preNaviagteVar = tempNavPath;
        return result;

    }

    @Override
    public void setAQLParams(char[] param) {
        // TODO Auto-generated method stub

    }

    @Override
    public char[] getAQLParams() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reInitializeAQLParams() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasParam() {
        // TODO Auto-generated method stub
        return false;
    }

}
