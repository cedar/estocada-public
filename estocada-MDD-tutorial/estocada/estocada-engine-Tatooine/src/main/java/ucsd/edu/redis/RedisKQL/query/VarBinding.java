package ucsd.edu.redis.RedisKQL.query;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ucsd.edu.redis.RedisKQL.datatype.DataType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;

/**
 * A Variable Binding
 * 
 * @author ranaalotaibi
 *
 */
public class VarBinding {

	private Map<Variable, LookUpExpression> varBinding = new LinkedHashMap<Variable, LookUpExpression>();

	/**
	 * Create a VarBinding
	 * 
	 * @param vVarBinding
	 *            the variable Binding
	 */
	public VarBinding(Map<Variable, LookUpExpression> varBinding) {
		this.varBinding = varBinding;

	}

	/**
	 * Apply a Var Binding Clause within a given lookup
	 * 
	 * @param map
	 *            the given MAP that needs to be evaluated in a VarBinding
	 *            Clause
	 * @param kqlParams
	 *            Parameters for setting in case of bind access
	 * @param kqlParamPos
	 *            preserving the order of the bind access parameter
	 * @return the result after applying a VarBinding Clause which is a Map of
	 *         variables and their values
	 */
	public DataType executeXRedisVarBindingClause(MapType map, List<char[]> kqlParams, int kqlParamPos) {

		MapType valResult = new MapType();
		if (map != null) {
			valResult.setAll(map.returnMap());
		}
		for (Map.Entry<Variable, LookUpExpression> entry : varBinding.entrySet()) {
			valResult.putToMap(entry.getKey().toString(),
			        entry.getValue().executeXRedisLookUpExpression(map, kqlParams, kqlParamPos));
		}
		return valResult;
	}

	/*
	 * ========================================================================
	 * String Representation of Variable Binding in FROM Clause
	 * ========================================================================
	 */
	@Override
	public String toString() {
		StringBuilder varBindingString = new StringBuilder();
		boolean begin = true;
		for (Map.Entry<Variable, LookUpExpression> e : varBinding.entrySet()) {
			if (!begin)
				varBindingString.append(", ");
			begin = false;
			varBindingString.append(e.getKey().returnVariableName());
			varBindingString.append(" IN ");
			varBindingString.append(e.getValue().toString());
		}
		return varBindingString.toString();
	}

}
