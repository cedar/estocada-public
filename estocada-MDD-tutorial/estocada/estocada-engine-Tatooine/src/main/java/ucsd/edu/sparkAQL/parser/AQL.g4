grammar AQL;

/*
 * ====================
 * XSparkSQLQuery
 * ====================
 */

aqlQuery

:
  expression
;
 
/*
 * ====================
 * Expression
 * ====================
 */

expression

:
  fwrQuery
  |primaryExpression
;
  
/*
 * =============================
 * FwrQuery (From Where Clause)
 * =============================
 */

fwrQuery

:
  forClause (whereClause)? returnClause
;

/*
 * ====================
 * For Clause
 * ====================
 */
 
 forClause 
 :
   FOR varBinding (',' varBinding)*
 ;
 
/*
 * ====================
 *  VarBinding Clause
 * ====================
 */
 
 varBinding 
 :
   variableRef IN primaryExpression
 ;
  
 /*
 * ====================
 * Where Clause
 * ====================
 */
 
 whereClause 
 :
   WHERE primaryExpression
 ;
 
 /*
 * ====================
 * Return Clause
 * ====================
 */
 
 returnClause 
 :
   returnStatement
 ;
 
 /*
 * ====================
 * Return Statement
 * ====================
 */
 
 returnStatement 
 :
   RETURN primaryExpression
 ;
/*
 * ====================
 *  Primary Expression
 * ====================
 */
primaryExpression
    : sourceAccessExpression						#otherrule
    | parenthesizedExpression						#otherrule
    | primaryExpression '[' INDEX ']'				#ArrayExpression
    | primaryExpression '.' STRING					#RecordExpression
    | primaryExpression '.' NAME					#RecordExpression
    | primaryExpression '=' STRING			 		#EqualExpression
    | primaryExpression 'OR' primaryExpression		#OrExpression
    | primaryExpression 'AND' primaryExpression		#AndExpression
    | 'NOT' primaryExpression						#NotExpression
    | variableRef								    #otherrule
    | recordConstructor								#otherrule
  	;
 
/*
 * ====================
 *  Source Access Expression
 * ====================
 */ 
 
  sourceAccessExpression
  : 
  	STRING
  ;
  
/*
 * =========================
 * Parenthesized Expression
 * =========================
 */ 
 
  parenthesizedExpression
  : 
  	'(' expression ')'
  ;
  
/*
 * =========================
 * Variable
 * =========================
 */ 
 
   variableRef
  : 
  	'$' NAME
  ;

/*
 * =========================
 * Record Constructor
 * =========================
 */ 
 
  recordConstructor
  : 
  	'{' attributeName (',' attributeName)* '}'
  	| '{' '}'
  ;
 
 
/*
 * =========================
 * Attribute Name
 * =========================
 */ 
 
  attributeName
  : 
    STRING ':' primaryExpression
    |NAME ':' primaryExpression
  ;  

/*
 * =========================
 * KEYWORDS
 * =========================
 */ 
 
FOR :    	F O R;
WHERE : 	W H E R E;
RETURN :    R E T U R N;
IN     :    I N;

/*
 * =====================
 * primitives
 */
 	
 INDEX
 	: '0' | [1-9] [0-9]*   
 	;
 
STRING
    :  '"' (ESCAPE | ~["\\])* '"'
    |  '\'' (ESCAPE | ~['\\])* '\''
    |  '?'
    ;

fragment ESCAPE
    : '\\' (['"\\/bfnrt] | UNICODE)
    ;
    
fragment UNICODE
    : 'u' HEX HEX HEX HEX
    ;

fragment HEX
    : [0-9a-fA-F]
    ;

NUMBER
    : '-'? INTEGER '.' [0-9]+ EXP?  // 1.35, 1.35E-9, 0.3, -4.5
    | '-'? INTEGER EXP              // 1e10 -3e4
    | '-'? INTEGER                  // -3, 45
    ;

fragment INTEGER
    : '0' | [1-9] [0-9]*            
    ;
    
fragment EXP
    :   [Ee] [-+]? INTEGER
    ;
    

/*
 * =====================
 * Identifiers
 * =====================
 */

NAME
    : [a-zA-Z] [a-zA-Z_0-9]*
    ;

/*
 * =====================
 * WhiteSpaces
 * =====================
 */

WHITESPACE
    : [ \t\n\r]+    ->  skip 
    ;

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];