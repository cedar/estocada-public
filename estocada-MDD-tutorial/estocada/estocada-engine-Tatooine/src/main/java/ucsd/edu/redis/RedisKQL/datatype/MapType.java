/**
 * 
 */
package ucsd.edu.redis.RedisKQL.datatype;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * A Map DataType
 * 
 * @author ranaalotaibi
 *
 */
public class MapType extends AbstractDataType {
	private Map<String, Object> map;

	public MapType() {
		map = new LinkedHashMap<String, Object>();
	}

	/**
	 * @param key
	 *            The key to retrieve a value FROM a Map
	 * 
	 * @return
	 */
	public Object getFromMap(String key) {
		return map.get(key);
	}

	/**
	 * @param key
	 *            The key of a value in a Map
	 * @param value
	 *            The value to be inserted in the Map
	 */
	public void putToMap(String key, Object value) {
		map.put(key, value);
	}

	/**
	 * @param mVal
	 *            The Map that needs to be appended with a current Map
	 */
	public void setAll(Map<String, Object> mVal) {
		map.putAll(mVal);
	}

	/**
	 * @return the Map
	 */
	public Map<String, Object> returnMap() {
		return map;
	}

	/**
	 * @return All keys in a Map
	 */
	public Set<String> getKeys() {
		return Collections.unmodifiableSet(map.keySet());
	}

	@Override
	public Map<String, Object> returneObject() {
		return map;
	}

	@Override
	public DataType returnDataType() {
		return this;
	}

	@Override
	public boolean equals(Object obj) {
		return this.returnMap().equals(((MapType) obj).returnMap());
	}
}
