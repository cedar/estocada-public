/**
 * 
 */
package ucsd.edu.redis.RedisKQL.runtime;

import java.util.Iterator;
import java.util.Map;

import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;
import ucsd.edu.redis.RedisKQL.query.RedisKQL;

/**
 * An evaluator for RedisKQL Query
 * 
 * @author ranaalotaibi
 * 
 */
public class RedisKQLEvaluator {
	/**
	 * Evaluates a RedisKQL query within a given Map
	 * 
	 * @param query
	 *            the query.
	 * @return the result value of evaluating the query
	 */
	public Object evaluateRedisKQLQuery(RedisKQL query) {
		assert (query != null);

		SetType resultValue = (SetType) query.evalaueRedisKQLQuery(null);
		SetType setType = new SetType();
		if (resultValue.returneObject().iterator().next() instanceof MapType) {
			Iterator<Object> setElements = ((SetType) resultValue).returneObject().iterator();
			while (setElements.hasNext()) {
				Map<String, Object> resultMap = ((MapType) (setElements.next())).returneObject();
				Map.Entry<String, Object> e = resultMap.entrySet().iterator().next();
				MapType resultInternalMap = ((MapType) e.getValue());
				setType.addToSet(resultInternalMap);

			}
			return setType;
		}
		return resultValue;

	}
}
