package ucsd.edu.sparkAQL.query;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * A fwrQuery Class
 * 
 * @author ranaalotaibi
 * 
 */
public class ForWhereReturnClause extends AbstractAQLQuery {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(ForWhereReturnClause.class);

    private ForClause forClause;

    private WhereClause whereClause;

    private ReturnClause returnClasue;

    private boolean parametrized;

    public ForWhereReturnClause(ForClause forClause, WhereClause whereClause, ReturnClause returnClasue,
            boolean paramterized) {

        this.forClause = forClause;
        this.whereClause = whereClause;
        this.returnClasue = returnClasue;
        this.parametrized = paramterized;

    }

    @Override
    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        Map<String, Dataset<Row>> varBindingMap = new LinkedHashMap<String, Dataset<Row>>();
        varBindingMap = forClause.evaluateForClause(varBindingVal, jsonPath);
        if (whereClause != null) {
            varBindingMap = whereClause.evaluateWhereClause(varBindingMap, jsonPath);
        }
        Dataset<Row> df = returnClasue.evaluateReturnClause(varBindingMap, jsonPath);
        return df;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(forClause);
        sb.append("\n");
        if (whereClause != null) {
            sb.append(whereClause);
            sb.append("\n");
        }
        sb.append(returnClasue);
        sb.append("\n");
        return sb.toString();
    }

    @Override
    public void setAQLParams(char[] param) {
        WhereClause.aqlParam = param;
    }

    @Override
    public char[] getAQLParams() {
        return WhereClause.aqlParam;
    }

    @Override
    public void reInitializeAQLParams() {
        WhereClause.aqlParam = null;

    }

    @Override
    public boolean hasParam() {
        return parametrized;

    }
}
