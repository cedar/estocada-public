
package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public interface ReturnClause {

    public Dataset evaluateReturnClause(Map<String, Dataset<Row>> varBindings, final String jsonPath);
}
