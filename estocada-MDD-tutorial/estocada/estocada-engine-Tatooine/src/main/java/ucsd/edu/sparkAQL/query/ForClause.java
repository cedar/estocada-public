package ucsd.edu.sparkAQL.query;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * A For clause.
 * 
 * @author ranaalotaibi
 * 
 */
public class ForClause {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(ForClause.class);

    private List<VarBinding> varBindings;

    public ForClause(List<VarBinding> varBindings) {

        this.varBindings = varBindings;
    }

    public Map<String, Dataset<Row>> evaluateForClause(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        Map<String, Dataset<Row>> varBindingsVal = new LinkedHashMap<String, Dataset<Row>>();
        if (varBindingVal != null) {
            varBindingsVal.putAll(varBindingVal);

        }
        for (VarBinding varBinding : varBindings) {
            if (!(varBindingsVal.isEmpty())) {
                varBindingsVal = varBinding.evaluateVarBiding(varBindingsVal, jsonPath);
            } else {
                varBindingsVal = varBinding.evaluateVarBiding(varBindingVal, jsonPath);
            }
        }
        return varBindingsVal;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FOR ");
        boolean begin = true;
        for (VarBinding groupByItem : varBindings) {
            if (!begin)
                sb.append(", ");
            begin = false;
            sb.append(groupByItem);
        }
        return sb.toString();
    }
}
