/**
 * 
 */
package ucsd.edu.sparkAQL.query;

/**
 * @author Rana Alotaibi
 * 
 */

public class StringType extends AbstractDataType {
	private String sString;

	public StringType(String string) {
		sString = string;
	}

	public String getString() {
		return sString;
	}

	@Override
	public String toString() {
		return "\"" + sString + "\"";
	}

}
