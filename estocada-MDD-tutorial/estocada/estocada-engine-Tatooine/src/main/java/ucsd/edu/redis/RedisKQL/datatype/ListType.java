package ucsd.edu.redis.RedisKQL.datatype;

import java.util.ArrayList;
import java.util.List;

/**
 * The class represents a List Type
 * 
 * @author ranaalotaibi
 *
 */
public class ListType extends AbstractDataType {

	private List<Object> Aelements;

	public ListType() {
		Aelements = new ArrayList<Object>();
	}

	/**
	 * @param listIndex
	 *            The index of a particular element
	 * @return The corresponding element at listIndex
	 */
	public Object getValueAt(int listIndex) {
		assert (listIndex >= 0 && listIndex < Aelements.size());
		return Aelements.get(listIndex);
	}

	/**
	 * @param arrayIndex
	 *            Remove element at listIndex
	 */
	public void removeValueAt(int listIndex) {
		assert (listIndex >= 0 && listIndex < Aelements.size());
		Aelements.remove(listIndex);
	}

	/**
	 * @param value
	 *            Value to be added to the list
	 */
	public void addToList(Object value) {
		Aelements.add(value);
	}

	/**
	 * @return Return the list/array itself
	 */
	public List<Object> returnArray() {
		return Aelements;
	}

	@Override
	public DataType returnDataType() {
		return this;
	}

	@Override
	public Object returneObject() {
		return Aelements;
	}
}
