package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * An Expression that could be for where return query or primary expression
 * 
 * @author ranaalotaibi
 *
 */
public interface Expression {

    /**
     * @param varBindingVal
     * @return the evaluated expression as a DataFrame
     */
    public Dataset<Row> evaluateExpression(final Map<String, Dataset<Row>> varBindingVal, final String jsonDataSetPath);

    /**
     * Sets AQL parameters for a map look up in case of bind access
     * 
     * @param paramter
     *            for bind access
     */
    public void setAQLParams(final char[] param);

    /**
     * Gets KQL parameters for bindAccess
     * 
     * @return
     */
    public char[] getAQLParams();

    /**
     * Re-Initialize KQLParams
     */
    public void reInitializeAQLParams();

    /**
     * Check parameterized query
     * 
     * @return
     */
    public boolean hasParam();
}
