// Generated from ucsd/edu/sparkAQL/parser/AQL.g4 by ANTLR 4.3
package ucsd.edu.sparkAQL.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AQLParser}.
 */
public interface AQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link AQLParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void enterWhereClause(@NotNull AQLParser.WhereClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void exitWhereClause(@NotNull AQLParser.WhereClauseContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull AQLParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull AQLParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#aqlQuery}.
	 * @param ctx the parse tree
	 */
	void enterAqlQuery(@NotNull AQLParser.AqlQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#aqlQuery}.
	 * @param ctx the parse tree
	 */
	void exitAqlQuery(@NotNull AQLParser.AqlQueryContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#recordConstructor}.
	 * @param ctx the parse tree
	 */
	void enterRecordConstructor(@NotNull AQLParser.RecordConstructorContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#recordConstructor}.
	 * @param ctx the parse tree
	 */
	void exitRecordConstructor(@NotNull AQLParser.RecordConstructorContext ctx);

	/**
	 * Enter a parse tree produced by the {@code EqualExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualExpression(@NotNull AQLParser.EqualExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EqualExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualExpression(@NotNull AQLParser.EqualExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code NotExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(@NotNull AQLParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NotExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(@NotNull AQLParser.NotExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(@NotNull AQLParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(@NotNull AQLParser.ReturnStatementContext ctx);

	/**
	 * Enter a parse tree produced by the {@code OrExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(@NotNull AQLParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OrExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(@NotNull AQLParser.OrExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#sourceAccessExpression}.
	 * @param ctx the parse tree
	 */
	void enterSourceAccessExpression(@NotNull AQLParser.SourceAccessExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#sourceAccessExpression}.
	 * @param ctx the parse tree
	 */
	void exitSourceAccessExpression(@NotNull AQLParser.SourceAccessExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void enterVarBinding(@NotNull AQLParser.VarBindingContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#varBinding}.
	 * @param ctx the parse tree
	 */
	void exitVarBinding(@NotNull AQLParser.VarBindingContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#returnClause}.
	 * @param ctx the parse tree
	 */
	void enterReturnClause(@NotNull AQLParser.ReturnClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#returnClause}.
	 * @param ctx the parse tree
	 */
	void exitReturnClause(@NotNull AQLParser.ReturnClauseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code AndExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(@NotNull AQLParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(@NotNull AQLParser.AndExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesizedExpression(@NotNull AQLParser.ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#parenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesizedExpression(@NotNull AQLParser.ParenthesizedExpressionContext ctx);

	/**
	 * Enter a parse tree produced by the {@code RecordExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterRecordExpression(@NotNull AQLParser.RecordExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code RecordExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitRecordExpression(@NotNull AQLParser.RecordExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#fwrQuery}.
	 * @param ctx the parse tree
	 */
	void enterFwrQuery(@NotNull AQLParser.FwrQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#fwrQuery}.
	 * @param ctx the parse tree
	 */
	void exitFwrQuery(@NotNull AQLParser.FwrQueryContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#forClause}.
	 * @param ctx the parse tree
	 */
	void enterForClause(@NotNull AQLParser.ForClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#forClause}.
	 * @param ctx the parse tree
	 */
	void exitForClause(@NotNull AQLParser.ForClauseContext ctx);

	/**
	 * Enter a parse tree produced by the {@code ArrayExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpression(@NotNull AQLParser.ArrayExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayExpression}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpression(@NotNull AQLParser.ArrayExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void enterAttributeName(@NotNull AQLParser.AttributeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#attributeName}.
	 * @param ctx the parse tree
	 */
	void exitAttributeName(@NotNull AQLParser.AttributeNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link AQLParser#variableRef}.
	 * @param ctx the parse tree
	 */
	void enterVariableRef(@NotNull AQLParser.VariableRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link AQLParser#variableRef}.
	 * @param ctx the parse tree
	 */
	void exitVariableRef(@NotNull AQLParser.VariableRefContext ctx);

	/**
	 * Enter a parse tree produced by the {@code otherrule}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterOtherrule(@NotNull AQLParser.OtherruleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code otherrule}
	 * labeled alternative in {@link AQLParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitOtherrule(@NotNull AQLParser.OtherruleContext ctx);
}