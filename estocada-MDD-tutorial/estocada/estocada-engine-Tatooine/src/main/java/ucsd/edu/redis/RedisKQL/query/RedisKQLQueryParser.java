package ucsd.edu.redis.RedisKQL.query;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import ucsd.edu.redis.RedisKQL.parser.RedisKQLLexer;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLParser;
import ucsd.edu.redis.RedisKQL.parser.RedisKQLQueryBuilder;

/**
 * A query Parser and Compiler.
 * 
 * @author ranaalotaibi
 * 
 */
public class RedisKQLQueryParser {
	/**
	 * Parses and return RedisKQL Query as an Instance of RedisKQL class
	 * 
	 * @param queryAsString
	 *            the query string.
	 * @return the query.
	 */
	public RedisKQL parse(String queryAsString) {
		RedisKQL query = null;
		try {
			ANTLRInputStream input = new ANTLRInputStream(queryAsString);
			RedisKQLLexer lexer = new RedisKQLLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);

			RedisKQLParser parser = new RedisKQLParser(tokens);
			parser.removeErrorListeners();
			ParseTree tree = parser.redisKQLQuery();
			ParseTreeWalker walker = new ParseTreeWalker();
			RedisKQLQueryBuilder builder = new RedisKQLQueryBuilder();
			walker.walk(builder, tree);

			query = builder.getRedisKQL(tree);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error: " + e.getMessage());
		}
		return query;
	}

}
