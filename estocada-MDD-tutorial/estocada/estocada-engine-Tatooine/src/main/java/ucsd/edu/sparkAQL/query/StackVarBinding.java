package ucsd.edu.sparkAQL.query;

import java.util.Stack;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * A For clause.
 * 
 * @author ranaalotaibi
 * 
 */
public class StackVarBinding {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(StackVarBinding.class);
    private static Stack<Dataset<Row>> varBindingStack = new Stack<Dataset<Row>>();

    static void showpush(Dataset<Row> df) {
        varBindingStack.push(df);
    }

    static Dataset<Row> showpop() {
        return varBindingStack.pop();
    }

    static Boolean isEmpty() {
        return varBindingStack.isEmpty();
    }

}
