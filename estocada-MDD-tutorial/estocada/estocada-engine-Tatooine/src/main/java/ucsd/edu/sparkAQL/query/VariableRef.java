package ucsd.edu.sparkAQL.query;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class VariableRef extends AbstractPrimaryExpression {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(VariableRef.class);

    private String variableRefName;

    public VariableRef(String variableRefName) {
        this.variableRefName = variableRefName;
    }

    public String getVariableRef() {
        return variableRefName;
    }

    @Override
    public String toString() {
        return variableRefName;
    }

    @Override
    public Dataset<Row> evaluateExpression(Map<String, Dataset<Row>> varBindingVal, final String jsonPath) {
        if (!(StackVarBinding.isEmpty())) {
            Dataset<Row> df = StackVarBinding.showpop().select(this.variableRefName.toString().replaceAll("\\$", ""));
            return df;
        } else {
            return varBindingVal.get(this.variableRefName);
        }
    }

    @Override
    public void setAQLParams(char[] param) {
        // TODO Auto-generated method stub

    }

    @Override
    public char[] getAQLParams() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void reInitializeAQLParams() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasParam() {
        // TODO Auto-generated method stub
        return false;
    }

}
