/**
 * 
 */
package ucsd.edu.redis.RedisKQL.runtime;

import redis.clients.jedis.Jedis;

/**
 * A Connection to the redis server on localhost
 * 
 * @author ranaalotaibi
 * 
 */
public class RedisConnection {
	public static Jedis jedis =  new Jedis("localhost");
	
	/**
	 *  Setup Redis with a selected database number
	 * @param dbNumber
	 * 			the database number in Redis
	 */
	public static void setUp(final int dbNumber)
	{
		jedis.select(dbNumber);
	}
	/**
	 * Connect to Redis after the setUp
	 * @return
	 */
	public static Jedis jedis()
	{
		return jedis;
	}
	/**
	 * Close connection to Redis
	 * @return
	 */
	public static void close()
	{
		 jedis.close();
	}
}
