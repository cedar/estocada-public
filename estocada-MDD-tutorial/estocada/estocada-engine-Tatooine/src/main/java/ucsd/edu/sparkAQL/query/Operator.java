package ucsd.edu.sparkAQL.query;

import java.util.List;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public interface Operator {

    public OperatorName getOperator();

    public Dataset<Row> evaluateOperator(Map<String, Dataset<Row>> varBinding, List<Object> operatorParameter);
}
