grammar RedisKQL;

/*
 * ====================
 * Query
 * ====================
 */
redisKQLQuery
:
	selectFromWhereQuery EOF
;

/*
 * =======================================
 * Select From Where Query 
 * TODISCUSS:WHERE Clause
 * =======================================
 */
selectFromWhereQuery
:
	select from ( limit )? ( offest )? 
;
/*
 * ====================
 * select
 * ====================
 */
select
:
	selectStatement
;

selectStatement
:
	SELECT selectItem (',' selectItem)*
;
selectItem
:
	variable
	|mapConstructor
	|arrayConstructor
	|setConsturctor
;
mapConstructor
:
   
   MAP '('  variable ':' variable ')'
;

arrayConstructor
:

  LIST '(' variable ')'

;
setConsturctor
:

	SET '(' variable ')'
;

/*
 * ====================
 * from
 * ====================
 */
from
:
	FROM varBinding
	(
		',' varBinding
	)*
;

varBinding
:
	variable IN lookUpExpression
;
/*
 * ====================
 * Map Expression
 * ====================
 */
lookUpExpression
:
	NAME '[' key ']' ( '[' key ']')* 	# VarMapLookUp
	| MAP '[' key ']'('[' key ']')* 	# MainMapLookUp
	| KEYS '[' mapName ']' 				# KeysMapLookUp
;
/*
 * ====================
 *	variable
 * ====================
 */
key
:
	stringKey
	|variable
;
stringKey
:
	STRING
;
variable
:
	NAME
;
mapName
:
	variable
	| MAP
;

/*
 * ====================
 * limit
 * ====================
 */
limit
:
	LIMIT INTEGER
;

/*
 * ====================
 * offest
 * ====================
 */
offest
:
	OFFEST INTEGER
;
/*
 * =======================
 * XRedis Query Keywords
 * =======================
 */
FROM
:
	F R O M
;

SELECT
:
	S E L E C T
;

WHERE
:
	W H E R E
;

OFFEST
:
	O F F E S T
;

LIMIT
:
	L I M I T
;

IN
:
	I N
;

MAP
:
	M A P
;

KEYS
:
	K E Y S
;
LIST
:
   L I S T
;

SET 
:
	S E T
;
/*
 * ===========================
 * String/Integers Primitives
 * ===========================
 */
NAME
:
	[a-zA-Z_] [a-zA-Z_0-9]*
;
STRING
    :  '"' (ESCAPE | ~["\\])* '"'
    |  '\'' (ESCAPE | ~['\\])* '\''
    |  '?'
    ;

fragment ESCAPE
    : '\\' (['"\\/bfnrt] | UNICODE)
    ;
    
fragment UNICODE
    : 'u' HEX HEX HEX HEX
    ;

fragment HEX
    : [0-9a-fA-F]
    ;
INTEGER
:
	'0'
	| [1-9] [0-9]*
;

/*
 * ====================
 *  Skip Whitespace
 * ====================
 */
WHITESPACE
:
	[ \t\n\r]+ -> skip
;
/*
 * ====================
 * Fragments
 * ====================
 */
fragment
A
:
	[aA]
;

fragment
B
:
	[bB]
;

fragment
C
:
	[cC]
;

fragment
D
:
	[dD]
;

fragment
E
:
	[eE]
;

fragment
F
:
	[fF]
;

fragment
G
:
	[gG]
;

fragment
H
:
	[hH]
;

fragment
I
:
	[iI]
;

fragment
J
:
	[jJ]
;

fragment
K
:
	[kK]
;

fragment
L
:
	[lL]
;

fragment
M
:
	[mM]
;

fragment
N
:
	[nN]
;

fragment
O
:
	[oO]
;

fragment
P
:
	[pP]
;

fragment
Q
:
	[qQ]
;

fragment
R
:
	[rR]
;

fragment
S
:
	[sS]
;

fragment
T
:
	[tT]
;

fragment
U
:
	[uU]
;

fragment
V
:
	[vV]
;

fragment
W
:
	[wW]
;

fragment
X
:
	[xX]
;

fragment
Y
:
	[yY]
;

fragment
Z
:
	[zZ]
;