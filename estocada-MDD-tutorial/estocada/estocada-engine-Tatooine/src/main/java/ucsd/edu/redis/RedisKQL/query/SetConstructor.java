/**
 * 
 */
package ucsd.edu.redis.RedisKQL.query;

import java.util.Iterator;

import com.google.gson.Gson;

import ucsd.edu.redis.RedisKQL.datatype.ListType;
import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;

/***********
 * 
 * This class is not used
 *
 ************/
/**
 * An Set Constructor
 * 
 * @author ranaalotaibi
 *
 */
public class SetConstructor extends AbstractSelectItem {

	private Variable variableName;

	/**
	 * Construct an set from Variables Values
	 * 
	 * @param vName
	 *            The variable name.
	 */
	public SetConstructor(Variable vName) {
		variableName = vName;
	}

	/**
	 * Return the variable name
	 * 
	 * @return the variable name.
	 */
	public SetType evaluateSelectItem(MapType val) {
		SetType returnedSet = new SetType();
		if (val.getFromMap(variableName.returnVariableName()) instanceof ListType) {
			ListType listValues = new ListType();
			listValues = (ListType) (val.getFromMap(variableName.returnVariableName()));
			Iterator<Object> listIterator = listValues.returnArray().iterator();
			while (listIterator.hasNext()) {
				returnedSet.addToSet((listIterator.next().toString()));
			}
		} else {
			if (val.getFromMap(variableName.returnVariableName()) instanceof SetType) {
				SetType setValues = new SetType();
				setValues = ((SetType) (val.getFromMap(variableName.returnVariableName())));
				returnedSet = setValues;
			}
		}

		return returnedSet;
	}

	/**
	 * Return JSONSet String Representation
	 * 
	 * @param setVal
	 *            the setVal to be converted to JSONSet
	 */
	public String JSONSet(SetType setVal) {
		return new Gson().toJson(setVal.getSet());
	}

	/*
	 * ========================================================================
	 * String Representation of SELECT Clause
	 * ========================================================================
	 */
	@Override
	public String toString() {
		return "Set ( " + variableName.returnVariableName() + " )";
	}

}
