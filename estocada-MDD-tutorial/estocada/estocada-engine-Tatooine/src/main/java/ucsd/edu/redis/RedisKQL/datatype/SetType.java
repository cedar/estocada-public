package ucsd.edu.redis.RedisKQL.datatype;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A Set Type
 * 
 * @author ranaalotaibi
 *
 */
public class SetType extends AbstractDataType {
	private Set<Object> Selements;

	public SetType() {
		Selements = new LinkedHashSet<Object>();
	}

	/**
	 * @param value
	 *            A string value to be added to a Set
	 * @return true if this set did not already contain the specified element
	 *         Throws
	 */
	public boolean addToSet(String value) {
		return Selements.add(value);
	}

	/**
	 * @param value
	 * @return true if this set did not already contain the specified element
	 *         Throws:
	 */
	public boolean addToSet(Object value) {
		return Selements.add(value);
	}

	/**
	 * @param setAllValue
	 *            Append setAllValue to the current set
	 */
	public void addValueAll(Set<String> setAllValue) {
		assert (setAllValue != null);
		Selements.addAll(setAllValue);
	}

	/**
	 * @return A Set of Elements
	 */
	public Set<Object> getSet() {
		return Selements;
	}

	/**
	 * @param set
	 *            A Set Value
	 * @return Return a first element in a Set
	 */
	public Object getFirstElement(SetType set) {

		Iterator<Object> setElements = ((SetType) set).returneObject().iterator();
		return setElements.next();
	}

	/**
	 * @param element
	 *            An element to be removed from a Set
	 * @return true if this set contained the specified element
	 */
	public boolean removeEelement(String element) {
		return Selements.remove(element);
	}

	@Override
	public DataType returnDataType() {
		return this;
	}

	@Override
	public Set<Object> returneObject() {
		return Selements;
	}

	@Override
	public boolean equals(Object obj) {
		boolean equality = false;

		if (getFirstElement((SetType) obj) instanceof SetType) {
			Iterator<Object> setElements = ((SetType) obj).returneObject().iterator();
			Iterator<Object> thisSetIterator = this.returneObject().iterator();
			while (setElements.hasNext()) {
				equality = thisSetIterator.next().equals(setElements.next());
			}
			return equality;
		}
		if (getFirstElement((SetType) obj) instanceof String) {

			Iterator<Object> setElements = ((SetType) obj).returneObject().iterator();
			Iterator<Object> thisSetIterator = this.returneObject().iterator();
			while (setElements.hasNext()) {
				equality = thisSetIterator.next().equals(setElements.next());
			}
			return equality;
		}

		if (getFirstElement((SetType) obj) instanceof MapType) {
			Iterator<Object> setElements = ((SetType) obj).returneObject().iterator();
			Iterator<Object> thisSetIterator = this.returneObject().iterator();
			while (setElements.hasNext()) {
				equality = thisSetIterator.next().equals(setElements.next());

			}
			return equality;
		}
		return false;

	}

}
