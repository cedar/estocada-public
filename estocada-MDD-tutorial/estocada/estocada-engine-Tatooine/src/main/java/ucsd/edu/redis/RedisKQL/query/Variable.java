package ucsd.edu.redis.RedisKQL.query;

import ucsd.edu.redis.RedisKQL.datatype.MapType;

/**
 * A Variable
 * 
 * @author ranaalotaibi
 *
 */
public class Variable extends AbstractSelectItem {
	private String variableName;

	/**
	 * Create a variable.
	 * 
	 * @param vName
	 *            The variable name.
	 */
	public Variable(String variableName) {
		this.variableName = variableName;
	}

	/**
	 * Return the variable name
	 * 
	 * @return the variable name.
	 */
	public String returnVariableName() {
		return variableName;
	}

	/*
	 * ========================================================================
	 * String Representation of Variable
	 * ========================================================================
	 */
	@Override
	public String toString() {
		return variableName;
	}

	@Override
	public Object evaluateSelectItem(MapType map) {

		return map.getFromMap(this.variableName);
	}

}
