/**
 * 
 */
package ucsd.edu.sparkAQL.query;

import org.apache.log4j.Logger;

public abstract class AbstractReturnClause implements ReturnClause {
    @SuppressWarnings("unused")
    private static final Logger log = Logger.getLogger(AbstractReturnClause.class);
}
