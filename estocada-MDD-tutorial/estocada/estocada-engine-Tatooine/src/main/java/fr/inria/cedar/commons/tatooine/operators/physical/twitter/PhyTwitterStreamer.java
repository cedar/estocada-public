package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

import java.time.Instant;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * Uses Twitter4J to connect to Twitter's Streaming API and fetch live tweets
 * Request parameters are as follows:
 * Users: A comma-separated list of user IDs, indicating the users whose tweets should be delivered on the stream
 * Keywords: A comma-separated list of phrases which will be used to determine what tweets will be delivered on the stream
 * Languages: A comma-separated list of BCP 47 language identifiers {@link https://tools.ietf.org/html/bcp47}
 * Coordinates: A comma-separated list of (longitude, latitude) pairs specifying a set of bounding boxes to filter tweets by
 * @author Raphaël Bonaque
 * @author Oscar Mendoza
 */
public class PhyTwitterStreamer extends BindAccess {
	
	/** Universal version identifier for the PhyTwitterLiveStreamer class */
	private static final long serialVersionUID = -3897051000664417654L;
	
	private static final Logger log = Logger.getLogger(PhyTwitterStreamer.class);
	
	// Twitter Streaming API input parameters 
	/** See {@link https://dev.twitter.com/streaming/overview/request-parameters} */
	private List<Long> users;	
	private List<String> keywords;	
	private List<String> languages;		
	private List<Coordinate> coordinates;	
	
	// Timeout in seconds
	private int timeout;
	private Instant timeoutInstant;
	
	// Twitter stream
	private static final TwitterStream STREAMER = new TwitterStreamFactory().getInstance();
	
	// Queue of tweets
	private Queue<NTuple> tweets;
	
	// Concurrency control
	// Note: Number of permits set to -1 so at least 2 tweets need to be fetched before releases can occur
	// This way right after the timeout is reached, the queue of output tweets will contain one final tweet
	private Semaphore semaphore = new Semaphore(-1);
	
	// Query placeholder
	private static final String PLACEHOLDER = "%s";
	
	// Constructor
	public PhyTwitterStreamer(List<Long> users, List<String> keywords, List<String> languages, List<Coordinate> coordinates, int timeout) {
		this.users = users;
		this.keywords = keywords;
		this.languages = languages;
		this.coordinates = coordinates;
		this.timeout = timeout;
	}
		
	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		
		boolean hasUsrParam = !CollectionUtils.isEmpty(users);
		boolean hasKeyParam = !CollectionUtils.isEmpty(keywords);
		boolean hasLanParam = !CollectionUtils.isEmpty(languages);
		boolean hasGpsParam = !CollectionUtils.isEmpty(coordinates);		
		
		// Query
		FilterQuery query = new FilterQuery();	
		
		List<String> parameters = new ArrayList<String>();
		
		if (hasUsrParam) {
			// For each specified user, the stream will contain:
			// Tweets created by the user
			// Replies to any tweet created by the user
			// Retweets of any tweet created by the user
			// Manual replies, created without pressing a reply button
			query.follow(users.stream().mapToLong(l->l).toArray());
			parameters.add(String.format("Users: %s", users.toString()));
		}
		
		if (hasKeyParam) {
			// Only tweets that contain all of input keywords will be returned, regardless of order and case
			query.track(keywords.stream().toArray(String[]::new));
			parameters.add(String.format("Keywords: %s", keywords.toString()));
		}
		
		if (hasLanParam) {
			// Only tweets detected as being written in one of these languages will be returned
			query.language(languages.stream().toArray(String[]::new));
			parameters.add(String.format("Languages: %s", languages.toString()));
		}
			
		// Only geo-located tweets falling within the requested bounding boxes will be returned
		// Note: Filtering on BOTH keywords and coordinates is NOT supported by the Streaming API
		if (hasGpsParam) {
			int index = 0;
			double[][] geo = new double[coordinates.size()][2];
			for (Coordinate coordinate : coordinates) {
				geo[index] = new double[]{coordinate.getLat(), coordinate.getLat()};
				index++;
			}
			parameters.add(String.format("Coordinates: %s", coordinates.toString()));
		}
		
		timeoutInstant = Instant.now().plusSeconds(timeout);
		tweets = new ArrayDeque<NTuple>();
		
		try {
	        StatusListener listener = new StatusListener() { 
	 
	        	@Override 
	        	public synchronized void onStatus(Status status) { 
	        		synchronized(tweets) {
	        			tweets.add(TwitterHelper.getNTupleFromStatus(status));
	        		}
	        		semaphore.release(1);
	        	} 
	 
	        	@Override 
	        	// Called upon deletion notices
	        	public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {} 
	 
	        	@Override 
	        	// This notice will be sent each time a limited stream becomes unlimited
	        	public void onTrackLimitationNotice(int numberOfLimitedStatuses) {} 
	 
	        	@Override 
	        	// Called upon location deletion messages
	        	public void onScrubGeo(long l, long l1) {} 	 
	 
	        	@Override 
	        	// Called when receiving stall warnings
	        	public void onStallWarning(StallWarning arg0) {}
	        
	        	@Override 
	        	public void onException(Exception ex) {
	        		String msg = String.format("Error in TweetListener: %s", ex.toString());
	        		log.error(msg);
	        	}
	        }; 
						
	        STREAMER.addListener(listener);
	        if (!hasUsrParam && !hasKeyParam && !hasLanParam && !hasGpsParam) {
	        	// Starts listening on random sample of all public statuses
		        String msg = String.format("Calling Twitter's Streaming API with timeout of %d seconds: "
		        		+ "listening on random sample of all tweets", timeout);
		        log.info(msg);
	        	STREAMER.sample();
	        } else {
				// Start consuming public statuses that match one or more filter predicates
		        String msg = String.format("Calling Twitter's Streaming API with timeout of %d seconds: "
		        		+ "listening for tweets that match parameters %s", timeout, parameters.toString());
		        log.info(msg);
				STREAMER.filter(query);
	        }	        
			
		} catch (Exception e) {
			String msg = String.format("Error listening for live tweets using the Streaming API: %s", e.getMessage());
			log.error(msg);
			throw new TatooineExecutionException(e);
		}
		
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}
	
	@Override
	public void close() throws TatooineExecutionException {
		STREAMER.cleanUp();
		STREAMER.shutdown();
	}
	
	@Override
	// Will return true as long as the input timeout hasn't been reached yet
	public boolean hasNext() throws TatooineExecutionException {
		return Instant.now().isBefore(timeoutInstant);
	}

	@Override
	// Uses a semaphore to wait until the listener fetches and processes a tweet
	public NTuple next() throws TatooineExecutionException {
		NTuple current = null;
		try {
			semaphore.tryAcquire((timeoutInstant.toEpochMilli() - Instant.now().toEpochMilli()), TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			String msg = String.format("The thread listening to live tweets was interrupted: %s", e.getMessage());
			throw new TatooineExecutionException(msg);
		}
		synchronized(tweets) {
			current = tweets.poll();
			return current;
		}
	}
	
	@Override
	// Sets parameterised keywords
	public void setParameters(NTuple params) throws IllegalArgumentException {
		try {
			checkParameters(params);
		} catch (TatooineExecutionException e) {
			String msg = e.getMessage();
			log.error(msg);
			throw new IllegalArgumentException(msg);
		}
		
		try {
			fillParameters(params);
		} catch (TatooineExecutionException e) {	
			String msg = e.getMessage();
			log.error(msg);
			throw new IllegalArgumentException(msg);
		}
	}
	
	private void checkParameters(NTuple params) throws TatooineExecutionException {
		int queryParamCount = StringUtils.countMatches(String.join(",", keywords), PLACEHOLDER);
		if (queryParamCount != params.nrsmd.colNo) {
			String msg = String.format("Invalid parameter count: expected %d, got %d", queryParamCount, params.nrsmd.colNo);
			throw new TatooineExecutionException(msg);
		}
	}
	
	/**
	 * Replaces placeholders in parameterised queries with actual parameters passed in as input
	 * @throws TatooineExecutionException
	 */
	private void fillParameters(NTuple params) throws TatooineExecutionException {
		NRSMD nrsmd = params.nrsmd;
		
		int index = 0;
		List<String> parameterisedKWs = new ArrayList<String>();
		for (String keyword : keywords) {
			String parameterisedKW = keyword;
			while (parameterisedKW.contains(PLACEHOLDER)) {
				String name = nrsmd.colNames[index];
				parameterisedKW = keyword.replaceFirst(PLACEHOLDER, new String((char[]) params.getValue(name)));
				index++;
			}
			parameterisedKWs.add(parameterisedKW);
		}
		
		this.keywords = parameterisedKWs;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public String getName() {
		String name = String.format("PhyTwitterLiveStreamer(%s, %s, %s, %d)", String.join(",", keywords), String.join(",", languages), 
				coordinates.stream()
				   .map(object -> Objects.toString(object, null))
				   .collect(Collectors.toList()), 
				   timeout);
		return name;
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String name = String.format("PhyTwitterLiveStreamer(%s, %s, %s, %d)", String.join(",", keywords), String.join(",", languages), 
				coordinates.stream()
				   .map(object -> Objects.toString(object, null))
				   .collect(Collectors.toList()), 
				   timeout);
		return "\n" + spaceForIndent + name;
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {		
		return new PhyTwitterStreamer(users, keywords, languages, coordinates, timeout);
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}		
}
