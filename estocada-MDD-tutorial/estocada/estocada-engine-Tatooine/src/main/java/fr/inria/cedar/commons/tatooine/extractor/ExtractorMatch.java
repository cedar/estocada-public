package fr.inria.cedar.commons.tatooine.extractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.IDs.ElementID;

/**
 * This class encapsulates the information needed to tell that a given XML node matches
 * a given view node (that is, a match).
 *
 * @author Ioana MANOLESCU
 */
public final  class ExtractorMatch {
	private static final Logger log = Logger.getLogger(ExtractorMatch.class);
	
	/**
	 * pre-order for this element in the document.
	 */
	int no;

	/**
	 * depth counted starting from the root. no and depth should be redundant.
	 */
	int depth;

	/**
	 * the ID that was constructed and retained from this document
	 */
	ElementID id;

	/**
	 * The tag of the element.
	 */
	String tag;

	/**
	 * match in the stack associated to the parent of the node to which this
	 * stack is associated.
	 */
	ExtractorMatch parent;

	/**
	 * If, when creating this match, there is another open match in the same
	 * stack, then that match becomes the ownParent of this match.
	 */
	ExtractorMatch ownParent;

	/**
	 * Children of this match, by the stack in which they are.
	 */
	HashMap<ExtractorMatchStack, ArrayList<ExtractorMatch>> childrenByStack;

	/**
	 * Children of this match in its own stack.
	 */
	ArrayList<ExtractorMatch> ownChildren;

	/**
	 * Becomes true when this match has been invalidated due to lack of
	 * required children/descendants.
	 */
	boolean erased;

	/**
	 * If this match is under a semijoin, we collect in minOccur: the minimum
	 * number of /a1/a2/a3.../ak descendants of elements on the path
	 * /b0/b1/b2/.../bl, where the path /b0/.../bl leads to the nearest
	 * ancestor above a semijoin edge, and the path /b0/b1/.../bl/a1/a2/.../ak
	 * is the path for this match
	 *
	 * If minOccur >=1 and underSemiJoin, then the node (and its descendants)
	 * are useless
	 */
	int minOccur;

	/**
	 * Each match points to its own stack -- this comes in handy at some point.
	 */
	ExtractorMatchStack theStack;

	private StringBuffer valSB;
	private String val;
	private StringBuffer contSB;
	private String content;


	/**
	 * Constructor method.
	 *
	 * @param no
	 *            The number of the path summary node for which this match is
	 *            created.
	 * @param parent
	 *            The parent match for this one.
	 * @param depth
	 *            The depth of this path summary node in the path summary.
	 */
	public ExtractorMatch(
			int no,
			ExtractorMatch parent,
			int depth,
			String tag) {
		this.no = no;
		this.depth = depth;
		this.parent = parent;
		this.tag = tag;
		this.childrenByStack = new HashMap<ExtractorMatchStack, ArrayList<ExtractorMatch>>();
		this.ownChildren = new ArrayList<ExtractorMatch>();
		this.erased = false;
		this.minOccur = -1;
		this.val = "";
		this.content = "";
		this.id = null;
		this.contSB = new StringBuffer();
		this.content = null;
		this.valSB = new StringBuffer();
		this.val = null;
	}

	public void setID(ElementID newID) {
		this.id = newID;
	}

	public void setVal(String val) {
		this.val = new String(val);
	}

	public String getVal(){
		if (val == null){
			val = new String(valSB);
		}
		return val;
	}

	public void addToVal(String add) {
		this.valSB.append(add);
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent(){
		if (content == null){
			content = new String(contSB);
		}
		return content;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void addToContent(String add) {
		this.contSB.append(add);
	}

	/**
	 * @param childmatch
	 *            A match in another stack.
	 */
	public void addChild(
			ExtractorMatch childExtractorMatch,
			ExtractorMatchStack theStack) {
		ArrayList<ExtractorMatch> v = childrenByStack.get(theStack);
		if (v == null) {
			v = new ArrayList<ExtractorMatch>();
		}
		v.add(childExtractorMatch);
		childrenByStack.put(theStack, v);

		//this.children.addElement(childExtractorMatch);
	}

	/**
	 * @param childmatch
	 *            A match in another stack.
	 */
	public void addOwnChild(ExtractorMatch childExtractorMatch) {
		this.ownChildren.add(childExtractorMatch);
	}

	public void displayTree() {
		StringBuffer sb = new StringBuffer();
		recDisplayTree(sb);
		log.debug(sb);
	}

	private void recDisplayTree(StringBuffer sb) {
		if (erased) {
			return;
		}
		sb.append(this.no);
		sb.append("\n ");
		sb.append("Tag: " + tag + " ID: " + id + " Val: " + val + " Cont: " + content + "\n");

		int cntChildren = 0;
		Iterator<ArrayList<ExtractorMatch>> iChildren = childrenByStack.values().iterator();
		while (iChildren.hasNext()) {
			cntChildren += iChildren.next().size();
		}

		if (cntChildren > 0) {
			sb.append("(");
		} else {
			sb.append(" ");
		}

		iChildren = childrenByStack.values().iterator();
		while (iChildren.hasNext()) {
			Iterator<ExtractorMatch> it = iChildren.next().iterator();
			while (it.hasNext()) {
				ExtractorMatch se = it.next();
				se.recDisplayTree(sb);
			}
		}

		if (cntChildren > 0) {
			sb.append(")\n");
		}
	}

	/**
	 * Removes this match from its own stack, and does the same with all its
	 * descendant entries (each one removed from its own stack).
	 */
	public void recErase() {
		this.theStack.removeEntry(this);

		Iterator<ArrayList<ExtractorMatch>> iChildren = childrenByStack.values().iterator();
		while (iChildren.hasNext()) {
			Iterator<ExtractorMatch> it = iChildren.next().iterator();
			while (it.hasNext()) {
				ExtractorMatch child = it.next();
				child.recErase();
			}
		}
	}
}

