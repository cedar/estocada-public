package fr.inria.cedar.commons.tatooine.utilities;

import java.io.Serializable;

/**
 * Bundle Class that holds the size of a view in the DHT. 
 * Two public fields are available: bytes and tuples that store sizes accordingly.
 * 
 * @author Asterios KATSIFODIMOS
 */
public class ViewSizeBundle implements Serializable {
	private static final long serialVersionUID = -7498317424667239081L;
	public double bytes = 0;
	public double cardinality = 0;

	public ViewSizeBundle(double bytes, double cardinality) {
		this.bytes = bytes;
		this.cardinality = cardinality;
	}

	public String toString() {
		return "Bytes=" + this.bytes + "|Cardinality=" + this.cardinality;
	}
}
