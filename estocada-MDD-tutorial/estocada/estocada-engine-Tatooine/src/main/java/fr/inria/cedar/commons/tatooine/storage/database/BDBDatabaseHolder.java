package fr.inria.cedar.commons.tatooine.storage.database;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.apache.log4j.Logger;

import com.sleepycat.je.Cursor;
import com.sleepycat.je.CursorConfig;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.DatabaseStats;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;
import com.sleepycat.je.Transaction;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.NTupleComparator;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.NTupleBinding;
import fr.inria.cedar.commons.tatooine.loader.multidoc.EngineSystem;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.loader.multidoc.TreePatternGSR;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;
import fr.inria.cedar.ontosql.db.BerkeleyDB;
import fr.inria.cedar.ontosql.db.BerkeleyDBException;

/**
 * This class is managing the Berkeley DB jar. It handles the opening, closing, reading and writing to databases.
 *
 * @author Spyros ZOUPANOS
 * @author Jesus CAMACHO RODRIGUEZ
 */
public class BDBDatabaseHolder implements DatabaseHolder {
	private static final Logger log = Logger.getLogger(BDBDatabaseHolder.class);

	/* Constants */
	private static final boolean USE_MULTI_LEVEL_VIEWS = Parameters.getProperty("multiLevel.enabled").equalsIgnoreCase("true");
	private static final String BERKELEY_DB_NAME = Parameters.getProperty("berkeleyDBName");
	public static final String DEFAULT_ID = "BDBDatabaseHolder";

	/* This database holder unique identifier. */
	private final String id;

	private BerkeleyDB berkeleyDB;

	/*
	 * This map keeps the number of tuples that each database has. In this way we ensure that when we add tuples in the
	 * database, the old data are not overwritten by the new ones.
	 */
	private Map<String, Integer> tuplesAtEachBDB;

	/*
	 * The NTupleBinding is needed in order to convert a NTuple to a database record and vice-versa.
	 */
	private NTupleBinding ntb;

	/**
	 * When creating the key to store the tuple in the database use NTuples with only one field
	 */
	private static final boolean FLAT_INDEX_KEYS = Parameters.getProperty("flatIndexKeys").equalsIgnoreCase("true");

	private static BDBDatabaseHolder dbHolderInstance = null;

	private int cursorCounter;

	private Hashtable<Integer, Cursor> idToCursor;

	private Hashtable<Integer, DatabaseEntry> cursorIDToDatabaseEntry;

	private Hashtable<Integer, Transaction> cursorIDToTransaction;

	public BDBDatabaseHolder() throws BerkeleyDBException {
		this(DEFAULT_ID);
	}

	public BDBDatabaseHolder(final String id) throws BerkeleyDBException {
		this.id = id;
		berkeleyDB = new BerkeleyDB(BERKELEY_DB_NAME, Parameters.getProperty("berkeleyDBPath"),
				createEnvironmentConfig(), createDatabaseConfig());
		this.ntb = new NTupleBinding();
		this.tuplesAtEachBDB = new HashMap<String, Integer>();
		this.cursorCounter = 0;
		this.idToCursor = new Hashtable<Integer, Cursor>();
		this.cursorIDToDatabaseEntry = new Hashtable<Integer, DatabaseEntry>();
		this.cursorIDToTransaction = new Hashtable<Integer, Transaction>();

	}

	/**
	 * Method that will create an instance of the class or return the existing instance of it.
	 *
	 * @return an instance of the class
	 * @throws BerkeleyDBException
	 */
	public synchronized static BDBDatabaseHolder getInstance() throws BerkeleyDBException {
		if (dbHolderInstance == null) {
			dbHolderInstance = new BDBDatabaseHolder();
		}

		return dbHolderInstance;
	}

	// @Override
	public synchronized DatabaseStats getStats(GSR view) throws Exception {
		return berkeleyDB.getStats(getName(view));
	}

	@Override
	public EngineSystem getEngineSystem() {
		return EngineSystem.BDB;
	}

	@Override
	public String getId() {
		return id;
	}

	private synchronized String getName(final GSR givenPat) throws Exception {
		if (givenPat instanceof TreePatternGSR) {
			TreePatternGSR patGSR = (TreePatternGSR) givenPat;
			TreePattern pat = null;
			pat = XAMParserUtility.getTreePatternFromString(patGSR.getPropertyValue("XAMString"),
					patGSR.getPropertyValue("ENVIRONMENT_PARA"));
			return pat.getName();
		} else {
			throw new Exception("Unsupported GSR.");
		}
	}

	private synchronized EnvironmentConfig createEnvironmentConfig() {
		// TODO [zabetak] Add posibility for keeping all the data in memory for testing purposes.
		// Properties properties = new Properties();
		// // sets the DB to work "In Memory"
		// properties.put(EnvironmentConfig.LOG_MEM_ONLY, "true");
		// final EnvironmentConfig config = new EnvironmentConfig(properties);
		final EnvironmentConfig config = new EnvironmentConfig();
		config.setAllowCreate(true);
		config.setTransactional(true);
		return config;
	}

	private synchronized DatabaseConfig createDatabaseConfig() {
		final DatabaseConfig config = new DatabaseConfig();
		config.setAllowCreate(true);
		config.setTransactional(true);
		if (!FLAT_INDEX_KEYS) {
			config.setOverrideBtreeComparator(true);
			config.setBtreeComparator(new NTupleComparator());
		}
		config.setDuplicateComparator(new NTupleComparator());
		config.setSortedDuplicates(true);
		return config;
	}

	@Override
	public boolean containsDB(GSR view) throws Exception {
		final String name = getName(view);
		return berkeleyDB.containsDatabase(name);
	}

	/**
	 * This method will open a database for the view described by the given pattern.
	 * @param givenPat The GSR of the pattern
	 * @return A reference to the opened database
	 */
	@Override
	public synchronized void open(GSR view) throws Exception {
		final String name = getName(view);
		if (!berkeleyDB.containsDatabase(name)) {
			berkeleyDB.openDatabase(name, createDatabaseConfig());
			tuplesAtEachBDB.put(name, 0);
		}
	}

	/**
	 * This method closes all the open databases and environments.
	 */
	@Override
	public synchronized void close() {
		// closing the open cursors (if there are)
		Collection<Cursor> openCursors = idToCursor.values();
		if (idToCursor.size() != 0) {
			log.warn("The peer will shutdown, but the database still had " + idToCursor.size()
					+ " open cursors while trying to close.");
			for (Cursor cursor : openCursors) {
				cursor.close();
			}
		}
		openCursors.clear();

		berkeleyDB.close();
	}

	/**
	 * It removes a joined pattern database and it does the same for each of its patterns.
	 *
	 * @param view The GSR of the pattern
	 * @throws Exception
	 */
	@Override
	public synchronized void remove(GSR view) throws Exception {
		final String name = getName(view);
		berkeleyDB.truncateDatabase(name);
		tuplesAtEachBDB.remove(name);
	}

	/**
	 * This is public method that should be called outside the DatabaseHolder when we want to store some data in a
	 * database. It will do the necessary database and environment initialization (if needed) and it will call the
	 * (lower level) storeTuplesInDB method in order to store the tuples in the right database.
	 *
	 * @param givGSR the Global Storage Reference of the pattern
	 * @param wrappedTuples the packet with tuples that we want to store in the database
	 * @throws Exception
	 */
	@Override
	public void storeTuples(GSR view, Packet wrappedTuples) throws Exception {
		final String name = getName(view);
		open(view);
		if (!wrappedTuples.getTuples().isEmpty()) {
			if (view instanceof TreePatternGSR) {
				TreePatternGSR patGSR = (TreePatternGSR) view;
				TreePattern pat = XAMParserUtility.getTreePatternFromString(patGSR.getPropertyValue("XAMString"),
						patGSR.getPropertyValue("ENVIRONMENT_PARA"));
				TreePatternNode pn = pat.getRoot();

				NRSMD originalNRSMD = wrappedTuples.getTuples().iterator().next().nrsmd;

				ArrayList<NTuple> listTuplesWithDocIDs = new ArrayList<NTuple>();

				/*
				 * If the tuples already contain a documentURI, we don't need to add an extra one. (related to
				 * MultiLevel views)
				 */
				if (originalNRSMD.uriNo == 0) {
					// We will have to add the document ID to the tuples as there is none there.
					char[] docAddress = wrappedTuples.getSingleUri();
					TupleMetadataType[] types = { TupleMetadataType.URI_TYPE };
					NRSMD tempNRSMD = null;
					NRSMD resultNRSMD = null;

					try {
						tempNRSMD = new NRSMD(1, types);
						resultNRSMD = NRSMD.appendNRSMD(tempNRSMD, originalNRSMD);
					} catch (TatooineExecutionException e2) {
						log.error("Exception: ", e2);
					}
					// We create the tuple with the doc ID
					NTuple docIDTuple = new NTuple(tempNRSMD);
					docIDTuple.addUri(docAddress);
					NTuple resTup = null;
					for (NTuple tuple : wrappedTuples.getTuples()) {
						try {
							// before storing each NTuple in BDB, we append docID to it
							resTup = NTuple.append(resultNRSMD, docIDTuple, tuple);
						} catch (TatooineExecutionException e1) {
							log.error("Exception: ", e1);
						}
						listTuplesWithDocIDs.add(resTup);
					}
					wrappedTuples.setTuples(listTuplesWithDocIDs);
				}

				// We create the transaction that will be used for writing in the database
				Transaction txn = berkeleyDB.beginTransaction(null, null);
				Integer tupleNo = tuplesAtEachBDB.get(name);

				if (tupleNo == null) {
					storeTuplesInDB(wrappedTuples, berkeleyDB.getDatabase(name), pat.isOrdered(), txn, 0, pn);
					tuplesAtEachBDB.put(name, wrappedTuples.getTuples().size());
				} else {
					storeTuplesInDB(wrappedTuples, berkeleyDB.getDatabase(name), pat.isOrdered(), txn, tupleNo, pn);
					tuplesAtEachBDB.put(name, tupleNo + wrappedTuples.getTuples().size());
				}
			}
		}
	}

	/*
	 * This method stores a pack of wrappedTuples in the database. It stores them locally (no network transfer
	 * involved), because this method is called on the receiver side. It also stores them with the docID as many times
	 * as there are tuples (it unpacks the docIDs). This is trading storage space for supposed efficiency at runtime
	 * (not having to "produce" the docIDs when scanning the view data). The method could (almost) be static: it uses no
	 * state from this class (observe that the packet of tuples is given, and so is the database); again, it happens on
	 * the "receiver" side
	 * @param wrappedTuples
	 * @param givenDB
	 * @param tupleNo
	 */

	private synchronized void storeTuplesInDB(Packet wrappedTuples, Database givenDB, boolean orderedXAM,
			Transaction txn, int tupleNo, TreePatternNode pn) {
		NRSMD nrsmdTuples = wrappedTuples.getTuples().iterator().next().nrsmd;

		NRSMD keyNRSMD = null;
		int[] keyMask = null;
		try {
			if (orderedXAM) {
				// If the XAM is ordered, and if there are some IDs, then the
				// key consists of the docID+ID tuple.
				keyMask = makeOrderAttributeIndices(nrsmdTuples);
				keyNRSMD = NRSMD.makeProjectRSMD(nrsmdTuples, keyMask);
			} else {
				TupleMetadataType[] types = { TupleMetadataType.URI_TYPE, TupleMetadataType.UNIQUE_ID };
				keyNRSMD = new NRSMD(2, types);
			}

			// if we use multilevel views, we have to inform the nested scan that the
			// tuples of a specific document have all arrived so that it does not wait
			// for more.
			if (USE_MULTI_LEVEL_VIEWS && !wrappedTuples.getTuples().isEmpty()) {
				wrappedTuples.getTuples().add(NTuple.nullTuple(nrsmdTuples));
			}

		} catch (Exception e) {
			log.error("Exception: ", e);
		}

		char[] docAddress = wrappedTuples.getSingleUri();
		int tupleCounter = tupleNo;
		NTuple keyTuple = null;
		DatabaseEntry key = null;
		DatabaseEntry value = null;
		boolean indexExist = pn.requiresSomething();

		int cnt = 0;
		int glcnt = 0;
		for (NTuple tuple : wrappedTuples.getTuples()) {

			// here we store in the database
			key = new DatabaseEntry();
			value = new DatabaseEntry();

			// Create the key for the tuple
			if (indexExist) {
				// Then we index values based on the required fields
				try {
					keyTuple = DatabaseIndexing.makeIndexKeys(tuple, pn).get(0);
				} catch (TatooineExecutionException e) {
					log.error(e.toString());
				}
			} else if (orderedXAM) {

				// the key is the tuple with doc ID + IDs
				keyTuple = NTuple.project(tuple, keyNRSMD, keyMask);
			} else {
				// the key is the doc ID + tuple count
				keyTuple = new NTuple(keyNRSMD);
				keyTuple.addUri(docAddress);
				keyTuple.addID(new OrderedIntegerElementID(tupleCounter));
			}

			// The key is converted to a single column tuple of STRING_TYPE
			if (FLAT_INDEX_KEYS) {
				String[] strArray = null;
				TupleMetadataType[] types = { TupleMetadataType.STRING_TYPE };
				try {
					strArray = keyTuple.toStringArray();
					keyTuple = new NTuple(new NRSMD(1, types));

					StringBuffer strb = new StringBuffer();
					for (String element : strArray) {
						strb.append(element);
					}

					keyTuple.addString(strb.toString());
				} catch (TatooineExecutionException e) {
					log.error(e.toString());
				}

			}

			ntb.objectToEntry(keyTuple, key);

			// the value is the tuple
			ntb.objectToEntry(tuple, value);
			try {
				givenDB.put(txn, key, value);
				tupleCounter++;
			} catch (DatabaseException e) {
				txn.abort();
				log.error("Exception: ", e);
			}

			cnt++;
			if (cnt == 10000) {
				log.info("Saving " + glcnt + " tuples...");
				cnt = 0;
			}
			glcnt++;

		}
		txn.commit();
		// log.info(wrappedTuples.getTuples().size() + " tuples stored in " +
		// givenDB.getDatabaseName());
	}

	/*
	 * Returns an array with the list of column indexes that should be kept for creating a key tuple. The key tuple
	 * would contain the URI types and ID types of the NRSMD provided to the method.
	 * @param givNRSMD the NRSMD of the original tuple
	 * @return the array of columns indexes that should be projected for creating the key tuple
	 */
	private int[] makeOrderAttributeIndices(NRSMD givNRSMD) {
		ArrayList<Integer> orderCols = new ArrayList<Integer>();
		int iColumn = 0;
		for (TupleMetadataType type : givNRSMD.types) {
			if (type == TupleMetadataType.URI_TYPE || type == TupleMetadataType.UNIQUE_ID
					|| type == TupleMetadataType.ORDERED_ID || type == TupleMetadataType.STRUCTURAL_ID
					|| type == TupleMetadataType.UPDATE_ID) {
				orderCols.add(new Integer(iColumn));
			}
			iColumn++;
		}

		int[] keyMask = new int[orderCols.size()];
		int iAux = 0;
		for (int index : orderCols) {
			keyMask[iAux] = index;
			iAux++;
		}

		return keyMask;
	}

	/**
	 * This method opens a cursor for a specific database (which is identified by a pattern) and it returns its ID. This
	 * ID will be used in the future whenever we want to refer to the cursor.
	 *
	 * @param givenPat The GSR of the pattern
	 * @return The cursor's ID
	 * @throws Exception
	 */
	public synchronized int getCursor(GSR view) throws Exception {
		return getCursor(view, false);
	}

	/**
	 * @author Asterios KATSIFODIMOS This method opens a cursor for a specific database (which is identified by a
	 *         pattern) and it returns its ID. This ID will be used in the future whenever we want to refer to the
	 *         cursor.
	 * @param givenPat The GSR of the pattern, readUncommittedData whether to read or not uncommitted data (when true,
	 *            cursos reads while data is written)
	 * @return The cursor's ID
	 * @throws Exception
	 */
	public synchronized int getCursor(GSR view, boolean readUncommittedData) throws Exception {
		final String name = getName(view);
		Transaction txn = berkeleyDB.beginTransaction(null, null);
		CursorConfig cconfig = new CursorConfig();

		if (readUncommittedData) {
			// cconfig.setReadCommitted(false);
			cconfig.setReadUncommitted(true); // read also uncommitted data
		} else {
			cconfig.setReadCommitted(true); // Use committed reads for this cursor
		}

		Cursor cursor = berkeleyDB.openCursor(name, txn, cconfig);
		cursorCounter++;
		idToCursor.put(cursorCounter, cursor);
		cursorIDToTransaction.put(cursorCounter, txn);

		return cursorCounter;
	}

	/**
	 * This method returns the next record of a specific cursor which is identified by its ID
	 *
	 * @param cursorId the cursor's ID
	 * @return The next record or null, if no more records exist or no cursor exists with the given ID
	 */
	public NTuple getNextRecord(int cursorId) {
		if (NIterator.timeout) {
			return null;
		}
		Cursor curCursor = idToCursor.get(cursorId);
		if (curCursor == null) {
			return null;
		}
		DatabaseEntry key = new DatabaseEntry();
		DatabaseEntry value = new DatabaseEntry();
		OperationStatus status = OperationStatus.SUCCESS;
		try {
			status = curCursor.getNext(key, value, LockMode.DEFAULT);
		} catch (Exception e) {
			log.error(e);
			return null;
		}
		if (status == OperationStatus.SUCCESS) {
			cursorIDToDatabaseEntry.put(cursorId, key);
			return ntb.entryToObject(value);
		}
		return null;
	}

	/**
	 * This method returns the first record of a specific cursor which matches the given key
	 *
	 * @param cursorId the cursor's ID
	 * @param key the matching key
	 * @return The first record that matches the key or null, if there is no record with this key
	 */
	public NTuple getFirstRecord(int cursorId, DatabaseEntry key) {
		Cursor curCursor = idToCursor.get(cursorId);
		if (curCursor == null) {
			return null;
		}

		DatabaseEntry value = new DatabaseEntry();

		OperationStatus status = OperationStatus.SUCCESS;
		status = curCursor.getSearchKey(key, value, LockMode.DEFAULT);

		if (status == OperationStatus.SUCCESS) {
			cursorIDToDatabaseEntry.put(cursorId, key);
			return ntb.entryToObject(value);
		}
		return null;
	}

	/**
	 * This method returns the next record of a specific cursor that matches the given key
	 *
	 * @param cursorId the cursors ID
	 * @param key the matching key
	 * @return The next record that matches the given key or null if there is no other record with this key
	 */
	public NTuple getNextDuplicateRecord(int cursorId, DatabaseEntry key) {
		Cursor curCursor = idToCursor.get(cursorId);
		if (curCursor == null) {
			return null;
		}
		DatabaseEntry value = new DatabaseEntry();

		OperationStatus status = OperationStatus.SUCCESS;
		status = curCursor.getNextDup(key, value, LockMode.DEFAULT);

		if (status == OperationStatus.SUCCESS) {
			cursorIDToDatabaseEntry.put(cursorId, key);
			return ntb.entryToObject(value);
		}
		return null;
	}

	/**
	 * Gets the key for the last tuple that has been read from the database.
	 *
	 * @param cursorId the ID of the cursor that we are using for reading the tuples from the database
	 * @return the key for the last tuple that has been read from the database
	 */
	public DatabaseEntry getKeyForLastTuple(int cursorId) {
		return cursorIDToDatabaseEntry.get(cursorId);
	}

	/**
	 * This method closes a cursor.
	 * 
	 * @param cursorId The ID of the cursor that we want to close
	 */
	public synchronized void closeCursor(int cursorId) {
		Cursor curCursor = idToCursor.remove(cursorId);
		if (curCursor != null) {
			curCursor.close();
			curCursor = null;
			Transaction txn = cursorIDToTransaction.remove(cursorId);
			txn.commit();
		}
	}

	@Override
	public boolean useOSFile() {
		return false;
	}
}
