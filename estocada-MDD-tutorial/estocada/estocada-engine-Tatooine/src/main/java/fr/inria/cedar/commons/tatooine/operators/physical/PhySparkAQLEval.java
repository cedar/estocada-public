package fr.inria.cedar.commons.tatooine.operators.physical;

import org.apache.spark.sql.Dataset;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import ucsd.edu.sparkAQL.query.AQLQueryParser;
import ucsd.edu.sparkAQL.query.Expression;
import ucsd.edu.sparkAQL.runtime.AQLEvaluator;

/**
 * Physical SparkAQL operator.
 * 
 * @author Rana Alotaibi
 *         TODO: Refactor operator just like the other Scan OPs were refactored (PhyJENAEval, PhySOLREval, PhySOLRScan, PhySQLEval, PhySQLScan, PhyJQEval, PhyJQScan)
 */
public class PhySparkAQLEval extends BindAccess {

    private static final long serialVersionUID = -8787186622219168434L;
    protected final String query;
    protected final StorageReference sref;
    protected Expression queryAQL;
    protected AQLEvaluator evaluator;
    protected SparkAQLResult aqlResult;

    public PhySparkAQLEval(String query, NRSMD nrsmd, StorageReference sref) throws TatooineExecutionException {
        super();
        this.query = query;
        this.sref = sref;
        this.aqlResult = null;
        this.queryAQL = new AQLQueryParser().parse(query);
        this.evaluator = new AQLEvaluator();
        this.nrsmd = nrsmd;

    }

    @Override
    public void open() throws TatooineExecutionException {
        if (!this.queryAQL.hasParam()) {
            Dataset df = evaluator.evaluateAQLQuery(queryAQL, sref.getPropertyValue("tableName"));
            this.aqlResult = new SparkAQLResult(df, nrsmd);
            aqlResult.open();
        }
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        return aqlResult.hasNext();
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        return aqlResult.next();
    }

    @Override
    public void close() throws TatooineExecutionException {
        aqlResult.close();
        this.aqlResult = null;
        this.nrsmd = null;
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        return aqlResult.getNestedIterator(i);
    }

    @Override
    public String getName() {
        return "PhySparkAQLEval";
    }

    @Override
    public String getName(int depth) {
        return getName();
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void initialize() throws TatooineExecutionException {
    }

    @Override
    public void setParameters(NTuple params) {
        throw new UnsupportedOperationException("Not implemented");

    }
}
