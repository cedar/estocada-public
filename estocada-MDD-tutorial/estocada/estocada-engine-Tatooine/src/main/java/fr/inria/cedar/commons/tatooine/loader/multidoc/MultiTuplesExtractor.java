package fr.inria.cedar.commons.tatooine.loader.multidoc;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.DocNtwID;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;

/**
 * Tuples extractor. It can be called locally or remotely via RMI.
 * @author Jesus CAMACHO RODRIGUEZ
 */
public class MultiTuplesExtractor {

	@SuppressWarnings("unused")
	/** Universal version identifier for the MultiTuplesExtractor class */
	private static final long serialVersionUID = 3496944496398396320L;

	/* Constant */
	private transient final int MAX_PATTERNS_TO_EXTRACT_IN_PARALLEL = Integer.parseInt(Parameters.getProperty("MaxPatternsToExtractInParallel"));

	public MultiTuplesExtractor() throws RemoteException {
		super();
	}

	/**
	 * Extractor for the tuples from a document that are relevant
	 * for a list of tree patterns.
	 * @param docId the document ID of the document that we will
	 * extract the tuples from
	 * @param list the list of tree patterns that will be used for
	 * extracting the tuples from the document
	 * @return a map from tree patterns to lists of extracted tuples
	 * @throws TatooineException
	 * @throws RemoteException
	 */
	public HashMap<TreePattern, ArrayList<NTuple>> extractMultiTuples (DocNtwID docId, ArrayList<TreePattern> list) throws TatooineException {
		HashMap<TreePattern, ArrayList<NTuple>> multiTuples = null;
		String filePath = DocumentsManager.getFilepath(docId.docName);


		// If the variable is set to Zero, then we are going to extract all patterns at once.
		if(MAX_PATTERNS_TO_EXTRACT_IN_PARALLEL == 0){
			multiTuples = (new MultiDocLoader()).extractTuples(filePath, list);
		}else{ // If not, we are going to extract them in buckets of max_tuples

			multiTuples = new HashMap<TreePattern, ArrayList<NTuple>>();
			ArrayList<TreePattern> all_patterns = new ArrayList<TreePattern>(list);
			HashSet<TreePattern> pattern_bucket = new HashSet<TreePattern>();

			int num_of_patterns = all_patterns.size();

			int extracted_patterns = 0;

			// Extract maximum *max_tuples* at a time
			// making sure that the number of the overall patterns is
			// not exceeded.
			while(extracted_patterns<num_of_patterns){
				for(int i=0; i< MAX_PATTERNS_TO_EXTRACT_IN_PARALLEL && extracted_patterns<num_of_patterns;i++){
					pattern_bucket.add(all_patterns.get(extracted_patterns));
					extracted_patterns++;
				}
				multiTuples.putAll((new MultiDocLoader()).extractTuples(filePath, pattern_bucket));
				pattern_bucket.clear();
			}
		}

		return multiTuples;
	}
}

