package fr.inria.cedar.commons.tatooine.utilities;

import java.util.List;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;

public interface NTupleFunction {
	public abstract List<NTuple> call(NTuple input) throws TatooineException;
	
	/**
	 * The metadata of the output tuples
	 */
	public abstract NRSMD getOutputNRSMD();
	
	/**
	 * A printable name for this function
	 */
	public String getName();
}
