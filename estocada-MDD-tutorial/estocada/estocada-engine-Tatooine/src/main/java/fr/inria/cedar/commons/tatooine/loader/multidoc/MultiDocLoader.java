package fr.inria.cedar.commons.tatooine.loader.multidoc;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.extractor.MultiDocBuilder;
import fr.inria.cedar.commons.tatooine.extractor.MultiPatternExtractor;
import fr.inria.cedar.commons.tatooine.loader.Loader;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;

/**
 * This loader has to be invoked for every newly created tuple, as this is more
 * efficient. This means the loader has to be the top-level class.
 *
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 */
public class MultiDocLoader implements Loader {

	public ArrayList<NTuple> extractedData;

	public ArrayList<ArrayList<NTuple>> extractedMultiData;

	public MultiDocLoader() throws TatooineException {
		this.extractedData = new ArrayList<NTuple>();
		this.extractedMultiData = new ArrayList<ArrayList<NTuple>>();
	}

	/**
	 * Extracts the tuples from a Reader (that can be a StringReader, or a
	 * FileReader etc.) It returns a set of tuples.
	 *
	 * @param XMLFileName
	 * @param x
	 * @return
	 * @throws TatooineException
	 */
	public List<NTuple> extractTuples(Reader xmlReader, TreePattern p)
			throws TatooineException {
		MultiPatternExtractor ext = new MultiPatternExtractor();
		TreePattern[] patterns = new TreePattern[1];
		MultiDocBuilder[] builders = new MultiDocBuilder[1];

		MultiDocBuilder mdbi = new MultiDocBuilder();
		mdbi.setLoader(this);
		builders[0] = mdbi;
		patterns[0] = p;

		this.extractedMultiData.add(new ArrayList<NTuple>());

		ext.extractMultiPatternData(xmlReader, patterns, builders);

		return extractedMultiData.get(0);
	}

	/**
	 * Extract tuples for several XAMs at a time.
	 * 
	 * @param XMLFileName
	 * @param xs
	 *            [jleblay] was change from to Collection to keep primitive type
	 *            handle a lower level.
	 * @return
	 * @throws TatooineException
	 */
	@Override
	public HashMap<TreePattern, ArrayList<NTuple>> extractTuples(
			String XMLFileName, Collection<TreePattern> xs) {
		MultiPatternExtractor ext = new MultiPatternExtractor();
		TreePattern[] patterns = new TreePattern[xs.size()];
		MultiDocBuilder[] builders = new MultiDocBuilder[xs.size()];
		int i = 0;
		for (TreePattern treePattern : xs) {
			MultiDocBuilder mdbi = new MultiDocBuilder();
			mdbi.setLoader(this);
			builders[i] = mdbi;
			patterns[i] = treePattern;
			i++;
			this.extractedMultiData.add(new ArrayList<NTuple>());
		}

		ext.extractMultiPatternData(XMLFileName, patterns, builders);

		HashMap<TreePattern, ArrayList<NTuple>> result = new HashMap<TreePattern, ArrayList<NTuple>>();

		for (ArrayList<NTuple> tuples : extractedMultiData) {
			result.put(patterns[patterns.length - (i--)], tuples);
			assert (i >= 0) : "Data extraction problem!";
		}
		this.extractedMultiData.clear();
		return result;
	}

	@Override
	public StorageReference[] load(String configFileName, String XMLFileName, TreePattern[] p) {
		throw new UnsupportedOperationException("The method load(String,String,TreePattern[])" +
				"is not implemented yet by the class MultiDocLoader.");
	}
}

