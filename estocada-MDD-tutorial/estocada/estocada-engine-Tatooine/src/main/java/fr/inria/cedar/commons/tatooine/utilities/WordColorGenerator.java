package fr.inria.cedar.commons.tatooine.utilities;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class WordColorGenerator {
		private static final int NUM_OF_TOPICS_PER_WORD = 3;
		private static final int NORMAL_TEXT_SIZE = 3;
		private static final int EMPHASIZED_TEXT_SIZE = 5;
		private static final String UNCLASSIFIED_COLOR_CODE = "#A9A9A9";
		private static final String[] COLOR_CODES = new String[] {
				"#5b7a35",
				"#bf57c5",
				"#59b74b",
				"#706adb",
				"#a9b34a",
				"#d24184",
				"#57b385",
				"#d33e3f",
				"#40bccc",
				"#d36b3a",
				"#649bd8",
				"#d69e39",
				"#5b66ad",
				"#a77b42",
				"#bc8fdb",
				"#b7595e",
				"#954e8e",
				"#e084af"
		};
		private static final String[] LIST_MINISTRIES = new String[] {
				"agriculture",
				"amenagement",
				"environnement",
				"economie",
				"education nationale",
				"interieur",
				"culture",
				"defense",
				"fonction publique",
				"affaires etrangeres",
				"sante",
				"sports",
				"familles",
				"finances",
				"outre-mer",
				"logement",
				"travail",
				"justice"
		};

		static class TopicWeight {
				String topicId;
				float weight;

				TopicWeight(String topicId, float weight) {
						this.topicId = topicId;
						this.weight = weight;
				}

				@Override
				public String toString() {
						return "TopicWeight{" +
										"topicId='" + topicId + '\'' +
										", weight=" + weight +
										'}';
				}
		}

		private static final JsonParser JSON_PARSER = new JsonParser();

		/**
		 *
		 * @param topicWordWeightsFile path to topic-word-weights file
		 * @param topicKeyFile path to the .keys file
		 * @return mapping between stem and its top topics
		 * @throws IOException
		 */
		public static Map<String, List<TopicWeight>> stemToTopTopicWeights(
						String topicWordWeightsFile,
						String topicKeyFile) throws IOException {
				long startTime = System.currentTimeMillis();

				Map<String, String> topicKeys = LLDAClassifierFunction.topicKeys(topicKeyFile);

				Map<String, List<TopicWeight>> mapStemListTopicWeight = new HashMap<>();
				Stream<String> stream = Files.lines(Paths.get(topicWordWeightsFile));
				stream.forEach(line -> {
						String[] items = line.split("\t");
						String stem = items[1];
						String topicId = topicKeys.get(items[0]);
						float weight = Float.parseFloat(items[2]);

						if (!mapStemListTopicWeight.containsKey(stem)) {
								mapStemListTopicWeight.put(stem, new LinkedList<>());
						}

						mapStemListTopicWeight.get(stem).add(new TopicWeight(topicId, weight));
				});

				Map<String, List<TopicWeight>> mapStemTopTopicWeight = new HashMap<>();
				mapStemListTopicWeight.forEach((stem, listTopicWeight) -> {
						listTopicWeight.sort(Comparator.comparing(topicWeight -> -topicWeight.weight));
						listTopicWeight = listTopicWeight.subList(0, NUM_OF_TOPICS_PER_WORD);
						mapStemTopTopicWeight.put(stem, listTopicWeight);
				});

				System.out.println("Time to load dictionary = " + (System.currentTimeMillis() - startTime));
				System.out.println();

				return mapStemTopTopicWeight;
		}

		/**
		 *
		 * @param topDocumentTopics topics of the given text document as inferred from LLDA model
		 * @param wordToStem mapping between a word and its stem, for all words in text document
		 * @param stemToTopTopicWeights mapping between stem and its top topics
		 * @return html content which represents each word by the color of that word's topic
		 * @throws IOException
		 */
		public String generateHtml(List<String> topDocumentTopics,
															 String wordToStem,
															 Map<String, List<TopicWeight>> stemToTopTopicWeights) throws IOException {

			if (topDocumentTopics.size() == 0) return "";

				Map<String, String> mapTopicColor = new HashMap<>();
				for(int topicId = 0; topicId < COLOR_CODES.length; ++topicId) {
						mapTopicColor.put("" + topicId, COLOR_CODES[topicId]);
				}

				StringBuilder htmlBuilder = new StringBuilder();
				StringBuilder tooltipBuilder = new StringBuilder();

				htmlBuilder.append("<html>");
				htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/business-frontpage.css\"/>");
				htmlBuilder.append("<body>");

				htmlBuilder.append("<b>Top " + topDocumentTopics.size() + " topics are</b><br/>");
				for(int index = 0; index < topDocumentTopics.size(); ++index) {
						String topicId = topDocumentTopics.get(index);
						htmlBuilder.append("<div class='" + LLDAClassifierFunction.getTopicRdfId(topicId) + "'>" +
										"<font size=" + (index == 0 ? EMPHASIZED_TEXT_SIZE : NORMAL_TEXT_SIZE) +
										" color=\"" + mapTopicColor.get(topicId) +
										"\">" + LIST_MINISTRIES[Integer.parseInt(topicId)] +
										"</font" +
								        "></div><br />"
						);
				}

				String mainTopics = topDocumentTopics.get(0);
				JSON_PARSER.parse(wordToStem).getAsJsonArray().forEach(jsonElement -> {
						JsonObject jsonObject = jsonElement.getAsJsonObject();
						String word = jsonObject.get("word").getAsString();
						String stem = jsonObject.get("stem").getAsString();

						if (stemToTopTopicWeights.containsKey(stem)) {
								List<TopicWeight> topTopicWeights = stemToTopTopicWeights.get(stem);

								tooltipBuilder.setLength(0);
								topTopicWeights.forEach(topicWeight ->
												tooltipBuilder.append(
																"label: " + LIST_MINISTRIES[Integer.parseInt(topicWeight.topicId)] +
																", weight: " + topicWeight.weight + " <br/>"
												)
								);

								String topicOfWord = topTopicWeights.get(0).topicId;
								String html =
												"<font size=" + (topicOfWord.equals(mainTopics) ? EMPHASIZED_TEXT_SIZE : NORMAL_TEXT_SIZE) +
												" color=\" " + mapTopicColor.get(topicOfWord) + " \">&nbsp" + word + "</font>";
								html = "<div class=\"colorword\">" + html +
												"<span class=\"colorword_tooltip\">" + tooltipBuilder.toString() +
												"</span></div>";

								htmlBuilder.append(html);
						} else {
								htmlBuilder.append(
												"<font size=" + NORMAL_TEXT_SIZE +
												" color=\" " + UNCLASSIFIED_COLOR_CODE + " \">&nbsp" + word + "</font>"
								);
						}
				});

				htmlBuilder.append("</body>");
				htmlBuilder.append("</html");

				return htmlBuilder.toString();
		}
}
