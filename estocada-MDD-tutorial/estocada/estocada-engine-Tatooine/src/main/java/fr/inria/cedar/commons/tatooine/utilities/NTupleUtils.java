package fr.inria.cedar.commons.tatooine.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.primitives.Ints;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Utility class used to simplify the creation of NTuple objects from TXT files containing NTuple definitions. When
 * parsing each line of a TXT file we create an NTupleReaderUtils object and store each NTuple field we find in it. We
 * then later build an NTuple object out of it.
 * 
 * @author Oscar Mendoza
 */
public class NTupleUtils {
	// Pointer to the current position of the TXT file we're parsing
	public int counter;

	// Fields needed to crate an NTuple object
	private List<ElementID> idFields = new ArrayList<ElementID>();
	private List<Integer> intFields = new ArrayList<Integer>();
	private List<String> strFields = new ArrayList<String>();
	private ArrayList<NTuple> nesFields = new ArrayList<NTuple>();

	// List of metadata types of an NTuple
	private List<String> names = new ArrayList<String>();
	private List<TupleMetadataType> types = new ArrayList<TupleMetadataType>();

	// List of metadata types of the children of an NTuple
	private List<NRSMD> children = new ArrayList<NRSMD>();

	/* Getters */
	public List<String> getStrFields() {
		return strFields;
	}

	public List<Integer> getIntFields() {
		return intFields;
	}

	public List<ElementID> getIdFields() {
		return idFields;
	}

	public ArrayList<NTuple> getNesFields() {
		return nesFields;
	}

	public List<TupleMetadataType> getTypes() {
		return types;
	}

	public List<String> getNames() {
		return names;
	}

	public List<NRSMD> getChildren() {
		return children;
	}

	/* Methods used to add new values */
	public void addString(String s) {
		strFields.add(s.trim());
	}

	public void addInt(Integer i) {
		intFields.add(i);
	}

	public void addId(ElementID e) {
		idFields.add(e);
	}

	public void addNestedField(NTuple tuple) {
		nesFields.add(tuple);
	}

	public void addType(TupleMetadataType type) {
		types.add(type);
	}

	public void addName(String name) {
		names.add(name.trim());
	}

	public void addChild(NRSMD child) {
		children.add(child);
	}

	/** Builds NTuple from NTupleReaderUtils */
	public NTuple buildTuple() throws TatooineExecutionException {
		return new NTuple(this.getTypes().size(), this.getTypes()
				.toArray(new TupleMetadataType[this.getTypes().size()]), this.getChildren().toArray(
				new NRSMD[this.getChildren().size()]), formatStringFields(this.getStrFields()), new char[][] {}, this
				.getIdFields().toArray(new ElementID[this.getIdFields().size()]), Ints.toArray(this.getIntFields()),
				formatNestedFields(this.getNesFields()));
	}

	/** Builds NTuple from NTupleReaderUtils */
	public NTuple buildTupleWithNames() throws TatooineExecutionException {
		return new NTuple(this.getTypes().size(), this.getTypes()
				.toArray(new TupleMetadataType[this.getTypes().size()]), this.getNames(), this.getChildren().toArray(
				new NRSMD[this.getChildren().size()]), formatStringFields(this.getStrFields()), new char[][] {}, this
				.getIdFields().toArray(new ElementID[this.getIdFields().size()]), Ints.toArray(this.getIntFields()),
				formatNestedFields(this.getNesFields()));
	}

	/** Transforms a List<NTuple> into an ArrayList<NTuple>[] needed to create an NTuple object */
	@SuppressWarnings("unchecked")
	private ArrayList<NTuple>[] formatNestedFields(List<NTuple> nesFields) {
		ArrayList<NTuple>[] tuples = (ArrayList<NTuple>[]) new ArrayList[nesFields.size()];
		int counter = 0;
		for (NTuple tuple : nesFields) {
			tuples[counter] = new ArrayList<>(Arrays.asList(tuple));
			counter++;
		}
		return tuples;
	}

	/** Finds number of characters of longest string of a string array */
	public int getNumCharsOfLongestStringInArray(String[] array) {
		int maxLength = 0;
		String longestString = null;
		for (String s : array) {
			if (s.length() >= maxLength) {
				maxLength = s.length();
				longestString = s;
			}
		}
		return longestString.length();
	}

	/** Transforms a List<String> into a char[][] needed to create an NTuple object */
	private char[][] formatStringFields(List<String> strFields) {
		int len = strFields.size() == 0 ? 0 : getNumCharsOfLongestStringInArray(strFields.toArray(new String[strFields
				.size()]));
		int i = 0;
		char[][] charFields = new char[strFields.size()][len];
		for (String str : strFields) {
			for (int j = 0; j < str.length(); j++) {
				charFields[i][j] = str.charAt(j);
			}
			i++;
		}
		return charFields;
	}
	
	@Override
	public int hashCode() {
		return 0; 
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		NTupleUtils utils = (NTupleUtils) obj;

		if ((utils.getNames().size() != getNames().size()) || (utils.getTypes().size() != getTypes().size())
				|| (utils.getIdFields().size() != getIdFields().size()) 
				|| (utils.getIntFields().size() != getIntFields().size())
				|| (utils.getStrFields().size() != getStrFields().size())
				|| (utils.getNesFields().size() != getNesFields().size())
				|| (utils.getChildren().size() != getChildren().size())) {
			return false;
		}
		
		for (int i = 0; i < utils.getIdFields().size(); i++) {
			if (!utils.getIdFields().get(i).equals(getIdFields().get(i))) {
				return false;
			}
		}
		
		for (int i = 0; i < utils.getIntFields().size(); i++) {
			if (!utils.getIntFields().get(i).equals(getIntFields().get(i))) {
				return false;
			}
		}
		
		for (int i = 0; i < utils.getStrFields().size(); i++) {
			if (!utils.getStrFields().get(i).equals(getStrFields().get(i))) {
				return false;
			}
		}
		
		for (int i = 0; i < utils.getNesFields().size(); i++) {
			if (!utils.getNesFields().get(i).equals(getNesFields().get(i))) {
				return false;
			}
		}
		
		for (int i = 0; i < utils.getChildren().size(); i++) {
			if (!utils.getChildren().get(i).equals(getChildren().get(i))) {
				return false;
			}
		}

		return true;
	}
	
	@Override
	public String toString() {
		return String.format("[N]: (%s) + [T]: (%s) + INT: (%s) + STR: (%s) + NES: (%s)", names, types, intFields, strFields, nesFields);
	}
}
