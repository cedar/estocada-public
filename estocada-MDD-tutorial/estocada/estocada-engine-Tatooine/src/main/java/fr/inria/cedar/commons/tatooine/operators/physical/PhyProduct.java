package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

public class PhyProduct extends NIterator {

	private static final long serialVersionUID = -6705887745077611392L;
    
	final protected NIterator left;
	final protected NIterator right;
    final protected List<NTuple> leftTuples = new ArrayList<>();

    protected NTuple nextTuple;
    protected NTuple rightTuple = null;
    protected int leftTuplesIndex = 0;

	public PhyProduct(NIterator left, NIterator right) throws TatooineExecutionException {
        super(left, right);
        this.left = left;
        this.right = right;
        this.nrsmd = NRSMD.appendNRSMD(left.nrsmd, right.nrsmd);
    }

	@Override
	public void open() throws TatooineExecutionException {
        this.left.open();
        this.right.open();

        initialize();
	}

	public void initialize() throws TatooineExecutionException {

        while(this.left.hasNext()) {
            NTuple tuple = this.left.next();
            leftTuples.add(tuple);
        }
    }

	@Override
	public boolean hasNext() throws TatooineExecutionException {

        if (this.leftTuples.isEmpty()) {
            return false;
        } else {
            // if initialization or end leftTuples
            if ((this.rightTuple == null) || this.leftTuplesIndex >= this.leftTuples.size()) {
                leftTuplesIndex = 0;
                // ask for a new right tuple
                if (this.right.hasNext()) {
                    this.rightTuple = this.right.next();
                } else {
                    return false;
                }
            }
            
            NTuple leftTuple = this.leftTuples.get(leftTuplesIndex);
            this.leftTuplesIndex++;
            this.nextTuple = NTuple.append(this.nrsmd, leftTuple, this.rightTuple);

            return true;
        }
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		return nextTuple;
	}

	@Override
	public void close() throws TatooineExecutionException {
        this.left.close();
        this.right.close();
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        throw new UnsupportedOperationException();
	}

	@Override
	public String getName() {
        String name = String.format("Product(%s,%s)", this.left.getName(1), this.right.getName(1));
        
        return name;
	}

	@Override
	public String getName(int depth) {
        String name = String.format("Product(%s,%s)", this.left.getName(1 + depth), this.right.getName(1 + depth));
        
        return name;
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyProduct(this.left.copy(), this.right.copy());
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        sb.append(firstAvailableNo + " [label=\"Product\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		int childNumber1 = left.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
		int childNumber2 = right.recursiveDotString(sb, firstAvailableNo, childNumber1);

		return childNumber2;
	}

    public NIterator getLeft() {
        return this.left;
    }

    public NIterator getRight() {
        return this.right;
    }

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
