package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

/**
 * Dummy XAM scan logical operator. It is a faked XAM scan.
 * @author Ioana MANOLESCU
 */
public class LogDummyXMLViewScan extends LogLeafOperator {
	private static final Logger log = Logger.getLogger(LogDummyXMLViewScan.class);
	private String xamName;

	public LogDummyXMLViewScan(StorageReference ref) throws TatooineException, TatooineExecutionException {
		this.ref = ref;
		String XAMString = this.ref.getPropertyValue("XAMString");
		// Parameters.logger.info("Got string: " + XAMString);

		try {
			pat = XAMParserUtility.getTreePatternFromString(XAMString, this.ref.getPropertyValue("ENVIRONMENT_PARA"));
			// XAM.init is needed here! This is a new XAM object and it needs initialization
			// x.init();
			this.setXamName(pat.getName());
		} catch (Exception pe) {
			log.error("Exception: ", pe);
			throw new TatooineExecutionException(pe.toString());
		}
		this.ref = ref;
		this.setVisible(true);
		TreePattern qp = (TreePattern) pat;
		this.setNRSMD(NRSMD.getNRSMD(qp.getRoot(), true, new HashMap<Integer, HashMap<String, ArrayList<Integer>>>()));
		this.setOwnName("LogDummyXAM(" + pat.getName() + ")");
	}

	public LogDummyXMLViewScan(TreePattern p) throws TatooineException, TatooineExecutionException {
		this.pat = p;
		this.setVisible(true);
		this.setNRSMD(NRSMD.getNRSMD(p.getRoot(), true, new HashMap<Integer, HashMap<String, ArrayList<Integer>>>()));
		this.setOwnName("LogDummyXAM(" + p.getName() + ")");
		this.setXamName(p.getName());
	}

	@Override
	public String getName() {
		return this.getOwnName();
	}

	@Override
	public void recDisplayNRSMD() {
		this.getNRSMD().display();
	}

	/**
	 * Added a clone method in order to have 2 readers from the same XAM. This method first clones the Xam and create a
	 * new XAMScan operator that uses the same storage reference and the cloned Xam. BDB/Postgres should deal with
	 * concurrency in multi-threaded environments. Thread safe?!
	 *
	 * @return a new XAMScan operators that reads data from the same BDB
	 */
	@Override
	public LogDummyXMLViewScan clone() throws CloneNotSupportedException {
		try {
			TreePattern cloneXam = ((TreePattern) this.pat).deepCopy(); // Same as clone()??????

			LogDummyXMLViewScan copy = new LogDummyXMLViewScan(cloneXam);
			copy.ref = ref;
			copy.setEquivalentPattern(this.getEquivalentPattern());
			return copy;

		} catch (Exception e) {
			log.error("Exception: ", e);
			return null;
		}
	}

	/**
	 * Makes a deep copy of the current operator
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogDummyXMLViewScan deepCopy() {
		try {
			return this.clone();
		} catch (CloneNotSupportedException e) {
			log.error("Exception: ", e);
			return null;
		}
	}

	@Override
	public int getJoinDepth() {
		return 0;
	}

	public void setXamName(String xamName) {
		this.xamName = xamName;
	}

	public String getXamName() {
		return xamName;
	}

	@Override
	public double estimatedCardinality() {
		return inputCardinality();
	}

	@Override
	public double inputCardinality() {
		return Catalog.getInstance().lookUpViewSize(this.ref.getPropertyValue("ENVIRONMENT_PARA")).cardinality;
	}

	@Override
	public double estimatedIOCost() {
		return Catalog.getInstance().lookUpViewSize(this.ref.getPropertyValue("ENVIRONMENT_PARA")).bytes;
	}

	@Override
	public double estimatedCPUCost() {
		return estimatedCardinality();
	}
}
