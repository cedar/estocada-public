package fr.inria.cedar.commons.tatooine.xam;

import java.io.Serializable;

import fr.inria.cedar.commons.tatooine.constants.PrintingLevel;

/**
 * This is the superclass for the {@link TreePattern} and the {@link JoinedPattern}.
 * 
 * @author Konstantinos KARANASOS
 * @created 20/02/2010
 */
public abstract class Pattern implements Serializable {

	/** Universal version identifier for the Pattern class */
	private static final long serialVersionUID = 2293823438774039209L;

	/** Unique identifier for the pattern */
	protected long patternID;

	/** The name of the pattern */
	protected String name;

	public Pattern() {
		super();
	}

	public Pattern(long patternID) {
		this.patternID = patternID;
	}

	public long getPatternID() {
		return patternID;
	}

	public void setPatternID(long patternID) {
		this.patternID = patternID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/** Counts the number of nodes in this pattern. Does not count the root of each tree pattern */
	public abstract int getNodesNo();

	public abstract String toString(PrintingLevel level);

	/** Transforms the pattern to a XAM string that can be used by the parser */
	public abstract String toXAMString();

	/** Prints the current pattern */
	public abstract void display();

	/**
	 * Creates an image representation of the pattern and stores it to a file.
	 * 
	 * @param givenFilename the name of the file where the image will be saved
	 * @param query true if the pattern that we want to draw is a query or false otherwise. Query nodes will be filled
	 *            in yellow and view nodes will be filled in blue
	 */
	public abstract void draw(String givenFilename, boolean query, String backgroundColor, String foregroundColor);
}
