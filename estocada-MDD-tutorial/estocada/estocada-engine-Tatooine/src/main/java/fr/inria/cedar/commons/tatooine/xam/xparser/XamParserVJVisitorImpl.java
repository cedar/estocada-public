package fr.inria.cedar.commons.tatooine.xam.xparser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.constants.IDGeneratorType;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;
import fr.inria.cedar.commons.tatooine.xam.catalog.SimpleIDGen;

/**
 * TODO ADD VALIDATION TESTS -the xam should be connex -no duplicate nodes
 *
 * @author Alin TILEA
 * @author Jesus CAMACHO RODRIGUEZ
 * @author Konstantinos KARANASOS
 * @last_modified 27/1/2010
 */
public class XamParserVJVisitorImpl implements XamParserVJVisitor {
	private static final Logger log = Logger.getLogger(XamParserVJVisitorImpl.class);
	/*
	 * A HashMap where all the PatternNodes that appear in the whole XAM file are stored. The key is the ID with which
	 * the nodes appear in the file and the value is the actual PatternNode.
	 */
	private HashMap<Integer, TreePatternNode> patternNodeMap;

	/*
	 * The key of the HashMap is the PatternNode that appears in the Xam file and the value is the (tree) Pattern to
	 * which it belongs.
	 */
	private HashMap<Integer, TreePattern> patternMap;

	/*
	 * Default namespace for the nodes.
	 */
	private String defaultNamespace;

	@Override
	public Object visit(SimpleNode node, Object data) {

		return null;
	}
	
	@SuppressWarnings("rawtypes")
	private String getClassName(Class c) {
		String name = c.getName();
		return name.substring(name.lastIndexOf('.') + 1);
	}

	/**
	 * the JoinedPattern is built from the tree patterns and the value joins specified
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object visit(ASTStart node, Object data) {
		patternNodeMap = new HashMap<Integer, TreePatternNode>();
		patternMap = new HashMap<Integer, TreePattern>();

		// Temp variables for building the patterns and the value-join edges
		TreePattern tempPat = null;
		try {
			Object child = node.jjtGetChild(0);
			Class c = child.getClass();

			if (getClassName(c).equals("ASTXAMSpec")) {
				tempPat = (TreePattern) visit(((ASTXAMSpec) child), data);
				return tempPat;
			}

		} catch (Exception E) {
			log.error("Exception: ", E);
		}
		return null;
	}

	/**
	 * Build the XAM representation in memory by gathering the nodes and the edges return the in-memory tree
	 * representation of the XAM
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object visit(ASTXAMSpec node, Object data) {
		defaultNamespace = new String("");
		TreePattern p = new TreePattern(SimpleIDGen.generateID(IDGeneratorType.XAM));
		LinkedList<TreePatternNode> nodes = new LinkedList<TreePatternNode>();
		boolean fromRoot = false;

		try {
			for (int i = 0; i < node.jjtGetNumChildren(); i++) {
				Object child = node.jjtGetChild(i);
				Class c = child.getClass();

				if (getClassName(c).equals("ASTNSPEC")) {
					// add all xam nodes to the in-memory tree structure x
					nodes = (LinkedList<TreePatternNode>) visit(((ASTNSPEC) child), data, nodes);
				} else if (getClassName(c).equals("ASTROOT")) { 
					// the first node is the root
					fromRoot = true;
				} else if (getClassName(c).equals("ASTDefaultNamespace")) {
					ASTDefaultNamespace defaultNamespaceNode = (ASTDefaultNamespace) child;
					ASTContent defaultNamespace = (ASTContent) defaultNamespaceNode.jjtGetChild(0);
					this.defaultNamespace = removeQuotes(defaultNamespace.getInfo());
				} else if (getClassName(c).equals("ASTXamOrdered")) {
					// the first node is the root
					p.setOrdered(true);
				} else if (getClassName(c).equals("ASTEdgeSpec")) {
					// add all xam edges to the in-memory tree structure x
					nodes = (LinkedList<TreePatternNode>) visit(((ASTEdgeSpec) child), data, nodes);
				} else {
					return null;// todo: maybe throw an exception instead
				}
			}
		} catch (Exception E) {
			log.error("Exception: ", E);
		}
		TreePatternNode theRoot = new TreePatternNode("", "", -1);
		theRoot.addEdge(nodes.getFirst(), fromRoot, false);
		p.setRoot(theRoot);

		// Add the PatternNodes with the Patterns to which they belong to the HashMap patternMap
		Iterator<TreePatternNode> iterPatNode = nodes.iterator();
		while (iterPatNode.hasNext()) {
			patternMap.put(iterPatNode.next().getNodeCode(), p);
		}

		return p;
	}

	/**
	 * from an AST node of type ASTIDSpec get XAMs id specification and store-it into the XAM's node
	 */
	public void getIDSpec(ASTIDSpec astNode, TreePatternNode patternNode) {
		// first part is an ID number
		SimpleNode idType = (SimpleNode) astNode.jjtGetChild(0);
		patternNode.setStoresID(true); // for XAM it was setIDSpecified(boolean)
		// then the type of the ID follows
		if (idType.toString().equals("Structural")) {
			patternNode.setIDType(false, false, true, false);
		} else if (idType.toString().equals("Ordered")) {
			patternNode.setIDType(false, true, false, false);
		} else if (idType.toString().equals("Integer")) {
			patternNode.setIDType(true, false, false, false);
		} else if (idType.toString().equals("Updating")) {
			patternNode.setIDType(false, false, false, true);
		} else {
			patternNode.setIDType(false, false, false, false);
			patternNode.setStoresID(false);
		}
		if (astNode.jjtGetNumChildren() == 2) {
			patternNode.setRequiresID(true);
		} else {
			patternNode.setRequiresID(false);
		}

	}

	/** Remove the " from the tag value specifications */
	public String removeQuotes(String s) {
		if (s.contains("\"")) {
			return s.substring(1, s.length() - 1);
		}
		return s;
	}

	/**
	 * from an AST node of type ASTTagRestriction get XAMs tag restriction specified as an equality predicate and
	 * store-it into the XAM's node (a tag restriction is a specification [Tag="gogo"] in the XAM) that restricts the
	 * elements stored under the current XAM to those that have the tagname "gogo"
	 */
	public void getTagRestrictionSpec(ASTTagRestriction astNode, TreePatternNode patternNode) {
		ASTContent namespace = (ASTContent) astNode.jjtGetChild(0);
		String tagString = removeQuotes(namespace.getInfo());
		if (tagString.contains("{")) {
			patternNode.setSelectOnTag(true, tagString.substring(tagString.indexOf("{") + 1, tagString.indexOf("}")),
					tagString.substring(tagString.indexOf("}") + 1));
		} else {
			patternNode.setSelectOnTag(true, this.defaultNamespace, tagString);
		}
	}

	/**
	 * from an AST node of type ASTValRestriction get XAMs value restriction specified as an equality predicate and
	 * store-it into the XAM's node (a tag restriction is a specification [Val="gogo"] in the XAM) that restricts the
	 * elements stored under the current XAM to those that have the value (text content or id content to "gogo")
	 */
	public void getValRestrictionSpec(ASTValRestriction astNode, TreePatternNode patternNode) {
		ASTContent restrValue = (ASTContent) astNode.jjtGetChild(0);
		patternNode.setSelectOnValue(true, removeQuotes(restrValue.getInfo()));
	}

	/**
	 * from an AST node of type ASTVal get XAMs value specification and store-it in the XAMs node
	 */
	public void getValSpec(ASTVal astNode, TreePatternNode patternNode) {
		patternNode.setSelectOnValue(false, "*");
		patternNode.setStoresValue(true);
		// it can have only a Required child
		if (astNode.jjtGetNumChildren() > 0) {
			patternNode.setRequiresVal(true);
		} else {
			patternNode.setRequiresVal(false);
		}

	}

	/**
	 * from an AST node of type ASTTag get XAMs tagname specification and store-it in the XAMs node
	 */
	public void getTagSpec(ASTTag astNode, TreePatternNode patternNode) {
		patternNode.setSelectOnTag(false, "", "*");
		patternNode.setStoresTag(true);
		// it can have only a Required child
		if (astNode.jjtGetNumChildren() > 0) {
			patternNode.setRequiresVal(true);
		} else {
			patternNode.setRequiresVal(false);
		}
	}

	@Override
	public Object visit(ASTNSPEC node, Object data) {
		return null;
	}

	/**
	 * for a ASTNSPEC node that correspond to a XAM node specification in the parser build and return the corresponding
	 * xamNode (element or attribute)
	 *
	 * @returns a new xamNode
	 */
	public Object visit(ASTNSPEC node, Object data, LinkedList<TreePatternNode> nodes) {
		TreePatternNode ne;

		try {
			for (int i = 0; i < node.jjtGetNumChildren(); i++) {
				ne = new TreePatternNode("", "*", -1);
				// a NE or NA node
				SimpleNode child1 = (SimpleNode) node.jjtGetChild(i);

				// if NA set "attribute" attribute to true				
				if (getClassName(child1.getClass()).equals("ASTNE")) {
					ne.setAttribute(false);
				} else {
					ne.setAttribute(true);
				}

				// all nodes have a node identifier - grab-it
				ASTMyID nodeId = (ASTMyID) child1.jjtGetChild(0);
				ne.setNodeCode(nodeId.getID());

				// for the children of NE test if we have IDSpec/TagRestriction
				// /ValRestriction/Tag.[Req] /Val[.Req]
				for (int j = 1; j < child1.jjtGetNumChildren(); j++) {
					// child2 is an IDSpec TagRestriction Val ValRestriction....
					SimpleNode child2 = (SimpleNode) child1.jjtGetChild(j);
					if (getClassName(child2.getClass()).equals("ASTIDSpec")) {
						getIDSpec((ASTIDSpec) child2, ne);
					} else if (getClassName(child2.getClass()).equals("ASTTagRestriction")) {
						getTagRestrictionSpec((ASTTagRestriction) child2, ne);
					} else if (getClassName(child2.getClass()).equals("ASTValRestriction")) {
						getValRestrictionSpec((ASTValRestriction) child2, ne);
					} else if (getClassName(child2.getClass()).equals("ASTTagFull")) {
						ne.setStoresTag(true); // Update, although actually we are not entering here
						if (child2.jjtGetNumChildren() > 0) {
							ne.setRequiresTag(true);
						}
					} else if (getClassName(child2.getClass()).equals("ASTValFull")) {
						ne.setStoresValue(true);
						if (child2.jjtGetNumChildren() > 0) {
							ne.setRequiresVal(true);
						}
					} else if (getClassName(child2.getClass()).equals("ASTTag")) {
						getTagSpec((ASTTag) child2, ne);
					} else if (getClassName(child2.getClass()).equals("ASTCSpec")) {
						// getSerializedContentSpec((ASTCSpec)child2,ne);
						ne.setStoresContent(true);
					} else {// ASTVal
						getValSpec((ASTVal) child2, ne);
					}
				}
				nodes.add(ne);

				// add the PatternNode to the HashMap patternNodeMap
				patternNodeMap.put(ne.getNodeCode(), ne);
			}

			return nodes;
		} catch (Exception E) {
			log.error("Exception: ", E);
		}
		return null;

	}

	@Override
	public Object visit(ASTNE node, Object data) {
		return visit((SimpleNode) node, data);
	}

	public Object visit(ASTSerializedContent node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTNA node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTOrdered node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTInteger node, Object data) {
		return visit((SimpleNode) node, data);
	}

	public Object visit(ASTNull node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTStructural node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTRequired node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTIDSpec node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTTag node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTTagRestriction node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTVal node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTValRestriction node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTContent node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTMyID node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTDescendant node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTChild node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTOuter node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTJoin node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTSemi node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTEdgeSpec node, Object data) {
		return visit((SimpleNode) node, data);
	}

	/**
	 * Get the edge specifications by getting the children of the node that should be From To Child/Desc Nested Join
	 *
	 * @param node the ASTNode storing info about the edge
	 * @param data the child subtree
	 * @param x the XAM to be updated
	 * @returns the Xam updated with the informations about the current edge
	 */
	public Object visit(ASTEdgeSpec node, Object data, LinkedList<TreePatternNode> nodes) {
		// XamEdge xe = new XamEdge();
		TreePatternNode from;
		TreePatternNode to;
		boolean parent = false;
		@SuppressWarnings("unused")
		boolean semijoin = false;
		@SuppressWarnings("unused")
		boolean outerjoin = false;
		boolean nested = false;

		// a NE or NA node
		ASTMyID fromNode = (ASTMyID) node.jjtGetChild(0);
		ASTMyID toNode = (ASTMyID) node.jjtGetChild(1);
		SimpleNode descendantTypeNode = (SimpleNode) node.jjtGetChild(2);
		from = patternNodeMap.get(fromNode.getID());
		to = patternNodeMap.get(toNode.getID());

		try {
			if (getClassName(descendantTypeNode.getClass()).equals("ASTChild")) {
				parent = true; // xe.setParent(true);
			}
			SimpleNode nestingTypeNode = (SimpleNode) node.jjtGetChild(3);
			SimpleNode joinTypeNode = null;
			
			if (getClassName(nestingTypeNode.getClass()).equals("ASTNested")) {
				// xe.setNested(true);
				nested = true;
				joinTypeNode = (SimpleNode) node.jjtGetChild(4);
			} else {
				// xe.setNested(false);
				joinTypeNode = nestingTypeNode;// the
				// nestingTypeNode
				// is absent
				// from the
				// AST
			}			
			if (getClassName(joinTypeNode.getClass()).equals("ASTJoin")) {// Simple
				// Join
				// xe.setJoinType(Constants.JOIN);			
			} else if (getClassName(joinTypeNode.getClass()).equals("ASTSemi")) {// Semijoin
				// xe.setJoinType(Constants.SEMI_JOIN);
				semijoin = true;
			} else {// Outer
				// xe.setJoinType(Constants.OUTER_JOIN);
				outerjoin = true;
			}
			// xe.setInternalID(SimpleIDGen.generateID(Constants.EDGES_KEY));
			if (from.getNodeCode() == to.getNodeCode()) {
				Exception e = new Exception("And edge between the same node has been found " + from.getNodeCode());
				throw e;

			}
			from.addEdge(to, parent, nested);
		} catch (Exception E) {
			log.fatal(E);
			System.exit(1);
		}

		return nodes;
	}

	@Override
	public Object visit(ASTNested node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTROOT node, Object data) {
		return visit((SimpleNode) node, data);
	}

	@Override
	public Object visit(ASTCSpec node, Object data) {
		return null;
	}

	@Override
	public Object visit(ASTNavigating node, Object data) {
		return null;
	}

	@Override
	public Object visit(ASTUpdating node, Object data) {
		return null;
	}

	@Override
	public Object visit(ASTXamOrdered node, Object data) {
		return null;
	}

	@Override
	public Object visit(ASTTagFull node, Object data) {
		if (node == null) {
			return null;
		}
		for (int i = 0; i < node.jjtGetNumChildren(); i++) {
			@SuppressWarnings("unused")
			Object child = node.jjtGetChild(i);
			// visit(child,data);
		}
		return null;
	}

	@Override
	public Object visit(ASTValFull node, Object data) {
		return null;
	}

	@Override
	public Object visit(ASTDefaultNamespace node, Object data) {
		return null;
	}

}