package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * Uses Twitter4J to connect to Twitter's Search API and retrieve specific tweets
 * Request parameters are as follows:
 * UserID: Identifier of the user whose tweets we want to retrieve
 * Username: Screen name of the user whose tweets we want to retrieve
 * Hashtag: Word or phrase preceded by a hash sigh (#) that marks a specific topic we're interested about
 * @author Oscar Mendoza 
 * @author Raphaël Bonaque
 */
public class PhyTwitterSearcher extends BindAccess {

	/** Universal version identifier for the PhyTwitterSearcher class */
	private static final long serialVersionUID = -3897051000664417654L;
	
	private static final Logger log = Logger.getLogger(PhyTwitterSearcher.class);
	
	// Twitter Search API input parameters 
	/** See {@link https://dev.twitter.com/rest/public/search} */
	private TwitterSearcherEnum mode;
	private Long userID;
	private String username;
	private String hashtag;
	private int numTweets;
	
	// List of output tweets
	private Queue<NTuple> tweets;
	
	// Default max number of tweets to retrieve at a time
	private static final int DEFAULT_NUM_TWEETS = 200;
	
	// Twitter instance
	private static final Twitter INSTANCE = TwitterFactory.getSingleton();
	
	/* Pagination variables */
	// Number of tweets to retrieve from a Twitter's Search API at a time
	private int tweetsPerAPICall;
	// It keeps track of the number of NTuple objects that have been processed in total
	private int cursorTotal = 0;
	// It keeps track of the current max tweet ID that was processed, used to control pagination
	private long pageMaxID = 0L;
	
	// Constructor
	public PhyTwitterSearcher(Long userID, String username, String hashtag, TwitterSearcherEnum mode) {
		this(userID, username, hashtag, mode, DEFAULT_NUM_TWEETS);
	}
	
	// Constructor
	public PhyTwitterSearcher(Long userID, String username, String hashtag, TwitterSearcherEnum mode, int numTweets) {
		this.mode = mode;
		this.userID = userID;
		this.username = username;
		this.hashtag = hashtag;
		this.numTweets = numTweets;
		
		// Checks
		String msg = "";
		switch (mode) {
			case USER_ID: {
				if (userID == null)
					msg = "Twitter searcher mode was set to USER_ID but no Long userID was provided";
				break;
			}
			case USER_NAME: {
				if (Strings.isNullOrEmpty(username)) 
					msg = "Twitter searcher mode was set to USER_NAME but no String username was provided";
				break;
			}
			case HASHTAG: {
				if (Strings.isNullOrEmpty(hashtag))
					msg = "Twitter searcher mode was set to HASHTAG but no String hashtag was provided";
				break;
			}
		}
		
		if (!Strings.isNullOrEmpty(msg)) {
			log.error(msg);
			throw new IllegalArgumentException(msg);
		}		
	}
	
	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		
		// Number of tweets to request from the Search API at a time
		int maxTweets = DEFAULT_NUM_TWEETS;
		String maxTweetsProperty = Parameters.getProperty("numberOfTweetsPerAPICall");
		try {
			if (!Strings.isNullOrEmpty(maxTweetsProperty)) {
				maxTweets = Integer.valueOf(maxTweetsProperty);
			}
		} catch (NumberFormatException nfe) {
			log.error("The number of tweets to fetch from Twitter's Search API at a time specified in the catalog not an integer");
		}
		tweetsPerAPICall = maxTweets;

		
		informPlysPlanMonitorOperStatus(1, getOperatorID());		
	}
	
	@Override
	public void close() throws TatooineExecutionException {}	
	
	private void getBatchTweets() throws TatooineExecutionException {
		tweets = new ArrayDeque<NTuple>();
		List<Status> statuses = new ArrayList<Status>();
		
		try {			
			// Set pagination variables
			int count = Math.min(tweetsPerAPICall, numTweets - cursorTotal);
			Paging paging = new Paging();
			paging.setCount(count);
			if (pageMaxID != 0L) {
				paging.setMaxId(pageMaxID);
			}
			
			// Call corresponding API endpoint
			switch (mode) {
				case USER_ID: {
					String msg = String.format("Calling Twitter's Search API with userID %d and paging count %d", userID, paging.getCount());
					log.info(msg);
					statuses = INSTANCE.getUserTimeline(userID, paging);
					break;
				}
				case USER_NAME: {
					String msg = String.format("Calling Twitter's Search API with username %s and paging count %d", username, paging.getCount());
					log.info(msg);
					statuses = INSTANCE.getUserTimeline(username, paging);
					break;
				}
				case HASHTAG: {			
					Query query = new Query("#" + hashtag);
					query.setCount(count);
					if (pageMaxID != 0L) {
						query.setMaxId(pageMaxID);
					}
					String msg = String.format("Calling Twitter's Search API with hashtag #%s and paging count %d", hashtag, query.getCount());
					log.info(msg);
					QueryResult result = INSTANCE.search(query);				
					statuses = result.getTweets();
				}
			}
					
			// Update pagination variables
			long maxID = Long.MAX_VALUE;
			for (Status status: statuses) {
				tweets.add(TwitterHelper.getNTupleFromStatus(status));
				if (status.getId() < maxID) {
					maxID = status.getId();
				}
			}
			
			pageMaxID = maxID - 1;			
			cursorTotal += count;
		
		} catch (TwitterException e) {
			String msg = String.format("Exception calling Twitter's Search API with parameters: (%s, %d, %s, %s, %d) and message: %s", 
				mode, userID, username, hashtag, numTweets, e.getMessage());
			throw new TatooineExecutionException(msg);
		}		
	}
	
	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// We don't need to keep on looking for results if we've already fetched enough
		if (cursorTotal >= numTweets) {
			return !CollectionUtils.isEmpty(tweets);
		} 
		// It's time to fetch more results from Twitter's Search API 
		if (CollectionUtils.isEmpty(tweets)) {
			getBatchTweets();
		}		
		return true;
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		NTuple current = tweets.poll();
		return current;
	}
	
	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		throw new UnsupportedOperationException("Not implemented");
	}
	
	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}
	
	@Override
	public String getName() {
		String name = String.format("PhyTwitterSearcher(%s, %d, %s, %s, %d)", mode, userID, username, hashtag, numTweets);
		return name;
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String name = String.format("PhyTwitterSearcher(%s, %d, %s, %s, %d)", mode, userID, username, hashtag, numTweets);
		return "\n" + spaceForIndent + name;
	}
	
	@Override
	public NIterator copy() throws TatooineExecutionException {		
		return new PhyTwitterSearcher(userID, username, hashtag, mode);
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}	
	
	
}
