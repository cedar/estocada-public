package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Physical operator that queries JSON data sources using Apache Spark SQL.
 * @author Oscar Mendoza
 */
public class PhyJQEval extends SparkJSONQueryParameterisedOperator implements PhyEvalOperator {

	/** Universal version identifier for the PhyJQEval class */
	private static final long serialVersionUID = -5097309867385242449L;
	
	private static final Logger log = Logger.getLogger(PhyJQEval.class);

	/* Constructor */
	public PhyJQEval(String query, GSR ref, NRSMD nrsmd) throws TatooineExecutionException {
		super(query, ref);
		this.nrsmd = inferMetadata(nrsmd, findOutputColumns(query));
	}
	
	/* Constructor */
	public PhyJQEval(String query, GSR ref, ViewSchema schema) throws TatooineExecutionException {
		this(query, ref, schema.getNRSMD());
	}
	
	@Override
	public String getName() {
		return "PhyJQEval";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}
	
	/** Attempts to determine the output columns of a given SQL query */
	private String findOutputColumns(String query) throws TatooineExecutionException {
		// The Spark SQL queries we support are of the form: 
		// SELECT [DISTINCT] col1, ..., colN FROM [...]		
		Pattern p = Pattern.compile("SELECT(\\s+DISTINCT)?\\s+(.*?)\\s+FROM");
		Matcher m = p.matcher(query);
		if (!m.find() || m.group(2) == null || m.group(2).isEmpty()) {
			String message = String.format("Failed to determine resulting metadata from input query: %s", query);
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		String match = m.group(2).trim();
		if (match.equals("*")) {
			log.error(MSG_USE_SCAN);
			throw new TatooineExecutionException(MSG_USE_SCAN);
		}
		return match;
	}
	
	@Override
	public NRSMD inferMetadata(NRSMD inputMD, String fieldsParam) throws TatooineExecutionException {					
		List<String> paramFields = new ArrayList<String>(); 
		for (String field : Arrays.asList(fieldsParam.split(","))) {
			paramFields.add(field.trim());
		}
		List<String> foundFields = new LinkedList<String>(paramFields);
		
		int counterCol = 0;
		int[] keepColumns = new int[paramFields.size()];			
		for (int i = 0; i < inputMD.colNames.length; i++) {
			String colName = inputMD.colNames[i];
			if (paramFields.contains(colName)) {
				keepColumns[counterCol] = i;
				counterCol++;
				foundFields.remove(colName);
			}					
		}		
		
		// All param fields in the query should also be present in the NRSMD
		if (!foundFields.isEmpty()) {
			String msg = String.format("The field(s) %s in the SELECT statement of the input query are not part of the input NRSMD [%s]", foundFields, String.join(" ", inputMD.colNames));
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		
		NRSMD resultMD = NRSMD.makeProjectRSMD(inputMD, keepColumns);
		return resultMD;
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		try {
			return new PhyJQEval(queryStr, ref, nrsmd);
		} catch (Exception e) {
			log.error("Exception copying PhyJQEval: ", e);
			return null;
		}
	}
}

