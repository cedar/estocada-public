package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * An adaptation of OrderedLongElementID for long
 */
public class OrderedLongElementID implements ElementID, Serializable {
	private static final Logger log = Logger.getLogger(OrderedLongElementID.class);

	private static final long serialVersionUID = 928326092797127292L;
	
	/** The actual ID */
	public long n;

	public OrderedLongElementID(long n) {
		this.n = n;
	}

	public static OrderedLongElementID theNull = new OrderedLongElementID(-1);
	
	@Override
	public int getType() {
		return 0;
	}

	@Override
	public String toString(){
	if (this == theNull){
		return TupleMetadataType.NULL.toString();
	}
	return ("" + n);
	}
	
	/** Returns true if ID1 is a parent of ID2 */
	@Override
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("LongIDs cannot answer isParentOf");
	}
	
	/** Returns true if ID1 is an ancestor of ID2 */
	@Override
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("LongIDs cannot answer isAncestorOf");
	}
	
	/** Returns the parent ID if it can be computed */
	@Override
	public ElementID getParent() throws TatooineExecutionException {
		throw new TatooineExecutionException("LongIDs cannot answer getParent");
	}
	
	/** Returns the null element for this kind of ID */
	@Override
	public ElementID getNull() {
		return theNull;
	}
	
	/** Returns true if this element ID is null */
	@Override
	public boolean isNull(){
		return (this == getNull());
	}
	
	@Override
	public boolean equals(Object o){
		try{
			OrderedLongElementID iid = (OrderedLongElementID)o;
			return (this.n == iid.n);
		}
		catch(ClassCastException cce){
			log.warn("ClassCastException: ", cce);
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return Long.hashCode(n);
	}
	
	/** Returns true if ID1 starts strictly after ID2 */
	@Override
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException {
		OrderedLongElementID other = (OrderedLongElementID)id2;
		return (this.n > other.n);
	}
	
	/** Returns true if ID1 ends strictly after ID2 */
	@Override
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException {
		throw new TatooineExecutionException("EndsAfter undefined for long IDs");
	}


}
