package fr.inria.cedar.commons.tatooine.operators.physical;

/**
 * Physical operator that accesses data from a given data source preserving its metadata.
 * This is equivalent to scanning a data source or performing a selection (filtering) on it.
 * @author Oscar Mendoza
 */
public interface PhyScanOperator {
	
	/** Checks if a query given to a physical scan operator preserves the metadata of the data source to be queried */
	public abstract boolean isScanOrFilter(String queryParams);	
	public static final String MSG_USE_EVAL = "Scan operators are meant for queries that preserve the metadata of the data source they're querying; for queries that do NOT preserve it, use Eval operators"; 
	
}
