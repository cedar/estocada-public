package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Memory-based vanilla version of a Sort operator.
 * 
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 * @created 14/08/2005
 */
public class PhyMemorySort extends NIterator {
	private static final Logger log = Logger.getLogger(PhyMemorySort.class);
	
	/** Universal version identifier for the MemorySort class */
	private static final long serialVersionUID = -9217289068647566645L;

	StringBuffer callTrace;
	int[] sortColumns;
	TreeMap<NTuple, ArrayList<NTuple>> map;
	NRSMD keyNRSMD;
	NTuple key;
	Iterator<NTuple> itRes;
	Iterator<NTuple> itKey;
	NTuple t;

	public NIterator child;

	/** The number of tuples that are read and sorted by the operator */
	int nTuple;

	/**
	 * A reference to the vector that we are processing at this moment. Note that in one vector there are all the
	 * NTuples that have the same key.
	 */
	ArrayList<NTuple> vRes;

	/**
	 * A reference to the current value of the key. Note that in the sort operator all the records are sorted based on a
	 * specific field which is (called) the key.
	 */
	NTuple currentKey;

	/** Flag that shows that we have reached the end of the sorted records of the operator */
	boolean over;

	public PhyMemorySort(NIterator child, int sortColumn) throws TatooineExecutionException {
		super(child);
		this.child = child;
		this.sortColumns = new int[1];
		this.sortColumns[0] = sortColumn;
		this.nrsmd = child.nrsmd;
		this.keyNRSMD = NRSMD.makeProjectRSMD(nrsmd, sortColumns);
		increaseOpNo();
	}

	public PhyMemorySort(NIterator child, int[] sortColumns) throws TatooineExecutionException {
		super(child);
		this.child = child;
		this.sortColumns = sortColumns;
		this.nrsmd = child.nrsmd;
		this.keyNRSMD = NRSMD.makeProjectRSMD(nrsmd, sortColumns);
		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		child.open();
		initialize();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public void initialize() throws TatooineExecutionException {
		nTuple = 0;
		over = false;
		map = new TreeMap<NTuple, ArrayList<NTuple>>(new SortComparator());

		while (child.hasNext()) {

			if (timeout) {
				break;
			}

			NTuple t = child.next();
			this.key = NTuple.project(t, this.keyNRSMD, sortColumns);
			ArrayList<NTuple> v = map.get(key);
			if (v == null) {
				v = new ArrayList<NTuple>();
				map.put(key, v);
			}
			v.add(t);
			nTuple++;

			if (timeout) {
				break;
			}
		}

		itKey = map.keySet().iterator();
		if (itKey.hasNext()) {
			currentKey = itKey.next();
			vRes = map.get(currentKey);
			itRes = vRes.iterator();
		} else {
			over = true;
		}
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (timeout) {
			return false;
		}

		// If the over flag is on, it means there are no more records at the sort operator
		// so we return directly false.
		if (over) {
			return false;
		} else {
			// having itRes == null is not normal behavior and that's why we throw an error.
			assert (itRes != null) : "Stop here";

			// If for the specific key we have more records, then we respond positively.
			if (itRes.hasNext()) {
				return true;
			} else {
				// We initialize the current vector reference
				vRes = null;

				// We move forward to the vector related to the next key...
				while (vRes == null && itKey.hasNext()) {
					if (timeout) {
						return false;
					}

					currentKey = itKey.next();
					vRes = map.get(currentKey);
				}

				// If there is such vector, we return true if it is not empty
				if (vRes != null) {
					itRes = vRes.iterator();
					return itRes.hasNext();
				}
				// Else we will have reached the end of the records available at the sort operator 
				// and we return false (and we also make this visible by switching on the over flag)
				else {
					over = true;
					return false;
				}
			}
		}
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		t = itRes.next();
		this.numberOfTuples++;
		return t;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());

		map = null;
		child.close();

		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return new PhyArrayIterator(t.getNestedField(i), t.nrsmd.getNestedChild(i));
	}

	@Override
	public String getName() {
		String stringRepresentation = "";
		String tabs = getTabs(PRINTING_INDENTATION_TABS);
		for (int i = 0; i < sortColumns.length; i++) {
			stringRepresentation += String.valueOf(sortColumns[i]);
			if (i < sortColumns.length - 1) {
				stringRepresentation += ",";
			}
		}
		return "MemorySort(" + child.getName(1) + "," + "\n" + tabs + "[" + stringRepresentation + "]" + "\n" + ")";
	}

	@Override
	public String getName(int depth) {

		String stringRepresentation = "";
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);

		for (int i = 0; i < sortColumns.length; i++) {
			stringRepresentation += String.valueOf(sortColumns[i]);
			if (i < sortColumns.length - 1) {
				stringRepresentation += ",";
			}
		}

		return "\n" + spaceForIndent + "MemorySort(" + child.getName(1 + depth) + "," + "\n" + tabs + "["
				+ stringRepresentation + "]" + "\n" + spaceForIndent + ")";
	}
	
	@Override
	public String getDetailedName() {
		String stringRepresentation = "";
		for (int i = 0; i < sortColumns.length; i++) {
			stringRepresentation += String.valueOf(sortColumns[i]);
			if (i < sortColumns.length - 1) {
				stringRepresentation += ",";
			}
		}
		return "MemorySort(" + stringRepresentation + ")";
	}

	class SortComparator implements Comparator<NTuple> {
		SortComparator() {}
		StringBuffer sb;

		@Override
		public int compare(NTuple o1, NTuple o2) {
			try {
				int k = o1.compareTo(o2);
				return k;
			} catch (Exception e) {
				log.error("Exception: ", e);
				return 0;
			}
		}
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyMemorySort(this.child.copy(), this.sortColumns);
	}

	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 * @author Aditya SOMANI
	 */
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"MemSort" + this.getOrderMaker().toString() + "\", color = "
				+ getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return child.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
	}
}
