package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

/**
 * Instances of this class are used as IDs for the physical operators.
 * @author Spyros ZOUPANOS
 */
public class PhysOpID implements Serializable {

	/** Universal version identifier for the PhysOpID class */
	private static final long serialVersionUID = 1L;

	/** A unique number for the operator at that peer */
	public int physOpNo;

	static private int physOpCounter = 0;

	/**
	 * It gives us a unique physical operator ID in the distributed system. 
	 * The NodeInformation module has to be initialized first in order to get meaningful IDs.
	 */
	public PhysOpID() {
		this.physOpNo = physOpCounter;
		physOpCounter++;
	}

	@Override
	public String toString() {
		return Integer.toString(this.physOpNo);
	}

	@Override
	public boolean equals(Object givenObj) {
		if (!(givenObj instanceof PhysOpID)) {
			return false;
		}
		PhysOpID givenPID = (PhysOpID) givenObj;
		if (this.physOpNo != givenPID.physOpNo) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		Long hc = new Long(this.physOpNo);
		hc = hc % Integer.MAX_VALUE;
		return hc.intValue();
	}
}
