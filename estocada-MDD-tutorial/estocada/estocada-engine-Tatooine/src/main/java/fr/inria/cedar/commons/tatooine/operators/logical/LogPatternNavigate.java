package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * Navigates on some CONT fields by adding subtrees underneath. Ignores all but first XAM. 
 * @author Ioana MANOLESCU
 */
public class LogPatternNavigate extends LogUnaryOperator {
	private static final Logger log = Logger.getLogger(LogPatternNavigate.class);
	public ArrayList<TreePattern> ps;

	public ArrayList<Integer> pos;

	public LogPatternNavigate(LogOperator op, ArrayList<TreePattern> ps, ArrayList<Integer> pos, NRSMD newNRSMD) {
		super(op);

		this.OUTPUT_TUPLES_OVERHEAD = 1.5;

		this.ps = ps;
		this.pos = pos;
		this.setNRSMD(newNRSMD);
		this.setOwnName("LogPatternNav");
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 0; i < ps.size(); i++) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append(pos.get(i) + "=>" + ps.get(i).toString());
		}
		sb.append("]");
		this.setOwnDetails(new String(sb));
		this.setVisible(true);
	}

	/**
	 * Builds a Navigate plan to align an operator to its virtual extended xam. BEWARE: this should be called on a leaf
	 * only, not on join plans!docu
	 * 
	 * @param operator
	 * @param navigationPattern
	 * @return
	 */
	public static LogOperator getNavigate(LogOperator operator, TreePattern navigationPattern) {
		ArrayList<TreePattern> patterns = new ArrayList<TreePattern>();
		ArrayList<Integer> positions = new ArrayList<Integer>();

		// nReal is the real xam, without expansion
		TreePatternNode originalPattern = LogPatternNavigate.copyWithoutVirtual(navigationPattern.getRoot());

		// Parameters.logger.debug("The real xam is: " + (new Xam(nReal, false)).toString());
		// its NRSMD
		NRSMD originalNRSMD = null;
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> hmReal = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
		try {
			originalNRSMD = NRSMD.getNRSMD(originalPattern, true, hmReal);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}

		// the Nav has the NRSMD obtained by cart.prod. of the real NRSMD followed by all the nav xam NRSMDs.
		NRSMD navigationNRSMD = originalNRSMD;
		// Parameters.logger.debug("Real NRSMD is: " + nrsReal.toString());

		// the NRSMD of the expanded xam. This is not the NRSMD of the Nav, but that of the projection on top of it.
		NRSMD expandedPatternNRSMD = null;
		HashMap<Integer, HashMap<String, ArrayList<Integer>>> hm = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
		try {
			expandedPatternNRSMD = NRSMD.getNRSMD(navigationPattern.getRoot(), true, hm);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		// Parameters.logger.debug("Desired NRSMD (Projection on top of Nav) is " + nrsx.toString());

		// Navigation on the real xam will add columns after
		// all the columns corresponding to real attributes.
		// Or, the expectation is that the extended xam (via navigation)
		// has exactly the metadata of the virtual extended xam.
		//
		// Therefore, a projection should be applied on the Navigate,
		// only to re-order columns according to the expectations.
		ArrayList<Integer> orderFix = new ArrayList<Integer>();
		int crtRealPosition = 0;
		int currentColumnNumber = originalNRSMD.colNo;
		orderFix.add(crtRealPosition);
		// Parameters.logger.debug("At " + (orderFix.size() - 1) + " should be docURI which is at " + 0 +
		// " in the initial operator (thus Nav) output");
		crtRealPosition++;

		try {
			Stack<TreePatternNode> patternStack = new Stack<TreePatternNode>();
			patternStack.push(navigationPattern.getRoot());
			// Parameters.logger.debug("Pushed: " + navP.root);
			while (!patternStack.empty()) {
				TreePatternNode currentNode = patternStack.pop();
				// Parameters.logger.debug("Popped: " + pn.tag);
				if (!currentNode.isVirtual()) {
					// Parameters.logger.debug("Not virtual!");
					if (currentNode.nodeStoresSomething()) {
						orderFix.add(crtRealPosition);
						// Parameters.logger.debug("At " + (orderFix.size() -1) + " should be " + pn.tag +
						// ".ID, which is at " + crtRealPosition + " in the Nav output");
						crtRealPosition++;
					}
				}
				if (currentNode.getParentEdge() != null && currentNode.getParentEdge().n1 != null) {

					// If a virtual node has a non-virtual parent, this is where the navigation should start
					if (!currentNode.getParentEdge().n1.isVirtual() && currentNode.isVirtual()) {

						TreePatternNode parent = currentNode.getParentEdge().n1;

						parent.setVirtual(true);
						parent.setStoresContent(false);
						parent.setStoresID(false);
						parent.setStoresValue(false);

						// Parameters.logger.debug("Node " + pn.tag + " is virtual and his parent " + parent.tag +
						// " is not");
						TreePattern thisNavPattern = new TreePattern(parent.deepCopy(), false);
						// Add a root node if needed - or else the extractor will get confused
						// The "type" of the edge (parent/child or ancestor/descendant) should
						// be the same as the type of the edge that the despatched TreePattern
						// had with its parent.
						if (thisNavPattern.getRoot().getNodeCode() != -1) {
							TreePatternNode theRoot = new TreePatternNode("", "", -1);
							theRoot.addEdge(thisNavPattern.getRoot(), true, false);
							thisNavPattern.setRoot(theRoot);
						}

						patterns.add(thisNavPattern);

						NRSMD thisVirtualMD = null;
						HashMap<Integer, HashMap<String, ArrayList<Integer>>> thisVirtualHM = new HashMap<Integer, HashMap<String, ArrayList<Integer>>>();
						thisVirtualMD = NRSMD.getNRSMD(thisNavPattern.getRoot(), true, thisVirtualHM);
						// Parameters.logger.debug("At first, NRSMD of navigation xam is: " + thisVirtualMD.toString());
						// avoid column no. 0 which is the doc URI
						int[] projectionMask = new int[thisVirtualMD.colNo - 1];
						for (int k = 0; k < projectionMask.length; k++) {
							projectionMask[k] = (k + 1);
						}
						thisVirtualMD = NRSMD.makeProjectRSMD(thisVirtualMD, projectionMask);

						for (int k = 0; k < thisVirtualMD.colNo; k++) {
							orderFix.add(currentColumnNumber + k);
						}

						currentColumnNumber += (thisVirtualMD.colNo);
						navigationNRSMD = NRSMD.appendNRSMD(navigationNRSMD, thisVirtualMD);

						// find out on which Cont attribute to navigate for this xam
						HashMap<String, ArrayList<Integer>> mapOfThisNode = hmReal.get(parent.getNodeCode());
						ArrayList<Integer> parentContIdxs = mapOfThisNode.get("Cont");
						Integer parentContIdx = parentContIdxs.get(0);
						if (parentContIdx != null) {
							positions.add(parentContIdx);
						} else {
							log.error("Could not find Cont of node " + "{"
									+ currentNode.getParentEdge().n1.getNamespace() + "}"
									+ currentNode.getParentEdge().n1.getTag());
						}
					}
				}

				for (int i = 0; i < currentNode.getEdges().size(); i++) {
					patternStack.push(currentNode.getEdges().get(i).n2);
				}
			}
		} catch (Exception e) {
			log.error(e.toString(), e);
		}

		if (patterns.size() > 0) {
			LogPatternNavigate nav = new LogPatternNavigate(operator, patterns, positions, expandedPatternNRSMD);
			return nav;
		} else {
			log.debug("No need for navigation");
			return operator;
		}
	}

	private static TreePatternNode copyWithoutVirtual(TreePatternNode n) {
		if (n.isVirtual()) {
			return null;
		} else {
			TreePatternNode copy = n.nodeCopy();
			for (int i = 0; i < n.getEdges().size(); i++) {
				PatternEdge eChild = n.getEdges().get(i);
				TreePatternNode nChild = eChild.n2;
				TreePatternNode copyChild = copyWithoutVirtual(nChild);
				if (copyChild != null) {
					copy.addEdge(copyChild, eChild.isParent(), eChild.isNested());
				}
			}
			return copy;
		}
	}

	/**
	 * Makes a deep copy of the current operator
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogPatternNavigate deepCopy() {
		ArrayList<Integer> newPos = new ArrayList<Integer>();
		for (int crtPos : this.pos) {
			newPos.add(crtPos);
		}

		// I don't do a deep copy of the ps, because it's not needed
		LogPatternNavigate copy = new LogPatternNavigate(this.getChild().deepCopy(), this.ps, newPos, this.getNRSMD());
		copy.setEquivalentPattern(this.getEquivalentPattern());

		return copy;
	}

	@Override
	public double estimatedCardinality() {
		return inputCardinality();
	}

	@Override
	public double estimatedIOCost() {
		return childrenIOCost() * 10;
	}

	@Override
	public double processingOverhead(double inputTuples) {
		int nodeno = 1;
		for (TreePattern p : this.ps) {
			nodeno += p.getNodesNo();
		}

		return nodeno * estimatedIOCost() * 1000;
	}

}
