package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

public class PhyMemoryNest extends NIterator {
	/** Universal version identifier for the PhyMemoryNest class */
	private static final long serialVersionUID = -8390473322081267942L;

	private static final Logger log = Logger.getLogger(PhyMemoryNest.class);

	NIterator child;
	ArrayList<NTuple> subTupleBuffer;
	HashMap<NTuple, ArrayList<NTuple>> state;
	NTuple res;
	boolean over;
	boolean started;
	NRSMD childNRSMD;
	
	int[] groupKeyMask;
	NRSMD groupKeyNRSMD;
	
	int[] nestedKeyMask;
	NRSMD nestedNRSMD;
	
	NRSMD groupNRSMD;
	
	Iterator<NTuple> resKeyIterator;
	
	public PhyMemoryNest(NIterator child, List<Integer> groupByColumns, List<Integer> groupedColumns) throws TatooineExecutionException{
		super(child);
		this.child = child;
		//log.debug("Child metadata is: "+ child.nrsmd.toString());
		this.nrsmd = NRSMD.nestNRSMD(child.nrsmd, groupByColumns, groupedColumns);
		//log.debug("Nest nrsmd is: " + this.nrsmd.toString());
		this.childNRSMD = child.nrsmd;
		groupKeyMask = new int[groupByColumns.size()];
		for (int i = 0; i < groupByColumns.size(); i++){
			groupKeyMask[i]=groupByColumns.get(i).intValue();
		}
		this.groupKeyNRSMD = NRSMD.makeProjectRSMD(childNRSMD, groupKeyMask); 
		nestedKeyMask = new int[groupedColumns.size()];
		for (int i = 0; i < groupedColumns.size(); i++){
			nestedKeyMask[i]=groupedColumns.get(i).intValue();
		}
		this.nestedNRSMD = NRSMD.makeProjectRSMD(childNRSMD, nestedKeyMask);
		subTupleBuffer = new ArrayList<NTuple>();
		state = new HashMap<NTuple, ArrayList<NTuple>>();
		res = null;
		
		TupleMetadataType[] groupTypes = new TupleMetadataType[1];
		groupTypes[0]=TupleMetadataType.TUPLE_TYPE;
		NRSMD[] groupMDs = new NRSMD[1];
		groupMDs[0] = this.nrsmd.getNestedChild(this.nrsmd.colNo-1);
		groupNRSMD = new NRSMD(groupTypes, groupMDs);
				
		//log.debug("NRSMD of nested group is: " + groupNRSMD.toString());
		
		this.orderMaker = new OrderMarker();
		
		over = false;
		started = false;
	}
	@Override
	public void open() throws TatooineExecutionException {
		child.open();	
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (over){
			return false;
		}
		if (!started){
			//log.debug("Filling in group-by table");
			while (child.hasNext()){
				NTuple childTuple = child.next();
				//log.debug("Got tuple: " + childTuple.toString());
				NTuple childGroupByKey = NTuple.project(childTuple, this.groupKeyNRSMD, groupKeyMask);
				NTuple childContributionToGroup = NTuple.project(childTuple, this.nestedNRSMD, nestedKeyMask);
				ArrayList<NTuple> group = state.get(childGroupByKey);
				if (group == null){
					group = new ArrayList<NTuple>();
					state.put(childGroupByKey, group);
				}
				group.add(childContributionToGroup);
				//log.debug("Added " + childContributionToGroup.toString() + " on " + childGroupByKey.toString());
			}
			resKeyIterator = state.keySet().iterator();
			started = true;
		}
		// when we got here, the group structure is full, and the child is finished
		return resKeyIterator.hasNext();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		// resKeyIterator is positioned on one group key
		NTuple thisGroupKey = resKeyIterator.next();
		ArrayList<NTuple> thisGroup = state.get(thisGroupKey);
		
		NTuple justTheGroup = new NTuple(groupNRSMD);
		justTheGroup.addNestedField(thisGroup);
		
		NTuple res = NTuple.append(this.nrsmd, thisGroupKey, justTheGroup);
		
		return res;
	}

	@Override
	public void close() throws TatooineExecutionException {
		child.close();
		this.state = null;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	private String getOwnDetails(){
		StringBuffer sb = new StringBuffer();
		sb.append("GroupBy[");
		for (int i = 0; i < groupKeyMask.length; i ++){
			if (child.nrsmd.getColNames()[groupKeyMask[i]] != null){
				sb.append(child.nrsmd.getColNames()[groupKeyMask[i]]);
			}
			else{
				sb.append(groupKeyMask[i]);
			}
			if (i < groupKeyMask.length-1){
				sb.append(",");
			}
		}	
		sb.append("],[");
		for (int i = 0; i < nestedKeyMask.length; i ++){
			if (child.nrsmd.getColNames()[nestedKeyMask[i]] != null){
				sb.append(child.nrsmd.getColNames()[nestedKeyMask[i]]);
			}
			else{
				sb.append(nestedKeyMask[i]);
			}
			if (i < nestedKeyMask.length-1){
				sb.append(",");
			}
		}	
		sb.append("]");
		return new String(sb);
	}
	@Override
	public String getName() {
		return getOwnDetails();
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);
		return "\n" + spaceForIndent + this.getOwnDetails() + 
				"(" + child.getName(1 + depth) + "," + "\n" + tabs +"\n"  + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		// TODO Auto-generated method stub
		return 0;
	}

}
