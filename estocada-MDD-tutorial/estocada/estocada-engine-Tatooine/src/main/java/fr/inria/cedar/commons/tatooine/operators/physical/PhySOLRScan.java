package fr.inria.cedar.commons.tatooine.operators.physical;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Physical operator that accesses data from a SOLR collection preserving the metadata associated with such collection.
 * This is equivalent to scanning a collection or performing a selection (filtering) on it.
 * @author Oscar Mendoza
 */
public class PhySOLRScan extends SolrJSONQueryParameterisedOperator implements PhyScanOperator {

	private static final Logger log = Logger.getLogger(PhySOLRScan.class);

	/** Universal version identifier for the PhySOLRScan class */
	private static final long serialVersionUID = -8912360206565859874L;
	
	/* Constructor */
	public PhySOLRScan(String query, String fields, GSR ref, NRSMD nrsmd) throws TatooineExecutionException, SolrServerException, IOException, TatooineException {		
		super(query, fields, ref);
		if (!isScanOrFilter(fields)) {
			String msg = "Scan operators are meant for queries that preserve the metadata of the data source they're querying; for queries that do NOT preserve it, use Eval operators";
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		this.nrsmd = nrsmd;
	}
	
	/* Constructor */
	public PhySOLRScan(String query, String fields, GSR ref, ViewSchema schema) throws TatooineExecutionException, SolrServerException, IOException, TatooineException {	
		this(query, fields, ref, schema.getNRSMD());
	}

	@Override
	public String getName() {
		return String.format("PhySOLRScan(%s, %s)", queryStr, fieldStr);
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return String.format("\n%sPhySOLRScan(%s, %s)", spaceForIndent, queryStr, fieldStr);
	}
	
	@Override
	public NIterator copy() throws TatooineExecutionException {
		try {
			return new PhySOLRScan(queryStr, fieldStr, ref, nrsmd);
		} catch (Exception e) {
			log.error("Exception copying PhySOLRScan: ", e);
			return null;
		}
	}
	
	@Override
	public boolean isScanOrFilter(String fields) {
		return Strings.isNullOrEmpty(fields) ? true : false;
	}

}
