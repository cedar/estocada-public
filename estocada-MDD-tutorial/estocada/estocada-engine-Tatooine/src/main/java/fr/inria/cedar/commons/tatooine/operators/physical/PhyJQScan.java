package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Physical operator that queries JSON data sources using Apache Spark SQL.
 * @author Oscar Mendoza
 */
public class PhyJQScan extends SparkJSONQueryParameterisedOperator implements PhyScanOperator {

	/** Universal version identifier for the PhyJQScan class */
	private static final long serialVersionUID = 1054505052984849304L;
	
	private static final Logger log = Logger.getLogger(PhyJQScan.class);

	/* Constructor */
	public PhyJQScan(String query, GSR ref, NRSMD nrsmd) throws TatooineExecutionException {
		super(query, ref);
		if (!isScanOrFilter(query)) {
			log.error(MSG_USE_EVAL);
			throw new TatooineExecutionException(MSG_USE_EVAL);
		}
		this.nrsmd = nrsmd;
	}
	
	/* Constructor */
	public PhyJQScan(String query, GSR ref, ViewSchema schema) throws TatooineExecutionException {
		this(query, ref, schema.getNRSMD());
	}
	
	@Override
	public String getName() {
		return "PhySQLScan";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}
	
	@Override
	public boolean isScanOrFilter(String query) {
		// The Spark SQL queries we support are of the form:
		// SELECT [DISTINCT] * FROM [...]
		Pattern p = Pattern.compile("SELECT(\\s+DISTINCT)?\\s+\\*\\s+FROM");
		Matcher m = p.matcher(query);
		return m.find();
	}
	
	@Override
	public NIterator copy() throws TatooineExecutionException {
		try {
			return new PhyJQScan(queryStr, ref, nrsmd);
		} catch (Exception e) {
			log.error("Exception copying PhyJQScan: ", e);
			return null;
		}
	}
}