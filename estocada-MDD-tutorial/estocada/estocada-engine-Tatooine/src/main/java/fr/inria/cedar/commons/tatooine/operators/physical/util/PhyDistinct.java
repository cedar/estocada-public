package fr.inria.cedar.commons.tatooine.operators.physical.util;

import java.util.stream.IntStream;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.sort.MemorySort;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * distinct meta operator build using sort and uniq operator
 * 
 * @author Maxime Buron
 */
public class PhyDistinct extends PhyUniq {

	private static final long serialVersionUID = 1L;

	public PhyDistinct(NIterator child) throws TatooineExecutionException {
		super(new MemorySort(child,
                             IntStream.rangeClosed(0, child.nrsmd.colNo - 1).toArray()));
	}

	public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit((PhyUniq) this);
    }
}
