package fr.inria.cedar.commons.tatooine.predicates;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.PredicateType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * This class is meant for very simple predicate selections: atomic field = constant, or atomic field = atomic field.
 * The main difference with SimplePredicate.java, the class that it extends, is that the method isTrue is overridden, so
 * we don't check the types of the columns that we are comparing and we do the comparison based only on the values
 * contained by each field. The use of this kind of predicates should be activated when we use VJ patterns. This can be
 * done setting the property "annovip.useOnlyJoinOnValue" in estocada.conf to the right value.
 *
 * @author Jesus CAMACHO RODRIGUEZ
 * @author Konstantinos KARANASOS
 */
public class TypeCasterSimplePredicate extends SimplePredicate implements Serializable {
	private static final Logger log = Logger.getLogger(TypeCasterSimplePredicate.class);
	private static final long serialVersionUID = -4624001532376402625L;

	public TypeCasterSimplePredicate(String s, int k, PredicateType predCode) {
		super(s, k, predCode);
	}

	public TypeCasterSimplePredicate(String s, int k) {
		super(s, k);
	}

	public TypeCasterSimplePredicate(char[] c, int k) {
		super(c, k);
	}

	public TypeCasterSimplePredicate(char[] c, int k, PredicateType predCode) {
		super(c, k, predCode);
	}

	public TypeCasterSimplePredicate(int k1, int k2) {
		super(k1, k2);
	}

	public TypeCasterSimplePredicate(int k1, int k2, PredicateType predCode) {
		super(k1, k2, predCode);
	}

	public TypeCasterSimplePredicate(int[] k1, int k2) {
		super(k1, k2);
	}

	public TypeCasterSimplePredicate(int[] k1, int k2, PredicateType predCode) {
		super(k1, k2, predCode);
	}

	public TypeCasterSimplePredicate(int[] k1, String constValue) {
		super(k1, constValue);
	}

	public TypeCasterSimplePredicate(int[] k1, String constValue, PredicateType predCode) {
		super(k1, constValue, predCode);
	}

	public static TypeCasterSimplePredicate makePredicate(ArrayList<Integer> v1, ArrayList<Integer> v2,
			PredicateType predCode) {
		if (v1.size() == 1) {
			if (v2.size() == 1) {
				int n1 = v1.get(0).intValue();
				int n2 = v2.get(0).intValue();
				return new TypeCasterSimplePredicate(n1, n2, predCode);
			} else {
				log.error("Right hand address used in predicate cannot be a vector");
			}
		} else {
			int[] k1 = new int[v1.size()];
			for (int i = 0; i < v1.size(); i++) {
				k1[i] = v1.get(i).intValue();
			}
			if (v2.size() == 1) {
				int k2 = v2.get(0).intValue();
				return new TypeCasterSimplePredicate(k1, k2, predCode);
			} else {
				log.error("Right hand address used in predicate cannot be a vector");
			}
		}
		return null;
	}

	@Override
	public final boolean isTrue(NTuple t) throws TatooineExecutionException {
		assert (this.column >= 0) : "Cannot test the predicate ! Use GeneralizedSelect instead.";
		if (this.onString) {
			switch (this.predCode) {
			case PREDICATE_EQUAL:
				return equal(t.getStringField(this.column), this.theChars);
			case PREDICATE_NOT_EQUAL:
				return (!equal(t.getStringField(this.column), this.theChars));
			default:
				log.error("Predicate " + this.predCode.toString() + " not applicable to type String");
			}
		} else if (this.onJoin) {
			char[] thisColumn = null;
			char[] otherColumn = null;
			switch (t.nrsmd.types[this.column]) {
			case URI_TYPE:
				thisColumn = t.getUriField(this.column);
				break;
			case STRING_TYPE:
				thisColumn = t.getStringField(this.column);
				break;
			case UPDATE_ID:
			case UNIQUE_ID:
			case ORDERED_ID:
			case STRUCTURAL_ID:
				thisColumn = t.getIDField(this.column).toString().toCharArray();
				break;
			default:
				log.error("Type unhandled by SimplePredicates " + t.nrsmd.types[this.column].toString());
			}
			switch (t.nrsmd.types[this.column]) {
			case URI_TYPE:
				otherColumn = t.getUriField(this.otherColumn);
				break;
			case STRING_TYPE:
				otherColumn = t.getStringField(this.otherColumn);
				break;
			case UPDATE_ID:
			case UNIQUE_ID:
			case ORDERED_ID:
			case STRUCTURAL_ID:
				otherColumn = t.getIDField(this.otherColumn).toString().toCharArray();
				break;
			default:
				log.error("Type unhandled by SimplePredicates " + t.nrsmd.types[this.otherColumn].toString());
			}

			switch (this.predCode) {
			case PREDICATE_EQUAL:
				return equal(thisColumn, otherColumn);
			case PREDICATE_NOT_EQUAL:
				return !equal(thisColumn, otherColumn);
			default:
				log.error("Predicate " + predCode.toString() + " not handled in String join");
			}
		}
		return false;
	}

	@Override
	public Object clone() {
		if (onJoin) {
			return new TypeCasterSimplePredicate(column, otherColumn);
		} else if (onString) {
			return new TypeCasterSimplePredicate(theChars, column);
		} else {
			return null;
		}
	}

}
