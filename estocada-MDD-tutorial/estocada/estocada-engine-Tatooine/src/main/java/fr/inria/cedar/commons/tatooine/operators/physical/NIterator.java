package fr.inria.cedar.commons.tatooine.operators.physical;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.apache.commons.lang.NotImplementedException;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.PhysOpID;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyMemoryHashJoin;
import fr.inria.cedar.commons.tatooine.operators.physical.join.StructuralJoin;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Abstract class that every physical operator should extend.
 *
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 * @author Aditya SOMANI
 */
public abstract class NIterator implements AutoCloseable, Serializable {
	private static final Logger log = Logger.getLogger(NIterator.class);
	public static boolean timeout = false;

	/** Universal version identifier for the NIterator class */
	private static final long serialVersionUID = -2931824780181255334L;

	/* Constant */
	// Here we store if the physical plan monitor is active or not.
	private static final boolean PHYSICAL_MONITOR_ACTIVE = Boolean.parseBoolean(Parameters.getProperty("physPlanMon"));
	private static final String DOT_OUTPUT_FOLDER = Parameters.getProperty("DOToutputFolder");

	/** The list of children, to enable tree navigation */
	public List<NIterator> children;

	/** Global counter to determine whether we create too many objects */
	public static long totalOperatorNo = 0;

	/** Here we keep the number assigned to this operator */
	private PhysOpID operatorID;

	public NRSMD nrsmd;

	/**
	 * The columns by which the output of this operator is ordered. The OrderMarker comprises: -- a list of direct
	 * order-by columns, which are top-level columns -- a set of nested OrderMarkers which express how the tuples of a
	 * nested child are ordered. There is no way of comparing tuples based on the values of their nested children ! This
	 * seems reasonable for now. Simple value order is understood as follows: any two non-null values are in order;
	 * nulls come first. (unless we see this causes some problem...)
	 */
	public OrderMarker orderMaker;

	/** Number of tuples that were passed to the above operator */
	protected long numberOfTuples;

	protected static final int PRINTING_INDENTATION_TABS = 2;

	/** Constructs an NIterator with an arbitrary number of children */
	public NIterator(List<NIterator> children) {
		// Creating an operator ID for this operator and reporting it
		this.operatorID = new PhysOpID();
		numberOfTuples = 0;
		this.children = children;
	}

	/** Constructs a leaf NIterator by calling {@link NIterator#NIterator(List)} */
	public NIterator() {
		this(new ArrayList<NIterator>(0));
	}

	// Getters and setters
	public OrderMarker getOrderMaker() {
		return orderMaker;
	}

	public void setOrderMaker(OrderMarker orderMaker) {
		this.orderMaker = orderMaker;
	}

	public PhysOpID getOperatorID() {
		return operatorID;
	}

	public void setOperatorID(PhysOpID operatorID) {
		this.operatorID = operatorID;
	}

	public List<NIterator> getChildren() {
		return children;
	}

	public void setBindings(NTuple t) throws TatooineExecutionException {
		for (NIterator child : children) {
			child.setBindings(t);
		}
	}

	/**
	 * Performs a DFS traversal of the tree under this operator: returns the list of operators under this operator.
	 *
	 * @return a list of LogicalOperators
	 * @author Asterios KATSIFODIMOS
	 */
	public List<NIterator> getDescendants() {
		ArrayList<NIterator> operators = new ArrayList<NIterator>();
		this.getDescendantsRec(operators);
		return operators;
	}

	/**
	 * Adds all children to an ArrayList of NIterators
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	private void getDescendantsRec(ArrayList<NIterator> descs) {
		if (children != null) {
			for (NIterator child : this.children) {
				child.getDescendantsRec(descs);
				descs.add(child);
			}
		}
	}

	/** Constructs a unary NIterator by calling {@link NIterator#NIterator(List)} */
	public NIterator(NIterator child) {
		this(Arrays.asList(child));
	}

	/** Constructs a binary NIterator by calling {@link NIterator#NIterator(List)} */
	public NIterator(NIterator leftChild, NIterator rightChild) {
		this(Arrays.asList(leftChild, rightChild));
	}

	/** Constructs an <i>n</i>-ary NIterator from an array of children by calling {@link NIterator#NIterator(List)} */
	public NIterator(NIterator[] children) {
		this(Arrays.asList(children));
	}

	/** Initialize the iterator */
	public abstract void open() throws TatooineExecutionException;

	/** Resets the iterator without having to re-open it */
	public void initialize() throws TatooineExecutionException {
		;
	}

	/** Returns true if there is at least some top-level tuple inside */
	public abstract boolean hasNext() throws TatooineExecutionException;

	/** Returns the next NTuple available */
	public abstract NTuple next() throws TatooineExecutionException;

	/** Closes the iterator */
	public abstract void close() throws TatooineExecutionException;

	/**
	 * TODO: Do we need this at the level of the interface ?
	 *
	 * @param i the column at which there is a nested field
	 * @return an iterator for that field. The iterator has still to be opened and closed.
	 */
	public abstract NIterator getNestedIterator(int i) throws TatooineExecutionException;

	/** Operator name, on one line */
	public abstract String getName();

	/** Operator name on several lines, with indentation reflecting operator nesting */
	public abstract String getName(int depth);
	
	/**
	 * Operator name with details, as presented in the execution plan.
	 * This shouldn't include details on the children of this op.
	 * When unavailable getName is automatically used instead.
	 */
	public String getDetailedName(){
		return getName();
	}
	

	/** Displays the NIterator */
	public void display() throws TatooineExecutionException {
		long t1 = System.currentTimeMillis();
		int n = 0;
		log.info(this.getName() + " " + this.getOrderMaker().toString());
		log.info("=============");
		this.open();
		assert (this.nrsmd != null) : "Null NRSMD";
		this.nrsmd.display();
		while (this.hasNext()) {
			NTuple nt = this.next();
			nt.display(this.getClass().getName());
			n++;
		}

		log.info(n + " tuples ordered by " + this.getOrderMaker().toString() + "\n");
		this.close();

		long t2 = System.currentTimeMillis();

		log.info("Time:" + (t2 - t1));
	}

	/** Displays the NIterator */
	public void display(StringBuffer sb) {
		long t1 = System.currentTimeMillis();
		try {
			int n = 0;
			sb.append("\n" + this.getName() + "\n");
			for (int i = 0; i < this.getName().length(); i++) {
				sb.append("=");
			}
			sb.append("\n");
			this.open();

			sb.append(this.nrsmd.toString());
			sb.append("\n");
			while (this.hasNext()) {
				NTuple nt = this.next();
				sb.append("   ");
				sb.append(nt.toString());
				sb.append("\n");
				n++;
			}

			sb.append(n + " tuples ordered by " + this.getOrderMaker().toString() + "\n");
			this.close();
		} catch (Exception e) {
			log.error("Exception: ", e);
		}

		long t2 = System.currentTimeMillis();

		log.info("times =" + (t2 - t1));
		log.info(this.nrsmd);
	}

	public abstract NIterator copy() throws TatooineExecutionException;

	public void bottomUpDisplay() throws TatooineExecutionException {
		for (NIterator child : this.children) {
			child.bottomUpDisplay();
		}
		this.display();
	}

	public int sortCount() throws TatooineExecutionException {
		int res = 0;
		if (this instanceof PhyMemorySort) {
			PhyMemorySort ms = (PhyMemorySort) this;
			res = 1 + ms.child.sortCount();
			ms.close();
		}
		if (this instanceof PhyProjection) {
			PhyProjection sp = (PhyProjection) this;
			res = sp.child.sortCount();
			sp.close();
		}
		if (this instanceof PhySelection) {
			PhySelection sp = (PhySelection) this;
			res = sp.child.sortCount();
			sp.close();
		}
		if (this instanceof StructuralJoin) {
			StructuralJoin sj = (StructuralJoin) this;
			res = sj.leftChild.sortCount() + sj.rightChild.sortCount();
			sj.close();
		}
		if (this instanceof PhyXMLViewScanBDB) {
			res = 0;
		}
		if (this instanceof PhyMemoryHashJoin) {
			PhyMemoryHashJoin mhj = (PhyMemoryHashJoin) this;
			res = mhj.left.sortCount() + mhj.right.sortCount();
			mhj.close();
		}
		return res;
	}

	/**
	 * Method that fills the string buffer with the code that represents the physical plans. Returns the ID of the node
	 * that has been filled.
	 */
	public abstract int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo);

	/** Method that draws an image depicting the physical plan rooted by this physical operator */
	public void draw() {
		String fileName = createDot(createDotString());
		drawImageFromDot(fileName);
	}

	/** Returns the content of the DOT file: an image which represents the physical plan rooted by this operator */
	public String createDotString() {
		StringBuffer sb = new StringBuffer();
		Calendar cal = new GregorianCalendar();
		sb.append("digraph  g{ graph[label = \"" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":"
				+ cal.get(Calendar.SECOND) + "." + cal.get(Calendar.MILLISECOND) + "\"]\n");
		sb.append("node [shape=rectangle, color=black, fontcolor=black, style=bold] ");
		sb.append("edge [color=black] ");
		recursiveDotString(sb, -1, 0);
        sb.append("}\n");

		return sb.toString();
	}

	/**
	 * This method receives the contents of a DOT file and creates one with these data on the disk.
	 *
	 * @param dotStr The contents of the file.
	 * @return The filename of the .dot file omitting the termination.
	 */
	public static String createDot(String dotStr) {
		Calendar cal = new GregorianCalendar();
		String planFileName = "phys-" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":"
				+ cal.get(Calendar.SECOND) + ":" + cal.get(Calendar.MILLISECOND);
		try {
			String fileNameDot = new String(planFileName + ".dot");
			FileWriter file = new FileWriter(fileNameDot);

			file.write(dotStr);
			file.close();
		} catch (IOException e) {
			log.error("IOException: ", e);
		}

		return planFileName;
	}

	/**
	 * This method creates an image using a DOT file.
	 *
	 * @param givenFileName The filename of the dot file without the termination.
	 */
	public static void drawImageFromDot(String givenFileName) {
		try {
			String fileNameDot = new String(givenFileName + ".dot");
			String fileNamePNG = new String(DOT_OUTPUT_FOLDER + File.separator + givenFileName + ".png");

			Runtime r = Runtime.getRuntime();
			log.info("I am drawing monitoring image " + fileNamePNG);
			String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePNG);
			Process p = r.exec(com);
			p.waitFor();
			Process p2 = r.exec("rm " + fileNameDot + "\n");
			p2.waitFor();
		} catch (IOException e) {
			log.error("Error: ", e);
		} catch (InterruptedException e) {
			log.error("Error: ", e);
		}
	}

	/**
	 * Presents the pictorial representation of the physical plan.
	 *
	 * @param: The physical plan is drawn in the PNG file with the name filename.... This method is very helpful in
	 *         debugging as one can identify the plan that has been drawn to this file.
	 */
	public void draw(String FileName) {
		StringBuffer sb = new StringBuffer();
		sb.append("digraph  g{\n");
		sb.append("node [shape=rectangle, color=blue2, fontcolor=blue4]");
		sb.append("edge [color=blue4]");
		recursiveDotString(sb, -1, 0);

		try {

			String pathName = "";
			int lastSlash = FileName.lastIndexOf(File.separator);

			if (lastSlash > 0) {
				pathName = FileName.substring(0, lastSlash);
				File dir = new File(pathName);
				dir.mkdirs();
			}

			int startIndex = 0;
			if (FileName.contains(File.separator)) {
				startIndex = FileName.lastIndexOf(File.separator) + 1;
			}
			int endIndex = FileName.length();
			if (FileName.contains(".")) {
				endIndex = FileName.lastIndexOf(".");
			}

			String fileNameDot = new String(FileName + ".dot");
			String fileNamePNG = new String(DOT_OUTPUT_FOLDER + File.separator
					+ FileName.substring(startIndex, endIndex) + "-PhyPlan.png");
			FileWriter file = new FileWriter(fileNameDot);

			file.write(new String(sb));
			file.write("}\n");
			file.close();

			Runtime r = Runtime.getRuntime();
			String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePNG);
			Process p = r.exec(com);
			p.waitFor();
			Process p2 = r.exec("rm " + fileNameDot + "\n");
			p2.waitFor();
		} catch (Exception e) {
			log.error("Error: ", e);
		}
	}

	public void increaseOpNo() {
		totalOperatorNo++;
	}

	/**
	 * Informing the PhysicalPlanMonitor for the operator status change in non-blocking mode.
	 *
	 * @param operStatus A code corresponding to the operator status.
	 * @param operID The id of the physical operator.
	 */
	public void informPlysPlanMonitorOperStatus(int operStatus, PhysOpID operID) {
		if (PHYSICAL_MONITOR_ACTIVE) {
			new StatusInformer(operStatus, operID);
		}
	}

	public void informPlysPlanMonitorTuplesPassed(long tupleNo, PhysOpID operID) {
		if (PHYSICAL_MONITOR_ACTIVE) {
			new TupleNoInformer(tupleNo, operID);
		}
	}

	/**
	 * Returns us the color that an operator of a physical plan should have when we draw the image depicting it. The
	 * color depends on the status of the operator (the one that has reported to the PhysicalPlanMonitor).
	 *
	 * @return The color of the operator.
	 */
	public String getColoring() {
		return "yellow";
	}

	/**
	 * Performs a BFS traversal of the tree under this operator and returns the list of operators under this operator.
	 *
	 * @return a list of LogicalOperators in a BFS manner
	 * @author Karan AGGARWAL
	 */
	public List<NIterator> getDescendantsBFS() {
		ArrayList<NIterator> nodes = new ArrayList<NIterator>();
		Queue<NIterator> BFSQueue = new LinkedList<NIterator>();
		BFSQueue.add(this);

		while (!BFSQueue.isEmpty()) {
			NIterator plan = BFSQueue.poll();
			nodes.add(plan);
			if (plan.children != null) {
				for (NIterator node : plan.children) {
					BFSQueue.add(node);
				}
			}
		}
		return nodes;
	}

	/**
	 * Returns a string with as many tabs as indicated.
	 *
	 * @author Karan AGGARWAL
	 */
	protected String getTabs(int space) {
		String spaces = "";
		for (int i = 0; i < space; i++) {
			spaces += "\t";
		}
		return spaces;
	}

	/**
	 * This thread is responsible to notify the PhysicalPlanMonitor for the change of the status of the operator.
	 *
	 * @author Spyros ZOUPANOS
	 */
	class StatusInformer extends Thread {
		int operStatus;
		PhysOpID operID;

		public StatusInformer(int givenOperStatus, PhysOpID givenOperID) {
			this.setName("StatusInformer");
			operStatus = givenOperStatus;
			operID = givenOperID;
			start();
		}
	}

	/**
	 * This thread is responsible to notify the PhysicalPlanMonitor for the change of the status of the operator.
	 *
	 * @author Spyros ZOUPANOS
	 */
	class TupleNoInformer extends Thread {
		long tupleNo;
		PhysOpID operID;

		public TupleNoInformer(long givenTupleNo, PhysOpID givenOperID) {
			this.setName("TupleNoInformer");
			tupleNo = givenTupleNo;
			operID = givenOperID;
			start();
		}
	}

	/**
	 * Provides additional details on the iterator, to be shown on the evaluation plan
	 */
	public Map<String,String> getDetails() {
		return new HashMap<>();
	}

    public void accept(Visitor visitor) throws TatooineExecutionException {
        throw new NotImplementedException("not implemented accept method for type " + this.getClass());
    }
}
