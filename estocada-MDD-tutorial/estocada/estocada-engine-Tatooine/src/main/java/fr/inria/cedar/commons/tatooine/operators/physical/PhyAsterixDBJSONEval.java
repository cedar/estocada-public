package fr.inria.cedar.commons.tatooine.operators.physical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * Physical operator that queries JSON data source using Apache AsterixDB.
 * 
 * @author ranaalotaibi
 */
public class PhyAsterixDBJSONEval extends AsterixDBJSONQueryParameterizedOperator implements PhyEvalOperator {

    /** Universal version identifier PhyAsterixDBEval class **/
    private static final long serialVersionUID = 4363573029200631096L;

    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(PhyAsterixDBJSONEval.class);

    /** Constructor **/
    public PhyAsterixDBJSONEval(final String query, final GSR ref, final NRSMD nrsmd)
            throws TatooineExecutionException {
        super(query, ref);
        this.nrsmd = nrsmd;
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        try {
            return new PhyAsterixDBJSONEval(queryStr, gsr, nrsmd);
        } catch (TatooineExecutionException exception) {
            throw new TatooineExecutionException(exception);

        }
    }

    @Override
    public String getName() {
        return "PhyAsterixDBJSONEval";
    }

    @Override
    public String getName(int depth) {
        return getName();
    }

    @Override
    public NRSMD inferMetadata(NRSMD inputMD, String queryParams) throws TatooineExecutionException {
        throw new TatooineExecutionException("Unsupported");
    }
}
