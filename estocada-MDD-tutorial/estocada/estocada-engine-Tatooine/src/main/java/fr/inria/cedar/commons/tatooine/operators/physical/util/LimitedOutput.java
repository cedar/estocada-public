package fr.inria.cedar.commons.tatooine.operators.physical.util;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * Forces an iterator to output at most a fixed number of tuples
 * 
 * @author Raphael BONAQUE
 */
public class LimitedOutput extends NIterator{
	
	private static final long serialVersionUID = 3312927307171007263L;
	
	private NIterator child;
	private int maxNumberOfOutputs;
	private boolean childIsClose = false;

	public LimitedOutput(NIterator child, int maxNumberOfOutputs) {
		super(child);
		this.child = child;
		this.nrsmd = child.nrsmd;
		this.maxNumberOfOutputs = maxNumberOfOutputs;
	}
	
	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (numberOfTuples >= maxNumberOfOutputs) {
			if (!childIsClose) {
				childIsClose = true;
				child.close();
			};
			return false;
		} else {
			return child.hasNext();
		}
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		if (numberOfTuples >= maxNumberOfOutputs) return null;
		NTuple output = child.next();
		if (output != null) numberOfTuples++;
		return output;
	}
	
	@Override
	public void open() throws TatooineExecutionException {
		child.open();
	}
	
	@Override
	public void close() throws TatooineExecutionException {
		if (!childIsClose) {
			childIsClose = true;
			child.close();
		}
	}

	@Override
	public String getName() {
		return "Limited output[" + Integer.valueOf(maxNumberOfOutputs) + "]";
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return (
			"\n" + spaceForIndent + "Limited output(" +
			child.getName(depth+1) + 
			"\n[" + Integer.valueOf(maxNumberOfOutputs) + "]"
		);
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new LimitedOutput(child.copy(), maxNumberOfOutputs);
	}


	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return null;
	}
	
	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo,int firstAvailableNo) {
		return 0;
	}

}
