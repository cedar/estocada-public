package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.TreeMap;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.DBSourceLayer;
import fr.inria.cedar.commons.tatooine.storage.database.SOLRSourceLayer;

/**
 * Physical operator that queries a collection of a running SOLR instance.
 * @author Oscar Mendoza
 */
public abstract class SolrJSONQueryParameterisedOperator extends QueryParameterisedOperator {

	/** Universal version identifier for the PhySOLROperator class */
	private static final long serialVersionUID = 3068801040773742263L;

	private static final Logger log = Logger.getLogger(SolrJSONQueryParameterisedOperator.class);

	/* Query parameters */
	protected GSR ref;
	protected NTuple params;
	protected String queryStr = "";
	protected String fieldStr = "";
	protected String paramStr = "";
	public static final String PLACEHOLDER = "%s";
	
	// String needed to escape queries with whitespaces 
	protected static final String REPLACEMENT_BLANK = "\\\\\\\\ ";
	
	// Interface that handles DB calls
	private DBSourceLayer source;	
	
	// In-memory queue of NTuples:
	// It is re-filled with "resultsPerQuery" tuples when empty and the user calls "hasNext()"
	// It is polled when the user calls "next()"
	private Queue<NTuple> tuples = null;

	// MD mismatch error message
	private static final String mismatchNRSMDErrorMsg = "The value '%s' does not match the expected input NRSMD %s";

	/* Constructor */
	public SolrJSONQueryParameterisedOperator(String query, String fields, GSR ref) throws TatooineExecutionException, TatooineException {
		queryStr = query;
		fieldStr = fields;
		source = new SOLRSourceLayer(ref);
		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		source.connect();
		paramStr = "";
		tuples = new ArrayDeque<NTuple>();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	/** Checks the query has been parameterised if needed */
	private String resolveQuery(String query) throws TatooineExecutionException {
		// Check for placeholders in parameterised queries
		if (queryStr.contains(PLACEHOLDER) && ((paramStr == null) || paramStr.isEmpty())) {
			String message = "Method hasNext() called with query that contains placeholders and hasn't been parameterised!";
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		return Strings.isNullOrEmpty(paramStr) ? queryStr : paramStr;
	}
	
	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// Checking for timeout
		if (timeout) {
			return false;
		}
		
		if (tuples.isEmpty()) {
			// It's time to query SOLR to fetch more results			
			String[] parameters = new String[] {resolveQuery(queryStr), fieldStr};
			Object batch = source.getBatchRecords(parameters);
			if (batch == null) {
				return false;
			} else {
				// Transforming the SOLR response into a list of NTuple objects
				tuples = transformResponse((QueryResponse) batch);
				return CollectionUtils.isEmpty(tuples) ? false : true;
			}
		} else {
			return true;
		}
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		numberOfTuples++;
		NTuple next = tuples.poll();
		return next;
	}
	
	/** Checks if a SOLR field is reserved or not */
	private boolean isReservedField(String fieldName) {
		// Names with both leading and trailing underscores are reserved
		return (fieldName.startsWith("_") | fieldName.endsWith("_")) ? true : false;
	}

	/** Transforms a SOLR QueryResponse object into a queue of NTuple objects */
	private Queue<NTuple> transformResponse(QueryResponse response) throws TatooineExecutionException {
		Queue<NTuple> records = new ArrayDeque<NTuple>();
		if (response == null || response.getResults() == null || response.getResults().size() == 0) {
			return records;
		}		
		SolrDocumentList results = response.getResults();
		log.info(String.format("SOLR: Got %s hits in %s ms", results.size(), response.getElapsedTime()));

		// Iterating through results
		for (SolrDocument document : results) {			
			Iterator<String> it = document.getFieldNames().iterator();
			TreeMap<String, Object> tree = new TreeMap<String, Object>();
			while (it.hasNext()) {
				String name = it.next();
				if (!isReservedField(name)) {
					tree.put(name, document.getFieldValue(name));
				}
			}

			int counter = 0;
			List<Object> fields = new ArrayList<Object>();
			Map<String, Integer> colNameToIndexMap = new HashMap<String, Integer>();
			for (Entry<String, Object> field : tree.entrySet()) {
				fields.add(field.getValue());
				colNameToIndexMap.put(field.getKey(), counter);
				counter++;
			}

			// Loop over the view's NRSMD (columns)
			NTuple tuple = new NTuple(nrsmd);
			for (int index = 0; index < nrsmd.types.length; index++) {
				String name = nrsmd.colNames[index];
				Integer colNo = colNameToIndexMap.get(name);
				if (colNo == null) {
					String message = String.format("The SOLR document has no [%s] field: %s", name, document);
					log.warn(message);
				} else {
					tuple = addFieldToNTuple(tuple, nrsmd.types[index], nrsmd, index, fields.get(colNo));
				}
			}

			records.add(tuple);
		}

		return records;
	}
	
	/** Gradually fills in an NTuple object from a SOLR response */
	@SuppressWarnings("unchecked")
	private NTuple addFieldToNTuple(NTuple tuple, TupleMetadataType type, NRSMD nrsmd, int index, Object obj)
			throws TatooineExecutionException {
		String name = obj.getClass().getSimpleName();
		switch (type) {
		case STRING_TYPE:
			if (name.equals("ArrayList")) {
				String message = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(message);
				throw new TatooineExecutionException(message);
			}
			tuple.addString(String.valueOf(obj));
			break;
		case INTEGER_TYPE:
			if (!name.equals("Integer") && !name.equals("Long")) {
				String message = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(message);
				throw new TatooineExecutionException(message);
			}
			tuple.addInteger(Integer.parseInt(String.valueOf(obj)));
			break;

		/* Nesting */
		case TUPLE_TYPE:
			if (!name.equals("ArrayList")) {
				String message = String.format(mismatchNRSMDErrorMsg, name, nrsmd);
				log.error(message);
				throw new TatooineExecutionException(message);
			}
			
			List<NTuple> children = new ArrayList<NTuple>();
			ArrayList<Object> array = (ArrayList<Object>) obj;
			
			NRSMD nrsmdChild = nrsmd.getNestedChild(index);
			String childName = nrsmdChild.colNames[0];
			TupleMetadataType childType = nrsmdChild.types[0];
			
			int counter = 1;
			switch (childType) {
				case STRING_TYPE: {
					for (Object elem : array) {
						NTuple child = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.STRING_TYPE }, new String[] { childName + counter }, new NRSMD[0]));
						child.addString(String.valueOf(elem));
						children.add(child);
						counter++;
					}
					break;
				}
				case INTEGER_TYPE: {
					for (Object elem : array) {
						NTuple child = new NTuple(new NRSMD(1, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE }, new String[] { childName + counter }, new NRSMD[0]));
						child.addInteger(Integer.parseInt(String.valueOf(elem)));
						children.add(child);
						counter++;
					}
					break;
				}
				default: {
					String message = String.format("The PhySOLROperator currently supports only simply-nested tuples");
					log.error(message);
					throw new UnsupportedOperationException(message);
				}
			}
			tuple.addNestedField(children);
			break;
		default:
			String message = String.format("The PhySOLROperator only handles string, integer, and nested types");
			log.error(message);
			throw new TatooineExecutionException(message);
		}

		return tuple;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public Map<String, String> getDetails() {
		Map<String, String> details = new HashMap<String, String>();
		details.put("query", queryStr);
		details.put("field", fieldStr);
		return details;
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		this.params = params;
		paramStr = super.updateParameters(params, queryStr, PLACEHOLDER, REPLACEMENT_BLANK);
		try {
			source.reset();
		} catch (TatooineExecutionException e) {
			String message = String.format("Error setting parameters: %s", params);
			log.error(message);
			throw new IllegalArgumentException(message);
		}
	}	
	
	/* Getters and setters */
	public String getQuery() {
		return queryStr;
	}
	
	public void setQuery(String query) {
		queryStr = query;
	}
	
	public String getField() {
		return fieldStr;
	}
	
	public void setField(String field) {
		fieldStr = field;
	}
	
	public NRSMD getNRSMD() {
		return nrsmd;
	}
	
	public void setNRSMD(NRSMD nrsmd) {
		this.nrsmd = nrsmd;
	}
	
	public DBSourceLayer getDBSourceLayer() {
		return source;
	}
	
	public void setDBSourceLayer(DBSourceLayer source) {
		this.source = source; 
	}
}
