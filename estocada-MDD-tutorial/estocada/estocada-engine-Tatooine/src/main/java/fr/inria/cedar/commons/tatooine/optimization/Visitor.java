package fr.inria.cedar.commons.tatooine.optimization;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccessEvalOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.FunctionCallBindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyArrayIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyJENAEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyProduct;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyProjection;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySelection;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyUnion;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyMemoryHashJoin;
import fr.inria.cedar.commons.tatooine.operators.physical.sort.MemorySort;
import fr.inria.cedar.commons.tatooine.operators.physical.util.PhyHashDistinct;
import fr.inria.cedar.commons.tatooine.operators.physical.util.PhyUniq;

public interface Visitor {

    void visit(PhyUnion op) throws TatooineExecutionException;

    void visit(PhySQLEval phySQLEval) throws TatooineExecutionException;

    void visit(PhyMemoryHashJoin phyMemoryHashJoin) throws TatooineExecutionException;

    void visit(PhySelection phySelection) throws TatooineExecutionException;

    void visit(PhyProduct phyProduct) throws TatooineExecutionException;

    void visit(PhyProjection phyProjection) throws TatooineExecutionException;

    void visit(FunctionCallBindAccess functionCallBindAccess) throws TatooineExecutionException;

    void visit(PhyArrayIterator phyArrayIterator) throws TatooineExecutionException;

    void visit(BindAccessEvalOperator bindAccessEvalOperator) throws TatooineExecutionException;

    void visit(PhyJENAEval phyJENAEval) throws TatooineExecutionException;

    void visit(PhyUniq phyUniq) throws TatooineExecutionException;

    void visit(MemorySort memorySort) throws TatooineExecutionException;

    void visit(PhyHashDistinct phyHashDistinct) throws TatooineExecutionException;

}
