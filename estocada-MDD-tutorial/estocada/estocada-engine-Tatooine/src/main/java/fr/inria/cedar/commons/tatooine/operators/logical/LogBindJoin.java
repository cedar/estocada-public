package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.predicates.Predicate;

/**
 * Logical operator for the executing joins with bindings.
 * @author Stamatis ZAMPETAKIS
 */
public class LogBindJoin extends LogJoin {

	public LogBindJoin(LogOperator left, LogOperator right, Predicate pred) {
		super(left, right, pred);
		this.setOwnName("LogBindJoin");
	}

	/** Makes a deep copy of the current operator */
	@Override
	public LogJoin deepCopy() {
		LogBindJoin joinCopy = null;
		joinCopy = new LogBindJoin(this.getLeft().deepCopy(), this.getRight().deepCopy(), (Predicate) this
				.getPredicate().clone());
		joinCopy.setOwnName(this.getOwnName());
		joinCopy.setName(this.getName());
		return joinCopy;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object myClone = null;
		try {
			myClone = new LogBindJoin((LogOperator) getLeft().clone(), (LogOperator) getRight().clone(), getPredicate());
		} catch (Exception e) {
			// TODO Handle StackTrace Differently
			e.printStackTrace();
		}
		return myClone;
	}
}
