package fr.inria.cedar.commons.tatooine.loader.multidoc;

import fr.inria.cedar.commons.tatooine.storage.database.DatabaseHolder;

/** Enum that represents all currently available implementations of {@link DatabaseHolder} */
public enum EngineSystem {
	BDB, POSTGRES, FS, HIVE, SOLR, JENA, REDIS, SPARK, BASEX;
}
