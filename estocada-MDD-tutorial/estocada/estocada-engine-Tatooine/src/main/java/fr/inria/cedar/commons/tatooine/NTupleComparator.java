package fr.inria.cedar.commons.tatooine;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.log4j.Logger;

import com.sleepycat.je.DatabaseEntry;

import fr.inria.cedar.commons.tatooine.loader.NTupleBinding;

/**
 * Comparator for {@link NTuple} objects.
 *
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 */
public class NTupleComparator implements Comparator<byte[]>, Serializable {
	private static final Logger log = Logger.getLogger(NTupleComparator.class);
	private static final long serialVersionUID = 6652469003671139285L;
	NTupleBinding ntb = new NTupleBinding();

	@Override
	public int compare(byte[] o1, byte[] o2) {
		DatabaseEntry e1 = new DatabaseEntry(o1);
		DatabaseEntry e2 = new DatabaseEntry(o2);
		
		NTuple t1 = ntb.entryToObject(e1);
		NTuple t2 = ntb.entryToObject(e2);
		int n = 0;
		try {
			n = t1.compareTo(t2);
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return n;
	}

}