package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.utilities.NTupleUtils;

/**
 * Physical operator that represents a GROUP BY operation over a set of tuples.
 * @author Oscar Mendoza
 */
public class PhyGroupBy extends NIterator {

	private static final Logger log = Logger.getLogger(PhyGroupBy.class);

	/** Universal version identifier for the PhyGroupBy class */
	private static final long serialVersionUID = 3699351514575271384L;

	int cursorId = -1;
	NTuple tuple = null;
	private NIterator child;
	private Aggregator aggregator = null;

	// Grouping attributes:
	// At least one needs to be specified and at most the total number of columns of the tuples we're working with
	private List<String> columns;
	
	private List<NTuple> groupedTuples = new ArrayList<NTuple>();

	/** Constructor */
	public PhyGroupBy(NIterator child, List<String> columns) throws TatooineExecutionException {
		super(child);
		if (columns == null || columns.isEmpty()) {
			String message = "No grouping attributes were specified";
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		this.child = child;
		this.columns = columns;		
		this.groupedTuples = new ArrayList<NTuple>();
	}
	
	/** Constructor */
	public PhyGroupBy(NIterator child, List<String> columns, Aggregator aggregator) throws TatooineExecutionException {	
		this(child, columns);
		this.aggregator = aggregator;
	}
	
	/**
	 * Adds a new field to a NTupleUtils object used as support to create grouped tuples
	 * @param result - NTupleUtils object we're filling in with fields from an NTuple
	 * @param current - NTuple object whose fields we want "result" to store
	 * @param colName - Name of the column in "current" we want "result" to store
	 * @param colNo - Number of the column in "current" we want "result" to store
	 * @return
	 */
	private NTupleUtils addField(NTupleUtils result, NTuple current,  String colName, int colNo) {
		TupleMetadataType type = current.nrsmd.types[colNo];		
		Object obj = current.getValue(colName);
		result.addName(colName);
		
		switch (type) {
			case INTEGER_TYPE:				
				result.addType(TupleMetadataType.INTEGER_TYPE);
				result.addInt((int) obj);				
				break;
			case STRING_TYPE:
				result.addType(TupleMetadataType.STRING_TYPE);
				result.addString(new String((char[]) obj));
				break;
			case TUPLE_TYPE:
				@SuppressWarnings("unchecked")
				ArrayList<NTuple> tuples = (ArrayList<NTuple>) obj;
				for (NTuple tuple : tuples) {
					result.addNestedField(tuple);
					result.addType(TupleMetadataType.TUPLE_TYPE);
					result.addChild(tuple.nrsmd);
				}
				break;
			default:
				throw new UnsupportedOperationException("Not implemented");
		}
		return result;
	}
	
	/** Updates the accumulated aggregation value appropriately according to the type of aggregation specified as input */
	private float updateAccumulatedValue(float accum, int value, int pos, int size) {
		switch (aggregator.getFunction()) {
			case COUNT:
				return (accum + 1);
			case SUM:
				return (accum += value);
			case AVG:
				return (accum += (value / (float) size));
			case MAX:				
				return ((pos == 0) || (value > accum)) ? value : accum;				
			case MIN:
				return ((pos == 0) || (value < accum)) ? value : accum;
			default:
				return value;
		}
	}

	/**
	 * Groups NTuples by one or more grouping attributes
	 * Note: Whenever we GROUP_BY k attributes [a1, a2, ..., ak], the result will have as attributes [a1, a2, ..., ak] (all grouping attributes)
	 * plus 1 attribute of type TUPLE that will contain all attributes of the input except for [a1, a2, ..., ak] (all grouped attributes)
	 * @return
	 * @throws TatooineExecutionException
	 */
	private void aggregateTuples() throws TatooineExecutionException {
		// This map groups tuples according to input grouping attributes:
		// The NTupleUtils object used as a key represents a set of unique values of input grouping attributes (GROUP BY attributes)
		// The list of NTuple objects represents what we call a "group": a special attribute created by a GROUP BY operation
		// A "group" will contain all attributes of the input except those used to perform the GROUP_BY operation (all grouped attributes)
		Map<NTupleUtils, List<NTuple>> map = new HashMap<NTupleUtils, List<NTuple>>();
		
		NTupleUtils keyUtils = null;
		NTupleUtils valUtils = null;
		
		// Loops through all input NTuples
		while (child.hasNext()) {
			NTuple current = child.next();			
			
			// Verifies that the current tuple actually contains all input grouping attributes
			for (String column : columns) {
				if (!Arrays.asList(current.nrsmd.colNames).contains(column)) {
					String error = "The NTuple does not have a column with the name " + column;
					log.error(error);
					throw new TatooineExecutionException(error);
				}
			}
			
			// Objects used as keys and values for our GROUP BY map
			keyUtils = new NTupleUtils();
			valUtils = new NTupleUtils();
			
			// Loops through all columns of an input NTuple
			int colNo = 0;
			for (String colName : current.nrsmd.colNames) {
				// If the column is a grouping attribute
				if (columns.contains(colName)) {
					keyUtils = addField(keyUtils, current, colName, colNo); 
				}				
				// If the column is a grouped attribute
				else {
					valUtils = addField(valUtils, current, colName, colNo);
				}
				colNo++;
			}			
						
			// If this is not the first time we encounter a specific set of grouping attributes
			// Then we will add a new NTuple to the list of NTuples associated with such set of attributes
			NTuple val = valUtils.buildTupleWithNames();
			if (map.containsKey(keyUtils)) {
				map.get(keyUtils).add(val);
			}
			// Otherwise we'll create an entry in the GROUP BY map for this specific set of attributes
			else {
				List<NTuple> list = new ArrayList<NTuple>();
				list.add(val);
				map.put(keyUtils, list);
			}
		}
		
		// Loops through all GROUP BY map entries
		for (Entry<NTupleUtils, List<NTuple>> entry : map.entrySet()) {
			// Unique set of grouping attributes
			NTupleUtils key = entry.getKey();
			// List of NTuples, each one containing a set of grouped attributes
			List<NTuple> list = entry.getValue();
		
			// Aggregate functions perform a calculation on a set of values and return a single value
			float accum = 0;
		
			// Creates a "group"
			NTupleUtils group = new NTupleUtils();
			for (int i = 0; i < list.size(); i++) {	
				
				// Adds a new "sub-group" (a set of grouped attributes)
				NTuple tap = list.get(i);
				group.addName("sub-group");
				group.addType(TupleMetadataType.TUPLE_TYPE);
				group.addNestedField(tap);
				group.addChild(tap.nrsmd);
				
				// Computes the value of the input aggregate function
				if (aggregator != null) {
					int value = 0;
					
					// Gets the value of the aggregation column
					if (!Strings.isNullOrEmpty(aggregator.getColumn())) {
						Object obj = tap.getValue(aggregator.getColumn());
						try {
							value = (int) obj;
						} catch (ClassCastException cce) {
							try{
								@SuppressWarnings("unchecked")
								ArrayList<NTuple> tuples = (ArrayList<NTuple>) obj;
								if (tuples.size() == 1) {
									value = tuples.get(0).getIntegerField(0);
								}
							} catch (Exception e) {
								String error = "The specified aggregator column is not of type integer or tuple and as such it is not valid";
								log.error(error);
								throw new TatooineExecutionException(error);
							}
						}						
					}
					
					// Updates our accumulated value
					accum = updateAccumulatedValue(accum, value, i, list.size());
				}
			}

			// Creates a special column that'll contain the aggregation function's accumulated value 
			if (aggregator != null) {
				key.addName("aggregator");
				key.addType(TupleMetadataType.INTEGER_TYPE);
				key.addInt(Math.round(accum));
			}
			
			// Creates a special column "group" containing a list of all sets of grouped attributes
			NTuple grouped = group.buildTupleWithNames();	
			key.addName("group");
			key.addType(TupleMetadataType.TUPLE_TYPE);
			key.addNestedField(grouped);
			key.addChild(grouped.nrsmd);

			groupedTuples.add(key.buildTupleWithNames());
		}		
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		cursorId = 0;
		try {
			// Groups NTuples by one or more columns (or grouping attributes) 
			aggregateTuples();
		} catch (Exception e) {
			String message = "Error aggregating NIterator: " + child.toString();
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	/** Checks if a NTuple is null or empty */
	private boolean isNullOrEmpty(NTuple tuple) {
		if (tuple == null) {
			return true;
		} else if (tuple.uriFields == null || tuple.integerFields == null || tuple.stringFields == null
				|| tuple.idFields == null || tuple.nestedFields == null) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (timeout) {
			return false;
		}
		
		try {
			tuple = groupedTuples.get(cursorId);
		} catch (IndexOutOfBoundsException e) {
			return false;
		}

		if (!isNullOrEmpty(tuple) && !timeout) {
			this.nrsmd = tuple.nrsmd;
			return true;
		}

		return false;
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		NTuple next = tuple;
		tuple = null;
		numberOfTuples++;
		cursorId++;
		return next;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		cursorId = 0;
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());		
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public String getName() {
		return "PhyGroupBy" + "(" + columns.toString() + ")";
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return "\n" + spaceForIndent + "PhyGroupBy(" + columns.toString() + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}

}
