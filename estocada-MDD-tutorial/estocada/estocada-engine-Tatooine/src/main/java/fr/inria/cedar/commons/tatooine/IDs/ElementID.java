package fr.inria.cedar.commons.tatooine.IDs;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Interface that will be implemented by classes that represent different kinds of IDs.
 * 
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public interface ElementID {

	/**
	 * TODO: Refine this into better type description (3 integers...).
	 * @return The type code, in a type system to be defined...
	 */
	public int getType();
	
	/** Returns true if ID1 is a parent of ID2 */
	public boolean isParentOf(ElementID id2) throws TatooineExecutionException;

	/** Returns true if ID1 is an ancestor of ID2 */
	public boolean isAncestorOf(ElementID id2) throws TatooineExecutionException;

	/** Returns the parent ID if it can be computed */
	public ElementID getParent() throws TatooineExecutionException;

	/** Returns true if ID1 starts strictly after ID2 */
	public boolean startsAfter(ElementID id2) throws TatooineExecutionException;

	/** Returns true if ID1 ends strictly after ID2 */
	public boolean endsAfter(ElementID id2) throws TatooineExecutionException;

	/** Returns the null element for this kind of ID */
	public ElementID getNull();

	/** Returns true if this element ID is null */
	public boolean isNull();
}
