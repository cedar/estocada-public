package fr.inria.cedar.commons.tatooine.operators.physical;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.DBSourceLayer;
import fr.inria.cedar.commons.tatooine.storage.database.RedisSourceLayer;
import ucsd.edu.redis.RedisKQL.query.RedisKQL;
import ucsd.edu.redis.RedisKQL.query.RedisKQLQueryParser;

/**
 * Physical operator that queries a running Redis instance.
 * @author Rana Alotaibi
 * @author Romain Primet
 * TODO: Refactor operator just like the other Scan OPs were refactored (PhyJENAEval, PhySOLREval, PhySOLRScan, PhySQLEval, PhySQLScan, PhyJQEval, PhyJQScan)
 */
public class PhyRedisEval extends BindAccess {
	
	private static final Logger log = Logger.getLogger(PhyRedisEval.class);
	
	/** Universal version identifier for the PhyRedisEval class */
	private static final long serialVersionUID = 8276324470960206143L;
	
	/* Query parameters */
	protected NTuple params;
	protected String queryStr;
	protected RedisKQL queryKql;
	protected GSR ref;
	protected static final String PLACEHOLDER = "?";	
	protected KQLResult result = null;

	// Interface that handles DB calls
	private DBSourceLayer source;	
		
	/* Constructor */
	public PhyRedisEval(String query, NRSMD nrsmd, GSR ref) throws TatooineExecutionException {
		super();
		queryStr = query;
		this.ref = ref;
		this.nrsmd = nrsmd;
		source = new RedisSourceLayer(ref);
		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		source.connect();
		result = null;
		queryKql = new RedisKQLQueryParser().parse(queryStr);
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		// Checking for timeout
		if (timeout) {
			return false;
		}
		
		// Fetching results from Redis if called for the first time
		if (result == null) {			
			Object[] parameters = new Object[] { queryKql };
			Object res = source.getBatchRecords(parameters);
			result = new KQLResult(res, nrsmd);
			result.open();
		} 
		
		return result.hasNext();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		return result.next();
	}

	@Override
	public void close() throws TatooineExecutionException {
		result.close();
		result = null;
		nrsmd = null;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return result.getNestedIterator(i);
	}

	@Override
	public String getName() {
		return "PhyRedisEval";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public void initialize() throws TatooineExecutionException {
	}
	
	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		this.params = params;
		int queryParamCount = StringUtils.countMatches(queryStr, PLACEHOLDER);
		if (queryParamCount != params.nrsmd.colNo) {
			String msg = String.format("Invalid parameter count: expected %d, got %d", queryParamCount, params.nrsmd.colNo);
			throw new IllegalArgumentException(msg);
		}
		try {
			updateParameters(params);	
		} catch (TatooineExecutionException e) {
			String msg = String.format("Error updating KQL input parameters '%s': %s", params, e.getMessage());
			log.error(msg);
			throw new IllegalArgumentException(msg);
		}
	}
	
	/** Fills in placeholders in parameterised queries with actual parameters passed in as input */
	public void updateParameters(NTuple params) throws TatooineExecutionException {
		int counter = 0;
		queryKql.reInitializeKQLParams();
		for (TupleMetadataType type : params.nrsmd.getColumnsMetadata()) {
			switch (type) {
			case INTEGER_TYPE: {
				queryKql.setKQLParams(("" + params.getIntegerField(counter)).toCharArray());
				break;
			}
			case STRING_TYPE: {
				queryKql.setKQLParams(params.getStringField(counter));
				break;
			}
			default:
				throw new TatooineExecutionException("Unsupported NTuple typecode: " + type);
			}
			counter++;
		}
		queryStr = queryKql.toString();
	}
	
	/* Getters and setters */
	public String getQuery() {
		return queryStr;
	}
	
	public void setQuery(String query) {
		queryStr = query;
	}
	
	public RedisKQL getQueryKQL() {
		return queryKql;
	}
	
	public void setQueryKQL(RedisKQL query) {
		queryKql = query;
	}
	
	public KQLResult getResultKQL() {
		return result;
	}
	
	public void setResultKQL(KQLResult result) {
		this.result = result;
	}
	
	public NRSMD getNRSMD() {
		return nrsmd;
	}
	
	public void setNRSMD(NRSMD nrsmd) {
		this.nrsmd = nrsmd;
	}
	
	public DBSourceLayer getDBSourceLayer() {
		return source;
	}
	
	public void setDBSourceLayer(DBSourceLayer source) {
		this.source = source; 
	}
}
