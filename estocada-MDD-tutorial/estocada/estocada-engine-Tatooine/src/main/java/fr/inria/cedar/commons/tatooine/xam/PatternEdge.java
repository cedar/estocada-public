package fr.inria.cedar.commons.tatooine.xam;

import java.io.Serializable;

/**
 * Represents the edges that will connect two nodes in a {@link TreePattern}.
 * 
 * @author Ioana MANOLESCU
 * @created 13/06/2005
 */
public final class PatternEdge implements Serializable {

	/** Universal version identifier for the PatternEdge class */
	private static final long serialVersionUID = 4223984824001655403L;

	/**
	 * The "parent" (upper) node. Strictly speaking the edge may avoid storing the parent node
	 * since the parent node stores the edges. But it's more convenient this way.
	 */
	public TreePatternNode n1;

	/** The "child" (lower) node */
	public TreePatternNode n2;

	/** If false, the edge is ancestor-descendant, otherwise it is parent-child */
	private boolean parent;

	/** If true, the join is nested with the child being nested under the parent */
	private boolean nested;	

	public PatternEdge(
		TreePatternNode n1,
		TreePatternNode n2,
		boolean parent,
		boolean nested) {
		this.n1 = n1;
		this.n2 = n2;
		this.parent = parent;
		this.nested = nested;
	}

	/** Returns the parent (upper) node */
	public TreePatternNode getN1() {
		return n1;
	}

	/** Returns the child (lower) node */
	public TreePatternNode getN2() {
		return n2;
	}

	/** If false, the edge is ancestor-descendant, otherwise it is parent-child */
	public boolean isParent() {
		return parent;
	}
	
	public void setParent(boolean parent) {
		this.parent = parent;
	}
	
	/** If true, the join is nested with the child being nested under the parent */
	public boolean isNested(){
		return this.nested;
	}
	
	public void setNested(boolean nested){
		this.nested = nested;
	}

	/** Returns a string representing a PatternEdge */
	public static String display(PatternEdge parentEdge) {
		StringBuffer sb = new StringBuffer();
		if (parentEdge == null){
			sb.append("null");
		}
		else{
			if (parentEdge.n1 != null) {
				sb.append(parentEdge.n1.getTag());
			}
			else{
				sb.append("null");
			}
			if (parentEdge.isParent()){
				sb.append("/");
			}
			else{
				sb.append("//");
			}
			if (parentEdge.isNested()){
				sb.append("n");
			}
			sb.append(parentEdge.n2.getTag() + " (" + (parentEdge.n1 == null?"":parentEdge.n1.getNodeCode()) + " " + parentEdge.n2.getNodeCode() + ")");
		}
		return new String(sb);
	}

}
