package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.StructField;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.utilities.NTupleUtils;

/**
 * Implements an iterator pattern over the result by AQL evaluator
 * 
 * @author ranaalotaibi.
 */
class SparkAQLResult extends NIterator {

    private static final long serialVersionUID = -1445643856542752260L;
    protected final Dataset dfResult;
    protected List<NTuple> tuples = null;
    protected Iterator<NTuple> nIterator = null;

    SparkAQLResult(Dataset result, NRSMD nrsmd) throws TatooineExecutionException {
        super();
        if (result == null) {
            throw new IllegalArgumentException("No DataFrame Provided");
        }
        this.dfResult = result;
        this.nrsmd = nrsmd;
    }

    @Override
    public boolean hasNext() throws TatooineExecutionException {
        return nIterator.hasNext();
    }

    @Override
    public NTuple next() throws TatooineExecutionException {
        return nIterator.next();
    }

    @Override
    public void open() throws TatooineExecutionException {
        tuples = DF2NTuples();
        nIterator = tuples.iterator();
    }

    @Override
    public void close() throws TatooineExecutionException {
        tuples = null;
        nIterator = null;
    }

    @Override
    public NIterator getNestedIterator(int i) throws TatooineExecutionException {
        throw new TatooineExecutionException("NOT Implemented");
    }

    @Override
    public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public NIterator copy() throws TatooineExecutionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getName() {
        return "AQLResult";
    }

    @Override
    public String getName(int depth) {
        return getName();
    }

    /**
     * Transform the DataFrame to Ntuples
     * 
     * @return List of NTuples
     * @throws TatooineExecutionException
     */
    private List<NTuple> DF2NTuples() throws TatooineExecutionException {
        List<NTuple> ntuples = new ArrayList<NTuple>();
        Row[] records = (Row[]) dfResult.collect();

        for (Row r : records) {
            NTupleUtils ntupleUtils = new NTupleUtils();
            NTuple ntuple = new NTuple();
            ntuple.nrsmd = this.nrsmd;
            for (StructField record : r.schema().fields()) {
                Object value = r.get(r.fieldIndex(record.name()));
                switch (value.getClass().getSimpleName()) {
                    case "String":
                        ntupleUtils.addName(record.name());
                        ntupleUtils.addString((String) value);
                        ntupleUtils.addType(TupleMetadataType.STRING_TYPE);
                        break;
                    case "Integer":
                    case "Double":
                    case "Float":
                    case "Long":
                    case "Short":
                        ntupleUtils.addName(record.name());
                        ntupleUtils.addInt((Integer) value);
                        ntupleUtils.addType(TupleMetadataType.INTEGER_TYPE);
                        break;
                    case "GenericRowWithSchema":
                        ntupleUtils = addNestedRecord(record.name(), ((GenericRowWithSchema) value), ntupleUtils);

                }
            }
            ntuple = ntupleUtils.buildTupleWithNames();
            ntuples.add(ntuple);
        }
        return ntuples;
    }

    /**
     * Add nested record into NTuple
     * 
     * @param recordName
     *            name of JSON Attribute
     * @param value
     *            the record value of type GenericRowWithSchema
     * @param ntupleUtils
     *            the main Ntuple to add to it the nested fields
     * @return
     * @throws TatooineExecutionException
     *             TODO: addNestedArray NOTE : Incorporated Oscar approach of
     *             using NTupleUtils that has been implemented by him
     * 
     */
    public NTupleUtils addNestedRecord(String recordName, GenericRowWithSchema value, NTupleUtils ntupleUtils)
            throws TatooineExecutionException {
        ntupleUtils.addName(recordName);
        ntupleUtils.addType(TupleMetadataType.TUPLE_TYPE);
        NTupleUtils newNtupleUtils = new NTupleUtils();
        for (StructField field : value.schema().fields()) {
            Object result = value.get(value.fieldIndex(field.name()));
            switch (result.getClass().getSimpleName()) {
                case "String":
                    newNtupleUtils.addName(field.name());
                    newNtupleUtils.addType(TupleMetadataType.STRING_TYPE);
                    newNtupleUtils.addString((String) result);
                    break;
                case "Integer":
                case "Double":
                case "Float":
                case "Long":
                case "Short":
                    newNtupleUtils.addName(field.name());
                    newNtupleUtils.addType(TupleMetadataType.INTEGER_TYPE);
                    newNtupleUtils.addInt((Integer) (result));
                    break;
                case "GenericRowWithSchema":
                    newNtupleUtils = addNestedRecord(field.name(), ((GenericRowWithSchema) value), ntupleUtils);
            }

        }

        ntupleUtils.addNestedField(newNtupleUtils.buildTupleWithNames());
        ntupleUtils.addChild(new NRSMD(newNtupleUtils.getTypes().size(),
                newNtupleUtils.getTypes().toArray(new TupleMetadataType[newNtupleUtils.getTypes().size()]),
                newNtupleUtils.getNames().toArray(new String[newNtupleUtils.getNames().size()]),
                newNtupleUtils.getChildren().toArray(new NRSMD[newNtupleUtils.getChildren().size()])));
        return ntupleUtils;
    }
}
