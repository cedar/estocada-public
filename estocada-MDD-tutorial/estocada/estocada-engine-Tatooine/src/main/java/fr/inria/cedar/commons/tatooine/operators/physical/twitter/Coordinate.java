package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

public class Coordinate {

	// Latitude
	private float lat;
	// Longitude
	private float lon;	
	 
	// Constants 
	// Minimum and maximum values for latitude and longitude
	private static final float MIN_LAT = Float.valueOf("-90.0000");	  
	private static final float MAX_LAT = Float.valueOf("90.0000");	  
	private static final float MIN_LON = Float.valueOf("-180.0000");	  
	private static final float MAX_LON = Float.valueOf("180.0000");
	 
	// Constructor
	public Coordinate(float lat, float lon) {
		if (isValidLat(lat) && isValidLon(lon)) {
			this.lat = lat;
			this.lon = lon;
		}
		else {
			String msg = "The latitude or longitude values provided are not valid";
			throw new IllegalArgumentException(msg);
		}
	}
	
	/** Checks if input latitude is valid */
	private boolean isValidLat(float lat) {
		return (lat >= MIN_LAT && lat <= MAX_LAT);
	}
	
	/** Checks if input longitude is valid */
	private boolean isValidLon(float lon) {
		return (lon >= MIN_LON && lon <= MAX_LON);
	}
	
	// Getters and setters
	public float getLat() {
		return lat;
	}
	
	public float getLon() {
		return lon;
	}
	
	public void setLat(float lat) {
		this.lat = lat;
	}
	
	public void setLon(float lon) {
		this.lon = lon;
	}
	
	public String toString() {
		return String.format("(Lat: %f, Lon: %f)", lat, lon);
	}
}
