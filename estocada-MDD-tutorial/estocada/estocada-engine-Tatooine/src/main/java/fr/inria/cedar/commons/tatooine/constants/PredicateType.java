package fr.inria.cedar.commons.tatooine.constants;

/**
 * Enumerated type representing the possible types of predicates.
 * 
 * @author Jesus CAMACHO RODRIGUEZ
 * @created 16/12/2010
 */
public enum PredicateType {
	PREDICATE_EQUAL, PREDICATE_NOT_EQUAL, PREDICATE_PARENT, PREDICATE_CHILD, PREDICATE_ANCESTOR, PREDICATE_DESCENDANT, PREDICATE_BEFORE, 
	// Predicate types for integers 
	PREDICATE_GREATER_THAN, PREDICATE_LESS_THAN, PREDICATE_GREATER_THAN_OR_EQUAL, PREDICATE_LESS_THAN_OR_EQUAL;

	/**
	 * Returns the reverse type for this predicate. If this predicate does not have a reverse, then the method returns
	 * the same predicate.
	 * 
	 * @return the reverse type
	 */
	@SuppressWarnings("incomplete-switch")
	public PredicateType revert() {
		switch (this) {
		case PREDICATE_PARENT:
			return PredicateType.PREDICATE_CHILD;
		case PREDICATE_CHILD:
			return PredicateType.PREDICATE_PARENT;
		case PREDICATE_ANCESTOR:
			return PredicateType.PREDICATE_DESCENDANT;
		case PREDICATE_DESCENDANT:
			return PredicateType.PREDICATE_ANCESTOR;
		}
		return this;
	}
}