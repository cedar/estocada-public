package fr.inria.cedar.commons.tatooine.loader;

import org.apache.solr.common.SolrInputDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by DucCao on 05/10/16.
 * Utility class to convert JSON to SolrInputDocument
 *
 * Reference: https://github.com/internetarchive/offlinesolr/blob/master/src/main/java/org/archive/offlinesolr/JSONDocumentReader.java
 */
public class SolrInputDocumentReader {

    /**
     *
     * @param jsonFilePath path to the JSON file which contains a JSON array
     * @return List of SolrInputDocument objects
     * @throws IOException
     * @throws ParseException
     */
    @SuppressWarnings("unchecked")
	public static List<SolrInputDocument> readJsonFile(String jsonFilePath) throws IOException, ParseException {
        FileReader fileReader = new FileReader(jsonFilePath);
        JSONArray jsonArray = (JSONArray) new JSONParser().parse(fileReader);
        return (List<SolrInputDocument>) jsonArray.stream()
                .map(jsonObject -> makeSolrDoc((JSONObject) jsonObject))
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
	private static SolrInputDocument makeSolrDoc(JSONObject jsonObject) {
        SolrInputDocument doc = new SolrInputDocument();

        jsonObject.keySet().forEach(key -> {
            String keyName = (String) key;
            Object value = jsonObject.get(keyName);
            if (value instanceof JSONArray) {
                JSONArray array = (JSONArray) value;
                for (int i = 0; i < array.size(); i++) {
                    doc.addField(keyName, array.get(i));
                }
            }
            else {
                doc.addField(keyName, value);
            }
        });
        return doc;
    }

}
