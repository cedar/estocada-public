package fr.inria.cedar.commons.tatooine.constants;

/**
 * Enumeration that represents the different types of identifiers that we can create for patterns.
 * @author Jesus CAMACHO RODRIGUEZ
 */
public enum IDGeneratorType {
	XAM, XAM_VJ;
}
