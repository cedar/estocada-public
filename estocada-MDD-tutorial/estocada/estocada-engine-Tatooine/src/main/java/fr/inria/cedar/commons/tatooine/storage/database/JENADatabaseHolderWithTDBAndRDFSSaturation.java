package fr.inria.cedar.commons.tatooine.storage.database;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.loader.JenaDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class JENADatabaseHolderWithTDBAndRDFSSaturation extends JENADatabaseHolderWithTDB {

    private static final Logger log = Logger.getLogger(JENADatabaseHolderWithTDBAndRDFSSaturation.class);

    final static protected Map<String, Model> mapLoadedSaturationRdfFiles = new HashMap<>();

    final static protected String RULES_FILE = "FullRDFSJenaRules.txt";

    public JENADatabaseHolderWithTDBAndRDFSSaturation(String model) {
        super(model);
    }

    @Override
    protected Model loadModel() {
        Model model = super.loadModel();

		BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(RULES_FILE)));
        
        List<Rule> rules = Rule.parseRules(Rule.rulesParserFromReader(br));
        GenericRuleReasoner reasoner = new GenericRuleReasoner(rules);

        InfModel inf = ModelFactory.createInfModel(reasoner, model);
        return inf;
    }

    public void setModel(GSR ref) {
        this.modelStr = ref.getPropertyValue(JenaDatabaseLoader.MODEL_RDF);
        this.currentCatalogEntry = ref.getViewURI();

        if (mapLoadedSaturationRdfFiles.containsKey(modelStr)) {
            log.debug(String.format("saturation of RDF file %s has been loaded", modelStr));
            Model loadedModel = mapLoadedSaturationRdfFiles.get(modelStr);
            mapLoadedCatalogEntries.put(currentCatalogEntry, loadedModel);
        } else if (!hasModelBeenLoaded(currentCatalogEntry)) {
            log.debug("setModel: " + currentCatalogEntry + ", " + modelStr);
            Model loadedModel = loadModel();
            mapLoadedCatalogEntries.put(currentCatalogEntry, loadedModel);
            mapLoadedRdfFiles.put(modelStr, loadedModel);
        }

    }
}
