package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayDeque;
import java.util.Queue;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;
import fr.inria.cedar.commons.tatooine.utilities.NTupleFunction;

/**
 * This creates a BindAcess from a NTupleFunction
 * 
 * @author Raphael BONAQUE
 */
public class FunctionCallBindAccess extends BindAccess {

	private static final long serialVersionUID = -6007103153724668912L;
	
	private NTupleFunction function;
	private Queue<NTuple> currentOutuputQueue;

	public FunctionCallBindAccess(NTupleFunction function) {
		super();
		this.function = function;
		this.nrsmd = function.getOutputNRSMD();
	}

	@Override
	public void setParameters(NTuple params) throws IllegalArgumentException {
		try {
			currentOutuputQueue = new ArrayDeque<NTuple>(function.call(params));
		} catch (TatooineException e){
		  throw new IllegalArgumentException();
		}
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		return !currentOutuputQueue.isEmpty();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		if (currentOutuputQueue.isEmpty()) throw new TatooineExecutionException("No more tuples");
		return currentOutuputQueue.poll();
	}

	@Override
	public void open() throws TatooineExecutionException {}

	@Override
	public void close() throws TatooineExecutionException {}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new FunctionCallBindAccess(function);
	}

	@Override
	public String getName() {
		return "FunctionCallBindAccess(" + function.getName() + ")";
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return "\n" + spaceForIndent + "FunctionCallBindAccess(" + function.getName() + ")";
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new UnsupportedOperationException();
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"FunctionCallBindAccess\n" +
                  this.function.toString().replace("\"","\\\"") +
                  "\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		return firstAvailableNo + 1;
	}

	public NTupleFunction getFunction() {
		return function;
	}

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
