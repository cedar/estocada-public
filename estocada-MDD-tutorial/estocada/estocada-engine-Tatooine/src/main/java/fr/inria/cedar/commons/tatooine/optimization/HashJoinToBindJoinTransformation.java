package fr.inria.cedar.commons.tatooine.optimization;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.primitives.Ints;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.JenaSPARQLQueryParameterisedOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.QueryParameterisedOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.SolrJSONQueryParameterisedOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyMemoryHashJoin;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

public class HashJoinToBindJoinTransformation implements Transformation {

	private static final String errorMsg = "Unable to transform HashJoin to BindJoin";
	private static final Logger log = Logger.getLogger(HashJoinToBindJoinTransformation.class);
	
	// Flag that indicates if the PhyHashJoin to be transformed has a simple or a conjunctive predicate
	private static boolean simplePredicate = true;
	
	// This list will contain the indexes of the columns that should be used to parameterise the resulting query
	List<Integer> bindingPatternList = new ArrayList<Integer>();
	
	// Flag that indicates that the common JOIN column(s) of the OPs of a PhyHashJoin appear in the original SQL query
	private static boolean flagSQL = false;
	
	@Override
	public NIterator apply(NIterator node) throws TatooineExecutionException {
		PhyBindJoin bind = null;
		PhyMemoryHashJoin hash = (PhyMemoryHashJoin) node;
		flagSQL = false;
				
		// Note: A PhyBindJoin keeps the left operator of a PhyHashJoin as is
		// Note: But the right operator needs to change: its query needs to be parameterised
		NIterator opl = hash.left;
		BindAccess opr = (BindAccess) hash.right;
		Predicate pred = hash.getPredicate();
			
		// Determine type of predicate
		if (pred instanceof SimplePredicate) {
			simplePredicate = true;
		} else if (pred instanceof ConjunctivePredicate) {
			simplePredicate = false;
		} else {
			String error = String.format("Unhandled predicate type: %s", pred.getClass().getName());
			log.error(error);
			throw new TatooineExecutionException(error);
		}				
		try {	
			// TODO: This code currently supports as right OPs:
			// SPARQL queries evaluated through Apache Jena
			// JSON queries evaluated through Apache Solr
			// SQL queries evaluated through a JDBC-compliant RDBMS (such as PostgresSQL)
			// Could also be handled in the future: JSON queries evaluated through Apache Spark and Redis queries
			switch (opr.getClass().getSimpleName()) {
				case "PhyJENAEval":
					bind = transformForJenaBindAccess(opl, opr, pred);
					break;
				case "PhySOLREval":
				case "PhySOLRScan":
					bind = transformForSolrBindAccess(opl, opr, pred);
					break;	
				case "PhySQLEval":
				case "PhySQLScan":
					bind = transformForSQLBindAccess(opl, opr, pred);
					break;					
				default:
					break;
			}
		} catch (TatooineExecutionException e) {
			String error = String.format("%s: %s", errorMsg, e.getMessage());
			log.error(error);
			throw new TatooineExecutionException(error);
		}
		return bind;
	}

	@Override
	public boolean isApplicable(NIterator node) {
		if (node instanceof PhyMemoryHashJoin) {
			return true;
		}
		return false;
	}
	
	
	/** Nested class used as a wrapper to simplify the transformation code */
	private class ParameterisedQuery {
		public String query = "";
		public List<Integer> bindings = new ArrayList<Integer>();
		
		public ParameterisedQuery(String query) {
			this.query = query;
		}			
	}
	
	/** Replaces common JOIN column names with Apache Jena's SPARQL placeholders */
	private ParameterisedQuery parameteriseSPARQLQueryJena(ParameterisedQuery parQuery, SimplePredicate pred, String col, String replacement) throws TatooineExecutionException {
		int n = StringUtils.countMatches(parQuery.query, col);
		
		// The common JOIN column should appear at least once in the SPARQL query
		if (n < 1) {
			String msg = "The name of the common JOIN column of both OPs of the PhyHashJoin ('%s') does not appear in its SPARQL query";
			String error = String.format(msg, col);
			log.error(error);
			throw new TatooineExecutionException(error);
		}
		
		// We'll replace appearances of this string in the SPARQL query (except for the SELECT clause) with placeholders
		// According to SPARQL's query syntax (@see https://www.w3.org/TR/sparql11-query/), the SELECT statement is followed by a curly bracket
		String pattern = "(.*SELECT.*)(\\{.*)";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(parQuery.query);
		String outer = "";
		String inner = "";
		if (m.find( )) {
			outer = m.group(1);
			inner = m.group(2);
		}
		
		// Example (assume the common JOIN column is "user_screen_name_s"):
		// HashJoin query: SELECT ?user_screen_name_s ?name_s ?surname_s WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:twitter ?user_screen_name_s . }
		// BindJoin query: SELECT ?user_screen_name_s ?name_s ?surname_s WHERE { ?pol fm:firstName ?name_s . ?pol fm:lastName ?surname_s . ?pol fm:twitter "%s" . } VALUES ?user_screen_name_s { "%s" }				
		int numRepetitions = StringUtils.countMatches(inner, "?" + col);
		inner = inner.replaceAll("\\?" + col, replacement);		
		parQuery.query = outer + inner;
		for (int i = 0; i < numRepetitions; i++) {
			parQuery.bindings.add(pred.column);
		}
		
		return parQuery;
	}
	
	private ParameterisedQuery parameteriseJSONQuerySolr(ParameterisedQuery parQuery, SimplePredicate pred, String col) {
		if (parQuery.query.equals("*:*")) {
			parQuery.query = String.format("%s:%s", col, SolrJSONQueryParameterisedOperator.PLACEHOLDER);
		} else {
			parQuery.query += String.format(" AND %s:%s", col, SolrJSONQueryParameterisedOperator.PLACEHOLDER);
		}
		parQuery.bindings.add(pred.column);
		return parQuery;
	}
	
	private PhyBindJoin transformForJenaBindAccess(NIterator opl, BindAccess opr, Predicate predicate) throws TatooineExecutionException {
		QueryParameterisedOperator leaf = (QueryParameterisedOperator) opr;
		ParameterisedQuery parQuery = new ParameterisedQuery(leaf.getQuery());
						
		// Each simple predicate tells us the name a common JOIN column of both OPs of a PhyHashJoin
		if (simplePredicate) {
			// Case: There is only one common JOIN column
			SimplePredicate pred = (SimplePredicate) predicate;
			String col = opl.nrsmd.colNames[pred.column];		
			parQuery = parameteriseSPARQLQueryJena(parQuery, pred, col, String.format("\"%s\"", JenaSPARQLQueryParameterisedOperator.PLACEHOLDER));
			
			// Finally we'll bind the name of this common JOIN column using VALUES
			parQuery.query = parQuery.query + String.format(" VALUES %s { \"%s\" }", "?" + col, JenaSPARQLQueryParameterisedOperator.PLACEHOLDER);
			parQuery.bindings.add(pred.column);			
		} else {
			// Case: There are several common JOIN columns
			ConjunctivePredicate conj = (ConjunctivePredicate) predicate;
			
			// Note: We will be replacing appearances of the common JOIN columns in the SPARQL query with placeholders
			// But we need to keep track of the order in which these columns appear, since this will tell us how to build the binding pattern list later on
			// We will use a map for this
			Map<String, Integer> map = new HashMap<String, Integer>();			
			List<String> bindingsVar = new ArrayList<String>();
			List<String> bindingsVal = new ArrayList<String>();
			
			// Index of the simple predicate we're iterating over
			int indexPredicate = 0;
			
			for (Predicate item: conj.preds) {			
				if (!(item instanceof SimplePredicate)) {
					String error = "Only conjunctive predicates made up of simple predicates are supported";
					log.error(error);
					throw new TatooineExecutionException(error);
				}
				
				// Update SPARQL query to reflect current predicate
				SimplePredicate pred = (SimplePredicate) item;
				String col = opl.nrsmd.colNames[pred.column];
				String tmp = String.format("%s%d", JenaSPARQLQueryParameterisedOperator.PLACEHOLDER, ++indexPredicate);	
				parQuery = parameteriseSPARQLQueryJena(parQuery, pred, col, tmp);
				map.put(tmp, opl.nrsmd.getColIndexFromName(col));
				
				// Building the VALUES block of the SPARQL query
				bindingsVar.add("?" + col);
				bindingsVal.add(tmp);				
			}
			
			// Appending the VALUES block to the SPARQL query
			parQuery.query = parQuery.query + String.format(" VALUES (%s) { (%s) } ", StringUtils.join(bindingsVar, " "), StringUtils.join(bindingsVal, " "));
			parQuery.bindings = new ArrayList<Integer>();
			
			// Populating the binding pattern list by looking at all temporary placeholders 
			String pattern = "%s\\d+";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(parQuery.query);			
			while (m.find( )) {
				parQuery.bindings.add(map.get(m.group(0)));
			}
			
			// Replacing temporary placeholders with actual placeholders
			for (Entry<String, Integer> tuple: map.entrySet()) {
				parQuery.query = parQuery.query.replaceAll(tuple.getKey(), String.format("\"%s\"", JenaSPARQLQueryParameterisedOperator.PLACEHOLDER));				
			}
		}
				
		leaf.setQuery(parQuery.query);		
		return new PhyBindJoin(opl, leaf, Ints.toArray(parQuery.bindings));
	}	
	
	private PhyBindJoin transformForSolrBindAccess(NIterator opl, BindAccess opr, Predicate predicate) throws TatooineExecutionException {
		QueryParameterisedOperator leaf = (QueryParameterisedOperator) opr;
		ParameterisedQuery parQuery = new ParameterisedQuery(leaf.getQuery());
				
		// Each simple predicate tells us the name a common JOIN column of both OPs of a PhyHashJoin
		if (simplePredicate) {
			// Case: There is only one common JOIN column
			SimplePredicate pred = (SimplePredicate) predicate;
			parQuery = parameteriseJSONQuerySolr(parQuery, pred, opl.nrsmd.colNames[pred.column]);		
		} else {
			// Case: There are several common JOIN columns
			ConjunctivePredicate conj = (ConjunctivePredicate) predicate;
			
			for (Predicate prod: conj.preds) {				
				if (!(prod instanceof SimplePredicate)) {
					String error = "Only conjunctive predicates made up of simple predicates are supported";
					log.error(error);
					throw new TatooineExecutionException(error);
				}				
				SimplePredicate pred = (SimplePredicate) prod;
				parQuery = parameteriseJSONQuerySolr(parQuery, pred, opl.nrsmd.colNames[pred.column]);
			}			
		}
		
		leaf.setQuery(parQuery.query);
		return new PhyBindJoin(opl, leaf, Ints.toArray(parQuery.bindings));
	}
	
	private PhyBindJoin transformForSQLBindAccess(NIterator opl, BindAccess opr, Predicate predicate) throws TatooineExecutionException {
		QueryParameterisedOperator leaf = (QueryParameterisedOperator) opr;
		String query = leaf.getQuery();
		bindingPatternList = new ArrayList<Integer>();
		
		if (simplePredicate) {
			// Case: There is only one common JOIN column
			SimplePredicate pred = (SimplePredicate) predicate;
			String col = opl.nrsmd.colNames[pred.column];
			query = parameteriseSQLQuery(query, col);
		} else {
			// Case: There are several common JOIN columns
			ConjunctivePredicate conj = (ConjunctivePredicate) predicate;
			
			for (Predicate prod: conj.preds) {				
				if (!(prod instanceof SimplePredicate)) {
					String error = "Only conjunctive predicates made up of simple predicates are supported";
						log.error(error);
						throw new TatooineExecutionException(error);
				}				
				SimplePredicate pred = (SimplePredicate) prod;
				String col = opl.nrsmd.colNames[pred.column];
				query = parameteriseSQLQuery(query, col);
			}	
		}
		
		// Now we have parameterised the initial query
		// We need to create an int array with the index numbers of the columns of the left JOIN operator to be used as parameters
		leaf.setQuery(query);		
		getSQLBindings(query, opl.nrsmd);		
		
		return new PhyBindJoin(opl, leaf, Ints.toArray(bindingPatternList));
	}
	
	/** Finds the index numbers of the columns of an operator to be used as parameters in a parameterised SQL query */
	private void getSQLBindings(String query, NRSMD nrsmd) throws TatooineExecutionException {
		CCJSqlParserManager manager = new CCJSqlParserManager();
		Select select = null;
		try {
			select = (Select) manager.parse(new StringReader(query));
			PlainSelect ps = (PlainSelect) select.getSelectBody();			
			if (ps.getWhere() != null) {
				bindingPatternBuilderForSQL(ps.getWhere(), nrsmd);
			} 
		} catch (JSQLParserException e) {
			String msg = String.format("Error parsing SQL query with JSQLParser: %s", query);
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}		
	}
	
	/** Traverses the conditions of a WHERE statement of a SQL query in order and creates a binding pattern list */
	private void bindingPatternBuilderForSQL(Expression exp, NRSMD nrsmd) throws TatooineExecutionException {
		switch (exp.getClass().getSimpleName()) {
			case "AndExpression":
				AndExpression expConj = (AndExpression) exp;
				bindingPatternBuilderForSQL(expConj.getLeftExpression(), nrsmd);
				bindingPatternBuilderForSQL(expConj.getRightExpression(), nrsmd);
				break;
			case "OrExpression":
				OrExpression expDisj = (OrExpression) exp;
				bindingPatternBuilderForSQL(expDisj.getLeftExpression(), nrsmd);
				bindingPatternBuilderForSQL(expDisj.getRightExpression(), nrsmd);
				break;
			case "Expression":				
			default:
				BinaryExpression eqx = (BinaryExpression) exp;
				if (eqx.getRightExpression().getClass().getSimpleName().equals("JdbcParameter")) {
					bindingPatternList.add(nrsmd.getColIndexFromName(eqx.getLeftExpression().toString()));
				}
				break;
		}	
	}
	
	/** 
	 * Parameterises a SQL query:
	 * If the query does NOT contain a WHERE clause, then add one with a clause that binds the common JOIN column to a parameterised value
	 * If it does but does NOT contain any occurrences of the common JOIN column, add a clause like the one described in the previous step 
	 * If it does and also contains occurrences of the common JOIN column, then parameterise these
	 */
	private String parameteriseSQLQuery(String query, String col) throws TatooineExecutionException {
		// Parser
		CCJSqlParserManager manager = new CCJSqlParserManager();
		Select select = null;
		try {
			select = (Select) manager.parse(new StringReader(query));
			PlainSelect ps = (PlainSelect) select.getSelectBody();			
			Expression where = null;
			if (ps.getWhere() != null) {
				where = updateWhereStatementSQL(ps.getWhere(), col);
				if (!flagSQL) {
					// If the common column was not found in the original query
					// Then add a parameterised clause to it
					where = new AndExpression(createEqualsToSQL(col), where); 
				}
			} else {
				// If the query did not contain a WHERE section
				// Add one to it with a parameterised clause
				where = createEqualsToSQL(col);
			}			
			ps.setWhere(where);
			select.setSelectBody(ps);
			query = select.toString();
		} catch (JSQLParserException e) {
			String msg = String.format("Error parsing SQL query with JSQLParser: %s", query);
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}	
		
		return query;
	}	
	
	/** Traverses the conditions of a WHERE statement of a SQL query and parameterises the occurrences of the common JOIN column */
	public Expression updateWhereStatementSQL(Expression exp, String col) {
		switch (exp.getClass().getSimpleName()) {
			case "AndExpression":
				AndExpression expConj = (AndExpression) exp;
				expConj.setLeftExpression(updateWhereStatementSQL(expConj.getLeftExpression(), col));
				expConj.setRightExpression(updateWhereStatementSQL(expConj.getRightExpression(), col));
				return expConj;
			case "OrExpression":
				OrExpression expDisj = (OrExpression) exp;
				expDisj.setLeftExpression(updateWhereStatementSQL(expDisj.getLeftExpression(), col));
				expDisj.setRightExpression(updateWhereStatementSQL(expDisj.getRightExpression(), col));
				return expDisj;
			case "Expression":				
			default:
				BinaryExpression eqx = (BinaryExpression) exp;				
				// If we find a binary expression whose left side is the common JOIN column
				// Then we'll parameterise it
				if (eqx.getLeftExpression().toString().equals(col)) {
					flagSQL = true;
					Column placeholder = new Column();
					placeholder.setColumnName("?");
					eqx.setRightExpression(placeholder);
				}
				return eqx;
		}				
	}	
	
	/** Creates EqualsTo SQL expression that will parameterise an given query */
	private EqualsTo createEqualsToSQL(String col) {
		Column expL = new Column();
		expL.setColumnName(col);
		Column expR = new Column();
		expR.setColumnName("?");						
		EqualsTo eqx = new EqualsTo();
		eqx.setLeftExpression(expL);
		eqx.setRightExpression(expR);
		return eqx;
	}

}
