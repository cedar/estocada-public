package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.Arrays;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.OrderMarker;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Physical operator that represents a simple projection over its child
 * that will keep only the columns specified in the constructor.
 * @author Ioana MANOLESCU
 */
public class PhyProjection extends NIterator {

	/** Universal version identifier for the PhyProjection class */
	private static final long serialVersionUID = -1877915260707327398L;

	int[] keepColumns;
	String projString;
	NTuple t;

	public NIterator child;

	/** Constructor */
	public PhyProjection(NIterator child, int keepColumn) throws TatooineExecutionException {
		this(child, new int[] { keepColumn });
	}

	/** Constructor */
	public PhyProjection(NIterator child, int[] keepColumns) throws TatooineExecutionException {
		super(child);
		this.child = child;
		this.nrsmd = NRSMD.makeProjectRSMD(child.nrsmd, keepColumns);
		this.keepColumns = keepColumns;

		StringBuffer sb = new StringBuffer();
		sb = new StringBuffer();
		sb.append("[");
		for (int i = 0; i < keepColumns.length; i ++){
			sb.append(keepColumns[i]);
			if (i < keepColumns.length - 1){
				sb.append(", ");
			}
		}
		sb.append("]");
		projString = new String(sb);
		
		if (child.getOrderMaker() != null) this.setOrderMaker(OrderMarker.project(child.getOrderMaker(), keepColumns));

		increaseOpNo();
	}

	@Override
	public void open() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(0, getOperatorID());
		child.open();
		informPlysPlanMonitorOperStatus(1, getOperatorID());
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if(timeout) {
			return false;
		}
		return child.hasNext();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		t = NTuple.project(child.next(), this.nrsmd, this.keepColumns);
		this.numberOfTuples++;
		return t;
	}

	@Override
	public void close() throws TatooineExecutionException {
		informPlysPlanMonitorOperStatus(2, getOperatorID());
		child.close();
		informPlysPlanMonitorOperStatus(3, getOperatorID());
		informPlysPlanMonitorTuplesPassed(this.numberOfTuples, this.getOperatorID());
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		return new PhyArrayIterator(t.getNestedField(i), t.nrsmd.getNestedChild(i));
	}

	@Override
	public String getName() {
		return "PhyProj(" + child.getName(0) + "," + projString + "\n"  + ")";
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		String tabs = spaceForIndent + getTabs(PRINTING_INDENTATION_TABS);
		return "\n" + spaceForIndent + "PhyProj(" + child.getName(1 + depth) + "," + "\n" + tabs +
				projString  + "\n"  + ")";
	}
	
	@Override
	public String getDetailedName(){
		return "Project(" + projString + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new PhyProjection(this.child.copy(), this.keepColumns);
	}

	/**
	 * Adds the code for the graphical representation to the StringBuffer.
	 * @author Aditya SOMANI
	 */
	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"Project\n" + Arrays.toString(this.keepColumns) +"\", color = " + getColoring() + "] ; \n");
		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		int childNumber1 = child.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
		return childNumber1;
	}

    public NIterator getChild() {
        return this.child;
    }

    public int[] getKeepColumns() {
        return this.keepColumns;
    }

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
