package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;

public abstract class BindAccess extends NIterator {
	/** Universal version identifier for the BindAccess class */
	private static final long serialVersionUID = -1388675961784307813L;

	public abstract void setParameters(final NTuple params) throws IllegalArgumentException;
	
	public BindAccess() {super();}
	public BindAccess(List<NIterator> children) {super(children);}
	public BindAccess(NIterator child) {super(child);}
	
	/** Method that fills in a parameterised query of type string using replace first */
	protected String updateParameters(NTuple params, String queryStr, String placeholder, String replacementBlank) throws IllegalArgumentException {
		String paramStr = queryStr;			
		
		// Verifies the number of input parameters matches the number of placeholders in the query 
		int queryParamCount = StringUtils.countMatches(queryStr, placeholder);
		if (queryParamCount != params.nrsmd.colNo) {
			String msg = String.format("Invalid parameter count: Expected %d, but got %d", queryParamCount, params.nrsmd.colNo);
			throw new IllegalArgumentException(msg);
		}

		// Replaces placeholders with the contents of the input parameters
		NRSMD nrsmd = params.nrsmd;			
		for (int i = 0; i < nrsmd.colNames.length; i++) {
			String name = nrsmd.colNames[i];
			TupleMetadataType type = nrsmd.types[i];
				
			switch (type) {
				case STRING_TYPE:
					String value = new String((char[]) params.getValue(name));
					// Some DB query languages need to have blanks escaped
					if (!Strings.isNullOrEmpty(replacementBlank) && (value.contains(" "))) {
						value = value.replaceAll(" ", replacementBlank);
					}
					paramStr = paramStr.replaceFirst(placeholder, value);
					break;
				case INTEGER_TYPE:						
					paramStr = paramStr.replaceFirst(placeholder, (params.getValue(name)).toString());
					break;
				default:
					String msg = String.format("Unsupported NTupleMD type: %s", type);
					throw new IllegalArgumentException(msg);
			}
		}
		return paramStr;
	}
	
}
