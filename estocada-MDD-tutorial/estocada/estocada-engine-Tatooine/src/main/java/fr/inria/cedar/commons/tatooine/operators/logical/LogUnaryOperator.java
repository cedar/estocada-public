package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * Class that represents a logical operator that has exactly one child.
 * @author Ioana MANOLESCU
 */
public abstract class LogUnaryOperator extends LogOperator {
	private static final Logger log = Logger.getLogger(LogUnaryOperator.class);
	@SuppressWarnings("unused")
	private String name;
	private String ownDetails;	

	public LogUnaryOperator() {
	}

	/** Constructor */
	public LogUnaryOperator(LogOperator child) {
		children = new ArrayList<LogOperator>();
		children.add(child);
	}

	/* Getters and setters */
	
	public LogOperator getChild() {
		return children.get(0);
	}

	public void setChild(LogOperator child) {
		children = new ArrayList<LogOperator>();
		children.add(child);

	}

	@Override
	public String getName() {
		String s = this.getOwnName() + "(" + getChild().getName();
		if (this.getOwnDetails() == null) {
			s = s + ")";
		} else {
			s = s + "," + this.getOwnDetails() + ")";
		}
		name = s;
		return s;
	}	

	@Override
	public int getJoinDepth() {
		return getChild().getJoinDepth();
	}

	/**
	 * @param ownDetails the ownDetails to set
	 */
	public void setOwnDetails(String ownDetails) {
		this.ownDetails = ownDetails;
	}

	/**
	 * @return the ownDetails
	 */
	public String getOwnDetails() {
		return ownDetails;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Makes a deep copy of the current operator
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public abstract LogUnaryOperator deepCopy();

	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailable) {
		int selfNumber = -1;

		if (this.isVisible() || parentNo == -1) {
			selfNumber = firstAvailable;
			sb.append(selfNumber + " [label=\"" + this.getOwnName());
			if (this.getOwnDetails() != null) {
				sb.append(this.getOwnDetails().replace("\"", "\\\""));
			}
			sb.append("\"] ; \n");
			if (parentNo != -1) {
				sb.append(parentNo + " -> " + selfNumber + " ; \n");
			}
			if (getChild() == null) {
				log.info(this.getOwnDetails());
				try {
					throw new Exception("");
				} catch (Exception e) {
					log.error("Exception: ", e);
				}
			}
			int childNumber = getChild().recursiveDotString(sb, selfNumber, (firstAvailable + 1));
			return childNumber;
		} else {
			return getChild().recursiveDotString(sb, parentNo, firstAvailable);
		}
	}

	@Override
	public void recDisplayNRSMD(StringBuffer sb) {
		sb.append(this.getOwnName() + " " + this.getOwnDetails() + " " + this.getNRSMD().toString() + "(");
		getChild().recDisplayNRSMD(sb);
		sb.append(")");
	}

}
