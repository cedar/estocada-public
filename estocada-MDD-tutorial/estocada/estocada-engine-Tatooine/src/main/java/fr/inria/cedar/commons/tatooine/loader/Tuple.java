package fr.inria.cedar.commons.tatooine.loader;

import java.util.Objects;

/**
 * Simple tuple implementation.
 *
 * @author Damian Bursztyn
 *
 * @param <S>
 * @param <T>
 */
public class Tuple<S, T, U> {
	private final S s;
	private final T t;
	private final U u;

	/**
	 * @param s
	 *            first element of the tuple
	 * @param t
	 *            second element of the tuple
	 * @param u
	 *            third element of the tuple
	 */
	public Tuple(final S s, final T t, final U u) {
		this.s = s;
		this.t = t;
		this.u = u;
	}

	/**
	 * Returns the first element of the tuple.
	 *
	 * @return the first element of the tuple
	 */
	public S first() {
		return s;
	}

	/**
	 * Returns the second element of the tuple
	 *
	 * @return the second element of the tuple
	 */
	public T second() {
		return t;
	}

	/**
	 * Returns the third element of the tuple
	 *
	 * @return the third element of the tuple
	 */
	public U third() {
		return u;
	}

	@Override
	public String toString() {
		return "<" + s.toString() + "," + t.toString() + "," + u.toString() + ">";
	}

	@Override
	public int hashCode() {
		return Objects.hash(s, t, u);
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Tuple<?, ?, ?>) && s.equals(((Tuple<?, ?, ?>) o).s) && t.equals(((Tuple<?, ?, ?>) o).t);
	}
}
