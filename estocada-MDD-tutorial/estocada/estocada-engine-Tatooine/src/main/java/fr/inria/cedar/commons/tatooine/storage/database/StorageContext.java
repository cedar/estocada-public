package fr.inria.cedar.commons.tatooine.storage.database;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.DocNtwID;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.loader.multidoc.TreePatternGSR;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;

/**
 * StorageContext is responsible to delegating replication strategies if any. It
 * should called by any client willing to store or retrieve data without knowing
 * about the underlying methods used. There is a unique StorageContext per node.
 *
 * @author Julien LEBLAY
 * @author Spyros ZOUPANOS
 * @author Asterios KATSIFODIMOS
 */
public class StorageContext {
	private static final Logger				log										= Logger
	        .getLogger(StorageContext.class);
	/* Constants */
	// Here we declare the maximum size of data that the buffer can contain (in
	// bytes).
	private static final long				WRAPPED_TUPLES_BUFFER_MAX_SIZE			= Long
	        .parseLong(Parameters.getProperty("wrappedTuplesBufferMaxSize"));
	// The sleeping time of the RMI thread.
	private static final long				RMI_ACCEPT_TUPLES_THREAD_SLEEPING_TIME	= Long
	        .parseLong(Parameters.getProperty("rmiAcceptTuplesThreadSleepingTime"));
	// The sleeping time of the BDB thread.
	private static final long				STORE_DATA_TO_BDB_THREAD_SLEEPING_TIME	= Long
	        .parseLong(Parameters.getProperty("storeDataToBDBThreadSleepingTime"));

	/* Database handlers */
	private Map<String, DatabaseHolder>		dbHolders;

	/**
	 * This is the temporary buffer that holds the tuples given by the method
	 * storeTulpesInDB before storing them at the correct BDB.
	 */
	LinkedList<WrappedTuplesBufferRecord>	wrappedTuplesBuffer;

	/** The current size of the wrappedTuplesBuffer in bytes */
	long									wrappedTuplesBufferCurSize;

	/** The lock of the wrappedTuplesBuffer & wrappedTuplesBufferCurSize */
	Object									wrappedTuplesBufferLock;

	/**
	 * The lock for informTheBootstrapForEmptyBuffer & currentDataStoringRound
	 */
	Object									informTheBootstrapForEmptyBufferLock	= new Object();

	/**
	 * This boolean can be set to true by the bootstrap peer (via RMI) and it
	 * affects the storeDataToBDB thread
	 */
	boolean									informTheBootstrapForEmptyBuffer		= false;

	/**
	 * At which data storing round we are (look at the class 'NodeInformation'
	 * for more information)
	 */
	int										currentDataStoringRound					= -1;

	private DBListener						dbListener;

	public StorageContext() {
		this.wrappedTuplesBuffer = new LinkedList<WrappedTuplesBufferRecord>();
		this.wrappedTuplesBufferCurSize = 0;
		this.wrappedTuplesBufferLock = new Object();
		this.dbHolders = new HashMap<String, DatabaseHolder>();
		dbListener = new DBListener();
		dbListener.open();
	}

	public void addDatabaseHolder(final DatabaseHolder dbHolder) throws TatooineExecutionException {
		if (dbHolders.containsKey(dbHolder.getId())) {
			throw new TatooineExecutionException("A database holder with the same id already exists.");
		}
		dbHolders.put(dbHolder.getId(), dbHolder);
	}

	public DatabaseHolder getDatabaseHolder(final String id) {
		return dbHolders.get(id);
	}

	public Collection<DatabaseHolder> getDatabaseHolders() {
		return dbHolders.values();
	}

	/**
	 * This methods stores locally and remotely, the tuples that correspond to
	 * the given view and documents, on all peers that have been chosen for
	 * hosting replicas. It is executed at the sender side after extracting the
	 * data that need to be materialized.
	 *
	 * @param patGSR
	 *            The view id to which the tuple correspond
	 * @param docId
	 *            The doc id to which the tuple correspond
	 * @param tuples
	 *            The list of tuples to store
	 * @param pattern
	 *            only here for statistics purposes
	 * @throws Exception
	 */
	public void storeTuples(TreePatternGSR gsr, DocNtwID docId, List<NTuple> tuples, final String dbHolderId)
	        throws Exception {
		String paramEmptyDB = Parameters.getProperty("emptyDB");
		Boolean emptyDB = false;
		if (paramEmptyDB != null) {
			emptyDB = Boolean.valueOf(paramEmptyDB);
		}
		resetDB(gsr, emptyDB, dbHolderId);
		log.info("There are " + tuples.size() + " tuples to be saved in DatabaseHolder[" + dbHolderId + "]");
		Packet wrappedTuples = new Packet(docId.toString(), tuples);
		byte[] wrappedTuplesArray = wrappedTuples.toByteArray();

		storeTuples(gsr, wrappedTuplesArray, dbHolderId);

		Catalog.getInstance().add(gsr.getViewName(), gsr, new ViewSizeBundle(wrappedTuplesArray.length, tuples.size()),
		        null);
	}

	/**
	 * This method is executed by the peer that receives a wrappedTuplesArray
	 * and it has to store them in its local database that corresponds to the
	 * given givGSR. Note that in normal workload this method is non blocking
	 * because the data will be stored at a buffer. If the buffer is full, this
	 * method will block until there is the needed space.
	 *
	 * @param givGSR
	 * @param wrapedTuplesArray
	 */
	public synchronized void storeTuples(GSR givGSR, byte[] wrappedTuplesArray, final String dbHolderId) {

		long wrappedTuplesBufferNewSize;

		// Get the actual buffer size and calculate the new size of the buffer
		synchronized (wrappedTuplesBufferLock) {
			wrappedTuplesBufferNewSize = wrappedTuplesBufferCurSize + wrappedTuplesArray.length;
		}

		// Sleep until there is some space in the buffer to put the new data
		while (wrappedTuplesBufferNewSize > WRAPPED_TUPLES_BUFFER_MAX_SIZE) {
			try {
				Thread.sleep(RMI_ACCEPT_TUPLES_THREAD_SLEEPING_TIME);
			} catch (InterruptedException e) {
				log.error("InterruptedException", e);
			}

			// Recalculate the new size of the buffer
			synchronized (wrappedTuplesBufferLock) {
				wrappedTuplesBufferNewSize = wrappedTuplesBufferCurSize + wrappedTuplesArray.length;
			}
		}

		// Now that there is space in the buffer, add the data
		WrappedTuplesBufferRecord recRecord = new WrappedTuplesBufferRecord(givGSR, wrappedTuplesArray, dbHolderId);
		synchronized (wrappedTuplesBufferLock) {
			wrappedTuplesBuffer.add(recRecord);
			wrappedTuplesBufferCurSize += recRecord.wrappedTuplesByteNo;
		}

		// Parameters.logger.info("StorageContext.storeTuplesInDB(): " +
		// NodeInformation.localPID + " received " + wrappedTuplesBuffer.size()
		// + " tuples to store locally.");
	}

	/**
	 * It resets - creates a database.
	 *
	 * @param view
	 *            The pattern is necessary in order to find its name. With the
	 *            name of the view we can find the view's database.
	 * @param emptyDBIfExists
	 *            If this boolean is true, then the database will be emptied if
	 *            it existed. If it is false, we will not empty it if it existed
	 * @return true if the database existed or false if it did not
	 * @throws Exception
	 * @throws TatooineExecutionException
	 * @throws RemoteException
	 */
	public boolean resetDB(GSR view, boolean emptyDBIfExists, final String dbHolderId) throws Exception {
		boolean dbExisted = getDatabaseHolder(dbHolderId).containsDB(view);
		if (!dbExisted) {
			getDatabaseHolder(dbHolderId).open(view);
		} else if (emptyDBIfExists) {
			getDatabaseHolder(dbHolderId).remove(view);
			getDatabaseHolder(dbHolderId).open(view);
		}
		return dbExisted;
	}

	/**
	 * This thread is responsible for storing data in the BDBs. It continuously
	 * reads the buffer wrappedTuplesBuffer and it stores at the appropriate BDB
	 * its elements.
	 */
	private class DBListener extends Thread {
		/**
		 * Default Constructor
		 */
		boolean active = false;

		DBListener() {
			this.setName("DBListener");
			// new ReportingBufferSizes();
		}

		public void open() {
			this.active = true;
			start();
		}

		public void close() {
			this.active = false;
		}

		@Override
		public void run() {
			boolean gotRecord = false;
			WrappedTuplesBufferRecord record = null;
			while (active) {
				synchronized (wrappedTuplesBufferLock) {
					if (!wrappedTuplesBuffer.isEmpty()) {
						record = wrappedTuplesBuffer.removeFirst();
						wrappedTuplesBufferCurSize -= record.wrappedTuplesByteNo;
						gotRecord = true;
					}
				}
				// If we found a record in the buffer
				if (gotRecord) {
					try {
						// Deserialize the tuples
						Packet wrappedTuples = new Packet(record.wrappedTuplesArray);
						// Store the tuples
						getDatabaseHolder(record.dbHolderId).storeTuples(record.destGSR, wrappedTuples);
					} catch (Exception e) {
						log.error("Error: ", e);
					}
					gotRecord = false;
				} else {
					try {
						Thread.sleep(STORE_DATA_TO_BDB_THREAD_SLEEPING_TIME);
					} catch (InterruptedException e) {
						log.error("InterruptedException", e);
					}
				}
			}
		}
	}

	// public DatabaseStats getStats(GSR givGSR, final String dbHolderId)
	// throws Exception {
	// return getDatabaseHolder(dbHolderId).getStats(givGSR);
	// }

	/**
	 * The following class stores the data that arrive during one call of the
	 * storeTulpesInDB method. Objects of this class are stored at the
	 * wrappedTuplesBuffer.
	 */
	private class WrappedTuplesBufferRecord {
		GSR		destGSR;
		byte[]	wrappedTuplesArray;
		long	wrappedTuplesByteNo;
		String	dbHolderId;

		WrappedTuplesBufferRecord(GSR givenGsr, byte[] givenWrappedTuplesArray, String dbHolderId) {
			destGSR = givenGsr;
			wrappedTuplesArray = givenWrappedTuplesArray;
			wrappedTuplesByteNo = givenWrappedTuplesArray.length;
			this.dbHolderId = dbHolderId;
		}
	}

	public void destroy() {
		this.dbListener.close();
	}
}
