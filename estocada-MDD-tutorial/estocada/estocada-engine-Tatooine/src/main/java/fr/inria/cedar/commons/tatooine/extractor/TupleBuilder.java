package fr.inria.cedar.commons.tatooine.extractor;

import java.io.BufferedOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.IDs.IDScheme;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * @author Ioana MANOLESCU
 */
public interface TupleBuilder {
	
	public void produceTuples(ExtractorMatch em, TreePatternNode pn, BufferedOutputStream bos, ArrayList<NTuple> v, TreePattern p);

	/**
	 * This method is aware that we are in a context of building data out of several XAMs,
	 * and that this particular call is made on behalf of the Xam at position i.
	 */
	public void produceTuples(ExtractorMatch em, TreePatternNode pn, BufferedOutputStream bos, ArrayList<NTuple> v, TreePattern p, int i);

	public void setStacksByNodes(HashMap<TreePatternNode, ExtractorMatchStack> hm);
	
	public void setSchemesByNodes(HashMap<TreePatternNode, IDScheme> hm);
	
}