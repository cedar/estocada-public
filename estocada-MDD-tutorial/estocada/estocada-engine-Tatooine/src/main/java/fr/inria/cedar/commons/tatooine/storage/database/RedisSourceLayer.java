package fr.inria.cedar.commons.tatooine.storage.database;

import org.apache.log4j.Logger;

import ucsd.edu.redis.RedisKQL.query.RedisKQL;
import ucsd.edu.redis.RedisKQL.runtime.RedisConnection;
import ucsd.edu.redis.RedisKQL.runtime.RedisKQLEvaluator;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class RedisSourceLayer implements DBSourceLayer {
	
	private static final Logger log = Logger.getLogger(RedisSourceLayer.class);
		
	private GSR view;
	private RedisKQLEvaluator evaluator;
	
	// Note: By default, every new Redis client connection is automatically set to DB = 0
	private int dbNumber = 0;
	
	/* Constructor */
	public RedisSourceLayer(GSR gsr) throws TatooineExecutionException {
		view = gsr;
		evaluator = new RedisKQLEvaluator();
		
		// Resolving database number		
		String dbNumberStr = view.getPropertyValue("dbNumber");
		try {
			if (!Strings.isNullOrEmpty(dbNumberStr)) {
				dbNumber = Integer.valueOf(dbNumberStr);
			}
		} catch (NumberFormatException nfe) {
			String msg = String.format("The database number specified in the catalog is NOT an integer: %s", dbNumberStr);
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
	}
	
	@Override
	public void connect() throws TatooineExecutionException {	
		RedisConnection.setUp(dbNumber);
	}
	
	@Override
	public Object getBatchRecords(Object[] parameters) throws TatooineExecutionException {
		// Check parameters
		int len = parameters.length;	
		if (parameters.length != 1) {
			String msg = String.format("Method getBatchRecords called with %d parameters instead of 1", len);
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		if (!(parameters[0] instanceof RedisKQL)) {
			String msg = "Method getBatchRecords called with non-RedisKQL parameters!";
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}

		// Evaluate query
		RedisKQL query = (RedisKQL) parameters[0];
		return evaluator.evaluateRedisKQLQuery(query);
	}

	@Override
	public void close() throws TatooineExecutionException {
		RedisConnection.close();
	}

	@Override
	public void reset() throws TatooineExecutionException {}
}
