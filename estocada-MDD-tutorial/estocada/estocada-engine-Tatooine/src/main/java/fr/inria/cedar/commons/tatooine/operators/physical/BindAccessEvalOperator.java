package fr.inria.cedar.commons.tatooine.operators.physical;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Naive operator that take two operators left and right 
 * such that the right is a bindaccess. It takes the Ntuple 
 * from the left as parameters for the right and returns 
 * right NTuples.
 *
 * @author Maxime Buron
 */

public class BindAccessEvalOperator extends NIterator {

    private static final long serialVersionUID = 1L;
    
	protected NIterator left;
	protected BindAccess right;

	protected NTuple nextTuple;
	protected boolean nextTupleIsAvailable = false;

	protected boolean isParamsSet = false;

	public BindAccessEvalOperator(NIterator left, BindAccess right) {
        super(left, right);
		this.left = left;
		this.right = right;
		this.nrsmd = right.nrsmd;
	}

	@Override
	public void close() throws TatooineExecutionException {
		this.left.close();
		this.right.close();
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		return new BindAccessEvalOperator(left.copy(), (BindAccess) right.copy());
	}

	@Override
	public String getName() {
		String name = String.format("BindAccessEval(%s, %s)", left.getName(1), right.getName(1));
		return name;
	}

	@Override
	public String getName(int depth) {
		String name = String.format("BindAccessEval(%s, %s)", left.getName(1 + depth), right.getName(1 + depth));
		return name;
	}

	@Override
	public NIterator getNestedIterator(int arg0) throws TatooineExecutionException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		if (!nextTupleIsAvailable) {
			nextTuple = computeNext();
			nextTupleIsAvailable = true;
		}

		return (nextTuple != null);
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		if (nextTupleIsAvailable) {
			nextTupleIsAvailable = false;
			return nextTuple;
		} else {
			nextTuple = computeNext();
			return nextTuple;
		}
	}

	@Override
	public void open() throws TatooineExecutionException {
		this.left.open();
		this.right.open();
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		sb.append(firstAvailableNo + " [label=\"BindAccessEval\", color = " + getColoring() + "] ; \n");

		if (parentNo != -1) {
			sb.append(parentNo + " -> " + firstAvailableNo + " ; \n");
		}

		int childNumber1 = left.recursiveDotString(sb, firstAvailableNo, (firstAvailableNo + 1));
		int childNumber2 = right.recursiveDotString(sb, firstAvailableNo, childNumber1);

		return childNumber2;
	}

	private NTuple computeNext() throws TatooineExecutionException {

		if (isParamsSet && right.hasNext()) {
			NTuple nextTuple = right.next();
			return nextTuple;
		} else if (left.hasNext()) {
			right.setParameters(left.next());
			isParamsSet = true;
			return computeNext();
		} else {
			return null;
		}
	}

	/**
	 * @return the left
	 */
	public NIterator getLeft() {
		return left;
	}

	/**
	 * @return the right
	 */
	public BindAccess getRight() {
		return right;
	}

    public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
}
