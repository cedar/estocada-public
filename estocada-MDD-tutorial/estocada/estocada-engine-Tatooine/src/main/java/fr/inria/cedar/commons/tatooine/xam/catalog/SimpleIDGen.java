package fr.inria.cedar.commons.tatooine.xam.catalog;

import java.io.Serializable;
import java.util.HashMap;

import fr.inria.cedar.commons.tatooine.constants.IDGeneratorType;

/**
 * Class that maintains all the ID generators and that has to be saved on disc to keep all the numbering.
 * Use the versions with 1 string parameter that will be a key in a HashMap that contains the last values of the associated counter. 
 * If no counter exist for a string, then a new one will be created starting from 0.
 * Example:
 *     1) Get the next ID available for a XAM:
 *     myID = SimpleIDGen.generateID(Constants.XAM_KEY);
 *     2) Initialize a new counter associated for "MyString":
 *     myID = SimpleIDGen.generateID("MyString"); // Will return 0 as the first ID and create a new entry in a HashMap MyString, 0
 *     and the next call will return 1, etc..
 *     
 * @author Andrei ARION
 */
public class SimpleIDGen implements Serializable{
	
    /** Serial version number */
	private static final long serialVersionUID = 4749177331974563080L;
	private static long ID = 0;
    private static HashMap<IDGeneratorType, Long> hm;

    /** Static method for generating incremental IDs */
    public static long ID() {
        return ID++;
    }

    public SimpleIDGen() {
        hm = new HashMap<IDGeneratorType, Long>();
    }

    public static void init() {
        if (hm == null) {
        	hm = new HashMap<IDGeneratorType, Long>();
        }
    }
    
    /**
     * Generate an ID given a string key; if there is the first ID with the key will generate 1 
     * else it will generate the next long in the sequence associated with the key
     */
    public final static long generateID(IDGeneratorType key) {
    	if(hm == null) {
            hm = new HashMap<IDGeneratorType, Long>();
        }
        Long i = (Long) hm.remove(key);
        if (i == null) {
            hm.put(key, new Long(1));
            return 1;
        } else {
            Long newVal = new Long(i.longValue() + 1);
            hm.put(key, newVal);
            return newVal.longValue();
        }
    }
}

