package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.xam.Pattern;

/**
 * Abstract class that represents a leaf operator and that should be extended by other leafs.
 * @author Ioana MANOLESCU
 */
public abstract class LogLeafOperator extends LogOperator implements Cloneable {

	public Pattern pat;
	public StorageReference ref;

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		int selfNumber = -1;
		if (isVisible()) {
			selfNumber = firstAvailableNo;

			sb.append(selfNumber + " [label=\"" + this.getOwnName() + "\"] ; \n");
			if (parentNo != -1) {
				sb.append(parentNo + " -> " + selfNumber + "\n");
			}
			return (firstAvailableNo + 1);
		} else {
			return firstAvailableNo;
		}
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	public void recDisplayNRSMD(StringBuffer sb) {
		sb.append(this.getOwnName() + " " + this.getNRSMD().toString());
	}

}