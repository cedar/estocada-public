package fr.inria.cedar.commons.tatooine.constants;

/**
 * Enumerated type representing the possible types of pattern that we can have in
 * the system in order to be properly drawn.
 * @author Jesus CAMACHO RODRIGUEZ
 * @created 21/04/2010
 */
public enum PatternType {
	COMPULSORY,
	COLLABORATIVE,
	SELFISH,
	CONTENT,
	QUERY;
	
	/**
	 * Get foreground color for representing this kind of pattern.
	 * @return the RGB code of the foreground color
	 */
	public String getForeground() {
		if (this == COMPULSORY)
			return "#ff9999";
		else if (this == COLLABORATIVE)
			return "#99ff99";
		else if (this == SELFISH)
			return "#ff99cc";
		else if (this == CONTENT)
			return "#99ccff";
		else if (this == QUERY)
			return "#ffcc33";
		else
			return null;
	}
	
	/**
	 * Get background color for representing this kind of pattern.
	 * @return the RGB code of the background color
	 */
	public String getBackground() {
		if (this == COMPULSORY)
			return "#ffcfbf";
		else if (this == COLLABORATIVE)
			return "#cfffbf";
		else if (this == SELFISH)
			return "#ffbfef";
		else if (this == CONTENT)
			return "#bfefff";
		else if (this == QUERY)
			return "#ffe7a0";
		else
			return null;
	}
}
