package fr.inria.cedar.commons.tatooine.loader;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.inria.cedar.commons.tatooine.NRSMD;

/**
 *
 * ViewSchema Class that holds the schema of a materialized view
 *
 * @author ranaalotaibi
 *
 */
public class ViewSchema {

    private final String pathSummaryFileName;
    private final NRSMD nrsmd;
    private final Map<String, Integer> columnNamedToIndexMapping;
    private final Map<Integer, String> reverseColumnNamedToIndexMapping;

    public ViewSchema(final NRSMD nrsmd, final Map<String, Integer> columnNamedToIndexMapping) {
        this.nrsmd = nrsmd;
        this.columnNamedToIndexMapping = columnNamedToIndexMapping;
        reverseColumnNamedToIndexMapping = new HashMap<Integer, String>();
        for (final Entry<String, Integer> entry : columnNamedToIndexMapping.entrySet()) {
            reverseColumnNamedToIndexMapping.put(entry.getValue(), entry.getKey());
        }
        pathSummaryFileName = null;
    }

    public ViewSchema(final NRSMD nrsmd, final Map<String, Integer> columnNamedToIndexMapping,
            final String pathSummaryFileName) {
        this.nrsmd = nrsmd;
        this.pathSummaryFileName = pathSummaryFileName;
        this.columnNamedToIndexMapping = columnNamedToIndexMapping;
        reverseColumnNamedToIndexMapping = new HashMap<Integer, String>();
    }

    /**
     * Get View Metadata (NRSMD)
     *
     * @return NRSMD TODO : To be implemented
     *
     */
    public NRSMD getNRSMD() {
        return nrsmd;
    }

    public Map<String, Integer> getColNameToIndexMapping() {
        return columnNamedToIndexMapping;
    }

    public Map<Integer, String> getReverseColNameToIndexMapping() {
        return reverseColumnNamedToIndexMapping;
    }
}
