package fr.inria.cedar.commons.tatooine.operators.logical;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.xparser.XAMParserUtility;

/**
 * Abstract class that represents a logical operator and that will be extended by all of them.
 * @author Ioana MANOLESCU
 */
public abstract class LogOperator implements Cloneable {
	private static final Logger log = Logger.getLogger(LogOperator.class);
	protected ArrayList<LogOperator> children;

	private boolean isDummy;
	private NRSMD nrsmd;

	// An identifier assigned to the LogicalOperator meant for facilitating its use during optimization.
	private int optimizationID;
	private String ownName;
	private boolean visible;

	/* Cost estimation specific variables */
	private double CPU_COST_COEFFICIENT = 1;
	private double IO_COST_COEFFICIENT = 10;
	protected double OUTPUT_TUPLES_OVERHEAD = 1;
	protected double INPUT_TUPLES_OVERHEAD = 1;
	protected TreePattern equivalentPattern = null;

	/* Getters and setters */
	public void setEquivalentPattern(TreePattern pattern) {
		this.equivalentPattern = pattern;
	}

	public TreePattern getEquivalentPattern() {
		return this.equivalentPattern;
	}

	public LogOperator getFatherOf(LogOperator operator) {
		if (this.children != null && this.children.contains(operator)) {
			return this;
		}
		for (LogOperator father : this.getDescendants()) {
			if (father.children != null && father.children.contains(operator)) {
				return father;
			}
		}
		return null;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		log.debug("Trying to clone a " + this.getClass());
		throw new CloneNotSupportedException();
	}

	/**
	 * Returns the list of all the peers that appear in this Logical Plan
	 * @author Aditya SOMANI
	 */
	public abstract int getJoinDepth();

	/* Returns the name on one line */
	public abstract String getName();

	public void recDisplayNRSMD() {
		StringBuffer sb = new StringBuffer();
		this.recDisplayNRSMD(sb);
		log.debug(new String(sb));
	}

	public abstract void recDisplayNRSMD(StringBuffer sb);

	public abstract int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo);

	/**
	 * @author Konstantinos KARANASOS
	 */
	public abstract LogOperator deepCopy();

	public void accept(LogOperatorVisitor visitor){
		throw new IllegalStateException("Not implemented");
	}

	public void draw() {
		this.draw(null);
	}

	public void draw(String givenFileName) {
		StringBuffer sb = new StringBuffer();
		sb.append("digraph  g{\n");
		recursiveDotString(sb, -1, 0);
		try {
			if (givenFileName == null) {
				givenFileName = "" + System.currentTimeMillis();
			}
			String pathName = "";
			int lastSlash = givenFileName.lastIndexOf(File.separator);
			if (lastSlash > 0) {
				pathName = givenFileName.substring(0, lastSlash);
				File dir = new File(pathName);
				dir.mkdirs();
			}

			String fileNameDot = new String(givenFileName + ".dot");
			String fileNamePS = new String(Parameters.getProperty("DOToutputFolder") + File.separator + givenFileName
					+ "-LogPlan.png");

			FileWriter file = new FileWriter(fileNameDot);
			file.write(new String(sb));
			file.write("}\n");
			file.close();
			Runtime r = Runtime.getRuntime();
			String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePS);
			Process p = r.exec(com);
			p.waitFor();
			log.debug("Logical plan drawn.");
			Process p2 = r.exec("rm " + fileNameDot + "\n");
			p2.waitFor();
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
	}

	/**
	 * Compares this LogicalOperator to the specified LogicalOperator using their identifier values.
	 *
	 * @param other - the LogicalOperator to compare with
	 * @return true if the LogicalOperators have the same identifier, false otherwise
	 * @author Alexandra ROATIS
	 */
	public boolean equalsOptID(LogOperator other) {
		return (this.optimizationID == other.optimizationID);
	}

	/**
	 * Traverses the tree of LogicalOperators, rooted in the current LogicalOperator and adds to a given list the names
	 * of the GSRs of the XAMScans that are the leaves of the tree
	 *
	 * @param leafGSRNamesList the list of the names of the GSRs of the leaves of the logical operators tree
	 * @author Konstantinos KARANASOS
	 */
	private void gatherLeafGSRNames(ArrayList<String> leafGSRNamesList) {
		// if it's a XAMScan, add the name of the GSR in the leafGSRList
		if (this instanceof LogXMLViewScan || this instanceof LogDummyXMLViewScan) {
			LogLeafOperator tmpXScan = (LogLeafOperator) this;
			if (tmpXScan.ref == null) {
				leafGSRNamesList.add(tmpXScan.pat.getName());
			} else {
				leafGSRNamesList.add(((GSR) tmpXScan.ref).getPropertyValue("ENVIRONMENT_PARA"));
			}

			return;
		}
		// if it's a UnaryOperator, call recursively for the child
		else if (this instanceof LogUnaryOperator) {
			this.getChildren().get(0).gatherLeafGSRNames(leafGSRNamesList);
		} else if (this instanceof LogBinaryOperator) {
			this.getChildren().get(0).gatherLeafGSRNames(leafGSRNamesList);
			this.getChildren().get(1).gatherLeafGSRNames(leafGSRNamesList);
		}
		// else
		// Parameters.logger.error("gatherLeafGSRs: Encountered unexpected logical operator");
	}

	/** Returns a list of the names of the GSRs that are located in the leafs of this logical plan */
	public ArrayList<String> getLeafGSRs() {
		ArrayList<String> leafGSRNamesList = new ArrayList<String>();
		gatherLeafGSRNames(leafGSRNamesList);
		return leafGSRNamesList;
	}

	/**
	 * Returns a list of the names of the GSRs that are located in the leafs of this logical plan
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public ArrayList<TreePattern> getLeafTreePatterns() {
		ArrayList<TreePattern> leafPats = new ArrayList<TreePattern>();
		gatherLeafPatterns(leafPats);
		return leafPats;
	}

	/**
	 * Traverses the tree of LogicalOperators, rooted in the current LogicalOperator and adds to a given list
	 * TreePatterns of the XAMScans (or DummyScans) that are the leaves of the tree.
	 *
	 * @param patterns the list of the patterns of the GSRs of the leaves of the logical operators tree
	 * @author Asterios KATSIFODIMOS
	 */
	private void gatherLeafPatterns(ArrayList<TreePattern> patterns) {
		// if it's a XAMScan, add the name of the GSR in the leafGSRList
		if (this instanceof LogXMLViewScan || this instanceof LogDummyXMLViewScan) {
			LogLeafOperator tmpXScan = (LogLeafOperator) this;
			if (tmpXScan.ref == null) { // if the XAMScan was created with a pattern and not with a GSR
				patterns.add(((TreePattern) tmpXScan.pat));
			} else {
				GSR gsr = ((GSR) tmpXScan.ref);
				try {
					patterns.add(XAMParserUtility.getTreePatternFromString(gsr.getPropertyValue("XAMString"),
							gsr.getPropertyValue("ENVIRONMENT_PARA")));
				} catch (Exception e) {
					log.warn(e.toString());
				}
			}
			return;
		}
		// if it's a UnaryOperator, call recursively for the child
		else if (this instanceof LogUnaryOperator) {
			this.getChildren().get(0).gatherLeafPatterns(patterns);
		} else if (this instanceof LogBinaryOperator) {
			this.getChildren().get(0).gatherLeafPatterns(patterns);
			this.getChildren().get(1).gatherLeafPatterns(patterns);
		}
		// else
		// Parameters.logger.error("gatherLeafGSRs: Encountered unexpected logical operator");
	}

	/**
	 * Performs a DFS traversal of the tree under this operator and returns the list of operators under this operator.
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public List<LogOperator> getDescendants() {
		LinkedList<LogOperator> operators = new LinkedList<LogOperator>();
		this.getDescendantsRec(operators);
		return operators;
	}

	/**
	 * @author Asterios KATSIFODIMOS
	 */
	private void getDescendantsRec(LinkedList<LogOperator> descs) {
		if (children != null) {
			for (LogOperator child : this.children) {
				child.getDescendantsRec(descs);
				descs.add(child);
			}
		}
	}

	/**
	 * Traverses a tree of LogicalOperators and gathers the Predicates of the Selections and of the Joins, as well as
	 * the LeafOperators (or the Navigates over LeafOperators)
	 *
	 * @param leafOpers the list of LogicalOperators where the Leaves of the tree (either LeafOperators or Navigates)
	 *            are gathered
	 * @param selPredsList the list of SimplePredicates where the predicates of the Selections are gathered
	 * @param joinPredsList the list of SimplePredicates where the predicates of the Joins are gathered
	 * @throws TatooineExecutionException if an unexpected operator is found
	 * @author Konstantinos KARANASOS
	 */
	public void gatherPredsAndLeaves(ArrayList<LogOperator> leafOpers, ArrayList<SimplePredicate> selPredsList,
			ArrayList<SimplePredicate> joinPredsList) throws TatooineExecutionException {
		// if it's a LeafOperator or a Navigate, add it in the leafOpers list
		if ((this instanceof LogLeafOperator) || (this instanceof LogPatternNavigate)) {
			leafOpers.add(this);
			return;
		}
		// if it's a Selection, add the predicate to the list and call recursively for the child
		else if (this instanceof LogSelection) {
			Predicate.addPredToSimplePredList(((LogSelection) this).pred, selPredsList);
			this.getChildren().get(0).gatherPredsAndLeaves(leafOpers, selPredsList, joinPredsList);
		}
		// if it's a Join, add the predicate to the list and call recursively first for the left and then for the right
		// child
		else if (this instanceof LogJoin) {
			Predicate.addPredToSimplePredList(((LogJoin) this).getPredicate(), joinPredsList);
			this.getChildren().get(0).gatherPredsAndLeaves(leafOpers, selPredsList, joinPredsList);
			this.getChildren().get(1).gatherPredsAndLeaves(leafOpers, selPredsList, joinPredsList);
		} else {
			throw new TatooineExecutionException("gatherPredsAndLeaves: Encountered unexpected logical operator");
		}
	}

	/** Returns the children of this operator */
	public ArrayList<LogOperator> getChildren() {
		return children;
	}

	/**
	 * Returns the cardinality assigned to the LogicalOperator
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public abstract double estimatedCardinality();

	/**
	 * Returns the sum of cardinalities of the children of a LogicalOperator
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	protected double inputCardinality() {
		double card = 0;
		if (this.children != null) {
			for (LogOperator child : this.children) {
				card += child.estimatedCardinality();
			}
		}
		return card;
	}

	/**
	 * Returns the total IO cost of this operator and all its descendants
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public abstract double estimatedIOCost();

	/**
	 * Returns the sum of sizes of the children of a LogicalOperator
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	protected final double childrenIOCost() {
		double result = 0;
		if (this.children != null) {
			for (LogOperator child : this.children) {
				result += child.estimatedIOCost();
			}
		}
		return result;
	}

	/**
	 * Returns the total CPU cost of this operator and all its descendants
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public double estimatedCPUCost() {
		/* The cardinality of its children */
		double inputCardinality = inputCardinality();
		double outputCardinality = estimatedCardinality();
		double cost = tupleProcessingCost(INPUT_TUPLES_OVERHEAD, inputCardinality)
				+ processingOverhead(inputCardinality) + tupleProcessingCost(OUTPUT_TUPLES_OVERHEAD, outputCardinality);
		return cost;
	}

	/**
	 * Returns the cost (weighted IO + CPU cost) of evaluating the the whole subtree starting form this operator.
	 * Typically called on the root of the logical plan, to have an estimation of the cost of evaluating it.
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	public final double estimatedCost() {
		double cost = CPU_COST_COEFFICIENT * this.estimatedCPUCost() + IO_COST_COEFFICIENT * this.estimatedIOCost();
		if (cost < 0) {
			log.warn("Cost of operator is less than zero - overflown double?");
		}
		return cost;
	}

	private final double tupleProcessingCost(double coefficient, double numberOfTuples) {
		return Math.ceil(coefficient * numberOfTuples);
	}

	/**
	 * Returns the CPU overhead of the operator that implements the function. For example, a sort operation sould return
	 * nlog(n) where n is the size of the input to the operator. IMPORTANT: Every operator that has a processing cost
	 * other than n should override this method.
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	protected double processingOverhead(double sizeOfInput) {
		return sizeOfInput;
	}

	/**
	 * Passes the sum of cardinalities of the children of a LogicalOperator.
	 *
	 * @author Asterios KATSIFODIMOS
	 */
	protected final double defaultEstimatedCPUCost() {
		double result = 0;
		if (this.children != null) {
			for (LogOperator child : this.children) {
				result += child.estimatedCPUCost();
			}
		}
		return result;
	}

	/**
	 * Returns the identifier assigned to the LogicalOperator.
	 *
	 * @author Alexandra ROATIS
	 */
	public int getOptID() {
		return optimizationID;
	}

	/**
	 * Sets the identifier value for the LogicalOperator.
	 *
	 * @author Alexandra ROATIS
	 */
	public void setOptID(int ID) {
		this.optimizationID = ID;
	}

	public void setTuplesForTuplesScan(ArrayList<NTuple> tuples) {
		if (this instanceof LogMemoryScan) {
			LogMemoryScan ts = (LogMemoryScan) this;
			ts.setTuples(tuples);
		} else if (children != null && !children.isEmpty()) {
			for (LogOperator lop : children) {
				lop.setTuplesForTuplesScan(tuples);
			}
		}
	}

	/* Getters and setters */

	public void setNRSMD(NRSMD nrsmd) {
		this.nrsmd = nrsmd;
	}

	public NRSMD getNRSMD() {
		return nrsmd;
	}

	public void setDummy(boolean isDummy) {
		this.isDummy = isDummy;
	}

	public boolean isDummy() {
		return isDummy;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}

	public String getOwnName() {
		return ownName;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isVisible() {
		return visible;
	}

	public static class TotalViewSizeComparator implements Comparator<LogOperator> {
		boolean ascending = true;

		public TotalViewSizeComparator(boolean ascendingOrder) {
			this.ascending = ascendingOrder;
		}

		@Override
		public int compare(LogOperator arg0, LogOperator arg1) {
			double contr1, contr2;

			if (ascending) {
				contr1 = arg0.estimatedIOCost();
				contr2 = arg1.estimatedIOCost();
			} else {
				contr2 = arg0.estimatedIOCost();
				contr1 = arg1.estimatedIOCost();
			}

			if (contr1 < contr2) {
				return -1;
			} else {
				if (contr1 > contr2) {
					return 1;
				}
			}

			return 0;
		}
	}

}
