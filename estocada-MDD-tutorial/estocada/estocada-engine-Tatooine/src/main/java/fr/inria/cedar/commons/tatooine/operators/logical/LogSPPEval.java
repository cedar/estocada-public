package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical SQL++ query operator.
 * 
 * @author ranaalotaibi
 *
 */
public class LogSPPEval extends LogLeafOperator {

    private final String query;
    private final StorageReference storageReference;

    public LogSPPEval(String query, NRSMD nrsmd, StorageReference storageReference) {
        this.query = query;
        this.storageReference = storageReference;
        this.setOwnName("LogSPPEval");
        this.setVisible(true);
        this.setNRSMD(nrsmd);
    }

    @Override
    public int getJoinDepth() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public String getName() {
        return "LogSPPEval";
    }

    public StorageReference getStorageReference() {
        return storageReference;
    }

    public String getQueryString() {
        return query;
    }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("[");
        str.append("LogSPPEval query: " + query + ",");
        str.append("NRSMD: " + this.getNRSMD());
        str.append("]");
        return str.toString();
    }

    @Override
    public void accept(LogOperatorVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public LogOperator deepCopy() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedCardinality() {
        throw new UnsupportedOperationException("Unsupported");
    }

    @Override
    public double estimatedIOCost() {
        throw new UnsupportedOperationException("Unsupported");
    }

}
