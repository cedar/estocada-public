package fr.inria.cedar.commons.tatooine.operators.logical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;

/**
 * Logical operator for the structural join.
 *
 * @author Ioana MANOLESCU
 */
public class LogStructJoin extends LogBinaryOperator {
	private static final Logger log = Logger.getLogger(LogStructJoin.class);
	private boolean useStackTreeAnc;

	public LogStructJoin(LogOperator left, LogOperator right, Predicate pred) throws TatooineExecutionException {
		super(left, right);
		this.setPredicate(pred);
		this.setOwnName("LogStructJoin");
		this.setVisible(true);
		this.useStackTreeAnc = false;
		// left.nrsmd.display();
		// right.nrsmd.display();
		this.setNRSMD(NRSMD.appendNRSMD(left.getNRSMD(), right.getNRSMD()));
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Object myClone = null;
		try {
			myClone = new LogStructJoin((LogOperator) getLeft().clone(), (LogOperator) getRight().clone(),
					getPredicate());
			((LogStructJoin) myClone).useStackTreeAnc = this.useStackTreeAnc;
		} catch (Exception e) {
			log.error("Exception: ", e);
		}
		return myClone;
	}

	/** Makes a deep copy of the current operator */
	@Override
	public LogStructJoin deepCopy() {

		LogStructJoin copy = null;
		try {
			copy = new LogStructJoin(this.getLeft().deepCopy(), this.getRight().deepCopy(), this.getPredicate());
			copy.setName(this.getName());
		} catch (TatooineExecutionException e) {
			log.error(e.toString());
		}

		copy.setEquivalentPattern(this.getEquivalentPattern());

		return copy;
	}

	/**
	 * True if {@link StackTreeAnc} should be used,
	 * false if {@link StackTreeDesc} should be used.
	 * 
	 * @return a boolean
	 */
	public boolean useStackTreeAnc() {
		return this.useStackTreeAnc;
	}

	/**
	 * True if {@link StackTreeAnc} should be used,
	 * false if {@link StackTreeDesc} should be used.
	 * 
	 * @param useStackTreeAnc the boolean
	 */
	public void setUseStackTreeAnc(boolean useStackTreeAnc) {
		this.useStackTreeAnc = useStackTreeAnc;
	}

	@Override
	public double estimatedCardinality() {
		return getLeft().estimatedCardinality() * getRight().estimatedCardinality();
	}

	@Override
	public double estimatedIOCost() {
		return childrenIOCost();
	}
}
