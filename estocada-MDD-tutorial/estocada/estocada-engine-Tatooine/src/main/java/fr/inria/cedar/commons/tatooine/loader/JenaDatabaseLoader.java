package fr.inria.cedar.commons.tatooine.loader;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolder;
import fr.inria.cedar.commons.tatooine.storage.database.JENADatabaseHolderWithTDB;
import fr.inria.cedar.commons.tatooine.utilities.ViewSizeBundle;
import twitter4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DucCao on 14/10/16.
 */
public class JenaDatabaseLoader extends DatabaseLoader {
	private static final Logger log = Logger.getLogger(JenaDatabaseLoader.class);

	private String[] columns;
	protected JENADatabaseHolder dbHolder = null;
	public static final String MODEL_RDF = "modelRDF";

	public JenaDatabaseLoader(StorageReference storageReference) {
		setStorageReference(storageReference);
	}

	public void setColumns(String[] columns) {
		this.columns = columns;
	}

	@Override
	public boolean load(String filePath) {
		dbHolder = new JENADatabaseHolderWithTDB(filePath);
		dbHolder.setModel((GSR) storageReference);
		return true;
	}

	@Override
	public void deleteAllData(String dataCollectionName) {
		throw new UnsupportedOperationException(getClass().getName() + " doesn't support deleteAllData");
	}

	@Override
	public void deleteCollection(String dataCollectionName) {
		throw new UnsupportedOperationException(getClass().getName() + " doesn't support deleteCollection");
	}

	@Override
	public boolean isDataLoaded(String dataCollectionName) {
		return JENADatabaseHolderWithTDB.hasModelBeenLoaded(dataCollectionName);
	}

	@Override
	public void registerNewCatalogEntry() {
		if (this.columns == null || this.columns.length == 0) {
			String message = "Function setColumns must be called first to register column names for new catalog entry";
			log.error(message);
			throw new IllegalStateException(message);
		}

		String catalogEntry = getCatalogEntryName();

		Catalog catalog = Catalog.getInstance();
		if (!catalog.contains(catalogEntry)) {
			TupleMetadataType[] types = new TupleMetadataType[columns.length];
			for (int i = 0; i < columns.length; ++i) {
				types[i] = TupleMetadataType.STRING_TYPE;
			}

			try {
				NRSMD nrsmd = new NRSMD(columns.length, types, columns, new NRSMD[0]);

				Map<String, Integer> colNameToIndexMap = new HashMap<>();
				int columnIndex = 0;
				for (String columnName : columns) {
					colNameToIndexMap.put(columnName, columnIndex++);
				}
				ViewSchema schema = new ViewSchema(nrsmd, colNameToIndexMap);

				catalog.add(catalogEntry, storageReference, new ViewSizeBundle(5, 5), schema);
			} catch (TatooineExecutionException e) {
				e.printStackTrace();
			}
		}
	}
}
