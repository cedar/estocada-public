package fr.inria.cedar.commons.tatooine.storage.database;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * Interface that defines the connection and querying of data sources used by physical operators.
 * @author Oscar Mendoza
 */
public interface DBSourceLayer {
	
	public void connect() throws TatooineExecutionException;
	
	public Object getBatchRecords(Object[] parameters) throws TatooineExecutionException;

	public void close() throws TatooineExecutionException; 
	
	public void reset() throws TatooineExecutionException;
	
}
