package fr.inria.cedar.commons.tatooine.IDs;

import java.io.Serializable;

/**
 * Class that represents a document ID.
 * 
 * @author Ioana MANOLESCU
 */
public class DocNtwID implements Serializable {

	private static final long serialVersionUID = 1L;

	public String docName;

	public static int docNameCounter = 0;

	public DocNtwID(String givenDocName) {
		this.docName = givenDocName;
	}

	@Override
	public String toString() {
		return new String(this.docName);
	}

	@Override
	public boolean equals(Object givenObj) {
		if (!(givenObj instanceof DocNtwID)) {
			return false;
		}

		if (!this.docName.equals(this.docName)) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		Long hc = new Long(this.docName.hashCode());
		hc = hc % Integer.MAX_VALUE;

		return hc.intValue();
	}
}
