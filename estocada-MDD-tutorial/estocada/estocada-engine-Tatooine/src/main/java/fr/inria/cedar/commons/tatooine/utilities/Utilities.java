package fr.inria.cedar.commons.tatooine.utilities;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.io.NTupleReader;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * Contains several classes that can be used in testing
 * 
 * @author Raphael BONAQUE
 */
public class Utilities {
	
	/**
	 * Implements most of the straightforward methods of NIterator so that it's
	 * easy to write concise classes inheriting this one
	 */
	public static abstract class DummyNIterator extends NIterator {
		
		private static final long serialVersionUID = 1L;
		protected String name;

		
		public void open() throws TatooineExecutionException {
			informPlysPlanMonitorOperStatus(0, getOperatorID());
			informPlysPlanMonitorOperStatus(1, getOperatorID());
		}
		public void close() throws TatooineExecutionException {
			informPlysPlanMonitorOperStatus(2, getOperatorID());
			informPlysPlanMonitorOperStatus(3, getOperatorID());
			informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
		}

		public String getName() { return name; }

		public String getName(int depth) {
			String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
			return ("\n" + spaceForIndent + name);
		}

		public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
			throw new UnsupportedOperationException();
		}
		public NIterator getNestedIterator(int i) throws TatooineExecutionException {
			throw new UnsupportedOperationException();
		}
		
	}
	
	/**
	 * Create a NIterator that will output the collection
	 */
	public static class NIteratorFromCollection extends DummyNIterator {

		private static final long serialVersionUID = -7501488429135415017L;
		private Collection<NTuple> initialCollection;
		private Iterator<NTuple> iterator;
		
		public NIteratorFromCollection(Collection<NTuple> initialCollection) {
			this.initialCollection = initialCollection;
			this.iterator = initialCollection.iterator();
			try {
				nrsmd = initialCollection.iterator().next().nrsmd;
			} catch (NoSuchElementException e) {
				// The collection is empty, therefore we cannot detect NRSMD
			}
		}

		@Override
		public NIterator copy() throws TatooineExecutionException {
			return new NIteratorFromCollection(initialCollection);
		}

		@Override
		public boolean hasNext() throws TatooineExecutionException {
			return iterator.hasNext();
		}

		@Override
		public NTuple next() throws TatooineExecutionException {
			return iterator.next();
		}
	}
	
	/**
	 * The NIterator equivalent of NTupleReader
	 */
	public static class NTupleReaderIterator extends DummyNIterator {
		
		private static final long serialVersionUID = -8724663326131784961L;
		private Queue<NTuple> outputs;
		private String path;
		
		public NTupleReaderIterator(String path) throws TatooineExecutionException {
			this.path = path;
			NTupleReader reader = new NTupleReader(path);
			outputs = new ArrayDeque<NTuple>(reader.read()) ;
			name = "NTupleReaderIterator";
			if (!outputs.isEmpty()) nrsmd = outputs.peek().nrsmd;
		}

		@Override
		public boolean hasNext() throws TatooineExecutionException {
			return !outputs.isEmpty();
		}

		@Override
		public NIterator copy() throws TatooineExecutionException {
			return new NTupleReaderIterator(path);
		}

		@Override
		public NTuple next() throws TatooineExecutionException {
			return outputs.poll();
		}

	}
	
	/**
	 * Implements most of the straightforward methods of BindAccess so that it
	 * is easy to write concise classes inheriting this one
	 */
	public static abstract class DummyBindAccess extends BindAccess {
		
		DummyBindAccess() {
			super();
		}

		private static final long serialVersionUID = 1L;
		protected String name;

		@Override
		public String getName() {return null;}

		@Override
		public String getName(int depth) {
			String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
			return ("\n" + spaceForIndent + name);
		}

		@Override
		public NIterator copy() throws TatooineExecutionException {return null;}

		@Override
		public NIterator getNestedIterator(int i) throws TatooineExecutionException {
			throw new UnsupportedOperationException();
		}

		@Override
		public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void open() throws TatooineExecutionException {
			informPlysPlanMonitorOperStatus(0, getOperatorID());
			informPlysPlanMonitorOperStatus(1, getOperatorID());
		}

		@Override
		public void close() throws TatooineExecutionException {
			informPlysPlanMonitorOperStatus(2, getOperatorID());
			informPlysPlanMonitorOperStatus(3, getOperatorID());
			informPlysPlanMonitorTuplesPassed(numberOfTuples, getOperatorID());
		}
	}
	
	/**
	 * Returns one NTuple containing one integer per binding (whatever the
	 * binding is). The integers start at 0 and go up by one at each binding.
	 */
	public static class CountingBindAccess extends DummyBindAccess {
		
		private static final long serialVersionUID = -4664030717526074388L;
		private int nb = 0;

		public CountingBindAccess() {
			TupleMetadataType[] metadata_array = {TupleMetadataType.INTEGER_TYPE,};
			try {
				nrsmd = new NRSMD(1, metadata_array);
			} catch (TatooineExecutionException e) {}
			name = "CountingBindAccess";
		}
		
		@Override
		public void setParameters(NTuple params) throws IllegalArgumentException {
			nb = Math.abs(nb) + 1;
		}

		@Override
		public boolean hasNext() throws TatooineExecutionException {
			return nb > 0;
		}

		@Override
		public NTuple next() throws TatooineExecutionException {
			if (nb <= 0) return null;
			NTuple outTuple = new NTuple(nrsmd);
			outTuple.integerFields[0] = nb -1;//So it starts at 0
			nb = - nb;
			numberOfTuples++;
			return outTuple;
		}
	}
	
	/**
	 * Echoes its bindings a certain number of times (default 1)
	 */
	public static class EchoBindAccess extends DummyBindAccess {
		
		private static final long serialVersionUID = -8049405834692003772L;
		private NTuple currentBinding;	
		private int currentCounter = 0;
		private int echoNumber = 1;
		
		public EchoBindAccess(NRSMD nrsmd) {
			this.nrsmd = nrsmd;
		}
		/**
		 * @param echoNumber The number of echos per binding, the minimum is 1
		 */
		public EchoBindAccess(NRSMD nrsmd, int echoNumber) {
			this.nrsmd = nrsmd;
			this.echoNumber = echoNumber;
		}

		@Override
		public void setParameters(NTuple params) throws IllegalArgumentException {
			currentBinding = params;
			currentCounter = echoNumber;
		}

		@Override
		public boolean hasNext() throws TatooineExecutionException {
			return currentBinding != null;
		}

		@Override
		public NTuple next() throws TatooineExecutionException {
			NTuple temp = currentBinding;
			currentCounter -= 1;
			if (currentCounter >= 0) numberOfTuples++;
			if (currentCounter <= 0) currentBinding = null;
			return temp;
		}
	}
	
	/**
	 * Accumulates the output of a NIterator into a List and returns it
	 * @warning Doesn't open or close the iterator on its own
	 */
	public static List<NTuple> listFromNIterator(NIterator input) throws TatooineExecutionException {
		List<NTuple> output = new ArrayList<NTuple>();
		while (input.hasNext()) output.add(input.next());
		return output;
	}
	
	public static class NTupleEchoFunction implements NTupleFunction {
		
		private NRSMD nrsmd;
		
		public NTupleEchoFunction(NRSMD nrsmd) {
			this.nrsmd = nrsmd;
		}

		@Override
		public List<NTuple> call(NTuple input) {
			List<NTuple> output = new ArrayList<>();
			output.add(input);
			return output;
		}

		@Override
		public NRSMD getOutputNRSMD() {
			return nrsmd;
		}

		@Override
		public String getName() {
			return "NTupleEchoFunction";
		}
	}
}
