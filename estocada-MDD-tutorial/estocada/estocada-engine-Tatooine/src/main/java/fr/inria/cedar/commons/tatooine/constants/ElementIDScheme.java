package fr.inria.cedar.commons.tatooine.constants;

/**
 * Enumerated type representing all supported kinds of ID schemes.
 * @author Oscar Mendoza
 */
public enum ElementIDScheme {
	PREPOSTDEPTH, PREPOST
}