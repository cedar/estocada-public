package fr.inria.cedar.commons.tatooine.extractor;

import java.io.BufferedOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.CompactDynamicDeweyIDScheme;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.IDs.IDScheme;
import fr.inria.cedar.commons.tatooine.IDs.IDSchemeAssignator;
import fr.inria.cedar.commons.tatooine.xam.PatternEdge;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * This is the basic class for computing tuples out of an XML document. It is
 * used by the MultiDocLoader.
 *
 * As of April 2007, the MultiDataExtractor class is the one that should be
 * called; it loads several XAMs during the same pass over the document.
 * MultiDataExtractor uses a set of DataExtractors, but the MultiDataExtractor
 * takes over the event handling role (it is the one who takes all
 * "beginElement", "endElement" etc. calls).
 *
 * @author Ioana MANOLESCU
 * @author Spyros ZOUPANOS
 *
 */
public final class SinglePatternExtractor extends DefaultHandler {

	/* Constants */
	private static final boolean INSPECT_WORDS = Boolean.parseBoolean(Parameters.getProperty("takeWordsIntoAccount"));

	/** The xam we currently try to match */
	TreePattern currentQP;
	// when the xam extractor is used in a multi-xam context,
	// it serves to know one's own position
	int myPosition;

	/**
	 * Stack in which the integer codes of paths get pushed as we open the
	 * corresponding path nodes.
	 */
	Stack<Integer> currentNodes;

	/**
	 * For each query xam node, the associated match stack.
	 */
	HashMap<TreePatternNode, ExtractorMatchStack> stacksByNodes;

	/**
	 * For each match stack, the associated query xam node.
	 */
	HashMap<ExtractorMatchStack, TreePatternNode> nodesByStacks;

	/**
	 * For each tag, a ArrayList of stacks associated with this tag. If the tag is
	 * "*", then we obtain exactly the set of stacks associated with
	 * "*"-labeled query xam nodes.
	 */
	HashMap<String, ArrayList<ExtractorMatchStack>> stacksByTag;

	/**
	 * The integer code of the last path summary node that was opened (the
	 * current path summary node).
	 */
	int currentPathNo;

	/**
	 * The integer code of the parent of the current node.
	 */
	int parentPathNo;

	/**
	 * The depth of the current node.
	 */
	int currentDepth;

	/**
	 * This is set by the match method. If we want to prune, we try to kill
	 * matches and even query nodes. If we don't want to prune, we just match
	 * every node to every possible path.
	 */
	boolean pruneExistentialNodes;

	/**
	 * To be used to assign identifiers.
	 */
	IDScheme[] schemes;
	boolean[] usefulSchemes;
	HashMap<TreePatternNode, IDScheme> schemesByNode;

	/**
	 * Set to false after the first attribute has been added to an output
	 * tuple; set back to true at the beginning of each tuple.
	 */
	boolean thisIsFirstAttribute = true;

	StringBuffer sb;

	/**
	 * This is the last created match. It is useful for finding out where to
	 * glue content that we just encountered.
	 */
	ExtractorMatch lastCreatedMatch;

	/**
	 * The name of the file where the data is going to be output. This name is
	 * obtained from the current time, and the file is created in the working
	 * directory.
	 */
	String dataFileName;

	/**
	 * Used for writing to the output -- this is used by the relational
	 * builder.
	 */
	BufferedOutputStream bos;

	/**
	 * Compute once and for all the set of stacks whose corresponding xam
	 * nodes need Content. This avoids the need to go and check at every step.
	 */
	ExtractorMatchStack[] stacksNeedingContent;

	/**
	 * Stacks with the prefixes defined for the scope of each element.
	 * One stack per node match. Each map has pairs <prefix, namespaceuri>.
	 */
	Stack<HashMap<String,String>>[] prefixesDefined;

	/**
	 * Stacks with the default namespaces for the scope of each element.
	 * One stack per node match.
	 */
	Stack<String>[] defaultNamespaces;

	/**
	 * The number of stacks which will need content.
	 */
	int numberOfStacksNeedingContent;

	/**
	 * Compute once and for all the set of stacks whose corresponding xam
	 * nodes need Value. This avoids the need to go and check at every step.
	 */
	ExtractorMatchStack[] stacksNeedingValue;

	/**
	 * The number of stacks which will need Value.
	 */
	int numberOfStacksNeedingValue;

	/**
	 * The builder will produce (nested) tuples from the matches extracted from
	 * the document
	 */
	TupleBuilder builder;

	/**
	 * Result vector -- this is used by the nested tuple builder
	 */
	ArrayList<NTuple> vTuples;

	/**
	 * StringBuffer that contains temporary characters.
	 */
	StringBuffer characterSB;


	public HashMap<TreePatternNode, ArrayList<String>> childrenContexts;
	public HashMap<TreePatternNode, ArrayList<String>> currentContexts;

	public SinglePatternExtractor()
	{
		this.characterSB = new StringBuffer();
		this.childrenContexts = new HashMap<TreePatternNode, ArrayList<String>>();
		this.currentContexts = new HashMap<TreePatternNode, ArrayList<String>>();
	}

	public void setPosition(int i){
		this.myPosition = i;
	}

	/**
	 * This method handles a "start element" event. In this context, a "start
	 * element" means a "start path summary node". What must be done:
	 *
	 * First, check if the context is appropriate for creating a match out of
	 * this path summary node. This means: either the element matches an
	 * (almost) root node in the query xam; or, the element matches a query xam
	 * node which has a parent, and the stack corresponding to the parent
	 * contains a currently open match, and that currently open match satisfies
	 * the parent/child resp. ancestor-descendant relationship with the current
	 * match.
	 *
	 * Second, if the context is appropriate, then attach the element to the
	 * correct stack.
	 *
	 * If this stack contained an open match, then connect the open match to
	 * this match.
	 */
	@Override
	public final void startElement(String namespaceuri,	String localName, String qName, Attributes attrs) {
		// creating word elements
		// we only create them if we are currently looking for word elements,
		// that is, if there is an open match on some stack, which needs a word child
		if (INSPECT_WORDS){
			createWordElements();
		}

		//Parameters.logger.debug("DataExtractor:beginElement " + localName);
		sb = new StringBuffer();

		//Parameters.logger.debug("\n<" + localName + ">");

		// processing element IDs
		schemesNotifyBegin(localName);

		// update current path, the stack of nodes, and the current depth
		currentPathNo++;
		currentNodes.push(new Integer(currentPathNo));
		currentDepth++;

		// In this vector, we collect all matches produced for this node.
		ArrayList<ExtractorMatch> matchesForThisNode = new ArrayList<ExtractorMatch>();

		//Parameters.logger.debug("Looking for stacks...");

		// For each stacks that may be interested in this node, try to produce
		// matches
		Iterator<ExtractorMatchStack> it1 = stacksCompatibleWithTag("{" + namespaceuri + "}" + localName);
		while (it1.hasNext()) {
			ExtractorMatchStack s = it1.next();
			if (s.isAttribute) {
				continue;
			}

			// the query xam node for this stack (and match)
			TreePatternNode nodeForThisMatch = nodesByStacks.get(s);

			//Parameters.logger.debug("\nLooking at stack "+ s.hashCode()+ " "+ s.tag+ " for "+ localName+ " "+ currentPathNo);

			// for each of these stacks, we may produce a match,
			// with a link to its parent match,
			// and perhaps to its self-parent (parent in the same stack).

			ExtractorMatchStack sPar = s.parentStack;
			ExtractorMatch parentMatch = null;
			if (sPar != null) {
				parentMatch = sPar.dnop;
				if (parentMatch != null) {
					if (parentMatch.no == currentPathNo) {
						//Parameters.logger.debug(
						//	"This is not the real parent, but the same match just pushed");
						parentMatch = parentMatch.ownParent;
					}
				}
			}

			// we are not sure if we keep this match
			boolean keep = false;

			//Parameters.logger.debug("Finding out whether to keep...");

			if (sPar != null) {
				//Parameters.logger.debug("PARENT STACK NOT NULL");
				if (parentMatch != null) {
					//Parameters.logger.debug("PARENT ENTRY NOT NULL "+ parentMatch.no+ " "+ parentMatch.ownPath);

					// establish if we have a parent-child or ancestor/desc
					// relationship in the query
					// between the current node and its parent

					// first, find the edge connecting the current query node
					// to its parent.
					// n1 is parent of the current query node:
					TreePatternNode n1 = nodesByStacks.get(sPar);
					// check for the edge uniting it with the node for this
					// match
					Iterator<PatternEdge> itEdge = n1.getEdges().iterator();
					boolean foundEdge = false;
					PatternEdge theEdge = null;
					while (itEdge.hasNext() && !foundEdge) {
						theEdge = itEdge.next();
						if (theEdge.n2 == nodeForThisMatch) {
							foundEdge = true;
						} else {
							theEdge = null;
						}
					}

					// now verify if the edge is of parent or ancestor type
					if (theEdge.isParent()) {
						// parent-child
						//Parameters.logger.info("PARENT-CHILD EDGE WITH THE PARENT");
						// is the current node one level below the parent match
						// ?
						//Parameters.logger.debug("Current depth is "+ currentDepth+ " parent match depth is "+ parentMatch.depth);
						if (parentMatch.depth + 1 == currentDepth) {
							//Parameters.logger.info("CURRENT NODE AT RIGHT DEPTH WRT PARENT MATCH");
							keep = true;
						} else {
							//Parameters.logger.info("CURRENT NODE AT WRONG DEPTH WRT PARENT MATCH");
						}
					} else {
						// ancestor-descendant, check descendant is below
						// ancestor
						//Parameters.logger.debug("ANCESTOR-DESCENDENT EDGE WITH THE PARENT");
						if (parentMatch.depth < currentDepth) {
							keep = true;
						}
					}
				} // parentMatch != null
				else {
					//Parameters.logger.debug("MISSING PARENT FOR THIS ELEMENT");
					// here sPar != null and parentMatch == null
					// this element should have a parent, but doesn't,
					// it appears out of its context. Do nothing.

				}
			} // sPar != null
			else
			{
				// sPar == null: this element should not have a parent
				// so, we must only check the ancestor-desc or parent-child
				// story
				//Parameters.logger.debug("THIS ELEMENT MATCHES THE TOP ONE FROM QUERY");
				TreePatternNode theRoot = currentQP.getRoot();
				PatternEdge theEdge = theRoot.getEdges().get(0);
				if (theEdge.isParent())
				{
					// check if the current node appears at depth 1, as
					// required
					if (currentDepth == 1)
					{
						keep = true;
					}
				} else
				{
					// no depth condition applies
					keep = true;
				}

			}

			// now we know that this element appears in the right context,
			// that is, it satisfies all conditions "upwards" (on the path to
			// the root).
			if (keep)
			{
				//Parameters.logger.debug("Keep !");
				// create the match
				ExtractorMatch thisMatch = new ExtractorMatch(currentPathNo, parentMatch, currentDepth, "{" + namespaceuri + "}" + localName);
				//Parameters.logger.debug(
				//	"Created match for "
				//		+ localName
				//		+ " "
				//		+ currentPathNo
				//		+ " for stack "
				//		+ s.hashCode());

				lastCreatedMatch = thisMatch;

				if (parentMatch != null) {
					parentMatch.addChild(thisMatch, s);
					//Parameters.logger.debug("***** Attached "+ thisMatch.ownPath+ " as child of "+ parentMatch.ownPath);
				}

				if (nodeForThisMatch.storesTag() || nodeForThisMatch.selectsTag()) {
					//logger.debug("In Extractor, node " +
					// nodeForThisMatch.toString() +
					//		" needs tag, is named: " + nodeForThisMatch.tag);
					thisMatch.setTag("{" + namespaceuri + "}" + localName);
				} else {
					//Parameters.logger.debug("In Extractor, node " + nodeForThisMatch.toString() + " does not need tag, is named: " +
					// nodeForThisMatch.tag);
				}

				// push this match
				s.push(thisMatch);

				// update the last open match on this stack, if needed
				if (s.dnop != null) {
					s.dnop.addOwnChild(thisMatch);
					thisMatch.ownParent = s.dnop;
					s.dnop = thisMatch;
				} else {
					s.dnop = thisMatch;
				}
				//Parameters.logger.info(
				//	"===> PUSHED " + localName + " at " + currentPathNo);
				matchesForThisNode.add(thisMatch);

				this.currentContexts.put(nodeForThisMatch, this.childrenContexts.get(nodeForThisMatch));
				//Parameters.logger.debug("Added current context of " + currentContextN.size() + " for node " + nodeForThisMatch.tag);
				//Parameters.logger.debug("Match added");

			}
			//else{
			//	Parameters.logger.info("!keep");
			//}
		}

		// If it is not a word element we add it into the Cont stacks.
		// Note that we have made the convention that only the word
		// elements (words that have been converted into elements) can
		// start with _
		if(localName.charAt(0) != '_') {
			addBeginElementToContStacks(namespaceuri, localName, qName, attrs);
		}

		for (int i = 0; i < attrs.getLength(); i++) {
			//logger.debug("Attribute number " + i + " 1");
			beginAttribute(attrs.getURI(i), attrs.getLocalName(i), attrs.getQName(i), attrs.getValue(i));
			endAttribute(attrs.getURI(i), attrs.getLocalName(i), attrs.getQName(i));
			//logger.debug("Attribute number " + i + " 2");
		}

		//Parameters.logger.debug("END OF <"+ localName + ">\n");
	}

	/**
	 * Attributes are processed in a manner very similar to elements. A notable
	 * difference is the fact that nothing is done to propagate the attribute
	 * name and value on other node's contents, because the parent element has
	 * already done that, nor are the attribute name and value propagate on some
	 * node's value, because they do not belong to some other node value.
	 *
	 * @param namespaceuri
	 *            The Namespace URI, or the empty string if the attribute has no
	 *            Namespace URI.
	 * @param localName
	 *            The local name (without prefix).
	 * @param qName
	 *            The qualified name (with prefix).
	 * @param attValue
	 *            The value of the attribute.
	 */
	private final void beginAttribute(String namespaceuri, String localName, String qName, String attValue) {
		//		processing element IDs
		schemesNotifyBegin(localName);

		//		Parameters.logger.info("@" + attName + "=" + attValue);

		// update current path, the stack of nodes, and the current depth
		currentPathNo++;
		currentNodes.push(new Integer(currentPathNo));
		currentDepth++;

		// In this vector, we collect all matches produced for this node.
		ArrayList<ExtractorMatch> matchesForThisNode = new ArrayList<ExtractorMatch>();

		// For each stacks that may be interested in this node, try to produce
		// matches
		Iterator<ExtractorMatchStack> it1 = stacksCompatibleWithTag("{" + namespaceuri + "}" + localName);
		while (it1.hasNext()) {
			ExtractorMatchStack s = it1.next();
			if (!s.isAttribute) {
				continue;
			}

			// the query xam node for this stack (and match)
			TreePatternNode nodeForThisMatch = nodesByStacks.get(s);

			//	Parameters.logger.info("Looking at stack for " + attName + " " + currentPathNo);

			// for each of these stacks, we may produce a match,
			// with a link to its parent match,
			// and perhaps to its self-parent (parent in the same stack).

			ExtractorMatchStack sPar = s.parentStack;
			ExtractorMatch parentMatch = null;
			if (sPar != null) {
				parentMatch = sPar.dnop;
			}

			// we are not sure if we keep this match
			boolean keep = false;

			if (sPar != null) {
				//Parameters.logger.debug("PARENT STACK NOT NULL");
				if (parentMatch != null) {
					//Parameters.logger.debug("PARENT ENTRY NOT NULL");

					// establish if we have a parent-child or ancestor/desc
					// relationship in the query
					// between the current node and its parent

					// first, find the edge connecting the current query node
					// to its parent.
					// n1 is parent of the current query node:
					TreePatternNode n1 = nodesByStacks.get(sPar);
					// check for the edge uniting it with the node for this
					// match
					Iterator<PatternEdge> itEdge = n1.getEdges().iterator();
					boolean foundEdge = false;
					PatternEdge theEdge = null;
					while (itEdge.hasNext() && !foundEdge) {
						theEdge = itEdge.next();
						if (theEdge.n2 == nodeForThisMatch) {
							foundEdge = true;
						} else {
							theEdge = null;
						}
					}

					// now verify if the edge is of parent or ancestor type
					if (theEdge.isParent()) {
						// parent-child
						//Parameters.logger.debug("PARENT-CHILD EDGE WITH THE PARENT");
						// is the current node one level below the parent match
						// ?
						if (parentMatch.depth + 1 == currentDepth) {
							//Parameters.logger.debug("CURRENT NODE AT RIGHT DEPTH WRT PARENT MATCH");
							keep = true;
						} else {
							//Parameters.logger.debug("CURRENT NODE AT WRONG DEPTH WRT PARENT MATCH");
						}
					} else {
						// ancestor-descendant, no depth condition is needed
						//Parameters.logger.debug("ANCESTOR-DESCENDENT EDGE WITH THE PARENT");
						if (parentMatch.depth < currentDepth) {
							keep = true;
						}
					}
				} // parentMatch != null
				else {
					//Parameters.logger.debug("MISSING PARENT FOR THIS ELEMENT");
					// here sPar != null and parentMatch == null
					// this element should have a parent, but doesn't,
					// it appears out of its context. Do nothing.

				}
			} // sPar != null
			else {
				// sPar == null: this element should not have a parent
				// so, we must only check the ancestor-desc or parent-child
				// story
				//Parameters.logger.debug("THIS ELEMENT MATCHES THE TOP ONE FROM QUERY");
				TreePatternNode theRoot = currentQP.getRoot();
				PatternEdge theEdge = theRoot.getEdges().get(0);
				if (theEdge.isParent()) {
					// check if the current node appears at depth 1, as
					// required
					if (currentDepth == 1) {
						keep = true;
					}
				} else {
					// no depth condition applies
					keep = true;
				}
			}

			// now we know that this element appears in the right context,
			// that is, it satisfies all conditions "upwards" (on the path to
			// the root).
			if (keep) { // create the match
				ExtractorMatch thisMatch = new ExtractorMatch(currentPathNo, parentMatch, currentDepth, "{" + namespaceuri + "}" + localName);
				//				logger.info(
				//					"Created match for "
				//						+ attName
				//						+ " number "
				// + currentPathNo +
				//						+ thisMatch.hashCode()
				//						+ " "
				//						+ thisMatch);

				lastCreatedMatch = thisMatch;

				if (parentMatch != null) {
					parentMatch.addChild(thisMatch, s);
				}

				if (nodeForThisMatch.storesTag() || nodeForThisMatch.selectsTag()) {
					thisMatch.setTag("{" + namespaceuri + "}" + localName);
				}

				if (nodeForThisMatch.storesValue() || nodeForThisMatch.selectsValue()) {
					thisMatch.setVal(attValue);
				}

				if (nodeForThisMatch.storesContent()) {
					if(qName.contains(":")) {
						thisMatch.setContent(qName + "=\"" + attValue + "\" xmlns:" + qName.substring(0,qName.indexOf(":")) + "=\"" + namespaceuri + "\"");
					} else if(namespaceuri.compareTo("") != 0) {
						thisMatch.setContent(qName + "=\"" + attValue + "\" xmlns=\"" + namespaceuri + "\"");
					} else {
						thisMatch.setContent(qName + "=\"" + attValue + "\"");
					}
				}
				// push this match
				s.push(thisMatch);
				//Parameters.logger.debug("PUSHED");

				this.currentContexts.put(nodeForThisMatch, this.childrenContexts.get(nodeForThisMatch));
				//Parameters.logger.debug("Added context for node " + nodeForThisMatch.tag);

				// update the last open match on this stack, if needed
				if (s.dnop != null) {
					s.dnop.addOwnChild(thisMatch);
					thisMatch.ownParent = s.dnop;
					s.dnop = thisMatch;
				} else {
					s.dnop = thisMatch;
				}
				//Parameters.logger.debug("===> PUSHED " + attName + " at " + currentPathNo);
				matchesForThisNode.add(thisMatch);

			}
		}
		//Parameters.logger.debug("END BEGIN " + attName + "\n");

		// no need to add attribute to some contents -- it's been added by the
		// element
	}

	@Override
	public final void characters(char ch[], int start, int length)
	{
		if(this.numberOfStacksNeedingContent > 0){
			addTextToContStacks(new String(ch, start, length));
		}
		if (this.numberOfStacksNeedingValue > 0){
			addTextToValueStack(new String(ch, start, length));
		}

		if(INSPECT_WORDS){
			// We store all the text of the parsed document in the following
			// stringBuffer. When there is a startElement or endElement
			// call the contents of the characterSB are split into words and
			// are created "word elements" (look createWordElements method
			// for more information).
			characterSB.append(new String(ch, start, length));
		}
	}

	/**
	 * This method is called by startElement and endElement. The purpose is
	 * to take the contents of the characterSB (which contains the parsed
	 * document text since the last time that createWordElements was called),
	 * remove punctuation points, split it into words and for each word to
	 * create a new element that has the label "_wordTxt".
	 */
	void createWordElements()
	{
		if(characterSB.length() == 0) {
			return;
		}

		String tempStr = characterSB.toString();
		characterSB = new StringBuffer();
		tempStr = tempStr.replaceAll("[^a-zA-Z0-9]", " ");

		StringTokenizer stk = new StringTokenizer(tempStr);
		while(stk.hasMoreTokens())
		{
			String elementName = "_" + stk.nextToken();
			Attributes atr = new AttributesImpl();
			startElement("", elementName, elementName, atr);
			endElement("", elementName, elementName);
		}
	}

	/**
	 * This handles the "end element" events. If this element did not produce a
	 * match, nothing to do (just adjust the current path, depth, stack etc.)
	 * If the element did produce matches, check if each match had encountered
	 * all its required descendants by this time. If a match does not have all
	 * required child matches, mark it as erased.
	 */
	@Override
	public final void endElement(String namespaceuri, String localName,	String qName) {
		// something else
		//Parameters.logger.debug("\n\n</" + localName + ">");

		// creating word elements
		if (INSPECT_WORDS){
			createWordElements();
		}

		schemesNotifyEnd();

		// If it is not a word element we add it into the Cont stacks.
		// Note that we have made the convention that only the word
		// elements (words that have been converted into elements) can
		// start with _
		if(localName.charAt(0) != '_') {
			addEndElementToContStacks(qName);
		}

		//Parameters.logger.debug("Getting ending node number");
		int endingNode = currentNodes.pop().intValue();

		Iterator<ExtractorMatchStack> it1 = stacksCompatibleWithTag("{" + namespaceuri + "}" + localName);
		while (it1.hasNext()) {
			ExtractorMatchStack s = it1.next();
			//Parameters.logger.debug("Checking consequences for end of " +
			// localName +
			// " "+ endingNode + " on compatible stack for " + s.tag);
			if (s.isAttribute) {
				//Parameters.logger.debug("This stack is for an attribute, continue");
				continue;
			}

			// getting the ID for this element
			ElementID currentID = null;
			TreePatternNode pn = this.nodesByStacks.get(s);

			//Parameters.logger.debug("The xam node for the stack is called " +
			// pn.tag);

			if (pn.storesID()) {
				IDScheme sch = this.schemesByNode.get(this.nodesByStacks.get(s));
				currentID = sch.getLastID();
			}

			ExtractorMatch em = s.dnop;
			if (em != null) {
				if (em.no == endingNode) {
					if (pn.storesID()) {
						em.setID(currentID);
					}

					//Parameters.logger.debug("At the top of the stack for " +
					//s.tag +	" there is " + em.no + ", ");// + em.id.toString());

					//Parameters.logger.debug("Stack for this node:");
					//s.display();
					checkPruneAndFillIn(s, endingNode);

					TreePatternNode realPatternRoot = (this.currentQP.getRoot().getEdges().get(0)).n2;
					//logger.debug("Real xam root is called: " +
					// realPatternRoot.tag);
					if (this.stacksByNodes.get(realPatternRoot) == s) {
						//Parameters.logger.debug("Associated to the real xam root we have the stack named "+ s.tag);
						if (!em.erased) {
							//Parameters.logger.debug("Match " + em.tag + " not erased ! Producing tuples");
							//Parameters.logger.info("Starting producing tuples from stack at " + System.currentTimeMillis());
							//s.display();

							if (myPosition >= 0) {
								builder.produceTuples(em, realPatternRoot, bos, vTuples, this.currentQP, this.myPosition);
							}
							else{
								builder.produceTuples(em, realPatternRoot, bos,	vTuples, this.currentQP);
							}
							//Parameters.logger.info("Finished producing tuples from stack at " + System.currentTimeMillis());
							// we cannot pop em, because it may hold the
							// children that another
							// match (a self-ancestor of em) needs.
							// we can pop it iff the self-parent is null.

							if (em.ownParent == null) {
								recursivePop(em, s);

							}

						} else {
							//Parameters.logger.info("Match erased !");
							recursivePop(em, s);
							em = null;
						}
					} else {
						//Parameters.logger.debug("The stack for the element just ended is not the root one");
					}
				} else {
					// em is not the node that just ended, so we don't care.
					//Parameters.logger.debug("This was not the end of the stack !");
					//s.display();
				}
			} else {
				// em is null, the stack is empty
				//Parameters.logger.debug("This stack is empty ");
			}
		}

		//shortenCurrentPath();
		currentDepth--;

		//Parameters.logger.debug("End of </" + localName + ">\n");
	}

	/**
	 * Attributes are processed in a manner very similar to elements. A notable
	 * difference is that no check-and-prune is performed, since attribute nodes
	 * do not have children in the XAM.
	 *
	 * @param namespaceuri
	 *            The Namespace URI, or the empty string if the attribute has no
	 *            Namespace URI.
	 * @param localName
	 *            The local name (without prefix).
	 * @param qName
	 *            The qualified name (with prefix).
	 */
	public final void endAttribute(String namespaceuri, String localName, String qName) {
		// something else
		//Parameters.logger.debug("/@" + attrName);

		schemesNotifyEnd();

		int endingNode = currentNodes.pop().intValue();

		Iterator<ExtractorMatchStack> it1 = stacksCompatibleWithTag("{" + namespaceuri + "}" + localName);
		while (it1.hasNext()) {
			ExtractorMatchStack s = it1.next();
			if (!s.isAttribute) {
				continue;
			}

			ExtractorMatch em = s.dnop;

			/**
			 * getting the ID for this attribute
			 */
			IDScheme sch = this.schemesByNode.get(this.nodesByStacks.get(s));
			if (sch != null) {
				ElementID currentID = sch.getLastID();
				if (em != null) {
					if (em.no == endingNode) {
						em.setID(currentID);
					}
				}
			}
			checkPruneAndFillIn(s, endingNode);

			TreePatternNode realPatternRoot = this.currentQP.getRoot().getEdges().get(0).n2;
			if (this.stacksByNodes.get(realPatternRoot) == s) {
				if (!em.erased) {
					if (myPosition >=0){
						builder.produceTuples(em, realPatternRoot, bos, vTuples, this.currentQP, this.myPosition);
					}
					else{
						builder.produceTuples(em, realPatternRoot, bos, vTuples, this.currentQP);
					}
					if (em.ownParent == null) {
						recursivePop(em, s);
					}
				} else {
					recursivePop(em, s);
					em = null;
				}
			}
		}

		//shortenCurrentPath();
		currentDepth--;

		//this.showMatchesCount();

		//Parameters.logger.debug("END OF /@" + attrName + "\n");
	}

	/**
	 * Checks if the match produced by the current node, on the stack s, has all
	 * required child matches (that is, at least a required child match for each
	 * required child).
	 *
	 * It also fills in the match with the children that it had by transitivity
	 * only
	 *
	 * @param s
	 * @param endingNode
	 */
	private final void checkPruneAndFillIn(ExtractorMatchStack s, int endingNode) {
		//logger.debug(
		//	"\nCheck and prune for "
		//		+ s.tag
		//		+ " "
		//		+ s.hashCode()
		//		+ " and ending node "
		//		+ endingNode);

		ExtractorMatch se = s.findEntry(endingNode);

		if (se != null) {
			//Parameters.logger.debug("Match: " + se.ownPath + " " + se.no);
			// pns is the query node:
			TreePatternNode pns = nodesByStacks.get(s);

			// this means all required children have been matched
			boolean childrenPresent = true;

			boolean correctValue = true;
			// checking value condition first
			if (pns.selectsValue()) {
				String thisVal = pns.getValue();
				if (se.getVal() == null) {
					correctValue = false;
				} else {
					if (se.getVal().compareTo(thisVal) != 0) {
						correctValue = false;
					}
				}
			}
			//Parameters.logger.debug("Correct value here: " + correctValue);
			if (correctValue) {
				Iterator<PatternEdge> iChildren = pns.getEdges().iterator();
				while (iChildren.hasNext()) {
					PatternEdge thisEdge = iChildren.next();

					TreePatternNode nChild = thisEdge.n2;
					// stack for this child
					ExtractorMatchStack sChild = stacksByNodes.get(nChild);
					//Parameters.logger.debug(
					//	"Checking required child "
					//		+ nChild.tag
					//		+ " in stack "
					//		+ sChild.hashCode());
					//Parameters.logger.debug("Child stack: ");
					//sChild.display();

					boolean hasChildInThisStack =
							(findChildInStack(se, sChild,
									thisEdge.isParent()) != null);

					if (!hasChildInThisStack) {
						//Parameters.logger.debug("No child in that stack ");
						childrenPresent = false;
					} else {
						//Parameters.logger.debug("Child is present in that stack");
						boolean thisChildPresent = false;
						ArrayList<ExtractorMatch> o = se.childrenByStack.get(sChild);
						Iterator<ExtractorMatch> itChildrenThisStack = o.iterator();
						while (itChildrenThisStack.hasNext()) {
							ExtractorMatch emChild = itChildrenThisStack.next();
							if (emChild.erased) {
							} else {
								thisChildPresent = true;
							}
						}
						if (!thisChildPresent) {
							childrenPresent = false;
						}
					}
				}
			}
			//Parameters.logger.debug("Children present here: " + childrenPresent);

			// connecting to the last open match in this stack, if necessary
			if (s.dnop == se) {
				s.dnop = se.ownParent;
				//Parameters.logger.debug("ROLLED BACK " + s.tag + ".dnop to " + ((s.dnop == null) ? "null" : (s.dnop.no + " ")));
			}
			// dropping this match if some required child is absent
			if ((!childrenPresent) || (!correctValue)) {
				se.erased = true;
				// dropping also what is underneath
				s.removeEntry(se);

				// clearing context
				if (s.dnop == null){
					//Parameters.logger.debug("Removing context for " + pns.tag);
					this.currentContexts.remove(pns);
				}

				// if se has no ownParent, then we can erase all its
				// descendant matches.
				if (se.ownParent == null) {
					// erase se's children

					Iterator<ArrayList<ExtractorMatch>> itChildren =
							se.childrenByStack
							.values().iterator();
					while (itChildren.hasNext()) {
						Iterator<ExtractorMatch> it4 = itChildren.next().iterator();
						while (it4.hasNext()) {
							ExtractorMatch sChild = it4.next();
							sChild.recErase();
						}
					}

				} else { // se.ownParent is not null
					// go see the children and connect them to the ownParent
					// tell the parent that these are its children
					int cnt = 0;
					Iterator<ArrayList<ExtractorMatch>> itChildren =
							se.childrenByStack
							.values().iterator();
					while (itChildren.hasNext()) {
						cnt += itChildren.next().size();
					}
					if (cnt > 0) {
						itChildren = se.childrenByStack.values().iterator();
						while (itChildren.hasNext()) {
							Iterator<ExtractorMatch> it4 = itChildren.next().iterator();
							while (it4.hasNext()) {
								ExtractorMatch sChild = it4.next();
								ExtractorMatchStack theChildsStack = sChild.theStack;
								TreePatternNode sesNode = this.nodesByStacks
										.get(se.theStack);
								TreePatternNode sChildsNode = this.nodesByStacks.get(theChildsStack);

								// learn if this matches for the child node were supposed to be direct
								// descendants of their parent:
								boolean wasParentEdge = false;
								if (sesNode.getEdges() != null){
									Iterator<PatternEdge> itEdges = sesNode.getEdges().iterator();
									while (itEdges.hasNext()){
										PatternEdge pe = itEdges.next();
										if (pe.n2 == sChildsNode){
											if (pe.isParent()){
												wasParentEdge = true;
											}
										}
									}
									// now establish if it is OK to reconnect the children to their
									// parent's own parent
									if (!wasParentEdge || (se.ownParent.depth + 1 == sChild.depth)){
										//Parameters.logger.debug("=+=+=+=+ Appending " + sChild.ownPath +
										//		" as child of " + se.ownParent.ownPath);
										sChild.ownParent = se.ownParent;
										se.ownParent.addChild(sChild, sChild.theStack);
									}
								}

							}
						}
					}
				}
			}
		}
	}

	/**
	 * The following two methods are used to set up the ID schemes needed for
	 * the xam.
	 *
	 * @param pn
	 *            the root node of the XAM.
	 */
	final void setSchemes(TreePatternNode pn) {
		schemes = new IDScheme[4];
		usefulSchemes = new boolean[4];
		for (int i = 0; i < 4; i++) {
			usefulSchemes[i] = false;
		}
		schemes[0] = IDSchemeAssignator.getIdentityScheme();
		schemes[1] = IDSchemeAssignator.getOrderPreservingScheme();
		schemes[2] = IDSchemeAssignator.getStructuralScheme();
		schemes[3] = IDSchemeAssignator.getUpdateScheme();
		recSetSchemes(pn);
	}

	private final void recSetSchemes(TreePatternNode pn) {
		if (pn.storesID()) {
			if (pn.isIdentityIDType()) {
				schemesByNode.put(pn, schemes[0]);
				usefulSchemes[0] = true;
				// Parameters.logger.info("IDENTITY SCHEME for " + pn.tag);
			}
			if (pn.isOrderIDType()) {
				schemesByNode.put(pn, schemes[1]);
				usefulSchemes[1] = true;
				// Parameters.logger.info("ORDER SCHEME FOR " + pn.tag);
			}
			if (pn.isStructIDType()) {
				schemesByNode.put(pn, schemes[2]);
				usefulSchemes[2] = true;
				// Parameters.logger.info("STRUCT SCHEME FOR " + pn.tag);
			}
			if (pn.isUpdateIDType()) {
				schemesByNode.put(pn, schemes[3]);
				usefulSchemes[3] = true;
				// Parameters.logger.info("UPDATE SCHEME FOR " + pn.tag);
			}
		}
		Iterator<PatternEdge> it = pn.getEdges().iterator();
		while (it.hasNext()) {
			TreePatternNode pChild = it.next().n2;
			recSetSchemes(pChild);
		}
	}

	final void schemesBeginDocument() {
		for (int i = 0; i < 4; i++) {
			if (schemes[i] != null && usefulSchemes[i]) {
				// Parameters.logger.info(schemes[i].getClass().getName() +
				// " is notified of element beginning");
				schemes[i].beginDocument();
			}
		}
	}

	/**
	 * The following two methods allow registering elements begin and end.
	 *
	 */
	private final void schemesNotifyBegin(String tag) {
		for (int i = 0; i < 4; i++) {
			if (schemes[i] != null && usefulSchemes[i]) {
				// Parameters.logger.info(schemes[i].getClass().getName() +
				// " is notified of element beginning");
				if (schemes[i] instanceof CompactDynamicDeweyIDScheme) {
					schemes[i].beginNode(tag);
				} else {
					schemes[i].beginNode();
				}
			}
		}
	}

	private final void schemesNotifyEnd() {
		for (int i = 0; i < 4; i++) {
			if (schemes[i] != null && usefulSchemes[i]) {
				// Parameters.logger.info(schemes[i].getClass().getName() +
				// " is notified of element ending");
				schemes[i].endNode();
			}
		}
	}

	/**
	 * This method creates the stacks that reflect the query xam. It ignores the
	 * symbolic document root. *
	 *
	 * @param pn
	 */
	final void createStacks(TreePatternNode pn) {
		// Parameters.logger.debug("Creating stack for node |" + pn.getTag() +
		// "|");
		Iterator<PatternEdge> it = pn.getEdges().iterator();
		ArrayList<String> rootContext = new ArrayList<String>();
		while (it.hasNext()) {
			PatternEdge e = it.next();
			TreePatternNode onlyChild = e.getN2();
			rootContext.add("{" + onlyChild.getNamespace() + "}"
					+ onlyChild.getTag());
			recCreateStacks(onlyChild, null);
		}
		childrenContexts.put(pn, rootContext);
		// Parameters.logger.debug("Adding a context of " + rootContext.size() +
		// " for " + pn.getTag());
	}

	/**
	 * The actual filling in of the stacks for the query xam.
	 *
	 * @param pn
	 * @param sParent
	 */
	private final void recCreateStacks(TreePatternNode pn,
			ExtractorMatchStack sParent) {
		// System.out.println(
		// "Creating stack for node |"
		// + pn.getTag())
		// + "| and parent stack "
		// + ((sParent == null) ? "null" : sParent.tag));

		ExtractorMatchStack s1 = new ExtractorMatchStack("{"
				+ pn.getNamespace() + "}" + pn.getTag(), pn.isAttribute(),
				sParent);
		// logger.debug(s1.hashCode() + "\n");

		stacksByNodes.put(pn, s1);
		nodesByStacks.put(s1, pn);
		if (pn.storesContent()) {
			this.stacksNeedingContent[numberOfStacksNeedingContent] = s1;
			this.prefixesDefined[numberOfStacksNeedingContent] = new Stack<HashMap<String, String>>();
			this.defaultNamespaces[numberOfStacksNeedingContent] = new Stack<String>();
			this.numberOfStacksNeedingContent++;
		}
		if (pn.storesValue() || pn.selectsValue()) {
			this.stacksNeedingValue[numberOfStacksNeedingValue] = s1;
			this.numberOfStacksNeedingValue++;
		}

		ArrayList<String> childrenContext = new ArrayList<String>();

		ArrayList<ExtractorMatchStack> stacks = stacksByTag.get("{"
				+ pn.getNamespace() + "}" + pn.getTag());
		if (stacks == null) {
			stacks = new ArrayList<ExtractorMatchStack>();
			stacks.add(s1);
			stacksByTag
			.put("{" + pn.getNamespace() + "}" + pn.getTag(), stacks);
		} else {
			stacks.add(s1);
		}

		Iterator<PatternEdge> it = pn.getEdges().iterator();
		while (it.hasNext()) {
			PatternEdge pe = it.next();
			TreePatternNode n2 = pe.n2;
			childrenContext.add("{" + n2.getNamespace() + "}" + n2.getTag());
			recCreateStacks(n2, s1);
		}
		childrenContexts.put(pn, childrenContext);
		// Parameters.logger.debug("Added a context of " +
		// childrenContext.size() + " children context for " + pn.tag);

	}

	/**
	 * Fetches all stacks of a given tag
	 */
	private final Iterator<ExtractorMatchStack> stacksCompatibleWithTag(String s) {
		ArrayList<ExtractorMatchStack> v = stacksByTag.get(s);
		ArrayList<ExtractorMatchStack> stars = stacksByTag.get("{}*");
		if (stars == null) {
			if (v == null) {
				return (new ArrayList<ExtractorMatchStack>()).iterator();
			}
			else {
				return v.iterator();
			}
		}
		else {
			if (v == null) {
				return stars.iterator();
			}
			else{
				// v != null and stars != null
				ArrayList<ExtractorMatchStack> vRes = new ArrayList<ExtractorMatchStack>();
				Iterator<ExtractorMatchStack> it = v.iterator();
				while (it.hasNext()) {
					vRes.add(it.next());
				}
				it = stars.iterator();
				while (it.hasNext()) {
					vRes.add(it.next());
				}
				return vRes.iterator();
			}
		}
	}

	/**
	 * Basically it adds the opening tag for an element to some content nodes.
	 * Two arrays have been created specially for this method: prefixesDefined
	 * and defaultNamespaces. Each position of those arrays stays for one
	 * element that needs content, so every element that needs content will have
	 * an element of each array. <a href="#prefixesDefined">prefixesDefined</a>
	 * is an array of stacks. Each stack will contain
	 * HashMap<prefix,namespaceuri> objects. The top of the stack will represent
	 * the prefixes defined for the scope of the last xml element that we have
	 * examined. When reach the end of an element, we will pop the top element
	 * from the stack. <a href="#defaultNamespaces">defaultNamespaces</a> is an
	 * array of stacks. Each stack will contain String objects. The top of the
	 * stack will represent the default namespace defined for the scope of the
	 * last xml element that we have examined. When reach the end of an element,
	 * we will pop the top element from the stack.
	 *
	 * @param namespaceuri
	 *            The Namespace URI, or the empty string if the element has no
	 *            Namespace URI.
	 * @param localName
	 *            The local name (without prefix).
	 * @param qName
	 *            The qualified name (with prefix).
	 * @param attributes
	 *            The attributes attached to the element. If there are no
	 *            attributes, it shall be an empty Attributes object.
	 */
	private final void addBeginElementToContStacks(String namespaceuri,
			String localName, String qName, Attributes attributes) {
		// System.out.println("TRYING TO ADD ELEMENT");
		for (int i = 0; i < this.numberOfStacksNeedingContent; i++) {
			ExtractorMatch em = this.stacksNeedingContent[i].dnop;
			if (em != null) {
				sb = new StringBuffer();

				HashMap<String, String> prefixes;
				if (this.prefixesDefined[i].size() != 0) {
					prefixes = new HashMap<String, String>(
							this.prefixesDefined[i].peek()); // We create a copy
					// of the
					// prefixes
					// already
					// defined by
					// this
					// element's
					// parent
				} else {
					prefixes = new HashMap<String, String>(); // If there is not
					// any HashMap
					// in the stack,
					// we will
					// create a new
					// prefixes
					// object
				}

				// Variable for knowing if the element defines a new default
				// namespace...
				boolean elementInDefaultNamespace = false;
				if (namespaceuri.compareTo("") == 0) { // Namespace is empty
					// If there is not any default namespace defined for this
					// scope
					if (defaultNamespaces[i].size() == 0
							|| defaultNamespaces[i].peek().compareTo("") == 0) {
						sb.append("<" + localName);
						// Else, there was a default namespace but now there is
						// not anymore, so we have to set it to the new empty
						// value
					} else {
						sb.append("<" + localName + " xmlns=\"\"");
					}
					elementInDefaultNamespace = true;
				} else if (localName.compareTo(qName) == 0) { // It has a
					// default
					// namespace
					// that is not
					// empty
					// The default namespace is not the same one that the
					// default namespace of his parent, so we have to define a
					// new default namespace
					if (defaultNamespaces[i].size() == 0
							|| defaultNamespaces[i].peek().compareTo(
									namespaceuri) != 0) {
						sb.append("<" + qName + " xmlns=\"" + namespaceuri
								+ "\"");
					} else {
						sb.append("<" + qName);
					}
					elementInDefaultNamespace = true;
				} else if (prefixes.containsKey(qName.substring(0,
						qName.indexOf(":")))) { // From here, we know it is a
					// qualified name with a prefix.
					// If the prefix has been redefined by this element.
					if (prefixes.get(qName.substring(0, qName.indexOf(":")))
							.compareTo(namespaceuri) != 0) {
						prefixes.put(qName.substring(0,qName.indexOf(":")), namespaceuri);
						sb.append("<" + qName + " xmlns:" + qName.substring(0,qName.indexOf(":")) + "=\"" + namespaceuri + "\"");
					}
					// Else, the prefix is already defined, so we don't have do
					// no anything with the prefixes HashMap
					else {
						sb.append("<" + qName);
					}
				} else { // If the prefix has not been defined, then we will do
					// it.
					prefixes.put(qName.substring(0, qName.indexOf(":")),
							namespaceuri);
					sb.append("<" + qName + " xmlns:"
							+ qName.substring(0, qName.indexOf(":")) + "=\""
							+ namespaceuri + "\"");
				}

				for (int ii = 0; ii < attributes.getLength(); ii++) { // Now,
					// for
					// each
					// attribute
					// of
					// this
					// element...
					if (attributes.getQName(ii).contains(":")) { // If the
						// qualified
						// name has
						// a
						// prefix...
						// If the prefix has been already defined for this
						// scope...
						if (prefixes.containsKey(attributes.getQName(ii)
								.substring(0,
										attributes.getQName(ii).indexOf(":")))) {
							// If the prefix has been redefined for this
							// scope...
							if (prefixes.get(
									attributes.getQName(ii).substring(
											0,
											attributes.getQName(ii)
											.indexOf(":"))).compareTo(
													attributes.getURI(ii)) != 0) {
								prefixes.put(attributes.getQName(ii).substring(0,attributes.getQName(ii).indexOf(":")), attributes.getURI(ii));
								sb.append(" " + attributes.getQName(ii) + "=\"" + attributes.getValue(ii) + "\" xmlns:" + attributes.getQName(ii).substring(0,attributes.getQName(ii).indexOf(":")) + "=\"" + attributes.getURI(ii) + "\"");
							}
							// Else, we don't have to do anything with the
							// prefixes list and we just add the attribute to
							// the String
							else {
								sb.append(" " + attributes.getQName(ii) + "=\""
										+ attributes.getValue(ii) + "\"");
							}
						}
						// Otherwise, we will have to add it to the prefixes and
						// define it.
						else {
							prefixes.put(
									attributes.getQName(ii).substring(
											0,
											attributes.getQName(ii)
											.indexOf(":")), attributes
											.getURI(ii));
							sb.append(" "
									+ attributes.getQName(ii)
									+ "=\""
									+ attributes.getValue(ii)
									+ "\" xmlns:"
									+ attributes.getQName(ii).substring(
											0,
											attributes.getQName(ii)
											.indexOf(":")) + "=\""
											+ attributes.getURI(ii) + "\"");
						}
					}
					// Else, the attribute does not have a prefix.
					else {
						sb.append(" " + attributes.getQName(ii) + "=\""
								+ attributes.getValue(ii) + "\"");
					}

				}
				sb.append(">");


				this.prefixesDefined[i].push(prefixes); // We put the prefixes
				// that we have defined
				// in the scope of this
				// element in the stack.
				if (elementInDefaultNamespace) {
					this.defaultNamespaces[i].push(namespaceuri);
				} else if (defaultNamespaces[i].empty()) {
					this.defaultNamespaces[i].push("");
				} else {
					this.defaultNamespaces[i].push(this.defaultNamespaces[i]
							.peek());
				}

				while (em != null) {
					em.addToContent(new String(sb));
					// logger.debug("+++ ADDED |" + new String(sb) + "|");
					// em.displayTree();
					em = em.ownParent;
				}
			}
		}
		// logger.debug("ADDED TO STACKS\n");
	}

	/**
	 * Basically it adds the closing tag for an element to some content nodes.
	 *
	 * @param qName
	 *            The qualified name (with prefix).
	 */
	private final void addEndElementToContStacks(String qName) {
		// System.out.println("TRYING TO ADD |" + qName + "|");
		for (int i = 0; i < this.numberOfStacksNeedingContent; i++) {
			ExtractorMatch em = this.stacksNeedingContent[i].dnop;
			if (em != null) {
				this.prefixesDefined[i].pop(); // We remove the prefixes defined
				// in this element's scope
				this.defaultNamespaces[i].pop(); // We remove the default
				// namespace defined in this
				// element's scope
				while (em != null) {
					em.addToContent("</" + qName + ">");
					// logger.debug("+++ ADDED |" + qName + "|");
					//em.displayTree();
					em = em.ownParent;
				}
			}
		}
		// logger.debug("ADDED TO STACKS\n");
	}

	/**
	 * It adds some text to some content nodes.
	 *
	 * @param text
	 */
	private final void addTextToContStacks(String text) {
		// System.out.println("TRYING TO ADD |" + text + "|");
		for (int i = 0; i < this.numberOfStacksNeedingContent; i++) {
			ExtractorMatch em = this.stacksNeedingContent[i].dnop;
			while (em != null) {
				em.addToContent(text);
				// Parameters.logger.info("+++ ADDED |" + text + "|");
				// em.displayTree();
				em = em.ownParent;
			}
		}
		// Parameters.logger.debug("ADDED TO STACKS\n");
	}

	/**
	 * It is called every time the characters method is called. It accumulates
	 * characters in a buffer that will hold the text value of the current
	 * element. The full text value of the current element will only be known
	 * after all the necessary calls to characters, which is at the end of the
	 * element.
	 *
	 * This method only adds text to XAM nodes annotated wit Val.
	 */
	private final void addTextToValueStack(String text) {
		text = text.trim();
		for (int i = 0; i < this.numberOfStacksNeedingValue; i++) {
			// here it looks who wants the value in order to add it
			ExtractorMatchStack ems = this.stacksNeedingValue[i];
			ExtractorMatch em = ems.dnop;
			if (em != null) {
				/*
				 * The following lines are commented because we want the val to
				 * contain the concatenation of all the text descendants of the
				 * node that is annotated with val label.
				 */
				if (!text.equals("")) {
					// //ADD a space after string
					// text = text + " ";
					// TODO Commented out:Mimma should fix this because it
					// creates bug_description
					// There is no reason for adding a space after a string in
					// here
					em.addToVal(text);
				}

			} else {
				// Parameters.logger.debug("Null dnop");
			}
		}
	}

	/**
	 * This method eliminates a match from a stack, taking some care to fix the
	 * dnop if needed. The invariant is that elimination is attempted only for a
	 * match whose own parent was null. Thus, we can afford to set the stack's
	 * dnop to null if the dnop was this match.
	 *
	 * @param s
	 * @param em
	 */
	private void eliminate(ExtractorMatchStack s, ExtractorMatch em) {
		// Parameters.logger.debug("Eliminating " + em.no + " from " + s.tag);
		if (s.dnop == em) {
			s.dnop = null;
		}
		s.removeEntry(em);
	}

	/**
	 * This method eliminates the match em, its children and descendants, in
	 * other stacks and in the same stack, from their respective stacks.
	 *
	 * @param em
	 * @param s
	 */
	private void recursivePop(ExtractorMatch em, ExtractorMatchStack s) {
		// Parameters.logger.debug("Recursive popping of " + em.no + " from " +
		// s.tag);
		if (em.childrenByStack == null) {
			eliminate(s, em);
		}
		if (em.childrenByStack.size() == 0) {
			eliminate(s, em);
		}
		Iterator<ExtractorMatchStack> it = em.childrenByStack.keySet()
				.iterator();
		while (it.hasNext()) {
			ExtractorMatchStack s2 = it.next();
			ArrayList<ExtractorMatch> v2 = em.childrenByStack.get(s2);
			if (v2 != null) {
				Iterator<ExtractorMatch> it3 = v2.iterator();
				while (it3.hasNext()) {
					ExtractorMatch em3 = it3.next();
					recursivePop(em3, s2);
				}
			}
		}
		Iterator<ExtractorMatch> itc = em.ownChildren.iterator();
		while (itc.hasNext()) {
			ExtractorMatch em4 = itc.next();
			recursivePop(em4, s);
		}
	}

	/**
	 * This method attempts to locate children of em in the stack s, either
	 * directly from em's children, or by transitivity from em's own children
	 * (matches in the same stack, corresponding to ancestors of em). The method
	 * returns em's resulting (potentially enhanced) children in the stack s.
	 *
	 * @param em
	 *            The match.
	 * @param s
	 *            The stack for which we attempt to find children.
	 * @return the set of the match's children in the stack, if any were found.
	 */
	private final ArrayList<ExtractorMatch> findChildInStack(ExtractorMatch em,
			ExtractorMatchStack s, boolean isParent) {
		// Parameters.logger.debug("\nFinding children of " + em.no + " for " +
		// s.tag);
		if (em.childrenByStack == null) {
			// Parameters.logger.debug("Ret 1\n");
			return null;
		}
		ArrayList<ExtractorMatch> v = em.childrenByStack.get(s);

		if (em.ownChildren != null) {
			Iterator<ExtractorMatch> it = em.ownChildren.iterator();
			while (it.hasNext()) {
				ExtractorMatch emChild = it.next();
				// cannot be null
				ArrayList<ExtractorMatch> vAux = findChildInStack(emChild, s,
						isParent);
				// Parameters.logger.debug("Back from " + emChild.no + " in " +
				// em.no);
				if (vAux != null) {
					Iterator<ExtractorMatch> iAux = vAux.iterator();
					if (v == null) {
						v = new ArrayList<ExtractorMatch>();
						em.childrenByStack.put(s, v);
						// Parameters.logger.debug("Created new children vector in "
						// + em.no + " for " + s.tag);
					}
					while (iAux.hasNext()) {
						ExtractorMatch o = iAux.next();
						if (v.indexOf(o) == -1) {
							ExtractorMatch emo = o;
							if ((!isParent) || (em.depth + 1 == emo.depth)) {
								v.add(o);
								// Parameters.logger.debug("Added as child of "
								// + em.ownPath + " for stack " + s.tag + " " +
								// s.hashCode() + " " + emo.ownPath);
							}
						}
					}
				}
			}
		}
		// Parameters.logger.debug("Return 2 for " + em.no + "\n");
		return v;
	}
}
