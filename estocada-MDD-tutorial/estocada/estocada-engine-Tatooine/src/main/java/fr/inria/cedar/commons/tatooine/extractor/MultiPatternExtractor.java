package fr.inria.cedar.commons.tatooine.extractor;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.IDScheme;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;
import fr.inria.cedar.commons.tatooine.xam.TreePatternNode;

/**
 * This class allows extracting data from more than one xam. It creates a DataExtractor for each xam and then traverses
 * the XML file, calling all the DataExtractors that may want to be notified about a given parsing event. It is used by
 * the NestedLoader. It is not used by the MultiDocLoader so far.
 *
 * @author Ioana MANOLESCU
 */
public class MultiPatternExtractor extends DefaultHandler {
	private static final Logger log = Logger.getLogger(MultiPatternExtractor.class);
	SinglePatternExtractor[] extractors;
	TreePattern[] treePatterns;
	Reader reader;
	TupleBuilder[] builders;

	int totalElementNo = 0;

	public MultiPatternExtractor() {
	}

	@SuppressWarnings("unchecked")
	final void match(boolean pruneExistentialNodes) {
		for (int i = 0; i < treePatterns.length; i++) {
			extractors[i].currentQP = treePatterns[i];
			extractors[i].pruneExistentialNodes = pruneExistentialNodes;
			extractors[i].stacksByNodes = new HashMap<TreePatternNode, ExtractorMatchStack>();
			extractors[i].nodesByStacks = new HashMap<ExtractorMatchStack, TreePatternNode>();
			extractors[i].currentNodes = new Stack<Integer>();
			extractors[i].stacksByTag = new HashMap<String, ArrayList<ExtractorMatchStack>>();
			extractors[i].schemesByNode = new HashMap<TreePatternNode, IDScheme>();

			extractors[i].stacksNeedingContent = new ExtractorMatchStack[treePatterns[i].getNodesNo()];
			extractors[i].prefixesDefined = new Stack[treePatterns[i].getNodesNo()];
			extractors[i].defaultNamespaces = new Stack[treePatterns[i].getNodesNo()];
			extractors[i].numberOfStacksNeedingContent = 0;
			extractors[i].stacksNeedingValue = new ExtractorMatchStack[treePatterns[i].getNodesNo()];
			extractors[i].numberOfStacksNeedingValue = 0;

			TreePatternNode root = treePatterns[i].getRoot();
			extractors[i].createStacks(root);
			extractors[i].setSchemes(treePatterns[i].getRoot());
			extractors[i].currentContexts.put(root, extractors[i].childrenContexts.get(root));
			extractors[i].schemesBeginDocument();

			builders[i].setSchemesByNodes(extractors[i].schemesByNode);
			builders[i].setStacksByNodes(extractors[i].stacksByNodes);

			extractors[i].currentPathNo = -1;
			extractors[i].parentPathNo = -1;
		}

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XMLReader parser = factory.newSAXParser().getXMLReader();
			parser.setContentHandler(this);
			parser.setErrorHandler(this);
			parser.parse(new InputSource(this.reader));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.error("Error: ", e);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		for (int i = 0; i < treePatterns.length; i++) {
			extractors[i].characters(ch, start, length);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		for (int i = 0; i < treePatterns.length; i++) {
			extractors[i].endElement(uri, localName, qName);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		for (int i = 0; i < treePatterns.length; i++) {
			extractors[i].startElement(uri, localName, qName, atts);
			totalElementNo++;
		}
	}

	public void extractMultiPatternData(String newXMLFileName, TreePattern[] newPatterns, MultiDocBuilder[] builders) {
		try {
			this.reader = new InputStreamReader(new BufferedInputStream(new FileInputStream(newXMLFileName)));
		} catch (FileNotFoundException e) {
			log.error(e.toString(), e);
		}

		extractMultiPatternData(this.reader, newPatterns, builders);

	}

	public void extractMultiPatternData(Reader rdr, TreePattern[] newPatterns, MultiDocBuilder[] builders) {
		this.treePatterns = new TreePattern[newPatterns.length];
		this.builders = new MultiDocBuilder[newPatterns.length];
		this.extractors = new SinglePatternExtractor[newPatterns.length];
		this.reader = rdr;

		for (int i = 0; i < treePatterns.length; i++) {
			this.treePatterns[i] = newPatterns[i];
			// The problem here is what builders to use.
			// If we give a standard NestedBuilder, it will attempt to locally store the tuples,
			// and we don't want that.
			// So, we they should be MultiDocBuilders.
			this.builders[i] = builders[i];
			this.extractors[i] = new SinglePatternExtractor();
			// The following line allows the extractor to know for which position (xam) it works.
			//
			// This is useful because the extractor commands the builder, and the builder delivers
			// the tuples at position i into the loader's internal variable!

			this.extractors[i].setPosition(i);
			this.extractors[i].builder = builders[i];
			this.extractors[i].currentQP = this.treePatterns[i];
			this.extractors[i].sb = new StringBuffer();
			this.extractors[i].dataFileName = (new Long(System.currentTimeMillis())).toString()
					+ Parameters.getProperty("dataFileSuffix");
		}

		// we now actually extract the data:
		match(false);

		try {
			this.reader.close();
		} catch (Exception e) {
			;
		}
	}

}
