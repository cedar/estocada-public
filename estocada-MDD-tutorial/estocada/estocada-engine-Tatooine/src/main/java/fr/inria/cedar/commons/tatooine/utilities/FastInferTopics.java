package fr.inria.cedar.commons.tatooine.utilities;

import cc.mallet.util.*;
import cc.mallet.types.*;
import cc.mallet.topics.*;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;

import java.io.*;
import java.util.regex.*;
import java.nio.charset.Charset;

public class FastInferTopics {

    public static String defaultLineRegex = "^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$";
    private static final int RANDOM_SEED = 2016;

    static CommandOption.String inferencerFilename = new CommandOption.String
            (FastInferTopics.class, "inferencer", "FILENAME", true, null,
                    "A serialized topic inferencer from a trained topic model.\n" +
                            "By default this is null, indicating that no file will be read.", null);

    static CommandOption.String inputFile = new CommandOption.String
            (FastInferTopics.class, "input", "FILENAME", true, null,
                    "The filename from which to read the list of instances\n" +
                            "for which topics should be inferred.  Use - for stdin.  " +
                            "The instances must be FeatureSequence or FeatureSequenceWithBigrams, not FeatureVector", null);

    static CommandOption.String docTopicsFile = new CommandOption.String
            (FastInferTopics.class, "output-doc-topics", "FILENAME", true, null,
                    "The filename in which to write the inferred topic\n" +
                            "proportions per document.  " +
                            "By default this is null, indicating that no file will be written.", null);

    static CommandOption.Double docTopicsThreshold = new CommandOption.Double
            (FastInferTopics.class, "doc-topics-threshold", "DECIMAL", true, 0.0,
                    "When writing topic proportions per document with --output-doc-topics, " +
                            "do not print topics with proportions less than this threshold value.", null);

    static CommandOption.Integer docTopicsMax = new CommandOption.Integer
            (FastInferTopics.class, "doc-topics-max", "INTEGER", true, -1,
                    "When writing topic proportions per document with --output-doc-topics, " +
                            "do not print more than INTEGER number of topics.  "+
                            "A negative value indicates that all topics should be printed.", null);

    static CommandOption.Integer numIterations = new CommandOption.Integer
            (FastInferTopics.class, "num-iterations", "INTEGER", true, 100,
                    "The number of iterations of Gibbs sampling.", null);

    static CommandOption.Integer sampleInterval = new CommandOption.Integer
            (FastInferTopics.class, "sample-interval", "INTEGER", true, 10,
                    "The number of iterations between saved samples.", null);

    static CommandOption.Integer burnInIterations = new CommandOption.Integer
            (FastInferTopics.class, "burn-in", "INTEGER", true, 10,
                    "The number of iterations before the first sample is saved.", null);

    static CommandOption.Integer randomSeed = new CommandOption.Integer
            (FastInferTopics.class, "random-seed", "INTEGER", true, 0,
                    "The random seed for the Gibbs sampler.  Default is 0, which will use the clock.", null);

    static CommandOption.File usePipeFromVectorsFile = new CommandOption.File(FastInferTopics.class, "use-pipe-from", "FILE", true, new File("text.vectors"),
            "Use the pipe and alphabets from a previously created vectors file.\n" +
                    "   Allows the creation, for example, of a test set of vectors that are\n" +
                    "   compatible with a previously created set of training vectors", null);

    static CommandOption.String encoding = new CommandOption.String(FastInferTopics.class, "encoding", "STRING", true, Charset.defaultCharset().displayName(),
            "Character encoding for input file", null);

    static CommandOption.String lineRegex = new CommandOption.String(FastInferTopics.class, "line-regex", "REGEX", true, defaultLineRegex,
            "Regular expression containing regex-groups for label, name and data.", null);

    static CommandOption.Integer dataOption = new CommandOption.Integer(FastInferTopics.class, "data", "INTEGER", true, 3,
            "The index of the group containing the data.", null);

    static CommandOption.Integer labelOption = new CommandOption.Integer(FastInferTopics.class, "label", "INTEGER", true, 2,
            "The index of the group containing the label string.\n" +
                    "   Use 0 to indicate that the label field is not used.", null);

    static CommandOption.Integer nameOption = new CommandOption.Integer(FastInferTopics.class, "name", "INTEGER", true, 1,
            "The index of the group containing the instance name.\n" +
                    "   Use 0 to indicate that the name field is not used.", null);

    public Pipe instancePipe;
    public TopicInferencer inferencer;

    public void preload(File trainDataFileName, String inferencerFileName) throws Exception {
        InstanceList previousInstanceList = InstanceList.load (trainDataFileName);
        instancePipe = previousInstanceList.getPipe();

        inferencer = TopicInferencer.read(new File(inferencerFileName));
    }

    public void predict(String inputFile, int numIterations, String topicDistributionsFileName) throws Exception {
        // Create the instance list and open the input file
        InstanceList instances = new InstanceList (instancePipe);
        Reader fileReader = new InputStreamReader(new FileInputStream(inputFile),
                Charset.defaultCharset().displayName());

        // Read instances from the file
        // instances.addThruPipe (new CsvIterator (fileReader, Pattern.compile(defaultLineRegex),
        // 										dataOption.value, labelOption.value, nameOption.value));
        instances.addThruPipe (new CsvIterator (fileReader, Pattern.compile(defaultLineRegex),
                3, 2, 1));

        // Inference
        try {
            if (randomSeed.value() != 0) {
                inferencer.setRandomSeed(RANDOM_SEED);
            }

            inferencer.writeInferredDistributions(instances, new File(topicDistributionsFileName),
                    numIterations, 10,
                    10,
                    0.0, -1);


        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }
}

