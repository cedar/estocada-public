package fr.inria.cedar.commons.tatooine.xam;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.PrintingLevel;
import fr.inria.cedar.commons.tatooine.constants.ValueJoinAttribute;

/**
 * This class models the tree patterns.
 *
 * @author Ioana MANOLESCU
 * @author Jesus CAMACHO RODRIGUEZ
 * @author Stamatis ZAMPETAKIS
 *
 * @created 13/06/2005
 */
public final class TreePattern extends Pattern implements Comparable<TreePattern> {
	private static final Logger log = Logger.getLogger(TreePattern.class);
	
	/** Universal version identifier for the TreePattern class */
	private static final long serialVersionUID = -351181383935058541L;

	private boolean ordered;

	protected TreePatternNode root;

	private String xpathString;
	
	// A set of tags that this tree pattern contains
	private List<String> orderedTagSet = null;

	public TreePattern(long patternID) {
		super(patternID);
	}

	public TreePattern(TreePatternNode r, boolean ordered) {
		super();
		this.root = r;
		this.ordered = ordered;
	}

	/** Returns the XamNode which is the "top" node of this XAM */
	public TreePatternNode getRoot() {
		return root;
	}

	/** Sets a new root */
	public void setRoot(TreePatternNode root) {
		this.root = root;
	}

	/**
	 * The root of every TreePattern is a TreePatternNode with an empty tag and nodeCode=-1.
	 * This method returns the "actual" root of the TreePattern, which is the child of its root.
	 * @return the actual root of the current TreePattern
	 * @author Konstantinos KARANASOS
	 */
	public TreePatternNode getActualRoot() {
		if(root.getEdges() == null || root.getEdges().isEmpty()) {
			return null;
		}
		return root.getEdges().get(0).getN2();
	}


	public String getXPathString(){
		return xpathString;
	}

	public void setXPathString(String xpath){
		xpathString = xpath;
	}

	public boolean isOrdered() {
		return ordered;
	}

	public void setOrdered(boolean ordered) {
		this.ordered = ordered;
	}

	/** Counts the number of nodes in this tree pattern. Does not count the root. */
	@Override
	public int getNodesNo() {
		return (root.getNumberOfNodes() - 1);
	}

	/** Counts the number of nodes in this subtree that return an ID, CONT or VAL. Counts the root. */
	public int getRetNodesNo() {
		return (root.getNumberOfReturningNodes());
	}

	/** Counts the number of nodes in this subtree that return an ID, CONT, VAL. Counts the root. */
	public int getValContRetNodesNo() {
		return (root.getNumberOfValContReturningNodes());
	}

	/** Gets the complete list of nodes that belong to this tree pattern */
	public LinkedList<TreePatternNode> getNodes() {
		if (root != null) {
			return root.getNodes();
		}
		return null;
	}

	/**
	 * Creates a map containing as key the node code of each pattern node and as value
	 * the corresponding pattern node.
	 */
	public HashMap<Integer,TreePatternNode> createCodeToNodeMap() {
		HashMap<Integer, TreePatternNode> codeToNodeMap = new HashMap<Integer, TreePatternNode>();

		// Traverse the tree pattern and populate the returnAtts and nodeLabels
		Stack<TreePatternNode> stack = new Stack<TreePatternNode>();
		TreePatternNode actualRoot = this.getActualRoot();
		stack.push(actualRoot);
		codeToNodeMap.put(actualRoot.getNodeCode(), actualRoot);

		while (!stack.empty()){
			TreePatternNode node = stack.pop();

			for (PatternEdge edge: node.getEdges()) {
				stack.push(edge.n2);
				codeToNodeMap.put(edge.n2.getNodeCode(), edge.n2);
			}
		}

		return codeToNodeMap;
	}

	/**
	 * Obtains the simplify representation of a tree pattern. 
	 * It is equivalent to calling the method {@link #toString(PrintingLevel.EXTENDED)}.
	 */
	@Override
	public String toString(){
		return (this.root.treeToString(PrintingLevel.EXTENDED));
	}

	/**
	 * Obtains the representation of a tree pattern.
	 * We can specify several level of details while transforming the tree into a string.
	 * The levels of detail are defined in the {@link PrintingLevel} enumeration.
	 */
	@Override
	public String toString(PrintingLevel level){
		return (this.root.treeToString(level));
	}

	/** Displays the simplify representation of a tree pattern */
	@Override
	public void display() {
		log.info("#>#> " + this.root.treeToString(PrintingLevel.SIMPLIFY));
	}

	/** Get a string ready for using in DOT and get an image representation of the pattern */
	public String getDotString(String givenFilename, String backgroundColor, String foregroundColor) {
		StringBuffer sb = new StringBuffer();
		sb.append("graph  " + givenFilename + "{\n");
		sb.append("node [fontname=\"Lucida Grande\" fontsize=18 color=\"white\"]\n");
		this.root.drawTree(sb, backgroundColor, foregroundColor);
		sb.append("}\n");
		return new String(sb);
	}

	/** Creates an image representation of the pattern */
	@Override
	public void draw(String givenFilename, boolean query, String backgroundColor, String foregroundColor)
	{
		String fileName;
		Calendar cal = new GregorianCalendar();
		if(givenFilename == null) {
			if(query) {
				fileName = "xam-" + "-" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + "-Query";
			} else {
				fileName = "xam-" + "-" + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
			}
		} else {
			if(query) {
				fileName = givenFilename + "-Query";
			} else {
				fileName = givenFilename;
			}
		}

		String sb = getDotString(fileName, backgroundColor, foregroundColor);

		try
		{
			String fileNameDot =  new String(fileName + ".dot");
			String fileNamePNG;
			if(fileName.contains("/")) {
				fileNamePNG = new String(fileName + ".png");
			} else {
				fileNamePNG = new String( Parameters.getProperty("DOToutputFolder") + File.separator + fileName + ".png");
			}
			FileWriter file = new FileWriter(fileNameDot);

			// writing the .dot file to disk
			file.write(sb);
			file.close();
			// calling GraphViz
			Runtime r = Runtime.getRuntime();
			String com = new String("dot -Tpng " + fileNameDot + " -o " + fileNamePNG);
			Process p = r.exec(com);
			p.waitFor();
			// removing the .dot file
			Process p2=r.exec("rm "+fileNameDot+"\n");
			p2.waitFor();
		}
		catch (Exception e) {
			log.error("Exception: ", e);
		}
	}

	public final TreePattern deepCopy(){
		TreePattern p = new TreePattern(this.root.deepCopy(), this.ordered);
		p.name = this.name;
		return p;
	}

	/** Gives fresh numbers to all nodes in a tree pattern */
	public void renumberNodes() {
		Stack<TreePatternNode> st = new Stack<TreePatternNode>();
		st.push(this.root);
		while (!st.empty()){
			TreePatternNode pn = st.pop();
			if (pn.getNodeCode() == -1){
				// Nothing...
			}
			else{
				pn.setNodeCode(TreePatternNode.globalNodeCounter.getAndIncrement());
			}
			Iterator<PatternEdge> ie = pn.getEdges().iterator();
			while (ie.hasNext()) {
				st.push(ie.next().n2);
			}
		}
	}

	/** Same as {@link #renumberNodes()}, but it starts the numbering from the localCounter */
	public void renumberNodes(int localCounter) {
		Stack<TreePatternNode> st = new Stack<TreePatternNode>();
		st.push(this.root);
		while (!st.empty()){
			TreePatternNode pn = st.pop();
			if (pn.getNodeCode() == -1){
				// Nothing...
			}
			else{
				pn.setNodeCode(localCounter);
				localCounter++;
			}
			Iterator<PatternEdge> ie = pn.getEdges().iterator();
			while (ie.hasNext()) {
				st.push(ie.next().n2);
			}
		}
	}

	/**
	 * Calls {@link #renumberNodes(int)}, using as localCounter the nodeCode of the first node of the given Pattern.
	 * However, if the nodeCode of the first nodes (immediately below the root) of the two patterns are the same, the
	 * renumbering is not performed (so as to avoid unnecessary renumbering).
	 * @author Konstantinos KARANASOS
	 */
	public void renumberNodes(TreePattern patToUse) {
		int patToUseFirstNodeCode = patToUse.getRoot().getEdges().get(0).n2.getNodeCode();
		if (this.getRoot().getEdges().get(0).n2.getNodeCode() != patToUseFirstNodeCode) {
			renumberNodes(patToUse.getRoot().getEdges().get(0).n2.getNodeCode());
		}
	}

	/** Transforms the tree pattern its equivalent in a XAM file */
	@Override
	public String toXAMString(){
		StringBuffer nodeBuffer = new StringBuffer();
		nodeBuffer.append(this.ordered ? "o" : "");
		PatternEdge rootEdge = (this.root.getEdges().get(0));
		nodeBuffer.append(rootEdge.isParent() ? "/\n" : "");
		StringBuffer edgeBuffer = new StringBuffer();
		TreePatternNode realRoot = rootEdge.n2;
		realRoot.toXAMString(nodeBuffer, edgeBuffer, 0, this.getActualRoot().getNodeCode());
		return new String(nodeBuffer) + "\n;\n" + new String(edgeBuffer);
	}

	/** Returns true if the tree pattern is linear */
	public boolean isLinear(){
		for(TreePatternNode node: this.getNodes()){
			if(node.getChildrenList().size()>1){
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns a XAM that contains all my nodes except the hierarchy under the parameter N.
	 * In other words: all n ancestors and their descendants except for those that are also the descendants of N.
	 */
	public TreePattern getCopyAbove(TreePatternNode n){
		TreePattern p = this.deepCopy();
		TreePatternNode copyOfN = p.root.locate(n.getNodeCode());
		assert (copyOfN != null) : "Erroneous call to getCopyAbove";
		copyOfN.cleanEdges();
		return p;
	}

	public TreePattern getPathAbove(TreePatternNode n1) {
		TreePattern p = this.deepCopy();
		p.root.pruneAllButPathTo(n1);
		return p;
	}

	/**
	 * Returns a copy of this pattern expanded with the attributes introduced in the parameters.
	 * @param nodesToUpdate a map with node codes as keys (unique identifiers) and a list of
	 * attributes that we would like those nodes to have
	 */
	protected TreePattern expandPattern(Map<Integer,ArrayList<ValueJoinAttribute>> nodesToUpdate) {
		TreePattern patternCopy = this.deepCopy();
		patternCopy.root.expandPattern(nodesToUpdate);
		return patternCopy;
	}

	/** The same as expandPattern but affects the current Pattern and not a copy of it */
	protected TreePattern expandPatternNoCopy(Map<Integer,ArrayList<ValueJoinAttribute>> nodesToUpdate) {
		this.root.expandPattern(nodesToUpdate);
		return this;
	}

	public ArrayList<String> getColumnsNames() {
		LinkedList<TreePatternNode> nodes = this.getNodes();
		LinkedList<String> list = new LinkedList<String>();
		list.add("Document ID");
		if(Boolean.parseBoolean(Parameters.getProperty("updatesAlgorithm.storeDerivationCount"))) {
			list.add("Derivation count");
		}
		for(TreePatternNode node:nodes) {
			list.addAll(node.getColumnsName());
		}
		return new ArrayList<String>(list);
	}

	/**
	 * Returns an {@link ArrayList} containing 3 strings each representing a transcription of the TreePattern into an aquax expression.
	 * The first string will contain the only the needed nodes from the pattern.
	 * The second string will contain all the nodes in the pattern.
	 * The third string will have the needed nodes as mandatory and the others as optional.
	 * @return an {@link ArrayList} containing 3 {@link String} objects with the significance described above.
	 */
	public ArrayList<String> convertToAquax() {

		// The list that will contain the aquax form for the pattern
		ArrayList<String> aquaxTranslations = new ArrayList<String>();
		LinkedList<TreePatternNode> optionalNodes = this.getNodes();
		LinkedList<TreePatternNode> neededNodes = new LinkedList<TreePatternNode>();

		for(TreePatternNode node : optionalNodes) {
			if (node.nodeStoresSomething()) {
				neededNodes.add(node);
			}
		}

		optionalNodes.removeAll(neededNodes);

		// Creating the first string that contains all the needed nodes
		StringBuffer sb = new StringBuffer(), sb2 = new StringBuffer();

		for (TreePatternNode n : neededNodes) {
			if (n.getParentEdge().n1.getNodeCode() == -1) {
				sb.append(n.getNodeCode() + ":" + 0 + ":m " + n.getTag() + " ");
			} else {
				sb.append(n.getNodeCode() + ":" + n.getParentEdge().n1.getNodeCode() + ":m " + n.getTag() + " ");
			}
		}

		aquaxTranslations.add(sb.toString());

		sb2.append(sb);

		// Second and third strings containing both the needed and the other nodes, in one as mandatory, in the other as optional
		for (TreePatternNode n : optionalNodes) {
			if (n.getParentEdge().n1.getNodeCode() == -1) {
				sb.append(n.getNodeCode() + ":" + 0 + ":m " + n.getTag() + " ");
				sb2.append(n.getNodeCode() + ":" + 0 + ":o " + n.getTag() + " ");
			}
			else {
				sb.append(n.getNodeCode() + ":" + n.getParentEdge().n1.getNodeCode() + ":m " + n.getTag() + " ");
				sb2.append(n.getNodeCode() + ":" + n.getParentEdge().n1.getNodeCode() + ":o " + n.getTag() + " ");
			}
		}

		aquaxTranslations.add(sb.toString());
		aquaxTranslations.add(sb2.toString());

		return aquaxTranslations;
	}

	/** Gets the normalized form of a tree pattern */
	public TreePattern getNormalizedTreePattern() {
		TreePattern normalizedTreePattern = deepCopy();

		TreeSet<TreePatternNode> sortedSetChildrenNodes = new TreeSet<TreePatternNode>();

		//FIXME: Assign node numbers after the normalization takes place
		for(PatternEdge edge:normalizedTreePattern.root.getEdges()) {
			recGetNormalizedTreePattern(edge.n2);
			sortedSetChildrenNodes.add(edge.n2);
		}

		normalizedTreePattern.root.cleanEdges();
		for(TreePatternNode node:sortedSetChildrenNodes.descendingSet()) {
			normalizedTreePattern.root.addEdge(node.getParentEdge());
		}

		normalizedTreePattern.renumberNodes(1);
		return normalizedTreePattern;
	}

	/** Adds a root in the TreePattern that is not a real node but an empty tag with nodeID=-1 */
	public static TreePattern addDefaultRoot(TreePattern headless_pattern){
		if(headless_pattern.getRoot().getNodeCode() == -1) {
			return headless_pattern;
		}

		TreePatternNode theRoot = new TreePatternNode("", "", -1);
		theRoot.addEdge(headless_pattern.getRoot(), false, false);
		headless_pattern.setRoot(theRoot);

		return headless_pattern;
	}



	/**
	 * Recursive method called in order to create the normalized tree pattern.
	 * @param node a tree pattern node, we will order its children
	 * @param counter the value of the counter that we are using for renumbering the tree pattern nodes
	 * @return the counter that we are using for renumbering the tree pattern nodes
	 */
	private void recGetNormalizedTreePattern(TreePatternNode node) {
		if(node.getEdges().isEmpty()) {
			return ;
		}

		TreeSet<TreePatternNode> sortedSetChildrenNodes = new TreeSet<TreePatternNode>();
		for(PatternEdge edge:node.getEdges()) {
			recGetNormalizedTreePattern(edge.n2);
			sortedSetChildrenNodes.add(edge.n2);
		}

		node.cleanEdges();
		for(TreePatternNode child:sortedSetChildrenNodes.descendingSet()) {
			node.addEdge(child.getParentEdge());
		}

		return ;
	}

	public TreePattern deepCopyWithReplace(TreePatternNode unfoldingNode, TreePatternNode pNew) {
		return new TreePattern(root.deepCopyWithReplace(unfoldingNode, pNew), this.ordered);
	}

	public void parseUnrequiredData(String tagOfTuplesToKeep) {
		this.root.parseUnrequiredData(tagOfTuplesToKeep);
	}

	@Override
	public int compareTo(TreePattern p2) {
		return this.getName().compareTo(p2.getName());
	}

	/**
	 * Returns a sorted set of strings. Important notice: this function always returns the tags that were 
	 * part of the pattern at the very first time that the function was called.
	 * @return a sorted list of strings containing all tags of the tree pattern
	 * */
	public List<String> getSortedSetofTags() {
		if(this.orderedTagSet==null){
			List<TreePatternNode> nodes = this.getNodes();
			this.orderedTagSet = new ArrayList<String>(nodes.size());
			for(TreePatternNode node: nodes){
				this.orderedTagSet.add(node.getTag());
			}
			Collections.sort(this.orderedTagSet);

		}
		return this.orderedTagSet;
	}

}
