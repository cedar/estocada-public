package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import ucsd.edu.redis.RedisKQL.datatype.MapType;
import ucsd.edu.redis.RedisKQL.datatype.SetType;

/**
 * Implements an iterator pattern over the result supplied by the Redis-KQL
 * 
 * @author ranaalotaibi
 * @author Romain Primet
 */
class KQLResult extends NIterator {
	protected final SetType		res;
	protected List<NTuple>		tuples	= null;
	protected Iterator<NTuple>	backIt	= null;

	KQLResult(Object res, NRSMD meta) {
		super();
		if (res == null) {
			throw new IllegalArgumentException("Expected non-null result");
		}
		this.res = (SetType) res;
		this.nrsmd = meta;
	}

	@Override
	public boolean hasNext() throws TatooineExecutionException {
		return backIt.hasNext();
	}

	@Override
	public NTuple next() throws TatooineExecutionException {
		return backIt.next();
	}

	@Override
	public void open() throws TatooineExecutionException {
		tuples = toNTuples();
		backIt = tuples.iterator();
	}

	@Override
	public void close() throws TatooineExecutionException {
		tuples = null;
		backIt = null;
	}

	protected NTuple toNTuple(Object kqlAtom) {
		NTuple retval = new NTuple();
		retval.nrsmd = this.nrsmd;
		if (kqlAtom instanceof String) {
			retval.stringFields = new char[1][];
			retval.addString((String) kqlAtom);
		} else if (kqlAtom instanceof MapType) {
			Map m = ((MapType) kqlAtom).returnMap();
			retval.stringFields = new char[m.size()][];
			for (Object str : m.values()) {
				if (!(str instanceof String)) {
					throw new IllegalArgumentException("Expected a map of strings, got a map of " + str.getClass());
				}
				retval.addString((String) str);
			}
		} else if (kqlAtom instanceof SetType) {
			Set s = ((SetType) kqlAtom).getSet();
			retval.stringFields = new char[s.size()][];
			for (Object str : s) {
				retval.addString((String) str);
			}
		} else {
			throw new IllegalArgumentException("Unhandled atom type: " + kqlAtom);
		}

		return retval;
	}

	protected List<NTuple> toNTuples() {
		List<NTuple> retval = new ArrayList<NTuple>();
		for (Object atom : res.getSet()) {
			retval.add(toNTuple(atom));
		}
		return retval;
	}

	@Override
	public NIterator getNestedIterator(int i) throws TatooineExecutionException {
		throw new TatooineExecutionException("KQL results are not nested");
	}

	@Override
	public int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		throw new UnsupportedOperationException("Not implemented");
	}

	@Override
	public String getName() {
		return "KQLResult";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}

}
