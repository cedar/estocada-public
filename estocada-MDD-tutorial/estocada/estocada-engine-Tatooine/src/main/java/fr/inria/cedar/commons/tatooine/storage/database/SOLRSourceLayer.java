package fr.inria.cedar.commons.tatooine.storage.database;

import static java.lang.Math.toIntExact;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.response.QueryResponse;

import com.google.common.base.Strings;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class SOLRSourceLayer implements DBSourceLayer {

    private static final Logger log = Logger.getLogger(SOLRSourceLayer.class);

    /* Solr client */
    // The recommended practice is to keep a static instance of HttpSolrClient per Solr server and share it
    private static HttpSolrClient solrClient = null;

    /* Default constants */
    private static final String DEFAULT_SERVER_URL = "http://localhost";
    private static final int DEFAULT_SERVER_PORT = 8983;
    private static final int DEFAULT_NUM_OF_DOCUMENTS = 100;

    private GSR view;

    /* Basic authentication plugin */
    // As described here:
    // https://cwiki.apache.org/confluence/display/solr/Basic+Authentication+Plugin
    private String username = "";
    private String password = "";

    /* Pagination variables */
    protected int maxResults = DEFAULT_NUM_OF_DOCUMENTS;
    // Number of NTuple objects that have been processed so far
    protected int cursorId = 0;
    // Number of results to retrieve from a SOLR collection at a time
    protected int resultsPerQuery;
    // Total number of results of a SOLR collection that match a given query
    protected int numResultsFound;

    /* SOLR query components */
    public String collection = "";

    /* Constructor */
    public SOLRSourceLayer(GSR gsr) {
        view = gsr;

        // Resolving authentication credentials
        String serverUsr = view.getPropertyValue("serverUsr");
        if (!Strings.isNullOrEmpty(serverUsr)) {
            username = serverUsr;
        }
        String serverPwd = view.getPropertyValue("serverPwd");
        if (!Strings.isNullOrEmpty(serverPwd)) {
            password = serverPwd;
        }

        // Resolving number of documents to retrieve at a time
        String numResults = Parameters.getProperty("numberOfDocumentsAtATime");
        try {
            if (!Strings.isNullOrEmpty(numResults)) {
                maxResults = Integer.valueOf(numResults);
            }
        } catch (NumberFormatException nfe) {
            String msg = String.format(
                    "The number of documents to fetch at a time specified in the catalog is NOT an integer: %s",
                    numResults);
            log.error(msg);
        }
        resultsPerQuery = numResultsFound = maxResults;

        // Resolving name of collection to connect to and query
        String coreName = view.getPropertyValue("coreName");
        if (!Strings.isNullOrEmpty(coreName)) {
            collection = coreName;
        }
    }

    @Override
    public void connect() throws TatooineExecutionException {
        // Setting default server values
        String url = DEFAULT_SERVER_URL;
        int port = DEFAULT_SERVER_PORT;

        // Reading input server values
        String serverUrl = view.getPropertyValue("serverUrl");
        if (!Strings.isNullOrEmpty(serverUrl)) {
            url = serverUrl;
        }
        String serverPort = view.getPropertyValue("serverPort");
        try {
            if (!Strings.isNullOrEmpty(serverPort)) {
                port = Integer.valueOf(serverPort);
            }
        } catch (NumberFormatException nfe) {
            String msg = String.format("The server port specified in the catalog is NOT an integer: %s", serverPort);
            log.error(msg);
        }

        try {
            solrClient = new HttpSolrClient(String.format("%s:%s/solr", url, port));
        } catch (Exception e) {
            String msg = String.format("Failure trying to create an HttpSolrClient with URL %s and port %d: %s", url,
                    port, e.getMessage());
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
        cursorId = 0;
    }

    @Override
    /**
     * Fetches the next "N" results from a SOLR instance that corresponds to an input query
     * "N" refers to the value of the variable "resultsPerQuery" which is provided by the user
     */
    public Object getBatchRecords(Object[] parameters) throws TatooineExecutionException {
        QueryResponse response = null;
        if (cursorId >= numResultsFound) {
            return response;
        }

        // Check parameters
        int len = parameters.length;
        if (parameters.length != 2) {
            String msg = String.format("Method getBatchRecords called with %d parameters instead of 2", len);
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }
        if (!(parameters[0] instanceof String) || !(parameters[1] instanceof String)) {
            String msg = "Method getBatchRecords called with non-String parameters!";
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }

        String[] params = String.valueOf(parameters[0]).split("&");
        String queryStr = params[0];
        String fieldStr = String.valueOf(parameters[1]);
        String[] filters = null;
        if (params.length > 1) {
            filters = new String[params.length - 1];
            for (int i = 1; i < params.length; i++) {
                filters[i - 1] = params[i];
            }
        }
        SolrQuery query = new SolrQuery();
        query.setQuery(queryStr);

        // Projection
        if (!Strings.isNullOrEmpty(fieldStr)) {
            query.setFields(fieldStr);
        }

        if (filters != null) {
            query.setFilterQueries(filters);
        }
        // We don't want to retrieve all matching records at a time to avoid memory overflows
        // We will thus retrieve records incrementally in groups of "resultsPerQuery" results
        // Offset
        query.setStart(cursorId);
        // Number of results to fetch at a time
        query.setRows(resultsPerQuery);

        try {
            log.info(String.format("Querying SOLR collection \"%s\" with query \"%s\"", collection, query.toString()));
            QueryRequest request = new QueryRequest(query);

            // Basic authentication
            // In SolrJ the basic authentication credentials need to be set every time a new request is made
            if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
                request.setBasicAuthCredentials(username, password);
            }

            response = request.process(solrClient, collection);
        } catch (SolrServerException | IOException e) {
            String msg = String.format("Error retrieving results from Solr's '%s' collection with query '%s': %s",
                    collection, query.toString(), e.getMessage());
            log.error(msg);
            throw new TatooineExecutionException(msg);
        }

        cursorId += response.getResults().size();
        numResultsFound = toIntExact(response.getResults().getNumFound());
        return response;
    }

    @Override
    public void close() throws TatooineExecutionException {
        try {
            solrClient.close();
        } catch (IOException e) {
            String msg = "Error closing the Http Solr client used by the SolrSourceLayer class";
            throw new TatooineExecutionException(msg);
        }
    }

    @Override
    /** Resets pagination variables */
    public void reset() {
        cursorId = 0;
        resultsPerQuery = numResultsFound = maxResults;
    }
}
