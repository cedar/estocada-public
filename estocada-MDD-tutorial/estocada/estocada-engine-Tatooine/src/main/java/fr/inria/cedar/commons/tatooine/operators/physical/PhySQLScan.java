package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;

/**
 * Physical SQL (through JDBC) query operator.
 * @author Oscar Mendoza
 */
public class PhySQLScan extends SQLQueryParameterisedOperator implements PhyScanOperator {

	/** Universal version identifier for the PhySQLScan class */
	private static final long serialVersionUID = 4140886496116233562L;
	
	private static final Logger log = Logger.getLogger(PhySQLScan.class);
	
	/* Constructor */
	public PhySQLScan(String query, StorageReference ref, NRSMD nrsmd) throws TatooineExecutionException {
		super(query, ref, nrsmd);
		if (!isScanOrFilter(query)) {
			log.error(MSG_USE_EVAL);
			throw new TatooineExecutionException(MSG_USE_EVAL);
		}
		this.nrsmd = nrsmd;
	}
	
	/* Constructor */
	public PhySQLScan(String query, StorageReference ref, ViewSchema schema) throws TatooineExecutionException {
		this(query, ref, schema.getNRSMD());
	}
	
	@Override
	public String getName() {
		return "PhySQLScan";
	}

	@Override
	public String getName(int depth) {
		return getName();
	}

	@Override
	public boolean isScanOrFilter(String query) {
		// We assume that the Scan queries we will support are of the form:
		// SELECT [DISTINCT] * FROM [...]
		Pattern p = Pattern.compile("SELECT(\\s+DISTINCT)?\\s+\\*\\s+FROM");
		Matcher m = p.matcher(query);
		return m.find();
	}
	
}
