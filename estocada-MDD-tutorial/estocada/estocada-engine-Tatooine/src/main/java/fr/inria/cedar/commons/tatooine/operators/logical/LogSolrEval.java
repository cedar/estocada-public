package fr.inria.cedar.commons.tatooine.operators.logical;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;

/**
 * Logical Solr Operator.
 * 
 * @author ranaalotaibi
 */
public class LogSolrEval extends LogLeafOperator {
	private String[]			speratedQuery;
	private StorageReference	solrRef;

	public LogSolrEval(String speratedQuery, NRSMD meta, StorageReference solrRef) {
		this.speratedQuery = speratedQuery.split("-");
		this.solrRef = solrRef;
		this.setNRSMD(meta);
	}

	@Override
	public int getJoinDepth() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public String getName() {
		return "LogSolrEval";
	}

	@Override
	public String toString() {
		return "[LogSolrEval query: " + speratedQuery + "]";
	}

	public void UpdateQuery(String parameter, String bindAccessVar) {
		speratedQuery[0] = speratedQuery[0].replaceAll(bindAccessVar, parameter);
	}

	public String getFields() {
		return speratedQuery[1];
	}

	public StorageReference getGSR() {
		return solrRef;
	}

	@Override
	public void accept(LogOperatorVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public LogOperator deepCopy() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public double estimatedCardinality() {
		throw new UnsupportedOperationException("Unsupported");
	}

	@Override
	public double estimatedIOCost() {
		throw new UnsupportedOperationException("Unsupported");
	}

	public String getQueryString() {
		return speratedQuery[0];
	}
}
