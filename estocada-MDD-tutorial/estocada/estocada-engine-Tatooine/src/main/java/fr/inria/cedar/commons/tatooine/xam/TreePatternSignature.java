package fr.inria.cedar.commons.tatooine.xam;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

/**
 * This class models the signature of a tree pattern. It contains a bag with the 
 * return attributes (together with the corresponding node labels) of the tree
 * pattern, as well as a bag with the labels of the pattern nodes.
 * These bags are implemented as maps that have as keys the return attributes
 * and the node labels, respectively, of the pattern, and as values their
 * occurrences in the tree pattern.
 * It also contains a map showing whether each of the pattern nodes is connected
 * to its immediate ancestor through a parent or an ancestor edge.
 * 
 * @author Konstantinos KARANASOS
 *
 * @created 15/07/2012
 */
public class TreePatternSignature {
	
	/**
	 * Map containing as key a string of the form "nodeLabel.returnAttribute" and
	 * as value the number of occurrences of this String in the tree pattern. 
	 */
	private Map<String,Integer> returnAttsMap;
	
	/**
	 * Map containing as key a node label and as value the number of occurrences
	 * of this label in the tree pattern.
	 */
	private Map<String,Integer> nodeLabelsMap;
	
	/**
	 * Map containing as key the node codes of the pattern and as value a boolean
	 * that is true if the node is connected with a parent edge to its parent, or
	 * false if it's connected with an ancestor edge.
	 */
	private Map<Integer,Boolean> hasParentEdgeMap;

	/**
	 * Constructor that takes as input a tree pattern and populates the maps
	 * that constitute its signature.
	 */
	public TreePatternSignature(TreePattern pattern) {
		super();
		
		this.returnAttsMap = new LinkedHashMap<String,Integer>();
		this.nodeLabelsMap = new LinkedHashMap<String,Integer>();
		this.hasParentEdgeMap = new LinkedHashMap<Integer,Boolean>();
		
		// Traverse the tree pattern and populate the returnAtts and nodeLabels
		Stack<TreePatternNode> stack = new Stack<TreePatternNode>();
		TreePatternNode actualRoot = pattern.getActualRoot();
		stack.push(actualRoot);
		this.hasParentEdgeMap.put(actualRoot.getNodeCode(), false);
		
		while (!stack.empty()){
			TreePatternNode node = stack.pop();
			
			// Increase the number of occurrences for the specific label
			increaseMapOccurrence(nodeLabelsMap, node.getTag());
			// Increase the number of occurrences for the specific return attribute
			if (node.storesID()) {
				increaseMapOccurrence(returnAttsMap, node.getTag() + ".ID");
			}
			if (node.storesValue()) {
				increaseMapOccurrence(returnAttsMap, node.getTag() + ".Val");
			}
			if (node.storesContent()) {
				increaseMapOccurrence(returnAttsMap, node.getTag() + ".Cont");
			}
			
			for (PatternEdge edge: node.getEdges()) {
				stack.push(edge.n2);
				this.hasParentEdgeMap.put(edge.n2.getNodeCode(), edge.isParent());
			}
		}
	}
	
	/* Getters and setters */
	public Map<String, Integer> getReturnAttsMap() {
		return returnAttsMap;
	}

	public void setReturnAttsMap(Map<String, Integer> returnAtts) {
		this.returnAttsMap = returnAtts;
	}

	public Map<String, Integer> getNodeLabelsMap() {
		return nodeLabelsMap;
	}

	public void setNodeLabelsMap(Map<String, Integer> nodeLabels) {
		this.nodeLabelsMap = nodeLabels;
	}
	
	public Map<Integer, Boolean> getHasParentEdgeMap() {
		return hasParentEdgeMap;
	}

	public void setHasParentEdgeMap(Map<Integer, Boolean> hasParentEdgeMap) {
		this.hasParentEdgeMap = hasParentEdgeMap;
	}
	
	/**
	 * Takes as input a map and a string value and increases the number of occurrences
	 * of this string in the map.
	 */
	public void increaseMapOccurrence(Map<String,Integer> map, String valToIncrease) {
		Integer previousValue = map.get(valToIncrease);
		if (previousValue == null) {
			map.put(valToIncrease, 1);
		}
		else {
			map.put(valToIncrease, ++previousValue);
		}
	}
	
	/**
	 * Takes two maps and returns true if they contain the same keys and the same values for these keys.
	 * Note: The Map.equals implements the same thing, and may be used instead.
	 */
	@SuppressWarnings("unused")
	private boolean compareOccurrenceMaps(Map<String,Integer> map1, Map<String,Integer> map2) {
		if (map1.size() != map2.size()) {
			return false;
		}
		
		for (String keyMap1 : map1.keySet()) {
			if (!map2.containsKey(keyMap1) || (map1.get(keyMap1) != map2.get(keyMap1))) {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Checks if the current tree pattern signature is equal to the given tree pattern 
	 * signature, by comparing the maps they contain.
	 */
	public boolean equals(TreePatternSignature treePatSigToCompare) {
		return ((returnAttsMap.equals(treePatSigToCompare.getReturnAttsMap())) && 
				(nodeLabelsMap.equals(treePatSigToCompare.getNodeLabelsMap())) &&
				(hasParentEdgeMap.equals(treePatSigToCompare.getHasParentEdgeMap())));
	}
	
	/**
	 * This method is used to check if the current signature is compatible with the given one for 
	 * containment checking. We want to check if the pattern <i>p1</i> corresponding to the given 
	 * signature is contained in the pattern <i>p2</i> corresponding to the current signature (i.e.,
	 * <i>p2</i> will have to be embedded in <i>p1</i>).
	 * This method checks whether the labels of <i>p2</i> are a subset of those of <i>p1</i>, and 
	 * whether the two patterns have the same return attributes.
	 * 
	 * @param treePatSigToCompare the signature to compare to
	 * @return true if the check succeeds
	 */
	public boolean compareSigForContainment(TreePatternSignature treePatSigToCompare) {
		return ((treePatSigToCompare.getNodeLabelsMap().keySet().containsAll(nodeLabelsMap.keySet())) && 
				(returnAttsMap.equals(treePatSigToCompare.getReturnAttsMap())));
	}
		
}
