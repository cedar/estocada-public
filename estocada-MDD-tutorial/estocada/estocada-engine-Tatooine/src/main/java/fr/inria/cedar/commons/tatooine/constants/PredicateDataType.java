package fr.inria.cedar.commons.tatooine.constants;

/** Enumerated type representing the possible types of data types predicates can be applied to */
public enum PredicateDataType {
	PREDICATE_ON_INTEGER, PREDICATE_ON_STRING, PREDICATE_ON_JOIN; 
}
