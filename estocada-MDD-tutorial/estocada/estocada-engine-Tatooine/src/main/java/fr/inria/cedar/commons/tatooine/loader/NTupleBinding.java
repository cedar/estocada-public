package fr.inria.cedar.commons.tatooine.loader;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.bind.tuple.TupleInput;
import com.sleepycat.bind.tuple.TupleOutput;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.IDs.CompactDynamicDeweyElementID;
import fr.inria.cedar.commons.tatooine.IDs.ElementID;
import fr.inria.cedar.commons.tatooine.IDs.ElementIDFactory;
import fr.inria.cedar.commons.tatooine.IDs.OrderedIntegerElementID;
import fr.inria.cedar.commons.tatooine.IDs.PrePostDepthElementID;
import fr.inria.cedar.commons.tatooine.IDs.PrePostElementID;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;

/**
 * This class is the liaison between NTuple and BerkeleyDB, that is, it wraps each NTuple as expected into BDB-storable
 * things.
 * 
 * @author Ioana MANOLESCU
 */
public class NTupleBinding extends TupleBinding<NTuple> implements Serializable {
	private static final Logger log = Logger.getLogger(NTupleBinding.class);

	/** Universal version identifier for the NTupleBinding class */
	private static final long serialVersionUID = -8053913867869844573L;

	/* Constant */
	private static final boolean USE_DEPTH = Boolean.parseBoolean(Parameters.getProperty("childAxis"));

	/*
	 * (non-Javadoc)
	 * @see com.sleepycat.bind.tuple.TupleBinding#entryToObject(com.sleepycat.bind.tuple.TupleInput)
	 */
	@Override
	public NTuple entryToObject(TupleInput ti) {
		int colNo = ti.readInt();
		// Parameters.logger.debug("\n" + colNo + " columns");
		TupleMetadataType[] types = new TupleMetadataType[colNo];
		int iStrings = 0;
		int iUri = 0;
		int iIDs = 0;
		int iInteger = 0;
		int iNested = 0;

		for (int i = 0; i < colNo; i++) {
			types[i] = TupleMetadataType.getTypeEnum(ti.readString());
			// Parameters.logger.debug("Read at "+ i + " type " + Constants.decodeConstant(types[i]));
			switch (types[i]) {
			case STRING_TYPE:
				iStrings++;
				break;
			case URI_TYPE:
				iUri++;
				break;
			case INTEGER_TYPE:
				iInteger++;
				break;
			case UNIQUE_ID:
			case ORDERED_ID:
			case STRUCTURAL_ID:
			case UPDATE_ID:
				iIDs++;
				break;
			case TUPLE_TYPE:
				iNested++;
				break;
			default:
				log.info("NTupleBinding: Unknown type");
			}
		}

		char[][] stringFields = new char[iStrings][];
		char[][] uriFields = new char[iUri][];
		ElementID[] idFields = new ElementID[iIDs];
		int[] integerFields = new int[iInteger];
		@SuppressWarnings("unchecked")
		ArrayList<NTuple>[] tupleFields = new ArrayList[iNested];
		NRSMD[] childrenRSMDs = new NRSMD[iNested];

		iStrings = 0;
		iUri = 0;
		iIDs = 0;
		iInteger = 0;
		iNested = 0;

		for (int i = 0; i < colNo; i++) {
			switch (types[i]) {
			case STRING_TYPE:
				// Parameters.logger.debug("At column " + i + " reading a string");
				String sAux = ti.readString();
				// Parameters.logger.debug("Read: " + sAux);
				stringFields[iStrings] = sAux.toCharArray();
				iStrings++;
				break;
			case URI_TYPE:
				// Parameters.logger.debug("At column " + i + " reading a string");
				String uriAux = ti.readString();

				// Parameters.logger.debug("Read: " + sAux);
				uriFields[iUri] = uriAux.toCharArray();
				iUri++;
				break;
			case INTEGER_TYPE:
				// Parameters.logger.debug("At column " + i + " reading a string");
				int integerAux = Integer.parseInt(ti.readString());

				// Parameters.logger.debug("Read: " + sAux);
				integerFields[iInteger] = integerAux;
				iInteger++;
				break;
			case UNIQUE_ID:
			case ORDERED_ID:
				// Parameters.logger.debug("At column " + i + " reading an ordered ID");
				int k = ti.readInt();
				if (k == -1) {
					idFields[iIDs] = OrderedIntegerElementID.theNull;
				} else {
					idFields[iIDs] = new OrderedIntegerElementID(k);
				}
				// Parameters.logger.debug("Understood " + idFields[iIDs].toString());
				iIDs++;
				break;
			case STRUCTURAL_ID:
				// Parameters.logger.debug("At column " + i + " reading structural ID");
				int pre = 0;
				int post = 0;
				int depth = 0;
				try {
					if (USE_DEPTH) {
						pre = ti.readInt();
						post = ti.readInt();
						depth = ti.readInt();
					} else {
						pre = ti.readInt();
						post = ti.readInt();
					}
				} catch (IndexOutOfBoundsException e) {
					log.error("Exception", e);
				}
				if (pre != -1) {
					idFields[iIDs] = ElementIDFactory.getElementID(pre, depth);
					if (idFields[iIDs] instanceof PrePostElementID && (!USE_DEPTH)) {
						((PrePostElementID) idFields[iIDs]).setPost(post);
					} else {
						if (idFields[iIDs] instanceof PrePostDepthElementID && USE_DEPTH) {
							((PrePostDepthElementID) idFields[iIDs]).setPost(post);
						} else {
							log.error("Doesn't know what was meant here");
						}
					}
				} else {
					idFields[iIDs] = ElementIDFactory.getElementID(-1, -1);
				}
				// Parameters.logger.debug("Understood: " + idFields[iIDs].toString());
				iIDs++;
				break;
			case UPDATE_ID:
				List<Integer> path = new ArrayList<Integer>();
				List<Integer> tagPath = new ArrayList<Integer>();
				Integer integer;
				int pathLength = ti.readInt();
				for (int j = 0; j < pathLength; j++) {
					integer = ti.readInt();
					path.add(integer);
				}
				for (int j = 0; j < pathLength; j++) {
					integer = ti.readInt();
					tagPath.add(integer);
				}
				int[] pathArray = new int[path.size()];
				for (int j = 0; j < path.size(); j++) {
					pathArray[j] = path.get(j);
				}

				int[] tagPathArray = new int[tagPath.size()];
				for (int j = 0; j < tagPath.size(); j++) {
					tagPathArray[j] = tagPath.get(j);
				}

				idFields[iIDs] = new CompactDynamicDeweyElementID(pathArray, tagPathArray);
				iIDs++;
				break;
			case TUPLE_TYPE:
				// Parameters.logger.debug("At column " + i + " reading nested field");
				int n = ti.readInt();
				// Parameters.logger.debug("There are " + n + " tuples here");
				ArrayList<NTuple> vChild = new ArrayList<NTuple>();
				for (int j = 0; j < n; j++) {
					NTuple nt = (entryToObject(ti));
					vChild.add(nt);
					childrenRSMDs[iNested] = nt.nrsmd;
				}
				tupleFields[iNested] = vChild;
				iNested++;
				break;
			default:
				// Parameters.logger.debug("NTupleBinding: Unknown type");
			}
		}

		try {
			NTuple res = new NTuple(colNo, types, childrenRSMDs, stringFields, uriFields, idFields, integerFields,
					tupleFields);
			// Parameters.logger.debug("Re-understood tuple: ");
			// res.display();
			return res;
		} catch (TatooineExecutionException uee) {
			log.error("TatooineExecutionException: ", uee);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.sleepycat.bind.tuple.TupleBinding#objectToEntry(java.lang.Object, com.sleepycat.bind.tuple.TupleOutput)
	 */
	@Override
	public void objectToEntry(NTuple arg0, TupleOutput to) {
		NTuple theTuple = null;
		NRSMD md = null;
		try {
			theTuple = arg0;
			md = theTuple.nrsmd;
			// Parameters.logger.info("\n Serializing tuple ");
			// theTuple.display();
			// md.display();
		} catch (Exception cce) {
			log.error("Exception: ", cce);
		}
		to.writeInt(md.getColNo());
		TupleMetadataType[] types = md.getColumnsMetadata();
		for (TupleMetadataType type : types) {
			to.writeString(type.toString());
		}
		int iString = 0;
		int iUri = 0;
		int iID = 0;
		int iInteger = 0;
		int iNested = 0;
		for (TupleMetadataType type : types) {
			switch (type) {
			case STRING_TYPE:
				if (theTuple.stringFields[iString] == null) {
					// Parameters.logger.debug("Null String");
					to.writeString(TupleMetadataType.NULL.toString());
				} else {
					to.writeString(new String(theTuple.stringFields[iString]));
				}
				iString++;
				break;
			case URI_TYPE:
				if (theTuple.uriFields[iUri] == null) {
					to.writeString(TupleMetadataType.NULL.toString());
				} else {
					to.writeString(new String(theTuple.uriFields[iUri]));
				}
				iUri++;
				break;
			case INTEGER_TYPE:
				to.writeString(Integer.toString(theTuple.integerFields[iInteger]));
				iInteger++;
				break;
			case UPDATE_ID:
				CompactDynamicDeweyElementID cddID = (CompactDynamicDeweyElementID) theTuple.idFields[iID];
				int[] path = cddID.getPath();
				/*
				 * This is output so we know the length of the path and tag path to allow us to know how many ints to
				 * extract when translating back to NTuple
				 */
				to.writeInt(path.length);
				for (int element : path) {
					to.writeInt(element);
				}
				int[] tagPath = cddID.getTag();
				for (int element : tagPath) {
					to.writeInt(element);
				}
				iID++;
				break;
			case UNIQUE_ID:
			case ORDERED_ID:

				OrderedIntegerElementID eID = (OrderedIntegerElementID) theTuple.idFields[iID];
				if (eID == null) {
					eID = OrderedIntegerElementID.theNull;
				}
				to.writeInt(eID.n);
				iID++;
				break;
			case STRUCTURAL_ID:
				// Parameters.logger.debug("This tuple has " + theTuple.idFields.length +
				// " IDs and I am using the " + iID + "-th");

				if (USE_DEPTH) {
					PrePostDepthElementID ppi = (PrePostDepthElementID) theTuple.idFields[iID];
					if (ppi == null) {
						ppi = PrePostDepthElementID.theNull;
						// Parameters.logger.info("NULL ID");
					}
					try {
						// Parameters.logger.info("Serialized a pre-post-depth");
						to.writeInt(ppi.pre);
						to.writeInt(ppi.post);
						to.writeInt(ppi.depth);
					} catch (IndexOutOfBoundsException e) {
						log.error("Exception", e);
					}
					iID++;
				} else {
					PrePostElementID ppi = (PrePostElementID) theTuple.idFields[iID];
					if (ppi == null) {
						ppi = PrePostElementID.theNull;
					}
					to.writeInt(ppi.pre);
					to.writeInt(ppi.post);
					iID++;
				}
				break;
			case TUPLE_TYPE:
				List<NTuple> nt = theTuple.nestedFields[iNested];
				int k = nt.size();
				boolean actuallyStore = true;
				if (k == 1) {
					NTuple possNull = nt.get(0);
					if (possNull.isNull) {
						actuallyStore = false;
						to.writeInt(0);
					}
				}
				if (actuallyStore) {
					to.writeInt(nt.size());
					// Parameters.logger.debug("There are " + nt.size() + " tuples nested at column " + i);
					Iterator<NTuple> iTuples = nt.iterator();
					while (iTuples.hasNext()) {
						NTuple childTuple = iTuples.next();
						objectToEntry(childTuple, to);
					}
				}
				iNested++;
				break;
			default:
				log.info("Unknown type in metadata: " + type);
			}
		}
	}

}
