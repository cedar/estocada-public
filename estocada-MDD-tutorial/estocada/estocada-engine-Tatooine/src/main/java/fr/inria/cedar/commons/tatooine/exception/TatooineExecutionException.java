package fr.inria.cedar.commons.tatooine.exception;

/**
 * Tatooine exception thrown at execution time.
 *
 * @author Andrei ARION
 */
public class TatooineExecutionException extends Exception {

	private static final long serialVersionUID = -4777975920270228924L;

	public TatooineExecutionException(String s) {
		super(s);
	}

	public TatooineExecutionException(Exception e) {
		super(e);
	}

}