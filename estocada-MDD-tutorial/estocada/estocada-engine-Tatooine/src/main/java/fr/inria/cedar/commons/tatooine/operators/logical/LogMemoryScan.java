package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.xam.TreePattern;

/**
 * This logical operator implements the scan of tuples from memory instead of from database.
 * @author Jesus CAMACHO RODRIGUEZ
 */
public class LogMemoryScan extends LogLeafOperator {
	private static final Logger log = Logger.getLogger(LogMemoryScan.class);
	
	/** The pattern that the memory scan will refer to */
	private TreePattern qp;
	
	/** The list of tuples */
	private ArrayList<NTuple> tuples;

	private boolean derivationCount;

	public LogMemoryScan(TreePattern qp, boolean derivationCount) throws TatooineException, TatooineExecutionException {
		super();
		this.qp = qp;
		this.tuples = new ArrayList<NTuple>();
		this.setOwnName("LogMemScan" + "(" + qp.getName() + ")");
		this.setVisible(true);
		this.derivationCount = derivationCount;
		this.setNRSMD(NRSMD.getNRSMD(qp.getRoot(), derivationCount,
				new HashMap<Integer, HashMap<String, ArrayList<Integer>>>()));
	}

	@Override
	public int getJoinDepth() {
		return 0;
	}

	@Override
	public String getName() {
		return this.getOwnName();
	}

	@Override
	public void recDisplayNRSMD() {
		this.getNRSMD().display();
	}

	public NRSMD getNrsmd() {
		return getNRSMD();
	}

	public void setNrsmd(NRSMD nrsmd) {
		this.setNRSMD(nrsmd);
	}

	public ArrayList<NTuple> getTuples() {
		return tuples;
	}

	public void setTuples(ArrayList<NTuple> tuples) {
		this.tuples = tuples;
	}

	public TreePattern getPattern() {
		return qp;
	}

	/**
	 * Makes a deep copy of the current operator
	 * @author Konstantinos KARANASOS
	 */
	@Override
	public LogMemoryScan deepCopy() {
		LogMemoryScan copy = null;
		try {
			copy = new LogMemoryScan(this.qp.deepCopy(), this.derivationCount);
		} catch (TatooineException | TatooineExecutionException e) {
			log.error("Exception: ", e);
		}

		copy.setEquivalentPattern(this.getEquivalentPattern());

		return copy;
	}

	@Override
	public double estimatedCardinality() {
		return this.tuples.size();
	}

	@Override
	public double inputCardinality() {
		return this.tuples.size();
	}

	@Override
	public double estimatedIOCost() {
		return 0d;
	}
}
