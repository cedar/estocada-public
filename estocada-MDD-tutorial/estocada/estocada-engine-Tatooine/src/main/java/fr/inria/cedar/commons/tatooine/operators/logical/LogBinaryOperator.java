package fr.inria.cedar.commons.tatooine.operators.logical;

import java.util.ArrayList;

import fr.inria.cedar.commons.tatooine.predicates.Predicate;

/**
 * Class that represents a logical operator that has two children.
 * @author Ioana MANOLESCU
 */
public abstract class LogBinaryOperator extends LogOperator {

	private Predicate pred;
	private String name;

	/** Constructor with left and right children */
	public LogBinaryOperator(LogOperator left, LogOperator right) {
		children = new ArrayList<LogOperator>();
		children.add(left);
		children.add(right);
	}
	
	/* Getters and setters */
	
	/** Returns the left child */
	public LogOperator getLeft() {
		return children.get(0);
	}

	/** Sets a logical operator as the left child of this LogBinaryOperator */
	public void setLeft(LogOperator op) {
		children.set(0, op);
	}

	/** Returns the right child */
	public LogOperator getRight() {
		return children.get(1);
	}

	/** Sets a logical operator as the right child of this LogBinaryOperator */
	public void setRight(LogOperator op) {
		children.set(1, op);
	}
	
	public void setPredicate(Predicate pred) {
		this.pred = pred;
	}

	public Predicate getPredicate() {
		return pred;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		StringBuffer sb = new StringBuffer();
		sb.append(this.getOwnName());
		sb.append("(" + getLeft().getName() + "," + getRight().getName());
		if (this.getPredicate() != null) {
			sb.append("," + getPredicate().toString() + ")");
		} else {
			sb.append(")");
		}
		name = new String(sb);
		return name;
	}

	@Override
	public final int recursiveDotString(StringBuffer sb, int parentNo, int firstAvailableNo) {
		int selfNumber = -1;

		if (this.isVisible()) {
			selfNumber = firstAvailableNo;

			sb.append(selfNumber + " [label=\"" + getOwnName());
			if (this.getPredicate() != null) {
				sb.append("\\n [" + getPredicate().toString().replace("\"", "\\\"") + "]");
			}
			sb.append("\"] ; \n");

			if (parentNo != -1) {
				sb.append(parentNo + " -> " + selfNumber + " ; \n");
			}
		}
		int childNumber1 = -1;
		if (isVisible()) {
			childNumber1 = getLeft().recursiveDotString(sb, selfNumber, (firstAvailableNo + 1));
		} else {
			childNumber1 = getLeft().recursiveDotString(sb, parentNo, firstAvailableNo);
		}
		int childNumber2 = -1;
		if (isVisible()) {
			childNumber2 = getRight().recursiveDotString(sb, selfNumber, childNumber1);
		} else {
			childNumber2 = getRight().recursiveDotString(sb, parentNo, childNumber1);
		}
		return childNumber2;
	}

	@Override
	public void recDisplayNRSMD(StringBuffer sb) {
		sb.append(this.getOwnName() + " " + this.getNRSMD().toString() + "(");
		getLeft().recDisplayNRSMD(sb);
		sb.append(", ");
		getRight().recDisplayNRSMD(sb);
		sb.append(")");
	}

	@Override
	public int getJoinDepth() {
		// Note to debugger: previously this was 1 + max(left,right). Why?
		return 1 + Math.max(getLeft().getJoinDepth(), getRight().getJoinDepth());
	}

}
