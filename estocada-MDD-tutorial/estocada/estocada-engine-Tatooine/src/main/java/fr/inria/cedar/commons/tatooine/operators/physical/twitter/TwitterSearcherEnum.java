package fr.inria.cedar.commons.tatooine.operators.physical.twitter;

/** Enumerated type representing the three different modes of querying Twitter's Search API  */
public enum TwitterSearcherEnum {
	USER_ID, USER_NAME, HASHTAG 
}
