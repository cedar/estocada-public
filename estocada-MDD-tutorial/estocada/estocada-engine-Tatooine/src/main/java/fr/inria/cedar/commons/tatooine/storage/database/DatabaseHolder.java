package fr.inria.cedar.commons.tatooine.storage.database;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.loader.multidoc.EngineSystem;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public interface DatabaseHolder {

	//TODO [zabetak] Refactor and change the name
	public boolean containsDB(GSR view) throws Exception;

	public void open(GSR view) throws Exception;

	public void remove(GSR view) throws Exception;

	public void close();

	// public DatabaseStats getStats(GSR view) throws Exception;

	public void storeTuples(GSR view, Packet wrappedTuples) throws Exception;

	public boolean useOSFile();

	public EngineSystem getEngineSystem();

	public String getId();

	// TODO [zabetak] Rethink about the last two methods if they should be part
	// of the interface
	public int getCursor(GSR view) throws Exception;

	public void closeCursor(int cursorId);

	public NTuple getNextRecord(int cursorId) throws Exception;
}
