package fr.inria.cedar.commons.tatooine.operators.physical;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.optimization.Visitor;

/**
 * Physical operator that accesses data from a RDF data source using Apache JENA.
 * The results it returns depend on any given query (and not only in the metadata associated with the RDF data source).
 * @author Oscar Mendoza
 */
public class PhyJENAEval extends JenaSPARQLQueryParameterisedOperator implements PhyEvalOperator {

	private static final Logger log = Logger.getLogger(PhyJENAEval.class);

	/** Universal version identifier for the PhyJENAEval class */
	private static final long serialVersionUID = -6637510814684845565L;

	/* Constructor */
	public PhyJENAEval(String query, GSR ref, NRSMD nrsmd) throws TatooineExecutionException {
		super(query, ref);
		this.nrsmd = inferMetadata(nrsmd, findOutputColumns(query));
	}
	
	/* Constructor */
	public PhyJENAEval(String query, GSR ref, ViewSchema schema) throws TatooineExecutionException {
		this(query, ref, schema.getNRSMD());
	}

	@Override
	public String getName() {
		return "PhyJENAEval" + (dbHolder!= null ? "(" + dbHolder.toString() + ")" : "");
	}

	@Override
	public String getName(int depth) {
		String spaceForIndent = getTabs(PRINTING_INDENTATION_TABS * depth);
		return "\n" + spaceForIndent + "PhyJENAEval" + (dbHolder!= null ? "(" + dbHolder.toString() + ")" : "");
	}
	
	@Override
	public String getDetailedName() {
		int cutLength = 30;
		String queryName = "(" + (queryStr.length() < cutLength ? queryStr : queryStr.substring(0, cutLength - 4) + "...") + ")";
		String baseName = dbHolder != null ? "[" + dbHolder.modelStr + "]" : "";
		return "PhyJENAEval" + "(" + queryName + baseName + ")";
	}

	@Override
	public NIterator copy() throws TatooineExecutionException {
		try {
			return new PhyJENAEval(queryStr, ref, nrsmd);
		} catch (Exception e) {
			log.error("Exception: ", e);
			return null;
		}
	}

    @Override
	public void accept(Visitor visitor) throws TatooineExecutionException {
        visitor.visit(this);
    }
    
	/** Attempts to determine the output columns of a given SQL query */
	private String findOutputColumns(String query) throws TatooineExecutionException {
		// SPARQL queries follow this syntax for their SELECT statements (@see https://www.w3.org/TR/rdf-sparql-query/#rSelectQuery): 
		// SELECT (DISTINCT|REDUCED) col1, ..., colN (FROM|WHERE) [...]		
		Pattern p = Pattern.compile("SELECT(\\s+(DISTINCT|REDUCED))?\\s+(.*?)\\s+(FROM|WHERE)");
		Matcher m = p.matcher(query);
		if (!m.find() || m.group(3) == null || m.group(3).isEmpty()) {
			String message = String.format("Failed to determine resulting metadata from input query: %s", query);
			log.error(message);
			throw new TatooineExecutionException(message);
		}
		String match = m.group(3).trim();
		if (match.equals("*")) {
			String msg = "Must specify one or more columns as part of the SELECT statement to restrict the query output";
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		// Replacing variable prefix symbol
		return match.replace("?", "");
	}

	@Override
	public NRSMD inferMetadata(NRSMD inputMD, String fieldsParam) throws TatooineExecutionException {
		List<String> paramFields = Arrays.asList(fieldsParam.split(" "));
		List<String> foundFields = new LinkedList<String>(paramFields);
		int counterCol = 0;
		int[] keepColumns = new int[paramFields.size()];			
		for (int i = 0; i < inputMD.colNames.length; i++) {
			String colName = inputMD.colNames[i];
			for (String field : paramFields) {
				if (field.trim().equals(colName)) {
					keepColumns[counterCol] = i;
					counterCol++; 
					foundFields.remove(colName);
				}
			}
		}		
		
		// All param fields in the query should also be present in the NRSMD
		if (!foundFields.isEmpty()) {
			String msg = String.format("The field(s) %s in the SELECT statement of the input query are not part of the input NRSMD [%s]", foundFields, String.join(" ", inputMD.colNames));
			log.error(msg);
			throw new TatooineExecutionException(msg);
		}
		
		NRSMD resultMD = NRSMD.makeProjectRSMD(inputMD, keepColumns);		
		return resultMD;
	}

}
