echo "Start Build"
cd estocada-commons/
cd commons-conjunctivequery/
mvn clean install -DskipTests
cd ..
cd commons-db/
mvn clean install -DskipTests
cd ..
cd commons-miscellaneous1/
mvn clean install -DskipTests
cd ..
cd commons-relationalschema/
mvn clean install -DskipTests
cd ..
cd commons-db-ontosql/
mvn clean install -DskipTests
cd ../..

cd estocada-PACB/
cd Chase/
mvn clean install -DskipTests
cd ..
cd Backchase/
mvn clean install -DskipTests
cd ..
cd CB/
mvn clean install -DskipTests
cd ../..

cd estocada-engine-Tatooine/
mvn clean install -DskipTests
cd ..

cd estocada-main/
mvn clean install -DskipTests
#mvn javadoc:javadoc
cd ..
cd estocada-tutorial/
mvn clean install -DskipTests
cd ..
