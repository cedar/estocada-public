grammar MADLIB; 
 
@header {
package fr.inria.oak.estocada.compiler.model.pm;
}

/* 
 * ==============
 * MADLIB Query
 * ==============
 */
 madQuery:
   viewName ':' madScript
 ;

/* ==============
 * View Name
 * ==============
 */
 viewName:
 ID
 ;
 
 /*
  * =============
  * Mad Script
  * =============
  */
  madScript:
  	 madStatemnet ';' (madStatemnet ';')*
  ;
  
  /* 
   * ==============
   * Mad Statements
   * ==============
   */
  madStatemnet
  :
  	madExpression 
  ; 
  /*
   * ==============
   * Mad Expression
   * ==============s
   */
   madExpression
   :	//Binary Operation
   		'SELECT madlib.matrix_mult(' matrixSourceName ',' matrixSourceName ',' matrixOutName ')'	 	 	#MatrixMulExpression
   		|'SELECT madlib.matrix_add(' matrixSourceName ',' matrixSourceName ',' matrixOutName')'		 	  	#MatrixAddExpression
   		|'SELECT madlib.matrix_elem_mult(' matrixSourceName ',' matrixSourceName ',' matrixOutName')' 		#MatrixMulElementwiseExpression
   		|'SELECT madlib.matrix_scalar_mult(' matrixSourceName ',' matrixOutName')'							#MatrixMulScalarExpression
   		//Unary Operations
   		|'SELECT madlib.matrix_trans(' matrixSourceName ',' matrixOutName ')'   							#MatrixTransposeExpression
   		|'SELECT madlib.matrix_inverse(' matrixSourceName ',' matrixOutName ')'								#MatrixInverseExpression
   ;	

 matrixSourceName:
  	ID
  ; 
 matrixOutName
 :
 	ID
 ; 
/*
 * ================
 * Primitives
 * ================
 */
ID
:
	[a-zA-Z_0-9] [a-zA-Z_0-9]*
;

WHITESPACE
:
	[ \t\n\r]+ -> skip
;

STRING
:
	'"'
	(
		ESCAPE
		| ~["\\]
	)* '"'
	| '\''
	(
		ESCAPE
		| ~['\\]
	)* '\''
;

fragment
ESCAPE
:
	'\\'
	(
		['"\\/bfnrt]
		| UNICODE
	)
;

fragment
UNICODE
:
	'u' HEX HEX HEX HEX
;

fragment
HEX
:
	[0-9a-fA-F]
;

INT
:
	'0'
	| [1-9] [0-9]*
;



/*
 * =========
 * Key Words
 * =========
 */
SELECT :

 S E L E C T
;

fragment
EXP
:
	(
		'E'
		| 'e'
	)
	(
		'+'
		| '-'
	)? INT
;
fragment
A
:
	[aA]
;

fragment
B
:
	[bB]
;

fragment
C
:
	[cC]
;

fragment
D
:
	[dD]
;

fragment
E
:
	[eE]
;

fragment
F
:
	[fF]
;

fragment
G
:
	[gG]
;

fragment
H
:
	[hH]
;

fragment
I
:
	[iI]
;

fragment
J
:
	[jJ]
;

fragment
K
:
	[kK]
;

fragment
L
:
	[lL]
;

fragment
M
:
	[mM]
;

fragment
N
:
	[nN]
;

fragment
O
:
	[oO]
;

fragment
P
:
	[pP]
;

fragment
Q
:
	[qQ]
;

fragment
R
:
	[rR]
;

fragment
S
:
	[sS]
;

fragment
T
:
	[tT]
;

fragment
U
:
	[uU]
;

fragment
V
:
	[vV]
;

fragment
W
:
	[wW]
;

fragment
X
:
	[xX]
;

fragment
Y
:
	[yY]
;

fragment
Z
:
	[zZ]
;


 