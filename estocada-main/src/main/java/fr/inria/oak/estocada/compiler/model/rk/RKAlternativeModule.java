package fr.inria.oak.estocada.compiler.model.rk;

public class RKAlternativeModule extends RKModule {
	@Override
	protected String getPropertiesFileName() {
		return "rk.properties";
	}
}
