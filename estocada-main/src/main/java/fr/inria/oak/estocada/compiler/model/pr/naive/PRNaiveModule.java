package fr.inria.oak.estocada.compiler.model.pr.naive;

import fr.inria.oak.estocada.compiler.model.pj.PJModule;

public class PRNaiveModule extends PJModule {
    @Override
    protected String getPropertiesFileName() {
        return "pj.naive.properties";
    }
}
