package fr.inria.oak.estocada.views.materialization.module;

import static com.google.common.base.Preconditions.checkNotNull;

import fr.inria.oak.estocada.qbt.walker.QBTMBaseListener;
import fr.inria.oak.estocada.views.materialization.module.blocks.QBTPattern;

/**
 * Abstract class for different QBT Return Listeners.
 * 
 * @author ranaalotaibi.
 *
 */
public abstract class QBTReturnListener extends QBTMBaseListener {

    /** QBT Pattern **/
    protected final QBTPattern qbtPattern;
    /** View Name **/
    protected String viewName;

    /** Constructor **/
    public QBTReturnListener(final String viewName, final QBTPattern qbtPattern) {
        this.viewName = checkNotNull(viewName);
        this.qbtPattern = checkNotNull(qbtPattern);
    }
}
