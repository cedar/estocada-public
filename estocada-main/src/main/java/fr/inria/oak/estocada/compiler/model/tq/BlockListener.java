package fr.inria.oak.estocada.compiler.model.tq;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * TQ BlockListener
 * 
 * @author Rana Alotaibi
 */
@Singleton
final class BlockListener {
    private static final Logger log = Logger.getLogger(BlockListener.class);

    private final PatternListener patternListener;
    private final ReturnTemplateListener returnTemplateListener;

    @Inject
    public BlockListener(final PatternListener patternListener, final ReturnTemplateListener returnTemplateListener) {
        this.patternListener = patternListener;
        this.returnTemplateListener = returnTemplateListener;
    }

    public RootBlock parse(final String queryName, final String str) throws ParseException {
        final BlockListenerAux listener = _parse(str);
        final Pattern pattern = patternListener.parse(listener.getPattern());
        return new RootBlock(queryName, pattern, returnTemplateListener.parse(listener.getReturnTemplate()));
    }

    private BlockListenerAux _parse(final String str) throws ParseException {
        final TQLLexer lexer = new TQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TQLParser parser = new TQLParser(tokens);
        final ParserRuleContext tree = parser.query();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final BlockListenerAux listener = new BlockListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getPattern() == null) {
            throw new ParseException(new IllegalStateException("Pattern expected."));
        }

        if (listener.getReturnTemplate() == null) {
            throw new ParseException(new IllegalStateException("Return template expected."));
        }

        return listener;
    }

    /*
     * We allow only strings that have pattern followed by return template.
     */
    private class BlockListenerAux extends TQLBaseListener {
        private String pattern;
        private String returnTemplate;

        public String getPattern() {
            return pattern;
        }

        public String getReturnTemplate() {
            return returnTemplate;
        }

        @Override
        public void enterFromClause(TQLParser.FromClauseContext ctx) {
            log.debug("Entering TQL FromClause: " + ctx.getText());
            if (pattern == null && returnTemplate != null) {
                System.out.println(ctx.getText());
                pattern = AntlrUtils.getFullText(ctx);
            }
        }

        @Override
        public void enterSelectClause(TQLParser.SelectClauseContext ctx) {
            log.debug("Entering TQL SelectClause: " + ctx.getText());
            if (pattern == null && returnTemplate == null) {
                returnTemplate = AntlrUtils.getFullText(ctx);
                log.debug("Return Template: " + returnTemplate);
            }
        }
    }
}
