package fr.inria.oak.estocada.compiler;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Block Encoder
 * 
 * @author ranaalotaibi
 *
 */
public interface BlockEncoder {
    ConjunctiveQuery encode(final Block block);
}
