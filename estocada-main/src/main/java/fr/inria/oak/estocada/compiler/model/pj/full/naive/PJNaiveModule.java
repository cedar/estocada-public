package fr.inria.oak.estocada.compiler.model.pj.full.naive;

import fr.inria.oak.estocada.compiler.model.pj.PJModule;

public class PJNaiveModule extends PJModule {
    @Override
    protected String getPropertiesFileName() {
        return "pj.naive.properties";
    }
}
