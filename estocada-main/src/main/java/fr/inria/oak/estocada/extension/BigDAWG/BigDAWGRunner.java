package fr.inria.oak.estocada.extension.BigDAWG;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.apache.log4j.Logger;

/**
 * BigDAWG Experiments
 * 
 * @author Rana Alotaibi
 */
public class BigDAWGRunner {
    private static final Logger LOGGER = Logger.getLogger(BigDAWGRunner.class);
    private static final String PATH = "src/main/resources/islandsQueries/";
    private static final String PATHCV = "src/main/resources/islandsQueries-V/";
    private static Writer WRITER;
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";

    /**
     * Scenario 1 Query
     * 
     * @throws IOException
     * @throws FileNotFoundException
     **/
    public static double QueryExample1() throws FileNotFoundException, IOException {
        double finalTime = 0;
        //        try {
        //            final long startTimeQuery = System.currentTimeMillis();
        //            final String textQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATH + "Query0" + 0 + "-Text"))));
        //            final FromPostgresRQToPostgresJQ fromPostgresRQToPostgresJQ = new FromPostgresRQToPostgresJQ();
        //            final MigrationResult resultText = fromPostgresRQToPostgresJQ.fromPostgresRQToPostgresJQ(textQuery);
        //            final double migrationTimeText = (resultText.getDurationMsec() / 1000.00);
        //            final long totalResultText = resultText.getCountExtractedElements();
        //            LOGGER.debug("Text Isalnd Query Execution and Migraton Time: " + migrationTimeText + " Total="
        //                    + totalResultText);
        //
        //            /** Query JSON island and migrate intermediate results **/
        //            final String jsonQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATH + "Query0" + 0 + "-JSON"))));
        //            final FromPostgresJQToPostgresRQ fromPostgresJQToPostgresRQ = new FromPostgresJQToPostgresRQ();
        //            final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresJQToPostgresRQ(jsonQuery);
        //            final long totalResultJSON = resultJSON.getCountExtractedElements();
        //            final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        //            LOGGER.debug("JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total="
        //                    + totalResultJSON);
        //
        //            /** Query Relational Island **/
        //
        //            DriverManager.registerDriver(new EmbeddedDriver());
        //            final String relationalQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATH + "Query0" + 0 + "-Relational"))));
        //            final Connection connectionPostgresRQ = DriverManager.getConnection(DB_URL);
        //            connectionPostgresRQ.setReadOnly(true);
        //            final Cancelable cancelable = new Cancelable();
        //            cancelable.startTimer(Cancelable.getPostgresConnectionsList(connectionPostgresRQ));
        //            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(relationalQuery);
        //            final ResultSet rsRQ = statement.executeQuery();
        //            int resCount = 0;
        //            while (rsRQ.next()) {
        //                resCount++;
        //            }
        //            rsRQ.close();
        //            statement.close();
        //            connectionPostgresRQ.close();
        //            cancelable.cancelTimer();
        //            final long endTimeQuery = System.currentTimeMillis();
        //            finalTime = ((endTimeQuery - startTimeQuery) / 1000.00);
        //            LOGGER.debug("Query0" + 0 + ": Total=" + resCount + " Took=" + finalTime + "s\n");
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        return finalTime;
    }

    /**
     * Scenario 1 Query
     * 
     * @throws IOException
     * @throws FileNotFoundException
     **/
    public static double RewritingExampl1() throws FileNotFoundException, IOException {
        double finalTime = 0;
        //        try {
        //            final long startTimeQuery = System.currentTimeMillis();
        //            final String textQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATHCV + "Query0" + 0 + "-Text"))));
        //            final FromPostgresRQToPostgresJQ fromPostgresRQToPostgresJQ = new FromPostgresRQToPostgresJQ();
        //            final MigrationResult resultText = fromPostgresRQToPostgresJQ.fromPostgresRQToPostgresJQ(textQuery);
        //            final double migrationTimeText = (resultText.getDurationMsec() / 1000.00);
        //            final long totalResultText = resultText.getCountExtractedElements();
        //            LOGGER.debug("Text Isalnd Query Execution and Migraton Time: " + migrationTimeText + " Total="
        //                    + totalResultText);
        //
        //            /** Query JSON island and migrate intermediate results **/
        //            final String jsonQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATHCV + "Query0" + 0 + "-JSON"))));
        //            final FromPostgresJQToPostgresRQ fromPostgresJQToPostgresRQ = new FromPostgresJQToPostgresRQ();
        //            final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresJQToPostgresRQ(jsonQuery);
        //            final long totalResultJSON = resultJSON.getCountExtractedElements();
        //            final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        //            LOGGER.debug("JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total="
        //                    + totalResultJSON);
        //
        //            /** Query Relational Island **/
        //
        //            DriverManager.registerDriver(new EmbeddedDriver());
        //            final String relationalQuery =
        //                    IOUtils.toString((new BufferedReader(new FileReader(PATHCV + "Query0" + 0 + "-Relational"))));
        //            final Connection connectionPostgresRQ = DriverManager.getConnection(DB_URL);
        //            connectionPostgresRQ.setReadOnly(true);
        //            final Cancelable cancelable = new Cancelable();
        //            cancelable.startTimer(Cancelable.getPostgresConnectionsList(connectionPostgresRQ));
        //            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(relationalQuery);
        //            final ResultSet rsRQ = statement.executeQuery();
        //            int resCount = 0;
        //            while (rsRQ.next()) {
        //                resCount++;
        //            }
        //            rsRQ.close();
        //            statement.close();
        //            connectionPostgresRQ.close();
        //            cancelable.cancelTimer();
        //            final long endTimeQuery = System.currentTimeMillis();
        //            finalTime = ((endTimeQuery - startTimeQuery) / 1000.00);
        //            LOGGER.debug("Query0" + 0 + ": Total=" + resCount + " Took=" + finalTime + "s\n");
        //        } catch (Exception e) {
        //            e.printStackTrace();
        //        }
        return finalTime;

    }

    private final static void openWriter() {
        try {
            WRITER = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATHCV + "output"), "utf-8"));
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private final static void closeWriter() {
        try {
            WRITER.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
