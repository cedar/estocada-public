package fr.inria.oak.estocada.stores.executores;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

/**
 * AsterixDB Query Executor
 * 
 * @author Rana Alotaibi
 *
 */
public class AsterixDBExecutor implements ExecutorInterface {

    private static final Logger LOGGER = Logger.getLogger(AsterixDBExecutor.class);
    private static final String QUERY_SERVICE_PATH = "/query/service";
    private static final String QUERY_CANCEL_PATH = "/admin/requests/running/";
    private static final String CLIENT_CONTEXT_ID = "CANCEL_PARAM";
    private Timer timer;
    private File file;
    private Writer writer;

    private String uri;

    @Override
    public void createConnection() {
        uri = "http://" + "127.0.0.1" + ":" + "19002";
        file = new File("debug");
        timer = new Timer();
        try {
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("debug"), "utf-8"));
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
        }
    }

    @Override
    public long executeQuery(String nativeQuery) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost apiHttpPost = new HttpPost(uri + QUERY_SERVICE_PATH);
        long queryTime = 0;
        String result = "";
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("statement", nativeQuery));
        params.add(new BasicNameValuePair("client_context_id", CLIENT_CONTEXT_ID));

        try {
            apiHttpPost.setEntity(new UrlEncodedFormEntity(params));
            final long start = System.currentTimeMillis();
            startTimer();
            InputStream inputStream = httpclient.execute(apiHttpPost).getEntity().getContent();
            final long end = System.currentTimeMillis();
            StringWriter writerInputstream = new StringWriter();
            IOUtils.copy(inputStream, writerInputstream, "UTF-8");
            result = writerInputstream.toString();
            if (result.contains("Syntax error")) {
                writer.write(nativeQuery + "\n\n" + result + "\n\n");
            }
            queryTime += (end - start);
        } catch (IOException e) {
            LOGGER.error("Couldn't connect to AsterixDB HTTP API", e);
        }
        return queryTime;
    }

    public boolean cancelQueryExecution() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpDelete endPoint = new HttpDelete(getCancelURI(CLIENT_CONTEXT_ID));
        try {
            InputStream inputStream = httpclient.execute(endPoint).getEntity().getContent();
            LOGGER.info("canceled!");
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            LOGGER.error("Could not cancel job for id " + CLIENT_CONTEXT_ID, e);
            return false;
        }
        return true;
    }

    private void startTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                LOGGER.info("**********Query Canceled Timeout************");
                cancelQueryExecution();
            }
        }, 1500000);
    }

    private String getCancelURI(String clientContextId) {
        return uri + QUERY_CANCEL_PATH + "?client_context_id=" + clientContextId;
    }

    @Override
    public void destroy() {
        LOGGER.info("**********Executor Canceled***********");
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        timer.cancel();
    }
}
