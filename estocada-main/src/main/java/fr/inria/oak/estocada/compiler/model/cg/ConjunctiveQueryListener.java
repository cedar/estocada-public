// Generated from ConjunctiveQuery.g4 by ANTLR 4.7.2

    package fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ConjunctiveQueryParser}.
 */
public interface ConjunctiveQueryListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#conjunctiveQuery}.
	 * @param ctx the parse tree
	 */
	void enterConjunctiveQuery(ConjunctiveQueryParser.ConjunctiveQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#conjunctiveQuery}.
	 * @param ctx the parse tree
	 */
	void exitConjunctiveQuery(ConjunctiveQueryParser.ConjunctiveQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#queryHeader}.
	 * @param ctx the parse tree
	 */
	void enterQueryHeader(ConjunctiveQueryParser.QueryHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#queryHeader}.
	 * @param ctx the parse tree
	 */
	void exitQueryHeader(ConjunctiveQueryParser.QueryHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#queryBody}.
	 * @param ctx the parse tree
	 */
	void enterQueryBody(ConjunctiveQueryParser.QueryBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#queryBody}.
	 * @param ctx the parse tree
	 */
	void exitQueryBody(ConjunctiveQueryParser.QueryBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#labelI}.
	 * @param ctx the parse tree
	 */
	void enterLabelI(ConjunctiveQueryParser.LabelIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#labelI}.
	 * @param ctx the parse tree
	 */
	void exitLabelI(ConjunctiveQueryParser.LabelIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#kindI}.
	 * @param ctx the parse tree
	 */
	void enterKindI(ConjunctiveQueryParser.KindIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#kindI}.
	 * @param ctx the parse tree
	 */
	void exitKindI(ConjunctiveQueryParser.KindIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#propertyI}.
	 * @param ctx the parse tree
	 */
	void enterPropertyI(ConjunctiveQueryParser.PropertyIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#propertyI}.
	 * @param ctx the parse tree
	 */
	void exitPropertyI(ConjunctiveQueryParser.PropertyIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#compI}.
	 * @param ctx the parse tree
	 */
	void enterCompI(ConjunctiveQueryParser.CompIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#compI}.
	 * @param ctx the parse tree
	 */
	void exitCompI(ConjunctiveQueryParser.CompIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#valueI}.
	 * @param ctx the parse tree
	 */
	void enterValueI(ConjunctiveQueryParser.ValueIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#valueI}.
	 * @param ctx the parse tree
	 */
	void exitValueI(ConjunctiveQueryParser.ValueIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#connectionI}.
	 * @param ctx the parse tree
	 */
	void enterConnectionI(ConjunctiveQueryParser.ConnectionIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#connectionI}.
	 * @param ctx the parse tree
	 */
	void exitConnectionI(ConjunctiveQueryParser.ConnectionIContext ctx);
	/**
	 * Enter a parse tree produced by {@link ConjunctiveQueryParser#nestValueI}.
	 * @param ctx the parse tree
	 */
	void enterNestValueI(ConjunctiveQueryParser.NestValueIContext ctx);
	/**
	 * Exit a parse tree produced by {@link ConjunctiveQueryParser#nestValueI}.
	 * @param ctx the parse tree
	 */
	void exitNestValueI(ConjunctiveQueryParser.NestValueIContext ctx);
}