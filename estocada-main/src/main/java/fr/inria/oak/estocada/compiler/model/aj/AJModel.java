package fr.inria.oak.estocada.compiler.model.aj;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

/**
 * AJ Model
 * 
 * @author Rana Alotaibi
 *
 */
public class AJModel extends Model {
    public final static String ID = "AJ";

    @Inject
    public AJModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
        super(ID, Format.JSON, Language.AQL, queryBlockTreeBuilder, blockEncoder);
    }
}
