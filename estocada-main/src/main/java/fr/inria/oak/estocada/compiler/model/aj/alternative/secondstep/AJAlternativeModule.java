package fr.inria.oak.estocada.compiler.model.aj.alternative.secondstep;

import fr.inria.oak.estocada.compiler.model.aj.AJModule;

public class AJAlternativeModule extends AJModule {
	@Override
	protected String getPropertiesFileName() {
		return "aj.alternative.second_step.properties";
	}
}
