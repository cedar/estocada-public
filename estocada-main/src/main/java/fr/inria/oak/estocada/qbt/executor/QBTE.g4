grammar QBTE;


@header {
package fr.inria.oak.estocada.qbt.executor;
}

/*
 * ====================
 * Query
 * ====================
 */

queryBlock
:
	 forPattern wherePattern returnPattern
;

/*
 * ============
 * For Pattern
 * ============
 */
forPattern
:
	'FOR' blockPattern
	(
		',' blockPattern
	)*
;


/*
 * ====================
 * Pattern
 * ====================
 */
blockPattern
:
	annotation ':' '{' pattern '}'
;

pattern
: 
  STRING 
  | sjPattern
;
annotation
:
	
	 'PR'
	| 'PJ'
	| 'SJ'

;

//whereClause
//:
// WHERE customizedStr
//;
/*
 * ====================
 * SJPattern
 * ====================
 */
sjPattern
:
	sjQuery
;
/*
 * ====================
 * Condition
 * ====================
 */
condition
:
	conditionAtom
	| '(' condition ')'
	| condition 'AND' condition
;
/*
 * ====================
 * Atom
 * ====================
 */
conditionAtom
:
	variable '=' variable
;

/*
 * ====================
 * SJQuery
 * ====================
 */
sjQuery
:
	sjCollectionName '/' 'query?q=' sjTextSearch? '&' sjProjectFields
;
/*
 * ==========================
 * Collection Name
 * ==========================
 */
sjCollectionName
:
	NAME
;

/*
 * ==========================
 * Text Search Predicate
 * ========================== 
 */
sjTextSearch
:
	sjConstant
;


/*
 * ==========================
 * Project Fields
 * ==========================
 */
sjProjectFields
:
	'fl=' sjFieldName
	(
		',' sjFieldName
	)*
;
 
 sjFieldName
:
	NAME
	| NAME ':' NAME
;

sjConstant
:
	STRING
;
variable
:
	NAME
;


/*
 * ============
 * Where Pattern
 * ============
 */
wherePattern
:
	'WHERE' condition
;

/*
 * ============
 * Return Pattern
 * ============
 */
returnPattern
:
	'RETURN' variable (',' variable)*
;


NAME
:
	[a-zA-Z_0-9] [a-zA-Z_0-9]*
;
WHITESPACE
:
	[ \t\n\r]+ -> skip
;

STRING
:
	'"'
	(
		ESCAPE
		| ~["\\]
	)* '"'
	| '\''
	(
		ESCAPE
		| ~['\\]
	)* '\''
;

fragment
ESCAPE
:
	'\\'
	(
		['"\\/bfnrt]
		| UNICODE
	)
;

RETURN
:
	R E T U R N
;

AJ
:
	A J
;

RK
:
	R K
;

PR
:
	P R
;

SPPJ
:
	S P P J
;

PJ
:
	P J
;

SJ
:
	S J
;

XQ
:
	X Q
;

TQ
:
	T Q
;

AND
:
	A N D
;

FROM
:
	F R O M
;

IN
:
	I N
;

SELECT
:
	S E L E C T
;

VALUE 
:
	 V A L U E
; 

AS
:
	A S
;

LIKE
:
	L I K E
;

QUERY
:
	Q U E R Y
;
USE :
 U S E 
;

fragment
UNICODE
:
	'u' HEX HEX HEX HEX
;

fragment
HEX
:
	[0-9a-fA-F]
;
/*
 * ====================
 * Fragments
 * ====================
 */
fragment
A
:
	[aA]
;

fragment
B
:
	[bB]
;

fragment
C
:
	[cC]
;

fragment
D
:
	[dD]
;

fragment
E
:
	[eE]
;

fragment
F
:
	[fF]
;

fragment
G
:
	[gG]
;

fragment
H
:
	[hH]
;

fragment
I
:
	[iI]
;

fragment
J
:
	[jJ]
;

fragment
K
:
	[kK]
;

fragment
L
:
	[lL]
;

fragment
M
:
	[mM]
;

fragment
N
:
	[nN]
;

fragment
O
:
	[oO]
;

fragment
P
:
	[pP]
;

fragment
Q
:
	[qQ]
;

fragment
R
:
	[rR]
;

fragment
S
:
	[sS]
;

fragment
T
:
	[tT]
;

fragment
U
:
	[uU]
;

fragment
V
:
	[vV]
;

fragment
W
:
	[wW]
;

fragment
X
:
	[xX]
;

fragment
Y
:
	[yY]
;

fragment
Z
:
	[zZ]
;