package fr.inria.oak.estocada.rewriting.physical;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;

import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.logical.LogJoin;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperatorVisitor;
import fr.inria.cedar.commons.tatooine.operators.logical.LogPJSQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSolrEval;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLREval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;

/**
 * This class walks the logical operators and constructs the corresponding physical operators.
 * The order of the join in the same order of the join in the logical plan (the join order in logical plan is random)
 * 
 * @author ranaalotaibi
 *
 */
public class LogPlanListener implements LogOperatorVisitor {
    private static final Logger LOGGER = Logger.getLogger(LogPlanListener.class);
    protected Map<String, NIterator> phyEvals;
    protected Stack<PhyBindJoin> bindJoinOperatos;

    public LogPlanListener() {
        bindJoinOperatos = new Stack<PhyBindJoin>();
        phyEvals = new HashMap<String, NIterator>();

    }

    @Override
    public void visit(final LogSQLEval logSQLEval) {
        LOGGER.debug("Entering LogSQLEval: " + logSQLEval);
        LOGGER.debug("Entering LogSQLEval Query: " + logSQLEval.getQueryString());
        LOGGER.debug("Entering LogSQLEval NRSMD: " + logSQLEval.getNRSMD());
        try {
            phyEvals.put(logSQLEval.getName(), new PhySQLEval(logSQLEval.getQueryString(),
                    logSQLEval.getStorageReference(), logSQLEval.getNRSMD()));
        } catch (TatooineExecutionException e) {
            LOGGER.debug("Could not construct " + logSQLEval.getName() + " PhyEval :" + e.getMessage());
        }
    }

    @Override
    public void visit(final LogPJSQLEval logPJSQLEval) {
        LOGGER.debug("Entering logPJQLEval: " + logPJSQLEval.getName());
        LOGGER.debug("Entering logPJQLEval Query: " + logPJSQLEval.getQueryString());
        LOGGER.debug("Entering logPJQLEval NRSDM: " + logPJSQLEval.getNRSMD());
        try {
            phyEvals.put(logPJSQLEval.getName(), new PhyPostgresJSONEval(logPJSQLEval.getQueryString(),
                    logPJSQLEval.getGSR(), logPJSQLEval.getNRSMD()));
        } catch (TatooineExecutionException e) {
            LOGGER.debug("Could not construct " + logPJSQLEval.getName() + " PhyEval :" + e.getMessage());
        }
    }

    @Override
    public void visit(final LogSolrEval logSolrEval) {
        LOGGER.debug("Entering logSolrEval: " + logSolrEval.getName());
        LOGGER.debug("Entering logSolrEval Query : " + logSolrEval.getQueryString());
        LOGGER.debug("Entering logSolrEval NRSMD: " + logSolrEval.getNRSMD());
        try {
            phyEvals.put(logSolrEval.getName(), new PhySOLREval(logSolrEval.getQueryString(), logSolrEval.getFields(),
                    (GSR) logSolrEval.getGSR(), logSolrEval.getNRSMD()));
        } catch (TatooineExecutionException | SolrServerException | IOException | TatooineException e) {
            LOGGER.debug("Could not construct " + logSolrEval.getName() + " PhyEval :" + e.getMessage());
        }

    }

    @Override
    public void visit(final LogJoin joinOperator) {
        LOGGER.debug("Entering join operator: " + joinOperator);
        if (bindJoinOperatos.isEmpty()) {
            LOGGER.debug("Entering join operator: " + joinOperator.getName());
            final LogOperator left = joinOperator.getChildren().get(0);
            final LogOperator right = joinOperator.getChildren().get(1);
            try {
                final BindAccess bindAccess = PhyEvalConstructorUtils.constructBindAccess(right,
                        getRightBindJoinPattern((ConjunctivePredicate) joinOperator.getPredicate()));
                final PhyBindJoin bindJoin = new PhyBindJoin(phyEvals.get(left.getName()), bindAccess,
                        getLeftBindJoinPattern((ConjunctivePredicate) joinOperator.getPredicate()));
                bindJoinOperatos.push(bindJoin);
            } catch (Exception e) {
                LOGGER.debug("Could not construct a physical operator " + e.getMessage());
            }

        } else {
            final PhyBindJoin left = bindJoinOperatos.pop();
            final LogOperator right = joinOperator.getChildren().get(1);
            try {
                final BindAccess bindAccess = PhyEvalConstructorUtils.constructBindAccess(right,
                        getRightBindJoinPattern((ConjunctivePredicate) joinOperator.getPredicate()));
                final PhyBindJoin bindJoin = new PhyBindJoin(left, bindAccess,
                        getLeftBindJoinPattern((ConjunctivePredicate) joinOperator.getPredicate()));
                bindJoinOperatos.push(bindJoin);
            } catch (Exception e) {
                LOGGER.debug("Could not construct a physical operator " + e.getMessage());
            }
        }

    }

    /**
     * Return the physical plan root operator.
     * 
     * @return This function returns the physical plan root operator.
     */
    public NIterator physicalPlanRootOperator() {
        if (bindJoinOperatos.size() > 1) {
            LOGGER.error("Expected one root physical operator");
            return null;
        } else {
            return bindJoinOperatos.pop();
        }

    }

    /**
     * Get left bind join pattern from conjunctive predicate in LogJoin operator.
     * 
     * @param predicate
     */
    private int[] getLeftBindJoinPattern(final ConjunctivePredicate predicate) {
        final Predicate[] predicates = predicate.getPreds();
        final int[] bindPattern = new int[predicates.length];
        for (int i = 0; i < predicates.length; i++) {
            bindPattern[i] = ((SimplePredicate) predicates[i]).getColumn();
        }
        return bindPattern;
    }

    /**
     * Get right bind join pattern from conjunctive predicate in LogJoin operator.
     * 
     * @param predicate
     */
    private int[] getRightBindJoinPattern(final ConjunctivePredicate predicate) {
        final Predicate[] predicates = predicate.getPreds();
        final int[] bindPattern = new int[predicates.length];
        for (int i = 0; i < predicates.length; i++) {
            bindPattern[i] = ((SimplePredicate) predicates[i]).getOtherColumn();
        }
        return bindPattern;
    }

}
