package fr.inria.oak.estocada.compiler.model.xq;

/**
 * @author Damian Bursztyn
 */
enum PathExpressionOptional {
	ENDS_WITH_TEXT;
}
