/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package fr.inria.oak.estocada.rewriter.legacy;

import java.util.ArrayList;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.constraints.Tgd;

/**
 * A TGD with explicit equalities
 */
public class TgdWithEq extends ConstraintWithEq
{
	private ArrayList<Atom> conclusion;

	/**
	 *  Constructor
	 *  
	 *  @param tgd
	 *  		The TGD without equalities
	 */
	public TgdWithEq(Tgd tgd)
	{	
		super();
		Utils.FromBodyToBodyWithEq(tgd.getPremise(), premiseRel, premiseEq);
		conclusion = new ArrayList<Atom>();
		for (Atom oldAtom:tgd.getConclusion())
		{
			Atom newAtom = Utils.GetTransformedRelational(oldAtom, tgd.getPremise(), premiseRel);
			conclusion.add(newAtom);
		}
	}

	/**
	 *  Gets the TGD's conclusion
	 *  
	 *  @return the TGD's conclusion
	 */
	public ArrayList<Atom> getConclusion() {
		return conclusion;
	}
	
	@Override
	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return Utils.fromAtomsToString(premiseRel)+","+Utils.fromEqsToString(premiseEq)+"->"+Utils.fromAtomsToString(conclusion);
	}
}


