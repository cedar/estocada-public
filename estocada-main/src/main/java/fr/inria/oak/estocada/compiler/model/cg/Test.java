package fr.inria.oak.estocada.compiler.model.cg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        String queryFileName = "graph/Ex_for_demo/query/query2.cypher";
        List<String> viewFileNames = new ArrayList<>(
                Arrays.asList("graph/Ex_for_demo/view/view1.cypher", "graph/Ex_for_demo/view/view2.cypher",
                        "graph/Ex_for_demo/view/view3.cypher", "graph/Ex_for_demo/view/view4.cypher",
                        "graph/Ex_for_demo/view/view5.cypher", "graph/Ex_for_demo/view/view6.cypher",
                        "graph/Ex_for_demo/view/view7.cypher", "graph/Ex_for_demo/view/view8.cypher"));
        String rewriteFolderPath = "src/main/resources/graph/output/", templatesPath = "graph/templates/ldbc/";
        Rewriter rewriter = new Rewriter(queryFileName, viewFileNames, rewriteFolderPath, templatesPath, false);
        //        List<String> cypherQueries = rewriter.rewrite();
        //        System.out.println(
        //                "\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------"
        //                        + "\nRewriting results:\n");
        //        for (String cypherQuery : cypherQueries) {
        //            System.out.println(cypherQuery);
        //        }
    }

}
