package fr.inria.oak.estocada.main.scenarios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;

import com.google.gson.JsonObject;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.oak.commons.miscellaneous.FileUtils;
import fr.inria.oak.estocada.extension.BigDAWG.FromPostgresRQToPostgresJQ;
import fr.inria.oak.estocada.extension.BigDAWG.FromSolrToPostgres;
import istc.bigdawg.migration.MigrationResult;

/**
 * Run MIMIC Scenarios on Tatooine (fixed plan order)
 * 
 * @author ranaalotaibi
 *
 */
public class BigDAWGScenariosMIMIC {
    private static final Logger LOGGER = Logger.getLogger(BigDAWGScenariosMIMIC.class);
    private static final String MIMIC_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";

    public static long QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema gdeltSchema = catalog.getViewSchema("MIMIC");
        final GSR gsrRE = (GSR) catalog.getStorageReference("MIMIC");

        final long start = System.nanoTime();
        final PhyPostgresJSONEval queryEval = new PhyPostgresJSONEval(queryPJ, gsrRE, gdeltSchema.getNRSMD());
        int resultCount = 0;
        queryEval.open();
        while (queryEval.hasNext()) {
            NTuple next = queryEval.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;
    }

    public static long Q1RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q2RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q3RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q4RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q5RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "embolism";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q6RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q7RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q8RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q9RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q10RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "cannulaes";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q11RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q12RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q13RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q14RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q15RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q16RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q17RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q18RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q19RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "embolism";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q20RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q21RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q22RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q23RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q24RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "cannulaes";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q25RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q26RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q27RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q28RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q29RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q30RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q31RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q32RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q33RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "embolism";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q34RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q35RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q36RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q37RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q38RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q39RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q40RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "intracranial";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q41RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q42RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q43RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "pneumonia";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q44RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "specis";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q45RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "tachypnea";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q46RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        /** Query Solr Island **/
        final String textQuery = "pneumonia";
        final String fields = "subject_id";
        //final String collectionName = "mimicnote";
        final String collectionName = "mimic_notes";
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(textQuery);
        solrQuery.setFields(fields);
        int resCount = 0;
        LOGGER.debug("Solr Query: " + solrQuery.toString());

        /** Text Island To Relational Island **/

        /** Postgres Destination info **/
        final String SJDF = "mimiciii.SJDF";
        final String CREATE_TABLE_SJDF = "CREATE TABLE " + SJDF + " (SUBJECT_ID INTEGER);";

        final FromSolrToPostgres fromSolrToPostgres =
                new FromSolrToPostgres(MIMIC_URL, SJDF, CREATE_TABLE_SJDF, collectionName);
        final MigrationResult resultText = fromSolrToPostgres.fromSolrToPostgresQuery(solrQuery);
        final long totalResultText = resultText.getCountExtractedElements();
        final double totalMigrationTime = resultText.getDurationMsec() / 1000.00;
        LOGGER.debug(" Total=" + totalResultText + "Time=" + totalMigrationTime);

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER, DOB varchar(80));";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q50RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q51RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q52RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q53RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q54RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q55RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q56RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q57RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q58RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q59RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        long start = System.nanoTime();
        int resCount = 0;

        /** Text Island To Relational Island **/

        /** Relational Island to JSON Island **/
        /** Postgres Destination info **/
        final String REDF = "mimiciii.REDF";
        final String CREATE_TABLE_REDF = "CREATE TABLE " + REDF + " (SUBJECT_ID INTEGER);";
        final String relationalIsalnd = FileUtils.readStringFromFile(path + "SJRE.query");
        final FromPostgresRQToPostgresJQ fromPostgresJQToPostgresRQ =
                new FromPostgresRQToPostgresJQ(MIMIC_URL, MIMIC_URL, REDF, CREATE_TABLE_REDF);
        final MigrationResult resultJSON = fromPostgresJQToPostgresRQ.fromPostgresRQToPostgresJQ(relationalIsalnd);
        final long totalResultJSON = resultJSON.getCountExtractedElements();
        final double migrationTimeJSON = (resultJSON.getDurationMsec() / 1000.00);
        LOGGER.debug(
                "JSON Island Query Execution and Migraton Time: " + migrationTimeJSON + " Total=" + totalResultJSON);
        /** Query JSON Island **/
        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            final String jsonIsalnd = FileUtils.readStringFromFile(path + "PJDF.query");

            final Connection connectionPostgresRQ = DriverManager.getConnection(MIMIC_URL);
            Statement statementUpate = null;
            try {
                statementUpate = connectionPostgresRQ.createStatement();
                statementUpate.executeUpdate("SET max_parallel_workers_per_gather = 0;");
                statementUpate.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                throw ex;
            } finally {
                if (statementUpate != null) {
                    statementUpate.close();
                }
            }
            connectionPostgresRQ.setReadOnly(true);
            final PreparedStatement statement = connectionPostgresRQ.prepareStatement(jsonIsalnd);
            final ResultSet rsRQ = statement.executeQuery();
            final ResultSetMetaData rsmetaData = rsRQ.getMetaData();
            int columnCount = rsmetaData.getColumnCount();
            JsonObject obj = new JsonObject();
            while (rsRQ.next()) {
                for (int i = 0; i < columnCount; i++) {
                    if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                        final int value = (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        if (key.equals("SID")) {
                            obj.addProperty("SUBJECT_ID", value);
                        } else {
                            obj.addProperty(key, value);
                        }
                    } else {
                        final String value = rsRQ.getString(rsmetaData.getColumnName(i + 1));
                        final String key = rsmetaData.getColumnName(i + 1).toUpperCase();
                        obj.addProperty(key, value);

                    }

                }
                resCount++;
                obj = new JsonObject();
            }
            rsRQ.close();
            statement.close();
            connectionPostgresRQ.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }
}
