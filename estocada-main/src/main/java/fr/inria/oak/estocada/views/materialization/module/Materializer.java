package fr.inria.oak.estocada.views.materialization.module;

/**
 * Abstract class for various models materializers.
 * 
 * @author ranaalotaibi
 *
 */
public interface Materializer {
    public void materialize() throws MaterializerException;
}
