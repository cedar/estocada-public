package fr.inria.oak.estocada.compiler.model.pr;

import java.util.List;
import java.util.function.Function;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Block;
import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Condition;
import fr.inria.oak.estocada.compiler.model.rk.Utils;

public class PRBlockEncoder implements BlockEncoder {

	@Override
	public ConjunctiveQuery encode(final Block block) {
		return new ConjunctiveQuery(block.getId(), block.getPattern()
				.getLocalDefinedVariables(), block.getPattern().encoding(conditionEncoder()));
	}
	public Function<Condition, List<Atom>> conditionEncoder() {
		return Utils.conditionEncoding;
	}
}
