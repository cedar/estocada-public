package fr.inria.oak.estocada.main;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.qbt.naive.QBTNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.qbt.executor.QBTExecutorBuilder;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriting.logical.RewritingToLogicalPlanGenerator;
import fr.inria.oak.estocada.rewriting.physical.LogicalToNaivePhysicalPlanGenerator;
import fr.inria.oak.estocada.utils.CMQRWRunnerUtils;
import fr.inria.oak.estocada.utils.RunnerUtils;

/**
 * This class is used to run an end-to-end ESTOCADA scenario on Tatooine.
 *
 * @author Rana Alotaibi
 *
 */
@Singleton
public class EstocadaRunner {
    private static final Logger LOGGER = Logger.getLogger(EstocadaRunner.class);
    private final String inputQuerysAndViewsFolder;

    @Inject
    private EstocadaRunner(@Named("cases.folder") final String inputQuerysAndViewsFolder) {
        Logger.getRootLogger().setLevel(Level.INFO);

        this.inputQuerysAndViewsFolder = checkNotNull(inputQuerysAndViewsFolder);
        try {
            Parameters.init();
            RegisterMetaDataManual.registerMetaData();
        } catch (Exception e) {
            LOGGER.error("Could not instantiates the execution engine (Tatooine): " + e.getMessage());
        }

    }

    /**
     * Run an end-to-end ESTOCADA scenarios rewritings on top of Tatooine.
     * Single Model Query - Cross Model Views - MIMIC data
     * 
     * @throws Exception
     */
    public void runTattoineSMQCMV() throws Exception {

        final File directory = new File(inputQuerysAndViewsFolder);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                if (!folder.getName().equals("Q44")) {
                    continue;
                }
                //Query 
                //TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                //Rewriting
                long startTime = System.currentTimeMillis();
                final String qbtQuery = RunnerUtils.readQuery(folder.getAbsolutePath());
                final List<String> views = RunnerUtils.readViews(folder.getAbsolutePath());
                final List<ConjunctiveQuery> rewritings = findRewriting(qbtQuery, views);
                LOGGER.info("Query Rewriitng :" + rewritings.get(0).toString());
                final LogOperator logicalPlanRootOperator =
                        new RewritingToLogicalPlanGenerator(rewritings.get(0)).generate();
                LOGGER.info("Logical Plan :" + logicalPlanRootOperator.getName());
                final NIterator rewritingPhyPlan =
                        new LogicalToNaivePhysicalPlanGenerator(logicalPlanRootOperator).generate();
                rewritingPhyPlan.open();
                int count = 0;
                while (rewritingPhyPlan.hasNext()) {
                    final NTuple ntuple = rewritingPhyPlan.next();
                    count++;
                }
                long entTime = System.currentTimeMillis();
                System.out.println("ResultCount : " + count + " Time : " + ((entTime - startTime) / 1000.00));
                rewritingPhyPlan.close();
            }
        }

    }

    /**
     * Run an end-to-end ESTOCADA scenarios rewritings on top of Tatooine.
     * Cross Model Query - Single Model Views - MIMIC data
     * 
     * @throws Exception
     */
    public void runTattoineCMQSMV() throws Exception {

        final File directory = new File(inputQuerysAndViewsFolder);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                if (!(folder.getName().equals("Q46"))) {
                    continue;
                }
                //Query
                long queryStartTime = System.currentTimeMillis();
                final String query = CMQRWRunnerUtils.readQBTEQuery(folder.getAbsolutePath());
                final QBTExecutorBuilder qbtExecutorBuilder = new QBTExecutorBuilder(query);
                qbtExecutorBuilder.parse();
                final PhyBindJoin phyBindJoin = qbtExecutorBuilder.getPhyPlan();
                int resultCount = 0;
                phyBindJoin.open();
                while (phyBindJoin.hasNext()) {
                    NTuple next = phyBindJoin.next();
                    resultCount++;
                }
                phyBindJoin.close();
                long queryEndTime = System.currentTimeMillis();
                System.out.println("Query ResultCount : " + resultCount + " Time : "
                        + ((queryEndTime - queryStartTime) / 1000.00));

                //Rewriting
                long RWStartTime = System.currentTimeMillis();
                final String qbtQuery = CMQRWRunnerUtils.readQuery(folder.getAbsolutePath());
                final List<String> views = CMQRWRunnerUtils.readViews(folder.getAbsolutePath());

                final List<ConjunctiveQuery> rewritings = CMQRWRunnerUtils.findRewriting(qbtQuery, views);
                LOGGER.info("Query Rewriitng :" + rewritings.get(0).toString());
                final LogOperator logicalPlanRootOperator =
                        new RewritingToLogicalPlanGenerator(rewritings.get(0)).generate();
                LOGGER.info("Logical Plan :" + logicalPlanRootOperator.getName());
                final NIterator rewritingPhyPlan =
                        new LogicalToNaivePhysicalPlanGenerator(logicalPlanRootOperator).generate();
                rewritingPhyPlan.open();
                int count = 0;
                while (rewritingPhyPlan.hasNext()) {
                    final NTuple ntuple = rewritingPhyPlan.next();
                    count++;
                }
                long RWEndTime = System.currentTimeMillis();
                System.out.println("ResultCount : " + count + " Time : " + ((RWEndTime - RWStartTime) / 1000.00));
                rewritingPhyPlan.close();

            }
        }

    }

    /**
     * Run an end-to-end ESTOCADA scenarios rewritings on top of tatooine.
     * 
     * @throws Exception
     */
    public void runBigDAWG() throws Exception {

    }

    /**
     * Process QBT scenario
     * 
     * @param qbtQuery
     *            qbt query
     * @param qbtViews
     *            qbt views
     * @return found rewritings
     * @throws Exception
     */
    private List<ConjunctiveQuery> findRewriting(final String qbtQuery, final List<String> qbtViews) throws Exception {
        //********************
        //* Encode Query
        //********************
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        final ConjunctiveQuery query =
                Utils.parseQuery(RunnerUtils.encodeUserQuery(RunnerUtils.preProcessQuery(qbtQuery)));
        System.out.print("Query:- " + query);

        //********************
        //* Encode Views
        //********************
        final List<QueryBlockTree> views = new ArrayList<QueryBlockTree>();
        final QBTNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class);
        final StringBuilder fwStrBuilder = new StringBuilder();
        final StringBuilder gsStrBuilder = new StringBuilder();
        for (final String view : qbtViews) {
            try {
                QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
                mixedBuilder.setRealtions(null);
                QueryBlockTree nbt = mixedBuilder.buildQueryBlockTree(RunnerUtils.preProcessQuery(view));
                views.add(nbt);

                final Context context = compiler0.compileContext(nbt,
                        new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), true);

                context.getForwardConstraints().stream().forEach(r -> fwStrBuilder.append(r.toString()).append("\n"));

                context.getGlobalSchema().getRelations().stream()
                        .forEach(r -> gsStrBuilder.append(r.toString()).append("\n"));
                QBTQueryBlockTreeBuilder.reIntilize();
            } catch (fr.inria.oak.estocada.compiler.exceptions.ParseException e) {
                throw new RuntimeException(e);
            }
        }
        //********************
        //* Find rewriting
        //********************
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(query);
        }

        return reformulations;
    }

    private List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(QBTNaiveQueryBlockTreeCompiler.class));
        return compilers;
    }

    private List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }

}