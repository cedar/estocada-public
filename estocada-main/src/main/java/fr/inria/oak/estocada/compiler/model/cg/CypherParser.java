// Generated from Cypher.g4 by ANTLR 4.7.2

    package  fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CypherParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		OPTIONAL=39, MATCH=40, CREATE=41, RETURN=42, WITH=43, AS=44, ORDER=45, 
		BY=46, LIMIT=47, ASCENDING=48, ASC=49, DESCENDING=50, DESC=51, WHERE=52, 
		AND=53, TRUE=54, FALSE=55, CASE=56, ELSE=57, END=58, WHEN=59, THEN=60, 
		StringLiteral=61, EscapedChar=62, HexInteger=63, DecimalInteger=64, OctalInteger=65, 
		HexLetter=66, HexDigit=67, Digit=68, NonZeroDigit=69, NonZeroOctDigit=70, 
		OctDigit=71, ZeroDigit=72, ExponentDecimalReal=73, RegularDecimalReal=74, 
		FILTER=75, EXTRACT=76, UnescapedSymbolicName=77, IdentifierStart=78, IdentifierPart=79, 
		EscapedSymbolicName=80, SP=81, WHITESPACE=82, Comment=83;
	public static final int
		RULE_oC_Cypher = 0, RULE_oC_Statement = 1, RULE_oC_Query = 2, RULE_oC_RegularQuery = 3, 
		RULE_oC_SingleQuery = 4, RULE_oC_SinglePartQuery = 5, RULE_oC_MultiPartQuery = 6, 
		RULE_oC_UpdatingClause = 7, RULE_oC_ReadingClause = 8, RULE_oC_Match = 9, 
		RULE_oC_Create = 10, RULE_oC_Return = 11, RULE_oC_With = 12, RULE_oC_ProjectionBody = 13, 
		RULE_oC_ProjectionItems = 14, RULE_oC_ProjectionItem = 15, RULE_oC_Order = 16, 
		RULE_oC_Limit = 17, RULE_oC_SortItem = 18, RULE_oC_Where = 19, RULE_oC_Pattern = 20, 
		RULE_oC_PatternPart = 21, RULE_oC_AnonymousPatternPart = 22, RULE_oC_PatternElement = 23, 
		RULE_oC_NodePattern = 24, RULE_oC_PatternElementChain = 25, RULE_oC_RelationshipPattern = 26, 
		RULE_oC_RelationshipDetail = 27, RULE_oC_Properties = 28, RULE_oC_RelationshipTypes = 29, 
		RULE_oC_NodeLabels = 30, RULE_oC_NodeLabel = 31, RULE_oC_LabelName = 32, 
		RULE_oC_RelTypeName = 33, RULE_oC_Expression = 34, RULE_oC_AndExpression = 35, 
		RULE_oC_ComparisonExpression = 36, RULE_oC_StringListNullOperatorExpression = 37, 
		RULE_oC_ListOperatorExpression = 38, RULE_oC_PropertyOrLabelsExpression = 39, 
		RULE_oC_Atom = 40, RULE_oC_Literal = 41, RULE_oC_BooleanLiteral = 42, 
		RULE_oC_ListLiteral = 43, RULE_oC_PartialComparisonExpression = 44, RULE_oC_ParenthesizedExpression = 45, 
		RULE_oC_PropertyLookup = 46, RULE_oC_CaseExpression = 47, RULE_oC_CaseAlternatives = 48, 
		RULE_oC_Variable = 49, RULE_oC_NumberLiteral = 50, RULE_oC_MapLiteral = 51, 
		RULE_oC_PropertyKeyName = 52, RULE_oC_IntegerLiteral = 53, RULE_oC_DoubleLiteral = 54, 
		RULE_oC_SchemaName = 55, RULE_oC_ReservedWord = 56, RULE_oC_SymbolicName = 57, 
		RULE_oC_LeftArrowHead = 58, RULE_oC_RightArrowHead = 59, RULE_oC_Dash = 60;
	private static String[] makeRuleNames() {
		return new String[] {
			"oC_Cypher", "oC_Statement", "oC_Query", "oC_RegularQuery", "oC_SingleQuery", 
			"oC_SinglePartQuery", "oC_MultiPartQuery", "oC_UpdatingClause", "oC_ReadingClause", 
			"oC_Match", "oC_Create", "oC_Return", "oC_With", "oC_ProjectionBody", 
			"oC_ProjectionItems", "oC_ProjectionItem", "oC_Order", "oC_Limit", "oC_SortItem", 
			"oC_Where", "oC_Pattern", "oC_PatternPart", "oC_AnonymousPatternPart", 
			"oC_PatternElement", "oC_NodePattern", "oC_PatternElementChain", "oC_RelationshipPattern", 
			"oC_RelationshipDetail", "oC_Properties", "oC_RelationshipTypes", "oC_NodeLabels", 
			"oC_NodeLabel", "oC_LabelName", "oC_RelTypeName", "oC_Expression", "oC_AndExpression", 
			"oC_ComparisonExpression", "oC_StringListNullOperatorExpression", "oC_ListOperatorExpression", 
			"oC_PropertyOrLabelsExpression", "oC_Atom", "oC_Literal", "oC_BooleanLiteral", 
			"oC_ListLiteral", "oC_PartialComparisonExpression", "oC_ParenthesizedExpression", 
			"oC_PropertyLookup", "oC_CaseExpression", "oC_CaseAlternatives", "oC_Variable", 
			"oC_NumberLiteral", "oC_MapLiteral", "oC_PropertyKeyName", "oC_IntegerLiteral", 
			"oC_DoubleLiteral", "oC_SchemaName", "oC_ReservedWord", "oC_SymbolicName", 
			"oC_LeftArrowHead", "oC_RightArrowHead", "oC_Dash"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "';'", "'*'", "','", "'='", "'('", "')'", "'['", "']'", "':'", 
			"'..'", "'<>'", "'<'", "'>'", "'<='", "'>='", "'.'", "'{'", "'}'", "'\u27E8'", 
			"'\u3008'", "'\uFE64'", "'\uFF1C'", "'\u27E9'", "'\u3009'", "'\uFE65'", 
			"'\uFF1E'", "'-'", "'\u00AD'", "'\u2010'", "'\u2011'", "'\u2012'", "'\u2013'", 
			"'\u2014'", "'\u2015'", "'\u2212'", "'\uFE58'", "'\uFE63'", "'\uFF0D'", 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, "'0'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, "OPTIONAL", "MATCH", "CREATE", "RETURN", "WITH", "AS", 
			"ORDER", "BY", "LIMIT", "ASCENDING", "ASC", "DESCENDING", "DESC", "WHERE", 
			"AND", "TRUE", "FALSE", "CASE", "ELSE", "END", "WHEN", "THEN", "StringLiteral", 
			"EscapedChar", "HexInteger", "DecimalInteger", "OctalInteger", "HexLetter", 
			"HexDigit", "Digit", "NonZeroDigit", "NonZeroOctDigit", "OctDigit", "ZeroDigit", 
			"ExponentDecimalReal", "RegularDecimalReal", "FILTER", "EXTRACT", "UnescapedSymbolicName", 
			"IdentifierStart", "IdentifierPart", "EscapedSymbolicName", "SP", "WHITESPACE", 
			"Comment"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Cypher.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public CypherParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class OC_CypherContext extends ParserRuleContext {
		public OC_StatementContext oC_Statement() {
			return getRuleContext(OC_StatementContext.class,0);
		}
		public TerminalNode EOF() { return getToken(CypherParser.EOF, 0); }
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_CypherContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Cypher; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Cypher(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Cypher(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Cypher(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_CypherContext oC_Cypher() throws RecognitionException {
		OC_CypherContext _localctx = new OC_CypherContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_oC_Cypher);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(122);
				match(SP);
				}
			}

			setState(125);
			oC_Statement();
			setState(130);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(126);
					match(SP);
					}
				}

				setState(129);
				match(T__0);
				}
				break;
			}
			setState(133);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(132);
				match(SP);
				}
			}

			setState(135);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_StatementContext extends ParserRuleContext {
		public OC_QueryContext oC_Query() {
			return getRuleContext(OC_QueryContext.class,0);
		}
		public OC_StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_StatementContext oC_Statement() throws RecognitionException {
		OC_StatementContext _localctx = new OC_StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_oC_Statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(137);
			oC_Query();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_QueryContext extends ParserRuleContext {
		public OC_RegularQueryContext oC_RegularQuery() {
			return getRuleContext(OC_RegularQueryContext.class,0);
		}
		public OC_QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Query(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Query(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Query(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_QueryContext oC_Query() throws RecognitionException {
		OC_QueryContext _localctx = new OC_QueryContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_oC_Query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(139);
			oC_RegularQuery();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RegularQueryContext extends ParserRuleContext {
		public OC_SingleQueryContext oC_SingleQuery() {
			return getRuleContext(OC_SingleQueryContext.class,0);
		}
		public OC_RegularQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RegularQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RegularQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RegularQuery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RegularQuery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RegularQueryContext oC_RegularQuery() throws RecognitionException {
		OC_RegularQueryContext _localctx = new OC_RegularQueryContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_oC_RegularQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(141);
			oC_SingleQuery();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_SingleQueryContext extends ParserRuleContext {
		public OC_SingleQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_SingleQuery; }
	 
		public OC_SingleQueryContext() { }
		public void copyFrom(OC_SingleQueryContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_SingleQuerySinglePartContext extends OC_SingleQueryContext {
		public OC_SinglePartQueryContext oC_SinglePartQuery() {
			return getRuleContext(OC_SinglePartQueryContext.class,0);
		}
		public OC_SingleQuerySinglePartContext(OC_SingleQueryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SingleQuerySinglePart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SingleQuerySinglePart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SingleQuerySinglePart(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_SingleQueryMultiPartContext extends OC_SingleQueryContext {
		public OC_MultiPartQueryContext oC_MultiPartQuery() {
			return getRuleContext(OC_MultiPartQueryContext.class,0);
		}
		public OC_SingleQueryMultiPartContext(OC_SingleQueryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SingleQueryMultiPart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SingleQueryMultiPart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SingleQueryMultiPart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_SingleQueryContext oC_SingleQuery() throws RecognitionException {
		OC_SingleQueryContext _localctx = new OC_SingleQueryContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_oC_SingleQuery);
		try {
			setState(145);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new OC_SingleQuerySinglePartContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(143);
				oC_SinglePartQuery();
				}
				break;
			case 2:
				_localctx = new OC_SingleQueryMultiPartContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(144);
				oC_MultiPartQuery();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_SinglePartQueryContext extends ParserRuleContext {
		public OC_SinglePartQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_SinglePartQuery; }
	 
		public OC_SinglePartQueryContext() { }
		public void copyFrom(OC_SinglePartQueryContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_SinglePartQueryNoUpdatingContext extends OC_SinglePartQueryContext {
		public OC_ReturnContext oC_Return() {
			return getRuleContext(OC_ReturnContext.class,0);
		}
		public List<OC_ReadingClauseContext> oC_ReadingClause() {
			return getRuleContexts(OC_ReadingClauseContext.class);
		}
		public OC_ReadingClauseContext oC_ReadingClause(int i) {
			return getRuleContext(OC_ReadingClauseContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_SinglePartQueryNoUpdatingContext(OC_SinglePartQueryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SinglePartQueryNoUpdating(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SinglePartQueryNoUpdating(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SinglePartQueryNoUpdating(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_SinglePartQueryUpdatingContext extends OC_SinglePartQueryContext {
		public List<OC_UpdatingClauseContext> oC_UpdatingClause() {
			return getRuleContexts(OC_UpdatingClauseContext.class);
		}
		public OC_UpdatingClauseContext oC_UpdatingClause(int i) {
			return getRuleContext(OC_UpdatingClauseContext.class,i);
		}
		public List<OC_ReadingClauseContext> oC_ReadingClause() {
			return getRuleContexts(OC_ReadingClauseContext.class);
		}
		public OC_ReadingClauseContext oC_ReadingClause(int i) {
			return getRuleContext(OC_ReadingClauseContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_SinglePartQueryUpdatingContext(OC_SinglePartQueryContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SinglePartQueryUpdating(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SinglePartQueryUpdating(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SinglePartQueryUpdating(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_SinglePartQueryContext oC_SinglePartQuery() throws RecognitionException {
		OC_SinglePartQueryContext _localctx = new OC_SinglePartQueryContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_oC_SinglePartQuery);
		int _la;
		try {
			int _alt;
			setState(176);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new OC_SinglePartQueryNoUpdatingContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(153);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==OPTIONAL || _la==MATCH) {
					{
					{
					setState(147);
					oC_ReadingClause();
					setState(149);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(148);
						match(SP);
						}
					}

					}
					}
					setState(155);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(156);
				oC_Return();
				}
				}
				break;
			case 2:
				_localctx = new OC_SinglePartQueryUpdatingContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==OPTIONAL || _la==MATCH) {
					{
					{
					setState(157);
					oC_ReadingClause();
					setState(159);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(158);
						match(SP);
						}
					}

					}
					}
					setState(165);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(166);
				oC_UpdatingClause();
				setState(173);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(168);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(167);
							match(SP);
							}
						}

						setState(170);
						oC_UpdatingClause();
						}
						} 
					}
					setState(175);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_MultiPartQueryContext extends ParserRuleContext {
		public OC_SinglePartQueryContext oC_SinglePartQuery() {
			return getRuleContext(OC_SinglePartQueryContext.class,0);
		}
		public List<OC_WithContext> oC_With() {
			return getRuleContexts(OC_WithContext.class);
		}
		public OC_WithContext oC_With(int i) {
			return getRuleContext(OC_WithContext.class,i);
		}
		public List<OC_ReadingClauseContext> oC_ReadingClause() {
			return getRuleContexts(OC_ReadingClauseContext.class);
		}
		public OC_ReadingClauseContext oC_ReadingClause(int i) {
			return getRuleContext(OC_ReadingClauseContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_MultiPartQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_MultiPartQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_MultiPartQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_MultiPartQuery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_MultiPartQuery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_MultiPartQueryContext oC_MultiPartQuery() throws RecognitionException {
		OC_MultiPartQueryContext _localctx = new OC_MultiPartQueryContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_oC_MultiPartQuery);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(191); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(184);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==OPTIONAL || _la==MATCH) {
						{
						{
						setState(178);
						oC_ReadingClause();
						setState(180);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(179);
							match(SP);
							}
						}

						}
						}
						setState(186);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(187);
					oC_With();
					setState(189);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(188);
						match(SP);
						}
					}

					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(193); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(195);
			oC_SinglePartQuery();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_UpdatingClauseContext extends ParserRuleContext {
		public OC_CreateContext oC_Create() {
			return getRuleContext(OC_CreateContext.class,0);
		}
		public OC_UpdatingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_UpdatingClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_UpdatingClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_UpdatingClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_UpdatingClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_UpdatingClauseContext oC_UpdatingClause() throws RecognitionException {
		OC_UpdatingClauseContext _localctx = new OC_UpdatingClauseContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_oC_UpdatingClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197);
			oC_Create();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ReadingClauseContext extends ParserRuleContext {
		public OC_MatchContext oC_Match() {
			return getRuleContext(OC_MatchContext.class,0);
		}
		public OC_ReadingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ReadingClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ReadingClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ReadingClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ReadingClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ReadingClauseContext oC_ReadingClause() throws RecognitionException {
		OC_ReadingClauseContext _localctx = new OC_ReadingClauseContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_oC_ReadingClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(199);
			oC_Match();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_MatchContext extends ParserRuleContext {
		public TerminalNode MATCH() { return getToken(CypherParser.MATCH, 0); }
		public OC_PatternContext oC_Pattern() {
			return getRuleContext(OC_PatternContext.class,0);
		}
		public TerminalNode OPTIONAL() { return getToken(CypherParser.OPTIONAL, 0); }
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_WhereContext oC_Where() {
			return getRuleContext(OC_WhereContext.class,0);
		}
		public OC_MatchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Match; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Match(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Match(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Match(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_MatchContext oC_Match() throws RecognitionException {
		OC_MatchContext _localctx = new OC_MatchContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_oC_Match);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(203);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPTIONAL) {
				{
				setState(201);
				match(OPTIONAL);
				setState(202);
				match(SP);
				}
			}

			setState(205);
			match(MATCH);
			setState(207);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(206);
				match(SP);
				}
			}

			setState(209);
			oC_Pattern();
			setState(214);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(211);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(210);
					match(SP);
					}
				}

				setState(213);
				oC_Where();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_CreateContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(CypherParser.CREATE, 0); }
		public OC_PatternContext oC_Pattern() {
			return getRuleContext(OC_PatternContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_CreateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Create; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Create(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Create(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Create(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_CreateContext oC_Create() throws RecognitionException {
		OC_CreateContext _localctx = new OC_CreateContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_oC_Create);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			match(CREATE);
			setState(218);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(217);
				match(SP);
				}
			}

			setState(220);
			oC_Pattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ReturnContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(CypherParser.RETURN, 0); }
		public OC_ProjectionBodyContext oC_ProjectionBody() {
			return getRuleContext(OC_ProjectionBodyContext.class,0);
		}
		public OC_ReturnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Return; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Return(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Return(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Return(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ReturnContext oC_Return() throws RecognitionException {
		OC_ReturnContext _localctx = new OC_ReturnContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_oC_Return);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(RETURN);
			setState(223);
			oC_ProjectionBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_WithContext extends ParserRuleContext {
		public TerminalNode WITH() { return getToken(CypherParser.WITH, 0); }
		public OC_ProjectionBodyContext oC_ProjectionBody() {
			return getRuleContext(OC_ProjectionBodyContext.class,0);
		}
		public OC_WhereContext oC_Where() {
			return getRuleContext(OC_WhereContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_WithContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_With; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_With(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_With(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_With(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_WithContext oC_With() throws RecognitionException {
		OC_WithContext _localctx = new OC_WithContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_oC_With);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(225);
			match(WITH);
			setState(226);
			oC_ProjectionBody();
			setState(231);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,22,_ctx) ) {
			case 1:
				{
				setState(228);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(227);
					match(SP);
					}
				}

				setState(230);
				oC_Where();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ProjectionBodyContext extends ParserRuleContext {
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_ProjectionItemsContext oC_ProjectionItems() {
			return getRuleContext(OC_ProjectionItemsContext.class,0);
		}
		public OC_OrderContext oC_Order() {
			return getRuleContext(OC_OrderContext.class,0);
		}
		public OC_LimitContext oC_Limit() {
			return getRuleContext(OC_LimitContext.class,0);
		}
		public OC_ProjectionBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ProjectionBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ProjectionBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ProjectionBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ProjectionBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ProjectionBodyContext oC_ProjectionBody() throws RecognitionException {
		OC_ProjectionBodyContext _localctx = new OC_ProjectionBodyContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_oC_ProjectionBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(233);
			match(SP);
			setState(234);
			oC_ProjectionItems();
			setState(237);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
			case 1:
				{
				setState(235);
				match(SP);
				setState(236);
				oC_Order();
				}
				break;
			}
			setState(241);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(239);
				match(SP);
				setState(240);
				oC_Limit();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ProjectionItemsContext extends ParserRuleContext {
		public OC_ProjectionItemsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ProjectionItems; }
	 
		public OC_ProjectionItemsContext() { }
		public void copyFrom(OC_ProjectionItemsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_ProjectionItemsStarContext extends OC_ProjectionItemsContext {
		public List<OC_ProjectionItemContext> oC_ProjectionItem() {
			return getRuleContexts(OC_ProjectionItemContext.class);
		}
		public OC_ProjectionItemContext oC_ProjectionItem(int i) {
			return getRuleContext(OC_ProjectionItemContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_ProjectionItemsStarContext(OC_ProjectionItemsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ProjectionItemsStar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ProjectionItemsStar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ProjectionItemsStar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_ProjectionItemsNoStarContext extends OC_ProjectionItemsContext {
		public List<OC_ProjectionItemContext> oC_ProjectionItem() {
			return getRuleContexts(OC_ProjectionItemContext.class);
		}
		public OC_ProjectionItemContext oC_ProjectionItem(int i) {
			return getRuleContext(OC_ProjectionItemContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_ProjectionItemsNoStarContext(OC_ProjectionItemsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ProjectionItemsNoStar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ProjectionItemsNoStar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ProjectionItemsNoStar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ProjectionItemsContext oC_ProjectionItems() throws RecognitionException {
		OC_ProjectionItemsContext _localctx = new OC_ProjectionItemsContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_oC_ProjectionItems);
		int _la;
		try {
			int _alt;
			setState(271);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				_localctx = new OC_ProjectionItemsStarContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(243);
				match(T__1);
				setState(254);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(245);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(244);
							match(SP);
							}
						}

						setState(247);
						match(T__2);
						setState(249);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(248);
							match(SP);
							}
						}

						setState(251);
						oC_ProjectionItem();
						}
						} 
					}
					setState(256);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
				}
				}
				}
				break;
			case T__4:
			case T__6:
			case T__16:
			case TRUE:
			case FALSE:
			case CASE:
			case StringLiteral:
			case HexInteger:
			case DecimalInteger:
			case OctalInteger:
			case HexLetter:
			case ExponentDecimalReal:
			case RegularDecimalReal:
			case FILTER:
			case EXTRACT:
			case UnescapedSymbolicName:
			case EscapedSymbolicName:
				_localctx = new OC_ProjectionItemsNoStarContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(257);
				oC_ProjectionItem();
				setState(268);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(259);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(258);
							match(SP);
							}
						}

						setState(261);
						match(T__2);
						setState(263);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(262);
							match(SP);
							}
						}

						setState(265);
						oC_ProjectionItem();
						}
						} 
					}
					setState(270);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				}
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ProjectionItemContext extends ParserRuleContext {
		public OC_ProjectionItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ProjectionItem; }
	 
		public OC_ProjectionItemContext() { }
		public void copyFrom(OC_ProjectionItemContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_ProjectionItemNoVariableContext extends OC_ProjectionItemContext {
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public OC_ProjectionItemNoVariableContext(OC_ProjectionItemContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ProjectionItemNoVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ProjectionItemNoVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ProjectionItemNoVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_ProjectionItemVariableContext extends OC_ProjectionItemContext {
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public TerminalNode AS() { return getToken(CypherParser.AS, 0); }
		public OC_VariableContext oC_Variable() {
			return getRuleContext(OC_VariableContext.class,0);
		}
		public OC_ProjectionItemVariableContext(OC_ProjectionItemContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ProjectionItemVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ProjectionItemVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ProjectionItemVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ProjectionItemContext oC_ProjectionItem() throws RecognitionException {
		OC_ProjectionItemContext _localctx = new OC_ProjectionItemContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_oC_ProjectionItem);
		try {
			setState(280);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				_localctx = new OC_ProjectionItemVariableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(273);
				oC_Expression();
				setState(274);
				match(SP);
				setState(275);
				match(AS);
				setState(276);
				match(SP);
				setState(277);
				oC_Variable();
				}
				}
				break;
			case 2:
				_localctx = new OC_ProjectionItemNoVariableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(279);
				oC_Expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_OrderContext extends ParserRuleContext {
		public TerminalNode ORDER() { return getToken(CypherParser.ORDER, 0); }
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public TerminalNode BY() { return getToken(CypherParser.BY, 0); }
		public List<OC_SortItemContext> oC_SortItem() {
			return getRuleContexts(OC_SortItemContext.class);
		}
		public OC_SortItemContext oC_SortItem(int i) {
			return getRuleContext(OC_SortItemContext.class,i);
		}
		public OC_OrderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Order; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Order(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Order(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Order(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_OrderContext oC_Order() throws RecognitionException {
		OC_OrderContext _localctx = new OC_OrderContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_oC_Order);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(282);
			match(ORDER);
			setState(283);
			match(SP);
			setState(284);
			match(BY);
			setState(285);
			match(SP);
			setState(286);
			oC_SortItem();
			setState(294);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(287);
				match(T__2);
				setState(289);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(288);
					match(SP);
					}
				}

				setState(291);
				oC_SortItem();
				}
				}
				setState(296);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_LimitContext extends ParserRuleContext {
		public TerminalNode LIMIT() { return getToken(CypherParser.LIMIT, 0); }
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public OC_LimitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Limit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Limit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Limit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Limit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_LimitContext oC_Limit() throws RecognitionException {
		OC_LimitContext _localctx = new OC_LimitContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_oC_Limit);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			match(LIMIT);
			setState(298);
			match(SP);
			setState(299);
			oC_Expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_SortItemContext extends ParserRuleContext {
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public TerminalNode ASCENDING() { return getToken(CypherParser.ASCENDING, 0); }
		public TerminalNode ASC() { return getToken(CypherParser.ASC, 0); }
		public TerminalNode DESCENDING() { return getToken(CypherParser.DESCENDING, 0); }
		public TerminalNode DESC() { return getToken(CypherParser.DESC, 0); }
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_SortItemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_SortItem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SortItem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SortItem(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SortItem(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_SortItemContext oC_SortItem() throws RecognitionException {
		OC_SortItemContext _localctx = new OC_SortItemContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_oC_SortItem);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(301);
			oC_Expression();
			setState(306);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				{
				setState(303);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(302);
					match(SP);
					}
				}

				setState(305);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASCENDING) | (1L << ASC) | (1L << DESCENDING) | (1L << DESC))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_WhereContext extends ParserRuleContext {
		public TerminalNode WHERE() { return getToken(CypherParser.WHERE, 0); }
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public OC_WhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Where; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Where(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Where(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Where(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_WhereContext oC_Where() throws RecognitionException {
		OC_WhereContext _localctx = new OC_WhereContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_oC_Where);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(308);
			match(WHERE);
			setState(309);
			match(SP);
			setState(310);
			oC_Expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PatternContext extends ParserRuleContext {
		public List<OC_PatternPartContext> oC_PatternPart() {
			return getRuleContexts(OC_PatternPartContext.class);
		}
		public OC_PatternPartContext oC_PatternPart(int i) {
			return getRuleContext(OC_PatternPartContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_PatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Pattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Pattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Pattern(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Pattern(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PatternContext oC_Pattern() throws RecognitionException {
		OC_PatternContext _localctx = new OC_PatternContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_oC_Pattern);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(312);
			oC_PatternPart();
			setState(323);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(314);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(313);
						match(SP);
						}
					}

					setState(316);
					match(T__2);
					setState(318);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(317);
						match(SP);
						}
					}

					setState(320);
					oC_PatternPart();
					}
					} 
				}
				setState(325);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,39,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PatternPartContext extends ParserRuleContext {
		public OC_PatternPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PatternPart; }
	 
		public OC_PatternPartContext() { }
		public void copyFrom(OC_PatternPartContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_PatternPartNoVariableContext extends OC_PatternPartContext {
		public OC_AnonymousPatternPartContext oC_AnonymousPatternPart() {
			return getRuleContext(OC_AnonymousPatternPartContext.class,0);
		}
		public OC_PatternPartNoVariableContext(OC_PatternPartContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PatternPartNoVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PatternPartNoVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PatternPartNoVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_PatternPartVariableContext extends OC_PatternPartContext {
		public OC_VariableContext oC_Variable() {
			return getRuleContext(OC_VariableContext.class,0);
		}
		public OC_AnonymousPatternPartContext oC_AnonymousPatternPart() {
			return getRuleContext(OC_AnonymousPatternPartContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_PatternPartVariableContext(OC_PatternPartContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PatternPartVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PatternPartVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PatternPartVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PatternPartContext oC_PatternPart() throws RecognitionException {
		OC_PatternPartContext _localctx = new OC_PatternPartContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_oC_PatternPart);
		int _la;
		try {
			setState(337);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HexLetter:
			case FILTER:
			case EXTRACT:
			case UnescapedSymbolicName:
			case EscapedSymbolicName:
				_localctx = new OC_PatternPartVariableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(326);
				oC_Variable();
				setState(328);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(327);
					match(SP);
					}
				}

				setState(330);
				match(T__3);
				setState(332);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(331);
					match(SP);
					}
				}

				setState(334);
				oC_AnonymousPatternPart();
				}
				}
				break;
			case T__4:
				_localctx = new OC_PatternPartNoVariableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(336);
				oC_AnonymousPatternPart();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_AnonymousPatternPartContext extends ParserRuleContext {
		public OC_PatternElementContext oC_PatternElement() {
			return getRuleContext(OC_PatternElementContext.class,0);
		}
		public OC_AnonymousPatternPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_AnonymousPatternPart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AnonymousPatternPart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AnonymousPatternPart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AnonymousPatternPart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_AnonymousPatternPartContext oC_AnonymousPatternPart() throws RecognitionException {
		OC_AnonymousPatternPartContext _localctx = new OC_AnonymousPatternPartContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_oC_AnonymousPatternPart);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(339);
			oC_PatternElement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PatternElementContext extends ParserRuleContext {
		public OC_PatternElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PatternElement; }
	 
		public OC_PatternElementContext() { }
		public void copyFrom(OC_PatternElementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_PatternElementParanthesisContext extends OC_PatternElementContext {
		public OC_PatternElementContext oC_PatternElement() {
			return getRuleContext(OC_PatternElementContext.class,0);
		}
		public OC_PatternElementParanthesisContext(OC_PatternElementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PatternElementParanthesis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PatternElementParanthesis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PatternElementParanthesis(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_PatternElementNoParanthesisContext extends OC_PatternElementContext {
		public OC_NodePatternContext oC_NodePattern() {
			return getRuleContext(OC_NodePatternContext.class,0);
		}
		public List<OC_PatternElementChainContext> oC_PatternElementChain() {
			return getRuleContexts(OC_PatternElementChainContext.class);
		}
		public OC_PatternElementChainContext oC_PatternElementChain(int i) {
			return getRuleContext(OC_PatternElementChainContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_PatternElementNoParanthesisContext(OC_PatternElementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PatternElementNoParanthesis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PatternElementNoParanthesis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PatternElementNoParanthesis(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PatternElementContext oC_PatternElement() throws RecognitionException {
		OC_PatternElementContext _localctx = new OC_PatternElementContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_oC_PatternElement);
		int _la;
		try {
			int _alt;
			setState(355);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				_localctx = new OC_PatternElementNoParanthesisContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(341);
				oC_NodePattern();
				setState(348);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(343);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(342);
							match(SP);
							}
						}

						setState(345);
						oC_PatternElementChain();
						}
						} 
					}
					setState(350);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,44,_ctx);
				}
				}
				}
				break;
			case 2:
				_localctx = new OC_PatternElementParanthesisContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(351);
				match(T__4);
				setState(352);
				oC_PatternElement();
				setState(353);
				match(T__5);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_NodePatternContext extends ParserRuleContext {
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_VariableContext oC_Variable() {
			return getRuleContext(OC_VariableContext.class,0);
		}
		public OC_NodeLabelsContext oC_NodeLabels() {
			return getRuleContext(OC_NodeLabelsContext.class,0);
		}
		public OC_PropertiesContext oC_Properties() {
			return getRuleContext(OC_PropertiesContext.class,0);
		}
		public OC_NodePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_NodePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_NodePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_NodePattern(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_NodePattern(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_NodePatternContext oC_NodePattern() throws RecognitionException {
		OC_NodePatternContext _localctx = new OC_NodePatternContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_oC_NodePattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			match(T__4);
			setState(359);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(358);
				match(SP);
				}
			}

			setState(365);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (HexLetter - 66)) | (1L << (FILTER - 66)) | (1L << (EXTRACT - 66)) | (1L << (UnescapedSymbolicName - 66)) | (1L << (EscapedSymbolicName - 66)))) != 0)) {
				{
				setState(361);
				oC_Variable();
				setState(363);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(362);
					match(SP);
					}
				}

				}
			}

			setState(371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(367);
				oC_NodeLabels();
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(368);
					match(SP);
					}
				}

				}
			}

			setState(377);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__16) {
				{
				setState(373);
				oC_Properties();
				setState(375);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(374);
					match(SP);
					}
				}

				}
			}

			setState(379);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PatternElementChainContext extends ParserRuleContext {
		public OC_RelationshipPatternContext oC_RelationshipPattern() {
			return getRuleContext(OC_RelationshipPatternContext.class,0);
		}
		public OC_NodePatternContext oC_NodePattern() {
			return getRuleContext(OC_NodePatternContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_PatternElementChainContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PatternElementChain; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PatternElementChain(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PatternElementChain(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PatternElementChain(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PatternElementChainContext oC_PatternElementChain() throws RecognitionException {
		OC_PatternElementChainContext _localctx = new OC_PatternElementChainContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_oC_PatternElementChain);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(381);
			oC_RelationshipPattern();
			setState(383);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(382);
				match(SP);
				}
			}

			setState(385);
			oC_NodePattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RelationshipPatternContext extends ParserRuleContext {
		public OC_RelationshipPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RelationshipPattern; }
	 
		public OC_RelationshipPatternContext() { }
		public void copyFrom(OC_RelationshipPatternContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_RelationshipPatternLeftDirContext extends OC_RelationshipPatternContext {
		public OC_LeftArrowHeadContext oC_LeftArrowHead() {
			return getRuleContext(OC_LeftArrowHeadContext.class,0);
		}
		public List<OC_DashContext> oC_Dash() {
			return getRuleContexts(OC_DashContext.class);
		}
		public OC_DashContext oC_Dash(int i) {
			return getRuleContext(OC_DashContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_RelationshipDetailContext oC_RelationshipDetail() {
			return getRuleContext(OC_RelationshipDetailContext.class,0);
		}
		public OC_RelationshipPatternLeftDirContext(OC_RelationshipPatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelationshipPatternLeftDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelationshipPatternLeftDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelationshipPatternLeftDir(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_RelationshipPatternBothDirContext extends OC_RelationshipPatternContext {
		public OC_LeftArrowHeadContext oC_LeftArrowHead() {
			return getRuleContext(OC_LeftArrowHeadContext.class,0);
		}
		public List<OC_DashContext> oC_Dash() {
			return getRuleContexts(OC_DashContext.class);
		}
		public OC_DashContext oC_Dash(int i) {
			return getRuleContext(OC_DashContext.class,i);
		}
		public OC_RightArrowHeadContext oC_RightArrowHead() {
			return getRuleContext(OC_RightArrowHeadContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_RelationshipDetailContext oC_RelationshipDetail() {
			return getRuleContext(OC_RelationshipDetailContext.class,0);
		}
		public OC_RelationshipPatternBothDirContext(OC_RelationshipPatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelationshipPatternBothDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelationshipPatternBothDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelationshipPatternBothDir(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_RelationshipPatternRightDirContext extends OC_RelationshipPatternContext {
		public List<OC_DashContext> oC_Dash() {
			return getRuleContexts(OC_DashContext.class);
		}
		public OC_DashContext oC_Dash(int i) {
			return getRuleContext(OC_DashContext.class,i);
		}
		public OC_RightArrowHeadContext oC_RightArrowHead() {
			return getRuleContext(OC_RightArrowHeadContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_RelationshipDetailContext oC_RelationshipDetail() {
			return getRuleContext(OC_RelationshipDetailContext.class,0);
		}
		public OC_RelationshipPatternRightDirContext(OC_RelationshipPatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelationshipPatternRightDir(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelationshipPatternRightDir(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelationshipPatternRightDir(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RelationshipPatternContext oC_RelationshipPattern() throws RecognitionException {
		OC_RelationshipPatternContext _localctx = new OC_RelationshipPatternContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_oC_RelationshipPattern);
		int _la;
		try {
			setState(439);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
			case 1:
				_localctx = new OC_RelationshipPatternBothDirContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(387);
				oC_LeftArrowHead();
				setState(389);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(388);
					match(SP);
					}
				}

				setState(391);
				oC_Dash();
				setState(393);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
				case 1:
					{
					setState(392);
					match(SP);
					}
					break;
				}
				setState(396);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(395);
					oC_RelationshipDetail();
					}
				}

				setState(399);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(398);
					match(SP);
					}
				}

				setState(401);
				oC_Dash();
				setState(403);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(402);
					match(SP);
					}
				}

				setState(405);
				oC_RightArrowHead();
				}
				}
				break;
			case 2:
				_localctx = new OC_RelationshipPatternLeftDirContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(407);
				oC_LeftArrowHead();
				setState(409);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(408);
					match(SP);
					}
				}

				setState(411);
				oC_Dash();
				setState(413);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
				case 1:
					{
					setState(412);
					match(SP);
					}
					break;
				}
				setState(416);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(415);
					oC_RelationshipDetail();
					}
				}

				setState(419);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(418);
					match(SP);
					}
				}

				setState(421);
				oC_Dash();
				}
				}
				break;
			case 3:
				_localctx = new OC_RelationshipPatternRightDirContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(423);
				oC_Dash();
				setState(425);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
				case 1:
					{
					setState(424);
					match(SP);
					}
					break;
				}
				setState(428);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(427);
					oC_RelationshipDetail();
					}
				}

				setState(431);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(430);
					match(SP);
					}
				}

				setState(433);
				oC_Dash();
				setState(435);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(434);
					match(SP);
					}
				}

				setState(437);
				oC_RightArrowHead();
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RelationshipDetailContext extends ParserRuleContext {
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_VariableContext oC_Variable() {
			return getRuleContext(OC_VariableContext.class,0);
		}
		public OC_RelationshipTypesContext oC_RelationshipTypes() {
			return getRuleContext(OC_RelationshipTypesContext.class,0);
		}
		public OC_PropertiesContext oC_Properties() {
			return getRuleContext(OC_PropertiesContext.class,0);
		}
		public OC_RelationshipDetailContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RelationshipDetail; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelationshipDetail(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelationshipDetail(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelationshipDetail(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RelationshipDetailContext oC_RelationshipDetail() throws RecognitionException {
		OC_RelationshipDetailContext _localctx = new OC_RelationshipDetailContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_oC_RelationshipDetail);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(441);
			match(T__6);
			setState(443);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(442);
				match(SP);
				}
			}

			setState(449);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (HexLetter - 66)) | (1L << (FILTER - 66)) | (1L << (EXTRACT - 66)) | (1L << (UnescapedSymbolicName - 66)) | (1L << (EscapedSymbolicName - 66)))) != 0)) {
				{
				setState(445);
				oC_Variable();
				setState(447);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(446);
					match(SP);
					}
				}

				}
			}

			setState(455);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(451);
				oC_RelationshipTypes();
				setState(453);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(452);
					match(SP);
					}
				}

				}
			}

			setState(461);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__16) {
				{
				setState(457);
				oC_Properties();
				setState(459);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(458);
					match(SP);
					}
				}

				}
			}

			setState(463);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PropertiesContext extends ParserRuleContext {
		public OC_PropertiesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Properties; }
	 
		public OC_PropertiesContext() { }
		public void copyFrom(OC_PropertiesContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_PropertiesMapLiteralContext extends OC_PropertiesContext {
		public OC_MapLiteralContext oC_MapLiteral() {
			return getRuleContext(OC_MapLiteralContext.class,0);
		}
		public OC_PropertiesMapLiteralContext(OC_PropertiesContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PropertiesMapLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PropertiesMapLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PropertiesMapLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PropertiesContext oC_Properties() throws RecognitionException {
		OC_PropertiesContext _localctx = new OC_PropertiesContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_oC_Properties);
		try {
			_localctx = new OC_PropertiesMapLiteralContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(465);
			oC_MapLiteral();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RelationshipTypesContext extends ParserRuleContext {
		public OC_RelTypeNameContext oC_RelTypeName() {
			return getRuleContext(OC_RelTypeNameContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_RelationshipTypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RelationshipTypes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelationshipTypes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelationshipTypes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelationshipTypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RelationshipTypesContext oC_RelationshipTypes() throws RecognitionException {
		OC_RelationshipTypesContext _localctx = new OC_RelationshipTypesContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_oC_RelationshipTypes);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(467);
			match(T__8);
			setState(469);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(468);
				match(SP);
				}
			}

			setState(471);
			oC_RelTypeName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_NodeLabelsContext extends ParserRuleContext {
		public List<OC_NodeLabelContext> oC_NodeLabel() {
			return getRuleContexts(OC_NodeLabelContext.class);
		}
		public OC_NodeLabelContext oC_NodeLabel(int i) {
			return getRuleContext(OC_NodeLabelContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_NodeLabelsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_NodeLabels; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_NodeLabels(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_NodeLabels(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_NodeLabels(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_NodeLabelsContext oC_NodeLabels() throws RecognitionException {
		OC_NodeLabelsContext _localctx = new OC_NodeLabelsContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_oC_NodeLabels);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(473);
			oC_NodeLabel();
			setState(480);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(475);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(474);
						match(SP);
						}
					}

					setState(477);
					oC_NodeLabel();
					}
					} 
				}
				setState(482);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_NodeLabelContext extends ParserRuleContext {
		public OC_LabelNameContext oC_LabelName() {
			return getRuleContext(OC_LabelNameContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_NodeLabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_NodeLabel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_NodeLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_NodeLabel(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_NodeLabel(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_NodeLabelContext oC_NodeLabel() throws RecognitionException {
		OC_NodeLabelContext _localctx = new OC_NodeLabelContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_oC_NodeLabel);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(483);
			match(T__8);
			setState(485);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(484);
				match(SP);
				}
			}

			setState(487);
			oC_LabelName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_LabelNameContext extends ParserRuleContext {
		public OC_SchemaNameContext oC_SchemaName() {
			return getRuleContext(OC_SchemaNameContext.class,0);
		}
		public OC_LabelNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_LabelName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LabelName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LabelName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LabelName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_LabelNameContext oC_LabelName() throws RecognitionException {
		OC_LabelNameContext _localctx = new OC_LabelNameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_oC_LabelName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(489);
			oC_SchemaName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RelTypeNameContext extends ParserRuleContext {
		public OC_SchemaNameContext oC_SchemaName() {
			return getRuleContext(OC_SchemaNameContext.class,0);
		}
		public OC_RelTypeNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RelTypeName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RelTypeName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RelTypeName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RelTypeName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RelTypeNameContext oC_RelTypeName() throws RecognitionException {
		OC_RelTypeNameContext _localctx = new OC_RelTypeNameContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_oC_RelTypeName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(491);
			oC_SchemaName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ExpressionContext extends ParserRuleContext {
		public OC_AndExpressionContext oC_AndExpression() {
			return getRuleContext(OC_AndExpressionContext.class,0);
		}
		public OC_ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ExpressionContext oC_Expression() throws RecognitionException {
		OC_ExpressionContext _localctx = new OC_ExpressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_oC_Expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(493);
			oC_AndExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_AndExpressionContext extends ParserRuleContext {
		public List<OC_ComparisonExpressionContext> oC_ComparisonExpression() {
			return getRuleContexts(OC_ComparisonExpressionContext.class);
		}
		public OC_ComparisonExpressionContext oC_ComparisonExpression(int i) {
			return getRuleContext(OC_ComparisonExpressionContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public List<TerminalNode> AND() { return getTokens(CypherParser.AND); }
		public TerminalNode AND(int i) {
			return getToken(CypherParser.AND, i);
		}
		public OC_AndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_AndExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AndExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AndExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_AndExpressionContext oC_AndExpression() throws RecognitionException {
		OC_AndExpressionContext _localctx = new OC_AndExpressionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_oC_AndExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(495);
			oC_ComparisonExpression();
			setState(502);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,79,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(496);
					match(SP);
					setState(497);
					match(AND);
					setState(498);
					match(SP);
					setState(499);
					oC_ComparisonExpression();
					}
					} 
				}
				setState(504);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,79,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ComparisonExpressionContext extends ParserRuleContext {
		public OC_ComparisonExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ComparisonExpression; }
	 
		public OC_ComparisonExpressionContext() { }
		public void copyFrom(OC_ComparisonExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_ComparisonSingleOrMultipleContext extends OC_ComparisonExpressionContext {
		public OC_StringListNullOperatorExpressionContext oC_StringListNullOperatorExpression() {
			return getRuleContext(OC_StringListNullOperatorExpressionContext.class,0);
		}
		public List<OC_PartialComparisonExpressionContext> oC_PartialComparisonExpression() {
			return getRuleContexts(OC_PartialComparisonExpressionContext.class);
		}
		public OC_PartialComparisonExpressionContext oC_PartialComparisonExpression(int i) {
			return getRuleContext(OC_PartialComparisonExpressionContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_ComparisonSingleOrMultipleContext(OC_ComparisonExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ComparisonSingleOrMultiple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ComparisonSingleOrMultiple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ComparisonSingleOrMultiple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_ComparisonSingleContext extends OC_ComparisonExpressionContext {
		public OC_StringListNullOperatorExpressionContext oC_StringListNullOperatorExpression() {
			return getRuleContext(OC_StringListNullOperatorExpressionContext.class,0);
		}
		public OC_ComparisonSingleContext(OC_ComparisonExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ComparisonSingle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ComparisonSingle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ComparisonSingle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ComparisonExpressionContext oC_ComparisonExpression() throws RecognitionException {
		OC_ComparisonExpressionContext _localctx = new OC_ComparisonExpressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_oC_ComparisonExpression);
		int _la;
		try {
			int _alt;
			setState(516);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				_localctx = new OC_ComparisonSingleContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(505);
				oC_StringListNullOperatorExpression();
				}
				break;
			case 2:
				_localctx = new OC_ComparisonSingleOrMultipleContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(506);
				oC_StringListNullOperatorExpression();
				setState(513);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,81,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(508);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(507);
							match(SP);
							}
						}

						setState(510);
						oC_PartialComparisonExpression();
						}
						} 
					}
					setState(515);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,81,_ctx);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_StringListNullOperatorExpressionContext extends ParserRuleContext {
		public OC_PropertyOrLabelsExpressionContext oC_PropertyOrLabelsExpression() {
			return getRuleContext(OC_PropertyOrLabelsExpressionContext.class,0);
		}
		public List<OC_ListOperatorExpressionContext> oC_ListOperatorExpression() {
			return getRuleContexts(OC_ListOperatorExpressionContext.class);
		}
		public OC_ListOperatorExpressionContext oC_ListOperatorExpression(int i) {
			return getRuleContext(OC_ListOperatorExpressionContext.class,i);
		}
		public OC_StringListNullOperatorExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_StringListNullOperatorExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_StringListNullOperatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_StringListNullOperatorExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_StringListNullOperatorExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_StringListNullOperatorExpressionContext oC_StringListNullOperatorExpression() throws RecognitionException {
		OC_StringListNullOperatorExpressionContext _localctx = new OC_StringListNullOperatorExpressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_oC_StringListNullOperatorExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(518);
			oC_PropertyOrLabelsExpression();
			setState(522);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,83,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(519);
					oC_ListOperatorExpression();
					}
					} 
				}
				setState(524);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,83,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ListOperatorExpressionContext extends ParserRuleContext {
		public List<OC_ExpressionContext> oC_Expression() {
			return getRuleContexts(OC_ExpressionContext.class);
		}
		public OC_ExpressionContext oC_Expression(int i) {
			return getRuleContext(OC_ExpressionContext.class,i);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_ListOperatorExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ListOperatorExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ListOperatorExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ListOperatorExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ListOperatorExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ListOperatorExpressionContext oC_ListOperatorExpression() throws RecognitionException {
		OC_ListOperatorExpressionContext _localctx = new OC_ListOperatorExpressionContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_oC_ListOperatorExpression);
		int _la;
		try {
			setState(544);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,88,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(526);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(525);
					match(SP);
					}
				}

				setState(528);
				match(T__6);
				setState(529);
				oC_Expression();
				setState(530);
				match(T__7);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(533);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(532);
					match(SP);
					}
				}

				setState(535);
				match(T__6);
				setState(537);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__6) | (1L << T__16) | (1L << TRUE) | (1L << FALSE) | (1L << CASE) | (1L << StringLiteral) | (1L << HexInteger))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DecimalInteger - 64)) | (1L << (OctalInteger - 64)) | (1L << (HexLetter - 64)) | (1L << (ExponentDecimalReal - 64)) | (1L << (RegularDecimalReal - 64)) | (1L << (FILTER - 64)) | (1L << (EXTRACT - 64)) | (1L << (UnescapedSymbolicName - 64)) | (1L << (EscapedSymbolicName - 64)))) != 0)) {
					{
					setState(536);
					oC_Expression();
					}
				}

				setState(539);
				match(T__9);
				setState(541);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__6) | (1L << T__16) | (1L << TRUE) | (1L << FALSE) | (1L << CASE) | (1L << StringLiteral) | (1L << HexInteger))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DecimalInteger - 64)) | (1L << (OctalInteger - 64)) | (1L << (HexLetter - 64)) | (1L << (ExponentDecimalReal - 64)) | (1L << (RegularDecimalReal - 64)) | (1L << (FILTER - 64)) | (1L << (EXTRACT - 64)) | (1L << (UnescapedSymbolicName - 64)) | (1L << (EscapedSymbolicName - 64)))) != 0)) {
					{
					setState(540);
					oC_Expression();
					}
				}

				setState(543);
				match(T__7);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PropertyOrLabelsExpressionContext extends ParserRuleContext {
		public OC_PropertyOrLabelsExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PropertyOrLabelsExpression; }
	 
		public OC_PropertyOrLabelsExpressionContext() { }
		public void copyFrom(OC_PropertyOrLabelsExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_PropertyOrLabelsExpressionLookupContext extends OC_PropertyOrLabelsExpressionContext {
		public OC_AtomContext oC_Atom() {
			return getRuleContext(OC_AtomContext.class,0);
		}
		public List<OC_PropertyLookupContext> oC_PropertyLookup() {
			return getRuleContexts(OC_PropertyLookupContext.class);
		}
		public OC_PropertyLookupContext oC_PropertyLookup(int i) {
			return getRuleContext(OC_PropertyLookupContext.class,i);
		}
		public OC_NodeLabelsContext oC_NodeLabels() {
			return getRuleContext(OC_NodeLabelsContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_PropertyOrLabelsExpressionLookupContext(OC_PropertyOrLabelsExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PropertyOrLabelsExpressionLookup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PropertyOrLabelsExpressionLookup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PropertyOrLabelsExpressionLookup(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_PropertyOrLabelsExpressionAtomContext extends OC_PropertyOrLabelsExpressionContext {
		public OC_AtomContext oC_Atom() {
			return getRuleContext(OC_AtomContext.class,0);
		}
		public OC_PropertyOrLabelsExpressionAtomContext(OC_PropertyOrLabelsExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PropertyOrLabelsExpressionAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PropertyOrLabelsExpressionAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PropertyOrLabelsExpressionAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PropertyOrLabelsExpressionContext oC_PropertyOrLabelsExpression() throws RecognitionException {
		OC_PropertyOrLabelsExpressionContext _localctx = new OC_PropertyOrLabelsExpressionContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_oC_PropertyOrLabelsExpression);
		int _la;
		try {
			int _alt;
			setState(563);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,93,_ctx) ) {
			case 1:
				_localctx = new OC_PropertyOrLabelsExpressionAtomContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(546);
				oC_Atom();
				}
				break;
			case 2:
				_localctx = new OC_PropertyOrLabelsExpressionLookupContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(547);
				oC_Atom();
				setState(554);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,90,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(549);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(548);
							match(SP);
							}
						}

						setState(551);
						oC_PropertyLookup();
						}
						} 
					}
					setState(556);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,90,_ctx);
				}
				setState(561);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,92,_ctx) ) {
				case 1:
					{
					setState(558);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(557);
						match(SP);
						}
					}

					setState(560);
					oC_NodeLabels();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_AtomContext extends ParserRuleContext {
		public OC_AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Atom; }
	 
		public OC_AtomContext() { }
		public void copyFrom(OC_AtomContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_AtomLiteralContext extends OC_AtomContext {
		public OC_LiteralContext oC_Literal() {
			return getRuleContext(OC_LiteralContext.class,0);
		}
		public OC_AtomLiteralContext(OC_AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AtomLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AtomLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AtomLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_AtomVariableContext extends OC_AtomContext {
		public OC_VariableContext oC_Variable() {
			return getRuleContext(OC_VariableContext.class,0);
		}
		public OC_AtomVariableContext(OC_AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AtomVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AtomVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AtomVariable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_AtomCaseContext extends OC_AtomContext {
		public OC_CaseExpressionContext oC_CaseExpression() {
			return getRuleContext(OC_CaseExpressionContext.class,0);
		}
		public OC_AtomCaseContext(OC_AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AtomCase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AtomCase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AtomCase(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_AtomParenthesizedExpressionContext extends OC_AtomContext {
		public OC_ParenthesizedExpressionContext oC_ParenthesizedExpression() {
			return getRuleContext(OC_ParenthesizedExpressionContext.class,0);
		}
		public OC_AtomParenthesizedExpressionContext(OC_AtomContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_AtomParenthesizedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_AtomParenthesizedExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_AtomParenthesizedExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_AtomContext oC_Atom() throws RecognitionException {
		OC_AtomContext _localctx = new OC_AtomContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_oC_Atom);
		try {
			setState(569);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
			case T__16:
			case TRUE:
			case FALSE:
			case StringLiteral:
			case HexInteger:
			case DecimalInteger:
			case OctalInteger:
			case ExponentDecimalReal:
			case RegularDecimalReal:
				_localctx = new OC_AtomLiteralContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(565);
				oC_Literal();
				}
				break;
			case HexLetter:
			case FILTER:
			case EXTRACT:
			case UnescapedSymbolicName:
			case EscapedSymbolicName:
				_localctx = new OC_AtomVariableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(566);
				oC_Variable();
				}
				break;
			case T__4:
				_localctx = new OC_AtomParenthesizedExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(567);
				oC_ParenthesizedExpression();
				}
				break;
			case CASE:
				_localctx = new OC_AtomCaseContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(568);
				oC_CaseExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_LiteralContext extends ParserRuleContext {
		public OC_LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Literal; }
	 
		public OC_LiteralContext() { }
		public void copyFrom(OC_LiteralContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OC_LiteralBooleanContext extends OC_LiteralContext {
		public OC_BooleanLiteralContext oC_BooleanLiteral() {
			return getRuleContext(OC_BooleanLiteralContext.class,0);
		}
		public OC_LiteralBooleanContext(OC_LiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LiteralBoolean(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LiteralBoolean(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LiteralBoolean(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_LiteralMapContext extends OC_LiteralContext {
		public OC_MapLiteralContext oC_MapLiteral() {
			return getRuleContext(OC_MapLiteralContext.class,0);
		}
		public OC_LiteralMapContext(OC_LiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LiteralMap(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LiteralMap(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LiteralMap(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_LiteralListContext extends OC_LiteralContext {
		public OC_ListLiteralContext oC_ListLiteral() {
			return getRuleContext(OC_ListLiteralContext.class,0);
		}
		public OC_LiteralListContext(OC_LiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LiteralList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LiteralList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LiteralList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_LiteralStringContext extends OC_LiteralContext {
		public TerminalNode StringLiteral() { return getToken(CypherParser.StringLiteral, 0); }
		public OC_LiteralStringContext(OC_LiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LiteralString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LiteralString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LiteralString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OC_LiteralNumberContext extends OC_LiteralContext {
		public OC_NumberLiteralContext oC_NumberLiteral() {
			return getRuleContext(OC_NumberLiteralContext.class,0);
		}
		public OC_LiteralNumberContext(OC_LiteralContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LiteralNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LiteralNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LiteralNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_LiteralContext oC_Literal() throws RecognitionException {
		OC_LiteralContext _localctx = new OC_LiteralContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_oC_Literal);
		try {
			setState(576);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HexInteger:
			case DecimalInteger:
			case OctalInteger:
			case ExponentDecimalReal:
			case RegularDecimalReal:
				_localctx = new OC_LiteralNumberContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(571);
				oC_NumberLiteral();
				}
				break;
			case StringLiteral:
				_localctx = new OC_LiteralStringContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(572);
				match(StringLiteral);
				}
				break;
			case TRUE:
			case FALSE:
				_localctx = new OC_LiteralBooleanContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(573);
				oC_BooleanLiteral();
				}
				break;
			case T__16:
				_localctx = new OC_LiteralMapContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(574);
				oC_MapLiteral();
				}
				break;
			case T__6:
				_localctx = new OC_LiteralListContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(575);
				oC_ListLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_BooleanLiteralContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(CypherParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(CypherParser.FALSE, 0); }
		public OC_BooleanLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_BooleanLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_BooleanLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_BooleanLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_BooleanLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_BooleanLiteralContext oC_BooleanLiteral() throws RecognitionException {
		OC_BooleanLiteralContext _localctx = new OC_BooleanLiteralContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_oC_BooleanLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(578);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ListLiteralContext extends ParserRuleContext {
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public List<OC_ExpressionContext> oC_Expression() {
			return getRuleContexts(OC_ExpressionContext.class);
		}
		public OC_ExpressionContext oC_Expression(int i) {
			return getRuleContext(OC_ExpressionContext.class,i);
		}
		public OC_ListLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ListLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ListLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ListLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ListLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ListLiteralContext oC_ListLiteral() throws RecognitionException {
		OC_ListLiteralContext _localctx = new OC_ListLiteralContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_oC_ListLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(580);
			match(T__6);
			setState(582);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(581);
				match(SP);
				}
			}

			setState(601);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__6) | (1L << T__16) | (1L << TRUE) | (1L << FALSE) | (1L << CASE) | (1L << StringLiteral) | (1L << HexInteger))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DecimalInteger - 64)) | (1L << (OctalInteger - 64)) | (1L << (HexLetter - 64)) | (1L << (ExponentDecimalReal - 64)) | (1L << (RegularDecimalReal - 64)) | (1L << (FILTER - 64)) | (1L << (EXTRACT - 64)) | (1L << (UnescapedSymbolicName - 64)) | (1L << (EscapedSymbolicName - 64)))) != 0)) {
				{
				setState(584);
				oC_Expression();
				setState(586);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(585);
					match(SP);
					}
				}

				setState(598);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(588);
					match(T__2);
					setState(590);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(589);
						match(SP);
						}
					}

					setState(592);
					oC_Expression();
					setState(594);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(593);
						match(SP);
						}
					}

					}
					}
					setState(600);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(603);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PartialComparisonExpressionContext extends ParserRuleContext {
		public OC_StringListNullOperatorExpressionContext oC_StringListNullOperatorExpression() {
			return getRuleContext(OC_StringListNullOperatorExpressionContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_PartialComparisonExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PartialComparisonExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PartialComparisonExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PartialComparisonExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PartialComparisonExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PartialComparisonExpressionContext oC_PartialComparisonExpression() throws RecognitionException {
		OC_PartialComparisonExpressionContext _localctx = new OC_PartialComparisonExpressionContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_oC_PartialComparisonExpression);
		int _la;
		try {
			setState(635);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__3:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(605);
				match(T__3);
				setState(607);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(606);
					match(SP);
					}
				}

				setState(609);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(610);
				match(T__10);
				setState(612);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(611);
					match(SP);
					}
				}

				setState(614);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(615);
				match(T__11);
				setState(617);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(616);
					match(SP);
					}
				}

				setState(619);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(620);
				match(T__12);
				setState(622);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(621);
					match(SP);
					}
				}

				setState(624);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(625);
				match(T__13);
				setState(627);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(626);
					match(SP);
					}
				}

				setState(629);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 6);
				{
				{
				setState(630);
				match(T__14);
				setState(632);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(631);
					match(SP);
					}
				}

				setState(634);
				oC_StringListNullOperatorExpression();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ParenthesizedExpressionContext extends ParserRuleContext {
		public OC_ExpressionContext oC_Expression() {
			return getRuleContext(OC_ExpressionContext.class,0);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_ParenthesizedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ParenthesizedExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ParenthesizedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ParenthesizedExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ParenthesizedExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ParenthesizedExpressionContext oC_ParenthesizedExpression() throws RecognitionException {
		OC_ParenthesizedExpressionContext _localctx = new OC_ParenthesizedExpressionContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_oC_ParenthesizedExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(637);
			match(T__4);
			setState(639);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(638);
				match(SP);
				}
			}

			setState(641);
			oC_Expression();
			setState(643);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(642);
				match(SP);
				}
			}

			setState(645);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PropertyLookupContext extends ParserRuleContext {
		public OC_PropertyKeyNameContext oC_PropertyKeyName() {
			return getRuleContext(OC_PropertyKeyNameContext.class,0);
		}
		public TerminalNode SP() { return getToken(CypherParser.SP, 0); }
		public OC_PropertyLookupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PropertyLookup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PropertyLookup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PropertyLookup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PropertyLookup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PropertyLookupContext oC_PropertyLookup() throws RecognitionException {
		OC_PropertyLookupContext _localctx = new OC_PropertyLookupContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_oC_PropertyLookup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(647);
			match(T__15);
			setState(649);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(648);
				match(SP);
				}
			}

			{
			setState(651);
			oC_PropertyKeyName();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_CaseExpressionContext extends ParserRuleContext {
		public TerminalNode END() { return getToken(CypherParser.END, 0); }
		public TerminalNode ELSE() { return getToken(CypherParser.ELSE, 0); }
		public List<OC_ExpressionContext> oC_Expression() {
			return getRuleContexts(OC_ExpressionContext.class);
		}
		public OC_ExpressionContext oC_Expression(int i) {
			return getRuleContext(OC_ExpressionContext.class,i);
		}
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public TerminalNode CASE() { return getToken(CypherParser.CASE, 0); }
		public List<OC_CaseAlternativesContext> oC_CaseAlternatives() {
			return getRuleContexts(OC_CaseAlternativesContext.class);
		}
		public OC_CaseAlternativesContext oC_CaseAlternatives(int i) {
			return getRuleContext(OC_CaseAlternativesContext.class,i);
		}
		public OC_CaseExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_CaseExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_CaseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_CaseExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_CaseExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_CaseExpressionContext oC_CaseExpression() throws RecognitionException {
		OC_CaseExpressionContext _localctx = new OC_CaseExpressionContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_oC_CaseExpression);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(675);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,117,_ctx) ) {
			case 1:
				{
				{
				setState(653);
				match(CASE);
				setState(658); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(655);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(654);
							match(SP);
							}
						}

						setState(657);
						oC_CaseAlternatives();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(660); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,113,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			case 2:
				{
				{
				setState(662);
				match(CASE);
				setState(664);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(663);
					match(SP);
					}
				}

				setState(666);
				oC_Expression();
				setState(671); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(668);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==SP) {
							{
							setState(667);
							match(SP);
							}
						}

						setState(670);
						oC_CaseAlternatives();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(673); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,116,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				}
				break;
			}
			setState(685);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,120,_ctx) ) {
			case 1:
				{
				setState(678);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(677);
					match(SP);
					}
				}

				setState(680);
				match(ELSE);
				setState(682);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(681);
					match(SP);
					}
				}

				setState(684);
				oC_Expression();
				}
				break;
			}
			setState(688);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(687);
				match(SP);
				}
			}

			setState(690);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_CaseAlternativesContext extends ParserRuleContext {
		public TerminalNode WHEN() { return getToken(CypherParser.WHEN, 0); }
		public List<OC_ExpressionContext> oC_Expression() {
			return getRuleContexts(OC_ExpressionContext.class);
		}
		public OC_ExpressionContext oC_Expression(int i) {
			return getRuleContext(OC_ExpressionContext.class,i);
		}
		public TerminalNode THEN() { return getToken(CypherParser.THEN, 0); }
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public OC_CaseAlternativesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_CaseAlternatives; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_CaseAlternatives(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_CaseAlternatives(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_CaseAlternatives(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_CaseAlternativesContext oC_CaseAlternatives() throws RecognitionException {
		OC_CaseAlternativesContext _localctx = new OC_CaseAlternativesContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_oC_CaseAlternatives);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(692);
			match(WHEN);
			setState(694);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(693);
				match(SP);
				}
			}

			setState(696);
			oC_Expression();
			setState(698);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(697);
				match(SP);
				}
			}

			setState(700);
			match(THEN);
			setState(702);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(701);
				match(SP);
				}
			}

			setState(704);
			oC_Expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_VariableContext extends ParserRuleContext {
		public OC_SymbolicNameContext oC_SymbolicName() {
			return getRuleContext(OC_SymbolicNameContext.class,0);
		}
		public OC_VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_VariableContext oC_Variable() throws RecognitionException {
		OC_VariableContext _localctx = new OC_VariableContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_oC_Variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(706);
			oC_SymbolicName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_NumberLiteralContext extends ParserRuleContext {
		public OC_DoubleLiteralContext oC_DoubleLiteral() {
			return getRuleContext(OC_DoubleLiteralContext.class,0);
		}
		public OC_IntegerLiteralContext oC_IntegerLiteral() {
			return getRuleContext(OC_IntegerLiteralContext.class,0);
		}
		public OC_NumberLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_NumberLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_NumberLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_NumberLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_NumberLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_NumberLiteralContext oC_NumberLiteral() throws RecognitionException {
		OC_NumberLiteralContext _localctx = new OC_NumberLiteralContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_oC_NumberLiteral);
		try {
			setState(710);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ExponentDecimalReal:
			case RegularDecimalReal:
				enterOuterAlt(_localctx, 1);
				{
				setState(708);
				oC_DoubleLiteral();
				}
				break;
			case HexInteger:
			case DecimalInteger:
			case OctalInteger:
				enterOuterAlt(_localctx, 2);
				{
				setState(709);
				oC_IntegerLiteral();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_MapLiteralContext extends ParserRuleContext {
		public List<TerminalNode> SP() { return getTokens(CypherParser.SP); }
		public TerminalNode SP(int i) {
			return getToken(CypherParser.SP, i);
		}
		public List<OC_PropertyKeyNameContext> oC_PropertyKeyName() {
			return getRuleContexts(OC_PropertyKeyNameContext.class);
		}
		public OC_PropertyKeyNameContext oC_PropertyKeyName(int i) {
			return getRuleContext(OC_PropertyKeyNameContext.class,i);
		}
		public List<OC_ExpressionContext> oC_Expression() {
			return getRuleContexts(OC_ExpressionContext.class);
		}
		public OC_ExpressionContext oC_Expression(int i) {
			return getRuleContext(OC_ExpressionContext.class,i);
		}
		public OC_MapLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_MapLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_MapLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_MapLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_MapLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_MapLiteralContext oC_MapLiteral() throws RecognitionException {
		OC_MapLiteralContext _localctx = new OC_MapLiteralContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_oC_MapLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(712);
			match(T__16);
			setState(714);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SP) {
				{
				setState(713);
				match(SP);
				}
			}

			setState(749);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 40)) & ~0x3f) == 0 && ((1L << (_la - 40)) & ((1L << (MATCH - 40)) | (1L << (CREATE - 40)) | (1L << (RETURN - 40)) | (1L << (AS - 40)) | (1L << (WHERE - 40)) | (1L << (AND - 40)) | (1L << (TRUE - 40)) | (1L << (FALSE - 40)) | (1L << (HexLetter - 40)) | (1L << (FILTER - 40)) | (1L << (EXTRACT - 40)) | (1L << (UnescapedSymbolicName - 40)) | (1L << (EscapedSymbolicName - 40)))) != 0)) {
				{
				setState(716);
				oC_PropertyKeyName();
				setState(718);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(717);
					match(SP);
					}
				}

				setState(720);
				match(T__8);
				setState(722);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(721);
					match(SP);
					}
				}

				setState(724);
				oC_Expression();
				setState(726);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==SP) {
					{
					setState(725);
					match(SP);
					}
				}

				setState(746);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(728);
					match(T__2);
					setState(730);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(729);
						match(SP);
						}
					}

					setState(732);
					oC_PropertyKeyName();
					setState(734);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(733);
						match(SP);
						}
					}

					setState(736);
					match(T__8);
					setState(738);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(737);
						match(SP);
						}
					}

					setState(740);
					oC_Expression();
					setState(742);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==SP) {
						{
						setState(741);
						match(SP);
						}
					}

					}
					}
					setState(748);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(751);
			match(T__17);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_PropertyKeyNameContext extends ParserRuleContext {
		public OC_SchemaNameContext oC_SchemaName() {
			return getRuleContext(OC_SchemaNameContext.class,0);
		}
		public OC_PropertyKeyNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_PropertyKeyName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_PropertyKeyName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_PropertyKeyName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_PropertyKeyName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_PropertyKeyNameContext oC_PropertyKeyName() throws RecognitionException {
		OC_PropertyKeyNameContext _localctx = new OC_PropertyKeyNameContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_oC_PropertyKeyName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(753);
			oC_SchemaName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_IntegerLiteralContext extends ParserRuleContext {
		public TerminalNode HexInteger() { return getToken(CypherParser.HexInteger, 0); }
		public TerminalNode OctalInteger() { return getToken(CypherParser.OctalInteger, 0); }
		public TerminalNode DecimalInteger() { return getToken(CypherParser.DecimalInteger, 0); }
		public OC_IntegerLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_IntegerLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_IntegerLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_IntegerLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_IntegerLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_IntegerLiteralContext oC_IntegerLiteral() throws RecognitionException {
		OC_IntegerLiteralContext _localctx = new OC_IntegerLiteralContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_oC_IntegerLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(755);
			_la = _input.LA(1);
			if ( !(((((_la - 63)) & ~0x3f) == 0 && ((1L << (_la - 63)) & ((1L << (HexInteger - 63)) | (1L << (DecimalInteger - 63)) | (1L << (OctalInteger - 63)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_DoubleLiteralContext extends ParserRuleContext {
		public TerminalNode ExponentDecimalReal() { return getToken(CypherParser.ExponentDecimalReal, 0); }
		public TerminalNode RegularDecimalReal() { return getToken(CypherParser.RegularDecimalReal, 0); }
		public OC_DoubleLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_DoubleLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_DoubleLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_DoubleLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_DoubleLiteral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_DoubleLiteralContext oC_DoubleLiteral() throws RecognitionException {
		OC_DoubleLiteralContext _localctx = new OC_DoubleLiteralContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_oC_DoubleLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(757);
			_la = _input.LA(1);
			if ( !(_la==ExponentDecimalReal || _la==RegularDecimalReal) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_SchemaNameContext extends ParserRuleContext {
		public OC_SymbolicNameContext oC_SymbolicName() {
			return getRuleContext(OC_SymbolicNameContext.class,0);
		}
		public OC_ReservedWordContext oC_ReservedWord() {
			return getRuleContext(OC_ReservedWordContext.class,0);
		}
		public OC_SchemaNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_SchemaName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SchemaName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SchemaName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SchemaName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_SchemaNameContext oC_SchemaName() throws RecognitionException {
		OC_SchemaNameContext _localctx = new OC_SchemaNameContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_oC_SchemaName);
		try {
			setState(761);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HexLetter:
			case FILTER:
			case EXTRACT:
			case UnescapedSymbolicName:
			case EscapedSymbolicName:
				enterOuterAlt(_localctx, 1);
				{
				setState(759);
				oC_SymbolicName();
				}
				break;
			case MATCH:
			case CREATE:
			case RETURN:
			case AS:
			case WHERE:
			case AND:
			case TRUE:
			case FALSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(760);
				oC_ReservedWord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_ReservedWordContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(CypherParser.CREATE, 0); }
		public TerminalNode MATCH() { return getToken(CypherParser.MATCH, 0); }
		public TerminalNode RETURN() { return getToken(CypherParser.RETURN, 0); }
		public TerminalNode WHERE() { return getToken(CypherParser.WHERE, 0); }
		public TerminalNode AND() { return getToken(CypherParser.AND, 0); }
		public TerminalNode AS() { return getToken(CypherParser.AS, 0); }
		public TerminalNode FALSE() { return getToken(CypherParser.FALSE, 0); }
		public TerminalNode TRUE() { return getToken(CypherParser.TRUE, 0); }
		public OC_ReservedWordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_ReservedWord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_ReservedWord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_ReservedWord(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_ReservedWord(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_ReservedWordContext oC_ReservedWord() throws RecognitionException {
		OC_ReservedWordContext _localctx = new OC_ReservedWordContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_oC_ReservedWord);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(763);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MATCH) | (1L << CREATE) | (1L << RETURN) | (1L << AS) | (1L << WHERE) | (1L << AND) | (1L << TRUE) | (1L << FALSE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_SymbolicNameContext extends ParserRuleContext {
		public TerminalNode UnescapedSymbolicName() { return getToken(CypherParser.UnescapedSymbolicName, 0); }
		public TerminalNode EscapedSymbolicName() { return getToken(CypherParser.EscapedSymbolicName, 0); }
		public TerminalNode HexLetter() { return getToken(CypherParser.HexLetter, 0); }
		public TerminalNode FILTER() { return getToken(CypherParser.FILTER, 0); }
		public TerminalNode EXTRACT() { return getToken(CypherParser.EXTRACT, 0); }
		public OC_SymbolicNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_SymbolicName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_SymbolicName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_SymbolicName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_SymbolicName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_SymbolicNameContext oC_SymbolicName() throws RecognitionException {
		OC_SymbolicNameContext _localctx = new OC_SymbolicNameContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_oC_SymbolicName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(765);
			_la = _input.LA(1);
			if ( !(((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (HexLetter - 66)) | (1L << (FILTER - 66)) | (1L << (EXTRACT - 66)) | (1L << (UnescapedSymbolicName - 66)) | (1L << (EscapedSymbolicName - 66)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_LeftArrowHeadContext extends ParserRuleContext {
		public OC_LeftArrowHeadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_LeftArrowHead; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_LeftArrowHead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_LeftArrowHead(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_LeftArrowHead(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_LeftArrowHeadContext oC_LeftArrowHead() throws RecognitionException {
		OC_LeftArrowHeadContext _localctx = new OC_LeftArrowHeadContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_oC_LeftArrowHead);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(767);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_RightArrowHeadContext extends ParserRuleContext {
		public OC_RightArrowHeadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_RightArrowHead; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_RightArrowHead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_RightArrowHead(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_RightArrowHead(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_RightArrowHeadContext oC_RightArrowHead() throws RecognitionException {
		OC_RightArrowHeadContext _localctx = new OC_RightArrowHeadContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_oC_RightArrowHead);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(769);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OC_DashContext extends ParserRuleContext {
		public OC_DashContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oC_Dash; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).enterOC_Dash(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof CypherListener ) ((CypherListener)listener).exitOC_Dash(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CypherVisitor ) return ((CypherVisitor<? extends T>)visitor).visitOC_Dash(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OC_DashContext oC_Dash() throws RecognitionException {
		OC_DashContext _localctx = new OC_DashContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_oC_Dash);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(771);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3U\u0308\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\3\2\5\2~\n\2\3\2\3\2\5\2\u0082\n\2\3\2\5\2\u0085\n\2\3\2\5\2\u0088"+
		"\n\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\5\6\u0094\n\6\3\7\3\7\5\7"+
		"\u0098\n\7\7\7\u009a\n\7\f\7\16\7\u009d\13\7\3\7\3\7\3\7\5\7\u00a2\n\7"+
		"\7\7\u00a4\n\7\f\7\16\7\u00a7\13\7\3\7\3\7\5\7\u00ab\n\7\3\7\7\7\u00ae"+
		"\n\7\f\7\16\7\u00b1\13\7\5\7\u00b3\n\7\3\b\3\b\5\b\u00b7\n\b\7\b\u00b9"+
		"\n\b\f\b\16\b\u00bc\13\b\3\b\3\b\5\b\u00c0\n\b\6\b\u00c2\n\b\r\b\16\b"+
		"\u00c3\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\5\13\u00ce\n\13\3\13\3\13\5\13"+
		"\u00d2\n\13\3\13\3\13\5\13\u00d6\n\13\3\13\5\13\u00d9\n\13\3\f\3\f\5\f"+
		"\u00dd\n\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\5\16\u00e7\n\16\3\16\5\16"+
		"\u00ea\n\16\3\17\3\17\3\17\3\17\5\17\u00f0\n\17\3\17\3\17\5\17\u00f4\n"+
		"\17\3\20\3\20\5\20\u00f8\n\20\3\20\3\20\5\20\u00fc\n\20\3\20\7\20\u00ff"+
		"\n\20\f\20\16\20\u0102\13\20\3\20\3\20\5\20\u0106\n\20\3\20\3\20\5\20"+
		"\u010a\n\20\3\20\7\20\u010d\n\20\f\20\16\20\u0110\13\20\5\20\u0112\n\20"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u011b\n\21\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\5\22\u0124\n\22\3\22\7\22\u0127\n\22\f\22\16\22\u012a"+
		"\13\22\3\23\3\23\3\23\3\23\3\24\3\24\5\24\u0132\n\24\3\24\5\24\u0135\n"+
		"\24\3\25\3\25\3\25\3\25\3\26\3\26\5\26\u013d\n\26\3\26\3\26\5\26\u0141"+
		"\n\26\3\26\7\26\u0144\n\26\f\26\16\26\u0147\13\26\3\27\3\27\5\27\u014b"+
		"\n\27\3\27\3\27\5\27\u014f\n\27\3\27\3\27\3\27\5\27\u0154\n\27\3\30\3"+
		"\30\3\31\3\31\5\31\u015a\n\31\3\31\7\31\u015d\n\31\f\31\16\31\u0160\13"+
		"\31\3\31\3\31\3\31\3\31\5\31\u0166\n\31\3\32\3\32\5\32\u016a\n\32\3\32"+
		"\3\32\5\32\u016e\n\32\5\32\u0170\n\32\3\32\3\32\5\32\u0174\n\32\5\32\u0176"+
		"\n\32\3\32\3\32\5\32\u017a\n\32\5\32\u017c\n\32\3\32\3\32\3\33\3\33\5"+
		"\33\u0182\n\33\3\33\3\33\3\34\3\34\5\34\u0188\n\34\3\34\3\34\5\34\u018c"+
		"\n\34\3\34\5\34\u018f\n\34\3\34\5\34\u0192\n\34\3\34\3\34\5\34\u0196\n"+
		"\34\3\34\3\34\3\34\3\34\5\34\u019c\n\34\3\34\3\34\5\34\u01a0\n\34\3\34"+
		"\5\34\u01a3\n\34\3\34\5\34\u01a6\n\34\3\34\3\34\3\34\3\34\5\34\u01ac\n"+
		"\34\3\34\5\34\u01af\n\34\3\34\5\34\u01b2\n\34\3\34\3\34\5\34\u01b6\n\34"+
		"\3\34\3\34\5\34\u01ba\n\34\3\35\3\35\5\35\u01be\n\35\3\35\3\35\5\35\u01c2"+
		"\n\35\5\35\u01c4\n\35\3\35\3\35\5\35\u01c8\n\35\5\35\u01ca\n\35\3\35\3"+
		"\35\5\35\u01ce\n\35\5\35\u01d0\n\35\3\35\3\35\3\36\3\36\3\37\3\37\5\37"+
		"\u01d8\n\37\3\37\3\37\3 \3 \5 \u01de\n \3 \7 \u01e1\n \f \16 \u01e4\13"+
		" \3!\3!\5!\u01e8\n!\3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3%\3%\3%\7%\u01f7"+
		"\n%\f%\16%\u01fa\13%\3&\3&\3&\5&\u01ff\n&\3&\7&\u0202\n&\f&\16&\u0205"+
		"\13&\5&\u0207\n&\3\'\3\'\7\'\u020b\n\'\f\'\16\'\u020e\13\'\3(\5(\u0211"+
		"\n(\3(\3(\3(\3(\3(\5(\u0218\n(\3(\3(\5(\u021c\n(\3(\3(\5(\u0220\n(\3("+
		"\5(\u0223\n(\3)\3)\3)\5)\u0228\n)\3)\7)\u022b\n)\f)\16)\u022e\13)\3)\5"+
		")\u0231\n)\3)\5)\u0234\n)\5)\u0236\n)\3*\3*\3*\3*\5*\u023c\n*\3+\3+\3"+
		"+\3+\3+\5+\u0243\n+\3,\3,\3-\3-\5-\u0249\n-\3-\3-\5-\u024d\n-\3-\3-\5"+
		"-\u0251\n-\3-\3-\5-\u0255\n-\7-\u0257\n-\f-\16-\u025a\13-\5-\u025c\n-"+
		"\3-\3-\3.\3.\5.\u0262\n.\3.\3.\3.\5.\u0267\n.\3.\3.\3.\5.\u026c\n.\3."+
		"\3.\3.\5.\u0271\n.\3.\3.\3.\5.\u0276\n.\3.\3.\3.\5.\u027b\n.\3.\5.\u027e"+
		"\n.\3/\3/\5/\u0282\n/\3/\3/\5/\u0286\n/\3/\3/\3\60\3\60\5\60\u028c\n\60"+
		"\3\60\3\60\3\61\3\61\5\61\u0292\n\61\3\61\6\61\u0295\n\61\r\61\16\61\u0296"+
		"\3\61\3\61\5\61\u029b\n\61\3\61\3\61\5\61\u029f\n\61\3\61\6\61\u02a2\n"+
		"\61\r\61\16\61\u02a3\5\61\u02a6\n\61\3\61\5\61\u02a9\n\61\3\61\3\61\5"+
		"\61\u02ad\n\61\3\61\5\61\u02b0\n\61\3\61\5\61\u02b3\n\61\3\61\3\61\3\62"+
		"\3\62\5\62\u02b9\n\62\3\62\3\62\5\62\u02bd\n\62\3\62\3\62\5\62\u02c1\n"+
		"\62\3\62\3\62\3\63\3\63\3\64\3\64\5\64\u02c9\n\64\3\65\3\65\5\65\u02cd"+
		"\n\65\3\65\3\65\5\65\u02d1\n\65\3\65\3\65\5\65\u02d5\n\65\3\65\3\65\5"+
		"\65\u02d9\n\65\3\65\3\65\5\65\u02dd\n\65\3\65\3\65\5\65\u02e1\n\65\3\65"+
		"\3\65\5\65\u02e5\n\65\3\65\3\65\5\65\u02e9\n\65\7\65\u02eb\n\65\f\65\16"+
		"\65\u02ee\13\65\5\65\u02f0\n\65\3\65\3\65\3\66\3\66\3\67\3\67\38\38\3"+
		"9\39\59\u02fc\n9\3:\3:\3;\3;\3<\3<\3=\3=\3>\3>\3>\2\2?\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bd"+
		"fhjlnprtvxz\2\13\3\2\62\65\3\289\3\2AC\3\2KL\5\2*,..\669\5\2DDMORR\4\2"+
		"\16\16\25\30\4\2\17\17\31\34\3\2\35(\2\u035d\2}\3\2\2\2\4\u008b\3\2\2"+
		"\2\6\u008d\3\2\2\2\b\u008f\3\2\2\2\n\u0093\3\2\2\2\f\u00b2\3\2\2\2\16"+
		"\u00c1\3\2\2\2\20\u00c7\3\2\2\2\22\u00c9\3\2\2\2\24\u00cd\3\2\2\2\26\u00da"+
		"\3\2\2\2\30\u00e0\3\2\2\2\32\u00e3\3\2\2\2\34\u00eb\3\2\2\2\36\u0111\3"+
		"\2\2\2 \u011a\3\2\2\2\"\u011c\3\2\2\2$\u012b\3\2\2\2&\u012f\3\2\2\2(\u0136"+
		"\3\2\2\2*\u013a\3\2\2\2,\u0153\3\2\2\2.\u0155\3\2\2\2\60\u0165\3\2\2\2"+
		"\62\u0167\3\2\2\2\64\u017f\3\2\2\2\66\u01b9\3\2\2\28\u01bb\3\2\2\2:\u01d3"+
		"\3\2\2\2<\u01d5\3\2\2\2>\u01db\3\2\2\2@\u01e5\3\2\2\2B\u01eb\3\2\2\2D"+
		"\u01ed\3\2\2\2F\u01ef\3\2\2\2H\u01f1\3\2\2\2J\u0206\3\2\2\2L\u0208\3\2"+
		"\2\2N\u0222\3\2\2\2P\u0235\3\2\2\2R\u023b\3\2\2\2T\u0242\3\2\2\2V\u0244"+
		"\3\2\2\2X\u0246\3\2\2\2Z\u027d\3\2\2\2\\\u027f\3\2\2\2^\u0289\3\2\2\2"+
		"`\u02a5\3\2\2\2b\u02b6\3\2\2\2d\u02c4\3\2\2\2f\u02c8\3\2\2\2h\u02ca\3"+
		"\2\2\2j\u02f3\3\2\2\2l\u02f5\3\2\2\2n\u02f7\3\2\2\2p\u02fb\3\2\2\2r\u02fd"+
		"\3\2\2\2t\u02ff\3\2\2\2v\u0301\3\2\2\2x\u0303\3\2\2\2z\u0305\3\2\2\2|"+
		"~\7S\2\2}|\3\2\2\2}~\3\2\2\2~\177\3\2\2\2\177\u0084\5\4\3\2\u0080\u0082"+
		"\7S\2\2\u0081\u0080\3\2\2\2\u0081\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083"+
		"\u0085\7\3\2\2\u0084\u0081\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u0087\3\2"+
		"\2\2\u0086\u0088\7S\2\2\u0087\u0086\3\2\2\2\u0087\u0088\3\2\2\2\u0088"+
		"\u0089\3\2\2\2\u0089\u008a\7\2\2\3\u008a\3\3\2\2\2\u008b\u008c\5\6\4\2"+
		"\u008c\5\3\2\2\2\u008d\u008e\5\b\5\2\u008e\7\3\2\2\2\u008f\u0090\5\n\6"+
		"\2\u0090\t\3\2\2\2\u0091\u0094\5\f\7\2\u0092\u0094\5\16\b\2\u0093\u0091"+
		"\3\2\2\2\u0093\u0092\3\2\2\2\u0094\13\3\2\2\2\u0095\u0097\5\22\n\2\u0096"+
		"\u0098\7S\2\2\u0097\u0096\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009a\3\2"+
		"\2\2\u0099\u0095\3\2\2\2\u009a\u009d\3\2\2\2\u009b\u0099\3\2\2\2\u009b"+
		"\u009c\3\2\2\2\u009c\u009e\3\2\2\2\u009d\u009b\3\2\2\2\u009e\u00b3\5\30"+
		"\r\2\u009f\u00a1\5\22\n\2\u00a0\u00a2\7S\2\2\u00a1\u00a0\3\2\2\2\u00a1"+
		"\u00a2\3\2\2\2\u00a2\u00a4\3\2\2\2\u00a3\u009f\3\2\2\2\u00a4\u00a7\3\2"+
		"\2\2\u00a5\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a8\3\2\2\2\u00a7"+
		"\u00a5\3\2\2\2\u00a8\u00af\5\20\t\2\u00a9\u00ab\7S\2\2\u00aa\u00a9\3\2"+
		"\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\3\2\2\2\u00ac\u00ae\5\20\t\2\u00ad"+
		"\u00aa\3\2\2\2\u00ae\u00b1\3\2\2\2\u00af\u00ad\3\2\2\2\u00af\u00b0\3\2"+
		"\2\2\u00b0\u00b3\3\2\2\2\u00b1\u00af\3\2\2\2\u00b2\u009b\3\2\2\2\u00b2"+
		"\u00a5\3\2\2\2\u00b3\r\3\2\2\2\u00b4\u00b6\5\22\n\2\u00b5\u00b7\7S\2\2"+
		"\u00b6\u00b5\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b9\3\2\2\2\u00b8\u00b4"+
		"\3\2\2\2\u00b9\u00bc\3\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb"+
		"\u00bd\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bd\u00bf\5\32\16\2\u00be\u00c0\7"+
		"S\2\2\u00bf\u00be\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c2\3\2\2\2\u00c1"+
		"\u00ba\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2"+
		"\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c6\5\f\7\2\u00c6\17\3\2\2\2\u00c7\u00c8"+
		"\5\26\f\2\u00c8\21\3\2\2\2\u00c9\u00ca\5\24\13\2\u00ca\23\3\2\2\2\u00cb"+
		"\u00cc\7)\2\2\u00cc\u00ce\7S\2\2\u00cd\u00cb\3\2\2\2\u00cd\u00ce\3\2\2"+
		"\2\u00ce\u00cf\3\2\2\2\u00cf\u00d1\7*\2\2\u00d0\u00d2\7S\2\2\u00d1\u00d0"+
		"\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d8\5*\26\2\u00d4"+
		"\u00d6\7S\2\2\u00d5\u00d4\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7\3\2"+
		"\2\2\u00d7\u00d9\5(\25\2\u00d8\u00d5\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9"+
		"\25\3\2\2\2\u00da\u00dc\7+\2\2\u00db\u00dd\7S\2\2\u00dc\u00db\3\2\2\2"+
		"\u00dc\u00dd\3\2\2\2\u00dd\u00de\3\2\2\2\u00de\u00df\5*\26\2\u00df\27"+
		"\3\2\2\2\u00e0\u00e1\7,\2\2\u00e1\u00e2\5\34\17\2\u00e2\31\3\2\2\2\u00e3"+
		"\u00e4\7-\2\2\u00e4\u00e9\5\34\17\2\u00e5\u00e7\7S\2\2\u00e6\u00e5\3\2"+
		"\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e8\3\2\2\2\u00e8\u00ea\5(\25\2\u00e9"+
		"\u00e6\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\33\3\2\2\2\u00eb\u00ec\7S\2\2"+
		"\u00ec\u00ef\5\36\20\2\u00ed\u00ee\7S\2\2\u00ee\u00f0\5\"\22\2\u00ef\u00ed"+
		"\3\2\2\2\u00ef\u00f0\3\2\2\2\u00f0\u00f3\3\2\2\2\u00f1\u00f2\7S\2\2\u00f2"+
		"\u00f4\5$\23\2\u00f3\u00f1\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\35\3\2\2"+
		"\2\u00f5\u0100\7\4\2\2\u00f6\u00f8\7S\2\2\u00f7\u00f6\3\2\2\2\u00f7\u00f8"+
		"\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fb\7\5\2\2\u00fa\u00fc\7S\2\2\u00fb"+
		"\u00fa\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00ff\5 "+
		"\21\2\u00fe\u00f7\3\2\2\2\u00ff\u0102\3\2\2\2\u0100\u00fe\3\2\2\2\u0100"+
		"\u0101\3\2\2\2\u0101\u0112\3\2\2\2\u0102\u0100\3\2\2\2\u0103\u010e\5 "+
		"\21\2\u0104\u0106\7S\2\2\u0105\u0104\3\2\2\2\u0105\u0106\3\2\2\2\u0106"+
		"\u0107\3\2\2\2\u0107\u0109\7\5\2\2\u0108\u010a\7S\2\2\u0109\u0108\3\2"+
		"\2\2\u0109\u010a\3\2\2\2\u010a\u010b\3\2\2\2\u010b\u010d\5 \21\2\u010c"+
		"\u0105\3\2\2\2\u010d\u0110\3\2\2\2\u010e\u010c\3\2\2\2\u010e\u010f\3\2"+
		"\2\2\u010f\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0111\u00f5\3\2\2\2\u0111"+
		"\u0103\3\2\2\2\u0112\37\3\2\2\2\u0113\u0114\5F$\2\u0114\u0115\7S\2\2\u0115"+
		"\u0116\7.\2\2\u0116\u0117\7S\2\2\u0117\u0118\5d\63\2\u0118\u011b\3\2\2"+
		"\2\u0119\u011b\5F$\2\u011a\u0113\3\2\2\2\u011a\u0119\3\2\2\2\u011b!\3"+
		"\2\2\2\u011c\u011d\7/\2\2\u011d\u011e\7S\2\2\u011e\u011f\7\60\2\2\u011f"+
		"\u0120\7S\2\2\u0120\u0128\5&\24\2\u0121\u0123\7\5\2\2\u0122\u0124\7S\2"+
		"\2\u0123\u0122\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0127"+
		"\5&\24\2\u0126\u0121\3\2\2\2\u0127\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128"+
		"\u0129\3\2\2\2\u0129#\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u012c\7\61\2\2"+
		"\u012c\u012d\7S\2\2\u012d\u012e\5F$\2\u012e%\3\2\2\2\u012f\u0134\5F$\2"+
		"\u0130\u0132\7S\2\2\u0131\u0130\3\2\2\2\u0131\u0132\3\2\2\2\u0132\u0133"+
		"\3\2\2\2\u0133\u0135\t\2\2\2\u0134\u0131\3\2\2\2\u0134\u0135\3\2\2\2\u0135"+
		"\'\3\2\2\2\u0136\u0137\7\66\2\2\u0137\u0138\7S\2\2\u0138\u0139\5F$\2\u0139"+
		")\3\2\2\2\u013a\u0145\5,\27\2\u013b\u013d\7S\2\2\u013c\u013b\3\2\2\2\u013c"+
		"\u013d\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u0140\7\5\2\2\u013f\u0141\7S"+
		"\2\2\u0140\u013f\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u0142\3\2\2\2\u0142"+
		"\u0144\5,\27\2\u0143\u013c\3\2\2\2\u0144\u0147\3\2\2\2\u0145\u0143\3\2"+
		"\2\2\u0145\u0146\3\2\2\2\u0146+\3\2\2\2\u0147\u0145\3\2\2\2\u0148\u014a"+
		"\5d\63\2\u0149\u014b\7S\2\2\u014a\u0149\3\2\2\2\u014a\u014b\3\2\2\2\u014b"+
		"\u014c\3\2\2\2\u014c\u014e\7\6\2\2\u014d\u014f\7S\2\2\u014e\u014d\3\2"+
		"\2\2\u014e\u014f\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151\5.\30\2\u0151"+
		"\u0154\3\2\2\2\u0152\u0154\5.\30\2\u0153\u0148\3\2\2\2\u0153\u0152\3\2"+
		"\2\2\u0154-\3\2\2\2\u0155\u0156\5\60\31\2\u0156/\3\2\2\2\u0157\u015e\5"+
		"\62\32\2\u0158\u015a\7S\2\2\u0159\u0158\3\2\2\2\u0159\u015a\3\2\2\2\u015a"+
		"\u015b\3\2\2\2\u015b\u015d\5\64\33\2\u015c\u0159\3\2\2\2\u015d\u0160\3"+
		"\2\2\2\u015e\u015c\3\2\2\2\u015e\u015f\3\2\2\2\u015f\u0166\3\2\2\2\u0160"+
		"\u015e\3\2\2\2\u0161\u0162\7\7\2\2\u0162\u0163\5\60\31\2\u0163\u0164\7"+
		"\b\2\2\u0164\u0166\3\2\2\2\u0165\u0157\3\2\2\2\u0165\u0161\3\2\2\2\u0166"+
		"\61\3\2\2\2\u0167\u0169\7\7\2\2\u0168\u016a\7S\2\2\u0169\u0168\3\2\2\2"+
		"\u0169\u016a\3\2\2\2\u016a\u016f\3\2\2\2\u016b\u016d\5d\63\2\u016c\u016e"+
		"\7S\2\2\u016d\u016c\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u0170\3\2\2\2\u016f"+
		"\u016b\3\2\2\2\u016f\u0170\3\2\2\2\u0170\u0175\3\2\2\2\u0171\u0173\5>"+
		" \2\u0172\u0174\7S\2\2\u0173\u0172\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176"+
		"\3\2\2\2\u0175\u0171\3\2\2\2\u0175\u0176\3\2\2\2\u0176\u017b\3\2\2\2\u0177"+
		"\u0179\5:\36\2\u0178\u017a\7S\2\2\u0179\u0178\3\2\2\2\u0179\u017a\3\2"+
		"\2\2\u017a\u017c\3\2\2\2\u017b\u0177\3\2\2\2\u017b\u017c\3\2\2\2\u017c"+
		"\u017d\3\2\2\2\u017d\u017e\7\b\2\2\u017e\63\3\2\2\2\u017f\u0181\5\66\34"+
		"\2\u0180\u0182\7S\2\2\u0181\u0180\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0183"+
		"\3\2\2\2\u0183\u0184\5\62\32\2\u0184\65\3\2\2\2\u0185\u0187\5v<\2\u0186"+
		"\u0188\7S\2\2\u0187\u0186\3\2\2\2\u0187\u0188\3\2\2\2\u0188\u0189\3\2"+
		"\2\2\u0189\u018b\5z>\2\u018a\u018c\7S\2\2\u018b\u018a\3\2\2\2\u018b\u018c"+
		"\3\2\2\2\u018c\u018e\3\2\2\2\u018d\u018f\58\35\2\u018e\u018d\3\2\2\2\u018e"+
		"\u018f\3\2\2\2\u018f\u0191\3\2\2\2\u0190\u0192\7S\2\2\u0191\u0190\3\2"+
		"\2\2\u0191\u0192\3\2\2\2\u0192\u0193\3\2\2\2\u0193\u0195\5z>\2\u0194\u0196"+
		"\7S\2\2\u0195\u0194\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0197\3\2\2\2\u0197"+
		"\u0198\5x=\2\u0198\u01ba\3\2\2\2\u0199\u019b\5v<\2\u019a\u019c\7S\2\2"+
		"\u019b\u019a\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u019d\3\2\2\2\u019d\u019f"+
		"\5z>\2\u019e\u01a0\7S\2\2\u019f\u019e\3\2\2\2\u019f\u01a0\3\2\2\2\u01a0"+
		"\u01a2\3\2\2\2\u01a1\u01a3\58\35\2\u01a2\u01a1\3\2\2\2\u01a2\u01a3\3\2"+
		"\2\2\u01a3\u01a5\3\2\2\2\u01a4\u01a6\7S\2\2\u01a5\u01a4\3\2\2\2\u01a5"+
		"\u01a6\3\2\2\2\u01a6\u01a7\3\2\2\2\u01a7\u01a8\5z>\2\u01a8\u01ba\3\2\2"+
		"\2\u01a9\u01ab\5z>\2\u01aa\u01ac\7S\2\2\u01ab\u01aa\3\2\2\2\u01ab\u01ac"+
		"\3\2\2\2\u01ac\u01ae\3\2\2\2\u01ad\u01af\58\35\2\u01ae\u01ad\3\2\2\2\u01ae"+
		"\u01af\3\2\2\2\u01af\u01b1\3\2\2\2\u01b0\u01b2\7S\2\2\u01b1\u01b0\3\2"+
		"\2\2\u01b1\u01b2\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b5\5z>\2\u01b4\u01b6"+
		"\7S\2\2\u01b5\u01b4\3\2\2\2\u01b5\u01b6\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7"+
		"\u01b8\5x=\2\u01b8\u01ba\3\2\2\2\u01b9\u0185\3\2\2\2\u01b9\u0199\3\2\2"+
		"\2\u01b9\u01a9\3\2\2\2\u01ba\67\3\2\2\2\u01bb\u01bd\7\t\2\2\u01bc\u01be"+
		"\7S\2\2\u01bd\u01bc\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\u01c3\3\2\2\2\u01bf"+
		"\u01c1\5d\63\2\u01c0\u01c2\7S\2\2\u01c1\u01c0\3\2\2\2\u01c1\u01c2\3\2"+
		"\2\2\u01c2\u01c4\3\2\2\2\u01c3\u01bf\3\2\2\2\u01c3\u01c4\3\2\2\2\u01c4"+
		"\u01c9\3\2\2\2\u01c5\u01c7\5<\37\2\u01c6\u01c8\7S\2\2\u01c7\u01c6\3\2"+
		"\2\2\u01c7\u01c8\3\2\2\2\u01c8\u01ca\3\2\2\2\u01c9\u01c5\3\2\2\2\u01c9"+
		"\u01ca\3\2\2\2\u01ca\u01cf\3\2\2\2\u01cb\u01cd\5:\36\2\u01cc\u01ce\7S"+
		"\2\2\u01cd\u01cc\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf"+
		"\u01cb\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d2\7\n"+
		"\2\2\u01d29\3\2\2\2\u01d3\u01d4\5h\65\2\u01d4;\3\2\2\2\u01d5\u01d7\7\13"+
		"\2\2\u01d6\u01d8\7S\2\2\u01d7\u01d6\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8"+
		"\u01d9\3\2\2\2\u01d9\u01da\5D#\2\u01da=\3\2\2\2\u01db\u01e2\5@!\2\u01dc"+
		"\u01de\7S\2\2\u01dd\u01dc\3\2\2\2\u01dd\u01de\3\2\2\2\u01de\u01df\3\2"+
		"\2\2\u01df\u01e1\5@!\2\u01e0\u01dd\3\2\2\2\u01e1\u01e4\3\2\2\2\u01e2\u01e0"+
		"\3\2\2\2\u01e2\u01e3\3\2\2\2\u01e3?\3\2\2\2\u01e4\u01e2\3\2\2\2\u01e5"+
		"\u01e7\7\13\2\2\u01e6\u01e8\7S\2\2\u01e7\u01e6\3\2\2\2\u01e7\u01e8\3\2"+
		"\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01ea\5B\"\2\u01eaA\3\2\2\2\u01eb\u01ec"+
		"\5p9\2\u01ecC\3\2\2\2\u01ed\u01ee\5p9\2\u01eeE\3\2\2\2\u01ef\u01f0\5H"+
		"%\2\u01f0G\3\2\2\2\u01f1\u01f8\5J&\2\u01f2\u01f3\7S\2\2\u01f3\u01f4\7"+
		"\67\2\2\u01f4\u01f5\7S\2\2\u01f5\u01f7\5J&\2\u01f6\u01f2\3\2\2\2\u01f7"+
		"\u01fa\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9I\3\2\2\2"+
		"\u01fa\u01f8\3\2\2\2\u01fb\u0207\5L\'\2\u01fc\u0203\5L\'\2\u01fd\u01ff"+
		"\7S\2\2\u01fe\u01fd\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff\u0200\3\2\2\2\u0200"+
		"\u0202\5Z.\2\u0201\u01fe\3\2\2\2\u0202\u0205\3\2\2\2\u0203\u0201\3\2\2"+
		"\2\u0203\u0204\3\2\2\2\u0204\u0207\3\2\2\2\u0205\u0203\3\2\2\2\u0206\u01fb"+
		"\3\2\2\2\u0206\u01fc\3\2\2\2\u0207K\3\2\2\2\u0208\u020c\5P)\2\u0209\u020b"+
		"\5N(\2\u020a\u0209\3\2\2\2\u020b\u020e\3\2\2\2\u020c\u020a\3\2\2\2\u020c"+
		"\u020d\3\2\2\2\u020dM\3\2\2\2\u020e\u020c\3\2\2\2\u020f\u0211\7S\2\2\u0210"+
		"\u020f\3\2\2\2\u0210\u0211\3\2\2\2\u0211\u0212\3\2\2\2\u0212\u0213\7\t"+
		"\2\2\u0213\u0214\5F$\2\u0214\u0215\7\n\2\2\u0215\u0223\3\2\2\2\u0216\u0218"+
		"\7S\2\2\u0217\u0216\3\2\2\2\u0217\u0218\3\2\2\2\u0218\u0219\3\2\2\2\u0219"+
		"\u021b\7\t\2\2\u021a\u021c\5F$\2\u021b\u021a\3\2\2\2\u021b\u021c\3\2\2"+
		"\2\u021c\u021d\3\2\2\2\u021d\u021f\7\f\2\2\u021e\u0220\5F$\2\u021f\u021e"+
		"\3\2\2\2\u021f\u0220\3\2\2\2\u0220\u0221\3\2\2\2\u0221\u0223\7\n\2\2\u0222"+
		"\u0210\3\2\2\2\u0222\u0217\3\2\2\2\u0223O\3\2\2\2\u0224\u0236\5R*\2\u0225"+
		"\u022c\5R*\2\u0226\u0228\7S\2\2\u0227\u0226\3\2\2\2\u0227\u0228\3\2\2"+
		"\2\u0228\u0229\3\2\2\2\u0229\u022b\5^\60\2\u022a\u0227\3\2\2\2\u022b\u022e"+
		"\3\2\2\2\u022c\u022a\3\2\2\2\u022c\u022d\3\2\2\2\u022d\u0233\3\2\2\2\u022e"+
		"\u022c\3\2\2\2\u022f\u0231\7S\2\2\u0230\u022f\3\2\2\2\u0230\u0231\3\2"+
		"\2\2\u0231\u0232\3\2\2\2\u0232\u0234\5> \2\u0233\u0230\3\2\2\2\u0233\u0234"+
		"\3\2\2\2\u0234\u0236\3\2\2\2\u0235\u0224\3\2\2\2\u0235\u0225\3\2\2\2\u0236"+
		"Q\3\2\2\2\u0237\u023c\5T+\2\u0238\u023c\5d\63\2\u0239\u023c\5\\/\2\u023a"+
		"\u023c\5`\61\2\u023b\u0237\3\2\2\2\u023b\u0238\3\2\2\2\u023b\u0239\3\2"+
		"\2\2\u023b\u023a\3\2\2\2\u023cS\3\2\2\2\u023d\u0243\5f\64\2\u023e\u0243"+
		"\7?\2\2\u023f\u0243\5V,\2\u0240\u0243\5h\65\2\u0241\u0243\5X-\2\u0242"+
		"\u023d\3\2\2\2\u0242\u023e\3\2\2\2\u0242\u023f\3\2\2\2\u0242\u0240\3\2"+
		"\2\2\u0242\u0241\3\2\2\2\u0243U\3\2\2\2\u0244\u0245\t\3\2\2\u0245W\3\2"+
		"\2\2\u0246\u0248\7\t\2\2\u0247\u0249\7S\2\2\u0248\u0247\3\2\2\2\u0248"+
		"\u0249\3\2\2\2\u0249\u025b\3\2\2\2\u024a\u024c\5F$\2\u024b\u024d\7S\2"+
		"\2\u024c\u024b\3\2\2\2\u024c\u024d\3\2\2\2\u024d\u0258\3\2\2\2\u024e\u0250"+
		"\7\5\2\2\u024f\u0251\7S\2\2\u0250\u024f\3\2\2\2\u0250\u0251\3\2\2\2\u0251"+
		"\u0252\3\2\2\2\u0252\u0254\5F$\2\u0253\u0255\7S\2\2\u0254\u0253\3\2\2"+
		"\2\u0254\u0255\3\2\2\2\u0255\u0257\3\2\2\2\u0256\u024e\3\2\2\2\u0257\u025a"+
		"\3\2\2\2\u0258\u0256\3\2\2\2\u0258\u0259\3\2\2\2\u0259\u025c\3\2\2\2\u025a"+
		"\u0258\3\2\2\2\u025b\u024a\3\2\2\2\u025b\u025c\3\2\2\2\u025c\u025d\3\2"+
		"\2\2\u025d\u025e\7\n\2\2\u025eY\3\2\2\2\u025f\u0261\7\6\2\2\u0260\u0262"+
		"\7S\2\2\u0261\u0260\3\2\2\2\u0261\u0262\3\2\2\2\u0262\u0263\3\2\2\2\u0263"+
		"\u027e\5L\'\2\u0264\u0266\7\r\2\2\u0265\u0267\7S\2\2\u0266\u0265\3\2\2"+
		"\2\u0266\u0267\3\2\2\2\u0267\u0268\3\2\2\2\u0268\u027e\5L\'\2\u0269\u026b"+
		"\7\16\2\2\u026a\u026c\7S\2\2\u026b\u026a\3\2\2\2\u026b\u026c\3\2\2\2\u026c"+
		"\u026d\3\2\2\2\u026d\u027e\5L\'\2\u026e\u0270\7\17\2\2\u026f\u0271\7S"+
		"\2\2\u0270\u026f\3\2\2\2\u0270\u0271\3\2\2\2\u0271\u0272\3\2\2\2\u0272"+
		"\u027e\5L\'\2\u0273\u0275\7\20\2\2\u0274\u0276\7S\2\2\u0275\u0274\3\2"+
		"\2\2\u0275\u0276\3\2\2\2\u0276\u0277\3\2\2\2\u0277\u027e\5L\'\2\u0278"+
		"\u027a\7\21\2\2\u0279\u027b\7S\2\2\u027a\u0279\3\2\2\2\u027a\u027b\3\2"+
		"\2\2\u027b\u027c\3\2\2\2\u027c\u027e\5L\'\2\u027d\u025f\3\2\2\2\u027d"+
		"\u0264\3\2\2\2\u027d\u0269\3\2\2\2\u027d\u026e\3\2\2\2\u027d\u0273\3\2"+
		"\2\2\u027d\u0278\3\2\2\2\u027e[\3\2\2\2\u027f\u0281\7\7\2\2\u0280\u0282"+
		"\7S\2\2\u0281\u0280\3\2\2\2\u0281\u0282\3\2\2\2\u0282\u0283\3\2\2\2\u0283"+
		"\u0285\5F$\2\u0284\u0286\7S\2\2\u0285\u0284\3\2\2\2\u0285\u0286\3\2\2"+
		"\2\u0286\u0287\3\2\2\2\u0287\u0288\7\b\2\2\u0288]\3\2\2\2\u0289\u028b"+
		"\7\22\2\2\u028a\u028c\7S\2\2\u028b\u028a\3\2\2\2\u028b\u028c\3\2\2\2\u028c"+
		"\u028d\3\2\2\2\u028d\u028e\5j\66\2\u028e_\3\2\2\2\u028f\u0294\7:\2\2\u0290"+
		"\u0292\7S\2\2\u0291\u0290\3\2\2\2\u0291\u0292\3\2\2\2\u0292\u0293\3\2"+
		"\2\2\u0293\u0295\5b\62\2\u0294\u0291\3\2\2\2\u0295\u0296\3\2\2\2\u0296"+
		"\u0294\3\2\2\2\u0296\u0297\3\2\2\2\u0297\u02a6\3\2\2\2\u0298\u029a\7:"+
		"\2\2\u0299\u029b\7S\2\2\u029a\u0299\3\2\2\2\u029a\u029b\3\2\2\2\u029b"+
		"\u029c\3\2\2\2\u029c\u02a1\5F$\2\u029d\u029f\7S\2\2\u029e\u029d\3\2\2"+
		"\2\u029e\u029f\3\2\2\2\u029f\u02a0\3\2\2\2\u02a0\u02a2\5b\62\2\u02a1\u029e"+
		"\3\2\2\2\u02a2\u02a3\3\2\2\2\u02a3\u02a1\3\2\2\2\u02a3\u02a4\3\2\2\2\u02a4"+
		"\u02a6\3\2\2\2\u02a5\u028f\3\2\2\2\u02a5\u0298\3\2\2\2\u02a6\u02af\3\2"+
		"\2\2\u02a7\u02a9\7S\2\2\u02a8\u02a7\3\2\2\2\u02a8\u02a9\3\2\2\2\u02a9"+
		"\u02aa\3\2\2\2\u02aa\u02ac\7;\2\2\u02ab\u02ad\7S\2\2\u02ac\u02ab\3\2\2"+
		"\2\u02ac\u02ad\3\2\2\2\u02ad\u02ae\3\2\2\2\u02ae\u02b0\5F$\2\u02af\u02a8"+
		"\3\2\2\2\u02af\u02b0\3\2\2\2\u02b0\u02b2\3\2\2\2\u02b1\u02b3\7S\2\2\u02b2"+
		"\u02b1\3\2\2\2\u02b2\u02b3\3\2\2\2\u02b3\u02b4\3\2\2\2\u02b4\u02b5\7<"+
		"\2\2\u02b5a\3\2\2\2\u02b6\u02b8\7=\2\2\u02b7\u02b9\7S\2\2\u02b8\u02b7"+
		"\3\2\2\2\u02b8\u02b9\3\2\2\2\u02b9\u02ba\3\2\2\2\u02ba\u02bc\5F$\2\u02bb"+
		"\u02bd\7S\2\2\u02bc\u02bb\3\2\2\2\u02bc\u02bd\3\2\2\2\u02bd\u02be\3\2"+
		"\2\2\u02be\u02c0\7>\2\2\u02bf\u02c1\7S\2\2\u02c0\u02bf\3\2\2\2\u02c0\u02c1"+
		"\3\2\2\2\u02c1\u02c2\3\2\2\2\u02c2\u02c3\5F$\2\u02c3c\3\2\2\2\u02c4\u02c5"+
		"\5t;\2\u02c5e\3\2\2\2\u02c6\u02c9\5n8\2\u02c7\u02c9\5l\67\2\u02c8\u02c6"+
		"\3\2\2\2\u02c8\u02c7\3\2\2\2\u02c9g\3\2\2\2\u02ca\u02cc\7\23\2\2\u02cb"+
		"\u02cd\7S\2\2\u02cc\u02cb\3\2\2\2\u02cc\u02cd\3\2\2\2\u02cd\u02ef\3\2"+
		"\2\2\u02ce\u02d0\5j\66\2\u02cf\u02d1\7S\2\2\u02d0\u02cf\3\2\2\2\u02d0"+
		"\u02d1\3\2\2\2\u02d1\u02d2\3\2\2\2\u02d2\u02d4\7\13\2\2\u02d3\u02d5\7"+
		"S\2\2\u02d4\u02d3\3\2\2\2\u02d4\u02d5\3\2\2\2\u02d5\u02d6\3\2\2\2\u02d6"+
		"\u02d8\5F$\2\u02d7\u02d9\7S\2\2\u02d8\u02d7\3\2\2\2\u02d8\u02d9\3\2\2"+
		"\2\u02d9\u02ec\3\2\2\2\u02da\u02dc\7\5\2\2\u02db\u02dd\7S\2\2\u02dc\u02db"+
		"\3\2\2\2\u02dc\u02dd\3\2\2\2\u02dd\u02de\3\2\2\2\u02de\u02e0\5j\66\2\u02df"+
		"\u02e1\7S\2\2\u02e0\u02df\3\2\2\2\u02e0\u02e1\3\2\2\2\u02e1\u02e2\3\2"+
		"\2\2\u02e2\u02e4\7\13\2\2\u02e3\u02e5\7S\2\2\u02e4\u02e3\3\2\2\2\u02e4"+
		"\u02e5\3\2\2\2\u02e5\u02e6\3\2\2\2\u02e6\u02e8\5F$\2\u02e7\u02e9\7S\2"+
		"\2\u02e8\u02e7\3\2\2\2\u02e8\u02e9\3\2\2\2\u02e9\u02eb\3\2\2\2\u02ea\u02da"+
		"\3\2\2\2\u02eb\u02ee\3\2\2\2\u02ec\u02ea\3\2\2\2\u02ec\u02ed\3\2\2\2\u02ed"+
		"\u02f0\3\2\2\2\u02ee\u02ec\3\2\2\2\u02ef\u02ce\3\2\2\2\u02ef\u02f0\3\2"+
		"\2\2\u02f0\u02f1\3\2\2\2\u02f1\u02f2\7\24\2\2\u02f2i\3\2\2\2\u02f3\u02f4"+
		"\5p9\2\u02f4k\3\2\2\2\u02f5\u02f6\t\4\2\2\u02f6m\3\2\2\2\u02f7\u02f8\t"+
		"\5\2\2\u02f8o\3\2\2\2\u02f9\u02fc\5t;\2\u02fa\u02fc\5r:\2\u02fb\u02f9"+
		"\3\2\2\2\u02fb\u02fa\3\2\2\2\u02fcq\3\2\2\2\u02fd\u02fe\t\6\2\2\u02fe"+
		"s\3\2\2\2\u02ff\u0300\t\7\2\2\u0300u\3\2\2\2\u0301\u0302\t\b\2\2\u0302"+
		"w\3\2\2\2\u0303\u0304\t\t\2\2\u0304y\3\2\2\2\u0305\u0306\t\n\2\2\u0306"+
		"{\3\2\2\2\u008b}\u0081\u0084\u0087\u0093\u0097\u009b\u00a1\u00a5\u00aa"+
		"\u00af\u00b2\u00b6\u00ba\u00bf\u00c3\u00cd\u00d1\u00d5\u00d8\u00dc\u00e6"+
		"\u00e9\u00ef\u00f3\u00f7\u00fb\u0100\u0105\u0109\u010e\u0111\u011a\u0123"+
		"\u0128\u0131\u0134\u013c\u0140\u0145\u014a\u014e\u0153\u0159\u015e\u0165"+
		"\u0169\u016d\u016f\u0173\u0175\u0179\u017b\u0181\u0187\u018b\u018e\u0191"+
		"\u0195\u019b\u019f\u01a2\u01a5\u01ab\u01ae\u01b1\u01b5\u01b9\u01bd\u01c1"+
		"\u01c3\u01c7\u01c9\u01cd\u01cf\u01d7\u01dd\u01e2\u01e7\u01f8\u01fe\u0203"+
		"\u0206\u020c\u0210\u0217\u021b\u021f\u0222\u0227\u022c\u0230\u0233\u0235"+
		"\u023b\u0242\u0248\u024c\u0250\u0254\u0258\u025b\u0261\u0266\u026b\u0270"+
		"\u0275\u027a\u027d\u0281\u0285\u028b\u0291\u0296\u029a\u029e\u02a3\u02a5"+
		"\u02a8\u02ac\u02af\u02b2\u02b8\u02bc\u02c0\u02c8\u02cc\u02d0\u02d4\u02d8"+
		"\u02dc\u02e0\u02e4\u02e8\u02ec\u02ef\u02fb";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}