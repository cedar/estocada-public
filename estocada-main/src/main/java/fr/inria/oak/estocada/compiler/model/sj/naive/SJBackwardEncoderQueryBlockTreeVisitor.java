package fr.inria.oak.estocada.compiler.model.sj.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Tgd;
import fr.inria.oak.estocada.compiler.ChildBlock;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeVisitor;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.model.sj.SJExtractVariableToCreatedNodeVisitor;
import fr.inria.oak.estocada.compiler.model.sj.Utils;
import fr.inria.oak.estocada.rewriter.Comment;

@Singleton
class SJBackwardEncoderQueryBlockTreeVisitor implements QueryBlockTreeVisitor {
    private final SJExtractVariableToCreatedNodeVisitor sJExtractVariableToCreatedNodeVisitor;
    private Variable viewID;
    private ImmutableList.Builder<Constraint> builder;
    private boolean includeComments;

    @Inject
    public SJBackwardEncoderQueryBlockTreeVisitor(
            final SJExtractVariableToCreatedNodeVisitor sJExtractVariableToCreatedNodeVisitor) {
        this.sJExtractVariableToCreatedNodeVisitor = sJExtractVariableToCreatedNodeVisitor;
        this.viewID = null;
    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, final Variable viewID,
            boolean includeComments) {
        this.viewID = viewID;
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(final RootBlock block) {
        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
            }
            builder.add(getBackwardConstraintForBodyEncoding(block));
        }
    }

    @Override
    public void visit(final ChildBlock block) {

    }

    private Constraint getBackwardConstraintForBodyEncoding(final RootBlock block) {
        final String viewName = block.getQueryName();
        final List<Atom> premise = new ArrayList<Atom>();
        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.addAll(block.getPattern().encoding(Utils.conditionEncoding));
        sJExtractVariableToCreatedNodeVisitor.encode(viewID, viewName);
        premise.addAll(sJExtractVariableToCreatedNodeVisitor.encode(block.getReturnTemplate()));
        premise.add(Utils.createRootAtom(viewID, viewName));
        return new Tgd(premise, conclusion);
    }

}