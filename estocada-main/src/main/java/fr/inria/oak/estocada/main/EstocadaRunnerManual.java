package fr.inria.oak.estocada.main;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.oak.estocada.main.scenarios.BigDAWGScenariosMIMIC;
import fr.inria.oak.estocada.main.scenarios.TatooineScenariosMIMIC;
import fr.inria.oak.estocada.main.scenarios.TatooineScenariosMIMICCMQ;

/**
 * This class is used to debug an end-to-end ESTOCADA scenario.
 *
 * @author Rana Alotaibi
 *
 */
@Singleton
public class EstocadaRunnerManual {
    private static final Logger LOGGER = Logger.getLogger(EstocadaRunnerManual.class);
    private final String inputQuerysAndViewsFolder;

    @Inject
    private EstocadaRunnerManual(@Named("cases.folder.manual") final String inputQuerysAndViewsFolder) {
        Logger.getRootLogger().setLevel(Level.INFO);

        this.inputQuerysAndViewsFolder = checkNotNull(inputQuerysAndViewsFolder);
        try {
            Parameters.init();
            RegisterMetaDataManual.registerMetaData();
        } catch (Exception e) {
            LOGGER.error("Could not instantiates the execution engine (Tatooine): " + e.getMessage());
        }

    }

    /**
     * Run
     * 
     * @throws Exception
     */
    public void runTatooineSMQCMV() throws Exception {

        final File directory = new File(inputQuerysAndViewsFolder);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                final String folderName = folder.getName();
                switch (folderName) {
                    case "Q01":
                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q1RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    //                    case "Q02":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q2RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q03":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q3RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q04":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q4RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q05":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q5RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q06":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q6RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q07":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q7RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q08":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q8RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q09":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q9RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q10":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q10RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q11":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q11RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q12":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q12RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q13":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q13RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q14":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q14RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q15":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q15RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q16":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q16RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q17":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q17RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q18":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q18RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q19":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q19RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q20":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q20RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q21":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q21RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q22":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q22RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q23":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q23RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q24":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q24RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q25":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q25RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q26":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q26RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q27":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q27RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q28":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q28RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q29":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q29RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q30":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q30RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q31":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q31RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q32":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q32RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q33":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q33RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q34":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q34RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q35":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q35RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q36":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q36RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q37":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q37RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q38":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q38RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q39":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q39RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q40":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q40RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q41":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q41RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q42":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q42RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q43":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q43RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q44":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q44RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q45":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q45RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q46":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q46RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q50":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q50RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q51":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q51RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q52":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q52RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q53":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q52RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q54":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q54RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q55":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q55RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q56":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q56RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q57":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q57RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q58":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q58RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q59":
                    //                        TatooineScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q59RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                }
            }
        }
    }

    /**
     * Run
     * 
     * @throws Exception
     */
    public void runBigDAWGSMQCMV() throws Exception {

        final File directory = new File(inputQuerysAndViewsFolder);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                final String folderName = folder.getName();
                switch (folderName) {
                    case "Q01":
                        //BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                        BigDAWGScenariosMIMIC.Q1RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    //                    case "Q02":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q2RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q03":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q3RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q04":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q4RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q05":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q5RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q06":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q6RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q07":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q7RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q08":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q8RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q09":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q9RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q10":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q10RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q11":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q11RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q12":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q12RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q13":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q13RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q14":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q14RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q15":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q15RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q16":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q16RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q17":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q17RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q18":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q18RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q19":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q19RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q20":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q20RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q21":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q21RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q22":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q22RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q23":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q23RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q24":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q24RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q25":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q25RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q26":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q26RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q27":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q27RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q28":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q28RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q29":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q29RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q30":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q30RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q31":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q31RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q32":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q32RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q33":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q33RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q34":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q34RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q35":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q35RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q36":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q36RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q37":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q37RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q38":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q38RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q39":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q39RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q40":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q40RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q41":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q41RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q42":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q42RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q43":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q43RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q44":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q44RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q45":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q45RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q46":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q46RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q50":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q50RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q51":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q51RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q52":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q52RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q53":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q53RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q54":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q54RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q55":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q55RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q56":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q56RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q57":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q57RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q58":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q58RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q59":
                    //                        BigDAWGScenariosMIMIC.QueryEval(folder.getAbsolutePath() + "/");
                    //                        BigDAWGScenariosMIMIC.Q59RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;

                }
            }
        }

    }

    /**
     * Run
     * 
     * @throws Exception
     */
    public void runTatooineCMQSMV() throws Exception {

        final File directory = new File(inputQuerysAndViewsFolder);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                final String folderName = folder.getName();
                switch (folderName) {
                    case "Q01":
                        TatooineScenariosMIMICCMQ.Q1QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q1RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q02":
                        TatooineScenariosMIMICCMQ.Q2QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q2RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    //                    case "Q03":
                    //                        TatooineScenariosMIMICCMQ.Q3QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q3RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //                    case "Q04":
                    //                        TatooineScenariosMIMICCMQ.Q4QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q4RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    case "Q05":
                        TatooineScenariosMIMICCMQ.Q5QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q5RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q06":
                        TatooineScenariosMIMICCMQ.Q6QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q6RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q07":
                        TatooineScenariosMIMICCMQ.Q7QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q7RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q08":
                        TatooineScenariosMIMICCMQ.Q8QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q8RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q09":
                        TatooineScenariosMIMICCMQ.Q9QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q9RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q10":
                        TatooineScenariosMIMICCMQ.Q10QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q10RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q11":
                        TatooineScenariosMIMICCMQ.Q11QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q11RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q12":
                        TatooineScenariosMIMICCMQ.Q12QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q12RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q13":
                        TatooineScenariosMIMICCMQ.Q13QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q13RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q14":
                        TatooineScenariosMIMICCMQ.Q14QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q14RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q15":
                        TatooineScenariosMIMICCMQ.Q15QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q15RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q16":
                        TatooineScenariosMIMICCMQ.Q16QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q16RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q17":
                        TatooineScenariosMIMICCMQ.Q17QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q17RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q18":
                        TatooineScenariosMIMICCMQ.Q18QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q18RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q19":
                        TatooineScenariosMIMICCMQ.Q19QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q19RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q20":
                        TatooineScenariosMIMICCMQ.Q20QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q20RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q21":
                        TatooineScenariosMIMICCMQ.Q21QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q21RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q22":
                        TatooineScenariosMIMICCMQ.Q22QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q22RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q23":
                        TatooineScenariosMIMICCMQ.Q23QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q23RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q24":
                        TatooineScenariosMIMICCMQ.Q24QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q24RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q25":
                        TatooineScenariosMIMICCMQ.Q25QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q25RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q26":
                        TatooineScenariosMIMICCMQ.Q26QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q26RewritingEval(folder.getAbsolutePath() + "/");
                        //                        break;
                    case "Q27":
                        TatooineScenariosMIMICCMQ.Q27QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q27RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q28":
                        TatooineScenariosMIMICCMQ.Q28QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q28RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q29":
                        TatooineScenariosMIMICCMQ.Q29QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q29RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q30":
                        TatooineScenariosMIMICCMQ.Q30QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q30RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    case "Q31":
                        TatooineScenariosMIMICCMQ.Q31QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q31RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q32":
                        TatooineScenariosMIMICCMQ.Q32QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q32RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q33":
                        TatooineScenariosMIMICCMQ.Q33QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q33RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q34":
                        TatooineScenariosMIMICCMQ.Q34QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q34RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q35":
                        TatooineScenariosMIMICCMQ.Q35QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q35RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q36":
                        TatooineScenariosMIMICCMQ.Q36QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q36RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q37":
                        TatooineScenariosMIMICCMQ.Q37QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q37RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q38":
                        TatooineScenariosMIMICCMQ.Q38QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q38RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q39":
                        TatooineScenariosMIMICCMQ.Q39QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q39RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q40":
                        TatooineScenariosMIMICCMQ.Q40QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q40RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q41":
                        TatooineScenariosMIMICCMQ.Q41QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q41RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q42":
                        TatooineScenariosMIMICCMQ.Q42QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q42RewritingEval(folder.getAbsolutePath() + "/");
                        break;
                    //
                    case "Q43":
                        TatooineScenariosMIMICCMQ.Q43QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q43RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q44":
                        TatooineScenariosMIMICCMQ.Q44QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q44RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q45":
                        TatooineScenariosMIMICCMQ.Q45QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q45RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    case "Q46":
                        TatooineScenariosMIMICCMQ.Q46QueryEval(folder.getAbsolutePath() + "/");
                        TatooineScenariosMIMIC.Q46RewritingEval(folder.getAbsolutePath() + "/");
                        break;

                    //                    case "Q50":
                    //                        TatooineScenariosMIMICCMQ.Q50QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q50RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q51":
                    //                        TatooineScenariosMIMICCMQ.Q51QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q51RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q52":
                    //                        TatooineScenariosMIMICCMQ.Q52QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q52RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q53":
                    //                        TatooineScenariosMIMICCMQ.Q53QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q52RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q54":
                    //                        TatooineScenariosMIMICCMQ.Q54QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q54RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q55":
                    //                        TatooineScenariosMIMICCMQ.Q55QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q55RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q56":
                    //                        TatooineScenariosMIMICCMQ.Q56QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q56RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q57":
                    //                        TatooineScenariosMIMICCMQ.Q57QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q57RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q58":
                    //                        TatooineScenariosMIMICCMQ.Q58QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q58RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;
                    //
                    //                    case "Q59":
                    //                        TatooineScenariosMIMICCMQ.Q59QueryEval(folder.getAbsolutePath() + "/");
                    //                        TatooineScenariosMIMIC.Q59RewritingEval(folder.getAbsolutePath() + "/");
                    //                        break;

                }
            }
        }
    }

}