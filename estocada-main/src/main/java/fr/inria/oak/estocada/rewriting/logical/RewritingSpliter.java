package fr.inria.oak.estocada.rewriting.logical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;

/**
 * This class splits the rewriting into sub-queries grouped by a storage reference.
 * 
 * @author ranaalotaibi
 *
 */
public class RewritingSpliter {
    private static final Logger LOGGER = Logger.getLogger(RewritingToLogicalPlanGenerator.class);
    final ConjunctiveQuery rewriting;
    final Map<StorageReference, String> mapToStorageReference;
    final Catalog catalog;

    /** Constructor **/
    public RewritingSpliter(final ConjunctiveQuery rewriting, final Catalog catalog) {
        this.rewriting = rewriting;
        this.catalog = catalog;
        mapToStorageReference = new HashMap<StorageReference, String>();
    }

    /**
     * Split the rewriting.
     * 
     * @return This function returns a maximum sub-query expression that can be executed by each single storage reference.
     */
    public Map<StorageReference, ConjunctiveQuery> split() {
        final Map<StorageReference, List<String>> storageReferenceViewsMap = mapsViewsToStorageReference();
        final Map<StorageReference, List<Atom>> storageReferenceAtomsMap = groupAtoms(storageReferenceViewsMap);
        final Map<StorageReference, ConjunctiveQuery> storageReferenceRewritingMap =
                groupRewriting(storageReferenceAtomsMap);

        return storageReferenceRewritingMap;

    }

    /**
     * Group the views that have the same storage reference.
     * 
     * @return This function returns that views predicates that share the same storage reference.
     */
    private Map<StorageReference, List<String>> mapsViewsToStorageReference() {
        final Map<StorageReference, List<String>> storageReferenceViewsMap =
                new HashMap<StorageReference, List<String>>();
        for (Atom atom : rewriting.getBody()) {
            if (!catalog.contains(atom.getPredicate())) {
                continue;
            }
            final StorageReference storageReference = catalog.getStorageReference(atom.getPredicate());

            storageReferenceViewsMap.putIfAbsent(storageReference, new ArrayList<String>());
            final List<String> viewPredicates = storageReferenceViewsMap.get(storageReference);
            if (viewPredicates.isEmpty()) {
                final String uniqueID = UUID.randomUUID().toString();
                mapToStorageReference.put(storageReference, uniqueID);
            }
            viewPredicates.add(atom.getPredicate());
        }
        return storageReferenceViewsMap;
    }

    /**
     * Given the views that have the same store reference,
     * this function finds the maximum list of atoms that can executed by each single storage reference.
     * 
     * @return This functions return that maximum sub-queries expressions that can executed by each single store.
     */
    private Map<StorageReference, List<Atom>> groupAtoms(
            final Map<StorageReference, List<String>> storageReferenceViewsMap) {
        final Map<StorageReference, List<Atom>> subQueries = new HashMap<StorageReference, List<Atom>>();
        for (final Map.Entry<StorageReference, List<String>> entery : storageReferenceViewsMap.entrySet()) {
            final List<Atom> dataSourceSubQueries = new ArrayList<>();
            for (final String viewPredicate : entery.getValue()) {
                dataSourceSubQueries.addAll(findViewAtoms(viewPredicate));
            }
            subQueries.put(entery.getKey(), dataSourceSubQueries);
        }

        return subQueries;
    }

    /**
     * Find all view atoms.
     * 
     * @param viewPredicate
     *            the view predicate.
     * @return This function returns a set of atoms that are related to a given view predicate.
     */
    private List<Atom> findViewAtoms(final String viewPredicate) {
        boolean existsViewPredicate = false;
        final List<Atom> viewAtoms = new ArrayList<Atom>();
        for (final Atom atom : rewriting.getBody()) {
            if (atom.getPredicate().equals(viewPredicate) && atom.getTerms().size() == 1) {
                final Variable rootVariable = (Variable) atom.getTerm(0);
                viewAtoms.add(atom);
                findAtom(rootVariable, viewAtoms);
                existsViewPredicate = true;
            }
            if (atom.getPredicate().equals(viewPredicate) && atom.getTerms().size() != 1) {
                final List<? extends Term> terms = atom.getTerms();
                findOtherTerms(terms, viewAtoms);
                existsViewPredicate = true;
                viewAtoms.add(atom);
            }
        }
        if (!existsViewPredicate)
            throw new RewritingToLogicalException(new IllegalStateException("View predicate could not be found"));
        else {
            return viewAtoms;
        }
    }

    /**
     * Find an atom where a given variable appears.
     * 
     * @param variable
     *            the variable
     * @param viewAtoms
     *            the list of a view atoms.
     */
    private void findAtom(final Variable variable, final List<Atom> viewAtoms) {
        for (final Atom atom : rewriting.getBody()) {
            if (!catalog.contains(atom.getPredicate()) && atom.getTerms().size() == 4
                    && atom.getTerm(0).equals(variable)) {
                viewAtoms.add(atom);
                findAtom((Variable) atom.getTerm(1), viewAtoms);
            }
            if (!catalog.contains(atom.getPredicate()) && atom.getTerms().size() == 2
                    && atom.getTerm(0).equals(variable)) {
                viewAtoms.add(atom);
            }
        }
    }

    /**
     * Construct the sub-rewritings from a given storage reference atoms map.
     * 
     * @param storageReferenceAtomsMap
     * @return
     */
    private Map<StorageReference, ConjunctiveQuery> groupRewriting(
            final Map<StorageReference, List<Atom>> storageReferenceAtomsMap) {
        final Map<StorageReference, List<Variable>> outerMap = new HashMap<StorageReference, List<Variable>>();
        outerMap.putAll(getAllTerms(storageReferenceAtomsMap));
        final Map<StorageReference, List<Variable>> innerMap = new HashMap<StorageReference, List<Variable>>();
        innerMap.putAll(getAllTerms(storageReferenceAtomsMap));

        final Map<StorageReference, List<Term>> storageReferenceJoinTermsMap =
                new HashMap<StorageReference, List<Term>>();

        for (Map.Entry<StorageReference, List<Variable>> entryOuter : outerMap.entrySet()) {
            final List<Term> joinTerms = new ArrayList<Term>();
            for (Map.Entry<StorageReference, List<Variable>> entryInner : outerMap.entrySet()) {
                if (mapToStorageReference.get(entryOuter.getKey())
                        .equals(mapToStorageReference.get(entryInner.getKey()))) {
                    continue;
                }
                final List<Variable> innerTerms = new ArrayList<Variable>();
                innerTerms.addAll(entryInner.getValue());
                final List<Variable> outerTerms = new ArrayList<Variable>();
                outerTerms.addAll(entryOuter.getValue());
                outerTerms.retainAll(innerTerms);
                newTermContainment(joinTerms, outerTerms);
            }
            storageReferenceJoinTermsMap.put(entryOuter.getKey(), joinTerms);
        }

        final Map<StorageReference, ConjunctiveQuery> subRewritings = new HashMap<StorageReference, ConjunctiveQuery>();
        for (Map.Entry<StorageReference, List<Variable>> entery : outerMap.entrySet()) {
            final List<Term> subRewritingHead = new ArrayList<Term>();
            subRewritingHead.addAll(storageReferenceJoinTermsMap.get(entery.getKey()));
            for (Term term : rewriting.getHead()) {
                if (!(subRewritingHead.contains((Variable) term)) && entery.getValue().contains((Variable) term)) {
                    subRewritingHead.add(term);
                }
            }
            subRewritings.put(entery.getKey(),
                    new ConjunctiveQuery(subRewritingHead, storageReferenceAtomsMap.get(entery.getKey())));
        }

        return subRewritings;
    }

    /**
     * Add the new term to the joinTerms if it doen't contains it.
     * 
     * @param joinTerms
     *            the set of join terms
     * @param outerTerms
     *            the set of newly terms that need to be added in a join terms list
     */
    private void newTermContainment(final List<Term> joinTerms, final List<Variable> outerTerms) {
        for (final Term term : outerTerms) {
            if (!joinTerms.contains(term)) {
                joinTerms.add(term);
            }
        }
    }

    /**
     * Get all terms that appear in the body of each storage reference sub-query.
     * 
     * @param storageReferenceAtomsMap
     * @return This function returns all terms that appear in the body of each storage reference sub-query.
     */
    private final Map<StorageReference, List<Variable>> getAllTerms(
            final Map<StorageReference, List<Atom>> storageReferenceAtomsMap) {
        final Map<StorageReference, List<Variable>> terms = new HashMap<StorageReference, List<Variable>>();
        for (Map.Entry<StorageReference, List<Atom>> entry : storageReferenceAtomsMap.entrySet()) {
            final List<Variable> storageReferneceTerms = new ArrayList<>();
            for (Atom atom : entry.getValue()) {
                for (Term term : atom.getTerms()) {
                    if (term.isVariable() && !storageReferneceTerms.contains((Variable) term))
                        storageReferneceTerms.add((Variable) term);
                }
            }
            terms.put(entry.getKey(), storageReferneceTerms);
        }
        return terms;
    }

    private void findOtherTerms(final List<? extends Term> terms, final List<Atom> viewsAtom) {
        boolean exitTerm = false;
        for (final Term term : terms) {
            for (Atom atom : rewriting.getBody()) {
                if (atom.getPredicate().equals("gte")) { // we can add other predicate
                    viewsAtom.add(atom);
                    exitTerm = true;
                    break;
                }
            }
            if (exitTerm) {
                break;
            }
        }
    }
}
