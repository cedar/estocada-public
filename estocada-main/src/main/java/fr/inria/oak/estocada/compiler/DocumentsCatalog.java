package fr.inria.oak.estocada.compiler;

import com.google.inject.Singleton;

/**
 * Documents Catalog
 * 
 * @author Rana Alotaibi
 * 
 *
 */
@Singleton
public class DocumentsCatalog {
    public String getId(final String documentName) {
        return String.valueOf(documentName.hashCode());
    }
}
