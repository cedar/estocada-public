grammar AFL; 
 
@header {
package fr.inria.oak.estocada.compiler.model.sm;
}

/* 
 * ==============
 * AFL Query
 * ==============
 */
 madQuery:
   STORE '(' aflExpression ',' arrayName ')'
 ;

/*
 * ==============
 * AFL Expression 
 * ==============
 */
 
 aflExpression
 :
 	arrayName   											#NameofArray
 	| 'multiply(' aflExpression ','  aflExpression ')'      #MatrixMulExpression
 	| 'transpose(' aflExpression ')'						#MatrixTransExpression
 
 ;
 
 /*
  * =============
  * Array Name
  * =============
  */
 
 arrayName
 :
 
   ID
 ;
/* 
 * ==============
 
/*
 * ================
 * Primitives
 * ================
 */
ID
:
	[a-zA-Z_0-9] [a-zA-Z_0-9]*
;

INT
:
	DIGIT+ [Ll]?
;

DOUBLE
:
	DIGIT+ '.' DIGIT* EXP? [Ll]?
	| DIGIT+ EXP? [Ll]?
	| '.' DIGIT+ EXP? [Ll]?
;

DIGIT
:
	'0' .. '9'
;

STRING: '"' ( ESC | ~[\\"] )*? '"' | '\'' ( ESC | ~[\\'] )*? '\'';


/*
 * =========
 * Key Words
 * =========
 */
MATRIX :

 M A T R I X
;

STORE
:
 S T O R E
;

ROWS
:
R O W S
;

COLS
:
C O L S
;

READ
:
  R E A D S
; 

fragment
EXP
:
	(
		'E'
		| 'e'
	)
	(
		'+'
		| '-'
	)? INT
;
fragment
A
:
	[aA]
;

fragment
B
:
	[bB]
;

fragment
C
:
	[cC]
;

fragment
D
:
	[dD]
;

fragment
E
:
	[eE]
;

fragment
F
:
	[fF]
;

fragment
G
:
	[gG]
;

fragment
H
:
	[hH]
;

fragment
I
:
	[iI]
;

fragment
J
:
	[jJ]
;

fragment
K
:
	[kK]
;

fragment
L
:
	[lL]
;

fragment
M
:
	[mM]
;

fragment
N
:
	[nN]
;

fragment
O
:
	[oO]
;

fragment
P
:
	[pP]
;

fragment
Q
:
	[qQ]
;

fragment
R
:
	[rR]
;

fragment
S
:
	[sS]
;

fragment
T
:
	[tT]
;

fragment
U
:
	[uU]
;

fragment
V
:
	[vV]
;

fragment
W
:
	[wW]
;

fragment
X
:
	[xX]
;

fragment
Y
:
	[yY]
;

fragment
Z
:
	[zZ]
;

fragment ESC : 
'\\' [btnfr"'\\] ;


 