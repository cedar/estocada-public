package fr.inria.oak.estocada.compiler.model.pr.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.model.pj.ArrayElementFactory;

@Singleton
public class PRRootBlockForwardEncoderReturnTermVisitor extends PRForwardEncoderReturnTermVisitor {
    private final VariableFactory elementVariableFactory;

    @Inject
    public PRRootBlockForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory,
            @Named("ElementVariableFactory") final VariableFactory elementVariableFactory) {
        super(arrayElementFactory);
        this.elementVariableFactory = elementVariableFactory;
    }

    public List<Atom> encode(final ReturnTemplate template, final String viewName) {
        builder = ImmutableList.builder();
        this.viewName = viewName;
        template.accept(this);
        builder.add(new Atom(viewName, terms));
        return builder.build();
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        super.visit(term);
    }

    @Override
    public void visit(final ReturnStringTerm term) {

    }
}