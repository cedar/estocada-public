package fr.inria.oak.estocada.compiler.model.tq;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.miscellaneous.Tuple;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Structural;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * TQ StructuralBaseListener
 * 
 * @author Rana Alotaibi
 *
 */
abstract class StructuralBaseListener extends TQLBaseListener {
    /* Used to parse path expressions */
    protected final ColumnLookUpListener columnLookUpListener;
    /* Used to create fresh kql variables */
    protected final VariableFactory tqlVariableFactory;
    /* Used to map kql variables and the internal fresh variables */
    protected final VariableMapper variableMapper;
    /*
     * Used to keep the introduced variables of the path expression being parsed
     */
    private List<Tuple<Variable, PathExpression>> definitions;
    /* Used to know the variable being introduced */
    protected Variable currentVar;
    /* Used to know the set variable being introduced */
    protected String setVarStr;
    /* Used to hold all pattern models */
    private List<String> models;

    public StructuralBaseListener(final ColumnLookUpListener columnLookUpListener,
            final VariableFactory tqlVariableFactory, final VariableMapper variableMapper) {
        this.columnLookUpListener = checkNotNull(columnLookUpListener);
        this.tqlVariableFactory = checkNotNull(tqlVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
    }

    public Structural parse(final String str) throws ParseException {
        definitions = new ArrayList<Tuple<Variable, PathExpression>>();
        currentVar = null;
        models = new ArrayList<String>();
        models.add(TQModel.ID);
        final TQLLexer lexer = new TQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TQLParser parser = new TQLParser(tokens);
        final ParserRuleContext tree = createParseTree(parser);
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return new Structural(definitions, models);
    }

    protected void defineVariable(final PathExpression pathExpression) {
        if (currentVar == null) {
            throw new IllegalStateException("Variable expected.");
        }
        definitions.add(new Tuple<Variable, PathExpression>(currentVar, pathExpression));
        currentVar = null;

    }

    protected abstract ParserRuleContext createParseTree(final TQLParser parser);
}
