package fr.inria.oak.estocada.rewriting.logical;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Decoder interface that returns the corresponding LogOperator for each sub-rewriting it decodes.
 *
 * @author ranaalotaibi
 *
 */
public interface Decoder {
    public LogOperator decode(final Catalog catalog, final ConjunctiveQuery conjunctiveQuery)
            throws TatooineExecutionException;
}
