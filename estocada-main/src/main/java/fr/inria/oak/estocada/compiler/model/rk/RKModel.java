package fr.inria.oak.estocada.compiler.model.rk;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class RKModel extends Model {
	public final static String ID = "RK";

	@Inject
	public RKModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
		super(ID, Format.KEYVALUE, Language.KQL, queryBlockTreeBuilder, blockEncoder);
	}
}
