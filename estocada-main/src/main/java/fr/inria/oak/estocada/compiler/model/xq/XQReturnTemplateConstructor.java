package fr.inria.oak.estocada.compiler.model.xq;

import java.util.Map;
import java.util.Map.Entry;

import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnLeafTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateConstructor;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.Row;
import fr.inria.oak.estocada.compiler.exceptions.ReturnConstructException;

/**
 * @author Damian Bursztyn
 */
public class XQReturnTemplateConstructor implements ReturnTemplateConstructor {
    private final XQConstructReturnTermVisitor constructVisitor;

    public XQReturnTemplateConstructor() {
        constructVisitor = new XQConstructReturnTermVisitor();
    }

    @Override
    public final String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
        return constructVisitor.construct(template, row);
    }

    private class XQConstructReturnTermVisitor implements ReturnTemplateVisitor {
        private StringBuilder builder;
        private Row row;

        public String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
            builder = new StringBuilder();
            this.row = row;
            template.accept(this);
            return builder.toString();
        }

        @Override
        public void visit(final ReturnTemplate template) {
            // NOP (no construction of the optionals)
        }

        @Override
        public void visitPre(final ReturnConstructTerm term) {
            if (term.hasChildren()) {
                builder.append("<").append(term.getElement().toString());
                visitAttributes(term.getAttributes());
                builder.append(">");
            } else {
                builder.append("<").append(term.getElement().toString());
                visitAttributes(term.getAttributes());
                builder.append("/>");
            }
        }

        private void visitAttributes(final Map<String, ReturnLeafTerm> attributes) {
            for (final Entry<String, ReturnLeafTerm> entry : attributes.entrySet()) {
                builder.append(" ").append(entry.getKey()).append("=");
                if (entry.getValue() instanceof ReturnVariableTerm) {
                    visit((ReturnVariableTerm) entry.getValue());
                } else if (entry.getValue() instanceof ReturnStringTerm) {
                    visit((ReturnStringTerm) entry.getValue());
                } else {
                    throw new IllegalStateException("Unknown return leaf term.");
                }
                builder.append(" ");
            }
        }

        @Override
        public void visitPost(final ReturnConstructTerm term) {
            if (term.hasChildren()) {
                builder.append("</").append(term.getElement().toString()).append(">");
            }
        }

        @Override
        public void visit(final ReturnVariableTerm term) {
            if (row == null) {
                builder.append(term.getVariable().toString());
            } else {
                builder.append(row.getValue(term.getVariable()).toString());
            }
        }

        @Override
        public void visit(final ReturnStringTerm term) {
            builder.append(term.toString());
        }
    }
}
