/**
 * 
 */
package fr.inria.oak.estocada.extension.BigDAWG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.jfree.util.Log;

import istc.bigdawg.LoggerSetup;
import istc.bigdawg.migration.MigrationResult;
import istc.bigdawg.postgresql.PostgreSQLHandler;

/**
 * BigDAWG Data Migration from PostgreJQ to PostgresRQ.
 * 
 * @author Rana Alotaibi
 */
public class FromPostgresJQToPostgresRQ {

	private static Logger logger = Logger.getLogger(FromPostgresJQToPostgresRQ.class);
	private static final String TABLE = "public.postgresjq_postgresrq";
	private static final String DBJQ_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
	private static final String DBRQ_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
	private static final String CREATE_TABLE;

	static {
		CREATE_TABLE = "CREATE TABLE " + TABLE + " (SUBJECT_ID INTEGER, HADM_ID INTEGER);";
	}

	public FromPostgresJQToPostgresRQ() {
		LoggerSetup.setLogging();
	}

	public MigrationResult fromPostgresJQToPostgresRQ(final String postgresJQQuery) {

		try {
			DriverManager.registerDriver(new EmbeddedDriver());
			Connection conRQ = DriverManager.getConnection(DBRQ_URL);

			DriverManager.registerDriver(new EmbeddedDriver());
			Connection conJQ = DriverManager.getConnection(DBJQ_URL);
			BigDAWGFromPostgresJQToPostgresRQMigrator fromsPostgresJQToPostgresrRQ = null;
			try {
				conRQ.setReadOnly(false);
				conJQ.setReadOnly(true);
			} catch (SQLException e1) {
				logger.error("Cannot create connection to PostgreSQL.");
				e1.printStackTrace();
				return null;
			}
			// Connect to PostgresRQ and create a table for incoming results from PostgresJQ
			try {
				PostgreSQLHandler.executeStatement(conRQ, "DROP TABLE IF EXISTS " + TABLE);
				PostgreSQLHandler.createTargetTableSchema(conRQ, TABLE, CREATE_TABLE);
			} catch (SQLException e1) {
				logger.error("Could not create table in PostgreSQL.");
				e1.printStackTrace();
				System.exit(1);
			}
			final Cancelable cancelable = new Cancelable();
			cancelable.startTimer(Cancelable.getPostgresConnectionsList(conRQ,conJQ));
			fromsPostgresJQToPostgresrRQ = new BigDAWGFromPostgresJQToPostgresRQMigrator(conJQ, conRQ);
			try {
				MigrationResult res = fromsPostgresJQToPostgresrRQ.fromPostgresJQToPostgresRQ(postgresJQQuery, TABLE);
				cancelable.cancelTimer();
				return res;
			} catch (Exception e) {
				logger.error("Could not migrate data from PostgresJQ to PostgresRQ.");
				e.printStackTrace();
			}
			logger.debug("The data was migrated from PostgresJQ to PostgresRQ.");

		} catch (SQLException e2) {
			Log.error("Could not create instance of fromPostgresJQToPostgresRQ");
			e2.printStackTrace();
		} finally {
		}
		return null;
	}
}
