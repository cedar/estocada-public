package fr.inria.oak.estocada.compiler.model.qbt;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;
import fr.inria.oak.estocada.compiler.model.aj.AJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.aj.naive.AJNaiveModule;
import fr.inria.oak.estocada.compiler.model.pj.full.PJFULLQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.pj.naive.PJNaiveModule;
import fr.inria.oak.estocada.compiler.model.pr.PRNaiveModule;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeBuilderAlternative;
import fr.inria.oak.estocada.compiler.model.pr.Relation;
import fr.inria.oak.estocada.compiler.model.rk.RKQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveModule;
import fr.inria.oak.estocada.compiler.model.sj.SJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.sj.naive.SJNaiveModule;
import fr.inria.oak.estocada.compiler.model.sppj.SPPJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveModule;

/**
 * QBT QueryBlockTreeBuilder
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
public final class QBTQueryBlockTreeBuilder implements QueryBlockTreeBuilder {
    static RKQueryBlockTreeBuilder rkBlockNestedTreeBuilder;
    static AJQueryBlockTreeBuilder ajBlockNestedTreeBuilder;
    static PRQueryBlockTreeBuilderAlternative prBlockNestedTreeBuilder;
    static PJFULLQueryBlockTreeBuilder pjBlockNestedTreeBuilder;
    static SPPJQueryBlockTreeBuilder sppjBlockNestedTreeBuilder;
    static SJQueryBlockTreeBuilder sjBlockNestedTreeBuilder;
    public static VariableMapper variableMapper;
    public static Map<String, Relation> relations;
    private final PatternListener patternListener;
    private final ReturnTemplateListener returnTemplateListener;
    private final QBTQueryBlockTreeListener queryBlockTreeListener;

    @Inject
    public QBTQueryBlockTreeBuilder(final QBTQueryBlockTreeListener queryBlockTreeListener,
            final PatternListener patternListener, final ReturnTemplateListener returnTemplateListener,
            final VariableMapper variableMapper) {

        Injector injectorRK = Guice.createInjector(new RKNaiveModule());
        Injector injectorAJ = Guice.createInjector(new AJNaiveModule());
        Injector injectorRQ = Guice.createInjector(new PRNaiveModule());
        Injector injectorPJ = Guice.createInjector(new PJNaiveModule());
        Injector injectorSPPJ = Guice.createInjector(new SPPJNaiveModule());
        Injector injectorSJ = Guice.createInjector(new SJNaiveModule());

        this.queryBlockTreeListener = queryBlockTreeListener;
        this.patternListener = patternListener;
        this.returnTemplateListener = returnTemplateListener;
        QBTQueryBlockTreeBuilder.variableMapper = variableMapper;
        QBTQueryBlockTreeBuilder.rkBlockNestedTreeBuilder = injectorRK.getInstance(RKQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder = injectorAJ.getInstance(AJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder =
                injectorRQ.getInstance(PRQueryBlockTreeBuilderAlternative.class);
        QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder = injectorPJ.getInstance(PJFULLQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sppjBlockNestedTreeBuilder = injectorSPPJ.getInstance(SPPJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder = injectorSJ.getInstance(SJQueryBlockTreeBuilder.class);

    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {
        QueryBlockTree mixedModelNestedBlockTree = queryBlockTreeListener.parse(str);
        return mixedModelNestedBlockTree;
    }

    @Override
    public Pattern buildPattern(String str) throws ParseException {

        return patternListener.parse(str);
    }

    @Override
    public PathExpression buildPathExpression(String str) throws ParseException {
        return null;
    }

    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, String str)
            throws ParseException {
        return returnTemplateListener.parse(str);
    }

    public void setRealtions(Map<String, Relation> relations) {
        PRQueryBlockTreeBuilderAlternative.relations = relations;
    }

    public static void reIntilize() {
        Injector injectorRK = Guice.createInjector(new RKNaiveModule());
        Injector injectorAJ = Guice.createInjector(new AJNaiveModule());
        Injector injectorRQ = Guice.createInjector(new PRNaiveModule());
        Injector injectorPJ = Guice.createInjector(new PJNaiveModule());
        Injector injectorSPPJ = Guice.createInjector(new SPPJNaiveModule());
        Injector injectorSJ = Guice.createInjector(new SJNaiveModule());
        QBTQueryBlockTreeBuilder.rkBlockNestedTreeBuilder = injectorRK.getInstance(RKQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder = injectorAJ.getInstance(AJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder =
                injectorRQ.getInstance(PRQueryBlockTreeBuilderAlternative.class);
        QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder = injectorPJ.getInstance(PJFULLQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sppjBlockNestedTreeBuilder = injectorSPPJ.getInstance(SPPJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder = injectorSJ.getInstance(SJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.variableMapper = new VariableMapper();
        PRQueryBlockTreeBuilderAlternative.relations = null;
    }

}
