package fr.inria.oak.estocada.compiler.model.pm;

public class PMNaiveModule extends PMModule {
    @Override
    protected String getPropertiesFileName() {
        return "pm.naive.properties";
    }
}
