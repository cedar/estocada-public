package fr.inria.oak.estocada.compiler.model.aj.alternative.secondstep;

import java.util.ArrayList;
import java.util.List;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.model.aj.Predicate;

public class Utils {
	public static ConjunctiveQuery restrict(final ConjunctiveQuery query) {
		final List<Atom> body = new ArrayList<Atom>();

		for (final Atom atom : query.getBody()) {
			if (!atom.getPredicate().startsWith(Predicate.TAG.toString())) {
				body.add(atom);
			}
		}
		return new ConjunctiveQuery(query.getName(), query.getHead(), body);
	}
}
