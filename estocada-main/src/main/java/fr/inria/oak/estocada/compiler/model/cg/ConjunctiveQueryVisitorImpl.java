package fr.inria.oak.estocada.compiler.model.cg;

import java.util.ArrayList;
import java.util.List;

public class ConjunctiveQueryVisitorImpl extends ConjunctiveQueryBaseVisitor<Object> {

    public ConjunctiveQueryVisitorImpl() {
        super();
    }

    @Override
    public List<Object> visitConjunctiveQuery(ConjunctiveQueryParser.ConjunctiveQueryContext ctx) {
        List<Object> cqAtoms = new ArrayList<Object>();
        cqAtoms.add(visit(ctx.queryHeader()));
        cqAtoms.addAll((List<Object>) visit(ctx.queryBody()));
        return cqAtoms;
    }

    @Override
    public Object visitQueryHeader(ConjunctiveQueryParser.QueryHeaderContext ctx) {
        List<String> varList = new ArrayList<String>();
        for (int i = 0; ctx.Identifier(i) != null; i++)
            varList.add(ctx.Identifier(i).getText());
        return new QueryHead(varList);
    }

    @Override
    public Object visitQueryBody(ConjunctiveQueryParser.QueryBodyContext ctx) {
        List<Object> cqBodyAtoms = new ArrayList<Object>();
        for (int i = 0; ctx.labelI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.labelI(i)));
        for (int i = 0; ctx.kindI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.kindI(i)));
        for (int i = 0; ctx.propertyI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.propertyI(i)));
        for (int i = 0; ctx.compI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.compI(i)));
        for (int i = 0; ctx.valueI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.valueI(i)));
        for (int i = 0; ctx.connectionI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.connectionI(i)));
        for (int i = 0; ctx.nestValueI(i) != null; i++)
            cqBodyAtoms.add(visit(ctx.nestValueI(i)));
        return cqBodyAtoms;
    }

    @Override
    public Object visitLabelI(ConjunctiveQueryParser.LabelIContext ctx) {
        String label = ctx.StringConstant().getText();
        return new LabelI(ctx.Identifier().getText(), label.substring(1, label.length() - 1));
    }

    @Override
    public Object visitKindI(ConjunctiveQueryParser.KindIContext ctx) {
        return new KindI(ctx.Identifier().getText(), ctx.children.get(4).getText());
    }

    @Override
    public Object visitPropertyI(ConjunctiveQueryParser.PropertyIContext ctx) {
        String pName = ctx.StringConstant().getText();
        return new PropertyI(ctx.Identifier(0).getText(), pName.substring(1, pName.length() - 1),
                ctx.Identifier(1).getText(), ctx.Identifier(2).getText());
    }

    @Override
    public Object visitCompI(ConjunctiveQueryParser.CompIContext ctx) {
        String atomName = ctx.children.get(0).getText();
        switch (atomName) {
            case "Eq_I":
                return new EqI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
            case "Ne_I":
                return new NeI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
            case "Lt_I":
                return new LtI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
            case "Gt_I":
                return new GtI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
            case "Le_I":
                return new LeI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
            case "Ge_I":
                return new GeI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
        }
        return null;
    }

    @Override
    public Object visitValueI(ConjunctiveQueryParser.ValueIContext ctx) {
        if (ctx.Identifier(1) != null)
            return new ValueI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText());
        else
            return new ValueI(ctx.Identifier(0).getText(), ctx.StringConstant().getText());
    }

    @Override
    public Object visitConnectionI(ConjunctiveQueryParser.ConnectionIContext ctx) {
        return new ConnectionI(ctx.Identifier(0).getText(), ctx.Identifier(1).getText(), ctx.Identifier(2).getText());
    }
}
