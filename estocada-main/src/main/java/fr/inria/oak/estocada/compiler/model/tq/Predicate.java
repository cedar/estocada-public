package fr.inria.oak.estocada.compiler.model.tq;

public enum Predicate {
	COLUMN("column_tq"),
	FILE("file_tq"),
	EQUALS("eq_tq");

	private final String str;

	private Predicate(final String str) {
		this.str = str;
	}

	@Override
	public String toString() {
		return str;
	}
}
