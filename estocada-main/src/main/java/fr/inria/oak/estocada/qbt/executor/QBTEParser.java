// Generated from QBTE.g4 by ANTLR 4.7.2

package fr.inria.oak.estocada.qbt.executor;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class QBTEParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, NAME=19, WHITESPACE=20, STRING=21, RETURN=22, AJ=23, RK=24, 
		PR=25, SPPJ=26, PJ=27, SJ=28, XQ=29, TQ=30, AND=31, FROM=32, IN=33, SELECT=34, 
		VALUE=35, AS=36, LIKE=37, QUERY=38, USE=39;
	public static final int
		RULE_queryBlock = 0, RULE_forPattern = 1, RULE_blockPattern = 2, RULE_pattern = 3, 
		RULE_annotation = 4, RULE_sjPattern = 5, RULE_condition = 6, RULE_conditionAtom = 7, 
		RULE_sjQuery = 8, RULE_sjCollectionName = 9, RULE_sjTextSearch = 10, RULE_sjProjectFields = 11, 
		RULE_sjFieldName = 12, RULE_sjConstant = 13, RULE_variable = 14, RULE_wherePattern = 15, 
		RULE_returnPattern = 16;
	private static String[] makeRuleNames() {
		return new String[] {
			"queryBlock", "forPattern", "blockPattern", "pattern", "annotation", 
			"sjPattern", "condition", "conditionAtom", "sjQuery", "sjCollectionName", 
			"sjTextSearch", "sjProjectFields", "sjFieldName", "sjConstant", "variable", 
			"wherePattern", "returnPattern"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'FOR'", "','", "':'", "'{'", "'}'", "'PR'", "'PJ'", "'SJ'", "'('", 
			"')'", "'AND'", "'='", "'/'", "'query?q='", "'&'", "'fl='", "'WHERE'", 
			"'RETURN'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, "NAME", "WHITESPACE", "STRING", 
			"RETURN", "AJ", "RK", "PR", "SPPJ", "PJ", "SJ", "XQ", "TQ", "AND", "FROM", 
			"IN", "SELECT", "VALUE", "AS", "LIKE", "QUERY", "USE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "QBTE.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public QBTEParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class QueryBlockContext extends ParserRuleContext {
		public ForPatternContext forPattern() {
			return getRuleContext(ForPatternContext.class,0);
		}
		public WherePatternContext wherePattern() {
			return getRuleContext(WherePatternContext.class,0);
		}
		public ReturnPatternContext returnPattern() {
			return getRuleContext(ReturnPatternContext.class,0);
		}
		public QueryBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterQueryBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitQueryBlock(this);
		}
	}

	public final QueryBlockContext queryBlock() throws RecognitionException {
		QueryBlockContext _localctx = new QueryBlockContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_queryBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			forPattern();
			setState(35);
			wherePattern();
			setState(36);
			returnPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForPatternContext extends ParserRuleContext {
		public List<BlockPatternContext> blockPattern() {
			return getRuleContexts(BlockPatternContext.class);
		}
		public BlockPatternContext blockPattern(int i) {
			return getRuleContext(BlockPatternContext.class,i);
		}
		public ForPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterForPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitForPattern(this);
		}
	}

	public final ForPatternContext forPattern() throws RecognitionException {
		ForPatternContext _localctx = new ForPatternContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_forPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			match(T__0);
			setState(39);
			blockPattern();
			setState(44);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(40);
				match(T__1);
				setState(41);
				blockPattern();
				}
				}
				setState(46);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockPatternContext extends ParserRuleContext {
		public AnnotationContext annotation() {
			return getRuleContext(AnnotationContext.class,0);
		}
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public BlockPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterBlockPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitBlockPattern(this);
		}
	}

	public final BlockPatternContext blockPattern() throws RecognitionException {
		BlockPatternContext _localctx = new BlockPatternContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_blockPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(47);
			annotation();
			setState(48);
			match(T__2);
			setState(49);
			match(T__3);
			setState(50);
			pattern();
			setState(51);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PatternContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(QBTEParser.STRING, 0); }
		public SjPatternContext sjPattern() {
			return getRuleContext(SjPatternContext.class,0);
		}
		public PatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitPattern(this);
		}
	}

	public final PatternContext pattern() throws RecognitionException {
		PatternContext _localctx = new PatternContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_pattern);
		try {
			setState(55);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case STRING:
				enterOuterAlt(_localctx, 1);
				{
				setState(53);
				match(STRING);
				}
				break;
			case NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(54);
				sjPattern();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationContext extends ParserRuleContext {
		public AnnotationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterAnnotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitAnnotation(this);
		}
	}

	public final AnnotationContext annotation() throws RecognitionException {
		AnnotationContext _localctx = new AnnotationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_annotation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__6) | (1L << T__7))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjPatternContext extends ParserRuleContext {
		public SjQueryContext sjQuery() {
			return getRuleContext(SjQueryContext.class,0);
		}
		public SjPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjPattern(this);
		}
	}

	public final SjPatternContext sjPattern() throws RecognitionException {
		SjPatternContext _localctx = new SjPatternContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_sjPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			sjQuery();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public ConditionAtomContext conditionAtom() {
			return getRuleContext(ConditionAtomContext.class,0);
		}
		public List<ConditionContext> condition() {
			return getRuleContexts(ConditionContext.class);
		}
		public ConditionContext condition(int i) {
			return getRuleContext(ConditionContext.class,i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		return condition(0);
	}

	private ConditionContext condition(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ConditionContext _localctx = new ConditionContext(_ctx, _parentState);
		ConditionContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_condition, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NAME:
				{
				setState(62);
				conditionAtom();
				}
				break;
			case T__8:
				{
				setState(63);
				match(T__8);
				setState(64);
				condition(0);
				setState(65);
				match(T__9);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(74);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new ConditionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_condition);
					setState(69);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(70);
					match(T__10);
					setState(71);
					condition(2);
					}
					} 
				}
				setState(76);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConditionAtomContext extends ParserRuleContext {
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public ConditionAtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionAtom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterConditionAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitConditionAtom(this);
		}
	}

	public final ConditionAtomContext conditionAtom() throws RecognitionException {
		ConditionAtomContext _localctx = new ConditionAtomContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_conditionAtom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			variable();
			setState(78);
			match(T__11);
			setState(79);
			variable();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjQueryContext extends ParserRuleContext {
		public SjCollectionNameContext sjCollectionName() {
			return getRuleContext(SjCollectionNameContext.class,0);
		}
		public SjProjectFieldsContext sjProjectFields() {
			return getRuleContext(SjProjectFieldsContext.class,0);
		}
		public SjTextSearchContext sjTextSearch() {
			return getRuleContext(SjTextSearchContext.class,0);
		}
		public SjQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjQuery(this);
		}
	}

	public final SjQueryContext sjQuery() throws RecognitionException {
		SjQueryContext _localctx = new SjQueryContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_sjQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			sjCollectionName();
			setState(82);
			match(T__12);
			setState(83);
			match(T__13);
			setState(85);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STRING) {
				{
				setState(84);
				sjTextSearch();
				}
			}

			setState(87);
			match(T__14);
			setState(88);
			sjProjectFields();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjCollectionNameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(QBTEParser.NAME, 0); }
		public SjCollectionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjCollectionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjCollectionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjCollectionName(this);
		}
	}

	public final SjCollectionNameContext sjCollectionName() throws RecognitionException {
		SjCollectionNameContext _localctx = new SjCollectionNameContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_sjCollectionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(90);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjTextSearchContext extends ParserRuleContext {
		public SjConstantContext sjConstant() {
			return getRuleContext(SjConstantContext.class,0);
		}
		public SjTextSearchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjTextSearch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjTextSearch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjTextSearch(this);
		}
	}

	public final SjTextSearchContext sjTextSearch() throws RecognitionException {
		SjTextSearchContext _localctx = new SjTextSearchContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_sjTextSearch);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(92);
			sjConstant();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjProjectFieldsContext extends ParserRuleContext {
		public List<SjFieldNameContext> sjFieldName() {
			return getRuleContexts(SjFieldNameContext.class);
		}
		public SjFieldNameContext sjFieldName(int i) {
			return getRuleContext(SjFieldNameContext.class,i);
		}
		public SjProjectFieldsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjProjectFields; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjProjectFields(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjProjectFields(this);
		}
	}

	public final SjProjectFieldsContext sjProjectFields() throws RecognitionException {
		SjProjectFieldsContext _localctx = new SjProjectFieldsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_sjProjectFields);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(T__15);
			setState(95);
			sjFieldName();
			setState(100);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(96);
				match(T__1);
				setState(97);
				sjFieldName();
				}
				}
				setState(102);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjFieldNameContext extends ParserRuleContext {
		public List<TerminalNode> NAME() { return getTokens(QBTEParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(QBTEParser.NAME, i);
		}
		public SjFieldNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjFieldName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjFieldName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjFieldName(this);
		}
	}

	public final SjFieldNameContext sjFieldName() throws RecognitionException {
		SjFieldNameContext _localctx = new SjFieldNameContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_sjFieldName);
		try {
			setState(107);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(103);
				match(NAME);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(104);
				match(NAME);
				setState(105);
				match(T__2);
				setState(106);
				match(NAME);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SjConstantContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(QBTEParser.STRING, 0); }
		public SjConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sjConstant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterSjConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitSjConstant(this);
		}
	}

	public final SjConstantContext sjConstant() throws RecognitionException {
		SjConstantContext _localctx = new SjConstantContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_sjConstant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(QBTEParser.NAME, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(111);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WherePatternContext extends ParserRuleContext {
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public WherePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wherePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterWherePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitWherePattern(this);
		}
	}

	public final WherePatternContext wherePattern() throws RecognitionException {
		WherePatternContext _localctx = new WherePatternContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_wherePattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			match(T__16);
			setState(114);
			condition(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnPatternContext extends ParserRuleContext {
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public ReturnPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).enterReturnPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof QBTEListener ) ((QBTEListener)listener).exitReturnPattern(this);
		}
	}

	public final ReturnPatternContext returnPattern() throws RecognitionException {
		ReturnPatternContext _localctx = new ReturnPatternContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_returnPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(116);
			match(T__17);
			setState(117);
			variable();
			setState(122);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(118);
				match(T__1);
				setState(119);
				variable();
				}
				}
				setState(124);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 6:
			return condition_sempred((ConditionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean condition_sempred(ConditionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3)\u0080\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\7\3-\n\3\f\3\16\3\60\13\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\5\3\5\5\5:\n\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\5\bF\n\b\3\b\3\b\3\b\7\bK\n\b\f\b\16\bN\13\b\3\t\3\t\3\t\3\t\3\n\3\n"+
		"\3\n\3\n\5\nX\n\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\r\7\re\n"+
		"\r\f\r\16\rh\13\r\3\16\3\16\3\16\3\16\5\16n\n\16\3\17\3\17\3\20\3\20\3"+
		"\21\3\21\3\21\3\22\3\22\3\22\3\22\7\22{\n\22\f\22\16\22~\13\22\3\22\2"+
		"\3\16\23\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"\2\3\3\2\b\n\2v\2$\3"+
		"\2\2\2\4(\3\2\2\2\6\61\3\2\2\2\b9\3\2\2\2\n;\3\2\2\2\f=\3\2\2\2\16E\3"+
		"\2\2\2\20O\3\2\2\2\22S\3\2\2\2\24\\\3\2\2\2\26^\3\2\2\2\30`\3\2\2\2\32"+
		"m\3\2\2\2\34o\3\2\2\2\36q\3\2\2\2 s\3\2\2\2\"v\3\2\2\2$%\5\4\3\2%&\5 "+
		"\21\2&\'\5\"\22\2\'\3\3\2\2\2()\7\3\2\2).\5\6\4\2*+\7\4\2\2+-\5\6\4\2"+
		",*\3\2\2\2-\60\3\2\2\2.,\3\2\2\2./\3\2\2\2/\5\3\2\2\2\60.\3\2\2\2\61\62"+
		"\5\n\6\2\62\63\7\5\2\2\63\64\7\6\2\2\64\65\5\b\5\2\65\66\7\7\2\2\66\7"+
		"\3\2\2\2\67:\7\27\2\28:\5\f\7\29\67\3\2\2\298\3\2\2\2:\t\3\2\2\2;<\t\2"+
		"\2\2<\13\3\2\2\2=>\5\22\n\2>\r\3\2\2\2?@\b\b\1\2@F\5\20\t\2AB\7\13\2\2"+
		"BC\5\16\b\2CD\7\f\2\2DF\3\2\2\2E?\3\2\2\2EA\3\2\2\2FL\3\2\2\2GH\f\3\2"+
		"\2HI\7\r\2\2IK\5\16\b\4JG\3\2\2\2KN\3\2\2\2LJ\3\2\2\2LM\3\2\2\2M\17\3"+
		"\2\2\2NL\3\2\2\2OP\5\36\20\2PQ\7\16\2\2QR\5\36\20\2R\21\3\2\2\2ST\5\24"+
		"\13\2TU\7\17\2\2UW\7\20\2\2VX\5\26\f\2WV\3\2\2\2WX\3\2\2\2XY\3\2\2\2Y"+
		"Z\7\21\2\2Z[\5\30\r\2[\23\3\2\2\2\\]\7\25\2\2]\25\3\2\2\2^_\5\34\17\2"+
		"_\27\3\2\2\2`a\7\22\2\2af\5\32\16\2bc\7\4\2\2ce\5\32\16\2db\3\2\2\2eh"+
		"\3\2\2\2fd\3\2\2\2fg\3\2\2\2g\31\3\2\2\2hf\3\2\2\2in\7\25\2\2jk\7\25\2"+
		"\2kl\7\5\2\2ln\7\25\2\2mi\3\2\2\2mj\3\2\2\2n\33\3\2\2\2op\7\27\2\2p\35"+
		"\3\2\2\2qr\7\25\2\2r\37\3\2\2\2st\7\23\2\2tu\5\16\b\2u!\3\2\2\2vw\7\24"+
		"\2\2w|\5\36\20\2xy\7\4\2\2y{\5\36\20\2zx\3\2\2\2{~\3\2\2\2|z\3\2\2\2|"+
		"}\3\2\2\2}#\3\2\2\2~|\3\2\2\2\n.9ELWfm|";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}