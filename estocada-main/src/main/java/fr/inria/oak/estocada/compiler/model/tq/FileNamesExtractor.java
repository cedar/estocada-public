package fr.inria.oak.estocada.compiler.model.tq;

import java.util.HashSet;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.estocada.compiler.Block;
import fr.inria.oak.estocada.compiler.ChildBlock;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeVisitor;
import fr.inria.oak.estocada.compiler.RootBlock;

@Singleton
public class FileNamesExtractor implements QueryBlockTreeVisitor {
	private final String	fileNamePrefix;
	private Set<String>		mapNames;

	@Inject
	public FileNamesExtractor(@Named("document_name_prefix") final String documentNamePrefix) {
		this.fileNamePrefix = documentNamePrefix;
	}

	public Set<String> getDocumentNames(final QueryBlockTree nbt) {
		mapNames = new HashSet<String>();
		nbt.accept(this);
		return mapNames;
	}

	@Override
	public void visit(final QueryBlockTree tree) {
		// NOP
	}

	@Override
	public void visit(final RootBlock block) {
		_visit(block);
	}

	@Override
	public void visit(final ChildBlock block) {
		_visit(block);
	}

	private void _visit(final Block block) {
		block.getPattern().encoding(Utils.conditionEncoding).stream()
		        .filter(a -> a.getPredicate().startsWith(Predicate.FILE.toString()))
		        .map(a -> a.getPredicate().substring(a.getPredicate().lastIndexOf(fileNamePrefix) + 1))
		        .forEach(mapNames::add);

	}

}
