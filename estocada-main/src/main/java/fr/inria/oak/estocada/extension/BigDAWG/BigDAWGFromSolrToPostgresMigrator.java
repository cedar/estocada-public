package fr.inria.oak.estocada.extension.BigDAWG;

import static java.lang.Math.toIntExact;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.accumulo.core.data.Range;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.QueryRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import istc.bigdawg.exceptions.MigrationException;
import istc.bigdawg.migration.FromDatabaseToDatabase;
import istc.bigdawg.migration.MigrationResult;
import istc.bigdawg.utils.ListConncatenator;

/**
 * Solr to Postgres Migrator
 * 
 * @author Rana Alotaibi
 */
public class BigDAWGFromSolrToPostgresMigrator extends FromDatabaseToDatabase {

    private static final long serialVersionUID = -3329851490314835580L;

    private static Logger logger = Logger.getLogger(BigDAWGFromSolrToPostgresMigrator.class);
    private static HttpSolrClient solrClient = null;
    private static final int DEFAULT_NUM_OF_DOCUMENTS = 30000;
    private int cursorId = 0;
    private int resultsPerQuery;
    private int numResultsFound;
    private SolrConnectionInfo solrConnectionInfo = null;
    private Connection connection = null;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    // parameters
    private int postgreSQLReaderCharSize = 1000000;
    private char delimiter = '|';

    public BigDAWGFromSolrToPostgresMigrator() {

    }

    public BigDAWGFromSolrToPostgresMigrator(SolrConnectionInfo solrConnectionInfo, Connection connection) {
        this.solrConnectionInfo = solrConnectionInfo;
        this.connection = connection;
    }

    private void cleanPostgreSQLResources() throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (st != null) {
            st.close();
        }
        if (connection != null) {
            connection.close();
        }
    }

    private void connectSolr() throws SolrException {
        try {
            solrClient = new HttpSolrClient(
                    String.format("%s:%s/solr", solrConnectionInfo.getUrl(), solrConnectionInfo.getPort()));
        } catch (Exception e) {
            String msg = String.format("Failure trying to create an HttpSolrClient with URL %s and port %d: %s",
                    solrConnectionInfo.getUrl(), solrConnectionInfo.getPort(), e.getMessage());
            logger.error(msg);
            logger.error(e);
        }
        logger.debug("Connected to Solr!");
    }

    private ResultSetMetaData getMetaData(final String tableName) throws SQLException {
        String query = "Select * from " + tableName.replace(";", "").replace(" ", "") + " limit 1";
        logger.debug("Query to get meta data: " + query);
        st = connection.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        rs = st.executeQuery();
        if (rs == null) {
            return null;
        }
        return rs.getMetaData();
    }

    public void createNewRowForPostgres(String[] row, StringBuilder sBuilder) {
        String rowString = ListConncatenator.joinList(row, delimiter, "\n");
        sBuilder.append(rowString);
    }

    public void flushRowsToPostgreSQL(StringBuilder sBuilder, PushbackReader reader, CopyManager cpManager,
            String postgresTable, String copyString) throws IOException, SQLException {
        reader.unread(sBuilder.toString().toCharArray());
        cpManager.copyIn(copyString.toString(), reader);
        sBuilder.delete(0, sBuilder.length());
    }

    public MigrationResult fromSolrToPostgres(final SolrQuery solrQuery, final String postgresTable,
            Range accumuloRange) throws SQLException, MigrationException {
        logger.debug("Migrate data from Solr to Postgres.");
        /* Solr Connect */
        connectSolr();
        long startTimeMigration = System.currentTimeMillis();
        long solrCounter = 0;
        long postgresCounter = 0;
        StringBuilder copyStringBuf = new StringBuilder();
        copyStringBuf.append("COPY ");
        copyStringBuf.append(postgresTable);
        copyStringBuf.append(" FROM STDIN WITH (DELIMITER '");
        copyStringBuf.append(delimiter);
        copyStringBuf.append("')");
        String copyString = copyStringBuf.toString();
        try {
            ResultSetMetaData rsmd = getMetaData(postgresTable);
            if (rsmd == null) {
                String message = "There is no table: " + postgresTable;
                logger.log(Level.INFO, message);
            } else {
                int numOfCol = rsmd.getColumnCount();
                Map<String, Integer> mapNameCol = new HashMap<>();
                for (int i = 0; i < numOfCol; ++i) {
                    String columnName = rsmd.getColumnName(i + 1);
                    logger.debug("Column name: " + columnName);
                    mapNameCol.put(columnName, i);
                }
                StringBuilder sBuilder = new StringBuilder();
                CopyManager cpManager = new CopyManager((BaseConnection) this.connection);
                PushbackReader reader = new PushbackReader(new StringReader(""), postgreSQLReaderCharSize);

                cursorId = 0;
                resultsPerQuery = numResultsFound = DEFAULT_NUM_OF_DOCUMENTS;
                try {
                    logger.info(String.format("Querying SOLR collection \"%s\" with query \"%s\"",
                            solrConnectionInfo.getDatabase(), solrQuery.toString()));

                    QueryResponse response = getBatchRecords(solrQuery);
                    while (response != null) {
                        SolrDocumentList results = response.getResults();
                        for (SolrDocument document : results) {
                            Collection<String> fieldNames = document.getFieldNames();
                            String[] values = new String[fieldNames.size()];
                            for (String fieldName : fieldNames) {
                                Integer index = mapNameCol.get(fieldName.toString().toLowerCase());
                                values[index] = Integer.toString((Integer) document.getFieldValue(fieldName));
                            }
                            ++postgresCounter;
                            ++solrCounter;
                            createNewRowForPostgres(values, sBuilder);
                        }

                        flushRowsToPostgreSQL(sBuilder, reader, cpManager, postgresTable, copyString);
                        response = getBatchRecords(solrQuery);
                    }

                } catch (IOException e) {
                    String msg =
                            String.format("Error retrieving results from Solr's '%s' collection with query '%s': %s",
                                    solrConnectionInfo.getDatabase(), solrQuery.toString(), e.getMessage());
                    logger.error(msg);
                }

            }
        } finally {
            cleanPostgreSQLResources();
        }
        long endTimeMigration = System.currentTimeMillis();
        long durationMsec = endTimeMigration - startTimeMigration;
        return new MigrationResult(solrCounter, postgresCounter, startTimeMigration, endTimeMigration, durationMsec);

    }

    private QueryResponse getBatchRecords(final SolrQuery solrQuery) {
        QueryResponse response = null;
        if (cursorId >= numResultsFound) {
            return response;
        }
        solrQuery.setStart(cursorId);
        solrQuery.setRows(resultsPerQuery);
        QueryRequest request = new QueryRequest(solrQuery);
        request.setBasicAuthCredentials("", "");
        try {
            response = request.process(solrClient, solrConnectionInfo.getDatabase());
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        cursorId += response.getResults().size();
        numResultsFound = toIntExact(response.getResults().getNumFound());
        return response;
    }

    public MigrationResult fromSolrToPostgres(final SolrQuery solrQuery, final String postgresTable)
            throws MigrationException, SQLException {
        return fromSolrToPostgres(solrQuery, postgresTable, null);
    }
}
