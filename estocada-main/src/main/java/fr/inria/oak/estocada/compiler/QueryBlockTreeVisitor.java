package fr.inria.oak.estocada.compiler;

/**
 * Visitor pattern.
 *
 * Provides a way to navigate (similarly to a functional fold) the elements of a
 * nested block tree.
 *
 * @author Rana Alotaibi
 */
public interface QueryBlockTreeVisitor {
    void visit(final QueryBlockTree tree);

    void visit(final RootBlock block);

    void visit(final ChildBlock block);
}
