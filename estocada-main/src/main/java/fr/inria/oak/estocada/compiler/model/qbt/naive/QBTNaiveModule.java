package fr.inria.oak.estocada.compiler.model.qbt.naive;

import fr.inria.oak.estocada.compiler.model.qbt.QBTModule;

public class QBTNaiveModule extends QBTModule {
    @Override
    protected String getPropertiesFileName() {
        return "qbt.properties";
    }
}
