package fr.inria.oak.estocada.rewriting.logical;

import java.util.Map;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Generates a cross-model/single model logical plan for a given rewriting.
 * 
 * @author ranaalotaibi
 */
public class RewritingToQBT {

    private static final Logger LOGGER = Logger.getLogger(RewritingToQBT.class);
    private final ConjunctiveQuery rewriitng;
    private Catalog catalog;

    /** Constructor **/
    public RewritingToQBT(final ConjunctiveQuery rewriitng, final Catalog catalog) {
        this.rewriitng = rewriitng;
        this.catalog = catalog;
    }

    /**
     * Generates a logical plan for a given cross-model/single model rewriting.
     * 
     * @return This function returns the root operator of the logical plan.
     */
    public LogOperator generate() {

        final RewritingSpliter rewritingSpliter = new RewritingSpliter(rewriitng, catalog);
        final Map<StorageReference, ConjunctiveQuery> subRewritings = rewritingSpliter.split();
        final LogicalPlanBuilder logicalPlanBuilder =
                new LogicalPlanBuilder(rewriitng.getHead(), subRewritings, catalog);
        return logicalPlanBuilder.build();

    }
}
