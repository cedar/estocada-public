package fr.inria.oak.estocada.views.materialization.module.SJ;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.common.cloud.SolrZkClient;
import org.apache.zookeeper.KeeperException;

import com.google.gson.JsonObject;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyEvalOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.oak.estocada.views.materialization.module.Materializer;
import fr.inria.oak.estocada.views.materialization.module.Utils;

/**
 * SJ Ingestor. Ingest the data into Solr
 * 
 * @author ranaalotaibi
 *
 */
public final class SJIngestor implements Materializer {
    private static final Logger LOGGER = Logger.getLogger(SJIngestor.class);
    private static final String JSON_PATH = "src/main/resources/";
    private final String viewName;
    private final Map<String, String> sjBuilder;
    private final PhyEvalOperator phyEval;
    private final StorageReference gsr;
    private String solrURL;

    private static final String ZOOKEEPER_URL = "localhost:9983";
    private static final int ZK_TIMEOUT = 30000;
    private static final int ZK_CONNECTION_TIMEOUT = 45000;
    private static final String CONFIGS_ZKNODE = "/configs";

    public SJIngestor(final String viewName, final Map<String, String> sjBuilder, final PhyEvalOperator phyEval,
            final StorageReference gsr) {
        this.sjBuilder = sjBuilder;
        this.phyEval = phyEval;
        this.viewName = viewName;
        this.gsr = gsr;

        solrConnection();
    }

    @Override
    public void materialize() {
        try {
            ingest();
        } catch (Exception e) {
            LOGGER.error("View materilization failed:" + e.getMessage());
        }
    }

    /**
     * Iterate over PhyEval result and issue INSERT commands.
     * 
     * @return
     */
    private boolean ingest() {
        try {

            final List<JsonObject> jsonObjects = new ArrayList<JsonObject>();

            if (phyEval instanceof PhySQLEval) {
                PhySQLEval phySQLEval = (PhySQLEval) phyEval;
                //TODO: Check
                final NRSMD nrsdm = phySQLEval.nrsmd;
                phySQLEval.open();
                while (phySQLEval.hasNext()) {
                    final NTuple next = phySQLEval.next();
                    jsonObjects.add(buildJSONDocument(nrsdm, next));
                }
                phySQLEval.close();
                postToSolr(jsonObjects);
            }
        } catch (TatooineExecutionException e) {
            LOGGER.error("Cannot ingest data to Solr" + e.getMessage());
            return false;
        }
        return true;

    }

    /**
     * Build JOSN Objects from NTuple to be ingested into Solr.
     * 
     * @return
     */
    private JsonObject buildJSONDocument(final NRSMD nrsmd, final NTuple next) {
        try {
            final TupleMetadataType[] tupleMetadataType = nrsmd.getColumnsMetadata();
            final JsonObject jsonObject = new JsonObject();
            for (Map.Entry<String, String> entry : sjBuilder.entrySet()) {
                int colInx = nrsmd.getColIndexFromName(entry.getValue());
                if (tupleMetadataType[colInx].equals(TupleMetadataType.INTEGER_TYPE)) {
                    jsonObject.addProperty(entry.getKey(), next.getIntegerField(colInx));
                }
                if (tupleMetadataType[colInx].equals(TupleMetadataType.STRING_TYPE)) {
                    jsonObject.addProperty(entry.getKey(), String.copyValueOf(next.getStringField(colInx)));
                }

            }
            return jsonObject;
        } catch (TatooineExecutionException e) {
            LOGGER.error("Cannot build json object" + e.getMessage());
        }
        return null;

    }

    /**
     * Post JSON Objects to Solr
     */
    private void postToSolr(final List<JsonObject> jsonObjects) {
        try {
            /* Data needs to be loaded from file in case of Solr*/
            final String fileName = "TempSolr";
            Utils.writeToFile(JSON_PATH + fileName, jsonObjects);
            /* Create collection*/
            uploadConfigToZk(Parameters.getProperty("solrConfigSets"), viewName);
            final SolrClient solrClient = new HttpSolrClient(solrURL);
            final CollectionAdminRequest.Create createCollectionRequest = new CollectionAdminRequest.Create();
            createCollectionRequest.setCollectionName(viewName);
            createCollectionRequest.setConfigName(viewName);
            createCollectionRequest.setNumShards(1);
            createCollectionRequest.setReplicationFactor(1);
            CollectionAdminResponse response = createCollectionRequest.process(solrClient);
            solrClient.close();
            if (!response.isSuccess()) {
                LOGGER.error(String.format("Failed to create collection: %s", response.getErrorMessages().toString()));
            }
            /* Post the data to Solr */
            Process process = Runtime.getRuntime().exec(String.format("%s/bin/post -c %s %s",
                    Parameters.getProperty("solrHome"), viewName, JSON_PATH + fileName));
            process.waitFor();
        } catch (IOException | SolrServerException | InterruptedException | KeeperException e) {
            LOGGER.error("Cann't post json objects to Solr" + e.getMessage());
        }
    }

    private void uploadConfigToZk(String configDir, String configName)
            throws InterruptedException, IOException, KeeperException {
        SolrZkClient zkClient = null;
        try {
            zkClient = new SolrZkClient(ZOOKEEPER_URL, ZK_TIMEOUT, ZK_CONNECTION_TIMEOUT, null);

            File[] files = new File(configDir).listFiles();
            for (File file : files) {
                uploadConfigFileToZk(zkClient, configName, file.getName(), new File(configDir, file.getName()));
            }
        } finally {
            if (zkClient != null) {
                zkClient.close();
            }
        }
    }

    private void uploadConfigFileToZk(SolrZkClient zkClient, String configName, String nameInZk, File file)
            throws InterruptedException, IOException, KeeperException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File child : files) {
                zkClient.makePath(CONFIGS_ZKNODE + "/" + configName + "/" + nameInZk + "/" + child.getName(), child,
                        false, true);
            }
        } else {
            zkClient.makePath(CONFIGS_ZKNODE + "/" + configName + "/" + nameInZk, file, false, true);
        }
    }

    /**
     * Create Redis Connection
     */
    private void solrConnection() {
        solrURL = String.format("%s:%s/solr/", gsr.getPropertyValue("serverUrl"), gsr.getPropertyValue("serverPort"));
    }

}
