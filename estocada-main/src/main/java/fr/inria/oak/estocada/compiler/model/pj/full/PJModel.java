package fr.inria.oak.estocada.compiler.model.pj.full;

import com.google.inject.Inject;

import fr.inria.oak.estocada.compiler.BlockEncoder;
import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;

public class PJModel extends Model {
    public final static String ID = "PJ";

    @Inject
    public PJModel(final QueryBlockTreeBuilder queryBlockTreeBuilder, final BlockEncoder blockEncoder) {
        super(ID, Format.JSON, Language.PJSQL, queryBlockTreeBuilder, blockEncoder);
    }
}
