package fr.inria.oak.estocada.compiler.model.tq;

public class TQLNaiveModule extends TQLModule {
	@Override
	protected String getPropertiesFileName() {
		return "tq.properties";
	}
}
