package fr.inria.oak.estocada.views.materialization.module.SJ;

public enum SJDataType {

    INTEGER("integer"),
    VARCHAR("string");

    private final String dataType;

    private SJDataType(final String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return dataType;
    }
}
