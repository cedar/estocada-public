package fr.inria.oak.estocada.compiler.model.sj;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * SJ QueryBlockTreeBuilder which implements {@link QueryBlockTreeBuilder}.
 * 
 * @author Rana Alotaibi
 */
@Singleton
public final class SJQueryBlockTreeBuilder implements QueryBlockTreeBuilder {
    private final PathExpressionListener pathExpressionListener;
    private final ReturnTemplateListener returnTemplateListener;
    private final QueryBlockTreeListener queryBlockTreeListener;
    private final PatternListener patternListener;

    @Inject
    public SJQueryBlockTreeBuilder(final PathExpressionListener pathExpressionListener,
            final ReturnTemplateListener returnTemplateListener, final QueryBlockTreeListener queryBlockTreeListener,
            final PatternListener patternListener) {
        this.pathExpressionListener = pathExpressionListener;
        this.returnTemplateListener = returnTemplateListener;
        this.queryBlockTreeListener = queryBlockTreeListener;
        this.patternListener = patternListener;
    }

    @Override
    public PathExpression buildPathExpression(final String str) throws ParseException {
        return pathExpressionListener.parse(str);
    }

    /**
     * The definitions are not used.
     */
    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, final String str)
            throws ParseException {
        return returnTemplateListener.parseRecordConstruct(str);
    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {
        return queryBlockTreeListener.parse(str);
    }

    @Override
    public Pattern buildPattern(String str) throws ParseException {

        return patternListener.parse(str);
    }

    public VariableMapper getVariableMapper() {

        return patternListener.getVariableMapper();
    }
}
