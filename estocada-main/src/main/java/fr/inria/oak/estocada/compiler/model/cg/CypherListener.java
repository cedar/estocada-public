// Generated from Cypher.g4 by ANTLR 4.7.2

    package  fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CypherParser}.
 */
public interface CypherListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Cypher}.
	 * @param ctx the parse tree
	 */
	void enterOC_Cypher(CypherParser.OC_CypherContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Cypher}.
	 * @param ctx the parse tree
	 */
	void exitOC_Cypher(CypherParser.OC_CypherContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Statement}.
	 * @param ctx the parse tree
	 */
	void enterOC_Statement(CypherParser.OC_StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Statement}.
	 * @param ctx the parse tree
	 */
	void exitOC_Statement(CypherParser.OC_StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Query}.
	 * @param ctx the parse tree
	 */
	void enterOC_Query(CypherParser.OC_QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Query}.
	 * @param ctx the parse tree
	 */
	void exitOC_Query(CypherParser.OC_QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_RegularQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_RegularQuery(CypherParser.OC_RegularQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_RegularQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_RegularQuery(CypherParser.OC_RegularQueryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_SingleQuerySinglePart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_SingleQuerySinglePart(CypherParser.OC_SingleQuerySinglePartContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_SingleQuerySinglePart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_SingleQuerySinglePart(CypherParser.OC_SingleQuerySinglePartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_SingleQueryMultiPart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_SingleQueryMultiPart(CypherParser.OC_SingleQueryMultiPartContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_SingleQueryMultiPart}
	 * labeled alternative in {@link CypherParser#oC_SingleQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_SingleQueryMultiPart(CypherParser.OC_SingleQueryMultiPartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_SinglePartQueryNoUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_SinglePartQueryNoUpdating(CypherParser.OC_SinglePartQueryNoUpdatingContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_SinglePartQueryNoUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_SinglePartQueryNoUpdating(CypherParser.OC_SinglePartQueryNoUpdatingContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_SinglePartQueryUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_SinglePartQueryUpdating(CypherParser.OC_SinglePartQueryUpdatingContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_SinglePartQueryUpdating}
	 * labeled alternative in {@link CypherParser#oC_SinglePartQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_SinglePartQueryUpdating(CypherParser.OC_SinglePartQueryUpdatingContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_MultiPartQuery}.
	 * @param ctx the parse tree
	 */
	void enterOC_MultiPartQuery(CypherParser.OC_MultiPartQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_MultiPartQuery}.
	 * @param ctx the parse tree
	 */
	void exitOC_MultiPartQuery(CypherParser.OC_MultiPartQueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_UpdatingClause}.
	 * @param ctx the parse tree
	 */
	void enterOC_UpdatingClause(CypherParser.OC_UpdatingClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_UpdatingClause}.
	 * @param ctx the parse tree
	 */
	void exitOC_UpdatingClause(CypherParser.OC_UpdatingClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ReadingClause}.
	 * @param ctx the parse tree
	 */
	void enterOC_ReadingClause(CypherParser.OC_ReadingClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ReadingClause}.
	 * @param ctx the parse tree
	 */
	void exitOC_ReadingClause(CypherParser.OC_ReadingClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Match}.
	 * @param ctx the parse tree
	 */
	void enterOC_Match(CypherParser.OC_MatchContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Match}.
	 * @param ctx the parse tree
	 */
	void exitOC_Match(CypherParser.OC_MatchContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Create}.
	 * @param ctx the parse tree
	 */
	void enterOC_Create(CypherParser.OC_CreateContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Create}.
	 * @param ctx the parse tree
	 */
	void exitOC_Create(CypherParser.OC_CreateContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Return}.
	 * @param ctx the parse tree
	 */
	void enterOC_Return(CypherParser.OC_ReturnContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Return}.
	 * @param ctx the parse tree
	 */
	void exitOC_Return(CypherParser.OC_ReturnContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_With}.
	 * @param ctx the parse tree
	 */
	void enterOC_With(CypherParser.OC_WithContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_With}.
	 * @param ctx the parse tree
	 */
	void exitOC_With(CypherParser.OC_WithContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ProjectionBody}.
	 * @param ctx the parse tree
	 */
	void enterOC_ProjectionBody(CypherParser.OC_ProjectionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ProjectionBody}.
	 * @param ctx the parse tree
	 */
	void exitOC_ProjectionBody(CypherParser.OC_ProjectionBodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ProjectionItemsStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 */
	void enterOC_ProjectionItemsStar(CypherParser.OC_ProjectionItemsStarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ProjectionItemsStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 */
	void exitOC_ProjectionItemsStar(CypherParser.OC_ProjectionItemsStarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ProjectionItemsNoStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 */
	void enterOC_ProjectionItemsNoStar(CypherParser.OC_ProjectionItemsNoStarContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ProjectionItemsNoStar}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItems}.
	 * @param ctx the parse tree
	 */
	void exitOC_ProjectionItemsNoStar(CypherParser.OC_ProjectionItemsNoStarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ProjectionItemVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 */
	void enterOC_ProjectionItemVariable(CypherParser.OC_ProjectionItemVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ProjectionItemVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 */
	void exitOC_ProjectionItemVariable(CypherParser.OC_ProjectionItemVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ProjectionItemNoVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 */
	void enterOC_ProjectionItemNoVariable(CypherParser.OC_ProjectionItemNoVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ProjectionItemNoVariable}
	 * labeled alternative in {@link CypherParser#oC_ProjectionItem}.
	 * @param ctx the parse tree
	 */
	void exitOC_ProjectionItemNoVariable(CypherParser.OC_ProjectionItemNoVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Order}.
	 * @param ctx the parse tree
	 */
	void enterOC_Order(CypherParser.OC_OrderContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Order}.
	 * @param ctx the parse tree
	 */
	void exitOC_Order(CypherParser.OC_OrderContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Limit}.
	 * @param ctx the parse tree
	 */
	void enterOC_Limit(CypherParser.OC_LimitContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Limit}.
	 * @param ctx the parse tree
	 */
	void exitOC_Limit(CypherParser.OC_LimitContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_SortItem}.
	 * @param ctx the parse tree
	 */
	void enterOC_SortItem(CypherParser.OC_SortItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_SortItem}.
	 * @param ctx the parse tree
	 */
	void exitOC_SortItem(CypherParser.OC_SortItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Where}.
	 * @param ctx the parse tree
	 */
	void enterOC_Where(CypherParser.OC_WhereContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Where}.
	 * @param ctx the parse tree
	 */
	void exitOC_Where(CypherParser.OC_WhereContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Pattern}.
	 * @param ctx the parse tree
	 */
	void enterOC_Pattern(CypherParser.OC_PatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Pattern}.
	 * @param ctx the parse tree
	 */
	void exitOC_Pattern(CypherParser.OC_PatternContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PatternPartVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 */
	void enterOC_PatternPartVariable(CypherParser.OC_PatternPartVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PatternPartVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 */
	void exitOC_PatternPartVariable(CypherParser.OC_PatternPartVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PatternPartNoVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 */
	void enterOC_PatternPartNoVariable(CypherParser.OC_PatternPartNoVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PatternPartNoVariable}
	 * labeled alternative in {@link CypherParser#oC_PatternPart}.
	 * @param ctx the parse tree
	 */
	void exitOC_PatternPartNoVariable(CypherParser.OC_PatternPartNoVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_AnonymousPatternPart}.
	 * @param ctx the parse tree
	 */
	void enterOC_AnonymousPatternPart(CypherParser.OC_AnonymousPatternPartContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_AnonymousPatternPart}.
	 * @param ctx the parse tree
	 */
	void exitOC_AnonymousPatternPart(CypherParser.OC_AnonymousPatternPartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PatternElementNoParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 */
	void enterOC_PatternElementNoParanthesis(CypherParser.OC_PatternElementNoParanthesisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PatternElementNoParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 */
	void exitOC_PatternElementNoParanthesis(CypherParser.OC_PatternElementNoParanthesisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PatternElementParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 */
	void enterOC_PatternElementParanthesis(CypherParser.OC_PatternElementParanthesisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PatternElementParanthesis}
	 * labeled alternative in {@link CypherParser#oC_PatternElement}.
	 * @param ctx the parse tree
	 */
	void exitOC_PatternElementParanthesis(CypherParser.OC_PatternElementParanthesisContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_NodePattern}.
	 * @param ctx the parse tree
	 */
	void enterOC_NodePattern(CypherParser.OC_NodePatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_NodePattern}.
	 * @param ctx the parse tree
	 */
	void exitOC_NodePattern(CypherParser.OC_NodePatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_PatternElementChain}.
	 * @param ctx the parse tree
	 */
	void enterOC_PatternElementChain(CypherParser.OC_PatternElementChainContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_PatternElementChain}.
	 * @param ctx the parse tree
	 */
	void exitOC_PatternElementChain(CypherParser.OC_PatternElementChainContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_RelationshipPatternBothDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelationshipPatternBothDir(CypherParser.OC_RelationshipPatternBothDirContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_RelationshipPatternBothDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelationshipPatternBothDir(CypherParser.OC_RelationshipPatternBothDirContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_RelationshipPatternLeftDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelationshipPatternLeftDir(CypherParser.OC_RelationshipPatternLeftDirContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_RelationshipPatternLeftDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelationshipPatternLeftDir(CypherParser.OC_RelationshipPatternLeftDirContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_RelationshipPatternRightDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelationshipPatternRightDir(CypherParser.OC_RelationshipPatternRightDirContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_RelationshipPatternRightDir}
	 * labeled alternative in {@link CypherParser#oC_RelationshipPattern}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelationshipPatternRightDir(CypherParser.OC_RelationshipPatternRightDirContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_RelationshipDetail}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelationshipDetail(CypherParser.OC_RelationshipDetailContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_RelationshipDetail}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelationshipDetail(CypherParser.OC_RelationshipDetailContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PropertiesMapLiteral}
	 * labeled alternative in {@link CypherParser#oC_Properties}.
	 * @param ctx the parse tree
	 */
	void enterOC_PropertiesMapLiteral(CypherParser.OC_PropertiesMapLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PropertiesMapLiteral}
	 * labeled alternative in {@link CypherParser#oC_Properties}.
	 * @param ctx the parse tree
	 */
	void exitOC_PropertiesMapLiteral(CypherParser.OC_PropertiesMapLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_RelationshipTypes}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelationshipTypes(CypherParser.OC_RelationshipTypesContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_RelationshipTypes}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelationshipTypes(CypherParser.OC_RelationshipTypesContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_NodeLabels}.
	 * @param ctx the parse tree
	 */
	void enterOC_NodeLabels(CypherParser.OC_NodeLabelsContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_NodeLabels}.
	 * @param ctx the parse tree
	 */
	void exitOC_NodeLabels(CypherParser.OC_NodeLabelsContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_NodeLabel}.
	 * @param ctx the parse tree
	 */
	void enterOC_NodeLabel(CypherParser.OC_NodeLabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_NodeLabel}.
	 * @param ctx the parse tree
	 */
	void exitOC_NodeLabel(CypherParser.OC_NodeLabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_LabelName}.
	 * @param ctx the parse tree
	 */
	void enterOC_LabelName(CypherParser.OC_LabelNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_LabelName}.
	 * @param ctx the parse tree
	 */
	void exitOC_LabelName(CypherParser.OC_LabelNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_RelTypeName}.
	 * @param ctx the parse tree
	 */
	void enterOC_RelTypeName(CypherParser.OC_RelTypeNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_RelTypeName}.
	 * @param ctx the parse tree
	 */
	void exitOC_RelTypeName(CypherParser.OC_RelTypeNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Expression}.
	 * @param ctx the parse tree
	 */
	void enterOC_Expression(CypherParser.OC_ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Expression}.
	 * @param ctx the parse tree
	 */
	void exitOC_Expression(CypherParser.OC_ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_AndExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_AndExpression(CypherParser.OC_AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_AndExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_AndExpression(CypherParser.OC_AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ComparisonSingle}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_ComparisonSingle(CypherParser.OC_ComparisonSingleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ComparisonSingle}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_ComparisonSingle(CypherParser.OC_ComparisonSingleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_ComparisonSingleOrMultiple}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_ComparisonSingleOrMultiple(CypherParser.OC_ComparisonSingleOrMultipleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_ComparisonSingleOrMultiple}
	 * labeled alternative in {@link CypherParser#oC_ComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_ComparisonSingleOrMultiple(CypherParser.OC_ComparisonSingleOrMultipleContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_StringListNullOperatorExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_StringListNullOperatorExpression(CypherParser.OC_StringListNullOperatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_StringListNullOperatorExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_StringListNullOperatorExpression(CypherParser.OC_StringListNullOperatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ListOperatorExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_ListOperatorExpression(CypherParser.OC_ListOperatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ListOperatorExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_ListOperatorExpression(CypherParser.OC_ListOperatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PropertyOrLabelsExpressionAtom}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_PropertyOrLabelsExpressionAtom(CypherParser.OC_PropertyOrLabelsExpressionAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PropertyOrLabelsExpressionAtom}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_PropertyOrLabelsExpressionAtom(CypherParser.OC_PropertyOrLabelsExpressionAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_PropertyOrLabelsExpressionLookup}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_PropertyOrLabelsExpressionLookup(CypherParser.OC_PropertyOrLabelsExpressionLookupContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_PropertyOrLabelsExpressionLookup}
	 * labeled alternative in {@link CypherParser#oC_PropertyOrLabelsExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_PropertyOrLabelsExpressionLookup(CypherParser.OC_PropertyOrLabelsExpressionLookupContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_AtomLiteral}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void enterOC_AtomLiteral(CypherParser.OC_AtomLiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_AtomLiteral}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void exitOC_AtomLiteral(CypherParser.OC_AtomLiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_AtomVariable}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void enterOC_AtomVariable(CypherParser.OC_AtomVariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_AtomVariable}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void exitOC_AtomVariable(CypherParser.OC_AtomVariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_AtomParenthesizedExpression}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void enterOC_AtomParenthesizedExpression(CypherParser.OC_AtomParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_AtomParenthesizedExpression}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void exitOC_AtomParenthesizedExpression(CypherParser.OC_AtomParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_AtomCase}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void enterOC_AtomCase(CypherParser.OC_AtomCaseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_AtomCase}
	 * labeled alternative in {@link CypherParser#oC_Atom}.
	 * @param ctx the parse tree
	 */
	void exitOC_AtomCase(CypherParser.OC_AtomCaseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_LiteralNumber}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void enterOC_LiteralNumber(CypherParser.OC_LiteralNumberContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_LiteralNumber}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void exitOC_LiteralNumber(CypherParser.OC_LiteralNumberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_LiteralString}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void enterOC_LiteralString(CypherParser.OC_LiteralStringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_LiteralString}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void exitOC_LiteralString(CypherParser.OC_LiteralStringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_LiteralBoolean}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void enterOC_LiteralBoolean(CypherParser.OC_LiteralBooleanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_LiteralBoolean}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void exitOC_LiteralBoolean(CypherParser.OC_LiteralBooleanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_LiteralMap}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void enterOC_LiteralMap(CypherParser.OC_LiteralMapContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_LiteralMap}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void exitOC_LiteralMap(CypherParser.OC_LiteralMapContext ctx);
	/**
	 * Enter a parse tree produced by the {@code oC_LiteralList}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void enterOC_LiteralList(CypherParser.OC_LiteralListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code oC_LiteralList}
	 * labeled alternative in {@link CypherParser#oC_Literal}.
	 * @param ctx the parse tree
	 */
	void exitOC_LiteralList(CypherParser.OC_LiteralListContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_BooleanLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_BooleanLiteral(CypherParser.OC_BooleanLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_BooleanLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_BooleanLiteral(CypherParser.OC_BooleanLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ListLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_ListLiteral(CypherParser.OC_ListLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ListLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_ListLiteral(CypherParser.OC_ListLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_PartialComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_PartialComparisonExpression(CypherParser.OC_PartialComparisonExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_PartialComparisonExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_PartialComparisonExpression(CypherParser.OC_PartialComparisonExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ParenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_ParenthesizedExpression(CypherParser.OC_ParenthesizedExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ParenthesizedExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_ParenthesizedExpression(CypherParser.OC_ParenthesizedExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_PropertyLookup}.
	 * @param ctx the parse tree
	 */
	void enterOC_PropertyLookup(CypherParser.OC_PropertyLookupContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_PropertyLookup}.
	 * @param ctx the parse tree
	 */
	void exitOC_PropertyLookup(CypherParser.OC_PropertyLookupContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_CaseExpression}.
	 * @param ctx the parse tree
	 */
	void enterOC_CaseExpression(CypherParser.OC_CaseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_CaseExpression}.
	 * @param ctx the parse tree
	 */
	void exitOC_CaseExpression(CypherParser.OC_CaseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_CaseAlternatives}.
	 * @param ctx the parse tree
	 */
	void enterOC_CaseAlternatives(CypherParser.OC_CaseAlternativesContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_CaseAlternatives}.
	 * @param ctx the parse tree
	 */
	void exitOC_CaseAlternatives(CypherParser.OC_CaseAlternativesContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Variable}.
	 * @param ctx the parse tree
	 */
	void enterOC_Variable(CypherParser.OC_VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Variable}.
	 * @param ctx the parse tree
	 */
	void exitOC_Variable(CypherParser.OC_VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_NumberLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_NumberLiteral(CypherParser.OC_NumberLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_NumberLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_NumberLiteral(CypherParser.OC_NumberLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_MapLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_MapLiteral(CypherParser.OC_MapLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_MapLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_MapLiteral(CypherParser.OC_MapLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_PropertyKeyName}.
	 * @param ctx the parse tree
	 */
	void enterOC_PropertyKeyName(CypherParser.OC_PropertyKeyNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_PropertyKeyName}.
	 * @param ctx the parse tree
	 */
	void exitOC_PropertyKeyName(CypherParser.OC_PropertyKeyNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_IntegerLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_IntegerLiteral(CypherParser.OC_IntegerLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_IntegerLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_IntegerLiteral(CypherParser.OC_IntegerLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_DoubleLiteral}.
	 * @param ctx the parse tree
	 */
	void enterOC_DoubleLiteral(CypherParser.OC_DoubleLiteralContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_DoubleLiteral}.
	 * @param ctx the parse tree
	 */
	void exitOC_DoubleLiteral(CypherParser.OC_DoubleLiteralContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_SchemaName}.
	 * @param ctx the parse tree
	 */
	void enterOC_SchemaName(CypherParser.OC_SchemaNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_SchemaName}.
	 * @param ctx the parse tree
	 */
	void exitOC_SchemaName(CypherParser.OC_SchemaNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_ReservedWord}.
	 * @param ctx the parse tree
	 */
	void enterOC_ReservedWord(CypherParser.OC_ReservedWordContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_ReservedWord}.
	 * @param ctx the parse tree
	 */
	void exitOC_ReservedWord(CypherParser.OC_ReservedWordContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_SymbolicName}.
	 * @param ctx the parse tree
	 */
	void enterOC_SymbolicName(CypherParser.OC_SymbolicNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_SymbolicName}.
	 * @param ctx the parse tree
	 */
	void exitOC_SymbolicName(CypherParser.OC_SymbolicNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_LeftArrowHead}.
	 * @param ctx the parse tree
	 */
	void enterOC_LeftArrowHead(CypherParser.OC_LeftArrowHeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_LeftArrowHead}.
	 * @param ctx the parse tree
	 */
	void exitOC_LeftArrowHead(CypherParser.OC_LeftArrowHeadContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_RightArrowHead}.
	 * @param ctx the parse tree
	 */
	void enterOC_RightArrowHead(CypherParser.OC_RightArrowHeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_RightArrowHead}.
	 * @param ctx the parse tree
	 */
	void exitOC_RightArrowHead(CypherParser.OC_RightArrowHeadContext ctx);
	/**
	 * Enter a parse tree produced by {@link CypherParser#oC_Dash}.
	 * @param ctx the parse tree
	 */
	void enterOC_Dash(CypherParser.OC_DashContext ctx);
	/**
	 * Exit a parse tree produced by {@link CypherParser#oC_Dash}.
	 * @param ctx the parse tree
	 */
	void exitOC_Dash(CypherParser.OC_DashContext ctx);
}