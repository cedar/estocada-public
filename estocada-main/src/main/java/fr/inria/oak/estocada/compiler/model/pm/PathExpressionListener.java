package fr.inria.oak.estocada.compiler.model.pm;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.StringConstant;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.DocumentsCatalog;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * SM PathExpressionListener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
class PathExpressionListener extends MADLIBBaseListener {
    private static final Logger LOGGER = Logger.getLogger(PathExpressionListener.class);
    private final VariableFactory cqVariableFactory;
    private final VariableMapper variableMapper;
    private final String documentNamePrefix;
    private final DocumentsCatalog documentsCatalog;
    private Set<Variable> referredVariables;
    private ParseTreeProperty<Variable> dmlStatementTreeProperty;
    private List<Atom> encoding;
    private Variable currentVar;

    @Inject
    public PathExpressionListener(@Named("document_name_prefix") final String documentNamePrefix,
            @Named("ConjunctiveQueryVariableFactory") final VariableFactory cqVariableFactory,
            final VariableMapper variableMapper, final DocumentsCatalog documentsCatalog) {
        this.documentNamePrefix = checkNotNull(documentNamePrefix);
        this.cqVariableFactory = checkNotNull(cqVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
        this.documentsCatalog = checkNotNull(documentsCatalog);
    }

    /**
     * Parse DML expression
     * 
     * @param str
     *            DML expression
     *
     * @return parsed expression
     * @throws ParseException
     */
    public PathExpression parse(final String str) throws ParseException {
        referredVariables = new HashSet<Variable>();
        encoding = new ArrayList<Atom>();
        currentVar = null;
        dmlStatementTreeProperty = new ParseTreeProperty<Variable>();
        final MADLIBLexer lexer = new MADLIBLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final MADLIBParser parser = new MADLIBParser(tokens);
        final ParserRuleContext tree = parser.madStatemnet();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
            return new PathExpression(PMModel.ID, referredVariables, encoding, currentVar,
                    new HashMap<String, String>());
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
    }

    @Override
    public void enterMadStatemnet(MADLIBParser.MadStatemnetContext ctx) {
        if (variableMapper.isNotDefined(ctx.madExpression().getChild(1).getText())) {
            LOGGER.debug("Entering matrix dml statemnet: " + ctx.getText());
            final Variable var = cqVariableFactory.createFreshVar();
            variableMapper.define(ctx.madExpression().getChild(1).getText(), var);
            encoding.add(new Atom(Predicate.NAME.toString(), var,
                    new StringConstant(ctx.madExpression().getChild(1).getText())));
            currentVar = var;
        }
    }

    @Override
    public void exitMatrixSourceName(MADLIBParser.MatrixSourceNameContext ctx) {
        LOGGER.debug("Entering matrix sources: " + ctx.getText());
        if (ctx != null) {
            final Variable var = variableMapper.getVariable(ctx.getText());
            referredVariables.add(var);
            setObject(ctx, var);
            currentVar = var;
        }
    }

    @Override
    public void exitMatrixMulExpression(MADLIBParser.MatrixMulExpressionContext ctx) {
        LOGGER.debug("Entering MatrixMulExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.matrixSourceName().get(0));
            final Variable varRight = retrieveVariable(ctx.matrixSourceName().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.MULTI.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixAddExpression(MADLIBParser.MatrixAddExpressionContext ctx) {
        LOGGER.debug("Entering MatrixAddExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.matrixSourceName().get(0));
            final Variable varRight = retrieveVariable(ctx.matrixSourceName().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.ADD.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixTransposeExpression(MADLIBParser.MatrixTransposeExpressionContext ctx) {
        LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.matrixSourceName());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.TRANS.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    /**
     * Retrieve variable associated with each sub-parse tree node.
     * 
     * @param subtree
     *            the sub-parse tree node
     * @return The corresponding sub-parse tree node variable
     */
    private Variable retrieveVariable(ParseTree subtree) {

        return dmlStatementTreeProperty.get(subtree);
    }

    /**
     * @param subtree
     *            the sub-parse tree node
     * @param var
     *            The variable that corresponds subtree
     */
    private void setObject(ParseTree subtree, Variable var) {
        dmlStatementTreeProperty.put(subtree, var);
    }

}
