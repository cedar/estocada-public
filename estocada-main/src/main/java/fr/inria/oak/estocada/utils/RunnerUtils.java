package fr.inria.oak.estocada.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.conjunctivequery.parser.ParseException;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.model.pj.full.Predicate;
import fr.inria.oak.estocada.compiler.model.qbt.QBTNaiveModule;
import fr.inria.oak.estocada.compiler.model.qbt.QBTQueryBlockTreeBuilder;

public class RunnerUtils {
    /**
     * Pre-process QBT query (temporary)
     * 
     * @param query
     *            the QBT query
     * @return processed query
     */
    public static String preProcessQuery(final String query) {

        final StringBuilder sre = new StringBuilder();
        final String viewName = StringUtils.substringBefore(query, "FOR");
        final String returnVar = StringUtils.substringAfter(query, "RETURN");
        final String pattern = StringUtils.substringBetween(query, "FOR", "RETURN");
        final String select = StringUtils.substringBetween(pattern, "SELECT", "FROM");
        boolean flag = false;
        if (query.contains("WHERE") || query.contains("where")) {
            flag = true;
        }
        String from = null;
        String where = null;
        if (flag) {
            from = StringUtils.substringBetween(pattern, "FROM", "WHERE");
            where = StringUtils.substringBetween(pattern, "WHERE", "}");

        } else {
            from = StringUtils.substringBetween(pattern, "FROM", "}");
        }

        sre.append(viewName);
        sre.append("FOR PJ:{");
        sre.append(" FROM ");
        sre.append(from);
        sre.append("\n");
        if (flag) {
            sre.append(" WHERE ");
            sre.append(where);
        }
        sre.append("\n");
        sre.append("SELECT");
        sre.append(select);
        sre.append("}");
        sre.append("\n");
        sre.append("RETURN");
        sre.append(returnVar);
        String reProcessedQueery = sre.toString();
        reProcessedQueery = reProcessedQueery.replace("jsonb_array_elements", "JSONARRAYELEMENTS");
        reProcessedQueery = reProcessedQueery.replace("json_build_object", "JSONBUILDOBJECT");
        return reProcessedQueery;
    }

    /**
     * Encode QBT query
     * 
     * @param query
     * @return
     */
    public static String encodeUserQuery(final String query) {
        final StringBuilder queryPre = new StringBuilder();
        final String beforeReturn = StringUtils.substringBefore(query, "RETURN");
        final String afterReturn = StringUtils.substringAfter(query, "RETURN");
        queryPre.append(beforeReturn);
        queryPre.append("\n");
        queryPre.append("RETURN ");
        queryPre.append(" PR:{");
        queryPre.append(afterReturn);
        queryPre.append(" }");
        final StringBuilder cqBuilder = new StringBuilder();
        final Injector injector = Guice.createInjector(new QBTNaiveModule());
        final QueryBlockTreeBuilder builder = injector.getInstance(QBTQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder mixedBuilder = (QBTQueryBlockTreeBuilder) builder;
        mixedBuilder.setRealtions(null);
        System.out.print(queryPre);
        QueryBlockTree nbt = mixedBuilder.buildQueryBlockTree(queryPre.toString());
        System.out.println(nbt);
        cqBuilder.append(nbt.getQueryName());
        cqBuilder.append("<");
        int count = 0;
        List<ReturnTerm> terms = nbt.getRoot().getReturnTemplate().getTerms();
        for (final ReturnTerm term : terms) {
            cqBuilder.append(term.getReferredVariables().iterator().next());
            if (count != (terms.size() - 1))
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(">:-");
        //Body (Path)
        final List<Atom> atomsPath = modifiedPremise(nbt.getRoot().getPattern()
                .encoding(fr.inria.oak.estocada.compiler.model.pj.full.Utils.conditionEncoding));
        count = 0;
        for (final Atom atom : atomsPath) {
            cqBuilder.append(atom);
            if (count != atomsPath.size() - 1)
                cqBuilder.append(",");
            count++;
        }
        cqBuilder.append(";");
        QBTQueryBlockTreeBuilder.reIntilize();
        return cqBuilder.toString();
    }

    /**
     * Temporary implementation to resolve join condition
     * 
     * @param premise
     *            PJ premise.
     * @return updated PJ premise.
     */
    public static List<Atom> modifiedPremise(final List<Atom> premise) {
        final List<Atom> copyList = new ArrayList<>(premise);
        final List<Atom> updatedAtoms = new ArrayList<>();
        final List<Term> joinTerms = new ArrayList<Term>();
        final Iterator<Atom> itertaor = copyList.listIterator();
        while (itertaor.hasNext()) {
            final Atom atom = itertaor.next();
            if (atom.getTerms().size() == 2 && atom.getPredicate().equals(Predicate.VAL.toString())) {
                if (atom.getTerms().get(0) instanceof Variable && atom.getTerms().get(1) instanceof Variable) {
                    joinTerms.add(atom.getTerms().get(0));
                    joinTerms.add(atom.getTerms().get(1));
                    itertaor.remove();
                }
            }
        }
        for (final Atom atom : copyList) {
            int index = -1;
            if (atom.getTerms().contains(joinTerms.get(0))) {
                index = atom.getTerms().indexOf(joinTerms.get(0));

            } else {
                if (atom.getTerms().contains(joinTerms.get(1))) {
                    index = atom.getTerms().indexOf(joinTerms.get(1));
                } else {
                    updatedAtoms.add(atom);
                }
            }
            if (index != -1) {
                final List<Term> terms = new ArrayList<Term>();
                terms.addAll(atom.getTerms());
                terms.set(index, joinTerms.get(0));
                final Atom atomNew = new Atom(atom.getPredicate(), terms);
                updatedAtoms.add(atomNew);
            }
        }
        return updatedAtoms;
    }

    public static String readQuery(final String folderName)
            throws IllegalArgumentException, ParseException, IOException {
        final File directory = new File(folderName);
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.getName().contains(".Q")) {
                return FileUtils.readFileToString(file);
            }

        }
        return null;
    }

    public static List<String> readViews(final String folderName)
            throws IllegalArgumentException, ParseException, IOException {
        final File directory = new File(folderName);
        File[] files = directory.listFiles();
        final List<String> views = new ArrayList<String>();
        for (File file : files) {
            if (file.getName().contains(".V")) {
                String view = FileUtils.readFileToString(file);
                view = view.replace("jsonb_array_elements", "JSONARRAYELEMENTS");
                if (view.contains("solr")) {
                    view = view.replace("solr_json_build_object", "SOLRJSONBUILDOBJECT");
                } else {
                    view = view.replace("json_build_object", "JSONBUILDOBJECT");
                }
                views.add(view);
            }

        }
        return views;
    }
}
