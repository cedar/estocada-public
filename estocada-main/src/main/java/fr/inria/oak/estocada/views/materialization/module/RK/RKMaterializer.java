package fr.inria.oak.estocada.views.materialization.module.RK;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.oak.estocada.views.materialization.module.Materializer;
import fr.inria.oak.estocada.views.materialization.module.MaterializerException;
import fr.inria.oak.estocada.views.materialization.module.blocks.QBTPattern;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

/**
 * RK Materializer.
 * 
 * @author ranaalotaibi
 *
 */
public final class RKMaterializer implements Materializer {
    private static final Logger LOGGER = Logger.getLogger(RKMaterializer.class);
    private final String viewName;
    private final Map<String, Map<String, String>> rkMapBuilder;
    private final QBTPattern qbtPattern;
    private final StorageReference gsr;
    private Jedis jedis;

    /** Constructor **/
    public RKMaterializer(final String viewName, final Map<String, Map<String, String>> rkMapBuilder,
            final QBTPattern qbtPattern, final StorageReference gsr) {
        this.qbtPattern = checkNotNull(qbtPattern);
        this.rkMapBuilder = checkNotNull(rkMapBuilder);
        this.viewName = checkNotNull(viewName);
        this.gsr = checkNotNull(gsr);
        redisConnection();
    }

    @Override
    public void materialize() throws MaterializerException {
        try {
            executePhyEval();
            LOGGER.debug("View materialized successfully.");
        } catch (MaterializerException exception) {
            LOGGER.error("View materialization faild :" + exception.getMessage());
            throw exception;
        }
    }

    /**
     * Execute HMSET commands to Redis.
     * 
     * @param nrsmd
     *            NRSMD of the input tuple to be inserted to Redis
     * 
     * @param next
     *            the NTuple value that need to be inserted
     * @throws MaterializerException
     */
    private void executeHMSETStatement(final NRSMD nrsmd, final NTuple next) throws MaterializerException {
        final TupleMetadataType[] tupleMetadataType = nrsmd.getColumnsMetadata();
        String outerMapKeyValueStr = null;
        int outerMapKeyValueInt = 0;
        try {
            for (Map.Entry<String, Map<String, String>> outerMap : rkMapBuilder.entrySet()) {
                int colInx0;
                colInx0 = nrsmd.getColIndexFromName(outerMap.getKey());

                if (tupleMetadataType[colInx0].equals(TupleMetadataType.STRING_TYPE)) {
                    char[] keyColumnStr = next.getStringField(colInx0);
                    outerMapKeyValueStr = String.copyValueOf(keyColumnStr);
                } else {
                    outerMapKeyValueInt = next.getIntegerField(colInx0);
                }
                final Map<String, String> innerMapConstruction = new HashMap<String, String>();

                for (Map.Entry<String, String> innerMap : outerMap.getValue().entrySet()) {
                    int colInx1 = nrsmd.getColIndexFromName(innerMap.getValue());
                    if (tupleMetadataType[colInx1].equals(TupleMetadataType.STRING_TYPE)) {
                        final char[] stringValue = next.getStringField(colInx1);
                        final String innerMapKeyValue = String.copyValueOf(stringValue);
                        innerMapConstruction.put(innerMap.getKey().replace("\"", ""), innerMapKeyValue);
                    }
                    if (tupleMetadataType[colInx1].equals(TupleMetadataType.INTEGER_TYPE)) {
                        final int integerValue = next.getIntegerField(colInx1);
                        innerMapConstruction.put(innerMap.getKey().replace("\"", ""), Integer.toString(integerValue));
                    }
                }
                if (outerMapKeyValueStr == null) {
                    jedis.hmset(Integer.toString(outerMapKeyValueInt), innerMapConstruction);

                } else {
                    jedis.hmset(outerMapKeyValueStr, innerMapConstruction);
                }
            }
        } catch (JedisException | TatooineExecutionException exception) {
            LOGGER.error("Data couldn't be loaded into Redis: " + exception.getMessage());
            throw new MaterializerException(exception);
        }
    }

    /**
     * Execute the QBT pattern operator.
     * 
     * @param phyEval
     *            the physical operator.
     * @throws TatooineExecutionException
     */
    void executePhyEval() throws MaterializerException {
        try {
            final NIterator phyEval = qbtPattern.getQBTBlockPhyEval();
            phyEval.open();
            while (phyEval.hasNext()) {
                final NTuple next = phyEval.next();
                executeHMSETStatement(qbtPattern.getBlockNRSMD(), next);
            }
            phyEval.close();
        } catch (TatooineExecutionException | MaterializerException exception) {
            throw new MaterializerException(exception);
        }

    }

    /**
     * Establish Redis connection
     */
    private void redisConnection() {

        jedis = new Jedis(gsr.getPropertyValue("host"));
        jedis.select(1);
    }

}
