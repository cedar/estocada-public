package fr.inria.oak.estocada.compiler.model.tq;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * 
 * TQQueryBlockTreeBuilder
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
public final class TQQueryBlockTreeBuilder implements QueryBlockTreeBuilder {
    private final ColumnLookUpListener columnLookUpListener;
    private final ReturnTemplateListener returnTemplateListener;
    private final BlockTreeListener rootBlock;
    private final PatternListener patetrnListener;

    @Inject
    public TQQueryBlockTreeBuilder(final ColumnLookUpListener ccolumnLookUpListener,
            final ReturnTemplateListener returnTemplateListener, final BlockTreeListener rootBlock,
            final PatternListener patternListener) {
        this.columnLookUpListener = ccolumnLookUpListener;
        this.returnTemplateListener = returnTemplateListener;
        this.rootBlock = rootBlock;
        this.patetrnListener = patternListener;
    }

    @Override
    public PathExpression buildPathExpression(final String str) throws ParseException {
        return columnLookUpListener.parse(str);
    }

    /**
     * The definitions are not used.
     */
    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, final String str)
            throws ParseException {
        return returnTemplateListener.parse(str);
    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {
        return rootBlock.parse(str);
    }

    @Override
    public Pattern buildPattern(String str) throws ParseException {
        return patetrnListener.parse(str);
    }

    public VariableMapper getVariableMapper() {
        return patetrnListener.getVariableMapper();
    }
}
