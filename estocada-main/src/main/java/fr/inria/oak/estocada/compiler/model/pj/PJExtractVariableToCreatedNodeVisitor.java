package fr.inria.oak.estocada.compiler.model.pj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.VariableCopier;

/**
 * PJ ExtractVariableToCreatedNodeVisitor which implements {@link ReturnTemplateVisitor}
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
public class PJExtractVariableToCreatedNodeVisitor implements ReturnTemplateVisitor {
    private final VariableCopier variableCopier;
    private ImmutableList.Builder<Atom> builder;
    private String queryName;
    private Set<Variable> copiedVariables;

    @Inject
    public PJExtractVariableToCreatedNodeVisitor(final VariableCopier variableCopier) {
        this.variableCopier = variableCopier;
    }

    public List<Atom> encode(final ReturnTemplate template, final String queryName) {
        builder = ImmutableList.builder();
        this.queryName = queryName;
        copiedVariables = new HashSet<Variable>();
        template.accept(this);
        return builder.build();
    }

    public List<Atom> encode(Variable viewID) {
        final List<Atom> viewConclusion = new ArrayList<Atom>();
        viewConclusion.add(new Atom(queryName, viewID));
        return viewConclusion;
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        // NOP (no encoding before the children are processed)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        if (!copiedVariables.contains(term.getVariable())) {
            copiedVariables.add(term.getVariable());
            builder.add(new Atom(Predicate.COPY.toString() + "_" + queryName, term.toTerm(),
                    variableCopier.getCopy(term.getVariable())));
        }
    }

    @Override
    public void visit(ReturnStringTerm term) {
        // NOP (no encoding for string terms)
    }
}