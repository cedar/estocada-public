// Generated from ConjunctiveQuery.g4 by ANTLR 4.7.2

    package fr.inria.oak.estocada.compiler.model.cg;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ConjunctiveQueryParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, RwName=24, 
		Identifier=25, StringConstant=26, WS=27;
	public static final int
		RULE_conjunctiveQuery = 0, RULE_queryHeader = 1, RULE_queryBody = 2, RULE_labelI = 3, 
		RULE_kindI = 4, RULE_propertyI = 5, RULE_compI = 6, RULE_valueI = 7, RULE_connectionI = 8, 
		RULE_nestValueI = 9;
	private static String[] makeRuleNames() {
		return new String[] {
			"conjunctiveQuery", "queryHeader", "queryBody", "labelI", "kindI", "propertyI", 
			"compI", "valueI", "connectionI", "nestValueI"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "':-'", "'<'", "','", "'>'", "';'", "'Label_I'", "'('", "')'", 
			"'Kind_I'", "'\"N\"'", "'\"E\"'", "'Property_I'", "'Eq_I'", "'Ne_I'", 
			"'Lt_I'", "'Gt_I'", "'Le_I'", "'Ge_I'", "'Value_I'", "'Connection_I'", 
			"'NestValue_I'", "'\"a\"'", "'\"o\"'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"RwName", "Identifier", "StringConstant", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ConjunctiveQuery.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ConjunctiveQueryParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ConjunctiveQueryContext extends ParserRuleContext {
		public QueryHeaderContext queryHeader() {
			return getRuleContext(QueryHeaderContext.class,0);
		}
		public QueryBodyContext queryBody() {
			return getRuleContext(QueryBodyContext.class,0);
		}
		public ConjunctiveQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conjunctiveQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterConjunctiveQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitConjunctiveQuery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitConjunctiveQuery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConjunctiveQueryContext conjunctiveQuery() throws RecognitionException {
		ConjunctiveQueryContext _localctx = new ConjunctiveQueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_conjunctiveQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20);
			queryHeader();
			setState(21);
			match(T__0);
			setState(22);
			queryBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryHeaderContext extends ParserRuleContext {
		public TerminalNode RwName() { return getToken(ConjunctiveQueryParser.RwName, 0); }
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public QueryHeaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryHeader; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterQueryHeader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitQueryHeader(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitQueryHeader(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryHeaderContext queryHeader() throws RecognitionException {
		QueryHeaderContext _localctx = new QueryHeaderContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_queryHeader);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			match(RwName);
			setState(25);
			match(T__1);
			setState(26);
			match(Identifier);
			setState(31);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(27);
				match(T__2);
				setState(28);
				match(Identifier);
				}
				}
				setState(33);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(34);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryBodyContext extends ParserRuleContext {
		public List<LabelIContext> labelI() {
			return getRuleContexts(LabelIContext.class);
		}
		public LabelIContext labelI(int i) {
			return getRuleContext(LabelIContext.class,i);
		}
		public List<KindIContext> kindI() {
			return getRuleContexts(KindIContext.class);
		}
		public KindIContext kindI(int i) {
			return getRuleContext(KindIContext.class,i);
		}
		public List<PropertyIContext> propertyI() {
			return getRuleContexts(PropertyIContext.class);
		}
		public PropertyIContext propertyI(int i) {
			return getRuleContext(PropertyIContext.class,i);
		}
		public List<CompIContext> compI() {
			return getRuleContexts(CompIContext.class);
		}
		public CompIContext compI(int i) {
			return getRuleContext(CompIContext.class,i);
		}
		public List<ValueIContext> valueI() {
			return getRuleContexts(ValueIContext.class);
		}
		public ValueIContext valueI(int i) {
			return getRuleContext(ValueIContext.class,i);
		}
		public List<ConnectionIContext> connectionI() {
			return getRuleContexts(ConnectionIContext.class);
		}
		public ConnectionIContext connectionI(int i) {
			return getRuleContext(ConnectionIContext.class,i);
		}
		public List<NestValueIContext> nestValueI() {
			return getRuleContexts(NestValueIContext.class);
		}
		public NestValueIContext nestValueI(int i) {
			return getRuleContext(NestValueIContext.class,i);
		}
		public QueryBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterQueryBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitQueryBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitQueryBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryBodyContext queryBody() throws RecognitionException {
		QueryBodyContext _localctx = new QueryBodyContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_queryBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				{
				setState(36);
				labelI();
				}
				break;
			case T__8:
				{
				setState(37);
				kindI();
				}
				break;
			case T__11:
				{
				setState(38);
				propertyI();
				}
				break;
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__16:
			case T__17:
				{
				setState(39);
				compI();
				}
				break;
			case T__18:
				{
				setState(40);
				valueI();
				}
				break;
			case T__19:
				{
				setState(41);
				connectionI();
				}
				break;
			case T__20:
				{
				setState(42);
				nestValueI();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(57);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(45);
				match(T__2);
				setState(53);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__5:
					{
					setState(46);
					labelI();
					}
					break;
				case T__8:
					{
					setState(47);
					kindI();
					}
					break;
				case T__11:
					{
					setState(48);
					propertyI();
					}
					break;
				case T__12:
				case T__13:
				case T__14:
				case T__15:
				case T__16:
				case T__17:
					{
					setState(49);
					compI();
					}
					break;
				case T__18:
					{
					setState(50);
					valueI();
					}
					break;
				case T__19:
					{
					setState(51);
					connectionI();
					}
					break;
				case T__20:
					{
					setState(52);
					nestValueI();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(59);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(60);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelIContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(ConjunctiveQueryParser.Identifier, 0); }
		public TerminalNode StringConstant() { return getToken(ConjunctiveQueryParser.StringConstant, 0); }
		public LabelIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterLabelI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitLabelI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitLabelI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LabelIContext labelI() throws RecognitionException {
		LabelIContext _localctx = new LabelIContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_labelI);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			match(T__5);
			setState(63);
			match(T__6);
			setState(64);
			match(Identifier);
			setState(65);
			match(T__2);
			setState(66);
			match(StringConstant);
			setState(67);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KindIContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(ConjunctiveQueryParser.Identifier, 0); }
		public KindIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_kindI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterKindI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitKindI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitKindI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KindIContext kindI() throws RecognitionException {
		KindIContext _localctx = new KindIContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_kindI);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			match(T__8);
			setState(70);
			match(T__6);
			setState(71);
			match(Identifier);
			setState(72);
			match(T__2);
			setState(73);
			_la = _input.LA(1);
			if ( !(_la==T__9 || _la==T__10) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(74);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyIContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public TerminalNode StringConstant() { return getToken(ConjunctiveQueryParser.StringConstant, 0); }
		public PropertyIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterPropertyI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitPropertyI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitPropertyI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PropertyIContext propertyI() throws RecognitionException {
		PropertyIContext _localctx = new PropertyIContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_propertyI);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			match(T__11);
			setState(77);
			match(T__6);
			setState(78);
			match(Identifier);
			setState(79);
			match(T__2);
			setState(80);
			match(StringConstant);
			setState(81);
			match(T__2);
			setState(82);
			match(Identifier);
			setState(83);
			match(T__2);
			setState(84);
			match(Identifier);
			setState(85);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompIContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public CompIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterCompI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitCompI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitCompI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompIContext compI() throws RecognitionException {
		CompIContext _localctx = new CompIContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_compI);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << T__17))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(88);
			match(T__6);
			setState(89);
			match(Identifier);
			setState(90);
			match(T__2);
			setState(91);
			match(Identifier);
			setState(92);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueIContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public TerminalNode StringConstant() { return getToken(ConjunctiveQueryParser.StringConstant, 0); }
		public ValueIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterValueI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitValueI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitValueI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueIContext valueI() throws RecognitionException {
		ValueIContext _localctx = new ValueIContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_valueI);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94);
			match(T__18);
			setState(95);
			match(T__6);
			setState(96);
			match(Identifier);
			setState(97);
			match(T__2);
			setState(98);
			_la = _input.LA(1);
			if ( !(_la==Identifier || _la==StringConstant) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(99);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConnectionIContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public ConnectionIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_connectionI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterConnectionI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitConnectionI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitConnectionI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConnectionIContext connectionI() throws RecognitionException {
		ConnectionIContext _localctx = new ConnectionIContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_connectionI);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(T__19);
			setState(102);
			match(T__6);
			setState(103);
			match(Identifier);
			setState(104);
			match(T__2);
			setState(105);
			match(Identifier);
			setState(106);
			match(T__2);
			setState(107);
			match(Identifier);
			setState(108);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NestValueIContext extends ParserRuleContext {
		public List<TerminalNode> Identifier() { return getTokens(ConjunctiveQueryParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(ConjunctiveQueryParser.Identifier, i);
		}
		public TerminalNode StringConstant() { return getToken(ConjunctiveQueryParser.StringConstant, 0); }
		public NestValueIContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nestValueI; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).enterNestValueI(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ConjunctiveQueryListener ) ((ConjunctiveQueryListener)listener).exitNestValueI(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ConjunctiveQueryVisitor ) return ((ConjunctiveQueryVisitor<? extends T>)visitor).visitNestValueI(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NestValueIContext nestValueI() throws RecognitionException {
		NestValueIContext _localctx = new NestValueIContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_nestValueI);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(110);
			match(T__20);
			setState(111);
			match(T__6);
			setState(112);
			match(Identifier);
			setState(113);
			match(T__2);
			setState(114);
			match(Identifier);
			setState(115);
			match(T__2);
			setState(116);
			match(StringConstant);
			setState(117);
			match(T__2);
			setState(118);
			_la = _input.LA(1);
			if ( !(_la==T__21 || _la==T__22) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(119);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\35|\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3"+
		"\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3 \n\3\f\3\16\3#\13\3\3\3\3\3\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\5\4.\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4"+
		"8\n\4\7\4:\n\4\f\4\16\4=\13\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\2\2\f\2\4\6\b\n\f\16\20\22\24\2\6\3\2\f\r\3\2\17\24\3\2\33"+
		"\34\3\2\30\31\2\177\2\26\3\2\2\2\4\32\3\2\2\2\6-\3\2\2\2\b@\3\2\2\2\n"+
		"G\3\2\2\2\fN\3\2\2\2\16Y\3\2\2\2\20`\3\2\2\2\22g\3\2\2\2\24p\3\2\2\2\26"+
		"\27\5\4\3\2\27\30\7\3\2\2\30\31\5\6\4\2\31\3\3\2\2\2\32\33\7\32\2\2\33"+
		"\34\7\4\2\2\34!\7\33\2\2\35\36\7\5\2\2\36 \7\33\2\2\37\35\3\2\2\2 #\3"+
		"\2\2\2!\37\3\2\2\2!\"\3\2\2\2\"$\3\2\2\2#!\3\2\2\2$%\7\6\2\2%\5\3\2\2"+
		"\2&.\5\b\5\2\'.\5\n\6\2(.\5\f\7\2).\5\16\b\2*.\5\20\t\2+.\5\22\n\2,.\5"+
		"\24\13\2-&\3\2\2\2-\'\3\2\2\2-(\3\2\2\2-)\3\2\2\2-*\3\2\2\2-+\3\2\2\2"+
		"-,\3\2\2\2.;\3\2\2\2/\67\7\5\2\2\608\5\b\5\2\618\5\n\6\2\628\5\f\7\2\63"+
		"8\5\16\b\2\648\5\20\t\2\658\5\22\n\2\668\5\24\13\2\67\60\3\2\2\2\67\61"+
		"\3\2\2\2\67\62\3\2\2\2\67\63\3\2\2\2\67\64\3\2\2\2\67\65\3\2\2\2\67\66"+
		"\3\2\2\28:\3\2\2\29/\3\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<>\3\2\2\2="+
		";\3\2\2\2>?\7\7\2\2?\7\3\2\2\2@A\7\b\2\2AB\7\t\2\2BC\7\33\2\2CD\7\5\2"+
		"\2DE\7\34\2\2EF\7\n\2\2F\t\3\2\2\2GH\7\13\2\2HI\7\t\2\2IJ\7\33\2\2JK\7"+
		"\5\2\2KL\t\2\2\2LM\7\n\2\2M\13\3\2\2\2NO\7\16\2\2OP\7\t\2\2PQ\7\33\2\2"+
		"QR\7\5\2\2RS\7\34\2\2ST\7\5\2\2TU\7\33\2\2UV\7\5\2\2VW\7\33\2\2WX\7\n"+
		"\2\2X\r\3\2\2\2YZ\t\3\2\2Z[\7\t\2\2[\\\7\33\2\2\\]\7\5\2\2]^\7\33\2\2"+
		"^_\7\n\2\2_\17\3\2\2\2`a\7\25\2\2ab\7\t\2\2bc\7\33\2\2cd\7\5\2\2de\t\4"+
		"\2\2ef\7\n\2\2f\21\3\2\2\2gh\7\26\2\2hi\7\t\2\2ij\7\33\2\2jk\7\5\2\2k"+
		"l\7\33\2\2lm\7\5\2\2mn\7\33\2\2no\7\n\2\2o\23\3\2\2\2pq\7\27\2\2qr\7\t"+
		"\2\2rs\7\33\2\2st\7\5\2\2tu\7\33\2\2uv\7\5\2\2vw\7\34\2\2wx\7\5\2\2xy"+
		"\t\5\2\2yz\7\n\2\2z\25\3\2\2\2\6!-\67;";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}