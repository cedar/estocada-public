package fr.inria.oak.estocada.views.materialization.module.RK;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * This is for test purposes
 *
 */
public class RegisterMetadata {

    public static void RegisterDataset() throws Exception {

        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        catalog.delete();
        // Register
        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("testPostgreJSON");
        // Columns names
        final List<String> colNames = new ArrayList<String>();
        colNames.add("created_at_s");
        colNames.add("user_name_s");
        colNames.add("user_screen_name_s");
        colNames.add("text_s");

        final NRSMD nrsmd = new NRSMD(3,
                new TupleMetadataType[] { TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE,
                        TupleMetadataType.STRING_TYPE, TupleMetadataType.STRING_TYPE },
                new String[] { "created_at_s", "user_name_s", "user_screen_name_s" }, new NRSMD[0]);
        final Map<String, Integer> serviceNRSMDMapping2 = new HashMap<>();
        serviceNRSMDMapping2.put("nameN", 0);
        serviceNRSMDMapping2.put("ageN", 1);
        serviceNRSMDMapping2.put("idN", 2);
        ViewSchema schema = new ViewSchema(nrsmd, serviceNRSMDMapping2);
        catalog.add("testPostgreJSON", storageReferencePostgresSQL1, null, schema);
    }

    /**
     * Storage reference for data in PostgreSQL
     * 
     * @param collectionName
     * @return
     * @throws Exception
     */
    private static StorageReference getPostgresSQLStorageReference(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        return gsr;
    }
}
