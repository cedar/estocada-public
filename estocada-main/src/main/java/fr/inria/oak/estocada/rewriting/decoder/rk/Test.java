package fr.inria.oak.estocada.rewriting.decoder.rk;

import java.io.File;
import java.util.Collection;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Utils;

public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/main/resources/testRKDecoder/reformulation");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        final Collection<ConjunctiveQuery> expectedReformulations = Utils.parseQueries((INPUT_QUERY_FILE));
        final RKTranslator rkTranslatorFixed = new RKTranslator();
        final String decodedRW = rkTranslatorFixed.translate(expectedReformulations.iterator().next());
        System.out.print(decodedRW);
    }

}
