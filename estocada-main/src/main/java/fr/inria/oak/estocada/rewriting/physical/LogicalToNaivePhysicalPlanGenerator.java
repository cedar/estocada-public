package fr.inria.oak.estocada.rewriting.physical;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;

/**
 * A class that generates and executes a naive physical plan. The join order is random and not optimized.
 * 
 * @author ranaalotaibi
 *
 */
public class LogicalToNaivePhysicalPlanGenerator {
    private static final Logger LOGGER = Logger.getLogger(LogicalToNaivePhysicalPlanGenerator.class);
    final LogOperator rootOperator;

    /** Constructor **/
    public LogicalToNaivePhysicalPlanGenerator(final LogOperator rootOperator) {
        this.rootOperator = rootOperator;
    }

    /**
     * Return the root of the physical plan.
     * 
     * @return return the root.
     */
    public NIterator generate() {
        if (rootOperator == null) {
            throw new IllegalArgumentException("Expected logical root operator!");
        }
        final LogPlanListener logPlanListener = new LogPlanListener();
        rootOperator.accept(logPlanListener);
        return logPlanListener.physicalPlanRootOperator();
    }
}
