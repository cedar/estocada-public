package fr.inria.oak.estocada.compiler.model.tm;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.AntlrUtils;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;

/**
 * SJ Structural Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class);

    @Inject
    public StructuralListener(final PathExpressionListener pathExpressionlistener,
            @Named("TFMVariableFactory") VariableFactory dmlVariableFactory, VariableMapper variableMapper) {
        super(pathExpressionlistener, dmlVariableFactory, variableMapper);
    }

    @Override
    public void enterTfmStatemnet(TFMParser.TfmStatemnetContext ctx) {
        LOGGER.debug("Entering TFM Statement Query: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }

        final Variable var = dmlVariableFactory.createFreshVar();
        variableMapper.define(ctx.matrixName().getText(), var);
        currentVar = var;
        final PathExpression expr = pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar);
        defineVariable(expr);
    }

    @Override
    protected ParserRuleContext createParseTree(final TFMParser parser) {
        return parser.tfmScript();
    }
}
