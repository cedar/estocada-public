package fr.inria.oak.estocada.extension.BigDAWG;

import java.text.ParseException;
import java.util.Collection;

import org.apache.commons.lang3.tuple.Pair;

import istc.bigdawg.executor.ExecutorEngine;
import istc.bigdawg.executor.ExecutorEngine.LocalQueryExecutionException;
import istc.bigdawg.query.ConnectionInfo;

public class SolrConnectionInfo implements ConnectionInfo {

	private static final long serialVersionUID = -708473017100057496L;
	String serverUrl;
	String serverPort;
	String coreName; // this goes to database


	public SolrConnectionInfo(String serverUrl, String serverPort,
			String coreName) {
		this.serverUrl = serverUrl;
		this.serverPort =serverPort;
		this.coreName = coreName;
	}

	@Override
	public String getUrl() {
		// TODO Auto-generated method stub
		return serverUrl;
	}

	@Override
	public String getHost() {
		return null;
	}

	@Override
	public String getPort() {
		return serverPort;
	}

	@Override
	public String getUser() {
		return null;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getDatabase() {
		return coreName;
	}

	@Override
	public String toString() {
		return "SolrConnectionInfo [serverUrl=" + serverPort
				+ ", coreName=" + coreName + ", coreName="
				+ coreName +"]";
	}

	@Override
	public Collection<String> getCleanupQuery(Collection<String> objects) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long[] computeHistogram(String object, String attribute, double start, double end, int numBuckets)
			throws LocalQueryExecutionException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Pair<Number, Number> getMinMax(String object, String attribute)
			throws LocalQueryExecutionException, ParseException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExecutorEngine getLocalQueryExecutor() throws LocalQueryExecutorLookupException {
		// TODO Auto-generated method stub
		return null;
	}

}
