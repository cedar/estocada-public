package fr.inria.oak.estocada.rewriting.decoder.sm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.compiler.model.sm.Predicate;

/**
 * 
 * This class translates the conjunctive rewriting into DML query syntax
 * 
 * @author ranaalotaibi
 *
 */
public class SMTranslator {

    private static final Logger LOGGER = Logger.getLogger(SMTranslator.class);
    private static final Character LParenthesis = '(';
    private static final Character RParenthesis = ')';
    private ConjunctiveQuery rewriting;
    private Map<Term, String> varaibleMapping;
    private Map<Term, String> baseMaticesMapping;
    private Map<Term, String> idDecodingMapping;
    private StringBuilder laodMatrices;
    private List<Atom> sizeMatrices;
    private Map<Term, Atom> idAtomMapping;
    private String headAfter;
    private Iterator<Map.Entry<Term, Atom>> iterator;

    /**
     * Constructor
     * 
     * @param query
     *            rewriting conjunctive query
     */
    public SMTranslator(final ConjunctiveQuery rewriting) {
        this();
        this.rewriting = rewriting;
    }

    /**
     * Default Constructor
     * 
     * @param query
     *            rewriting conjunctive query
     */
    public SMTranslator() {
        varaibleMapping = new HashMap<>();
        idAtomMapping = new HashMap<>();
        idDecodingMapping = new HashMap<>();
        laodMatrices = new StringBuilder();
        baseMaticesMapping = new HashMap<>();
        sizeMatrices = new ArrayList<>();
        headAfter = null;
    }

    public void setRewriting(final ConjunctiveQuery rewriting) {
        this.rewriting = rewriting;
    }

    public void setRewriting(final ConjunctiveQuery rewriting, final String headAfter) {
        this.rewriting = rewriting;
        this.headAfter = headAfter;
    }

    /**
     * Translate the given rewriting into DML query
     * 
     * @return the translated rewriting
     */
    public String translate() {
        preProcess();
        do {
            for (iterator = idAtomMapping.entrySet().iterator(); iterator.hasNext();) {
                final Map.Entry<Term, Atom> entry = iterator.next();
                switch (entry.getValue().getPredicate()) {
                    case "tr":
                        processTranspose(entry.getValue());
                        break;
                    case "in":
                        processInverse(entry.getValue());
                        break;
                    case "ins":
                        processInverseScalar(entry.getValue());
                        break;
                    case "trace":
                        processTrace(entry.getValue());
                        break;
                    case "diag":
                        processDiag(entry.getValue());
                        break;
                    case "add":
                        processAdd(entry.getValue());
                        break;
                    case "multi":
                        processMulti(entry.getValue());
                        break;
                    case "det":
                        processDet(entry.getValue());
                        break;
                    case "add_s":
                        processAddS(entry.getValue());
                        break;
                    case "identityPrune":
                        procesPruneOut(entry.getValue());
                        break;
                    default:
                        break;
                }
            }

        } while (idAtomMapping.size() != 0);
        final Term head = rewriting.getHead().get(0);
        final StringBuilder str = new StringBuilder();
        processSize();
        if (laodMatrices.length() != 0) {
            str.append(laodMatrices);
        }
        if (headAfter == null)
            str.append(rewriting.getName());
        else {
            str.append(headAfter);
        }

        str.append("=");
        if (idDecodingMapping.isEmpty()) {
            str.append(head);
        } else {
            str.append(idDecodingMapping.get(head));
        }
        varaibleMapping.clear();
        idAtomMapping.clear();
        idDecodingMapping.clear();
        laodMatrices.setLength(0);
        baseMaticesMapping.clear();
        sizeMatrices.clear();
        headAfter = null;
        return str.toString();
    }

    /**
     * Pre-process the rewriting
     */
    private void preProcess() {
        for (final Atom atom : rewriting.getBody()) {
            final String atomPredicte = atom.getPredicate();
            if (atomPredicte.equals(Predicate.SIZE.toString())) {
                sizeMatrices.add(atom);
            } else {
                if (atomPredicte.equals(Predicate.NAME.toString())) {
                    varaibleMapping.put(atom.getTerm(0), atom.getTerm(0).toString());
                    baseMaticesMapping.put(atom.getTerm(0), atom.getTerm(1).toString());
                } else {
                    if (atom.getTerms().size() == 2) {
                        idAtomMapping.put(atom.getTerm(0), atom);
                    }
                    if (atom.getTerms().size() == 3) {
                        idAtomMapping.put(atom.getTerm(0), atom);
                        idAtomMapping.put(atom.getTerm(1), atom);
                    }
                }
            }
        }
    }

    /**
     * Process transpose operation
     * 
     * @param atom
     *            transpose atom
     */
    private void processTranspose(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(SMOperation.TRANS);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(SMOperation.TRANS);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process determinaint operation
     * 
     * @param atom
     *            determinaint atom
     */
    private void processDet(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingDet = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingDet.append(LParenthesis);
            decodingDet.append(SMOperation.DET);
            decodingDet.append(LParenthesis);
            decodingDet.append(varaibleMapping.get(term));
            decodingDet.append(RParenthesis);
            decodingDet.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingDet.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingDet.append(LParenthesis);
                decodingDet.append(SMOperation.DET);
                decodingDet.append(LParenthesis);
                decodingDet.append(idDecodingMapping.get(term));
                decodingDet.append(RParenthesis);
                decodingDet.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingDet.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process inverse operation
     * 
     * @param atom
     *            inverse atom
     */
    private void processInverse(final Atom atom) {
        if (atom.toString().equals("in(c_7,b_3)") || atom.toString().equals("in(b_3,c_7)")) {
            idDecodingMapping.put(atom.getTerm(1), "");
            iterator.remove();
        } else {
            final Term term = atom.getTerm(0);
            boolean status = false;
            final StringBuilder decodingInverse = new StringBuilder();
            if (varaibleMapping.containsKey(term)) {
                status = true;
                decodingInverse.append(LParenthesis);
                decodingInverse.append(SMOperation.INVERSE);
                decodingInverse.append(LParenthesis);
                decodingInverse.append(varaibleMapping.get(term));
                decodingInverse.append(RParenthesis);
                decodingInverse.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
            } else {
                if (idDecodingMapping.containsKey(term)) {
                    status = true;
                    decodingInverse.append(LParenthesis);
                    decodingInverse.append(SMOperation.INVERSE);
                    decodingInverse.append(LParenthesis);
                    decodingInverse.append(idDecodingMapping.get(term));
                    decodingInverse.append(RParenthesis);
                    decodingInverse.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
                }
            }
            if (status) {
                iterator.remove();
            }
        }
    }

    /**
     * Process inverse scalar
     * 
     * @param atom
     *            inverse scalar atom
     */
    private void processInverseScalar(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingInverse = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingInverse.append(LParenthesis);
            decodingInverse.append("1/");
            decodingInverse.append(LParenthesis);
            decodingInverse.append(varaibleMapping.get(term));
            decodingInverse.append(RParenthesis);
            decodingInverse.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;
                decodingInverse.append(LParenthesis);
                decodingInverse.append("1/");
                decodingInverse.append(LParenthesis);
                decodingInverse.append(idDecodingMapping.get(term));
                decodingInverse.append(RParenthesis);
                decodingInverse.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
            }
        }
        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process size operation
     * 
     * @param atom
     *            size atom
     */
    private void processSize() {

        for (Atom atom : sizeMatrices) {
            final Term term = atom.getTerm(0);
            final StringBuilder decodingSize = new StringBuilder();
            if (baseMaticesMapping.containsKey(term)) {
                decodingSize.append("matrix(");
                decodingSize.append("scan(");
                decodingSize.append(baseMaticesMapping.get(term));
                decodingSize.append(",what=numeric(), skip=1),");
                decodingSize.append("nrow=");
                decodingSize.append(atom.getTerm(1));
                decodingSize.append(",");
                decodingSize.append("ncol=");
                decodingSize.append(atom.getTerm(2));
                decodingSize.append(")");

            }
            laodMatrices.append(term.toString());
            laodMatrices.append("=");
            laodMatrices.append(decodingSize);
            laodMatrices.append("\n");
        }
    }

    /**
     * Process trace operation
     * 
     * @param atom
     *            trace atom
     */
    private void processTrace(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(SMOperation.TRACE);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(SMOperation.TRACE);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();

        }
    }

    /**
     * Process procesPruneOut
     * 
     * @param atom
     *            tprocesPruneOut
     */
    private void procesPruneOut(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        if (varaibleMapping.containsKey(term)) {

            idDecodingMapping.put(atom.getTerm(1), atom.getTerm(0).toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;
                idDecodingMapping.put(atom.getTerm(1), atom.getTerm(0).toString());
            }
        }

        if (status) {
            iterator.remove();

        }
    }

    /**
     * Process diag operation
     * 
     * @param atom
     *            diag atom
     */
    private void processDiag(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(SMOperation.DIAG);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(SMOperation.DIAG);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processAdd(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(SMOperation.ADD);
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(SMOperation.ADD);
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(SMOperation.ADD);
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(SMOperation.ADD);
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processAddS(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(SMOperation.ADDS);
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(SMOperation.ADDS);
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(SMOperation.ADDS);
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(SMOperation.ADDS);
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processMulti(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(SMOperation.MULTI);
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(SMOperation.MULTI);
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(SMOperation.MULTI);
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(SMOperation.MULTI);
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }
}
