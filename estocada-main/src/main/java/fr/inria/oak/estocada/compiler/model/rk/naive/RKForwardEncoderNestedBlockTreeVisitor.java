package fr.inria.oak.estocada.compiler.model.rk.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Tgd;
import fr.inria.oak.estocada.compiler.ChildBlock;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeVisitor;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.RootBlock;
import fr.inria.oak.estocada.compiler.VariableCopier;
import fr.inria.oak.estocada.compiler.model.rk.RKExtractVariableToCreatedNodeVisitor;
import fr.inria.oak.estocada.compiler.model.rk.Utils;
import fr.inria.oak.estocada.rewriter.Comment;

@Singleton
class RKForwardEncoderNestedBlockTreeVisitor implements QueryBlockTreeVisitor {
    private final RKExtractVariableToCreatedNodeVisitor rKExtractVariableToCreatedNodeVisitor;
    private Variable viewSetID;
    private Variable internalMapID;
    private ImmutableList.Builder<Constraint> builder;
    private boolean includeComments;

    @Inject
    public RKForwardEncoderNestedBlockTreeVisitor(final VariableCopier variableCopier,
            final RKExtractVariableToCreatedNodeVisitor rKExtractVariableToCreatedNodeVisitor) {
        this.rKExtractVariableToCreatedNodeVisitor = rKExtractVariableToCreatedNodeVisitor;
        this.viewSetID = null;
        this.internalMapID = null;
    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, boolean includeComments,
            final Variable viewSetID, final Variable internalMapID) {
        this.viewSetID = viewSetID;
        this.internalMapID = internalMapID;
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(ChildBlock block) {
        // No Implementation Since There is no Child Block

    }

    @Override
    public void visit(final RootBlock block) {

        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
            }
            builder.add(getForwardConstraintForBodyEncoding(block, internalMapID));
        }
    }

    private Constraint getForwardConstraintForBodyEncoding(final RootBlock block, final Variable InternalMapID) {
        final String queryName = block.getQueryName();
        final List<Atom> premise = new ArrayList<Atom>();
        Atom mapConclusion = null;
        for (Atom atom : block.getPattern().getStructural().encoding()) {
            if (atom.getPredicate().contains("mainmap")) {
                mapConclusion = atom;
                break;
            }
        }
        premise.addAll(block.getPattern().encoding(Utils.conditionEncoding));
        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.addAll(rKExtractVariableToCreatedNodeVisitor.encode(block.getReturnTemplate(), queryName));
        conclusion.addAll(rKExtractVariableToCreatedNodeVisitor.encode(mapConclusion));
        conclusion.add(Utils.getCollectionAtom(block, viewSetID));
        if (block.getReturnTemplate().getTerms().get(0) instanceof ReturnConstructTerm) {
            conclusion.add(Utils.getInternalMapAtom(block, internalMapID));
            //conclusion.addAll(Utils.getCollectionAtomMap(block, InternalMapID));
        }
        return new Tgd(premise, conclusion);
    }

}