package fr.inria.oak.estocada.rewriting.decoder.pr;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Constant;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.exceptions.QueryTranslationException;
import fr.inria.oak.estocada.rewriting.logical.Semantics;
import fr.inria.oak.estocada.utils.Constants;

/**
 * Translates conjunctive queries into SQL.
 *
 * To be correctly translated, the queries must have the following properties:
 * - query atom predicates must match a table name
 * - query atom terms are matched to relation attributes by position
 *
 * @author Romain Primet
 */
public class PRTranslatorNoType {
    private final Semantics semantics;
    private final Catalog catalog;
    private final NRSMD nrsmd;

    /**
     * Creates a SQLTranslator.
     *
     * @param catalog
     *            - the tatooine catalog to use for output queries
     * @param semantics
     *            - bag or set semantics
     */
    public PRTranslatorNoType(final NRSMD nrsmd, final Catalog catalog, final Semantics semantics) {
        this.catalog = catalog;
        this.semantics = semantics;
        this.nrsmd = nrsmd;
    }

    protected boolean useSetSemantics() {
        return semantics == Semantics.SET;
    }

    /**
     * Translates a conjunctive query into SQL.
     * 
     * @param query
     *            - the conjunctive query to translate.
     * @param schema
     *            - a schema that matches the underlying DB.
     * @return a SQL request serialized into a String.
     */
    public String translate(ConjunctiveQuery query) throws QueryTranslationException {
        final StringBuilder builder = new StringBuilder();
        final List<Term> head = query.getHead();
        final Collection<Atom> body = query.getBody();

        builder.append(generateSelectClause(head, body));
        builder.append(generateFromClause(body));
        builder.append(generateWhereClause(query));

        return builder.toString();
    }

    private String generateSelectClause(final List<Term> head, final Collection<Atom> body)
            throws QueryTranslationException {
        if (head.isEmpty()) {
            throw new QueryTranslationException("Input query must have at least one head variable/constant");
        }

        final StringBuilder result = new StringBuilder(Constants.SELECT);
        if (this.useSetSemantics()) {
            result.append(Constants.DISTINCT);

        }

        for (int i = 0; i < head.size(); i++) {
            final Term term = head.get(i);
            if (term.isConstant()) {
                result.append(Constants.QUOTE).append(((Constant) term).getValue()).append(Constants.QUOTE);
            } else {
                final Variable headVar = (Variable) term;
                final Map.Entry<Optional<String>, Atom> columnAtom = body.stream()
                        .map(atom -> new AbstractMap.SimpleImmutableEntry<Optional<String>, Atom>(
                                columnNameFor(headVar, atom), atom))
                        .filter(entry -> entry.getKey().isPresent()).findFirst()
                        .orElseThrow(() -> new QueryTranslationException(
                                "Head variable " + headVar + " should have a match within the query body"));
                result.append(nrsmd.getColNames()[i]);
            }
            if (i == (head.size() - 1)) {
                result.append(Constants.SPACE);
            } else {
                result.append(Constants.COMMA);

            }
        }

        return result.toString();
    }

    private String generateFromClause(Collection<Atom> body) {
        final StringBuilder result = new StringBuilder(Constants.FROM);
        final Iterator<Atom> it = body.iterator();
        while (it.hasNext()) {
            final Atom atom = it.next();
            result.append(atom.getPredicate());
            result.append(it.hasNext() ? Constants.COMMA : Constants.SPACE);
        }
        return result.toString();
    }

    private String generateWhereClause(ConjunctiveQuery query) {
        final Map<Variable, Set<Atom>> clusters = query.getClusters();
        final Iterator<Entry<Variable, Set<Atom>>> eqClusters =
                clusters.entrySet().stream().filter(entry -> hasEqualityConstraint(entry.getValue())).iterator();

        final Iterator<AbstractMap.SimpleImmutableEntry<Constant, Atom>> constantTuples =
                query.getBody().stream().flatMap(atom -> {
                    return atom.getTerms().stream()
                            .map(term -> new AbstractMap.SimpleImmutableEntry<Term, Atom>(term, atom));
                }).filter(entry -> entry.getKey().isConstant())
                        .map(entry -> new AbstractMap.SimpleImmutableEntry<Constant, Atom>((Constant) entry.getKey(),
                                entry.getValue()))
                        .iterator();

        if ((!eqClusters.hasNext()) && (!constantTuples.hasNext())) {
            return "";
        }

        final StringBuilder result = new StringBuilder(Constants.WHERE);

        // Join conditions
        while (eqClusters.hasNext()) {
            final Entry<Variable, Set<Atom>> cluster = eqClusters.next();
            final Variable var = cluster.getKey();
            final Set<Atom> atoms = cluster.getValue();
            final Iterator<Atom> it = atoms.iterator();
            final Atom first = it.next();
            final String firstColumnName = columnNameForEx(var, first);

            while (it.hasNext()) {
                final Atom atom = it.next();
                final String secondColumnName = columnNameForEx(var, atom);
                result.append(firstColumnName);
                result.append(Constants.EQUALS);
                result.append(secondColumnName);
                result.append(Constants.SPACE);
                result.append(it.hasNext() ? Constants.AND : Constants.SPACE);
            }
            result.append((eqClusters.hasNext() || constantTuples.hasNext()) ? Constants.AND : Constants.SPACE);
        }

        while (constantTuples.hasNext()) {
            final Entry<Constant, Atom> entry = constantTuples.next();
            result.append(columnNameForEx(entry.getKey(), entry.getValue()));
            result.append(Constants.EQUALS);
            result.append(Constants.QUOTE).append(entry.getKey().getValue()).append(Constants.QUOTE);
            result.append(constantTuples.hasNext() ? Constants.AND : Constants.SPACE);
        }

        return result.toString();
    }

    /**
     * A cluster has an equality constraint if its set of atoms is
     * of size 2 or more (i.e. the Variable head of the cluster will be found
     * in two separate places at least).
     */
    private boolean hasEqualityConstraint(Set<Atom> clusterAtoms) {
        return clusterAtoms.size() >= 2;
    }

    //TODO: explain why we need table aliases relative to each atom.
    private String makeTableAlias(Atom atom) {
        return "t_" + atom.getId();
    }

    /**
     * Matches a term within an atom to a column name by position.
     */
    private Optional<String> columnNameFor(Term term, Atom atom) {
        int termIdx = 0;
        final String tableName = atom.getPredicate();
        final Map<Integer, String> mapping = catalog.getViewSchema(tableName).getReverseColNameToIndexMapping();
        System.out.println(mapping);
        for (Term atomTerm : atom.getTerms()) {
            if (term.equals(atomTerm)) {
                return Optional.of(mapping.get(termIdx));
            }
            termIdx++;
        }
        return Optional.empty();
    }

    private String columnNameForEx(Term term, Atom atom) throws QueryTranslationException {
        return columnNameFor(term, atom)
                .orElseThrow(() -> new QueryTranslationException("No match for term " + term + "in atom " + atom));
    }

}
