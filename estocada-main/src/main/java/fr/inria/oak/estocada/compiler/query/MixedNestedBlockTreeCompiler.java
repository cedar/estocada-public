package fr.inria.oak.estocada.compiler.query;

import java.util.LinkedHashSet;
import java.util.Set;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.Pattern;
import fr.inria.oak.estocada.compiler.QueryBlockTreeQueryCompiler;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTerm;
import fr.inria.oak.estocada.compiler.exceptions.CompilationException;

/**
 * Mixed nested block query compiler
 * 
 * @author Rana Alotaibi
 */
public final class MixedNestedBlockTreeCompiler implements QueryBlockTreeQueryCompiler {
    @Override
    public Set<? extends Atom> compilePattern(Pattern nbtPattern) throws CompilationException {

        final Set<Atom> queryBody = new LinkedHashSet<Atom>();
        for (final PathExpression pathExpression : nbtPattern.getStructural().getPathExpressions()) {
            queryBody.addAll(pathExpression.encoding());
        }
        return queryBody;
    }

    @Override
    public Set<? extends Term> compileReturnTemplate(ReturnTemplate nbtReturnTemplate) throws CompilationException {
        final Set<Term> queryHead = new LinkedHashSet<Term>();
        for (final ReturnTerm returnTerm : nbtReturnTemplate.getTerms()) {
            queryHead.addAll(returnTerm.getReferredVariables());
        }
        return queryHead;
    }
}