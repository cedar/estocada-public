package fr.inria.oak.estocada.compiler.model.pr;

import fr.inria.oak.estocada.compiler.Format;
import fr.inria.oak.estocada.compiler.Language;
import fr.inria.oak.estocada.compiler.Model;

public class PRModel extends Model {
    public final static String ID = "PR";

    public PRModel() {
        super(ID, Format.RELATIONAL, Language.CONJUNCTIVE_QUERY, new PRQueryBlockTreeBuilder(), new PRBlockEncoder());
    }
}
