package fr.inria.oak.estocada.compiler;

/**
 * Part of query block tree data structure.
 *
 * Represents the supported languages.
 * 
 * @author ranaalotaibi
 * @author Damian Bursztyn
 * 
 */
public enum Language {
    CONJUNCTIVE_QUERY,
    XQUERY,
    AQL,
    KQL,
    SQLPlusPlus,
    TQL,
    CYPHER,
    PJSQL,
    SJQL,
    QBT,
    BIGDAWG,
    DML,
    MADLIB,
    TFM;
}
