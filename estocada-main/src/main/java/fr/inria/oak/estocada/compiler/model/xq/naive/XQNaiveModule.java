package fr.inria.oak.estocada.compiler.model.xq.naive;

import fr.inria.oak.estocada.compiler.model.xq.XQueryModule;

public class XQNaiveModule extends XQueryModule {
	@Override
	protected String getPropertiesFileName() {
		return "xq.naive.properties";
	}
}
