package fr.inria.oak.estocada.compiler.model.xq.alternative.firststep;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.model.xq.Predicate;

@Singleton
class BlockForwardTagsEncoderReturnTermVisitor implements
		ReturnTemplateVisitor {
	private ImmutableList.Builder<Atom> builder;
	private String viewName;

	public List<Atom> encode(final ReturnTemplate template,
			final String viewName) {
		builder = ImmutableList.builder();
		this.viewName = viewName;
		template.accept(this);
		return builder.build();
	}

	@Override
	public void visit(final ReturnTemplate template) {
		// NOP (no encoding for the template optionals)
	}

	@Override
	public void visitPost(final ReturnConstructTerm term) {
		// NOP (no encoding after the children are processed)
	}

	@Override
	public void visitPre(final ReturnConstructTerm term) {
		builder.add(new Atom(Predicate.TAG.toString() + "_" + viewName, term
				.getCreatedNode(), term.getElement().toTerm()));
	}

	@Override
	public void visit(final ReturnVariableTerm term) {

	}

	@Override
	public void visit(final ReturnStringTerm term) {

	}
}