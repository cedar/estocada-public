package fr.inria.oak.estocada.views.materialization.module.SPPJ;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import fr.inria.oak.estocada.views.materialization.module.QBTViewListener;

public class Test {
    private static final String INPUT_QUERY_FILE = "src/main/resources/testQBT/v.view";
    private static final Logger LOGGER = Logger.getLogger(Test.class);

    public static void main(String[] args) throws Exception {
        RegisterMetadata.RegisterDataset1();
        final QBTViewListener qbtViewsMigrator = new QBTViewListener();
        qbtViewsMigrator.run(FileUtils.readFileToString(new File(INPUT_QUERY_FILE)));
    }
}
