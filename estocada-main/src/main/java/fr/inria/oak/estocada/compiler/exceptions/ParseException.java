package fr.inria.oak.estocada.compiler.exceptions;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Used when an exception occurs on runtime during query syntax parsing.
 *
 * @author Rana Alotaibi
 * @author Damian Bursztyn
 */
public final class ParseException extends EstocadaException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new parse exception with the specified illegal state
     * exception.
     *
     * @param msg
     *            the detail message.
     */
    public ParseException(Exception e) {
        super(checkNotNull(e));
    }
}
