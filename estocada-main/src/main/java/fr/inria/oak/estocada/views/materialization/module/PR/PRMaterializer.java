package fr.inria.oak.estocada.views.materialization.module.PR;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.oak.estocada.views.materialization.module.Materializer;
import fr.inria.oak.estocada.views.materialization.module.MaterializerException;
import fr.inria.oak.estocada.views.materialization.module.blocks.QBTPattern;

/**
 * PR Materializer.
 * 
 * @author ranaalotaibi
 *
 */
public final class PRMaterializer implements Materializer {

    private static final Logger LOGGER = Logger.getLogger(PRMaterializer.class);
    private final String viewName;
    private final List<String> selectedVariables;
    private final QBTPattern qbtPattern;
    private final StorageReference gsr;
    private Connection connection;

    /** Constructor **/
    public PRMaterializer(final String viewName, final List<String> selectedVariables, final QBTPattern qbtPattern,
            final StorageReference gsr) throws MaterializerException {
        this.selectedVariables = checkNotNull(selectedVariables);
        this.qbtPattern = checkNotNull(qbtPattern);
        this.viewName = checkNotNull(viewName);
        this.gsr = checkNotNull(gsr);
        try {
            connect();
        } catch (MaterializerException exception) {
            throw exception;
        }
    }

    @Override
    public void materialize() throws MaterializerException {
        try {
            createTargetTableSchema();
            executePhyEval();
            LOGGER.debug("View materialized successfully.");
        } catch (MaterializerException exception) {
            LOGGER.error("View materialization faild :" + exception.getMessage());
            throw exception;

        }
    }

    /**
     * Create the target schema of the view.
     * 
     * @throws MaterializerException
     */
    private void createTargetTableSchema() throws MaterializerException {
        try {
            final NRSMD queryNRSMD = qbtPattern.getBlockNRSMD();
            executeSQLStatement(prePareTargetSchema(queryNRSMD));
        } catch (MaterializerException excpetion) {
            throw excpetion;
        }
    }

    /**
     * Prepare the target schema.
     * 
     * @param queryNRSMD.
     *            The NRSMD of the physical operator.
     * @return The prepared target schema.
     * @throws MaterializerException
     */
    private String prePareTargetSchema(final NRSMD queryNRSMD) throws MaterializerException {
        final StringBuilder targetSchema = new StringBuilder();
        targetSchema.append("DROP TABLE IF EXISTS " + viewName + ";");
        targetSchema.append("CREATE TABLE " + viewName + "(");
        final String[] colNames = queryNRSMD.getColNames();
        final TupleMetadataType[] tupleMetadataType = queryNRSMD.getColumnsMetadata();
        int count = 0;
        for (final String projectedVariableName : selectedVariables) {
            for (int i = 0; i < colNames.length; i++) {
                if (projectedVariableName.equals(colNames[i])) {
                    if (tupleMetadataType[i].equals(TupleMetadataType.STRING_TYPE)) {
                        targetSchema.append(projectedVariableName + " " + PRDataType.VARCHAR);
                    } else {
                        if (tupleMetadataType[i].equals(TupleMetadataType.INTEGER_TYPE)) {
                            targetSchema.append(projectedVariableName + " " + PRDataType.INTEGER);
                        } else {
                            throw new MaterializerException(new IllegalStateException(
                                    "The data to be materlized in Postgres relational must be scalar."));
                        }
                    }
                }
            }
            count++;
            if (count != (selectedVariables.size())) {
                targetSchema.append(",");
            }
        }
        targetSchema.append(");");
        LOGGER.info("Target Schema :" + targetSchema.toString());
        return targetSchema.toString();
    }

    /**
     * Insert record into PostgreSQL.
     * 
     * @param nrsmd
     *            the NRSMD of the tuple to be inserted.
     * @param nTuple
     *            the tuple values to be inserted.
     * @throws MaterializerException
     */
    private void insertRecord(final NRSMD nrsmd, final NTuple nTuple) throws MaterializerException {
        try {
            final TupleMetadataType[] tupleMetadataType = nrsmd.getColumnsMetadata();
            final StringBuilder insertStatement = new StringBuilder();
            insertStatement.append("INSERT INTO " + viewName + " VALUES (");
            int count = 0;
            for (String projectedVariableName : selectedVariables) {
                int colInx = nrsmd.getColIndexFromName(projectedVariableName);
                if (tupleMetadataType[colInx].equals(TupleMetadataType.STRING_TYPE)) {
                    final char[] column = nTuple.getStringField(colInx);
                    final String stringValue = String.copyValueOf(column);
                    insertStatement.append("'" + stringValue + "'");
                }
                if (tupleMetadataType[colInx].equals(TupleMetadataType.INTEGER_TYPE)) {
                    final int column = nTuple.getIntegerField(colInx);
                    insertStatement.append(column);
                }
                if (++count < (selectedVariables.size())) {
                    insertStatement.append(",");
                }
            }
            insertStatement.append(");");
            executeSQLStatement(insertStatement.toString());
        } catch (TatooineExecutionException e) {
            LOGGER.error("Cannot insert to postgres" + e.getMessage());
            throw new MaterializerException(e);
        }
    }

    /**
     * Execute SQL statement (CREATE table and INSERT statement)
     * 
     * @param stringStatement
     * @throws SQLException
     * @throws MaterializerException
     */
    private void executeSQLStatement(final String stringStatement) throws MaterializerException {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            LOGGER.debug("Statement to be executed in Postgres: " + stringStatement);
            statement.executeUpdate(stringStatement);
            statement.close();
        } catch (SQLException exepction) {
            LOGGER.error(exepction.getMessage());
            throw new MaterializerException(exepction);
        }
    }

    /**
     * Create PostgreSQL Connection.
     * 
     * @throws MaterializerException
     */
    private void connect() throws MaterializerException {

        try {
            connection = DriverManager.getConnection(gsr.getPropertyValue("url"));
            connection.setReadOnly(false);
        } catch (SQLException exepction) {
            LOGGER.error("Connection to Postgres falid" + exepction.getMessage());
            throw new MaterializerException(exepction);
        }
    }

    /**
     * Execute the QBT pattern operator.
     * 
     * @param phyEval
     *            the physical operator.
     * @throws TatooineExecutionException
     */
    private void executePhyEval() throws MaterializerException {
        try {
            final NIterator phyEval = qbtPattern.getQBTBlockPhyEval();
            phyEval.open();
            while (phyEval.hasNext()) {
                final NTuple next = phyEval.next();
                insertRecord(qbtPattern.getBlockNRSMD(), next);
            }
            phyEval.close();
        } catch (TatooineExecutionException | MaterializerException exception) {
            throw new MaterializerException(exception);
        }

    }

}
