package fr.inria.oak.estocada.compiler.model.qbt;

public class QBTNaiveModule extends QBTModule {
    @Override
    protected String getPropertiesFileName() {
        return "qbt.properties";
    }
}
