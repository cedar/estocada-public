package fr.inria.oak.estocada.stores.executores;

import java.util.Iterator;

import org.apache.log4j.Logger;
import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;

/**
 * MongoDB Query Executor
 * 
 * @author Rana Alotaibi
 *
 */
public class MongoDBExecutor implements ExecutorInterface {

    private static final Logger LOGGER = Logger.getLogger(MongoDBExecutor.class);
    private MongoClient mongoClient;
    private MongoDatabase database;

    @Override
    public void createConnection() {

        mongoClient = new MongoClient("localhost", 27017);
        database = mongoClient.getDatabase("db");
    }

    @Override
    public long executeQuery(String nativeQuery) {

        long queryTime = 0;
        boolean plan = false;
        final long start = System.currentTimeMillis();
        final BasicDBObject basicDB = BasicDBObject.parse(nativeQuery);
        Document result = null;
        try {
            result = database.runCommand(basicDB);

        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }
        final long end = System.currentTimeMillis();
        LOGGER.info(result.toString());

        //        Gson gson = new GsonBuilder().setPrettyPrinting().setLenient().create();
        //        JsonParser jp = new JsonParser();
        //        JsonElement je = jp.parse(result.toString());
        //        String prettyJsonString = gson.toJson(je);
        //        LOGGER.info(prettyJsonString);
        queryTime += ((end - start));
        LOGGER.info("**Finished Executing**" + queryTime);
        Document cursor = (Document) result.get("cursor");
        LOGGER.info(cursor.toString());

        //        cursor.append("batchSize", 1000000);
        //        queryTime += getResult(cursor, true);
        //        while (cursor != null && cursor.getLong("id") > 0) {
        //            result = getNextBatch(cursor);
        //            cursor = (BasicDBObject) result.get("cursor");
        //            queryTime += getResult(cursor, false);
        //        }
        return queryTime;
    }

    /**
     * Get the query result in batch mood
     * 
     * @param cursor
     * @param firs
     */
    private long getResult(Document cursor, boolean first) {
        if (cursor == null) {
            LOGGER.info("Cursor is null");
        }
        final long start = System.currentTimeMillis();
        final BasicDBList batch =
                first ? (BasicDBList) cursor.get("firstBatch") : (BasicDBList) cursor.get("nextBatch");
        final Iterator<Object> iterator = batch.iterator();
        while (iterator.hasNext()) {
            LOGGER.info(iterator.next().toString());
            iterator.next();
        }
        final long end = System.currentTimeMillis();
        return (((end - start)));
    }

    /**
     * Get next batch of the result
     * 
     * @param cursor
     * @return
     */
    private Document getNextBatch(final BasicDBObject cursor) {
        BasicDBObject getMore = new BasicDBObject();
        getMore.append("getMore", cursor.getLong("id"));
        getMore.append("collection", cursor.getString("ns").split("\\.")[1]);
        return database.runCommand(getMore);
    }

    /**
     * Create indexes
     */
    public void createIndexes() {
        final MongoDatabase database = mongoClient.getDatabase("db");
        final MongoCollection<Document> collection = database.getCollection("mimic");
        IndexOptions indexOptions = new IndexOptions().languageOverride("english");
        collection.createIndex(Indexes.text("ADMISSIONS.noteevents_0.text"), indexOptions);
        //collection.createIndex(Indexes.text("ADMISSIONS"), indexOptions);
        LOGGER.info("Text Index Created");
        //        collection.createIndex(Indexes.descending("ADMISSIONS.labevents.flag"));
        //        LOGGER.info("Flag Index Created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.icustays.prescriptions.drug_type"));
        //        LOGGER.info("Drug_type Index created");
        //        collection.createIndex(Indexes.ascending("ITEMID"));
        //        LOGGER.info("ITEMID Index created");
        //        collection.createIndex(Indexes.ascending("LABEL"));
        //        LOGGER.info("LABEL Index created");
        //        collection.createIndex(Indexes.ascending("CATEGORY"));
        //        LOGGER.info("CATEGORY Index created");
        //        collection.createIndex(Indexes.ascending("FLUID"));
        //        LOGGER.info("FLUID Index created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.services.curr_service"));
        //        LOGGER.info("Curr_service Index Created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.microbiologyevents.ab_name"));
        //        LOGGER.info("Ab_name Index Created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.icustays.prescriptions.drug"));
        //        LOGGER.info("Drug Index Created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.procedureevents_mv.ordercategoryname"));
        //        LOGGER.info("Ordercategoryname Index Created");
        //        collection.createIndex(Indexes.ascending("ADMISSIONS.microbiologyevents.interpretation"));
        //        LOGGER.debug("Interpretation Index Created");
    }
}
