package fr.inria.oak.estocada.extension.BigDAWG;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.apache.accumulo.core.data.Range;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import istc.bigdawg.exceptions.MigrationException;
import istc.bigdawg.migration.FromDatabaseToDatabase;
import istc.bigdawg.migration.MigrationResult;
import istc.bigdawg.utils.ListConncatenator;

/**
 * PostgresRQ to PostgresJQ Migrator
 * 
 * @author Rana Alotaibi
 */
public class BigDAWGFromPostgresRQToPostgresJQMigrator extends FromDatabaseToDatabase {

    private static final long serialVersionUID = -3329851490314835580L;

    private static Logger logger = Logger.getLogger(BigDAWGFromPostgresRQToPostgresJQMigrator.class);

    private Connection connectionPostgresJQ = null;
    private Connection connectionPostgresRQ = null;

    private ResultSetMetaData rsmetaData;
    private PreparedStatement stJQ = null;
    private ResultSet rsJQ = null;
    private PreparedStatement stRQ = null;
    private ResultSet rsRQ = null;

    // parameters
    private int postgreSQLWritebatchSize = 1000;
    private int postgreSQLReaderCharSize = 1000000;
    private char delimiter = '|';

    public BigDAWGFromPostgresRQToPostgresJQMigrator() {

    }

    public BigDAWGFromPostgresRQToPostgresJQMigrator(Connection connectionPostgresRQ, Connection connectionPostgresJQ) {
        this.connectionPostgresJQ = connectionPostgresJQ;
        this.connectionPostgresRQ = connectionPostgresRQ;
    }

    private void cleanPostgreSQLResources() throws SQLException {
        if (rsRQ != null) {
            rsRQ.close();
        }
        if (stRQ != null) {
            stRQ.close();
        }
        if (connectionPostgresRQ != null) {
            connectionPostgresRQ.close();
        }

        if (rsJQ != null) {
            rsJQ.close();
        }
        if (stJQ != null) {
            stJQ.close();
        }
        if (connectionPostgresJQ != null) {
            connectionPostgresJQ.close();
        }
    }

    private ResultSetMetaData getMetaData(final String tableName) throws SQLException {
        String query = "Select * from " + tableName.replace(";", "").replace(" ", "") + " limit 1";
        logger.debug("Query to get meta data: " + query);
        stRQ = connectionPostgresRQ.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        rsRQ = stRQ.executeQuery();
        if (rsRQ == null) {
            return null;
        }
        return rsRQ.getMetaData();
    }

    public void createNewRowForPostgres(String[] row, StringBuilder sBuilder) {
        String rowString = ListConncatenator.joinList(row, delimiter, "\n");
        sBuilder.append(rowString);
    }

    public void flushRowsToPostgreSQL(StringBuilder sBuilder, PushbackReader reader, CopyManager cpManager,
            String postgresTable, String copyString) throws IOException, SQLException {
        reader.unread(sBuilder.toString().toCharArray());
        cpManager.copyIn(copyString.toString(), reader);
        sBuilder.delete(0, sBuilder.length());
    }

    public MigrationResult fromPostgresRQToPostgresJQ(final String postgresRQQuery, final String postgresTable,
            Range accumuloRange) throws SQLException, MigrationException {
        logger.debug("Migrate data from PostgresRQ to PostgresJQ.");

        long startTimeMigration = System.currentTimeMillis();
        long postgresJQCounter = 0;
        long postgresRQCounter = 0;
        StringBuilder copyStringBuf = new StringBuilder();
        copyStringBuf.append("COPY ");
        copyStringBuf.append(postgresTable);
        copyStringBuf.append(" FROM STDIN WITH (DELIMITER '");
        copyStringBuf.append(delimiter);
        copyStringBuf.append("')");
        String copyString = copyStringBuf.toString();
        try {
            ResultSetMetaData rsmd = getMetaData(postgresTable);
            if (rsmd == null) {
                String message = "There is no table: " + postgresTable;
                logger.log(Level.INFO, message);
            } else {
                int numOfCol = rsmd.getColumnCount();
                Map<String, Integer> mapNameCol = new HashMap<>();
                for (int i = 0; i < numOfCol; ++i) {
                    String columnName = rsmd.getColumnName(i + 1);
                    logger.debug("Column name: " + columnName);
                    mapNameCol.put(columnName, i);
                }
                StringBuilder sBuilder = new StringBuilder();
                CopyManager cpManager = new CopyManager((BaseConnection) this.connectionPostgresJQ);
                PushbackReader reader = new PushbackReader(new StringReader(""), postgreSQLReaderCharSize);

                stRQ = connectionPostgresRQ.prepareStatement(postgresRQQuery);
                rsRQ = stRQ.executeQuery();
                rsmetaData = rsRQ.getMetaData();
                int columnCount = rsmetaData.getColumnCount();
                String[] values = new String[columnCount];
                try {
                    while (rsRQ.next()) {
                        for (int i = 0; i < columnCount; i++) {
                            Integer index = mapNameCol.get(rsmetaData.getColumnName(i + 1).toString().toLowerCase());
                            if (rsmetaData.getColumnTypeName(i + 1).contains("in")) {
                                values[index] = Integer
                                        .toString((Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1)) == null ? 0
                                                : (Integer) rsRQ.getObject(rsmetaData.getColumnName(i + 1)));
                            } else {
                                if (rsmetaData.getColumnTypeName(i + 1).contains("timestamp")) {
                                    values[index] = (String) rsRQ.getString(rsmetaData.getColumnName(i + 1));
                                }
                            }

                        }
                        ++postgresJQCounter;
                        ++postgresRQCounter;
                        createNewRowForPostgres(values, sBuilder);
                        if (postgresJQCounter % postgreSQLWritebatchSize == 0) {
                            flushRowsToPostgreSQL(sBuilder, reader, cpManager, postgresTable, copyString);
                        }
                    }

                    flushRowsToPostgreSQL(sBuilder, reader, cpManager, postgresTable, copyString);
                    final String indexCreation = "CREATE INDEX ON " + postgresTable + "("
                            + rsmetaData.getColumnName(1).toString().toLowerCase() + ");";
                    final Statement statement = connectionPostgresJQ.createStatement();
                    statement.executeUpdate(indexCreation);
                    statement.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            cleanPostgreSQLResources();
        }
        long endTimeMigration = System.currentTimeMillis();
        long durationMsec = endTimeMigration - startTimeMigration;
        return new MigrationResult(postgresJQCounter, postgresRQCounter, startTimeMigration, endTimeMigration,
                durationMsec);

    }

    public MigrationResult fromPostgresRQToPostgresJQ(final String postgresRQQuery, final String postgresTable)
            throws MigrationException, SQLException {
        return fromPostgresRQToPostgresJQ(postgresRQQuery, postgresTable, null);
    }
}
