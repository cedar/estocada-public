package fr.inria.oak.estocada.rewriting.logical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.operators.logical.LogJoin;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogProjection;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.Predicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import fr.inria.oak.commons.conjunctivequery.Term;

/**
 * This class represents a decoder node in a logical plan tree.
 * 
 * @author ranaalotaibi
 *
 */
public class DecoderNode {
    private static final Logger LOGGER = Logger.getLogger(RewritingToLogicalPlanGenerator.class);
    final List<Term> headVariables;
    LogOperator logOperator;

    /** Constructor **/
    public DecoderNode(final List<Term> headVariables, final LogOperator logOperator) {
        this.headVariables = headVariables;
        this.logOperator = logOperator;
    }

    /**
     * Get the logical operator
     * 
     * @return This function returns the logical operator
     */
    public LogOperator getLogOperator() {

        return logOperator;
    }

    /**
     * Get the operator head variables
     * 
     * @return This function returns the operator head variables.
     */
    public List<Term> getHeadVariables() {
        return headVariables;
    }

    /**
     * Check if the current decoder node can be joined with the other node.
     * 
     * @param other
     * @return
     */
    boolean canJoinWith(DecoderNode other) {
        return !(intersection(other).isEmpty());
    }

    /**
     * Find the intersection in the head variables
     * 
     * @param other
     *            the other decoder node to join with
     * @return This functions return the join variables
     */
    private Set<Term> intersection(DecoderNode other) {
        Set<Term> intersection = new HashSet<Term>(other.headVariables);
        intersection.retainAll(this.headVariables);
        return intersection;
    }

    /**
     * Join the current operate with other operator
     * 
     * @param other
     */
    public void joinWith(DecoderNode other) {
        LOGGER.debug("Joining " + this.logOperator.getName() + " with " + other.logOperator.getName());
        final Set<Term> intersection = intersection(other);
        if (intersection.isEmpty()) {
            throw new IllegalArgumentException("Cannot join node " + this + " with node " + other + ".");
        }
        final Map<Term, Integer> leftIndices = indicesIn(this.headVariables, intersection);
        final Map<Term, Integer> rightIndices = indicesIn(other.headVariables, intersection);

        final List<SimplePredicate> preds = new ArrayList<>();
        for (final Term variable : leftIndices.keySet()) {
            LOGGER.debug("Generating join predicate on variable " + variable);
            preds.add(new SimplePredicate((int) leftIndices.get(variable),
                    (int) rightIndices.get(variable) + this.headVariables.size()));
        }

        final Predicate joinPredicate = new ConjunctivePredicate(preds.toArray(new SimplePredicate[0]));

        final LogOperator joinOp = new LogJoin(this.logOperator, other.logOperator, joinPredicate);
        this.logOperator = joinOp;
        headVariables.addAll(other.headVariables);
    }

    /**
     * Make root operator as projection operator.
     * 
     * @param variables
     *            the projected variables.
     */
    public void project(final Set<Term> variables) {
        if (!this.headVariables.containsAll(variables)) {
            throw new IllegalArgumentException(
                    "Projection must be done on a subset of the exported variables " + headVariables);
        }
        int[] indices = indicesIn(this.headVariables, variables).values().stream().mapToInt(i -> i).toArray();
        LogOperator newOp = new LogProjection(this.logOperator, indices);
        this.logOperator = newOp;
        headVariables.retainAll(variables);
    }

    /**
     * Find the index of the join terms.
     * 
     * @param variables
     *            the operator head variables
     * @param joinTerms
     *            the join terms
     * @return This function returns the index mapping
     */
    static Map<Term, Integer> indicesIn(final List<Term> variables, final Set<Term> joinTerms) {
        final Map<Term, Integer> retval = new HashMap<>();
        for (Term variable : joinTerms) {
            int index = variables.indexOf(variable);
            if (index == -1) {
                throw new IllegalArgumentException("Cannot find variable " + variable + " in " + variables + ".");
            }
            retval.put(variable, index);
        }
        return retval;
    }
}
