package fr.inria.oak.estocada.rewriting.decoder.pr;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Constant;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;

/**
 * Simplifies  a conjunctive query by transforming it into an equivalent without eq_rq predicates.
 * @author Romain Primet
 */
public class EqSimplifier {

  /**
   * Simplifies a conjunctive query by transforming it into an equivalent without eq_rq predicates.
   * @param cq a CQ that may contain eq_rq predicates
   * @return a CQ that does not contain any eq_rq predicate
   */
  public static ConjunctiveQuery simplify(ConjunctiveQuery cq){
    Atom eq = getFirstEq(cq);
    if(eq == null){
      return cq;
    }

    assert(eq.getTerms().size() == 2);

    Term first = eq.getTerms().get(0);
    Term second = eq.getTerms().get(1);

    assert(! (first.isConstant() && second.isConstant()));

    ConjunctiveQuery newCq;

    if(first.isConstant()){
      newCq = replaceWith(filter(cq, eq), (Variable)second, (Constant)first);
    } else if(second.isConstant()){
      newCq = replaceWith(filter(cq, eq), (Variable)first, (Constant)second);
    } else {
      //two variables
      newCq = replaceWith(filter(cq, eq), (Variable)first, (Variable)second);
    }

    //We have consumed 1 eq atom, so we recur until we reach the base case (no eq atom).
    return simplify(newCq);
  }

  static ConjunctiveQuery filter(ConjunctiveQuery cq, Atom atom){
    List<Atom> body = cq.getBody().stream().filter(a -> a != atom).collect(Collectors.toList());
    return new ConjunctiveQuery(cq.getHead(), body);
  }

  /**
   * Returns a new CQ where instances of 'replaced' are replaced with instances of 'replaceBy'
   */
  static ConjunctiveQuery replaceWith(ConjunctiveQuery cq, Term replaced, Term replaceBy){
    List<Atom> body = cq.getBody().stream().map(a -> {
      List<Term> newTerms = new ArrayList<>();
      for(Term term: a.getTerms()){
        if(term.equals(replaced)){
          newTerms.add(replaceBy);
        } else {
          newTerms.add(term);
        }
      }
      return new Atom(a.getPredicate(), newTerms);
    }).collect(Collectors.toList());
    return new ConjunctiveQuery(cq.getHead(), body);
  }


  static Atom getFirstEq(ConjunctiveQuery cq){
    for(Atom atom: cq.getBody()){
      if(atom.getPredicate().equals("eq_rq")){
        return atom;
      }
    }
    return null;
  }

}
