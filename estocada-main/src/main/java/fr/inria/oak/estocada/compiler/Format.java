package fr.inria.oak.estocada.compiler;

/**
 * Part of nested block tree data structure.
 *
 * Represents the supported formats.
 *
 * @author ranaalotaibi
 * @author Damian Bursztyn
 */
public enum Format {
	RELATIONAL, XML, JSON, KEYVALUE, TSV,
	MATRIX;
}
