package fr.inria.oak.estocada.views.materialization.module.PR;

/** PR Supported Types **/
public enum PRDataType {

    INTEGER("integer"),
    VARCHAR("varchar");

    private final String dataType;

    private PRDataType(final String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return dataType;
    }
}
