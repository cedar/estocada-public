package fr.inria.oak.estocada.views.materialization.module.blocks;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * An interface for different QBTBlockConstructor types.
 * 
 * @author Rana Alotaibi
 *
 */
public interface QBTBlockConstructor {
    public QBTPattern constructQBTBlock() throws ParseException, TatooineExecutionException;
}
