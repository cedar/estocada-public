package fr.inria.oak.estocada.compiler.model.pm;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
final class ConditionListener {
    private final ConditionTermListener conditionTermListener;

    @Inject
    public ConditionListener(final ConditionTermListener conditionTermListener) {
        this.conditionTermListener = conditionTermListener;
    }
}
