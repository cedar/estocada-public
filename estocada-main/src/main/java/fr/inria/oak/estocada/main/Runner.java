package fr.inria.oak.estocada.main;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.estocada.compiler.Language;

/**
 * ESTOCADA main runner
 * 
 * @author Rana Alotaibi
 */
public class Runner {
    public static void main(String[] args) throws NumberFormatException,

            Exception {
        Injector injector = Guice.createInjector(new EstocadaModule());
        final EstocadaRunner estoadaRunner = injector.getInstance(EstocadaRunner.class);
        final Language lang = Language.QBT;
        switch (lang) {
            case QBT:
                //estoadaRunner.runTattoineSMQCMV();
                estoadaRunner.runTattoineCMQSMV();
                break;
            case BIGDAWG:
                //estoadaRunner.runBigDAWG();
                break;
            default:
                throw new IllegalArgumentException("language Not Suported");
        }

    }
}
