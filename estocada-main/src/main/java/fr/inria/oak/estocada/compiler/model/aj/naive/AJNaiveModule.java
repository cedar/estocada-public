package fr.inria.oak.estocada.compiler.model.aj.naive;

import fr.inria.oak.estocada.compiler.model.aj.AJModule;

public class AJNaiveModule extends AJModule {
    @Override
    protected String getPropertiesFileName() {
        return "aj.naive.properties";
    }
}
