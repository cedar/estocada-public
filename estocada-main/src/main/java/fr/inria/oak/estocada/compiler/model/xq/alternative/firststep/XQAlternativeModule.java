package fr.inria.oak.estocada.compiler.model.xq.alternative.firststep;

import fr.inria.oak.estocada.compiler.model.xq.XQueryModule;

public class XQAlternativeModule extends XQueryModule {
	@Override
	protected String getPropertiesFileName() {
		return "xq.alternative.first_step.properties";
	}
}
