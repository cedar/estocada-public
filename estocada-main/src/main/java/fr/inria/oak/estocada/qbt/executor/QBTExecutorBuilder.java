package fr.inria.oak.estocada.qbt.executor;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * NOTE: TO EXECUTE OUR MIMMIC QBT QUERIES
 * 
 * @author ranaalotaibi
 *
 */
public class QBTExecutorBuilder {
    private final String query;
    private QBTEListenerAux listener;

    public QBTExecutorBuilder(final String query) {
        String qtr = query.replaceAll("\"", "\\\\\"");
        qtr = qtr.replaceAll("\\{", "\\{\"");
        qtr = qtr.replaceAll("\\}", "\"}");
        qtr = qtr.replaceAll("\\\"\"}", "\\\"\\}");
        qtr = qtr.replaceAll("\\{\"\\\\\"", "\\{\\\\\"");
        this.query = qtr;
        listener = new QBTEListenerAux();
    }

    public void parse() {
        final QBTELexer lexer = new QBTELexer(CharStreams.fromString(query));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTEParser parser = new QBTEParser(tokens);
        final ParserRuleContext tree = parser.queryBlock();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        //        TreeViewer viewr = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
        //        viewr.open();
        //        System.out.println(tree.getText());

    }

    public PhyBindJoin getPhyPlan() {
        return listener.getPlanRoot();
    }

}
