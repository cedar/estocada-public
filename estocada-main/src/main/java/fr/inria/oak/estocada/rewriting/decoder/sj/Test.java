package fr.inria.oak.estocada.rewriting.decoder.sj;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSolrEval;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.Utils;

public class Test {
    private static final File INPUT_QUERY_FILE =
            new File("src/test/resources/compiler/sj-translator/test02-Translator/rewriting");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        catalog.delete();
        // Register
        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("Notes");
        // Columns names
        final List<String> colNames = new ArrayList<String>();
        colNames.add("SUBJECT_ID");
        colNames.add("HADM_ID");
        colNames.add("TEXT");

        // NRSMD
        final NRSMD NRSMD = new NRSMD(3, new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE,
                TupleMetadataType.INTEGER_TYPE, TupleMetadataType.STRING_TYPE }, colNames);
        final Map<String, Integer> serviceNRSMDMapping = new HashMap<>();
        serviceNRSMDMapping.put("SUBJECT_ID", 0);
        serviceNRSMDMapping.put("HADM_ID", 1);
        serviceNRSMDMapping.put("TEXT", 2);
        ViewSchema serviceSchema = new ViewSchema(NRSMD, serviceNRSMDMapping);
        catalog.add("Notes", storageReferencePostgresSQL1, null, serviceSchema);

        /*final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        final SJTranslator pjTranslator = new SJTranslator(rw);
        System.out.print(pjTranslator.translate());*/

        final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        final LogOperator sjDecoder = new SJDecoder().decode(catalog, rw);
        System.out.print(((LogSolrEval) sjDecoder).getQueryString());
    }

    private static StorageReference getPostgresSQLStorageReference(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        return gsr;
    }

}
