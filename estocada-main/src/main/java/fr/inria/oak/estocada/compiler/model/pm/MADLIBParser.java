// Generated from MADLIB.g4 by ANTLR 4.4

package fr.inria.oak.estocada.compiler.model.pm;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MADLIBParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__9=1, T__8=2, T__7=3, T__6=4, T__5=5, T__4=6, T__3=7, T__2=8, T__1=9, 
		T__0=10, ID=11, WHITESPACE=12, STRING=13, INT=14, SELECT=15;
	public static final String[] tokenNames = {
		"<INVALID>", "'SELECT madlib.matrix_trans('", "'SELECT madlib.matrix_add('", 
		"'SELECT madlib.matrix_mult('", "'SELECT madlib.matrix_elem_mult('", "'SELECT madlib.matrix_scalar_mult('", 
		"')'", "':'", "';'", "'SELECT madlib.matrix_inverse('", "','", "ID", "WHITESPACE", 
		"STRING", "INT", "SELECT"
	};
	public static final int
		RULE_madQuery = 0, RULE_viewName = 1, RULE_madScript = 2, RULE_madStatemnet = 3, 
		RULE_madExpression = 4, RULE_matrixSourceName = 5, RULE_matrixOutName = 6;
	public static final String[] ruleNames = {
		"madQuery", "viewName", "madScript", "madStatemnet", "madExpression", 
		"matrixSourceName", "matrixOutName"
	};

	@Override
	public String getGrammarFileName() { return "MADLIB.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MADLIBParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class MadQueryContext extends ParserRuleContext {
		public ViewNameContext viewName() {
			return getRuleContext(ViewNameContext.class,0);
		}
		public MadScriptContext madScript() {
			return getRuleContext(MadScriptContext.class,0);
		}
		public MadQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_madQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMadQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMadQuery(this);
		}
	}

	public final MadQueryContext madQuery() throws RecognitionException {
		MadQueryContext _localctx = new MadQueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_madQuery);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14); viewName();
			setState(15); match(T__3);
			setState(16); madScript();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ViewNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MADLIBParser.ID, 0); }
		public ViewNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_viewName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterViewName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitViewName(this);
		}
	}

	public final ViewNameContext viewName() throws RecognitionException {
		ViewNameContext _localctx = new ViewNameContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_viewName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(18); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MadScriptContext extends ParserRuleContext {
		public List<MadStatemnetContext> madStatemnet() {
			return getRuleContexts(MadStatemnetContext.class);
		}
		public MadStatemnetContext madStatemnet(int i) {
			return getRuleContext(MadStatemnetContext.class,i);
		}
		public MadScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_madScript; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMadScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMadScript(this);
		}
	}

	public final MadScriptContext madScript() throws RecognitionException {
		MadScriptContext _localctx = new MadScriptContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_madScript);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20); madStatemnet();
			setState(21); match(T__2);
			setState(27);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__9) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__1))) != 0)) {
				{
				{
				setState(22); madStatemnet();
				setState(23); match(T__2);
				}
				}
				setState(29);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MadStatemnetContext extends ParserRuleContext {
		public MadExpressionContext madExpression() {
			return getRuleContext(MadExpressionContext.class,0);
		}
		public MadStatemnetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_madStatemnet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMadStatemnet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMadStatemnet(this);
		}
	}

	public final MadStatemnetContext madStatemnet() throws RecognitionException {
		MadStatemnetContext _localctx = new MadStatemnetContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_madStatemnet);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30); madExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MadExpressionContext extends ParserRuleContext {
		public MadExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_madExpression; }
	 
		public MadExpressionContext() { }
		public void copyFrom(MadExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MatrixMulExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName(int i) {
			return getRuleContext(MatrixSourceNameContext.class,i);
		}
		public List<MatrixSourceNameContext> matrixSourceName() {
			return getRuleContexts(MatrixSourceNameContext.class);
		}
		public MatrixMulExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixMulExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixMulExpression(this);
		}
	}
	public static class MatrixTransposeExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName() {
			return getRuleContext(MatrixSourceNameContext.class,0);
		}
		public MatrixTransposeExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixTransposeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixTransposeExpression(this);
		}
	}
	public static class MatrixMulScalarExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName() {
			return getRuleContext(MatrixSourceNameContext.class,0);
		}
		public MatrixMulScalarExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixMulScalarExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixMulScalarExpression(this);
		}
	}
	public static class MatrixAddExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName(int i) {
			return getRuleContext(MatrixSourceNameContext.class,i);
		}
		public List<MatrixSourceNameContext> matrixSourceName() {
			return getRuleContexts(MatrixSourceNameContext.class);
		}
		public MatrixAddExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixAddExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixAddExpression(this);
		}
	}
	public static class MatrixInverseExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName() {
			return getRuleContext(MatrixSourceNameContext.class,0);
		}
		public MatrixInverseExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixInverseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixInverseExpression(this);
		}
	}
	public static class MatrixMulElementwiseExpressionContext extends MadExpressionContext {
		public MatrixOutNameContext matrixOutName() {
			return getRuleContext(MatrixOutNameContext.class,0);
		}
		public MatrixSourceNameContext matrixSourceName(int i) {
			return getRuleContext(MatrixSourceNameContext.class,i);
		}
		public List<MatrixSourceNameContext> matrixSourceName() {
			return getRuleContexts(MatrixSourceNameContext.class);
		}
		public MatrixMulElementwiseExpressionContext(MadExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixMulElementwiseExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixMulElementwiseExpression(this);
		}
	}

	public final MadExpressionContext madExpression() throws RecognitionException {
		MadExpressionContext _localctx = new MadExpressionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_madExpression);
		try {
			setState(74);
			switch (_input.LA(1)) {
			case T__7:
				_localctx = new MatrixMulExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(32); match(T__7);
				setState(33); matrixSourceName();
				setState(34); match(T__0);
				setState(35); matrixSourceName();
				setState(36); match(T__0);
				setState(37); matrixOutName();
				setState(38); match(T__4);
				}
				break;
			case T__8:
				_localctx = new MatrixAddExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(40); match(T__8);
				setState(41); matrixSourceName();
				setState(42); match(T__0);
				setState(43); matrixSourceName();
				setState(44); match(T__0);
				setState(45); matrixOutName();
				setState(46); match(T__4);
				}
				break;
			case T__6:
				_localctx = new MatrixMulElementwiseExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(48); match(T__6);
				setState(49); matrixSourceName();
				setState(50); match(T__0);
				setState(51); matrixSourceName();
				setState(52); match(T__0);
				setState(53); matrixOutName();
				setState(54); match(T__4);
				}
				break;
			case T__5:
				_localctx = new MatrixMulScalarExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(56); match(T__5);
				setState(57); matrixSourceName();
				setState(58); match(T__0);
				setState(59); matrixOutName();
				setState(60); match(T__4);
				}
				break;
			case T__9:
				_localctx = new MatrixTransposeExpressionContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(62); match(T__9);
				setState(63); matrixSourceName();
				setState(64); match(T__0);
				setState(65); matrixOutName();
				setState(66); match(T__4);
				}
				break;
			case T__1:
				_localctx = new MatrixInverseExpressionContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(68); match(T__1);
				setState(69); matrixSourceName();
				setState(70); match(T__0);
				setState(71); matrixOutName();
				setState(72); match(T__4);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixSourceNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MADLIBParser.ID, 0); }
		public MatrixSourceNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixSourceName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixSourceName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixSourceName(this);
		}
	}

	public final MatrixSourceNameContext matrixSourceName() throws RecognitionException {
		MatrixSourceNameContext _localctx = new MatrixSourceNameContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_matrixSourceName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatrixOutNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(MADLIBParser.ID, 0); }
		public MatrixOutNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matrixOutName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).enterMatrixOutName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MADLIBListener ) ((MADLIBListener)listener).exitMatrixOutName(this);
		}
	}

	public final MatrixOutNameContext matrixOutName() throws RecognitionException {
		MatrixOutNameContext _localctx = new MatrixOutNameContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_matrixOutName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\21S\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\3\2\3\3\3\3\3\4"+
		"\3\4\3\4\3\4\3\4\7\4\34\n\4\f\4\16\4\37\13\4\3\5\3\5\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\5\6M\n\6\3\7\3\7\3\b\3\b\3\b\2\2\t\2\4\6\b\n\f\16\2\2Q\2\20"+
		"\3\2\2\2\4\24\3\2\2\2\6\26\3\2\2\2\b \3\2\2\2\nL\3\2\2\2\fN\3\2\2\2\16"+
		"P\3\2\2\2\20\21\5\4\3\2\21\22\7\t\2\2\22\23\5\6\4\2\23\3\3\2\2\2\24\25"+
		"\7\r\2\2\25\5\3\2\2\2\26\27\5\b\5\2\27\35\7\n\2\2\30\31\5\b\5\2\31\32"+
		"\7\n\2\2\32\34\3\2\2\2\33\30\3\2\2\2\34\37\3\2\2\2\35\33\3\2\2\2\35\36"+
		"\3\2\2\2\36\7\3\2\2\2\37\35\3\2\2\2 !\5\n\6\2!\t\3\2\2\2\"#\7\5\2\2#$"+
		"\5\f\7\2$%\7\f\2\2%&\5\f\7\2&\'\7\f\2\2\'(\5\16\b\2()\7\b\2\2)M\3\2\2"+
		"\2*+\7\4\2\2+,\5\f\7\2,-\7\f\2\2-.\5\f\7\2./\7\f\2\2/\60\5\16\b\2\60\61"+
		"\7\b\2\2\61M\3\2\2\2\62\63\7\6\2\2\63\64\5\f\7\2\64\65\7\f\2\2\65\66\5"+
		"\f\7\2\66\67\7\f\2\2\678\5\16\b\289\7\b\2\29M\3\2\2\2:;\7\7\2\2;<\5\f"+
		"\7\2<=\7\f\2\2=>\5\16\b\2>?\7\b\2\2?M\3\2\2\2@A\7\3\2\2AB\5\f\7\2BC\7"+
		"\f\2\2CD\5\16\b\2DE\7\b\2\2EM\3\2\2\2FG\7\13\2\2GH\5\f\7\2HI\7\f\2\2I"+
		"J\5\16\b\2JK\7\b\2\2KM\3\2\2\2L\"\3\2\2\2L*\3\2\2\2L\62\3\2\2\2L:\3\2"+
		"\2\2L@\3\2\2\2LF\3\2\2\2M\13\3\2\2\2NO\7\r\2\2O\r\3\2\2\2PQ\7\r\2\2Q\17"+
		"\3\2\2\2\4\35L";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}