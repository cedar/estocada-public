package fr.inria.oak.estocada.compiler.exceptions;

/**
 * Thrown when an error occurs during CQ to SQL translation.
 *
 * @author Julien Leblay
 * @author Damian Bursztyn
 */
public class QueryTranslationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	/**
	 * @param e
	 */
	public QueryTranslationException(Exception e) {
		super(e);
	}

	/**
	 * @param msg
	 *            the detail message
	 */
	public QueryTranslationException(String msg) {
		super(msg);
	}
}
