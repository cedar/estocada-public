package fr.inria.oak.estocada.compiler.model.rk.naive;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Tgd;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.CreatedNodesSetteQueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeConstraintsTemplate;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.exceptions.CompilationException;
import fr.inria.oak.estocada.compiler.model.rk.Predicate;
import fr.inria.oak.estocada.compiler.model.rk.RKMapNamesExtractor;
import fr.inria.oak.estocada.compiler.model.rk.Utils;
import fr.inria.oak.estocada.rewriter.Comment;

/**
 * RK NaiveQueryBlockTreeCompiler
 * 
 * @author Rana Alotaibi
 */
public final class RKNaiveQueryBlockTreeCompiler implements QueryBlockTreeViewCompiler {
    private final String mapNamePrefix;
    private final String viewNamePrefix;
    private final Properties properties = new Properties();
    private Variable viewSetID;
    private Variable internalMapID;
    private final CreatedNodesSetteQueryBlockTree createdNodesSetterVisitor;
    private final QueryBlockTreeConstraintsTemplate forwardConstraintsTemplate;
    private final QueryBlockTreeConstraintsTemplate backwardConstraintsTemplate;
    private final RKForwardEncoderNestedBlockTreeVisitor forwardEncoderVisitor;
    private final RKBackwardEncoderNestedBlockTreeVisitor backwardEncoderVisitor;
    private final RKMapNamesExtractor rKMapNamesExtractor;

    @Inject
    public RKNaiveQueryBlockTreeCompiler(@Named("document_name_prefix") final String documentNamePrefix,
            @Named("view_name_prefix") final String viewNamePrefix,
            final CreatedNodesSetteQueryBlockTree createdNodesSetterVisitor,
            @Named("ForwardConstraintsTemplate") final QueryBlockTreeConstraintsTemplate forwardConstraintsTemplate,
            @Named("BackwardConstraintsTemplate") final QueryBlockTreeConstraintsTemplate backwardConstraintsTemplate,
            final RKForwardEncoderNestedBlockTreeVisitor forwardEncoderVisitor,
            final RKBackwardEncoderNestedBlockTreeVisitor backwardEncoderVisitor,
            final RKMapNamesExtractor rKMapNamesExtractor) {
        this.mapNamePrefix = documentNamePrefix;
        this.viewNamePrefix = viewNamePrefix;
        this.createdNodesSetterVisitor = createdNodesSetterVisitor;
        this.forwardConstraintsTemplate = forwardConstraintsTemplate;
        this.backwardConstraintsTemplate = backwardConstraintsTemplate;
        this.forwardEncoderVisitor = forwardEncoderVisitor;
        this.backwardEncoderVisitor = backwardEncoderVisitor;
        this.rKMapNamesExtractor = rKMapNamesExtractor;
        this.viewSetID = new VariableFactory(properties.getProperty("fresh_conjunctive_query_variable_prefix", "s_"))
                .createFreshVar();
        this.internalMapID =
                new VariableFactory(properties.getProperty("fresh_conjunctive_query_variable_prefix", "d_"))
                        .createFreshVar();
    }

    @Override
    public List<? extends Constraint> compileForwardConstraints(final QueryBlockTree nbt, boolean includeComments)
            throws CompilationException {
        final Set<String> documentNames = rKMapNamesExtractor.getDocumentNames(nbt);
        final QueryBlockTree nbtClone = nbt.deepClone();
        for (Atom atom : nbtClone.getRoot().getPattern().getStructural().encoding()) {
            if (atom.getPredicate().contains("mainmap")) {
                viewSetID = new Variable(atom.getTerm(0).toString());
                break;
            }
        }
        createdNodesSetterVisitor.setCreatedNodes(nbtClone);
        final List<Constraint> constraints = new ArrayList<Constraint>();
        if (includeComments && forwardConstraintsTemplate.hasGrexSchemaForwardConstraints()) {
            constraints.add(new Comment("Grex schema constraints"));
        }
        constraints.addAll(forwardConstraintsTemplate.getGrexSchemaConstraints(documentNames));
        if (includeComments && forwardConstraintsTemplate.hasViewsConstraints()) {
            constraints.add(new Comment(nbtClone.getQueryName() + " view constraints"));
        }
        constraints.addAll(forwardConstraintsTemplate.getViewsConstraints(documentNames, nbtClone.getQueryName()));
        constraints
                .addAll(forwardEncoderVisitor.compileConstraints(nbtClone, includeComments, viewSetID, internalMapID));
        return constraints;
    }

    @Override
    public List<? extends Constraint> compileBackwardConstraints(final QueryBlockTree nbt, boolean includeComments)
            throws CompilationException {
        final Set<String> documentNames = rKMapNamesExtractor.getDocumentNames(nbt);
        final QueryBlockTree nbtClone = nbt.deepClone();
        createdNodesSetterVisitor.setCreatedNodes(nbtClone);
        final List<Constraint> constraints = new ArrayList<Constraint>();
        if (includeComments && backwardConstraintsTemplate.hasGrexSchemaForwardConstraints()) {
            constraints.add(new Comment("Grex schema constraints"));
        }
        constraints.addAll(backwardConstraintsTemplate.getGrexSchemaConstraints(documentNames));
        if (includeComments && backwardConstraintsTemplate.hasViewsConstraints()) {
            constraints.add(new Comment(nbtClone.getQueryName() + " view constraints"));
        }
        constraints.addAll(backwardConstraintsTemplate.getViewsConstraints(documentNames, nbtClone.getQueryName()));
        constraints
                .addAll(backwardEncoderVisitor.compileConstraints(nbtClone, includeComments, viewSetID, internalMapID));
        return constraints;
    }

    @Override
    public RelationalSchema compileGlobalSchema(final QueryBlockTree nbt) throws CompilationException {
        final Set<String> documentNames = rKMapNamesExtractor.getDocumentNames(nbt);
        final QueryBlockTree nbtClone = nbt.deepClone();
        final List<Relation> relations = new ArrayList<Relation>(getRelations(nbtClone));

        for (final Relation relation : Utils.getGlobalRelationsToEnsure(mapNamePrefix, documentNames, viewNamePrefix,
                nbt.getQueryName())) {
            if (!relations.contains(relation)) {

                relations.add(relation);
            }
        }
        return new RelationalSchema(relations);
    }

    @Override
    public RelationalSchema compileTargetSchema(final QueryBlockTree nbt) throws CompilationException {
        final Set<String> documentNames = rKMapNamesExtractor.getDocumentNames(nbt);
        final QueryBlockTree nbtClone = nbt.deepClone();
        final List<Relation> targetSchemaRelations = new ArrayList<Relation>();
        for (final Relation relation : getRelations(nbtClone)) {
            if (isInTargetSchema(nbtClone.getQueryName(), relation)) {
                targetSchemaRelations.add(relation);
            }
        }
        for (final Relation relation : Utils.getTargetRelationsToEnsure(viewNamePrefix, nbt.getQueryName())) {
            if (!targetSchemaRelations.contains(relation)) {
                targetSchemaRelations.add(relation);
            }
        }
        for (final String documentName : documentNames) {
            targetSchemaRelations.add(new Relation(Predicate.MAINMAP.toString() + mapNamePrefix + documentName, 1));
        }

        return new RelationalSchema(targetSchemaRelations);
    }

    private Set<Relation> getRelations(final QueryBlockTree nbt) {
        final Set<Relation> relations = getRelations(compileForwardConstraints(nbt, false));
        relations.addAll(getRelations(compileBackwardConstraints(nbt, false)));
        return relations;
    }

    private Set<Relation> getRelations(final List<? extends Constraint> constraints) {
        final Set<Relation> relations = new HashSet<Relation>();
        for (final Constraint constraint : constraints) {
            if (!(constraint instanceof Comment)) {
                for (final Atom atom : constraint.getPremise()) {
                    relations.add((new Relation(atom.getPredicate(), atom.getTerms().size())));
                }
                if (constraint instanceof Tgd) {
                    for (final Atom atom : ((Tgd) constraint).getConclusion()) {
                        relations.add((new Relation(atom.getPredicate(), atom.getTerms().size())));
                    }
                }
            }
        }
        return relations;
    }

    private boolean isInTargetSchema(final String viewName, final Relation relation) throws CompilationException {
        if (!relation.getName().startsWith(Predicate.COPY.toString())
                && (relation.getName().equals(viewName) || relation.getName().endsWith(viewNamePrefix + viewName))) {
            return true;
        }
        return false;

    }
}