/**
 * 
 */
package fr.inria.oak.estocada.extension.BigDAWG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.jfree.util.Log;

import istc.bigdawg.LoggerSetup;
import istc.bigdawg.migration.MigrationResult;
import istc.bigdawg.postgresql.PostgreSQLHandler;

/**
 * BigDAWG Data Migration from Solr to Postgres.
 * 
 * @author Rana Alotaibi
 */
public class FromSolrToPostgres {

    private static Logger logger = Logger.getLogger(FromSolrToPostgres.class);

    private String TABLE;
    //= "gdelt.solrResult";
    private String COLLECTIONNAME;
    //= "gdeltarticle";
    private String DB_URL;
    //= "jdbc:postgresql://localhost:5432/GDELT?user=postgres&password=postgres";
    public static final String DEFAULT_SOLR_URL = "http://localhost";
    public static final String DEFAULT_SOLR_PORT = "8983";
    private String CREATE_TABLE;
    //= "CREATE TABLE " + TABLE + " (globaleventid INTEGER);";

    public FromSolrToPostgres(final String DB_URL, final String TABLE, final String CREATE_TABLE,
            final String COLLECTIONNAME) {
        this.DB_URL = DB_URL;
        this.CREATE_TABLE = CREATE_TABLE;
        this.COLLECTIONNAME = COLLECTIONNAME;
        this.TABLE = TABLE;
        LoggerSetup.setLogging();
        logger.setLevel(Level.OFF);
        Logger.getLogger("org.apache.http").setLevel(org.apache.log4j.Level.OFF);
    }

    public MigrationResult fromSolrToPostgresQuery(final SolrQuery solrQuery) {

        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            Connection con = DriverManager.getConnection(DB_URL);
            SolrConnectionInfo solrConnection =
                    new SolrConnectionInfo(DEFAULT_SOLR_URL, DEFAULT_SOLR_PORT, COLLECTIONNAME);
            BigDAWGFromSolrToPostgresMigrator fromsSolrToPostgres = null;
            try {
                con.setReadOnly(false);
            } catch (SQLException e1) {
                logger.error("Cannot create connection to PostgreSQL.");
                e1.printStackTrace();
                return null;
            }
            // Connect to Postgres and create a table for incoming results from Solr
            try {
                PostgreSQLHandler.executeStatement(con, "DROP TABLE IF EXISTS " + TABLE);
                PostgreSQLHandler.createTargetTableSchema(con, TABLE, CREATE_TABLE);
            } catch (SQLException e1) {
                logger.error("Could not create table in PostgreSQL.");
                e1.printStackTrace();
                System.exit(1);
            }
            fromsSolrToPostgres = new BigDAWGFromSolrToPostgresMigrator(solrConnection, con);
            try {
                return fromsSolrToPostgres.fromSolrToPostgres(solrQuery, TABLE);
            } catch (Exception e) {
                logger.error("Could not migrate data from Solr to Postgres.");
                e.printStackTrace();
            }
            logger.debug("The data was migrated from Solr to PostgreSQL.");

        } catch (SQLException e2) {
            Log.error("Could not create instance of fromSolrtoPostgres");
            e2.printStackTrace();
        } finally {
        }
        return null;
    }
}
