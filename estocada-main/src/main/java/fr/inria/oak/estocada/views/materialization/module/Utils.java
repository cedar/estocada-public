package fr.inria.oak.estocada.views.materialization.module;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import com.google.gson.JsonObject;

public class Utils {
    private static final String CHARSET = "utf-8";

    public static void writeToFile(final String fileName, final List<JsonObject> jsonObjects) throws IOException {
        final File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        final Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), CHARSET));
        try {
            for (final JsonObject jsonObject : jsonObjects) {
                writer.write(jsonObject + "\n");
            }
        } catch (IOException e) {
            throw e;
        } finally {
            writer.close();
        }
    }

}
