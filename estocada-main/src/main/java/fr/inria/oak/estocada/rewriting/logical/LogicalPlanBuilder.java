package fr.inria.oak.estocada.rewriting.logical;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.views.materialization.module.Annotation;

/**
 * This class builds a logical plan given the sub-rewritings grouped by a storage reference.
 * 
 * @author ranaalotaibi
 *
 */
public class LogicalPlanBuilder {
    private static final Logger LOGGER = Logger.getLogger(LogicalPlanBuilder.class);
    final Map<StorageReference, ConjunctiveQuery> subRewritings;
    final List<Term> rewritingHeads;
    final Catalog catalog;
    Map<Annotation, Decoder> modelDecodersInstances;

    /** Constructor **/
    public LogicalPlanBuilder(final List<Term> rewritingHeads,
            final Map<StorageReference, ConjunctiveQuery> subRewritings, final Catalog catalog) {
        this.rewritingHeads = rewritingHeads;
        this.subRewritings = subRewritings;
        this.catalog = catalog;
        instantiateModelDecodersInstances();
    }

    /**
     * Build the logical plan.
     * 
     * @return This function returns the root of the plan.
     */
    public LogOperator build() {
        final List<DecoderNode> decoderNodes = subRewritings.entrySet().stream()
                .map(e -> new DecoderNode(e.getValue().getHead(), decode(e.getKey(), e.getValue())))
                .collect(Collectors.toList());

        if (decoderNodes.isEmpty()) {
            throw new IllegalArgumentException("Cannot join an empty queires list.");
        }
        final DecoderNode firstNode = decoderNodes.remove(decoderNodes.size() - 1);
        final List<DecoderNode> otherNodes = new ArrayList<>(decoderNodes);
        while (otherNodes.size() > 0) {
            DecoderNode nextToJoin = null;
            for (final DecoderNode candidate : otherNodes) {
                if (firstNode.canJoinWith(candidate)) {
                    otherNodes.remove(candidate);
                    nextToJoin = candidate;
                    break;
                }
            }
            if (nextToJoin == null) {
                throw new IllegalStateException("Could not find a join candidate.");
            }
            firstNode.joinWith(nextToJoin);
        }
        return firstNode.getLogOperator();
    }

    /**
     * Decode a given sub-rewriting to be executed by a single storage reference.
     * 
     * @param storageReference
     *            the storage reference where the sub-rewriting should be executed.
     * @param conjunctiveQuery
     *            the sub-rewriting to be decoded.
     * @return This function returns decoded sub-rewriting as logical operator.
     */
    private LogOperator decode(final StorageReference storageReference, final ConjunctiveQuery conjunctiveQuery) {
        String model = storageReference.getPropertyValue("modelId");
        Decoder decoder = modelDecodersInstances.get(Annotation.valueOf(model));
        if (decoder == null) {
            throw new IllegalStateException("Could not find a decoder for " + model + "model.");
        }
        try {
            return decoder.decode(catalog, conjunctiveQuery);
        } catch (TatooineExecutionException tee) {
            throw new RuntimeException(tee);
        }
    }

    /**
     * instantiates instances of model decoders
     */
    private void instantiateModelDecodersInstances() {
        modelDecodersInstances = ModelDecodersInstantiator.instantiate();
    }
}
