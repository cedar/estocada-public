package fr.inria.oak.estocada.views.materialization.module;

import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

/**
 * GSR Initializer.
 * 
 * @author ranaalotaibi
 *
 */
public class GSRInitializr {
    private static final Logger LOGGER = Logger.getLogger(GSRInitializr.class);

    public static StorageReference getPostgresSQLStorageReference() {
        //TODO: The url should be obtained from Tatooine config
        final String postgresURL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR("");
        try {
            gsr.setProperty("url", postgresURL);
        } catch (Exception e) {
            LOGGER.error("Cannot set the database url" + e.getMessage());
        }
        return gsr;
    }

    public static StorageReference getRedisStorageReference() {
        //TODO: The host should be obtained from Tatooine config
        final String redisHost = "localhost";
        StorageReference gsr = new GSR("");
        try {
            gsr.setProperty("host", redisHost);
        } catch (Exception e) {
            LOGGER.error("Cannot set redis host" + e.getMessage());
        }
        return gsr;
    }

    public static StorageReference geSolrStorageReference() {
        //TODO: The host should be obtained from Tatooine config
        StorageReference gsr = new GSR("");
        try {
            gsr.setProperty("serverUrl", "http://localhost");
            gsr.setProperty("serverPort", "8983");

        } catch (Exception e) {
            LOGGER.error("Cannot set solr host" + e.getMessage());
        }
        return gsr;
    }

    public static StorageReference getAsterixDBStorageReference() {
        //TODO: The host should be obtained from Tatooine config
        final GSR gsr = new GSR("");
        try {
            gsr.setProperty("host", "127.0.0.1");
            gsr.setProperty("port", "19002");

        } catch (Exception e) {
            LOGGER.error("Cannot set AsterixDB host" + e.getMessage());
        }
        return gsr;
    }
}
