package fr.inria.oak.estocada.compiler.model.tq;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.MapsCatalog;
import fr.inria.oak.estocada.compiler.PathExpression;
import fr.inria.oak.estocada.compiler.VariableFactory;
import fr.inria.oak.estocada.compiler.VariableMapper;
import fr.inria.oak.estocada.compiler.exceptions.ParseException;

/**
 * TQ ColumnLookUpListener
 * 
 * @author Rana Alotaibi
 *
 */
@Singleton
class ColumnLookUpListener extends TQLBaseListener {
    private static final Logger log = Logger.getLogger(ColumnLookUpListener.class);
    /* Used to create fresh CQ variables */
    private final VariableFactory cqVariableFactory;
    /* Used to map kqlquery variables and the internal fresh variables */
    private final VariableMapper variableMapper;
    /* Used to keep the document name prefix of this path expression */
    private final String mapNamePrefix;
    /* Used to get the document name of the document sources */
    private final MapsCatalog mapsCatalog;
    /*
     * Used to keep the referred variables in the path expression being parsed
     */
    private Set<Variable> referredVariables;
    /* Used to keep the encoding of the path expression being parsed */
    private List<Atom> encoding;

    /*
     * The pointer to the current variable for the return element of the path
     * expression
     */
    private Variable currentVar;

    @Inject
    public ColumnLookUpListener(@Named("document_name_prefix") final String mapNamePrefix,
            @Named("ConjunctiveQueryVariableFactory") final VariableFactory cqVariableFactory,
            final VariableMapper variableMapper, final MapsCatalog mapsCatalog) {
        this.mapNamePrefix = checkNotNull(mapNamePrefix);
        this.cqVariableFactory = checkNotNull(cqVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
        this.mapsCatalog = checkNotNull(mapsCatalog);
    }

    public PathExpression parse(final String str) throws ParseException {
        referredVariables = new HashSet<Variable>();
        encoding = new ArrayList<Atom>();
        currentVar = null;
        final TQLLexer lexer = new TQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TQLParser parser = new TQLParser(tokens);
        final ParserRuleContext tree = parser.source();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
            return new PathExpression(TQModel.ID, referredVariables, encoding, currentVar,
                    new HashMap<String, String>());
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
    }

    @Override
    public void enterFilename(TQLParser.FilenameContext ctx) {
        log.debug("Entering FileName: " + ctx.getText());
        if (currentVar == null) {
            final Variable var = cqVariableFactory.createFreshVar();
            currentVar = var;
            encoding.add(
                    new Atom(Predicate.FILE.toString() + mapNamePrefix + mapsCatalog.getId(ctx.NAME().getText()), var));
        }
    }

    @Override
    public void exitColumnLookUp(TQLParser.ColumnLookUpContext ctx) {
        log.debug("Exiting Main Map Look Up " + ctx.getText());
        final Variable var = cqVariableFactory.createFreshVar();
        encoding.add(new Atom(Predicate.COLUMN.toString(), currentVar, var, Utils.toTerm(ctx.INTEGER().getText())));
        currentVar = var;
    }

    @Override
    public void enterColumnVar(TQLParser.ColumnVarContext ctx) {
        log.debug("Entering ColumnVar Name: " + ctx.getText());
        final Variable var = variableMapper.getVariable(ctx.getText());
        referredVariables.add(var);
        currentVar = var;
    }

}
