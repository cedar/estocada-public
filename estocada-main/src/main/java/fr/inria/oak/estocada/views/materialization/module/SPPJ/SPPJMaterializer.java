package fr.inria.oak.estocada.views.materialization.module.SPPJ;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyAsterixDBJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.oak.estocada.utils.Utils;
import fr.inria.oak.estocada.views.materialization.module.Materializer;
import fr.inria.oak.estocada.views.materialization.module.MaterializerException;
import fr.inria.oak.estocada.views.materialization.module.blocks.QBTPattern;

/**
 * SPPJ Materializer.
 * 
 * @author ranaalotaibi
 *
 */
public final class SPPJMaterializer implements Materializer {

    private static final Logger LOGGER = Logger.getLogger(SPPJMaterializer.class);

    /** AsterixDB Connection parameters **/
    private static final String QUERY_SERVICE_PATH = "/query/service";
    private static final String QUERY_CANCEL_PATH = "/admin/requests/running/";
    private static final String CLIENT_CONTEXT_ID = "CANCEL_PARAM";
    private CloseableHttpClient closeableHttpClient = null;
    private HttpPost apiHttpPost = null;
    private StringBuilder uri;

    /** SPPJMaterializer parameters **/
    private final String viewName;
    private final List<String> selectedVariables;
    private final QBTPattern qbtPattern;
    private final StorageReference gsr;
    private final NRSMD queryNRSMD;
    private final String returnClauseQuery;

    /** Constructor **/
    public SPPJMaterializer(final String viewName, final List<String> selectedVariables, final QBTPattern qbtPattern,
            final NRSMD returnNRSMD, final String returnClauseQuery, final StorageReference gsr)
            throws MaterializerException {
        this.selectedVariables = checkNotNull(selectedVariables);
        this.qbtPattern = checkNotNull(qbtPattern);
        this.viewName = checkNotNull(viewName);
        this.gsr = checkNotNull(gsr);
        this.queryNRSMD = checkNotNull(returnNRSMD);
        this.returnClauseQuery = checkNotNull("USE " + viewName + ";" + returnClauseQuery);

    }

    @Override
    public void materialize() throws MaterializerException {
        try {
            connect();
            executeStatement(getAsterixDBDDL());
            executePhyEval();
            LOGGER.debug("View materialized successfully.");
        } catch (MaterializerException exception) {
            close();
            LOGGER.error("View materialization faild :" + exception.getMessage());
            throw exception;

        }
    }

    /**
     * Execute the QBT pattern operator.
     * 
     * @param phyEval
     *            the physical operator.
     * @throws MaterializerException
     */
    private void executePhyEval() throws MaterializerException {
        try {
            final List<NTuple> nTuples = new ArrayList<NTuple>();
            final NIterator patternPhyEval = qbtPattern.getQBTBlockPhyEval();
            final PhyAsterixDBJSONEval phyAsterixDBEval =
                    new PhyAsterixDBJSONEval(returnClauseQuery, (GSR) gsr, queryNRSMD);
            final PhyBindJoin join = new PhyBindJoin(patternPhyEval, phyAsterixDBEval, getBindingPattern());
            final NRSMD joinNRSMD = join.nrsmd;
            join.open();
            while (join.hasNext()) {
                final NTuple next = join.next();
                nTuples.add(next);
            }
            join.close();
            load(transformNTuples(nTuples, qbtPattern.getBlockNRSMD(), joinNRSMD));

        } catch (TatooineExecutionException | MaterializerException exception) {
            throw new MaterializerException(exception);
        }

    }

    /**
     * Get the binding pattern.
     * 
     * @return the binding pattern.
     */
    private int[] getBindingPattern() {
        final String[] colNames = qbtPattern.getBlockNRSMD().getColNames();
        final int[] bindingPattern = new int[selectedVariables.size()];
        int index = 0;
        for (int i = 0; i < colNames.length; i++) {
            if (selectedVariables.contains(colNames[i])) {
                bindingPattern[index] = i;
                index++;
            }
        }
        return bindingPattern;
    }

    /**
     * Get AsterixDB DDL statement.
     * 
     * @return AsterixDB DDL statement.
     */
    private String getAsterixDBDDL() {
        final StringBuilder ddl = new StringBuilder();
        ddl.append(" DROP DATAVERSE " + viewName + " IF EXISTS; CREATE DATAVERSE " + viewName + ";");
        ddl.append(" USE " + viewName + ";");
        ddl.append(" CREATE TYPE " + viewName + "Type" + " AS { PK: UUID};");
        ddl.append(" CREATE DATASET " + viewName + "(" + viewName + "Type" + ")" + " PRIMARY KEY PK autogenerated;");
        return ddl.toString();
    }

    /**
     * Execute AsterixDB statement.
     * 
     * @param statement
     *            the AsterixDB statement.
     * @throws MaterializerException
     */
    private void executeStatement(final String statement) throws MaterializerException {
        try {
            final List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("statement", statement));
            params.add(new BasicNameValuePair("client_context_id", CLIENT_CONTEXT_ID));
            LOGGER.info(String.format("Querying AsterixDB with query \"%s\"", statement));
            apiHttpPost.setEntity(new UrlEncodedFormEntity(params));
            InputStream inputStream = closeableHttpClient.execute(apiHttpPost).getEntity().getContent();
            StringWriter writerInputstream = new StringWriter();
            IOUtils.copy(inputStream, writerInputstream, "UTF-8");
        } catch (IOException exception) {
            LOGGER.error("Satatement execution faild: " + exception.getMessage());
            throw new MaterializerException(exception);
        }
    }

    /**
     * Bulk load the JSON documents into AsterixDB.
     * 
     * @param jsonObjects
     *            the list of JSON documents to be loaded.
     * @throws MaterializerException
     */
    private void load(final List<JsonObject> jsonObjects) throws MaterializerException {
        try {
            final String absolute_path = Utils.writeJsonData(viewName, jsonObjects);
            executeStatement(getLoadStatement(absolute_path));
        } catch (IOException exception) {
            throw new MaterializerException(exception);
        }
    }

    /**
     * Close connection to AsterixDB.
     */
    private void close() {
        final CloseableHttpClient httpclient = HttpClients.createDefault();
        final HttpDelete endPoint =
                new HttpDelete(uri.toString() + QUERY_CANCEL_PATH + "?client_context_id=" + CLIENT_CONTEXT_ID);
        try {
            final InputStream inputStream = httpclient.execute(endPoint).getEntity().getContent();
            LOGGER.info("AsterixDB connection canceled.");
            StringWriter writer = new StringWriter();
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException iOException) {
            LOGGER.error("Could not cancel job for id " + CLIENT_CONTEXT_ID, iOException);
        }
    }

    /**
     * Establish AsterixDB connection.
     */
    private void connect() {
        uri = new StringBuilder();
        uri.append("http://");
        final String host = gsr.getPropertyValue("host");
        if (!Strings.isNullOrEmpty(host)) {
            uri.append(host);
        }
        uri.append(":");
        final String port = gsr.getPropertyValue("port");
        if (!Strings.isNullOrEmpty(port)) {
            uri.append(port);
        }
        closeableHttpClient = HttpClients.createDefault();
        apiHttpPost = new HttpPost(uri.toString() + QUERY_SERVICE_PATH);

    }

    /**
     * Transform NTuples to JSON.
     * 
     * @param nTuples
     *            the list of NTuples.
     * @param leftNRSMD
     *            the left operator NRSMD.
     * @param joinNRSMD
     *            the join operator NRSMD.
     * @return the transformed NTuples.
     * @throws MaterializerException
     * @throws TatooineExecutionException
     */
    private List<JsonObject> transformNTuples(final List<NTuple> nTuples, final NRSMD leftNRSMD, final NRSMD joinNRSMD)
            throws MaterializerException {
        final List<JsonObject> jsonObjects = new ArrayList<JsonObject>();
        /** find return clause columns **/
        final int[] projectedCol = new int[queryNRSMD.getColNames().length];
        int indx = 0;
        for (int i = leftNRSMD.getColNo(); i < joinNRSMD.getColNo(); i++) {
            projectedCol[indx++] = i;
        }
        try {
            final NRSMD projectedNRSMD = NRSMD.makeProjectRSMD(joinNRSMD, projectedCol);
            final TupleMetadataType[] types = projectedNRSMD.getColumnsMetadata();
            final String[] colNames = projectedNRSMD.getColNames();

            for (final NTuple nTuple : nTuples) {
                final JsonObject jsonObject = new JsonObject();
                for (int i = 0; i < types.length; i++) {
                    switch (types[i]) {
                        case INTEGER_TYPE:
                            jsonObject.addProperty(colNames[i], nTuple.getIntegerField(projectedCol[i]));
                            break;
                        case STRING_TYPE:
                            final String strValue = new String(((char[]) nTuple.getStringField(projectedCol[i])));
                            jsonObject.addProperty(colNames[i], strValue);
                            break;
                        case TUPLE_TYPE:
                            final JsonArray nestedValue = transofrmNestedField(nTuple.getNestedField(projectedCol[i]));
                            jsonObject.add(colNames[i], nestedValue);
                            break;
                        default:
                            String msg = String.format("Unsupported NTupleMD type: %s", types[i]);
                            throw new MaterializerException(msg);
                    }
                }
                jsonObjects.add(jsonObject);
            }
        } catch (TatooineExecutionException exception) {
            throw new MaterializerException(exception);
        }
        return jsonObjects;
    }

    /**
     * Transform the nested NTuple field to JSON format.
     * 
     * @param nTuples
     *            the nested field.
     * @return the constructed JSON format from nested NTuples.
     * @throws MaterializerException
     */
    private JsonArray transofrmNestedField(final List<NTuple> nTuples) throws MaterializerException {
        final NRSMD childNRSDM = nTuples.get(0).nrsmd;
        final JsonArray jsonArray = new JsonArray();
        for (final NTuple nTuple : nTuples) {
            final JsonObject jsonObject = new JsonObject();
            for (int i = 0; i < childNRSDM.types.length; i++) {
                final TupleMetadataType type = childNRSDM.types[i];
                switch (type) {
                    case STRING_TYPE:
                        final String strValue = new String((char[]) nTuple.getStringField(i));
                        jsonObject.addProperty(childNRSDM.getColNames()[i], strValue);
                        break;
                    case INTEGER_TYPE:
                        final int intValue = nTuple.getIntegerField(i);
                        jsonObject.addProperty(childNRSDM.getColNames()[i], intValue);
                        break;
                    case TUPLE_TYPE:;
                        final JsonArray nestedValue = transofrmNestedField(nTuple.getNestedField(i));
                        jsonObject.add(childNRSDM.getColNames()[i], nestedValue);
                        break;
                    default:
                        String msg = String.format("Unsupported NTupleMD type: %s", type);
                        throw new MaterializerException(msg);
                }
            }
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

    /**
     * Get AsterixDB data load statement.
     * 
     * @return the load statement.
     */
    final String getLoadStatement(final String absolute_path) {
        final StringBuilder laodStatement = new StringBuilder();
        laodStatement.append("USE " + viewName + " ;");
        laodStatement.append(" LOAD DATASET " + viewName + " USING localfs ");
        laodStatement.append("((\"path\"=\"127.0.0.1://" + absolute_path + "\"), (\"format\"=\"json\"));");
        return laodStatement.toString();
    }
}
