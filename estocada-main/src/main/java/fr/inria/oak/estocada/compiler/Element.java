package fr.inria.oak.estocada.compiler;

import fr.inria.oak.commons.conjunctivequery.Term;

/**
 * Part of query block tree data structure.
 *
 * @author Damian Bursztyn
 * @author Rana Alotaibi
 */
public interface Element {
    /**
     * The conjunctive query term for this element.
     *
     * @return the conjunctive query term for this element.
     */
    public Term toTerm();

    public boolean isEmpty();
}
