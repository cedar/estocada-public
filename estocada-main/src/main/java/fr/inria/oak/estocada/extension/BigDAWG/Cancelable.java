package fr.inria.oak.estocada.extension.BigDAWG;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

/**
 * Terminate connections (timed-out query)
 * 
 * @author ranaalotaibi
 *
 */
public class Cancelable {

    private static final Logger LOGGER = Logger.getLogger(Cancelable.class);
    private Timer timer;

    public Cancelable() {
    }

    public void startTimer(List<? extends IResource> connections) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                LOGGER.info("Query Canceled Timeout!");
                connections.forEach(x -> x.close());
            }
        }, 420000);
    }

    public void cancelTimer() {
        timer.cancel();
    }

    @FunctionalInterface
    public interface IResource {
        void close();
    }

    public static List<? extends IResource> getPostgresConnectionsList(Connection... connections) {
        List<IResource> rList = new ArrayList<>(connections.length);
        for (Connection c : connections) {
            rList.add(new IResource() {
                @Override
                public void close() {
                    try {
                        c.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return rList;
    }
}
