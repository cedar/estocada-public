package fr.inria.oak.estocada.compiler.model.cg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.misc.Array2DHashSet;

import javafx.util.Pair;

class QueryHead {
    private List<String> varList;

    public QueryHead(List<String> varList) {
        this.varList = varList;
    }

    public List<String> getVarList() {
        return varList;
    }

    @Override
    public String toString() {
        String str = "q<";
        for (String varName : varList)
            str += varName + ", ";
        str = str.substring(0, str.length() - 2);
        str += ">";
        return str;
    }
}

class KindI {
    private String id;
    private String entityKind;

    public KindI(String id, String entityKind) {
        this.id = id;
        this.entityKind = entityKind;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntityKind() {
        return entityKind;
    }

    public void setEntityKind(String entityKind) {
        this.entityKind = entityKind;
    }

    @Override
    public String toString() {
        return "Kind_I (" + id + ", " + entityKind + ")";
    }
}

class LabelI {
    private String id;
    private String label;

    public LabelI(String id, String label) {
        this.id = id;
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Label_I (" + id + ", " + label + ")";
    }

    @Override
    public boolean equals(Object obj) {
        return this.id.equals(((LabelI) obj).id) && this.label.equals(((LabelI) obj).label);
    }
}

class PropertyI {
    private String id;
    private String propertyKeyName;
    private String dtId;
    private String vId;

    public PropertyI(String id, String propertyKeyName, String dtId, String vId) {
        this.id = id;
        this.propertyKeyName = propertyKeyName;
        this.dtId = dtId;
        this.vId = vId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPropertyKeyName() {
        return propertyKeyName;
    }

    public void setPropertyKeyName(String propertyKeyName) {
        this.propertyKeyName = propertyKeyName;
    }

    public String getDtId() {
        return dtId;
    }

    public void setDtId(String dtId) {
        this.dtId = dtId;
    }

    public String getvId() {
        return vId;
    }

    public void setvId(String vId) {
        this.vId = vId;
    }

    @Override
    public String toString() {
        return "Property_I (" + id + ", " + propertyKeyName + ", " + dtId + ", " + vId + ")";
    }

    @Override
    public boolean equals(Object obj) {
        return this.id.equals(((PropertyI) obj).id) && this.propertyKeyName.equals(((PropertyI) obj).propertyKeyName);
    }
}

class ValueI {
    private String vId;
    private String v;

    public ValueI(String vId, String v) {
        this.vId = vId;
        this.v = v;
    }

    public String getvId() {
        return vId;
    }

    public void setvId(String vId) {
        this.vId = vId;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    @Override
    public String toString() {
        return "Value_I (" + vId + ", " + v + ")";
    }

    @Override
    public boolean equals(Object o) {
        return this.vId.equals(((ValueI) o).vId) && this.v.equals(((ValueI) o).v);
    }
}

class CompI {
    protected String vId1;
    protected String vId2;

    public CompI(String vId1, String vId2) {
        this.vId1 = vId1;
        this.vId2 = vId2;
    }

    public String getvId1() {
        return vId1;
    }

    public void setvId1(String vId1) {
        this.vId1 = vId1;
    }

    public String getvId2() {
        return vId2;
    }

    public void setvId2(String vId2) {
        this.vId2 = vId2;
    }
}

class EqI extends CompI {
    public EqI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Eq_I (" + vId1 + ", " + vId2 + ")";
    }
}

class NeI extends CompI {
    public NeI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Ne_I (" + vId1 + ", " + vId2 + ")";
    }
}

class LtI extends CompI {
    public LtI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Lt_I (" + vId1 + ", " + vId2 + ")";
    }
}

class GtI extends CompI {
    public GtI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Gt_I (" + vId1 + ", " + vId2 + ")";
    }
}

class LeI extends CompI {
    public LeI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Le_I (" + vId1 + ", " + vId2 + ")";
    }
}

class GeI extends CompI {
    public GeI(String vId1, String vId2) {
        super(vId1, vId2);
    }

    @Override
    public String toString() {
        return "Ge_I (" + vId1 + ", " + vId2 + ")";
    }
}

class ConnectionI {
    private String sId;
    private String tId;
    private String eId;

    public ConnectionI(String sId, String tId, String eId) {
        this.sId = sId;
        this.tId = tId;
        this.eId = eId;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String geteId() {
        return eId;
    }

    public void seteId(String eId) {
        this.eId = eId;
    }

    protected void replaceTempToken(String nId) {
        if (sId.equals(Constraint.getTempToken()))
            setsId(nId);
        else
            settId(nId);
    }

    protected String getNotTempNId() {
        if (getsId().equals(Constraint.getTempToken()))
            return tId;
        else
            return sId;
    }

    @Override
    public String toString() {
        return "Connection_I(" + sId + ", " + tId + ", " + eId + ')';
    }
}

public class Constraint {
    private int idCount;
    private int dtIdCount;
    private int vIdCount;
    private int vCount;

    public Constraint() {
        idCount = dtIdCount = vIdCount = vCount = 0;
    }

    protected String getNextId() {
        return "id" + idCount++;
    }

    protected String getNextDtId() {
        return "dtId" + dtIdCount++;
    }

    protected String getNextVId() {
        return "vId" + vIdCount++;
    }

    protected String getNextV() {
        return "v" + vCount++;
    }

    protected static String getTempToken() {
        return "Temp";
    }

    public static String genEncodedQuery(List<Object> encoding) {
        String str = "";
        for (Object atom : encoding) {
            if (atom instanceof QueryHead)
                str = atom.toString() + " :- " + str;
            else
                str += atom.toString() + ", ";
        }
        str = str.substring(0, str.length() - 2) + ";\n";
        return str;
    }

    public static Map<String, String> genViewChaseEncodingText(List<List<Object>> encoding) {
        genViewChaseEncoding(encoding.get(0), encoding.get(1));
        String str = "";
        for (List<Object> part : encoding) {
            for (Object atom : part)
                str += atom.toString() + ", ";
            str = str.substring(0, str.length() - 2);
            str += " -> ";
        }
        str = str.substring(0, str.length() - 4) + ";\n";
        Map<String, String> viewChaseEncoding = genViewIC(encoding);
        viewChaseEncoding.put("chase", str);
        return viewChaseEncoding;
    }

    public static Map<String, String> genViewIC(List<List<Object>> encoding) {
        Map<String, String> viewIC = new HashMap<>();
        // generate property existence constraints
        Map<String, Pair<Boolean, String>> viewLabelMap = new HashMap<String, Pair<Boolean, String>>();
        Map<String, Set<String>> viewPropertyMap = new HashMap<String, Set<String>>();
        List<ConnectionI> connectionIAtoms = new ArrayList<ConnectionI>();
        List<PropertyI> propertyIAtoms = new ArrayList<PropertyI>();
        for (Object atom : encoding.get(1)) {
            if (atom instanceof LabelI) {
                viewLabelMap.put(((LabelI) atom).getId(), new Pair<Boolean, String>(true, ((LabelI) atom).getLabel()));
                viewPropertyMap.put(((LabelI) atom).getId(), new Array2DHashSet<String>());
            } else if (atom instanceof ConnectionI)
                connectionIAtoms.add((ConnectionI) atom);
            else if (atom instanceof PropertyI)
                propertyIAtoms.add((PropertyI) atom);
        }
        for (ConnectionI atom : connectionIAtoms) {
            String label = viewLabelMap.get(atom.geteId()).getValue();
            viewLabelMap.put(atom.geteId(), new Pair<Boolean, String>(false, label));
        }
        for (PropertyI atom : propertyIAtoms)
            viewPropertyMap.get(atom.getId()).add(atom.getPropertyKeyName());
        Set<String> viewEntityIdSet = viewLabelMap.keySet();
        String PECStr = "";
        for (String viewEntityId : viewEntityIdSet) {
            Pair<Boolean, String> pair = viewLabelMap.get(viewEntityId);
            boolean nOrE = pair.getKey();
            String label = pair.getValue();
            Set<String> pNames = viewPropertyMap.get(viewEntityId);
            PECStr += propertyExistenceConstraint(label, nOrE, pNames);
        }
        viewIC.put("PEC", PECStr);
        // generate view set semantics constraints
        String VSSStr = "";
        for (String viewEntityId : viewEntityIdSet) {
            String label = viewLabelMap.get(viewEntityId).getValue();
            Set<String> pNames = viewPropertyMap.get(viewEntityId);
            VSSStr += viewSetSemanticsConstraint(label, pNames);
        }
        viewIC.put("VSS", VSSStr);
        return viewIC;
    }

    public static String genViewBackchaseEncodingText(List<List<Object>> encoding) {
        genViewBackchaseEncoding(encoding.get(0), encoding.get(1));
        String str = "";
        for (List<Object> part : encoding) {
            for (Object atom : part)
                str += atom.toString() + ", ";
            str = str.substring(0, str.length() - 2);
            str += " -> ";
        }
        str = str.substring(0, str.length() - 4) + ";\n";
        return str;
    }

    private static void genViewChaseEncoding(List<Object> readingEncoding, List<Object> updatingEncoding) {
        // move atoms not belonging to view from conclusion to premise
        Set<String> idInData = new HashSet<String>();
        for (Object atom : readingEncoding) {
            if (atom instanceof LabelI)
                idInData.add(((LabelI) atom).getId());
            else if (atom instanceof KindI)
                idInData.add(((KindI) atom).getId());
        }
        List<Object> moveAtoms = new ArrayList<Object>();
        for (Iterator<Object> it = updatingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            if (atom instanceof LabelI && idInData.contains(((LabelI) atom).getId())) {
                moveAtoms.add(atom);
                it.remove();
            } else if (atom instanceof KindI && idInData.contains(((KindI) atom).getId())) {
                moveAtoms.add(atom);
                it.remove();
            } else if (atom instanceof PropertyI && idInData.contains(((PropertyI) atom).getId())) {
                moveAtoms.add(atom);
                it.remove();
            }
        }
        readingEncoding.addAll(moveAtoms);
        // remove redundant KindI if LabelI exists
        removeRedundantKind(readingEncoding);
        removeRedundantKind(updatingEncoding);
    }

    private static void genViewBackchaseEncoding(List<Object> readingEncoding, List<Object> updatingEncoding) {
        // find all id LabelI / KindI in updatingEncoding, move relevant atoms to premise part, others to conclusion part
        Set<String> idInUpdating = new HashSet<String>();
        for (Object atom : updatingEncoding) {
            if (atom instanceof LabelI)
                idInUpdating.add(((LabelI) atom).getId());
            else if (atom instanceof KindI)
                idInUpdating.add(((KindI) atom).getId());
        }
        List<Object> moveToPremiseAtoms = new ArrayList<Object>();
        for (Iterator<Object> it = updatingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            for (String id : idInUpdating) {
                if (atom instanceof LabelI && ((LabelI) atom).getId().equals(id)) {
                    moveToPremiseAtoms.add(atom);
                    it.remove();
                    break;
                } else if (atom instanceof KindI && ((KindI) atom).getId().equals(id)) {
                    moveToPremiseAtoms.add(atom);
                    it.remove();
                    break;
                } else if (atom instanceof PropertyI && ((PropertyI) atom).getId().equals(id)) {
                    moveToPremiseAtoms.add(atom);
                    it.remove();
                    break;
                }
            }
        }
        List<Object> moveToConclusionAtoms = new ArrayList<Object>();
        for (Iterator<Object> it = readingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            boolean find = false;
            for (String id : idInUpdating) {
                if (!(atom instanceof LabelI) && !(atom instanceof KindI) && !(atom instanceof PropertyI)) { // to avoid affect other atom kinds such as Connection
                    find = true;
                    break;
                }
                if (atom instanceof LabelI && ((LabelI) atom).getId().equals(id)) {
                    find = true;
                    break;
                } else if (atom instanceof KindI && ((KindI) atom).getId().equals(id)) {
                    find = true;
                    break;
                } else if (atom instanceof PropertyI && ((PropertyI) atom).getId().equals(id)) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                moveToConclusionAtoms.add(atom);
                it.remove();
            }
        }
        readingEncoding.addAll(moveToPremiseAtoms);
        updatingEncoding.addAll(moveToConclusionAtoms);

        // move all comparison atoms (Eq_I, Lt_I...) of which both vIds are related to idInUpdating to premise, others to conclusion
        List<CompI> compAtomList = new ArrayList<CompI>();
        Set<String> vIdInUpdating = new HashSet<String>();
        for (Iterator<Object> it = readingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            if (atom instanceof CompI) {
                compAtomList.add((CompI) atom);
                it.remove();
            } else if (atom instanceof PropertyI) { // find vIds related to idInUpdating
                if (idInUpdating.contains(((PropertyI) atom).getId()))
                    vIdInUpdating.add(((PropertyI) atom).getvId());
            }
        }
        for (Iterator<Object> it = updatingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            if (atom instanceof CompI) {
                compAtomList.add((CompI) atom);
                it.remove();
            } else if (atom instanceof PropertyI) { // find vIds related to idInUpdating
                if (idInUpdating.contains(((PropertyI) atom).getId()))
                    vIdInUpdating.add(((PropertyI) atom).getvId());
            }
        }
        for (CompI atom : compAtomList) {
            if (vIdInUpdating.contains(atom.getvId1()) && vIdInUpdating.contains(atom.getvId2()))
                readingEncoding.add(atom);
            else
                updatingEncoding.add(atom);
        }

        // exchange ValueI atoms and ConnectionI atoms
        moveToPremiseAtoms = new ArrayList<Object>();
        for (Iterator<Object> it = updatingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            if (atom instanceof ConnectionI) {
                moveToPremiseAtoms.add(atom);
                it.remove();
            } else if (atom instanceof ValueI) {
                moveToPremiseAtoms.add(atom);
                it.remove();
            }
        }
        moveToConclusionAtoms = new ArrayList<Object>();
        for (Iterator<Object> it = readingEncoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            if (atom instanceof ConnectionI) {
                moveToConclusionAtoms.add(atom);
                it.remove();
            } else if (atom instanceof ValueI) {
                moveToConclusionAtoms.add(atom);
                it.remove();
            }
        }
        readingEncoding.addAll(moveToPremiseAtoms);
        updatingEncoding.addAll(moveToConclusionAtoms);

        // remove redundant KindI if LabelI exists
        removeRedundantKind(readingEncoding);
        removeRedundantKind(updatingEncoding);
    }

    private static void removeRedundantKind(List<Object> encoding) {
        Set<String> idInLabelI = new HashSet<String>();
        for (Object atom : encoding) {
            if (atom instanceof LabelI)
                idInLabelI.add(((LabelI) atom).getId());
        }
        for (Iterator<Object> it = encoding.iterator(); it.hasNext();) {
            Object atom = it.next();
            for (String id : idInLabelI) {
                if (atom instanceof KindI && ((KindI) atom).getId().equals(id)) {
                    it.remove();
                    break;
                }
            }
        }
    }

    public static String propertyExistenceConstraint(String label, boolean nOrE, Set<String> pNames) {
        Constraint constraint = new Constraint();
        //        String str = "";
        //        if(nOrE)
        //            str += new LabelI("n", label).toString() + " -> " + new KindI("n", "\"N\"");
        //        else
        //            str += new LabelI("e", label).toString() + " -> " + new KindI("e", "\"E\"");
        //        for(String pName: pNames)
        //            str += ", " + new PropertyI("n", pName, constraint.getNextDtId(), constraint.getNextVId());
        //        str += ";";
        String premise, str = "";
        if (nOrE) {
            premise = new LabelI("n", label).toString();
            str += premise + " -> " + new KindI("n", "\"N\"") + ";\n";
        } else {
            premise = new LabelI("e", label).toString();
            str += premise + " -> " + new KindI("e", "\"E\"") + ";\n";
        }
        for (String pName : pNames) {
            str += premise + " -> " + new PropertyI("n", pName, "dtId", "vId") + ";\n";
        }
        return str;
    }

    public static String viewSetSemanticsConstraint(String label, Set<String> pNames) {
        Constraint constraint = new Constraint();
        String str = new LabelI("id1", label).toString() + ", " + new LabelI("id2", label).toString();
        for (String pName : pNames) {
            String dtId1 = constraint.getNextDtId(), vId1 = constraint.getNextVId(), dtId2 = constraint.getNextDtId(),
                    vId2 = constraint.getNextVId();
            str += ", " + new PropertyI("id1", pName, dtId1, vId1);
            str += ", " + new PropertyI("id2", pName, dtId2, vId2);
            str += ", " + new EqI(vId1, vId2);
        }
        str += " -> id1 = id2;";
        return str + "\n";
    }
}
