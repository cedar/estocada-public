package fr.inria.oak.estocada.rewriting.decoder.pr;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;

public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/test/resources/compiler/rq/test01-Translator/rewriting");
    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        catalog.delete();
        // Register
        final StorageReference storageReferencePostgresSQL1 = getPostgresSQLStorageReference("V_PATIENTS");
        // Columns names
        final List<String> colNames = new ArrayList<String>();
        colNames.add("SUBJECT_ID");
        colNames.add("HADM_ID");

        // NRSMD
        final NRSMD NRSMD = new NRSMD(2,
                new TupleMetadataType[] { TupleMetadataType.INTEGER_TYPE, TupleMetadataType.INTEGER_TYPE }, colNames);
        final Map<String, Integer> serviceNRSMDMapping = new HashMap<>();
        serviceNRSMDMapping.put("SUBJECT_ID", 0);
        serviceNRSMDMapping.put("HADM_ID", 1);
        ViewSchema serviceSchema = new ViewSchema(NRSMD, serviceNRSMDMapping);
        catalog.add("V_PATIENTS", storageReferencePostgresSQL1, null, serviceSchema);

        /*final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        final PJTranslator pjTranslator = new PJTranslator(rw, serviceSchema.getNRSMD());
        System.out.print(pjTranslator.translate());*/

        //        final ConjunctiveQuery rw = Utils.parseQuery((INPUT_QUERY_FILE));
        //        final LogOperator pjTranslator = new PRDecoder(Semantics.SET).decode(catalog, rw);
        //        System.out.print(pjTranslator.getNRSMD().toString());
    }

    private static StorageReference getPostgresSQLStorageReference(String collectionName) throws Exception {
        final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/mimic?user=postgres&password=postgres";
        StorageReference gsr = new GSR(collectionName);
        gsr.setProperty("url", POSTGRES_URL);
        return gsr;
    }
}
