/**
 * 
 */
package fr.inria.oak.estocada.extension.BigDAWG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.derby.jdbc.EmbeddedDriver;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import istc.bigdawg.LoggerSetup;
import istc.bigdawg.migration.MigrationResult;
import istc.bigdawg.postgresql.PostgreSQLHandler;

/**
 * BigDAWG Data Migration from PostgreRQ to PostgresJQ.
 * 
 * @author Rana Alotaibi
 */
public class FromPostgresRQToPostgresJQ extends Cancelable {

    private static Logger logger = Logger.getLogger(FromPostgresRQToPostgresJQ.class);
    private String TABLE;
    private String DBRQ_URL;
    private String DBJQ_URL;
    private String CREATE_TABLE;

    public FromPostgresRQToPostgresJQ(final String DBRQ_URL, final String DBJQ_URL, final String TABLE,
            final String CREATE_TABLE) {
        this.DBRQ_URL = DBRQ_URL;
        this.DBJQ_URL = DBJQ_URL;
        this.TABLE = TABLE;
        this.CREATE_TABLE = CREATE_TABLE;
        LoggerSetup.setLogging();
    }

    public MigrationResult fromPostgresRQToPostgresJQ(final String postgresRQQuery) {

        try {
            DriverManager.registerDriver(new EmbeddedDriver());
            Connection conRQ = DriverManager.getConnection(DBJQ_URL);

            DriverManager.registerDriver(new EmbeddedDriver());
            Connection conJQ = DriverManager.getConnection(DBRQ_URL);
            BigDAWGFromPostgresRQToPostgresJQMigrator fromsPostgresRQToPostgresrJQ = null;
            try {
                conJQ.setReadOnly(false);
            } catch (SQLException e1) {
                logger.error("Cannot create connection to PostgreSQL.");
                e1.printStackTrace();
                return null;
            }
            // Connect to PostgresRQ and create a table for incoming results from PostgresRQ

            try {
                PostgreSQLHandler.executeStatement(conJQ, "DROP TABLE IF EXISTS " + TABLE);
                PostgreSQLHandler.createTargetTableSchema(conJQ, TABLE, CREATE_TABLE);
            } catch (SQLException e1) {
                logger.error("Could not create table in PostgreSQL.");
                e1.printStackTrace();
                System.exit(1);
            }
            final Cancelable cancelable = new Cancelable();
            cancelable.startTimer(Cancelable.getPostgresConnectionsList(conRQ, conJQ));
            fromsPostgresRQToPostgresrJQ = new BigDAWGFromPostgresRQToPostgresJQMigrator(conRQ, conJQ);
            try {
                MigrationResult res = fromsPostgresRQToPostgresrJQ.fromPostgresRQToPostgresJQ(postgresRQQuery, TABLE);
                cancelable.cancelTimer();
                return res;
            } catch (Exception e) {
                logger.error("Could not migrate data from PostgresRQ to PostgresJQ.");
                e.printStackTrace();
            }
            logger.debug("The data was migrated from PostgresRQ to PostgresJQ.");

        } catch (SQLException e2) {
            Log.error("Could not create instance of fromPostgresJQtoPostgresRQ");
            e2.printStackTrace();
        } finally {
        }
        return null;
    }
}
