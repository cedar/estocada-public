// Generated from TFM.g4 by ANTLR 4.4

package fr.inria.oak.estocada.compiler.model.tm;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TFMParser}.
 */
public interface TFMListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TFMParser#matrixName}.
	 * @param ctx the parse tree
	 */
	void enterMatrixName(@NotNull TFMParser.MatrixNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#matrixName}.
	 * @param ctx the parse tree
	 */
	void exitMatrixName(@NotNull TFMParser.MatrixNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixAdjointExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixAdjointExpression(@NotNull TFMParser.MatrixAdjointExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixAdjointExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixAdjointExpression(@NotNull TFMParser.MatrixAdjointExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#tfmMatrixConstructionMatrixSource}.
	 * @param ctx the parse tree
	 */
	void enterTfmMatrixConstructionMatrixSource(@NotNull TFMParser.TfmMatrixConstructionMatrixSourceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#tfmMatrixConstructionMatrixSource}.
	 * @param ctx the parse tree
	 */
	void exitTfmMatrixConstructionMatrixSource(@NotNull TFMParser.TfmMatrixConstructionMatrixSourceContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#source}.
	 * @param ctx the parse tree
	 */
	void enterSource(@NotNull TFMParser.SourceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#source}.
	 * @param ctx the parse tree
	 */
	void exitSource(@NotNull TFMParser.SourceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixAddExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixAddExpression(@NotNull TFMParser.MatrixAddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixAddExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixAddExpression(@NotNull TFMParser.MatrixAddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulElementwiseExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulElementwiseExpression(@NotNull TFMParser.MatrixMulElementwiseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulElementwiseExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulElementwiseExpression(@NotNull TFMParser.MatrixMulElementwiseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixDetExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixDetExpression(@NotNull TFMParser.MatrixDetExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixDetExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixDetExpression(@NotNull TFMParser.MatrixDetExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulExpression(@NotNull TFMParser.MatrixMulExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulExpression(@NotNull TFMParser.MatrixMulExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixDivision}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixDivision(@NotNull TFMParser.MatrixDivisionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixDivision}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixDivision(@NotNull TFMParser.MatrixDivisionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MeanExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMeanExpression(@NotNull TFMParser.MeanExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MeanExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMeanExpression(@NotNull TFMParser.MeanExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#viewName}.
	 * @param ctx the parse tree
	 */
	void enterViewName(@NotNull TFMParser.ViewNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#viewName}.
	 * @param ctx the parse tree
	 */
	void exitViewName(@NotNull TFMParser.ViewNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#tfmMatrixConstruction}.
	 * @param ctx the parse tree
	 */
	void enterTfmMatrixConstruction(@NotNull TFMParser.TfmMatrixConstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#tfmMatrixConstruction}.
	 * @param ctx the parse tree
	 */
	void exitTfmMatrixConstruction(@NotNull TFMParser.TfmMatrixConstructionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixMulScalarExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixMulScalarExpression(@NotNull TFMParser.MatrixMulScalarExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixMulScalarExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixMulScalarExpression(@NotNull TFMParser.MatrixMulScalarExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixInvExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixInvExpression(@NotNull TFMParser.MatrixInvExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixInvExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixInvExpression(@NotNull TFMParser.MatrixInvExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixNormExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixNormExpression(@NotNull TFMParser.MatrixNormExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixNormExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixNormExpression(@NotNull TFMParser.MatrixNormExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#filePath}.
	 * @param ctx the parse tree
	 */
	void enterFilePath(@NotNull TFMParser.FilePathContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#filePath}.
	 * @param ctx the parse tree
	 */
	void exitFilePath(@NotNull TFMParser.FilePathContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#tfmScript}.
	 * @param ctx the parse tree
	 */
	void enterTfmScript(@NotNull TFMParser.TfmScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#tfmScript}.
	 * @param ctx the parse tree
	 */
	void exitTfmScript(@NotNull TFMParser.TfmScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#tfmStatemnet}.
	 * @param ctx the parse tree
	 */
	void enterTfmStatemnet(@NotNull TFMParser.TfmStatemnetContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#tfmStatemnet}.
	 * @param ctx the parse tree
	 */
	void exitTfmStatemnet(@NotNull TFMParser.TfmStatemnetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixIdentifier}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixIdentifier(@NotNull TFMParser.MatrixIdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixIdentifier}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixIdentifier(@NotNull TFMParser.MatrixIdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#matrixNameExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixNameExpression(@NotNull TFMParser.MatrixNameExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#matrixNameExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixNameExpression(@NotNull TFMParser.MatrixNameExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixDiagonalExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixDiagonalExpression(@NotNull TFMParser.MatrixDiagonalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixDiagonalExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixDiagonalExpression(@NotNull TFMParser.MatrixDiagonalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#numericScalar}.
	 * @param ctx the parse tree
	 */
	void enterNumericScalar(@NotNull TFMParser.NumericScalarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#numericScalar}.
	 * @param ctx the parse tree
	 */
	void exitNumericScalar(@NotNull TFMParser.NumericScalarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixTraceExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixTraceExpression(@NotNull TFMParser.MatrixTraceExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixTraceExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixTraceExpression(@NotNull TFMParser.MatrixTraceExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixTransposeExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixTransposeExpression(@NotNull TFMParser.MatrixTransposeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixTransposeExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixTransposeExpression(@NotNull TFMParser.MatrixTransposeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#importstatement}.
	 * @param ctx the parse tree
	 */
	void enterImportstatement(@NotNull TFMParser.ImportstatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#importstatement}.
	 * @param ctx the parse tree
	 */
	void exitImportstatement(@NotNull TFMParser.ImportstatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixRankExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixRankExpression(@NotNull TFMParser.MatrixRankExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixRankExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixRankExpression(@NotNull TFMParser.MatrixRankExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MatrixSubExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void enterMatrixSubExpression(@NotNull TFMParser.MatrixSubExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MatrixSubExpression}
	 * labeled alternative in {@link TFMParser#tfmExpression}.
	 * @param ctx the parse tree
	 */
	void exitMatrixSubExpression(@NotNull TFMParser.MatrixSubExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TFMParser#tfmQuery}.
	 * @param ctx the parse tree
	 */
	void enterTfmQuery(@NotNull TFMParser.TfmQueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TFMParser#tfmQuery}.
	 * @param ctx the parse tree
	 */
	void exitTfmQuery(@NotNull TFMParser.TfmQueryContext ctx);
}