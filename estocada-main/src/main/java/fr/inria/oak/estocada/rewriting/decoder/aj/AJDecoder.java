package fr.inria.oak.estocada.rewriting.decoder.aj;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.operators.logical.LogAQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.estocada.rewriting.logical.Decoder;

/**
 * AQLDecoder implements decoder interface and returns LogAQLEval.
 * 
 * @author ranaalotaibi
 *
 */
public class AJDecoder implements Decoder {
    private static final Logger logger = Logger.getLogger(AJDecoder.class);

    @Override
    public LogOperator decode(Catalog catalog, ConjunctiveQuery cq) throws TatooineExecutionException {
        final AJTranslatorOld trans = new AJTranslatorOld();
        final String query = trans.translate(cq);
        final NRSMD queryNRSMD = inferNRSDM(catalog, cq);
        final String viewName = cq.getBody().stream().filter(atom -> atom.getTerms().size() == 1)
                .collect(Collectors.reducing((a, b) -> null)).get().getPredicate().toString();
        return new LogAQLEval(query, queryNRSMD, catalog.getStorageReference(viewName));
    }

    /**
     *
     * Gets the NRSDM for the result of the query
     *
     * @param catalog
     *            provides the views' schemas
     * @param cq
     *            subset of the re-written query for JQ model
     * @return
     * @throws TatooineExecutionException
     */
    private NRSMD inferNRSDM(Catalog catalog, ConjunctiveQuery cq) throws TatooineExecutionException {

        final String viewName = cq.getBody().stream().filter(atom -> atom.getTerms().size() == 1)
                .collect(Collectors.reducing((a, b) -> null)).get().getPredicate().toString();
        final NRSMD viewNRSMD = catalog.getViewSchema(viewName).getNRSMD();
        final Map<String, Integer> viewMapping = catalog.getViewSchema(viewName).getColNameToIndexMapping();
        final Map<Term, Term> headVariablesMapping = getHeadVariableMapping(cq);
        TupleMetadataType[] newMetaData = new TupleMetadataType[cq.getHead().size()];

        for (Iterator<Atom> atom = cq.getBody().iterator(); atom.hasNext();) {
            Atom currentAtom = atom.next();
            if (!(currentAtom.getTerms().size() == 1 || currentAtom.getTerms().size() == 2)) {
                if (headVariablesMapping.containsKey(currentAtom.getTerms().get(1))) {
                    final int columnIndex =
                            viewMapping.get(currentAtom.getTerms().get(2).toString().replaceAll("\"", ""));
                    final int metadataInex =
                            cq.getHead().indexOf(headVariablesMapping.get(currentAtom.getTerms().get(1)));
                    final String columnMetaData = viewNRSMD.getColumnMetadata(columnIndex);
                    switch (columnMetaData) {
                        case "STRING_TYPE":
                            newMetaData[metadataInex] = TupleMetadataType.STRING_TYPE;
                            break;
                        case "INTEGER_TYPE":
                            newMetaData[metadataInex] = TupleMetadataType.INTEGER_TYPE;
                            break;
                        case "TUPLE_TYPE":
                            newMetaData = viewNRSMD.getNestedChild(columnIndex).getColumnsMetadata();
                            break;
                    }
                }
            } else
                continue;
        }
        return new NRSMD(newMetaData.length, newMetaData);
    }

    /**
     * Return the mapping for variables that appear in the head of the query
     *
     * @param cq
     * @return headVariablesMapping
     */
    private Map<Term, Term> getHeadVariableMapping(ConjunctiveQuery cq) {
        final List<Term> headVariables = cq.getHead();
        boolean valAtom = false;
        BiMap<Term, Term> headVariablesMapping = HashBiMap.create();
        for (Iterator<Atom> i = cq.getBody().iterator(); i.hasNext();) {
            Atom atom = i.next();
            if ((atom.getPredicate().toString().startsWith("eq") && headVariables.contains(atom.getTerm(1)))) {
                if (!valAtom) {
                    if (headVariablesMapping.containsValue(atom.getTerm(1))) {
                        headVariablesMapping.remove(atom.getTerm(1));
                        headVariablesMapping.put(atom.getTerm(1), atom.getTerm(1));

                    } else
                        headVariablesMapping.put(atom.getTerm(0), atom.getTerm(1));
                } else
                    headVariablesMapping.put(headVariablesMapping.inverse().get(atom.getTerm(0)), atom.getTerm(1));
            }
            if (atom.getPredicate().toString().startsWith("val")) {
                valAtom = true;
                headVariablesMapping.put(atom.getTerm(0), atom.getTerm(1));
            }
            if ((atom.getTerms().size() > 2 && headVariables.contains(atom.getTerm(1)))) {
                headVariablesMapping.put(atom.getTerm(1), atom.getTerm(1));
            }
        }
        return headVariablesMapping;
    }
}
