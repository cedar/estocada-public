package fr.inria.oak.estocada.views.materialization.module;

import java.io.IOException;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.oak.estocada.qbt.walker.QBTMLexer;
import fr.inria.oak.estocada.qbt.walker.QBTMParser;

/**
 * QBT View Listener.
 * 
 * @author ranaalotaibi
 *
 */
public final class QBTViewListener {
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(QBTViewListener.class);

    /** Constructor **/
    public QBTViewListener() throws MaterializerException {
        try {
            Parameters.init();
        } catch (IOException | TatooineException exception) {
            LOGGER.error("Tatooine config can not be initialized:" + exception.getMessage());
            throw new MaterializerException(exception);
        }
    }

    /**
     * Walks the view QBT.
     * 
     * @param qbtQuery
     *            the qbt query that needs to be parsed and executed.
     * @throws ParseException
     */
    public void run(final String qbtQuery) throws MaterializerException {
        final QBTMLexer lexer = new QBTMLexer(CharStreams.fromString(qbtQuery));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTMParser parser = new QBTMParser(tokens);
        try {
            /** Parse QBT pattern clause **/
            final ParserRuleContext tree = parser.mixedqbt();
            final ParseTreeWalker patternWalker = new ParseTreeWalker();
            final QBTBlockListenerAux blocklistener = new QBTBlockListenerAux();
            patternWalker.walk(blocklistener, tree);

            /** Parse QBT return clause **/
            final ParseTreeWalker returnWalker = new ParseTreeWalker();
            final QBTReturnListenerAux returnListener =
                    new QBTReturnListenerAux(blocklistener.getViewName(), blocklistener.getQBTBlockRoot());

            returnWalker.walk(returnListener, tree);
        } catch (IllegalStateException excpetion) {
            throw new MaterializerException(excpetion);
        }
    }
}
