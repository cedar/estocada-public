package fr.inria.oak.estocada.compiler.model.cg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Utils {

    public static ParseTree parseCypher(String cypherQuery) {
        CharStream input = CharStreams.fromString(cypherQuery);
        CypherLexer lexer = new CypherLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CypherParser parser = new CypherParser(tokens);
        ParseTree tree = parser.oC_Cypher();
        //        System.out.println(tree.toStringTree(parser)+"\n");
        return tree;
    }

    public static ParseTree parseCQ(String cqQuery) {
        CharStream input = CharStreams.fromString(cqQuery);
        ConjunctiveQueryLexer lexer = new ConjunctiveQueryLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ConjunctiveQueryParser parser = new ConjunctiveQueryParser(tokens);
        ParseTree tree = parser.conjunctiveQuery();
        //        System.out.println(tree.toStringTree(parser)+"\n");
        return tree;
    }

    public static String readFile(String fileName, boolean inThisProject) { // inThisProject     true: under "src/main/resources/";  false: can be anywhere, just use the fileName as filePath
        //if (inThisProject)
        //fileName = "src/main/resources/" + fileName;
        String encoding = "UTF-8";
        File file = new File(fileName);
        Long fileLength = file.length();
        byte[] fileContent = new byte[fileLength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(fileContent);
            in.close();
            return new String(fileContent, encoding);
        } catch (Exception e) {
            System.err.println("The OS does not support " + encoding);
            e.printStackTrace();
            return null;
        }
    }

    public static void writeFile(File file, boolean append, String content) throws IOException {
        FileWriter fileWriter = new FileWriter(file, append);
        fileWriter.write(content);
        fileWriter.flush();
        fileWriter.close();
    }
}
