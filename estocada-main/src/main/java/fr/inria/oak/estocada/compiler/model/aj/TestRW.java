package fr.inria.oak.estocada.compiler.model.aj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.apache.commons.io.IOUtils;

import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.TimedReformulations;

public class TestRW {
    public static void main(String[] args) throws Exception {
        final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testAJ3-Filter/constraints_chase";
        final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testAJ3-Filter/constraints_bkchase";
        final String OUTPUT_SCHEMA_FILE = "src/main/resources/testAJ3-Filter/schemas";

        final Context context01 = fr.inria.oak.estocada.rewriter.server.Utils.parseContext(OUTPUT_SCHEMA_FILE,
                OUTPUT_FORWARD_CONSTRAINTS_FILE, OUTPUT_BACKWARD_CONSTRAINTS_FILE);
        final ConjunctiveQueryRewriter rewriter = new PACBConjunctiveQueryRewriter(context01);

        final BufferedReader query =
                new BufferedReader(new FileReader(new File("src/main/resources/testAJ3-Filter/query")));
        final TimedReformulations timedRewritings = rewriter.getTimedReformulations(
                fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query)));
        System.out.println(timedRewritings.getRewritings());

    }

}
