package fr.inria.oak.estocada.main.scenarios;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;

import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.loader.Catalog;
import fr.inria.cedar.commons.tatooine.loader.ViewSchema;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.physical.BindAccess;
import fr.inria.cedar.commons.tatooine.operators.physical.PhyPostgresJSONEval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySOLREval;
import fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval;
import fr.inria.cedar.commons.tatooine.operators.physical.join.PhyBindJoin;
import fr.inria.oak.commons.miscellaneous.FileUtils;

/**
 * Run MIMIC Scenarios on Tatooine (fixed plan order)
 * 
 * @author ranaalotaibi
 *
 */
public class TatooineScenariosMIMIC {
    private static final Logger LOGGER = Logger.getLogger(TatooineScenariosMIMIC.class);

    public static long QueryEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing Query");
        final String queryPJ = FileUtils.readStringFromFile(path + "PJQuery.query");
        final ViewSchema gdeltSchema = catalog.getViewSchema("MIMIC");
        final GSR gsrRE = (GSR) catalog.getStorageReference("MIMIC");

        final long start = System.nanoTime();
        final PhyPostgresJSONEval queryEval = new PhyPostgresJSONEval(queryPJ, gsrRE, gdeltSchema.getNRSMD());
        int resultCount = 0;
        queryEval.open();
        while (queryEval.hasNext()) {
            NTuple next = queryEval.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;
    }

    public static long Q1RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        //final String queryText = "intracranial";
        //final String queryText = "hemorrhag";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q2RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW2");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q3RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q4RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW4");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q5RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW5");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q6RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q7RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q8RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q9RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q10RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "cannulaes";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q11RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q12RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q13RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q14RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q15RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q16RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW2");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q17RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q18RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW4");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q19RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW5");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q20RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q21RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q22RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q23RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q24RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "cannulaes";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q25RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q26RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q27RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;
    }

    public static long Q28RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V4_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V4_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q29RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        //final String queryText = "intracranial";
        //final String queryText = "hemorrhag";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q30RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW2");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q31RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q32RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW4");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q33RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW5");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V2_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V2_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "embolism";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q34RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q35RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q36RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q37RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q38RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q39RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q40RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V3_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V3_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "intracranial";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q41RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q42RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q43RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "pneumonia";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q44RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "specis";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q45RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "tachypnea";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q46RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V5_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V5_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        //SolrQuery
        final String queryText = "pneumonia";
        final ViewSchema articlesNRSMD = catalog.getViewSchema("mimic_notes");
        final GSR gsrText = (GSR) catalog.getStorageReference("mimic_notes");

        final PhySOLREval qyertTextEval = new PhySOLREval(queryText, "subject_id", gsrText, articlesNRSMD.getNRSMD());
        final BindAccess queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final PhyBindJoin leftBindJoin = new PhyBindJoin(qyertTextEval, queryREval, new int[] { 0 });
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(leftBindJoin, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    //-----NO TEXT PREDICATES------//

    public static long Q50RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q51RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q52RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q53RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q54RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW3");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("VPJPJJSON");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("VPJPJJSON");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q55RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V6_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V6_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJRE");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJRE");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q56RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V8_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V8_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q57RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V8_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V8_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        System.out.println(executionTime);
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q58RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V8_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V8_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        System.out.println(executionTime);
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

    public static long Q59RewritingEval(final String path) throws Exception {
        Parameters.init();
        Catalog catalog = Catalog.getInstance();
        LOGGER.info("Start Executing RW1");
        final long start = System.nanoTime();
        //Query PJ
        final String queryPJ = FileUtils.readStringFromFile(path + "queryPJ");
        final ViewSchema VREPJSchema = catalog.getViewSchema("V8_JSONT");
        final GSR gsrVREPJ = (GSR) catalog.getStorageReference("V8_JSONT");

        //Query RE
        final String queryRE = FileUtils.readStringFromFile(path + "queryRE");
        final ViewSchema VREreSchema = catalog.getViewSchema("VPJREM");
        final GSR gsrVRERE = (GSR) catalog.getStorageReference("VPJREM");

        final PhySQLEval queryREval = new PhySQLEval(queryRE, gsrVRERE, VREreSchema.getNRSMD());
        final BindAccess queryPJval = new PhyPostgresJSONEval(queryPJ, gsrVREPJ, VREPJSchema.getNRSMD());
        final PhyBindJoin righBindjoin = new PhyBindJoin(queryREval, queryPJval, new int[] { 0 });
        int resultCount = 0;
        righBindjoin.open();
        while (righBindjoin.hasNext()) {
            NTuple next = righBindjoin.next();
            final JsonObject obj = new JsonObject();
            obj.addProperty("SID", next.getIntegerField(0));
            obj.addProperty("DOB", String.valueOf(next.getStringField(2)));
            resultCount++;
        }
        final long executionTime = System.nanoTime() - start;
        System.out.println(executionTime);
        LOGGER.info("Count=" + resultCount + " Took: " + (executionTime * 1e-9) + "s");
        return executionTime;

    }

}
