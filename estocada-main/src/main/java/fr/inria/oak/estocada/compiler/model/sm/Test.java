package fr.inria.oak.estocada.compiler.model.sm;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.estocada.compiler.QueryBlockTree;

public class Test {
    private static final Logger LOGGER = Logger.getLogger(Test.class);
    private static final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testSM/constraints_chase";
    private static final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testSM/constraints_bkchase";
    private static final String OUTPUT_SCHEMA_FILE = "src/main/resources/testSM/schemas";
    private static final String INPUT_QUERY_FILE = "src/main/resources/testSM/v.view";

    private static final int COMPILER = 0;

    public static void main(String[] args) throws Exception {
        Injector injector = null;
        switch (COMPILER) {
            case 0:
                injector = Guice.createInjector(new SMNaiveModule());
                break;
        }
        SMQueryBlockTreeBuilder builder = injector.getInstance(SMQueryBlockTreeBuilder.class);
        final String str = FileUtils.readFileToString(new File(INPUT_QUERY_FILE));
        final QueryBlockTree nbt = builder.buildQueryBlockTree(str);
        System.out.println(nbt.toString());
        //        SMQueryBlockTreeBuilder builder = injector.getInstance(SMQueryBlockTreeBuilder.class);
        //        final String str = FileUtils.readFileToString(new File(INPUT_QUERY_FILE));
        //        final String strNew = str.substring(0, str.indexOf("write"));
        //        final String writeNew = str.substring(str.indexOf("write"));
        //        final String viewNameBefore = writeNew.split(",")[1];
        //        final String viewNameAfter = viewNameBefore.substring(0, viewNameBefore.indexOf(")"));
        //        final QueryBlockTree nbt = builder.buildQueryBlockTree(strNew);
        //        System.out.println("\n" + nbt.toString());
        //        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        //        final List<Atom> atoms = new ArrayList<>();
        //        final StringBuilder strBuilder = new StringBuilder();
        //        Atom sizeAtom = null;
        //        final String returnVar =
        //                nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();
        //        for (final PathExpression path : paths) {
        //            atoms.addAll(path.encoding());
        //        }
        //        int i = 0;
        //        for (final Atom atom : atoms) {
        //            if (atom.getPredicate().contains("size")) {
        //                sizeAtom = atom;
        //            }
        //            strBuilder.append(atom);
        //            if (i != atoms.size() - 1)
        //                strBuilder.append(",");
        //            i++;
        //        }
        //        strBuilder.append("->");
        //        strBuilder.append("name");
        //        strBuilder.append("(");
        //        strBuilder.append(returnVar);
        //        strBuilder.append(",");
        //        strBuilder.append(viewNameAfter);
        //        strBuilder.append(")");
        //        strBuilder.append(",");
        //        strBuilder.append("size(");
        //        strBuilder.append(returnVar);
        //        strBuilder.append(",");
        //        strBuilder.append(sizeAtom.getTerm(1));
        //        strBuilder.append(",");
        //        strBuilder.append(sizeAtom.getTerm(2));
        //        strBuilder.append(")");
        //        System.out.println("\n" + strBuilder.toString());

    }
}
