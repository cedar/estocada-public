package fr.inria.oak.estocada.compiler.model.pm;

/**
 * PM predicates
 * 
 * @author ranaalotaibi
 *
 */
public enum Predicate {
    NAME("name_pm"),
    SIZE("size_pm"),
    ZERO("zero_pm"),
    IDENTITY("identity_pm"),
    ADD("add_pm"),
    MULTI("multi_pm"),
    MULTIS("multi_s_pm"),
    MULTIE("multi_e_pm"),
    DIV("div_pm"),
    DIVS("div_s_pm"),
    TRANS("tr_pm"),
    INVERSE("inv_pm"),
    DET("det_pm"),
    ADJ("adj_pm"),
    COFA("cof_pm"),
    TRACE("trace_pm"),
    DIAG("diag_pm"),
    POW("pow_pm"),
    AVG("avg_pm"),
    MEAN("mean_pm"),
    VAR("var_pm"),
    SD("sd_pm"),
    COLSUM("colSums_pm"),
    COLMEAN("colMeans_pm"),
    COLVARS("colVars_pm"),
    COLSDS("colSds_pm"),
    COLMAX("colMaxs_pm"),
    COLMINS("colMins_pm"),
    ROWSUM("rowSums_pm"),
    ROWMEAN("rowMeans_pm"),
    ROWVARS("rowVars_pm"),
    ROWSDS("rowSds_pm"),
    ROWMAX("rowMaxs_pm"),
    ROWMIN("rowMins_pm"),
    CUMSUM("cumsum_pm"),
    CUMPROD("cumprod_pm"),
    CUMMIN("cummin_pm"),
    CUMMAX("cummax_pm"),
    EQUALS("eq_pm");

    private final String str;

    private Predicate(final String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
