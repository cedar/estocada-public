package fr.inria.oak.estocada.rewriting.decoder.sppj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Queue;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.Term;

/**
 * Translate the rewriting to SPPJQL query
 * 
 * @author ranaalotaibi
 *
 */
public class SPPJTranslator {

    /**
     * Some characters and keywords that are used in AQL Surface syntax
     */
    private static final Character DOT = '.';
    private static final String SELECT = " SELECT ";
    private static final String FROM = " FROM ";
    private static final String AS = " AS ";
    private static final String WHERE = " WHERE ";
    private static final String COMMA = ",";
    private ConjunctiveQuery query;

    public SPPJTranslator(final ConjunctiveQuery query) {
        this.query = query;
    }

    /**
     * Translate the given rewriting into PJQL query
     * 
     * @return
     */
    public String translate() {
        final StringBuilder pjqlQuery = new StringBuilder();
        final StringBuilder fromClause = new StringBuilder(trasnlateFromClause());
        final StringBuilder whereClause = new StringBuilder(trasnlateWhereClause());
        final StringBuilder selectClause = new StringBuilder(translateSelectClause());
        pjqlQuery.append(selectClause);
        pjqlQuery.append(fromClause);
        pjqlQuery.append(whereClause);
        return pjqlQuery.toString();
    }

    private String trasnlateFromClause() {
        final StringBuilder fromClause = new StringBuilder();
        final Collection<Atom> queryBody = new ArrayList<>(query.getBody());
        final List<Term> head = query.getHead();
        final Map<Term, Term> variableBindings = new HashMap<>();
        final Map<Term, Atom> variableAtomMpping = new HashMap<>();

        Term root = null;
        int countBinding = 0;
        for (final Atom atom : queryBody) {
            if (atom.getTerms().size() == 1) {
                fromClause.append(FROM);
                fromClause.append(atom.getPredicate());
                fromClause.append(AS);
                fromClause.append(atom.getTerm(0));
                root = atom.getTerm(0);
                if (queryBody.size() != 2) {
                    fromClause.append(COMMA);
                }
            }
            if (atom.getTerms().size() > 2) {
                variableBindings.put(atom.getTerm(0), atom.getTerm(1));
                variableAtomMpping.put(atom.getTerm(1), atom);
                countBinding++;
            }
        }

        while (countBinding != 0) {
            final Term term1 = variableBindings.get(root);
            final Atom atom1 = variableAtomMpping.get(term1);
            final Term term2 = variableBindings.get(term1);
            final Atom atom2 = variableAtomMpping.get(term2);
            root = term2;
            if (term2 != null && atom2 != null) {
                fromClause.append(atom1.getTerm(0).toString() + DOT + atom1.getTerm(2).toString().replace("\"", "") + AS
                        + atom2.getTerm(1).toString());
                fromClause.append(COMMA);
            }
            countBinding--;

        }

        if (!fromClause.substring(fromClause.toString().length() - 1).equals(COMMA))
            return fromClause.toString();
        else
            return fromClause.substring(0, fromClause.toString().length() - 1);
    }

    private String trasnlateWhereClause() {
        return "";
    }

    private String translateSelectClause() {
        final StringBuilder selectClause = new StringBuilder();
        final List<Atom> queryBody = new ArrayList<>(query.getBody());
        final List<Term> head = new ArrayList<>(query.getHead());
        selectClause.append(SELECT);

        for (final Term term : head) {
            Term temp = null;
            ListIterator<Atom> itr = queryBody.listIterator();
            Queue<Atom> queue = new LinkedList<>(queryBody);
            while (!queue.isEmpty()) {
                final Atom atom = queue.poll();
                if (atom.getPredicate().startsWith("eq") && atom.getTerm(1).equals(term)) {
                    temp = atom.getTerm(0);
                }
                if (atom.getTerms().size() == 1 && (atom.getTerm(0).equals(temp))) {
                    selectClause.append(temp);
                    break;

                } else {
                    queue.add(atom);
                }
                if (atom.getTerms().size() > 2 && (atom.getTerm(1).equals(temp))) {
                    if (atom.getTerm(3).toString().replace("\"", "").equals("a")) {
                        selectClause.append(temp);
                    } else {
                        selectClause.append(atom.getTerm(0));
                        selectClause.append(DOT);
                        selectClause.append(atom.getTerm(2).toString().replace("\"", ""));
                        selectClause.append(AS);
                        selectClause.append(temp);
                        selectClause.append(COMMA);
                    }
                    break;

                } else {
                    queue.add(atom);
                }

            }

        }
        if (!selectClause.substring(selectClause.toString().length() - 1).equals(COMMA))
            return selectClause.toString();
        else
            return selectClause.substring(0, selectClause.toString().length() - 1);
    }
}
