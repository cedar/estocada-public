package fr.inria.oak.estocada.compiler.model.pj.full.naive;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.estocada.compiler.ReturnConstructTerm;
import fr.inria.oak.estocada.compiler.ReturnStringTerm;
import fr.inria.oak.estocada.compiler.ReturnTemplate;
import fr.inria.oak.estocada.compiler.ReturnTemplateVisitor;
import fr.inria.oak.estocada.compiler.ReturnVariableTerm;
import fr.inria.oak.estocada.compiler.model.pj.ArrayElementFactory;
import fr.inria.oak.estocada.compiler.model.pj.Predicate;
import fr.inria.oak.estocada.compiler.model.pj.Utils;

@Singleton
public abstract class PJForwardEncoderReturnTermVisitor implements ReturnTemplateVisitor {
    protected final ArrayElementFactory arrayElementFactory;
    protected ImmutableList.Builder<Atom> builder;
    protected String viewName;
    protected Variable viewID;

    @Inject
    public PJForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory) {
        this.arrayElementFactory = arrayElementFactory;

    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, viewID, term.getVariable(),
                term.getParent().getElement().toTerm(), Utils.getDataType(term)));
    }

    @Override
    public void visit(final ReturnStringTerm term) {

    }
}