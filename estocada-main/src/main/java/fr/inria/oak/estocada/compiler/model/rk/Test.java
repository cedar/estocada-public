package fr.inria.oak.estocada.compiler.model.rk;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.relationalschema.Relation;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.estocada.compiler.QueryBlockTree;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveModule;
import fr.inria.oak.estocada.compiler.model.rk.naive.RKNaiveQueryBlockTreeCompiler;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;

public class Test {
    private static final String OUTPUT_FORWARD_CONSTRAINTS_FILE = "src/main/resources/testRK/constraints_chase";
    private static final String OUTPUT_BACKWARD_CONSTRAINTS_FILE = "src/main/resources/testRK/constraints_bkchase";
    private static final String OUTPUT_SCHEMA_FILE = "src/main/resources/testRK/schemas";
    private static final String INPUT_QUERY_FILE = "src/main/resources/testRK/v.view";
    private static final int COMPILER = 1;
    private static final Logger log = Logger.getLogger(Test.class);

    public static void main(String[] args) throws Exception {
        Injector injector = null;
        switch (COMPILER) {
            case 1:
                injector = Guice.createInjector(new RKNaiveModule());
                break;

        }

        RKQueryBlockTreeBuilder builder = injector.getInstance(RKQueryBlockTreeBuilder.class);
        final QueryBlockTree nbt = builder.buildQueryBlockTree(FileUtils.readFileToString(new File(INPUT_QUERY_FILE)));
        System.out.println(nbt.toString());
        final List<QueryBlockTree> views = new ArrayList<>();
        views.add(nbt);

        //        final Context context01 = fr.inria.oak.estocada.rewriter.server.Utils.parseContext(OUTPUT_SCHEMA_FILE,
        //                OUTPUT_FORWARD_CONSTRAINTS_FILE, OUTPUT_BACKWARD_CONSTRAINTS_FILE);
        //        final ConjunctiveQueryRewriter rewriter = new PACBConjunctiveQueryRewriter(context01);
        //        final BufferedReader query = new BufferedReader(new FileReader(new File("src/main/resources/testRK/query")));
        //        final TimedReformulations timedRewritings = rewriter.getTimedReformulations(
        //                fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query)));
        //        System.out.println(timedRewritings.getRewritings());

        final RKNaiveQueryBlockTreeCompiler compiler0 = injector.getInstance(RKNaiveQueryBlockTreeCompiler.class);
        Context context = compiler0.compileContext(nbt, new RelationalSchema(new ArrayList<Relation>()), true);
        final StringBuilder globalSchemaStrBuilder = new StringBuilder();
        context.getGlobalSchema().getRelations().stream()
                .forEach(r -> globalSchemaStrBuilder.append(r.toString()).append("\n"));

        final StringBuilder targetSchemaStrBuilder = new StringBuilder();
        context.getTargetSchema().getRelations().stream()
                .forEach(r -> targetSchemaStrBuilder.append(r.toString()).append("\n"));

        final StringBuilder fwStrBuilder = new StringBuilder();
        context.getForwardConstraints().stream().forEach(r -> fwStrBuilder.append(r.toString()).append("\n"));

        fr.inria.oak.estocada.compiler.Utils.writeConstraints(OUTPUT_FORWARD_CONSTRAINTS_FILE,
                context.getForwardConstraints());
        fr.inria.oak.estocada.compiler.Utils.writeConstraints(OUTPUT_BACKWARD_CONSTRAINTS_FILE,
                context.getBackwardConstraints());
        fr.inria.oak.estocada.compiler.Utils.writeSchemas(OUTPUT_SCHEMA_FILE, context.getGlobalSchema(),
                context.getTargetSchema());

        final BufferedReader query = new BufferedReader(new FileReader(new File("src/main/resources/testRK/query")));
        final ConjunctiveQuery cqquery =
                fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query));
        final List<ConjunctiveQueryRewriter> rewriters = createRewriters(views, injector);
        final Iterator<ConjunctiveQueryRewriter> it = rewriters.iterator();
        List<ConjunctiveQuery> reformulations = new ArrayList<ConjunctiveQuery>();
        while (it.hasNext()) {
            final ConjunctiveQueryRewriter rewriter = it.next();
            reformulations = rewriter.getReformulations(cqquery);
        }
        System.out.println(reformulations);

    }

    private static List<QueryBlockTreeViewCompiler> createCompilers(final Injector injector) {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(RKNaiveQueryBlockTreeCompiler.class));
        return compilers;
    }

    private static List<Context> createContexts(final List<QueryBlockTree> nbts, final Injector injector) {
        final List<Context> contexts = new ArrayList<Context>();
        createCompilers(injector).stream().forEach(c -> contexts.add(c.compileContext(nbts,
                new RelationalSchema(new ArrayList<fr.inria.oak.commons.relationalschema.Relation>()), false)));
        return contexts;
    }

    private static List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts,
            final Injector injector) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts, injector).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }
}
