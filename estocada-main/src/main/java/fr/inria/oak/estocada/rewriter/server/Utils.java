package fr.inria.oak.estocada.rewriter.server;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.parser.CQParser;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.parser.ConstraintParser;
import fr.inria.oak.commons.relationalschema.RelationalSchema;
import fr.inria.oak.commons.relationalschema.parser.RelSchemaParser;
import fr.inria.oak.estocada.rewriter.Context;

public final class Utils {
	public static List<Constraint> parseConstraints(final String fileName)
			throws IOException,
			fr.inria.oak.commons.constraints.parser.ParseException {
		final FileReader fr = new FileReader(fileName);
		final ConstraintParser parser = new ConstraintParser(fr);
		final List<Constraint> constraints = parser.parse();
		fr.close();
		return constraints;
	}

	public static List<RelationalSchema> parseSchemas(final String fileName)
			throws IOException,
			fr.inria.oak.commons.relationalschema.parser.ParseException {
		final FileReader fr = new FileReader(fileName);
		final RelSchemaParser parser = new RelSchemaParser(fr);
		final List<RelationalSchema> constraints = parser.parse();
		fr.close();
		return constraints;
	}

	public static Context parseContext(final String schemasFileName,
			final String forwardConstraintsFileName,
			final String backwardConstraintsFileName) throws IOException,
			fr.inria.oak.commons.relationalschema.parser.ParseException,
			fr.inria.oak.commons.constraints.parser.ParseException {
		final List<RelationalSchema> schemas = Utils
				.parseSchemas(schemasFileName);
		return new Context(schemas.get(0), schemas.get(1),
				Utils.parseConstraints(forwardConstraintsFileName),
				Utils.parseConstraints(backwardConstraintsFileName));
	}

	public static ConjunctiveQuery parseQuery(final String str)
			throws IllegalArgumentException,
			fr.inria.oak.commons.conjunctivequery.parser.ParseException {
		final CQParser parser = new CQParser(new StringReader(str));
		final Map<String, ConjunctiveQuery> queries = parser.parse();
		if (queries.size() != 1) {
			throw new IllegalArgumentException(
					"The query string is not a valid RDF CQ.");
		}
		return queries.values().iterator().next();
	}
}
