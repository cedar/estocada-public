Q = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'noteevents') AS NOTEVEENTS,
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'procedureevents_mv') AS  PROCEDUREEVENTS_MV
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
       LABEVENTS->'flag' ="abnormal" AND
	   D->'Fluid'="Cerebrospinal Fluid (CSF)" AND
	   PROCEDUREEVENTS_MV->'ordercategoryname'='Ventilation' AND
	   PROCEDUREEVENTS_MV->'ordercategorydescription'='Task' AND
	   P->'GENDER' ="F" AND
	   NOTEVEENTS->'TEXT'='specis'} 

RETURN	SUBJECT_ID, DOB