V5_JSONT = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID, A->'procedureevents_mv' AS PROCEDUREEVENTS_MV
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'labevents') AS LABEVENTS
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   LABEVENTS->'flag' ="abnormal" AND
	   D->'Fluid'="Cerebrospinal Fluid (CSF)" }
	   
RETURN	 PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID, 'PROCEDUREEVENTS_MV', PROCEDUREEVENTS_MV) }      	
