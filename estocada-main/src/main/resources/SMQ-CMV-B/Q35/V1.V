VPJPJJSON = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID, ICUSTAYS->'prescriptions' AS PRESCRIPTIONS
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'icustays') AS ICUSTAYS 
		
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   D->'Fluid'="Ascites" }
	   
RETURN	 PJ:{ json_build_object ('SUBJECT_ID',SUBJECT_ID, 'PRESCRIPTIONS', PRESCRIPTIONS) }      	
