Q = FOR PJ:{
SELECT P->'SUBJECT_ID' AS SUBJECT_ID , P->'DOB' AS DOB
FROM MIMIC AS P, MIMIC_D AS D, 
	jsonb_array_elements(P->'ADMISSIONS') AS A, 
	jsonb_array_elements(A->'labevents') AS LABEVENTS,
	jsonb_array_elements(A->'icustays') AS ICUSTAYS, 
	jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS 
	
WHERE  D->'ITEMID'=LABEVENTS->'itemid' AND 
	   PRESCRIPTIONS->'drug_type'= "MAIN" AND
	   PRESCRIPTIONS->'drug'= "Potassium Chloride" AND 
	   D->'Fluid'="Ascites" AND
	   P->'GENDER' ="F"} 

RETURN	SUBJECT_ID, DOB