match (tag:Tag {name: "Rumi"})<-[:HAS_TAG]-(message:Message)-[:HAS_CREATOR]->(p:Person)
create (:Tag_v7 {tagId:tag.tagId, name:tag.name, url:tag.url})<-[:HAS_TAG_v7]-(message)-[:HAS_CREATOR_v7]->(:Person_v7 {personId:p.personId, firstName:p.firstName,
    lastName:p.lastName, gender:p.gender})

// Person_v7: from 4 properties -> 5 properties slow a lot?