// modified from interactive-short-2
MATCH (:Person {personId: "4398046512167"})<-[:HAS_CREATOR]-(m:Message)-[:REPLY_OF]->(p:Post)
MATCH (p)-[:HAS_CREATOR]->(c)
RETURN
  m.commentId as messageId,
  m.creationDate AS messageCreationDate,
  p.postId AS originalPostId,
  c.personId AS originalPostAuthorId,
  c.firstName as originalPostAuthorFirstName,
  c.lastName as originalPostAuthorLastName