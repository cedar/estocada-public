// modified from bi-13
MATCH (:Place {type: "Country", name: "United_States"})<-[:IS_LOCATED_IN]-(message:Message)
MATCH (message)-[:HAS_TAG]->(tag:Tag)
WHERE tag.name = "Rumi"
RETURN
  message.content,
  message.imageFile,
  tag.url