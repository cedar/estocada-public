// modified from bi-2
MATCH
  (country:Place {type: "Country"})<-[:IS_PART_OF]-(:Place {type: "City"})<-[:IS_LOCATED_IN]-(person:Person)
  <-[:HAS_CREATOR]-(message:Message)-[:HAS_TAG]->(tag:Tag {name: "Rumi"})
WHERE
  country.name = "United_States"
WITH
  person.gender AS gender,
  person.firstName AS firstName,
  person.lastName AS lastName
RETURN
  gender,
  firstName,
  lastName