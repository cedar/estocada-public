 FOR  SJ:{ mimicnote/query?q="pneumonia"&fl=SUBJECT_ID1:subject_id },
 	  PR:{ SELECT SUBJECT_ID AS SID
		FROM mimiciii.PatientsF 
        WHERE gender ='F' },
PJ:{SELECT D.data->>'SUBJECT_ID' AS psubject_id
    FROM  "mimicJSON".mimic AS D, 
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS, 
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'procedureevents_mv') AS  PROCEDUREEVENTS_MV
 
   WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
       ITEMS.data@>'{"FLUID":"Cerebrospinal Fluid (CSF)"}' AND
       LABEVENTS->>'flag' ='abnormal' AND
       PROCEDUREEVENTS_MV->>'ordercategoryname'='Invasive Lines' AND
       PROCEDUREEVENTS_MV->>'ordercategorydescription'='Task'}


 WHERE SUBJECT_ID1=SID AND SID=psubject_id
 RETURN psubject_id