SELECT count(*)
FROM  (SELECT D.data as data
        FROM  "mimicJSON".mimic1 AS D
        WHERE to_tsvector('English', D.data::text) @@ plainto_tsquery('English','tachypnea')) AS D, 
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS,
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'procedureevents_mv') AS PROCEDUREEVENTS_MV, 
      jsonb_array_elements(ADMISSIONS->'noteevents_0') AS N

 
WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	  ITEMS.data@>'{"FLUID":"Cerebrospinal Fluid (CSF)"}' AND
	  LABEVENTS->>'flag' ='abnormal' AND
      PROCEDUREEVENTS_MV->>'ordercategoryname'='Invasive Lines' AND
      D.data@>'{"GENDER":"F"}' AND
      N->>'text' LIKE '%tachypnea%'
