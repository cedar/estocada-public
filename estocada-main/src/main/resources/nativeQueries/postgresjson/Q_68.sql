SELECT count(*)
FROM  (SELECT D.data as data
        FROM  "mimicJSON".mimic1 AS D
        WHERE to_tsvector('English', D.data::text) @@ plainto_tsquery('English','embolism')) AS D, 
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS,
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'microbiologyevents') AS MICROBIOLOGYEVENTS, 
      jsonb_array_elements(ADMISSIONS->'procedureevents_mv') AS PROCEDUREEVENTS_MV, 
      jsonb_array_elements(ADMISSIONS->'noteevents_0') AS N

 
WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 
	  ITEMS.data@>'{"FLUID":"Cerebrospinal Fluid (CSF)"}' AND
	  ITEMS.data@>'{"CATEGORY":"Hematology"}' AND
      MICROBIOLOGYEVENTS->>'ab_name'='CEFEPIME' AND
      PROCEDUREEVENTS_MV->>'ordercategoryname'='Peripheral Lines' AND
      D.data@>'{"GENDER":"F"}' AND
      N->>'text' LIKE '%embolism%'
