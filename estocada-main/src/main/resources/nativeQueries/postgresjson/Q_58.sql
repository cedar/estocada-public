SELECT count(*)
FROM  "mimicJSON".mimic1 as D,
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS,
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'microbiologyevents') AS MICROBIOLOGYEVENTS

 
WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 	
      ITEMS.data@>'{"FLUID":"Ascites"}' AND
      MICROBIOLOGYEVENTS->>'ab_name'='PENICILLIN' AND  
      D.data@>'{"GENDER":"M"}'
