SELECT count(*)
FROM  (SELECT D.data as data
        FROM  "mimicJSON".mimic1 AS D
        WHERE to_tsvector('English', D.data::text) @@ plainto_tsquery('English','specis')) AS D, 
      "mimicJSON".d_labitems AS ITEMS, 
      jsonb_array_elements(D.data->'ADMISSIONS') AS ADMISSIONS,
      jsonb_array_elements(ADMISSIONS->'labevents') AS LABEVENTS, 
      jsonb_array_elements(ADMISSIONS->'icustays') AS ICUSTAYS, 
      jsonb_array_elements(ICUSTAYS->'prescriptions') AS PRESCRIPTIONS,
      jsonb_array_elements(ADMISSIONS->'noteevents_0') AS N

 
WHERE ITEMS.data->>'ITEMID'=LABEVENTS->>'itemid' AND 	
	  LABEVENTS->>'flag' ='abnormal' AND
      PRESCRIPTIONS->>'drug_type'='ADDITIVE' AND 
      ITEMS.data@>'{"FLUID":"Ascites"}' AND
      PRESCRIPTIONS->>'drug'='Potassium Chloride' AND  
      D.data@>'{"GENDER":"M"}' AND
      N->>'text' LIKE '%specis%'
