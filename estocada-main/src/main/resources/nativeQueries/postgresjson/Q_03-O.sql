SELECT count(*)
FROM (SELECT D.data as data
	  FROM  "mimicJSON".mimic1 AS D
	  WHERE to_tsvector('English', D.data::text) @@ plainto_tsquery('English','hematuria')) AS D, 
	  "mimicJSON".d_labitems AS LI,
	  jsonb_array_elements(D.data->'ADMISSIONS') AS A,
	  jsonb_array_elements(A->'microbiologyevents') AS M,
	  jsonb_array_elements(A->'labevents') AS LE,
	  jsonb_array_elements(A->'noteevents_0') AS T

WHERE LI.data->'ITEMID'=LE->'itemid' AND
	  LI.data@>'{"FLUID":"Urine"}' AND
	  LE->>'flag'='abnormal' AND
	  M->>'ab_nme'='PENICILLIN' AND
	  T->>'text' LIKE '%hematuria%';
