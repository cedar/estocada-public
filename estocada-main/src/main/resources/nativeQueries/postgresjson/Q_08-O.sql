SELECT count(*)
FROM  "mimicJSON".mimic AS D, 
       mimiciii.mimic_notes AS notes,
      "mimicJSON".d_labitems AS LI,
	  jsonb_array_elements(D.data->'ADMISSIONS') AS A,
	  jsonb_array_elements(A->'microbiologyevents') AS M,
	  jsonb_array_elements(A->'labevents') AS LE

WHERE LI.data->'ITEMID'=LE->'itemid' AND
	  LI.data@>'{"FLUID":"Ascites"}' AND
	  LI.data@>'{"CATEGORY":"Hematology"}' AND
	  M->>'ab_name'='CEFEPIME' AND
	  M->>'interpretation'='S' AND
      D.data->>'SUBJECT_ID'=NOTES.subject_id AND
      to_tsvector('english', NOTES.text) @@ plainto_tsquery('english','intracranial')
