SELECT count(D.SUBJECT_ID)
FROM   d_labitems AS LI, mimic AS D LATERAL VIEW explode(D.ADMISSIONS) AS ADMISSIONS 
									LATERAL VIEW explode(ADMISSIONS.labevents) AS LABEVENTS 
									LATERAL VIEW explode(ADMISSIONS.icustays) AS ICUSTAYS
									LATERAL VIEW explode(ICUSTAYS.prescriptions) AS PRESCRIPTIONS

WHERE LI.ITEMID=LABEVENTS.itemid AND
	  PRESCRIPTIONS.drug_type="MAIN" AND
	  LI.FLUID="Ascites" AND 
	  D.GENDER="F" AND
	  PRESCRIPTIONS.drug="Hemorrhoidal Suppository"


