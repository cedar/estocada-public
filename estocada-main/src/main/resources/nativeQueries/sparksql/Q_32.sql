SELECT count(D.SUBJECT_ID)
FROM   d_labitems AS LI, mimic AS D LATERAL VIEW explode(D.ADMISSIONS) AS ADMISSIONS 
									LATERAL VIEW explode(ADMISSIONS.labevents) AS LABEVENTS 
									LATERAL VIEW explode(ADMISSIONS.icustays) AS ICUSTAYS
									LATERAL VIEW explode(ICUSTAYS.prescriptions) AS PRESCRIPTIONS
									LATERAL VIEW explode(ADMISSIONS.noteevents_0) AS N

WHERE LI.ITEMID=LABEVENTS.itemid AND
	  LABEVENTS.flag="abnormal" AND
	  PRESCRIPTIONS.drug_type="ADDITIVE" AND
	  LI.FLUID="Ascites" AND 
	  D.GENDER="F" AND
	  PRESCRIPTIONS.drug="Potassium Chloride" AND
	  N.text LIKE "%tachypnea%"


