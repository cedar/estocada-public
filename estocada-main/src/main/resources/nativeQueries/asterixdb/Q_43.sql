USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS ADMISSIONS, 
	   ADMISSIONS.labevents AS LABEVENTS,
	   ADMISSIONS.procedureevents_mv AS PROCEDUREEVENTS_MV,  
	   ADMISSIONS.noteevents_0 AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LABEVENTS.flag="abnormal" AND
	   LI.FLUID="Cerebrospinal Fluid (CSF)" AND 
	   PROCEDUREEVENTS_MV.ordercategoryname="Invasive Lines" AND 
	   D.GENDER="F" AND
       N.text LIKE "%pneumonia%";
	   