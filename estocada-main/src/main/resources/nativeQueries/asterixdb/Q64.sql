USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS ADMISSIONS, 
	   ADMISSIONS.labevents AS LABEVENTS,
	   ADMISSIONS.microbiologyevents AS MICROBIOLOGYEVENTS,  
	   ADMISSIONS.procedureevents_mv AS PROCEDUREEVENTS_MV,
	   ADMISSIONS.noteevents_0 AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LI.FLUID="Cerebrospinal Fluid (CSF)" AND 
	   LI.CATEGORY="Hematology" AND
	   MICROBIOLOGYEVENTS.ab_name="CEFAZOLIN" AND 
	   PROCEDUREEVENTS_MV.ordercategoryname="Peripheral Lines" AND
	   D.GENDER="F" AND
       N.text LIKE "%specis%";
	   