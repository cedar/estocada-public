USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS ADMISSIONS, 
	   ADMISSIONS.labevents AS LABEVENTS,
	   ADMISSIONS.microbiologyevents AS MICROBIOLOGYEVENTS,  
	   ADMISSIONS.noteevents_0 AS N

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LABEVENTS.flag="abnormal" AND
	   LI.FLUID="Urine" AND 
	   MICROBIOLOGYEVENTS.ab_name="CEFEPIME" AND 
	   D.GENDER="M" AND
       N.text LIKE "%tachypnea%";
	   