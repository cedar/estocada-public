USE mimiciii;
SELECT count(*)
FROM mimic AS D, 
	 d_labitems LI, 
	 D.ADMISSIONS AS ADMISSIONS, 
	 ADMISSIONS.labevents AS LABEVENTS, 
	 ADMISSIONS.icustays AS ICUSTAYS,
	 ICUSTAYS.prescriptions AS PRESCRIPTIONS, 
	 ADMISSIONS.noteevents_0 AS N
	 
WHERE LI.ITEMID=LABEVENTS.itemid AND
	  PRESCRIPTIONS.drug_type="ADDITIVE" AND
	  LI.FLUID="Ascites" AND 
	  D.GENDER="F" AND
	  PRESCRIPTIONS.drug="Lidocaine 1% P.F." AND
	  N.text LIKE "%specis%";