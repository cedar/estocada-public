USE mimiciii;
SELECT count(*)
FROM mimic AS D, 
	 d_labitems LI, 
	 D.ADMISSIONS AS A, 
	 A.labevents AS LE, 
	 A.noteevents_0 AS N
	 
WHERE LI.ITEMID=LE.itemid AND
	  LE.flag="abnormal" AND
	  LI.CATEGORY="Blood Gas" AND
	  LI.FLUID="Blood" AND 
	  D.GENDER ="F" AND
	  N.text LIKE "%respiratory failure%";