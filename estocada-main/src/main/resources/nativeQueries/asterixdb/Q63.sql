USE mimiciii;
SELECT count(*)
FROM   mimic AS D, d_labitems LI, 
	   D.ADMISSIONS AS ADMISSIONS, 
	   ADMISSIONS.labevents AS LABEVENTS,
	   ADMISSIONS.microbiologyevents AS MICROBIOLOGYEVENTS,  
	   ADMISSIONS.procedureevents_mv AS PROCEDUREEVENTS_MV

WHERE  LI.ITEMID=LABEVENTS.itemid AND
	   LABEVENTS.flag="abnormal" AND
	   LI.FLUID="Cerebrospinal Fluid (CSF)" AND 
	   LI.CATEGORY="Hematology" AND
	   MICROBIOLOGYEVENTS.ab_name="CEFEPIME" AND 
	   PROCEDUREEVENTS_MV.ordercategoryname="Peripheral Lines" AND
	   D.GENDER="M";
	   