package fr.inria.oak.estocada.compiler.model.encoder.pr;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.model.pr.PRNaiveModule;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeBuilderAlternative;
import fr.inria.oak.estocada.compiler.model.pr.PRQueryBlockTreeCompiler;

/**
 * PJ QueryBlockTreeNaiveCompilerTest
 * 
 * @author Rana Alotaibi
 *
 */
@Ignore
public final class PRQueryBlockTreeNaiveCompilerTest extends PRQueryBlockTreeCompilerTest {
    private Injector injector;

    @Override
    @Before
    public void setUp() {
        injector = Guice.createInjector(new PRNaiveModule());
        super.setUp();
    }

    @Override
    @After
    public void tearDown() {
        injector = null;
        super.tearDown();
    }

    @Override
    protected QueryBlockTreeBuilder createBuilder() {
        return injector.getInstance(PRQueryBlockTreeBuilderAlternative.class);
    }

    @Override
    protected List<QueryBlockTreeViewCompiler> createCompilers() {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(PRQueryBlockTreeCompiler.class));
        return compilers;
    }

    @Override
    protected ConjunctiveQuery restrict(final ConjunctiveQuery query) {
        return query;
    }
}
