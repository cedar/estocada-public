package fr.inria.oak.estocada.rewriting.logical;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.oak.commons.conjunctivequery.parser.ParseException;
import fr.inria.oak.estocada.compiler.Utils;
import fr.inria.oak.estocada.compiler.model.encoder.pj.PJQueryBlockTreeCompilerTest;

/**
 * Rewriting to logical temporary test
 * 
 * @author Rana Alotaibi
 *
 */
public class RewritingToLogicalTest {
    final static Logger LOGGER = Logger.getLogger(RewritingToLogicalTest.class);
    final static String TEST_DIR = "/logical";

    @BeforeClass
    public static void setUp() throws Exception {
        Parameters.init();
        RegisterMetaData.registerMetaData();
    }

    /**
     * Get all tests directories
     * 
     * @return List<File>
     */
    private List<File> getTestsDirectories() {
        final File directory =
                new File(PJQueryBlockTreeCompilerTest.class.getResource(TEST_DIR).toString().substring(5));

        LOGGER.debug("Test home path: " + directory.toString());

        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    /**
     * Get test rewriting
     * 
     * @param testDirectory
     * @return file
     * @throws IOException
     */
    private File getTestRewriting(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("rewriting")) {
                return file;
            }
        }
        throw new IOException("Rewriting file not found.");
    }

    @Test
    public void test() {
        getTestsDirectories().stream().forEach(f -> test(f));
    }

    public void test(final File testDirectory) {
        LOGGER.debug("Testing " + testDirectory.toString());
        File inputRewritingFile;
        boolean flag = false;
        try {
            inputRewritingFile = getTestRewriting(testDirectory);
            final RewritingToLogicalPlanGenerator planGenerator =
                    new RewritingToLogicalPlanGenerator(Utils.parseQuery((inputRewritingFile)));
            final String generatedLogical = planGenerator.generate().getName();
            if (generatedLogical.contains("LogPJQLEval") && generatedLogical.contains("LogSQLEval")
                    && generatedLogical.contains("LogSolrEval")) {
                flag = true;
            }
            assertTrue(flag);
        } catch (IllegalArgumentException | ParseException | IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
