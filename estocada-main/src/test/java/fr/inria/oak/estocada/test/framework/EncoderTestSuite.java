package fr.inria.oak.estocada.test.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.inria.oak.estocada.compiler.model.encoder.aj.AJQueryBlockTreeNaiveCompilerTest;
import fr.inria.oak.estocada.compiler.model.encoder.pj.PJQueryBlockTreeNaiveCompilerTest;
import fr.inria.oak.estocada.compiler.model.encoder.pr.PRQueryBlockTreeNaiveCompilerTest;
import fr.inria.oak.estocada.compiler.model.encoder.qbt.QueryBlockTreeNaiveCompilerTest;
import fr.inria.oak.estocada.compiler.model.encoder.sj.SJQueryBlockTreeNaiveCompilerTest;
import fr.inria.oak.estocada.compiler.model.encoder.xq.XQQueryBlockTreeNaiveCompilerTest;

/**
 * Encoders Test Suite.
 * QBT,SJ,PJ,SPPJ, XQ and AJ
 * 
 * @author Rana Alotaibi
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ PJQueryBlockTreeNaiveCompilerTest.class, SJQueryBlockTreeNaiveCompilerTest.class,
        AJQueryBlockTreeNaiveCompilerTest.class, PRQueryBlockTreeNaiveCompilerTest.class,
        XQQueryBlockTreeNaiveCompilerTest.class, QueryBlockTreeNaiveCompilerTest.class })
public class EncoderTestSuite {
};

//SPPJQueryBlockTreeNaiveCompilerTest.class