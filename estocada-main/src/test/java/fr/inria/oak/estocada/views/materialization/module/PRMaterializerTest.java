package fr.inria.oak.estocada.views.materialization.module;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.oak.estocada.views.materialization.module.PR.RegisterMetadata;

/**
 * RMaterializerTest
 * 
 * @author Rana Alotaibi
 *
 */
@Ignore
public class PRMaterializerTest {
    final static Logger LOGGER = Logger.getLogger(PRMaterializerTest.class);
    final static String TEST_DIR = "/VMModule/pr-module";

    @BeforeClass
    public static void setUp() throws Exception {
        Parameters.init();
        RegisterMetadata.RegisterDataset();
    }

    /**
     * Get all tests directories
     * 
     * @return List<File>
     */
    private List<File> getTestsDirectories() {
        final File directory = new File(PRMaterializerTest.class.getResource(TEST_DIR).toString().substring(5));
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    /**
     * Get the result file.
     * 
     * @param testDirectory
     *            the test directory.
     * @return The result file.
     * @throws IOException
     */
    private File getResultCount(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("resultCount")) {
                return file;
            }
        }
        throw new IOException("Result file not found.");
    }

    /**
     * Get the result file.
     * 
     * @param testDirectory
     *            the test directory.
     * @return The result file.
     * @throws IOException
     */
    private File getQBTView(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("view")) {
                return file;
            }
        }
        throw new IOException("View file not found.");
    }

    @Test
    public void test() {
        getTestsDirectories().stream().forEach(f -> {
            try {
                test(f);
            } catch (Exception e) {
                LOGGER.error("Test failed :" + e.getMessage());
            }
        });
    }

    /**
     * Run the test.
     * 
     * @param testDirectory
     *            the test directory.
     * @throws Exception
     */
    public void test(final File testDirectory) throws Exception {
        File inputResultFile;
        try {
            inputResultFile = getResultCount(testDirectory);
            final QBTViewListener qbtViewsMigrator = new QBTViewListener();
            qbtViewsMigrator.run(FileUtils.readFileToString(new File(getQBTView(testDirectory).getAbsolutePath())));

            final BufferedReader resultFile = new BufferedReader(new FileReader(inputResultFile));
            final int result = Integer.parseInt(IOUtils.toString(resultFile));
            //TODO: CEHCK assertEquals(count, result);
        } catch (IllegalArgumentException | IOException e) {
            throw new Exception(e);
        }
    }
}
