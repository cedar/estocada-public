package fr.inria.oak.estocada.test.framework;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.inria.oak.estocada.integrationtest.IntegrationTest;

/**
 * Integration Test
 * 
 * @author Rana Alotaibi
 *
 */
@Ignore
@RunWith(Suite.class)
@SuiteClasses({ IntegrationTest.class })
public class IntegrationTestSuite {
};
