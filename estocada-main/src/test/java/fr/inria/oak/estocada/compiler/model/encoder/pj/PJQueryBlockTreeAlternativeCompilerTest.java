package fr.inria.oak.estocada.compiler.model.encoder.pj;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.model.pj.PJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.pj.Utils;
import fr.inria.oak.estocada.compiler.model.pj.naive.PJNaiveModule;
import fr.inria.oak.estocada.compiler.model.pj.naive.PJNaiveQueryBlockTreeCompiler;

/**
 * PJ QueryBlockTreeAlternativeCompilerTest
 * 
 * @author ranaalotaibi
 *
 */
public final class PJQueryBlockTreeAlternativeCompilerTest extends PJQueryBlockTreeCompilerTest {
    private Injector injector;

    @Override
    @Before
    public void setUp() {
        injector = Guice.createInjector(new PJNaiveModule());;
        super.setUp();
    }

    @Override
    @After
    public void tearDown() {
        injector = null;
        super.tearDown();
    }

    @Override
    protected QueryBlockTreeBuilder createBuilder() {
        return injector.getInstance(PJQueryBlockTreeBuilder.class);
    }

    @Override
    protected List<QueryBlockTreeViewCompiler> createCompilers() {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(PJNaiveQueryBlockTreeCompiler.class));

        return compilers;
    }

    @Override
    protected ConjunctiveQuery restrict(final ConjunctiveQuery query) {
        return Utils.restrict(query);
    }
}
