package fr.inria.oak.estocada.compiler.model.encoder.sppj;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.estocada.compiler.QueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.QueryBlockTreeViewCompiler;
import fr.inria.oak.estocada.compiler.model.pj.Utils;
import fr.inria.oak.estocada.compiler.model.sppj.SPPJQueryBlockTreeBuilder;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveModule;
import fr.inria.oak.estocada.compiler.model.sppj.naive.SPPJNaiveQueryBlockTreeCompiler;

/**
 * SPPJ QueryBlockTreeAlternativeCompilerTest
 * 
 * @author ranaalotaibi
 *
 */
public final class SPPJQueryBlockTreeAlternativeCompilerTest extends SPPJQueryBlockTreeCompilerTest {
    private Injector injector;

    @Override
    @Before
    public void setUp() {
        injector = Guice.createInjector(new SPPJNaiveModule());;
        super.setUp();
    }

    @Override
    @After
    public void tearDown() {
        injector = null;
        super.tearDown();
    }

    @Override
    protected QueryBlockTreeBuilder createBuilder() {
        return injector.getInstance(SPPJQueryBlockTreeBuilder.class);
    }

    @Override
    protected List<QueryBlockTreeViewCompiler> createCompilers() {
        final List<QueryBlockTreeViewCompiler> compilers = new ArrayList<QueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(SPPJNaiveQueryBlockTreeCompiler.class));

        return compilers;
    }

    @Override
    protected ConjunctiveQuery restrict(final ConjunctiveQuery query) {
        return Utils.restrict(query);
    }
}
