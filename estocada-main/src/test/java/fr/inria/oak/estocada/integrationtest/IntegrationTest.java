package fr.inria.oak.estocada.integrationtest;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.parser.ParseException;
import fr.inria.oak.estocada.main.ExecutionUtils;
import fr.inria.oak.estocada.main.RegisterMetaData;
import fr.inria.oak.estocada.rewriter.ConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.Context;
import fr.inria.oak.estocada.rewriter.PACBConjunctiveQueryRewriter;
import fr.inria.oak.estocada.rewriter.TimedReformulations;
import fr.inria.oak.estocada.rewriting.logical.RewritingToLogicalPlanGenerator;
import fr.inria.oak.estocada.rewriting.physical.LogicalToNaivePhysicalPlanGenerator;

/**
 * Integration Test.
 * 
 * @author Rana Alotaibi
 *
 */
@Ignore
public class IntegrationTest {
    final static Logger LOGGER = Logger.getLogger(IntegrationTest.class);
    final static String TEST_DIR = "/integration";

    @BeforeClass
    public static void setUp() throws Exception {
        Parameters.init();
        RegisterMetaData.registerMetaData();
    }

    /**
     * Get all tests directories
     * 
     * @return List<File>
     */
    private List<File> getTestsDirectories() {
        final File directory = new File(IntegrationTest.class.getResource(TEST_DIR).toString().substring(5));
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    /**
     * Create a rewriting context.
     * 
     * @param testDirectory
     *            the test directory.
     * @return the context of the rewriting.
     * @throws Exception
     */
    private Context constructContext(final File testDirectory) throws Exception {
        try {
            String schemas = null;
            String constraints_chase = null;
            String constraints_bkchase = null;

            for (final File file : testDirectory.listFiles()) {
                if (file.isFile() && file.getName().equals("schemas")) {
                    schemas = file.getAbsolutePath();
                }
                if (file.isFile() && file.getName().equals("constraints_chase")) {
                    constraints_chase = file.getAbsolutePath();
                }
                if (file.isFile() && file.getName().equals("constraints_bkchase")) {
                    constraints_bkchase = file.getAbsolutePath();
                }
            }
            return fr.inria.oak.estocada.rewriter.server.Utils.parseContext(schemas, constraints_chase,
                    constraints_bkchase);
        } catch (IOException | fr.inria.oak.commons.relationalschema.parser.ParseException e) {
            throw new Exception(e);
        }

    }

    /**
     * Find a query rewriting.
     * 
     * @param testDirectory
     *            the test directory.
     * @param context
     *            the rewriting context.
     * @return the list of found rewritings.
     * @throws Exception
     */
    private List<ConjunctiveQuery> findRewriting(final File testDirectory, final Context context) throws Exception {
        final ConjunctiveQueryRewriter rewriter = new PACBConjunctiveQueryRewriter(context);
        try {
            for (final File file : testDirectory.listFiles()) {
                if (file.isFile() && file.getName().equals("query")) {
                    final BufferedReader query = new BufferedReader(new FileReader(file.getAbsolutePath()));
                    final TimedReformulations timedRewritings = rewriter.getTimedReformulations(
                            fr.inria.oak.estocada.rewriter.server.Utils.parseQuery(IOUtils.toString(query)));
                    final List<TimedReformulations> rewritings = new ArrayList<TimedReformulations>();
                    rewritings.add(timedRewritings);
                    final ExecutionUtils exe = new ExecutionUtils(rewritings);
                    if (!exe.getRewritings().isEmpty()) {
                        final List<ConjunctiveQuery> rws = exe.getRewritings();
                        return rws;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        throw new Exception("Query rewriting is not found.");

    }

    /**
     * Get the result file.
     * 
     * @param testDirectory
     *            the test directory.
     * @return The result file.
     * @throws IOException
     */
    private File getResultCount(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("resultCount")) {
                return file;
            }
        }
        throw new IOException("Result file not found.");
    }

    @Test
    public void test() {
        getTestsDirectories().stream().forEach(f -> {
            try {
                test(f);
            } catch (Exception e) {
                LOGGER.error("Test failed :" + e.getMessage());
            }
        });
    }

    /**
     * Run the integration test.
     * 
     * @param testDirectory
     *            the test directory.
     * @throws Exception
     */
    public void test(final File testDirectory) throws Exception {
        File inputResultFile;
        try {
            inputResultFile = getResultCount(testDirectory);
            final Context context = constructContext(testDirectory);
            final List<ConjunctiveQuery> rewritings = findRewriting(testDirectory, context);
            final LogOperator logicalPlanRootOperator =
                    new RewritingToLogicalPlanGenerator(rewritings.get(0)).generate();
            final NIterator rewritingPhyPlan =
                    new LogicalToNaivePhysicalPlanGenerator(logicalPlanRootOperator).generate();
            rewritingPhyPlan.open();
            int count = 0;
            while (rewritingPhyPlan.hasNext()) {
                rewritingPhyPlan.next();
                count++;
            }
            final BufferedReader resultFile = new BufferedReader(new FileReader(inputResultFile));
            final int result = Integer.parseInt(IOUtils.toString(resultFile));
            assertEquals(count, result);
        } catch (IllegalArgumentException | ParseException | IOException e) {
            throw new Exception(e);
        }
    }
}
