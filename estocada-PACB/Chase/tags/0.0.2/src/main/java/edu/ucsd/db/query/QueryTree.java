package edu.ucsd.db.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Stack;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Statement;
import edu.ucsd.db.datalogexpr.StatementBinaryOp;
import edu.ucsd.db.datalogexpr.StatementJoin;
import edu.ucsd.db.datalogexpr.StatementOp;
import edu.ucsd.db.datalogexpr.StatementScan;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This class implements a physical query tree. It contains methods
 * to construct a query tree out of a <code>Statement</code>
 */
public class QueryTree {

	private static final int PRINTING_INDENT = 4;

	/**
	 * The query tree root
	 */
	private Operator root;

	// TODO: Convert to a hashtable
	
	/**
	 * The leaves of the tree
	 */
	private ArrayList<MapPredicates> leaves;
	
	/**
	 * The blocking operators in the tree
	 */
	private ArrayList<Operator> blockingOps;

	/**
	 * Constructor.
	 * 
	 * @param The root of the tree
	 */
	private QueryTree(Operator topOp) {
		root = topOp;
		topOp.setTopOperator(true);
		computeLeavesAndBlocks();

//		System.out.println("QueryTree:\n" + this.printString());
//		System.out.println("\nLeaves:\n" + this.printLeaves());
//		System.out.println("\nBlockingOps:\n" + this.printBlockingOps());
		
		initialize();
	}

	// TODO: Since we are creating the tree, we can compute the leaves in
	// parallel

	// TODO: This constructor creates a query that does not return the
	// exact query head (although it returns the vars needed)

	// Do postorder traversal to convert Statement to QueryTree
	// TODO: Add ops for duplicate elimination and projection
	
	public QueryTree(Statement s, Database db) {
		StatementOp currStOp, oldStOp;
		Operator leftChild, rightChild, newOperator;
		CanonicalSchema projectOut, project;
		LinkedHashMap<Integer, Value> newValuesMap;

		Stack<StatementOp> parentStOp = new Stack<StatementOp>();
		Stack<Operator> childrenOp = new Stack<Operator>();

		oldStOp = null;
		currStOp = s;
		parentStOp.push(null);

		while (currStOp != null) {
			if (currStOp instanceof Statement) {
				currStOp = ((Statement) currStOp).getChildOp();
			} else if (currStOp instanceof StatementScan) {
				// TODO: What about the database?
				if (parentStOp.size() == 1) {
					project = s.head().getCanonicalSchema();
					newValuesMap = s.head().getPosToValueCorrespondence();
				} else {
					project = currStOp.getAttribsToProject();
					newValuesMap = new LinkedHashMap<Integer, Value>();
				}
					
				newOperator = new MapPredicates(((StatementScan) currStOp)
						.getPredicate(), db, project, newValuesMap);

				childrenOp.push(newOperator);

				oldStOp = currStOp;
				currStOp = parentStOp.pop();
			} else if (currStOp instanceof StatementBinaryOp) {
				// If you returned from the left child, visit the right child
				if (((StatementBinaryOp) currStOp).getChildOp1() == oldStOp) {
					parentStOp.push(currStOp);
					currStOp = ((StatementBinaryOp) currStOp).getChildOp2();
				}
				// If you returned from the right child, create operator and
				// return to your parent
				else if (((StatementBinaryOp) currStOp).getChildOp2() == oldStOp) {
					rightChild = childrenOp.pop();
					leftChild = childrenOp.pop();

					if (parentStOp.size() == 1) {
						project = s.head().getCanonicalSchema();
						newValuesMap = s.head().getPosToValueCorrespondence();
					} else {
						project = currStOp.getAttribsToProject();
						newValuesMap = new LinkedHashMap<Integer, Value>();
					}
					
					if (currStOp instanceof StatementJoin)
						newOperator = new NaturalJoin(leftChild, rightChild, project, newValuesMap);
					else // TODO: Make it throw an exception when encountering an unkown operator
						newOperator = null;

					childrenOp.push(newOperator);

					oldStOp = currStOp;
					currStOp = parentStOp.pop();
				}
				// Otherwise, visit the left child
				else {
					parentStOp.push(currStOp);
					currStOp = ((StatementBinaryOp) currStOp).getChildOp1();
				}
			}

		}

		if (!childrenOp.isEmpty()) {
			root = childrenOp.pop();
			root.setTopOperator(true);
		}

		computeLeavesAndBlocks();

		// Add unique identifier to each operator, so that they can be
		// distinguished when printed
//		System.out.println("QueryTree:\n" + this.printString());
//		System.out.println("\nLeaves:\n" + this.printLeaves());
//		System.out.println("\nBlockingOps:\n" + this.printBlockingOps());
		
		initialize();
	}
	
	protected void initialize() {
		int numOfLeaves = leaves.size();
		
		for (int i = 0; i < numOfLeaves; i++)
			leaves.get(i).pushAll();
	}

	public static QueryTree CreateQueryTree(Statement s, Database db) {
		return new QueryTree(s, db);
	}

	// Can we compute the leaves and blocks incrementally?
	// What do we do with the output operators of the old trees?
	public static QueryTree CreateQueryTreeForDifference(Statement left,
			Statement right, Database db) {
		QueryTree leftTree = CreateQueryTree(left, db);
		QueryTree rightTree = CreateQueryTree(right, db);

		Difference diffOp = new Difference(leftTree.root, rightTree.root,
				leftTree.root.outSchema, new LinkedHashMap<Integer, Value>());

		// Fix roots
		leftTree.root.setTopOperator(false);
		rightTree.root.setTopOperator(false);
		diffOp.setTopOperator(true);

		return new QueryTree(diffOp);
	}

	// TODO: Check that the tree has indeed as leaves MapPredicates and not
	// other types of operators

	// TODO: For this to work every operator should either be a MapPredicate,
	// a unary or a binary operator. We can avoid that by adding to every
	// operator class a getChildren method, which returns a list of its
	// children

	// TODO: The leaves may not be in the right order! Is this a problem?

	private void computeLeavesAndBlocks() {
		leaves = new ArrayList<MapPredicates>();
		blockingOps = new ArrayList<Operator>();

		// BFS
		LinkedList<Operator> opQueue = new LinkedList<Operator>();
		opQueue.addFirst(root);

		while (!opQueue.isEmpty()) {
			Operator currentOp = opQueue.removeFirst();

			if (currentOp instanceof MapPredicates)
				leaves.add((MapPredicates) currentOp);
			else if (currentOp instanceof UnaryOperator)
				opQueue.addLast(((UnaryOperator) currentOp).childOperator());
			else if (currentOp instanceof BinaryOperator) {
				opQueue.addLast(((BinaryOperator) currentOp).childOp1());
				opQueue.addLast(((BinaryOperator) currentOp).childOp2());
			}

			if (currentOp.isBlockedDefault())
				// why does it have to be first?
				blockingOps.add(0, currentOp);
		}
	}

	public ArrayList<MapPredicates> getLeaves() {
		return leaves;
	}

	public ArrayList<Operator> getBlockingOps() {
		return blockingOps;
	}

	public Operator getRoot() {
		return root;
	}

	/**
	 * @return A <code>StringBuffer</code> containing a string
	 * representation of the query tree. Printing is done by
	 * doing a preorder traversal of the tree.
	 */ 
	public StringBuffer printString() {
		StringBuffer retVal;
		Stack<OperatorNode> stack;
		OperatorNode curNode;
		Operator curOp;
		int curDepth;

		if (root == null)
			return new StringBuffer("");

		retVal = new StringBuffer("");
		stack = new Stack<OperatorNode>();
		stack.push(new OperatorNode(root, 0));

		while (!stack.isEmpty()) {
			curNode = stack.pop();
			curOp = curNode.getOperator();
			curDepth = curNode.getDepth();

			// TODO: Is there a better way to append a number of spaces?
			for (int i = 0; i < curDepth * PRINTING_INDENT; i++)
				retVal.append(" ");
			retVal.append(curOp.printString() + "\n");

			if (curOp instanceof UnaryOperator)
				stack.push(new OperatorNode(((UnaryOperator) curOp)
						.childOperator(), curDepth + 1));
			else if (curOp instanceof BinaryOperator) {
				stack.push(new OperatorNode(
						((BinaryOperator) curOp).childOp2(), curDepth + 1));
				stack.push(new OperatorNode(
						((BinaryOperator) curOp).childOp1(), curDepth + 1));
			}
		}

		return retVal;
	}

	/**
	 * @return A <code>StringBuffer</code> containing the leaves of the tree
	 */
	public StringBuffer printLeaves() {
		StringBuffer str;
		Iterator<MapPredicates> leavesIter = leaves.iterator();

		str = new StringBuffer("");

		while (leavesIter.hasNext()) {
			str.append(((Operator) leavesIter.next()).printString() + "\n");
		}

		return str;
	}

	/**
	 * @return A <code>StringBuffer</code> containing the operators in the tree that
	 * are blocking
	 */
	public StringBuffer printBlockingOps() {
		StringBuffer str;
		Iterator<Operator> blockingOpsIter = blockingOps.iterator();

		str = new StringBuffer("");

		while (blockingOpsIter.hasNext()) {
			str
					.append(((Operator) blockingOpsIter.next()).printString()
							+ "\n");
		}

		return str;
	}
	
	/**
	 * Flushes the query tree by calling the <code>flush()</code> function of the
	 * root
	 */
	public void flush() {
		root.flush();
	}

	private class OperatorNode {
		Operator op;
		int depth;

		public OperatorNode(Operator _op, int _depth) {
			op = _op;
			depth = _depth;
		}

		public int getDepth() {
			return depth;
		}

		public Operator getOperator() {
			return op;
		}
	}
}
