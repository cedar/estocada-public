package edu.ucsd.db.canonicaldb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import edu.ucsd.db.chase.Equalities;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This class represents a canonical database, which is a collection of
 * <code>Relation</code> instances.
 */
public class Database {

	private int version;
	
	/**
	 * The relations in the database indexed by their name
	 */
	Map<String, Relation> db;
	
	/**
	 * The list of tuples that contain any specific value
	 */
	private Hashtable<String, ArrayList<Tuple>> valueMap;
	
	// TODO: Add version management
	
	/**
	 * Constructor.
	 */
	public Database() {
		version = 0;
		
		db = new HashMap<String, Relation>();
		valueMap = new Hashtable<String, ArrayList<Tuple>>();
	}
	
	public static Database createDatabaseWithSameSchema(Database oldDB) {
		Database newDB = new Database();
		
		Collection<Relation> oldRelations = oldDB.db.values();
		
		Iterator<Relation> iter = oldRelations.iterator();
		
		while (iter.hasNext()) {
			Relation oldRel = iter.next();
			Relation newRel = Relation.createRelationWithSameSchema(oldRel);
			newDB.addRelationInstance(newRel);
		}
		
		return newDB;
	}
	
	protected void updateMap(Tuple tuple) {
		int arity = tuple.size();
		Set<Value> duplicateValues = new HashSet<Value>();
		
		for (int i = 0; i < arity; i++) {
			Value curValue = tuple.getValue(i);
			if (duplicateValues.add(curValue))
				addToMap(curValue.getValueStr(), tuple);
		}	
	}
	
	private void addToMap(String str, Tuple tuple) {
		if (valueMap.containsKey(str))
			valueMap.get(str).add(tuple);
		else {
			ArrayList<Tuple> newArray = new ArrayList<Tuple>();
			newArray.add(tuple);
			valueMap.put(str, newArray);
		}
	}
	
	// TODO: Add utility function to create relation instance and add it to
	// the database
	
	/**
	 * Creates a new <code>Relation</code> instance and adds it to the
	 * canonical database.
	 * 
	 * @param name
	 *            the name of the <code>Relation</code>
	 * @param schema
	 *            the schema of the <code>Relation</code>
	 * @return the <code>Relation</code> created
	 * @throws CanonicalDBException
	 *             when there is another <code>Relation</code> with the same
	 *             name, or the <code>schema</code> is <code>null</code>.
	 */
//	public Relation createRelationInstance(String name,
//			CanonicalSchema schema) throws CanonicalDBException {
//		
//	}

	/**
	 * Adds a <code>Relation</code> instance to the canonical database.
	 * 
	 * @param relation
	 *            the <code>Relation</code> to add
	 * @throws CanonicalDBException
	 *             when a <code>Relation</code> with the same name is already
	 *             in the <code>CanonicalDB</code>
	 */
	public void addRelationInstance(Relation relation)
			throws CanonicalDBException {
		if (db.containsKey(relation.name))
			throw new CanonicalDBException("Adding relation \"" +
					relation.name + "\" to the database failed: " +
					"relation with the same name already in the database");
		else {
			db.put(relation.name, relation);
			relation.db = this;
		}
	}

	// TODO: Change exception, so that a query asking for a relation not
	// in the database does not cause a runtime exception
	
	/**
	 * Gets a <code>Relation</code> instance.
	 * 
	 * @param name
	 *            the name of the <code>Relation</code>
	 * @return a <code>Relation</code> object
	 * @throws CanonicalDBException
	 *             when there is no <code>Relation</code> with the given
	 *             <code>name</code>.
	 */
	public Relation getRelationInstance(String name)
			throws CanonicalDBException  {
		if (db.containsKey(name))
			return db.get(name);
		else
			throw new CanonicalDBException("Retrieving relation \"" +
					name + "\" from the database failed: " +
					"relation not in the database");
	}

	public ArrayList<String> getRelationNames() {
		return new ArrayList<String>(db.keySet());
	}
	
	public ArrayList<Tuple> getTuplesContainingValue(Value v) {
		return valueMap.get(v.getValueStr());
	}
	
	public String toString()
	{
		String out="";
		for (Map.Entry<String, Relation> ent: db.entrySet())
			out+=ent.getValue().toString()+"\n";
		return out;
	}
	// TODO: Implement equality management
	
	/**
	 * Gets the <code>Equalities</code> for the latest version.
	 * 
	 * @return an <code>Equalities</code> object.
	 */
//	public Equalities getEqualities() {
//		
//	}

	/**
	 * Gets the number of the latest version.
	 * 
	 * @return a positive integer denoting the latest version, or
	 *         <code>-1</code> if there are no versions.
	 */
	public int getLatestVersion() {
		return version;
	}

	/**
	 * Reverts the <code>CanonicalDB</code> to the version provided.
	 * 
	 * @param version
	 *            the version number to revert to
	 * @throws CanonicalDBException
	 *             when there is no version equal to the one provided.
	 */
//	public void revertToVersion(int version)
//			throws CanonicalDBException {
//		
//	}

	// TODO: Implement skolem generator
	
	/**
	 * Gets a unique variable.
	 * 
	 * @return a <code>Value</code> object
	 */
//	public Value getFreshSkolem() {
//		
//	}

}
