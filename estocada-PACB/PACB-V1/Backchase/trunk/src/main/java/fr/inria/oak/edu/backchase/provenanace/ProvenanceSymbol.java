/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package fr.inria.oak.edu.backchase.provenanace;

/**
 * Provenance Symbol
 * 
 * @author ranaalotaibi
 * @author Ioana Ileana
 */
public class ProvenanceSymbol {
    private String name;
    private int index;

    /**
     * Constructor
     * 
     * @param provenance
     *            name
     * @param provenance
     *            index
     */
    public ProvenanceSymbol(final String name, final int index) {
        this.name = name;
        this.index = index;
    }

    /**
     * Set provenance name
     * 
     * @param name
     */
    public void setProvenanceName(final String name) {
        this.name = name;
    }

    /**
     * Set provenance index
     * 
     * @param index
     */
    public void setProvenanceIndex(final int index) {
        this.index = index;
    }

    /**
     * Get provenance name
     * 
     * @return provenance name
     */
    public String getProvenanceName() {
        return name;
    }

    /**
     * Get provenance index
     * 
     * @return provenance index
     */
    public int getProvenanceIndex() {
        return index;
    }
}
