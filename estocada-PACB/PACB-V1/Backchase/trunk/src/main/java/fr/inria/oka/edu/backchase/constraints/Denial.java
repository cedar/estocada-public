/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package fr.inria.oka.edu.backchase.constraints;

import java.io.BufferedReader;
import java.util.List;

import fr.inria.oak.edu.backchase.Utils.Parser;
import fr.inria.oak.edu.backchase.atoms.AtomPositionTerm;
import fr.inria.oak.edu.backchase.atoms.DefRelAtom;
import fr.inria.oak.edu.backchase.atoms.PremiseDefinition;
import fr.inria.oak.edu.backchase.instance.LocalMapping;
import fr.inria.oak.edu.backchase.nodes.Node;
import fr.inria.oak.edu.backchase.provenanace.flatProvenance.FlatFormula;
import fr.inria.oak.edu.backchase.provenanace.placeHodlerProvenance.PHFormula;

/**
 * Denial constraint
 * @author ranaalotaibi
 *
 */
public class Denial extends Node {
    
    
    private PremiseDefinition premDef;
    private boolean applied;

    public Denial() {
        super();
        premDef = new PremiseDefinition();
        applied = false;

    }

    public boolean enforce() {
        if (!hasNew)
            return false;
        while (hasNew)
            pushNew();
        return true;

    }

    @Override
    public void computeNeededTerms(List<AtomPositionTerm> termsNeededUpper) {

        premDef.getTopJoinNode().computeNeededTerms(termsNeededUpper);
    }

    @Override
    public boolean hasAtom(DefRelAtom atom) {
        return false;
    }

    @Override
    public void registerAsWatcherOnRelations() {
        premDef.getTopJoinNode().registerAsWatcherOnRelations();
    }

    @Override
    public void flush() {
        premDef.getTopJoinNode().flush();
    }

    @Override
    public void addNewLocalMapping(LocalMapping mapping, Object adder) {
        applied = true;
    }

    @Override
    public void addExistingMappingAdditionalProvenance(LocalMapping mapping, PHFormula phform, FlatFormula flatform,
            Object adder) {
    }

    @Override
    public void pushNew() {
        if (premDef.getTopJoinNode().hasNew) {
            premDef.getTopJoinNode().pushNew();
            hasNew = premDef.getTopJoinNode().hasNew;
        }
    }

    @Override
    public void refreshDummyMapping() {
    }

    public void readFromFile(BufferedReader br) throws Exception {
        premDef.readFromFile(br);
        premDef.getTopJoinNode().parent = this;
        Parser parser = new Parser();
        String line = br.readLine();
        parser.parseRelationals(line);
    }

    public boolean getApplied() {
        return applied;
    }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(premDef);
        str.append("->");
        str.append("false");
        return str.toString();
    }

}
