package fr.inria.oak.edu.backchase.costUtils;

/**
 * Cost Type
 * @author ranaalotaibi
 *
 */
public enum CostType {
    
    NAIVE_COSTING ("naive_costing"),
    PRUNED_COSTING ("pruned_costing");
    
    private final String costType;

    private CostType(final String costType) {
        this.costType=costType;
    }
    
    public String getCostType() {
        return costType;
    } 
}
