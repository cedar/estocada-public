package rwTests;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A concrete implementation of a FileFiler which
 * filters directories based on the content.
 * 
 * @author Stamatis Zampetakis
 *
 */
class DirectoryContentFilter implements FileFilter {

	/**
	 * Initializes a new DirectoryContentFilter with a collection
	 * of fileNames. 
	 */
	private Collection<String> mandatoryFiles;
	DirectoryContentFilter(Collection<String> fileNames){
		if(fileNames==null||fileNames.isEmpty())
			throw new IllegalArgumentException();
		mandatoryFiles = new ArrayList<String>(fileNames);
	}
	
	/**
	 * Returns true if the following conditions hold for the directory given as a parameter:
	 * (i) dir parameter is a directory;
	 * (ii) it contains at least the files with the names specified in the constructor;
	 * (iii) it does not contain a nested directory.
	 * 
	 * @param dir - the input directory
	 * @return true if the input parameters is a directory containing the files specified 
	 * in the constructor.
	 */
	@Override
	public boolean accept(File dir) {
		if(dir == null)
			throw new IllegalArgumentException("Input directory cannot be null");
		if(!dir.isDirectory())
 			return false;
 		List<String> mandatoryFilesCopy = new ArrayList<String>(mandatoryFiles); 
 		for(File file:dir.listFiles()){
 			//Nested directories inside scenario files are not allowed
 			if(file.isDirectory())
 				return false;
 			mandatoryFilesCopy.remove(file.getName());
 		}
 		return mandatoryFilesCopy.isEmpty();
	}

}
