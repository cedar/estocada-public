package rwTests;


import org.junit.Test;


public class TestRewritings {

	@Test
	public void TestSimple1() {
		TestUtils.TestComputeRewritings("TestFiles/test1");
	}
	
	@Test
	public void TestSimple2() {
		TestUtils.TestComputeRewritings("TestFiles/test2");
	}
	
	@Test
	public void TestComplex1() {
		TestUtils.TestComputeRewritings("TestFiles/test3");
	}
	
	@Test
	public void TestComplex2() {
		TestUtils.TestComputeRewritings("TestFiles/test5");
	}
	

	@Test
	public void TestComplex3() {
		TestUtils.TestComputeRewritings("TestFiles/test7");
	}
}
