/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package rwTests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

public class TestUtils {
	
	public static void TestComputeRewritings(String inputDir)
	{
		boolean ok = true;
		try {
			inputDir = Paths.get("TestUtils".getClass().getResource("/"+inputDir).toURI()).toString();
			cbTool.ToolUtils.ComputeRewritings(inputDir);
			ok = FileUtils.contentEquals(new File(inputDir+"/results"), new File(inputDir+"/expectedresults"));
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
		assertTrue("The files differ!", ok);
	}
	
	public static void TestChaseAndRestrict(String inputDir) 
	{
		boolean ok = true;
		try {
			inputDir = Paths.get("TestUtils".getClass().getResource("/"+inputDir).toURI()).toString();
			cbTool.ToolUtils.ChaseAndRestrict(inputDir);
			ok = FileUtils.contentEquals(new File(inputDir+"/results_chase"), new File(inputDir+"/expectedresults_chase"));
		} catch (Exception e) {
			e.printStackTrace(System.out);
			fail(e.getMessage());
		}
		assertTrue("The files differ!", ok);
	}	
}