/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 */
package fr.inria.oak.edu.cb.cbTool;

import java.io.File;

import org.apache.log4j.Logger;

public class Main {
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String args[]) throws Exception {
        final File[] files = new File("src/test/resources/ExperTests/").listFiles();
        for (File file : files) {
            if (!file.getName().equals("Test01")) {
                continue;
            } else {
                logger.debug(file.getName() + ": ");
                CBConfig.loadConfig();
                ToolUtils.ComputeRewritings(file.getAbsolutePath(), CBConfig.getProperties());
            }
        }
    }
}
