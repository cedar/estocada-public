package edu.ucsd.db.query;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Vector;

import edu.ucsd.db.canonicaldb.CanonicalSchema;
import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Relation;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Value;

/**
 * This operator takes as input a predicate and a database instance and produces
 * the database tuples matching the predicate. It is a ScanRelation operator
 * that also does Selection and Join inside the relational atom.
 */
public class MapPredicates extends Operator {

	/**
	 * The query <code>Predicate</code>.
	 */
	Predicate queryPred;

	/**
	 * The <code>Database</code> instance on which the query is run.
	 */
	Database db;

	/**
	 * An iterator over the set of tuples of the database <code>Relation</code>
	 * corresponding to the query predicate.
	 */
	Relation.TupleIterator dbTupleIter;

	/**
	 * Map of a column position to the position of the immediately previous
	 * column with the same variable (if it exists). This is used for joins.
	 */
	Map<Integer, Integer> joins;
	
	/**
	 * The projection component.
	 */
	UnaryProjectComponent project;

	// TODO: Allow to set the parent operator

	/**
	 * Constructor.
	 * 
	 * @param _queryPred
	 *            A <code>Predicate</code> of the query <code>Statement</code>
	 * @param _db
	 *            The contained <code>Database</code> instance
	 */
	public MapPredicates(Predicate _queryPred, Database _db, CanonicalSchema outputSchema, LinkedHashMap<Integer, Value> newVars) {
		super();
		
		this.queryPred = _queryPred;
		this.db = _db;

		// TODO: Give error if database does not contain such a relation
		// or the database is null
		Relation dbRelation = db.getRelationInstance(queryPred
				.getFunctionHead());
		dbTupleIter = dbRelation.getTupleIterator(this);

		joins = findJoins();
		
		// Create a <code>ProjectComponent</code>
		CanonicalSchema predSchema = queryPred.getCanonicalSchema();
		project = new UnaryProjectComponent(outputSchema, predSchema, newVars);
		
		outSchema = project.outSchema;
	}

	/**
	 * Finds joins within the same predicate.
	 */
	private Map<Integer, Integer> findJoins() {
		// TODO: Decide which map class we should use
		Map<String, Integer> vars = new LinkedHashMap<String, Integer>();
		Map<Integer, Integer> _joins = new LinkedHashMap<Integer, Integer>();
		Value queryVal;

		for (int i = 0; i < queryPred.size(); i++) {
			queryVal = queryPred.variableI(i);

			if (!queryVal.isAnyConstant()) {

				if (vars.containsKey(queryVal.getValueStr()))
					_joins.put(i, vars.get(queryVal.getValueStr()));

				vars.put(queryVal.getValueStr(), i);
			}
		}

		return _joins;
	}

	// TODO: Explain it better.
	// TODO: We may leave in a column which is not needed because it is not
	// used in a join with another predicate

	/**
	 * Sets the schema of the tuples this operator outputs. It projects out the
	 * columns that are conditioned with constants in the query predicate. It
	 * also leaves only a single occurence of join cols.
	 */
	protected void setOutSchema() {
//		Value val;
//		String valName;
//
//		for (int k = 0; k < queryPred.size(); k++) {
//			val = queryPred.variableI(k);
//			valName = val.getValueStr();
//			
//			// If <code>joins</code> does not contain as key a position, then the
//			// <code>queryPred</code> does not join on the variable in that
//			// position or it is the first occurrence of that variable
//			if (!val.isAnyConstant() && !joins.containsKey(k)) {
//				outSchema.addColumnName(valName);
//			}
//		}
	}

	/**
	 * This Operator does not have an input queue, since its input is the
	 * database instance.
	 */
	public void enqueue(LinkedList<Tuple> tuples, Object caller) {
		throw new QueryEvaluationException("Enqueue called for "
				+ this.getClass().getName());
	}
	
	public void enqueue(Tuple tuple, Object caller) {
		throw new QueryEvaluationException("Enqueue called for "
				+ this.getClass().getName());		
	}
	
	public void enqueueSingle(Tuple tuple, Object caller) {
		throw new QueryEvaluationException("Enqueue called for "
				+ this.getClass().getName());		
	}

	/**
	 * Gets the next <code>Tuple</code> produced by this operator.
	 * 
	 * @return a <code>Tuple</code>object
	 * @throws QueryEvaluationException
	 *             if there is no next <code>Tuple</code>
	 */
	protected Tuple next() {
		Value queryVal;
		Value dbVal;
		Tuple tupleAfterProjection = null;
		Tuple nextTuple = new Tuple(queryPred.size());
		boolean foundTuple;
//		int queryPredSize, curTupleSize;
		
		//System.out.println("Map predicates calling next");
		//System.out.println(queryPred);
		//project.outSchema.display();
		
		int queryPredSize = queryPred.size();
		
		while (dbTupleIter.hasNext()) {
			Tuple curTuple = dbTupleIter.next();

			// TODO: Throw an exception when the queryPred and curTuple have
			// different size
			
			foundTuple = true;
			
			for (int i = 0; i < queryPredSize; i++) {
				queryVal = queryPred.variableI(i);
				dbVal = curTuple.getValue(i);
				
				if ((queryVal.isAnyConstant() && !queryVal.equals(dbVal))
						|| (!queryVal.isAnyConstant() && joins.containsKey(i) && !dbVal
								.equals(curTuple.getValue(joins.get(i))))) {
					foundTuple = false;
					break;
				}
			}

			 if (foundTuple) {
				tupleAfterProjection = project.projectTuple(curTuple);
				if (tupleAfterProjection != null)
					break;
			 }
		}

		// This might be also null if no tuple was produced
		//System.out.println("tuple after projection: "+tupleAfterProjection);
		return tupleAfterProjection;
	}
	
	protected Tuple nextSingle() {
		return next();
	}
	
	protected boolean hasEmptyInput() {
		return !dbTupleIter.hasNext();
	}
	
	protected StringBuffer printString() {
		if (queryPred == null)
			return new StringBuffer("");
		
		StringBuffer retVal = new StringBuffer(project.printString());
		
		retVal.append(" Scan(");
		retVal.append(queryPred.printString().toString());
		retVal.append(")");
		
		return retVal;
	}
	
	protected boolean isBlockedDefault() {
		return false;
	}
	
	protected void flush() {
		dbTupleIter.reset();
		project.flush();
	}

}
