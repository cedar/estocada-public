package edu.ucsd.db.chase;

import java.util.ArrayList;
import java.util.HashMap;

import edu.ucsd.db.canonicaldb.Database;
import edu.ucsd.db.canonicaldb.Tuple;
import edu.ucsd.db.chaseexceptions.InconsistencyException;
import edu.ucsd.db.datalogexpr.Predicate;
import edu.ucsd.db.datalogexpr.Statement;
import edu.ucsd.db.datalogexpr.Value;
import edu.ucsd.db.query.QueryTree;


/**
 * This class represents a database constraint in the context of a
 * <code>CanonicalDB<code>.
 */
public abstract class Constraint {

	/**
	 * Static counter used to generate IDs for the constraints
	 */
	private static int generateID = 1;
	
	/**
	 * The canonical database to which this constraint is to be applied
	 */
	protected Database db;
	
	/**
	 * The id of the constraint.
	 */
	protected int id = -1;

	protected Constraint(Database _db) {
		db = _db;
	}
	
	public final int getID() {
		if (id == -1)
			id = generateID++;

		return id;
	}
	
	/**
	 * Abstract function called to enforce the constraint.
	 * 
	 * @return True if constraint had effect on the canonical database,
	 * 		   False otherwise
	 */
	public abstract boolean enforce() throws InconsistencyException;
	
	/**
	 * Abstract function that flushes the query tree corresponding to the
	 * constraint.
	 */
	public abstract void flush();
	
	public void createFreshInstanceOfQueryBody(Statement query, Tuple headValues,
			boolean makeFreshValuesSkolems) {
		Tuple tuple;
		Value value, variable;
		int listSize, predSize;
		Predicate curPred;
		String varName;
		
		// Add to the hashtable the values that correspond to the vars in the head
		HashMap<String, Value> varsToValues = new HashMap<String, Value>();
		
		if (headValues != null) {
			int headSize = headValues.size();
			Predicate queryHead = query.head();
			
			for (int i = 0; i < headSize; i++) {
				variable = query.head().variableI(i);
				
				if (!variable.isAnyConstant()) {
					varName = variable.getValueStr();
					value = headValues.getValue(i);
					varsToValues.put(varName, value);
				}
			}
		}
		
		// Look at the body and create new tuples
		ArrayList<Predicate> preds = query.getPredicates();
		listSize = preds.size();
		
		// Loop over all predicates
		for (int i = 0; i < listSize; i++) {
			curPred = preds.get(i);
			predSize = curPred.size();
			
			// Create new tuple
			tuple = new Tuple(predSize);
			
			// For each variable in the predicate, add values
			for (int j = 0; j < predSize; j++) {
				value = curPred.variableI(j);
				
				if (!value.isAnyConstant()) {
					varName = value.getValueStr();
					if (varsToValues.containsKey(varName))
						value = varsToValues.get(varName);
					else {
						if (makeFreshValuesSkolems)
							value = Value.createFreshSkolem();
						else
							value = Value.createFreshVariable();
						varsToValues.put(varName, value);
					}
				}
				
				tuple.addValue(value);
			}
			
			// Add tuple to the relation
			db.getRelationInstance(curPred.getFunctionHead()).addTuple(tuple);
		}
	}

}
