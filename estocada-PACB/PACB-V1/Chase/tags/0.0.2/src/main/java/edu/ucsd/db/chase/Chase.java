package edu.ucsd.db.chase;

import java.util.ArrayList;
import java.util.List;

import edu.ucsd.db.chaseexceptions.InconsistencyException;
import edu.ucsd.db.chaseexceptions.RedundancyException;

/**
 * This interface represents a chase engine which is associated with a
 * <code>CanonicalDB<code>.
 */
public class Chase {

	private ArrayList<Assertion> assertions;
	private ArrayList<TupleGenConstraint> tgds;
	private ArrayList<EqualityGenConstraint> egds;

	public Chase(List<TupleGenConstraint> tupleConstraints,
			List<EqualityGenConstraint> equalityConstraints,
			List<Assertion> assertionConstraints) {

		tgds = new ArrayList<TupleGenConstraint>(tupleConstraints);
		egds = new ArrayList<EqualityGenConstraint>(equalityConstraints);
		assertions = new ArrayList<Assertion>(assertionConstraints);
	}

	/**
	 * Sets the set of tuple and equality generating constraints, as well as
	 * assertions, registered by the same and other sources. The implementation
	 * should create a "base" canonical instance whenever the existing
	 * constraints are set. After that, the chase engine will chase on the base
	 * canonical instance with ALL constraints.
	 *
	 * @param constraints
	 *            Constraints registered by the same and other sources.
	 * @param equalityConstraints
	 *            The <code>EqualityGenConstraint</code> objects
	 * @param assertions
	 *            The <code>Assertion</code> objects
	 */
//	public void setConstraints(
//			ArrayList<TupleGenConstraint> tupleConstraints,
//			ArrayList<EqualityGenConstraint> equalityConstraints,
//			ArrayList<Assertion> assertionConstraints) {
//
//		tgds = new ArrayList<TupleGenConstraint>(tupleConstraints);
//		egds = new ArrayList<EqualityGenConstraint>(equalityConstraints);
//		assertions = new ArrayList<Assertion>(assertionConstraints);
//	}

// TODO: Allow different policies on the order on which constraints are enforced

	/**
	 * Creates a <code>CanonicalDB</code> and chases it with the set of given
	 * <code>Constraint</code> objects.
	 *
	 * @return the <code>CanonicalDB</code> created
	 * @throws RedundancyException
	 *             when the given <code>Constraint</code> objects do not
	 *             augment the <code>CanonicalDB</code>.
	 * @throws InconsistencyException
	 *             when the chase tries to equate two different constants.
	 */
	public void run() throws InconsistencyException {
		// Enforce assertions
		for (int i = 0; i < assertions.size(); i++) {
			assertions.get(i).enforce();
		}

		// Enforce tgds
		boolean changed = true;
		boolean localChange;

		while (changed) {
			changed = false;

			for (int i = 0; i < tgds.size(); i++) {
				TupleGenConstraint curTgd = tgds.get(i);
//				if (curTgd.needsActivation())
					localChange = curTgd.enforce();
				//else
				//	localChange = false;
				changed = changed || localChange;
			}
		}

		// Enforce egds and tgds together
		changed = true;

		while (changed) {
			changed = false;

			for (int i = 0; i < tgds.size(); i++) {
				TupleGenConstraint curTgd = tgds.get(i);
				//if (curTgd.needsActivation())
					localChange = curTgd.enforce();
				//else
				//	localChange = false;
				changed = changed || localChange;
			}

			for (int i = 0; i < egds.size(); i++) {
				EqualityGenConstraint curEgd = egds.get(i);
				localChange = curEgd.enforce();

				if (localChange) {
					for (int j = 0; j < tgds.size(); j++) {
						tgds.get(j).flush();
					}
					for (int j = 0; j < egds.size(); j++) {
						egds.get(j).flush();
					}
				}

				changed = changed || localChange;
			}
		}

	}
}
