/**
 * Copyright © 2015 The Regents of the University of California. All Rights Reserved. 
 * 
 * @author Ioana Ileana
 */

package cbInternal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import fr.inria.oak.commons.conjunctivequery.Atom;
import fr.inria.oak.commons.conjunctivequery.ConjunctiveQuery;
import fr.inria.oak.commons.conjunctivequery.IntegerConstant;
import fr.inria.oak.commons.conjunctivequery.StringConstant;
import fr.inria.oak.commons.conjunctivequery.Term;
import fr.inria.oak.commons.conjunctivequery.Variable;
import fr.inria.oak.commons.constraints.Constraint;
import fr.inria.oak.commons.constraints.Egd;
import fr.inria.oak.commons.constraints.Equality;
import fr.inria.oak.commons.constraints.Tgd;
import instance.ChasedInstance;

/**
 * Wrapper class for the Backchase/reformulation engine
 */

public class BackchaseWrapper {
    /**
     * Runs the Backchase phase of Prov C&B
     * 
     * @param initquery
     *            The query to be rewritten
     * @param uPlan
     *            The universal plan
     * @param constraints
     *            The set of constraints for the Backchase
     * 
     * @return The reformulations
     */
    public static ArrayList<ConjunctiveQuery> Backchase(ConjunctiveQuery initquery, ConjunctiveQuery uPlan,
            ArrayList<Constraint> bwconstraints, long chaseTime) throws Exception {
        ConjunctiveQuery queryAsInstance = addSpecialVars(initquery);
        CQWithEq query = new CQWithEq(queryAsInstance);

        ConjunctiveQuery uPlanAsInstance = addSpecialVars(uPlan);

        ArrayList<ConstraintWithEq> constraints = getConstraintsWithEq(bwconstraints);

        BufferedWriter writer = new BufferedWriter(new FileWriter("bkchasetmp.in"));
        writer.write(uPlan.getBody().size() + 1 + "\n"); //for now, an additional symbol; should be revised

        //query
        writer.write("query\n");
        ArrayList<Atom> relquery = query.getBodyRel();
        for (int i = 0; i < relquery.size(); ++i) {
            Atom crtAtom = relquery.get(i);
            writer.write(toBackchaseInput(crtAtom));
            if (i < relquery.size() - 1)
                writer.write(",");
            else
                writer.write("\n");
        }

        ArrayList<Equality> eqquery = query.getBodyEq();
        for (int i = 0; i < eqquery.size(); ++i) {
            Equality crtEq = eqquery.get(i);
            writer.write(toBackchaseInput(crtEq));
            if (i < eqquery.size() - 1)
                writer.write(",");
        }
        writer.write("\n");

        //Atom head = query.getHead();
        //writer.write(Utils.toBackchaseInput(head)+"\n");
        writer.write("\n");

        //constraints
        writer.write("constraints\n");
        writer.write(constraints.size() + "\n");

        for (int i = 0; i < constraints.size(); ++i) {
            ConstraintWithEq crtConstraint = constraints.get(i);

            if (crtConstraint instanceof TgdWithEq)
                writer.write("TGD\n");
            else
                writer.write("EGD\n");

            ArrayList<Atom> premRel = crtConstraint.getPremiseRel();
            for (int j = 0; j < premRel.size(); ++j) {
                Atom crtAtom = premRel.get(j);
                writer.write(toBackchaseInput(crtAtom));
                if (j < premRel.size() - 1)
                    writer.write(",");
            }
            if (premRel.size() == 0)
                writer.write("TRUE()");
            writer.write("\n");

            ArrayList<Equality> premEq = crtConstraint.getPremiseEq();
            for (int j = 0; j < premEq.size(); ++j) {
                Equality crtEq = premEq.get(j);
                writer.write(toBackchaseInput(crtEq));
                if (j < premEq.size() - 1)
                    writer.write(",");
            }
            writer.write("\n");

            if (crtConstraint instanceof TgdWithEq) {
                ArrayList<Atom> concRel = ((TgdWithEq) crtConstraint).getConclusion();
                for (int j = 0; j < concRel.size(); ++j) {
                    Atom crtAtom = concRel.get(j);
                    writer.write(toBackchaseInput(crtAtom));
                    if (j < concRel.size() - 1)
                        writer.write(",");
                }
                writer.write("\n");
            } else {

                ArrayList<Equality> concEq = ((EgdWithEq) crtConstraint).getConclusion();
                for (int j = 0; j < concEq.size(); ++j) {
                    Equality crtEq = concEq.get(j);
                    writer.write(toBackchaseInput(crtEq));
                    if (j < concEq.size() - 1)
                        writer.write(",");
                }
                writer.write("\n");

            }
        }

        //universal plan
        writer.write("instance\n");
        Collection<Atom> uPlanAtoms = uPlanAsInstance.getBody();
        int i = 0;
        for (Atom crtAtom : uPlanAtoms) {
            writer.write(toBackchaseInput(crtAtom) + " p" + (i++) + "\n");
        }
        writer.write("TRUE() true");

        writer.close();
        ChasedInstance.Restart();
        System.out
                .println("Rewriting Time: " + ChasedInstance.zeInstance.computeRewritings("bkchasetmp.in") + chaseTime);
        //		 System.out.println("Stam print");
        //		for(instance.Tgd t:ChasedInstance.zeInstance.m_tgds){
        //			System.out.println(t);
        //		}

        ArrayList<ConjunctiveQuery> results = new ArrayList<ConjunctiveQuery>();
        if (ChasedInstance.zeInstance.m_query.m_provenance == null) //no rewritings
            return results; //empty list

        List<flatProvenance.FlatConjunct> rwconj = ChasedInstance.zeInstance.m_query.m_provenance.getConjuncts();

        ArrayList<Atom> uplanAtoms = new ArrayList<Atom>();
        for (Atom tmp : uPlan.getBody())
            uplanAtoms.add(tmp);
        for (i = 0; i < rwconj.size(); ++i) {
            ArrayList<Atom> rwAtoms = new ArrayList<Atom>();
            String conjStr = rwconj.get(i).toString();
            String[] symbols = conjStr.split(",");
            for (int j = 0; j < symbols.length; ++j)
                if (symbols[j].substring(0, 1).equals("p")) {
                    int index = Integer.parseInt(symbols[j].substring(1));
                    rwAtoms.add(uplanAtoms.get(index));
                }
            ConjunctiveQuery rw = new ConjunctiveQuery("RW" + String.valueOf(i), uPlan.getHead(), rwAtoms);
            results.add(rw);
        }

        return results;
    }

    private static ConjunctiveQuery addSpecialVars(ConjunctiveQuery query) {
        HashSet<String> specialVars = new HashSet<String>();
        for (Term term : query.getHead())
            if (term.isVariable())
                specialVars.add(((Variable) term).getName());
        ArrayList<Atom> newAtoms = new ArrayList<Atom>();
        for (Atom atom : query.getBody()) {
            ArrayList<Term> newTerms = new ArrayList<Term>();
            for (Term term : atom.getTerms())
                if (term.isVariable() && specialVars.contains(((Variable) term).getName()))
                    newTerms.add(new StringConstant("$" + ((Variable) term).getName()));
                else
                    newTerms.add(term);
            newAtoms.add(new Atom(atom.getPredicate(), newTerms));
        }
        return new ConjunctiveQuery(query.getName(), query.getHead(), newAtoms);
    }

    private static String toBackchaseInput(Atom atom) {
        String stringRep = atom.getPredicate() + "(";
        for (int i = 0; i < atom.getTerms().size(); ++i) {
            Term crtTerm = atom.getTerm(i);
            if (crtTerm.isVariable())
                stringRep += crtTerm;
            else {
                if (crtTerm instanceof StringConstant)
                    stringRep += "co." + ((StringConstant) crtTerm).getValue();
                else
                    stringRep += "co." + ((IntegerConstant) crtTerm).getValue();
            }
            if (i < atom.getTerms().size() - 1)
                stringRep += ",";
            else
                stringRep += ")";
        }
        return stringRep;
    }

    private static String toBackchaseInput(Equality eq) {
        String stringRep = "";
        Term crtTerm = eq.getTerm1();
        if (crtTerm instanceof Variable)
            stringRep += eq.getTerm1();
        else {
            if (crtTerm instanceof StringConstant)
                stringRep += "co." + ((StringConstant) crtTerm).getValue();
            else
                stringRep += "co." + ((IntegerConstant) crtTerm).getValue();
        }
        stringRep += "=";

        crtTerm = eq.getTerm2();
        if (crtTerm instanceof Variable)
            stringRep += eq.getTerm2();
        else {
            if (crtTerm instanceof StringConstant)
                stringRep += "co." + ((StringConstant) crtTerm).getValue();
            else
                stringRep += "co." + ((IntegerConstant) crtTerm).getValue();
        }
        return stringRep;
    }

    private static ArrayList<ConstraintWithEq> getConstraintsWithEq(ArrayList<Constraint> constraints) {
        ArrayList<ConstraintWithEq> result = new ArrayList<ConstraintWithEq>();
        for (Constraint constraint : constraints) {
            if (constraint instanceof Egd)
                result.add(new EgdWithEq((Egd) constraint));
            else
                result.add(new TgdWithEq((Tgd) constraint));
        }
        return result;
    }

}
