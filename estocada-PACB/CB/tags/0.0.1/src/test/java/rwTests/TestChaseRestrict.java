package rwTests;

import org.junit.Test;

public class TestChaseRestrict{

	@Test
	public void TestSimple() {
		TestUtils.TestChaseAndRestrict("TestFiles/test4");
	}
	
	@Test
	public void TestComplex1() {
		TestUtils.TestChaseAndRestrict("TestFiles/test3");
	}

	@Test
	public void TestComplex2() {
		TestUtils.TestChaseAndRestrict("TestFiles/test6");
	}
	

	@Test
	public void TestComplex3() {
		TestUtils.TestChaseAndRestrict("TestFiles/test7");
	}
	
	@Test
	public void TestComplex4() {
		TestUtils.TestChaseAndRestrict("TestFiles/test8");
	}
}
