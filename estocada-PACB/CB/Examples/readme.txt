This folder contains some simple examples.

FILE STRUCTURE

- schema

The schema file contains two lines, each line ends with a ;

In the first line we have all the relations that are used and their arity; in the second line we have all the 
relations in the output schema with their arity.

- constraints_chase

file used for the forward constraints

- constraits_bkchase

file used for the backward constraints

NOTE:
We used constraits_bkchase to write one constraint.